package eu.domibus.plugin.fs;

import eu.domibus.api.scheduler.DomibusScheduler;
import eu.domibus.ext.services.DomibusSchedulerExtService;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.BackendConnector;
import eu.domibus.plugin.fs.queue.FSSendMessageListenerContainer;
import eu.domibus.test.AbstractIT;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@TestPropertySource(value = "file:${domibus.config.location}/dataset/fsplugin/fs-plugin.properties")
public class AbstractFSPluginTestIT extends AbstractIT {

    @Configuration
    static class ContextConfiguration {

        @Primary
        @Bean
        public DomibusSchedulerExtService domibusSchedulerExt() {
            return Mockito.mock(DomibusSchedulerExtService.class);
        }

        @Primary
        @Bean
        public DomibusScheduler domibusScheduler() {
            return Mockito.mock(DomibusScheduler.class);
        }

        @Primary
        @Bean
        public FSSendMessageListenerContainer messageListenerContainer() {
            return Mockito.mock(FSSendMessageListenerContainer.class);
        }

    }

    @Autowired
    FSPluginImpl fsPlugin;


    @Override
    protected List<BackendConnector<?, ?>> getBackendConnectors() {
        return Arrays.asList(fsPlugin);
    }

    @BeforeEach
    public void before() throws XmlProcessingException, IOException {
        Mockito.when(backendConnectorProvider.getBackendConnector(Mockito.any(String.class))).thenReturn((BackendConnector) fsPlugin);
        Mockito.when(backendConnectorProvider.getEnableAware(Mockito.any(String.class))).thenReturn(fsPlugin);
        //activate sync notifications
        itTestsService.clearConnectorFromAsyncNotification();
        deleteCurrentPMode();
    }
}
