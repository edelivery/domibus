package eu.domibus.plugin.fs.property;

import eu.domibus.ext.exceptions.DomibusPropertyExtException;
import eu.domibus.plugin.fs.AbstractFSPluginTestIT;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static eu.domibus.plugin.fs.property.FSPluginPropertiesMetadataManagerImpl.LOCATION;

/**
 * @author Catalin Enache
 * @since 5.0
 */
class FSPluginPropertiesMultitenantIT extends AbstractFSPluginTestIT {

    private static final String DOMAIN1 = "default";
    private static final String DOMAIN2 = "red";
    private static final String NONEXISTENT_DOMAIN = "NONEXISTENT_DOMAIN";

    private static final String DOMAIN1_LOCATION = "/tmp/fs_plugin_data/default";

    @Autowired
    FSPluginProperties fsPluginProperties;

    @Test
    void testGetLocation_Domain1() {
        String location = fsPluginProperties.getLocation(DOMAIN1);
        Assertions.assertEquals(DOMAIN1_LOCATION, location);
    }

    @Test
    void testGetLocation_NonExistentDomain() {

        DomibusPropertyExtException e = Assertions.assertThrows(DomibusPropertyExtException.class, () -> fsPluginProperties.getLocation(NONEXISTENT_DOMAIN));
        Assertions.assertTrue(e.getMessage().contains("Could not find domain with code"));
    }

    @Test
    void testGetUser() {
        String user = fsPluginProperties.getUser(DOMAIN1);
        Assertions.assertEquals("user1", user);
    }

    @Test
    void testGetPassword() {
        Assertions.assertEquals("pass1", fsPluginProperties.getPassword(DOMAIN1));
    }

    @Test
    void testGetUser_NotSecured() {
        Assertions.assertNull(fsPluginProperties.getUser(DOMAIN2));
    }

    @Test
    void testGetPayloadId_Domain() {
        Assertions.assertEquals("cid:message", fsPluginProperties.getPayloadId(DOMAIN1));
    }

    @Test
    void testGetPassword_NotSecured() {
        Assertions.assertNull(fsPluginProperties.getPassword(DOMAIN2));
    }

    @Test
    void testKnownPropertyValue_multiTenancy() {
        final String oldPropertyValue1 = "/tmp/fs_plugin_data/default";
        final String oldPropertyValue2 = "/tmp/fs_plugin_data/red";
        final String newPropertyValue1 = "new-property-value1";
        final String newPropertyValue2 = "new-property-value2";

        // test get value
        String value1 = fsPluginProperties.getKnownPropertyValue(DOMAIN1, LOCATION);
        String value2 = fsPluginProperties.getKnownPropertyValue(DOMAIN2, LOCATION);

        Assertions.assertEquals(oldPropertyValue1, value1);
        Assertions.assertEquals(oldPropertyValue2, value2);

        // test set value
        fsPluginProperties.setKnownPropertyValue(DOMAIN1, LOCATION, newPropertyValue1, true);
        fsPluginProperties.setKnownPropertyValue(DOMAIN2, LOCATION, newPropertyValue2, true);

        value1 = fsPluginProperties.getKnownPropertyValue(DOMAIN1, LOCATION);
        value2 = fsPluginProperties.getKnownPropertyValue(DOMAIN2, LOCATION);

        Assertions.assertEquals(newPropertyValue1, value1);
        Assertions.assertEquals(newPropertyValue2, value2);

        // reset context
        fsPluginProperties.setKnownPropertyValue(DOMAIN1, LOCATION, oldPropertyValue1, true);
        fsPluginProperties.setKnownPropertyValue(DOMAIN2, LOCATION, oldPropertyValue2, true);
    }

}
