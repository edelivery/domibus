package eu.domibus.plugin.fs.property;

import eu.domibus.api.property.DomibusPropertyException;
import eu.domibus.api.scheduler.DomibusScheduler;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomainExtService;
import eu.domibus.ext.services.DomibusSchedulerExtService;
import eu.domibus.plugin.fs.AbstractFSPluginTestIT;
import eu.domibus.plugin.fs.FSPluginImpl;
import eu.domibus.plugin.fs.property.listeners.FSPluginEnabledChangeListener;
import eu.domibus.plugin.fs.property.listeners.OutQueueConcurrencyChangeListener;
import eu.domibus.plugin.fs.property.listeners.TriggerChangeListener;
import eu.domibus.plugin.fs.queue.FSSendMessageListenerContainer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static eu.domibus.plugin.fs.property.FSPluginPropertiesMetadataManagerImpl.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

/**
 * @author Ion Perpegel
 * @author Catalin Enache
 * @since 4.2
 */
class ChangeListenersTestIT extends AbstractFSPluginTestIT {

    @Autowired
    private DomibusSchedulerExtService domibusSchedulerExt;

    @Autowired
    private FSSendMessageListenerContainer messageListenerContainer;

    @Autowired
    private OutQueueConcurrencyChangeListener outQueueConcurrencyChangeListener;

    @Autowired
    private TriggerChangeListener triggerChangeListener;

    @Autowired
    private FSPluginEnabledChangeListener enabledChangeListener;

    @Autowired
    private DomainExtService domainExtService;

    @Autowired
    private FSPluginProperties fsPluginProperties;

    @Autowired
    DomibusScheduler domibusScheduler;

    @Autowired
    FSPluginImpl fsPlugin;

    @Test
    void testTriggerChangeListener() {
        boolean handlesWorkerInterval = triggerChangeListener.handlesProperty(SEND_WORKER_INTERVAL);
        Assertions.assertTrue(handlesWorkerInterval);

        try {
            triggerChangeListener.propertyValueChanged("default", SEND_WORKER_INTERVAL, "wrong-value");
            Assertions.fail("Expected exception not raised");
        } catch (IllegalArgumentException e) {
            Assertions.assertTrue(e.getMessage().contains("Invalid"));
        }

        triggerChangeListener.propertyValueChanged("default", SEND_WORKER_INTERVAL, "3000");
        triggerChangeListener.propertyValueChanged("default", SENT_PURGE_WORKER_CRONEXPRESSION, "0 0/15 * * * ?");

        Mockito.verify(domibusSchedulerExt, Mockito.times(1)).rescheduleJob("default", "fsPluginSendMessagesWorkerJob", 3000);
        Mockito.verify(domibusSchedulerExt, Mockito.times(1)).rescheduleJob("default", "fsPluginPurgeSentWorkerJob", "0 0/15 * * * ?");
    }

    @Test
    void testOutQueueConcurrencyChangeListener() {

        boolean handlesProperty = outQueueConcurrencyChangeListener.handlesProperty(OUT_QUEUE_CONCURRENCY);
        Assertions.assertTrue(handlesProperty);

        DomainDTO aDefault = domainExtService.getDomain("default");

        outQueueConcurrencyChangeListener.propertyValueChanged("default", OUT_QUEUE_CONCURRENCY, "1-2");
        Mockito.verify(messageListenerContainer, Mockito.times(1)).updateMessageListenerContainerConcurrency(aDefault, "1-2");
    }

    @Test
    void testEnabledChangeListenerException() {
        boolean handlesProperty = enabledChangeListener.handlesProperty(DOMAIN_ENABLED);
        Assertions.assertTrue(handlesProperty);

        Assertions.assertThrows(DomibusPropertyException.class, () -> fsPluginProperties.setKnownPropertyValue(DOMAIN_ENABLED, "false"));
    }

    @Test
    void testEnabledChangeListener() {
        String domainCode = "default";

        boolean handlesProperty = enabledChangeListener.handlesProperty(DOMAIN_ENABLED);
        Assertions.assertTrue(handlesProperty);

        DomibusPropertyException ex = Assertions.assertThrows(DomibusPropertyException.class, () -> fsPluginProperties.setKnownPropertyValue(DOMAIN_ENABLED, "false"));
        assertThat(ex.getCause().getMessage(), containsString("Cannot disable the plugin [backendFSPlugin] on domain [default] because there won't remain any enabled"));

        Assertions.assertTrue(fsPlugin.isEnabled(domainCode));
    }
}
