package eu.domibus.plugin.fs;

import eu.domibus.api.jms.JmsMessage;
import eu.domibus.api.model.AbstractBaseAuditEntity;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.security.AuthRole;
import eu.domibus.common.MessageStatus;
import eu.domibus.common.MessageStatusChangeEvent;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.jms.JMSManagerImpl;
import eu.domibus.ext.delegate.services.jms.JMSExtServiceDelegate;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessageConstants;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.fs.queue.FSSendMessageListener;
import eu.domibus.plugin.fs.worker.FSProcessFileService;
import eu.domibus.plugin.fs.worker.FSSendMessagesService;
import eu.domibus.test.PModeUtil;
import eu.domibus.test.common.SoapAttachmentTest;
import eu.domibus.test.common.SoapSampleUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.net.util.Base64;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.xml.sax.SAXException;

import javax.jms.Queue;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static eu.domibus.plugin.fs.property.FSPluginPropertiesMetadataManagerImpl.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FSPluginTestIT extends AbstractFSPluginTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(FSPluginTestIT.class);


    @Autowired
    FSPluginImpl fsPlugin;

    @Autowired
    FSSendMessagesService fsSendMessagesService;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    FSProcessFileService fsProcessFileService;

    @Autowired
    JMSExtServiceDelegate jmsExtServiceDelegate;

    @Autowired
    FSSendMessageListener fsSendMessageListener;

    @Autowired
    PModeUtil pModeUtil;

    @Autowired
    MSHWebservice mshWebserviceTest;

    File domainDefaultFSPluginLocation;
    private Domain currentDomain;

    @BeforeEach
    @Override
    public void before() throws XmlProcessingException, IOException {
        super.before();
        pModeUtil.uploadPmode(null, "PModeTemplate_fs.xml");

        deleteAllMessages();

        //create the directory location for the default domain
        domainDefaultFSPluginLocation = new File("target/fsPlugin/default");
        FileUtils.deleteDirectory(domainDefaultFSPluginLocation);
        FileUtils.forceMkdir(domainDefaultFSPluginLocation);

        //set the property for the directory location for the default domain
        currentDomain = domainContextProvider.getCurrentDomain();
        String absolutePath = domainDefaultFSPluginLocation.getAbsolutePath();
        domibusPropertyProvider.setProperty(currentDomain, LOCATION, absolutePath);
    }

    @AfterEach
    public void clean() {
        deleteAllMessages();
        // reset default values
        domibusPropertyProvider.setProperty(currentDomain, LOCATION, "/tmp/fs_plugin_data/default");
        domibusPropertyProvider.setProperty(currentDomain, SENT_ACTION, "delete");
        domibusPropertyProvider.setProperty(currentDomain, SEND_DELAY, "2000");
    }

    @Test
    void submitMessage() throws IOException {
        //make sure there are no message leftovers from other tests; we have only the dummy UserMessage with entityId 19700101
        assertEquals(1, userMessageDao.findAll().size());

        //create the plugin user used to authenticate for the default plugin
        itTestsService.createPluginUser(AuthRole.ROLE_ADMIN, "defaultUser", ".2yHNdJshvLevRGLMijsWM");

        //create the OUT folder
        final File outFolder = new File(domainDefaultFSPluginLocation, "OUT");
        FileUtils.forceMkdir(outFolder);

        //we write the metadata.xml file to the OUT folder
        final byte[] metadataFileBytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResource("eu/domibus/plugin/fs/FSXMLHelper_testParseXML_metadata.xml"));
        FileUtils.writeByteArrayToFile(new File(outFolder, "metadata.xml"), metadataFileBytes);

        //create a file in the OUT folder so that it can be sent
        final String fileToBeSubmitted = "file1.txt";
        final File file = new File(outFolder, fileToBeSubmitted);
        file.setLastModified(DateUtils.addMinutes(new Date(), -1 * 10).getTime());
        FileUtils.writeByteArrayToFile(file, "hello".getBytes(StandardCharsets.UTF_8));

        //disable the timestamp verification
        domibusPropertyProvider.setProperty(currentDomain, SEND_DELAY, "0");

        //modify the JMS manager to process the file directly
        Object savedJmsManager = null;
        try {
            savedJmsManager = modifyJmsManagerToSubmitUserMessage();
            fsSendMessagesService.sendMessages();
        } finally {
            //we put back the original JMS manager
            itTestsService.setJmsManager(savedJmsManager);
        }

        //we put back the domain on the context; sendMessagesSafely cleans the domain
        domainContextProvider.setCurrentDomain(currentDomain);

        final List<UserMessage> allMessages = userMessageDao.findAll();
        assertEquals(2, allMessages.size());//dummy UserMessage + our submitted message

        //we get the latest UserMessage based on the created date
        final UserMessage submittedMessage = allMessages.stream().max(Comparator.comparing(AbstractBaseAuditEntity::getCreationTime)).get();

        //expecting the file to be submitted
        final String messageId = submittedMessage.getMessageId();
        String expectedFileNameAfterSubmissionName = "file1_" + messageId + ".txt";
        final File expectedFileNameAfterSubmission = new File(outFolder, expectedFileNameAfterSubmissionName);
        assertTrue(expectedFileNameAfterSubmission.exists());

        //we change the 'sent action' to archive so that the file is not deleted from the OUT folder
        domibusPropertyProvider.setProperty(currentDomain, SENT_ACTION, "archive");

        MessageStatusChangeEvent sentStatusChange = new MessageStatusChangeEvent();
        sentStatusChange.setFromStatus(MessageStatus.SEND_ENQUEUED);
        sentStatusChange.setToStatus(MessageStatus.ACKNOWLEDGED);
        sentStatusChange.setMessageId(messageId);
        fsPlugin.messageStatusChanged(sentStatusChange);

        //the file should be moved in the SENT folder
        final File sentFolder = new File(domainDefaultFSPluginLocation, "SENT");
        final File fileInSentFolder = new File(sentFolder, expectedFileNameAfterSubmissionName);
        assertTrue(fileInSentFolder.exists());

    }

    public Object modifyJmsManagerToSubmitUserMessage() {
        //we save the JMS manager to restore it later
        Object savedJmsManager = ReflectionTestUtils.getField(jmsExtServiceDelegate, "jmsManager");

        final Domain domain = domainContextProvider.getCurrentDomain();

        ReflectionTestUtils.setField(jmsExtServiceDelegate, "jmsManager", new JMSManagerImpl() {
            @Override
            public void sendMessageToQueue(JmsMessage message, Queue destination) {
                LOG.info("Jms override [{}] [{}]", message, destination);
                //we simulate the dispatch of the JMS message
                String fileName = message.getStringProperty(MessageConstants.FILE_NAME);

                fsSendMessageListener.processFile(fileName, domain.getCode());
            }
        });
        return savedJmsManager;
    }

    @Test
    void receiveMessage() throws SOAPException, IOException, ParserConfigurationException, SAXException, XMLStreamException, JAXBException {
        String filename = "SOAPMessage2.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        final File inFolder = new File(domainDefaultFSPluginLocation, "IN");
        final File receivedMessageFolder = new File(inFolder, "urn_oasis_names_tc_ebcore_partyid-type_unregistered_C4/" + messageId);
        assertTrue(receivedMessageFolder.exists());
        final File metadataFile = new File(receivedMessageFolder, "metadata.xml");
        assertTrue(metadataFile.exists());
        final File messageFile = new File(receivedMessageFolder, "message.xml");
        assertTrue(messageFile.exists());

        final byte[] metadataBytes = FileUtils.readFileToByteArray(metadataFile);
        final eu.domibus.plugin.fs.ebms3.UserMessage userMessage = fsProcessFileService.parseMetadata(new ByteArrayInputStream(metadataBytes));
        assertEquals(messageId, userMessage.getMessageInfo().getMessageId());
        assertEquals("TC1Leg1", userMessage.getCollaborationInfo().getAction());
    }

    @Test
    void receiveMessage_noFinalRecipient() throws SOAPException, IOException, ParserConfigurationException, SAXException, XMLStreamException, JAXBException {
        String filename = "SOAPMessage2_noFinalRecipient.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        final File inFolder = new File(domainDefaultFSPluginLocation, "IN");
        final File receivedMessageFolder = new File(inFolder, messageId);
        assertTrue(receivedMessageFolder.exists());
        final File metadataFile = new File(receivedMessageFolder, "metadata.xml");
        assertTrue(metadataFile.exists());
        final File messageFile = new File(receivedMessageFolder, "message.xml");
        assertTrue(messageFile.exists());

        final byte[] metadataBytes = FileUtils.readFileToByteArray(metadataFile);
        final eu.domibus.plugin.fs.ebms3.UserMessage userMessage = fsProcessFileService.parseMetadata(new ByteArrayInputStream(metadataBytes));
        assertEquals(messageId, userMessage.getMessageInfo().getMessageId());
        assertEquals("TC1Leg1", userMessage.getCollaborationInfo().getAction());
    }


    @Test
    void receiveMessageSanitizeFileName() throws SOAPException, IOException, ParserConfigurationException, SAXException, XMLStreamException, JAXBException {
        String filename = "SOAPMessage1.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        final File inFolder = new File(domainDefaultFSPluginLocation, "IN");
        final File receivedMessageFolder = new File(inFolder, "urn_oasis_names_tc_ebcore_partyid-type_unregistered_C4/" + messageId);
        assertTrue(receivedMessageFolder.exists());
        final File metadataFile = new File(receivedMessageFolder, "metadata.xml");
        assertTrue(metadataFile.exists());
        final File messageFile = new File(receivedMessageFolder, ".._2F.._2FxmlName3.xml");
        assertTrue(messageFile.exists());

        final byte[] metadataBytes = FileUtils.readFileToByteArray(metadataFile);
        final eu.domibus.plugin.fs.ebms3.UserMessage userMessage = fsProcessFileService.parseMetadata(new ByteArrayInputStream(metadataBytes));
        assertEquals(messageId, userMessage.getMessageInfo().getMessageId());
        assertEquals("PayloadName", userMessage.getPayloadInfo().getPartInfo().get(0).getPartProperties().getProperty().get(1).getName());
        assertEquals("..%2F..%2FxmlName3.xml", userMessage.getPayloadInfo().getPartInfo().get(0).getPartProperties().getProperty().get(1).getValue());
        assertEquals(".._2F.._2FxmlName3.xml", fsPlugin.sanitizeFileName("..%2F..%2FxmlName3.xml"));
    }

    @Test
    void receiveMessageWithMultiplePayloads() throws SOAPException, IOException, ParserConfigurationException, SAXException, XMLStreamException, JAXBException {
        String filename = "SOAPMessage-with-multiple-payloads.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";
        List<SoapAttachmentTest> attachments = new ArrayList<>();
        byte[] attachment1 = java.util.Base64.getEncoder().encodeToString("PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=".getBytes(StandardCharsets.UTF_8)).getBytes(StandardCharsets.UTF_8);
        attachments.add(new SoapAttachmentTest(attachment1, "message"));
        byte[] attachment2 = java.util.Base64.getEncoder().encodeToString("aW52b2ljZSBjb250ZW50".getBytes(StandardCharsets.UTF_8)).getBytes(StandardCharsets.UTF_8);
        attachments.add(new SoapAttachmentTest(attachment2, "invoice"));

        itTestsService.receiveMessage(filename, messageId, false, attachments);

        final File inFolder = new File(domainDefaultFSPluginLocation, "IN");
        final File receivedMessageFolder = new File(inFolder, "urn_oasis_names_tc_ebcore_partyid-type_unregistered_C4/" + messageId);
        assertTrue(receivedMessageFolder.exists());
        final File metadataFile = new File(receivedMessageFolder, "metadata.xml");
        assertTrue(metadataFile.exists());
        final File messageFile = new File(receivedMessageFolder, "message.xml");
        assertTrue(messageFile.exists());

        final File invoiceFile = new File(receivedMessageFolder, "invoice.xml");
        assertTrue(invoiceFile.exists());

        final byte[] metadataBytes = FileUtils.readFileToByteArray(metadataFile);
        final eu.domibus.plugin.fs.ebms3.UserMessage userMessage = fsProcessFileService.parseMetadata(new ByteArrayInputStream(metadataBytes));
        assertEquals(messageId, userMessage.getMessageInfo().getMessageId());
        assertEquals("TC1Leg1", userMessage.getCollaborationInfo().getAction());
    }
}
