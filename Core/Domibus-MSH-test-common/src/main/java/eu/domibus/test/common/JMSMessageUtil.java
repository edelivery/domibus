package eu.domibus.test.common;

import eu.domibus.common.DomibusJMSConstants;
import eu.domibus.jms.activemq.InternalJMSManagerActiveMQ;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class JMSMessageUtil {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(JMSMessageUtil.class);

    @Autowired
    @Qualifier(DomibusJMSConstants.DOMIBUS_JMS_CONNECTION_FACTORY)
    private ConnectionFactory jmsConnectionFactory;


    public Message consumeMessageWithTimeout(javax.jms.Connection connection, String queueName, long mSecs) throws Exception {
        Session session = connection.createSession(true, Session.SESSION_TRANSACTED);
        Destination destination = session.createQueue(queueName);
        MessageConsumer consumer = session.createConsumer(destination);
        Message message = consumer.receive(mSecs);
        consumer.close();
        session.close();
        return message;
    }

    @Transactional
    public Message consumeMessage(String queue) throws Exception {
        javax.jms.Connection connection = jmsConnectionFactory.createConnection("domibus", "changeit");
        connection.start();
        Message message = consumeMessageWithTimeout(connection, queue, 1000);
        connection.close();
        return message;
    }

    @Transactional
    public void consumeMessagesSafely(String queue, int numberOfMessages) {
        LOG.info("Consuming [{}] messages from queue [{}] if any", queue, numberOfMessages);
        for (int index = 0; index < numberOfMessages; index++) {
            try {
                consumeMessages(queue, numberOfMessages);
            } catch (Exception e) {
                LOG.debug("Finished consuming messages count [{}]", numberOfMessages);
            }
        }
    }

    public List<Message> consumeMessages(String queue, int numberOfMessages) throws Exception {
        List<Message> result = new ArrayList<>();

        javax.jms.Connection connection = jmsConnectionFactory.createConnection("domibus", "changeit");
        connection.start();
        for (int index = 0; index < numberOfMessages; index++) {
            Message message = consumeMessageWithTimeout(connection, queue, 1000);
            if (message == null) {
                throw new RuntimeException("Expected JMS message but got null for message with index [" + index + "](index starts with 0)");
            }
            result.add(message);
        }
        connection.close();

        return result;
    }
}
