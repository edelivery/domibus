package eu.domibus.test.common;

public class SoapAttachmentTest {

    protected byte[] attachment;
    protected String contentId;

    public SoapAttachmentTest(byte[] attachment, String contentId) {
        this.attachment = attachment;
        this.contentId = contentId;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public String getContentId() {
        return contentId;
    }
}
