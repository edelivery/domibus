import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {DomibusInfo} from './domibusinfo';
import {SupportTeamInfo} from '../../security/not-authorized/supportteaminfo';

@Injectable()
export class DomibusInfoService {

  private isExtAuthProviderEnabledPromise: Promise<boolean>;
  private domibusInfo: Promise<DomibusInfo>;
  private supportTeamInfo: Promise<SupportTeamInfo>;
  private publicAppUrl = 'rest/public/application';
  private privateAppUrl = 'rest/internal/user/application';

  constructor(private http: HttpClient) {
  }

  getDomibusInfo(): Promise<DomibusInfo> {
    if (!this.domibusInfo) {
      this.domibusInfo = this.http.get<DomibusInfo>(this.privateAppUrl + '/info').toPromise();
      this.domibusInfo.catch(() => {
        this.domibusInfo = null; // if there is an error, reset the promise so that it can be fetched again
      });
    }
    return this.domibusInfo;
  }

  isExtAuthProviderEnabled(): Promise<boolean> {
    if (!this.isExtAuthProviderEnabledPromise) {
      this.isExtAuthProviderEnabledPromise = this.http.get<boolean>(this.publicAppUrl + '/extauthproviderenabled').toPromise();
    }
    return this.isExtAuthProviderEnabledPromise;
  }

  getSupportTeamInfo(): Promise<SupportTeamInfo> {
    if (!this.supportTeamInfo) {
      this.supportTeamInfo = this.http.get<SupportTeamInfo>(this.publicAppUrl + '/supportteam').toPromise();
    }
    return this.supportTeamInfo;
  }
}
