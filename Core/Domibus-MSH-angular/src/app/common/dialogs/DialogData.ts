export interface YesNoDialogData {
  title: string,
  yesText: string,
  yesIcon: string,
  noText: string,
  noIcon: string,
}

export interface OKDialogData {
  message?: any;
  title: string,
  okText: string,
  okIcon: string,
}
