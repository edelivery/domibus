import {Component, EventEmitter, Input, OnInit} from "@angular/core";
import {DomainService} from "../../security/domain.service";
import {Domain} from "../../security/domain";

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements OnInit {

  isMultiDomain: boolean;
  currentDomain: string;

  constructor(private domainService : DomainService) {
  }

  async ngOnInit() {
      this.isMultiDomain = await this.domainService.isMultiDomain() ;
      if (this.isMultiDomain) {
        this.domainService.getCurrentDomain().subscribe((domain: Domain) => this.currentDomain = domain ? domain.name : '');
      }
  }

}
