import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {AlertService} from './alert/alert.service';
import {HttpResponse} from '@angular/common/http';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) {
  }

  handleError(error: HttpResponse<any> | any) {
    console.error('global error handler processing error: ');
    console.error(error);
    if (error.rejection) {
      // unpack the promise rejection
      error = error.rejection;
    }
    const alertService = this.injector.get(AlertService);
    alertService.exception('', error);
  }

}
