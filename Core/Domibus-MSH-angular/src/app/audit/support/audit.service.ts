import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import 'rxjs-compat/add/operator/mergeMap';
import 'rxjs-compat/add/observable/from';

/**
 * @author Thomas Dussart
 * @since 4.0
 *
 * In charge of retrieving audit information from the backend.
 */
@Injectable()
export class AuditService {
  static readonly BASE_URL = 'rest/internal/admin/audit';

  constructor(private http: HttpClient) {
  }

  countAuditLogs(searchParams: HttpParams): Observable<number> {
    return this.http.get<number>(AuditService.BASE_URL + '/count', {params: searchParams});
  }

  listTargetTypes(): Observable<string[]> {
    return this.http.get<string[]>(AuditService.BASE_URL + '/targets');
  }

  listActions(): Observable<string> {
    return Observable.from(['Created', 'Modified', 'Deleted', 'Downloaded', 'Resent', 'Moved']);
  }

}
