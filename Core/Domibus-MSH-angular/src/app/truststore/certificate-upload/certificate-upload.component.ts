import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TrustStoreService} from '../support/trustore.service';
import {AbstractControl, NgControl, NgForm, UntypedFormBuilder} from '@angular/forms';

@Component({
  selector: 'app-certificate-upload',
  templateUrl: './certificate-upload.component.html',
  styleUrls: ['./certificate-upload.component.css'],
  providers: [TrustStoreService]
})
export class CertificateUploadComponent implements OnInit {

  @ViewChild('truststoreForm') truststoreForm: NgForm;
  selectedFileName: string;
  fileSelected = false;
  @ViewChild('fileInput') fileInput;
  securityProfiles: string[];
  certificatePurposes: string[];
  // legacy = true is called "No security profile" in UI
  legacy: boolean;
  legacyVisible: boolean;
  securityProfile: string;
  certificatePurpose: string;
  alias: string;
  partyName: string;

  constructor(public dialogRef: MatDialogRef<CertificateUploadComponent>,
              private fb: UntypedFormBuilder, @Inject(MAT_DIALOG_DATA) public data: any,
              private trustStoreService: TrustStoreService) {
    console.log('data=', data)
    this.legacy = data.legacy;
    this.legacyVisible = data.legacyVisible;
  }

  async ngOnInit() {
    this.securityProfiles = await this.trustStoreService.getSecurityProfiles();
    this.certificatePurposes = await this.trustStoreService.getCertificatePurposes();
  }

  public isFormValid(): boolean {
    return this.truststoreForm && this.truststoreForm.valid && this.fileSelected;
  }

  public async submit() {
    if (!this.isFormValid()) {
      return;
    }
    const fileToUpload = this.fileInput.nativeElement.files[0];
    let result;
    if (this.legacy) {
      result = {
        file: fileToUpload,
        alias: this.alias,
      };
    } else {
      result = {
        file: fileToUpload,
        partyName: this.partyName,
        securityProfile: this.securityProfile,
        certificatePurpose: this.certificatePurpose
      };
    }
    result.legacy = this.legacy;
    this.dialogRef.close(result);
  }

  selectFile() {
    const fi = this.fileInput.nativeElement;
    const file = fi.files[0];
    this.selectedFileName = file.name;

    this.fileSelected = fi.files.length != 0;
  }

  public shouldShowErrors(field: NgControl | NgForm | AbstractControl): boolean {
    return (field.touched || field.dirty) && !!field.errors;
  }
}
