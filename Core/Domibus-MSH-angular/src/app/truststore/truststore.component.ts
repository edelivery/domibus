import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {TrustStoreService} from './support/trustore.service';
import {AlertService} from '../common/alert/alert.service';
import {HttpClient} from '@angular/common/http';
import {ApplicationContextService} from '../common/application-context.service';
import {ComponentName} from '../common/component-name-decorator';
import {BaseTruststoreComponent} from './base-truststore.component';
import {FileUploadValidatorService} from '../common/file-upload-validator.service';
import {DialogsService} from '../common/dialogs/dialogs.service';

@Component({
  selector: 'app-truststore',
  templateUrl: './base-truststore.component.html',
  providers: [TrustStoreService]
})
@ComponentName('TrustStore')
export class TruststoreComponent extends BaseTruststoreComponent implements OnInit {
  protected LEGACY_ADD_CERTIFICATE_URL: string;

  constructor(applicationService: ApplicationContextService, http: HttpClient, trustStoreService: TrustStoreService,
              alertService: AlertService, changeDetector: ChangeDetectorRef,
              fileUploadValidatorService: FileUploadValidatorService, dialogsService: DialogsService) {
    super(applicationService, http, trustStoreService, alertService, changeDetector, fileUploadValidatorService, trustStoreService, dialogsService);

    this.BASE_URL = 'rest/internal/admin/truststore';
    this.CSV_URL = this.BASE_URL + '/csv';
    this.DOWNLOAD_URL = this.BASE_URL + '/download';
    this.UPLOAD_URL = this.BASE_URL + '/save';
    this.LIST_ENTRIES_URL = this.BASE_URL + '/list';
    this.ADD_CERTIFICATE_URL = this.BASE_URL + '/certificates';
    this.LEGACY_ADD_CERTIFICATE_URL = this.BASE_URL + '/entries';
    this.REMOVE_CERTIFICATE_URL = this.BASE_URL + '/entries/alias';

    this.showResetOperation = true;
    this.canHandleCertificates = true;
  }

  ngOnInit() {
    super.ngOnInit();

    this.checkModifiedOnDisk();
  }

  protected getAddCertificateUrl(params: Object) {
    console.log('getAddCertificateUrl params=', params)
    if (params && params['legacy']) {
      return this.LEGACY_ADD_CERTIFICATE_URL;
    }
    return this.ADD_CERTIFICATE_URL;
  }

  protected getUploadDialogParams() {
    return {legacy: true, legacyVisible: true, title: 'Upload truststore', description: ''};
  }
}
