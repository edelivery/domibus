import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpClient, HttpParams} from '@angular/common/http';
import {CustomURLEncoder} from '../../common/custom-url-encoder';
import {MessageLogComponent} from '../messagelog.component';
import {MessageLogResult} from '../support/messagelogresult';

@Component({
  selector: 'app-messagelog-details',
  templateUrl: './messagelog-details.component.html'
})
export class MessagelogDetailsComponent implements OnInit {

  message;

  constructor(public dialogRef: MatDialogRef<MessagelogDetailsComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
              private http: HttpClient) {
    this.message = data.message;
  }

  async ngOnInit() {
    if (!this.data.fetchData) {
      return;
    }
    let filterParams = new HttpParams({encoder: new CustomURLEncoder()})
        .append('messageType', this.message['messageType'])
        .append('messageId', this.message['messageId'])
        .append('mshRole', this.message['mshRole'])
        .append('applyDefaultFilters', 'false')
    this.data.fields.map(field => !!field).forEach(field => filterParams.append('fields', field));

    const res = <MessageLogResult>await this.http.get<any>(MessageLogComponent.MESSAGE_LOG_URL, {params: filterParams}).toPromise();
    if (res && res.count == 1) {
      console.log('res.messageLogEntries[0]=', res.messageLogEntries[0])
      this.message = res.messageLogEntries[0];
    }
  }
}
