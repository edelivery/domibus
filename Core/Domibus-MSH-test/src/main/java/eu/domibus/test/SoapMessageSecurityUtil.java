package eu.domibus.test;

import eu.domibus.api.ebms3.model.Ebms3MessagingWithSecurityProfileMetadata;
import eu.domibus.api.security.SecurityProfileMetadata;
import eu.domibus.core.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import static eu.domibus.core.ebms3.sender.client.DispatchClientDefaultProvider.MESSAGING_WITH_SECURITY_PROFILE_METADATA;

@Service
public class SoapMessageSecurityUtil {

    @Autowired
    MessageUtil messageUtil;

    public void setEbms3MessagingWithSecurityProfileMetadata(SOAPMessage soapMessage) throws SOAPException {
        final Ebms3MessagingWithSecurityProfileMetadata messagingWithSecurityProfileMetadata = messageUtil.getMessagingWithSecurityProfileMetadata(soapMessage);
        SecurityProfileMetadata securityProfileMetadata = new SecurityProfileMetadata();
        securityProfileMetadata.setSignatureAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
        securityProfileMetadata.setEncryptionKeyTransportAlgorithm("http://www.w3.org/2009/xmlenc11#rsa-oaep");
        securityProfileMetadata.setEncryptionMGFAlgorithm("http://www.w3.org/2009/xmlenc11#mgf1sha256");
        securityProfileMetadata.setEncryptionDigestAlgorithm("http://www.w3.org/2001/04/xmlenc#sha256");
        messagingWithSecurityProfileMetadata.setSecurityProfileMetadata(securityProfileMetadata);
        soapMessage.setProperty(MESSAGING_WITH_SECURITY_PROFILE_METADATA, messagingWithSecurityProfileMetadata);
    }
}
