package eu.domibus.test;

import eu.domibus.common.model.configuration.Configuration;
import eu.domibus.common.model.configuration.ConfigurationRaw;
import eu.domibus.common.model.configuration.Party;
import eu.domibus.core.party.DynamicPartyDao;
import eu.domibus.core.pmode.ConfigurationDAO;
import eu.domibus.core.pmode.ConfigurationRawDAO;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.spring.lock.LockDao;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.XmlProcessingException;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
public class PModeTestService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PModeTestService.class);

    @Autowired
    protected PModeProvider pModeProvider;

    @Autowired
    protected ConfigurationDAO configurationDAO;

    @Autowired
    LockDao lockDao;

    @Autowired
    DynamicPartyDao dynamicPartyDao;

    @Autowired
    protected ConfigurationRawDAO configurationRawDAO;

    @Transactional
    public void uploadPMode(Integer redHttpPort, String pModeFilepath, Map<String, String> toReplace) throws IOException, XmlProcessingException {
        deleteCurrentPMode();

        final InputStream inputStream = new ClassPathResource(pModeFilepath).getInputStream();

        String pmodeText = IOUtils.toString(inputStream, UTF_8);
        if (toReplace != null) {
            pmodeText = replace(pmodeText, toReplace);
        }
        if (redHttpPort != null) {
            LOG.info("Using wiremock http port [{}]", redHttpPort);
            pmodeText = pmodeText.replace(String.valueOf(AbstractIT.SERVICE_PORT), String.valueOf(redHttpPort));
        }

        byte[] bytes = pmodeText.getBytes(UTF_8);
        final Configuration pModeConfiguration = pModeProvider.getPModeConfiguration(bytes);
        configurationDAO.updateConfiguration(pModeConfiguration);
        final ConfigurationRaw configurationRaw = new ConfigurationRaw();
        configurationRaw.setConfigurationDate(Calendar.getInstance().getTime());
        configurationRaw.setXml(bytes);
        configurationRaw.setDescription("upload Pmode for testing on port: " + redHttpPort);
        configurationRawDAO.create(configurationRaw);
        configurationDAO.flush();

        pModeProvider.refresh();
    }

    @Transactional
    public void deleteCurrentPMode() {
        ConfigurationRaw currentRawConfiguration = null;
        Configuration currentConfiguration = null;

        try {
            currentConfiguration = configurationDAO.read();
            if (configurationDAO.configurationExists()) {
                configurationDAO.delete(currentConfiguration);
            }

            currentRawConfiguration = configurationRawDAO.getCurrentRawConfiguration();
            if (currentRawConfiguration != null) {
                configurationRawDAO.deleteById(currentRawConfiguration.getEntityId());
            }
        } catch (Exception ex) {
            LOG.info("Could not delete current pmode");
        }
    }

    protected String replace(String body, Map<String, String> toReplace) {
        for (String key : toReplace.keySet()) {
            body = body.replaceAll(key, toReplace.get(key));
        }
        return body;
    }
}
