package eu.domibus.test;

import eu.domibus.api.jms.JmsMessage;
import eu.domibus.api.model.*;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.security.AuthRole;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.user.plugin.AuthenticationEntity;
import eu.domibus.common.JPAConstants;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.common.model.configuration.Mpc;
import eu.domibus.common.model.configuration.ReceptionAwareness;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.ebms3.sender.MessageSenderListener;
import eu.domibus.core.ebms3.sender.ResponseResult;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.jms.JMSManagerImpl;
import eu.domibus.core.message.UserMessageDao;
import eu.domibus.core.message.UserMessageDefaultService;
import eu.domibus.core.message.UserMessageDefaultServiceHelper;
import eu.domibus.core.message.UserMessageLogDao;
import eu.domibus.core.message.dictionary.*;
import eu.domibus.core.message.reliability.ReliabilityChecker;
import eu.domibus.core.message.retention.UserMessageDeletionService;
import eu.domibus.core.multitenancy.dao.UserDomainDao;
import eu.domibus.core.plugin.handler.MessageSubmitterImpl;
import eu.domibus.core.pmode.multitenancy.MultiDomainPModeProvider;
import eu.domibus.core.pmode.provider.CachingPModeProvider;
import eu.domibus.core.user.UserPersistenceService;
import eu.domibus.core.user.plugin.AuthenticationDAO;
import eu.domibus.core.user.plugin.PluginUserServiceImpl;
import eu.domibus.core.user.ui.UserRoleDaoImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessageConstants;
import eu.domibus.messaging.MessagingProcessingException;
import eu.domibus.plugin.Submission;
import eu.domibus.plugin.notification.AsyncNotificationConfiguration;
import eu.domibus.test.common.*;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.commons.lang3.StringUtils;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.infinispan.commons.jdkspecific.CallerId.getCallerClass;

/**
 * @author François Gautier
 * @since 5.0
 */
@Service
public class ITTestsService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ITTestsService.class);
    public static final String JMS_MANAGER = "jmsManager";

    @PersistenceContext(unitName = JPAConstants.PERSISTENCE_UNIT_NAME)
    protected EntityManager em;

    @Autowired(required = false)
    @Lazy
    //Lazy loading of AsyncNotificationConfiguration to avoid the circular dependency triggered by loading the BackendConnectors(SPIs) at startup
    protected List<AsyncNotificationConfiguration> asyncNotificationConfigurations;


    @Autowired
    protected UserMessageDefaultServiceHelper userMessageDefaultServiceHelper;

    @Autowired
    PluginUserServiceImpl pluginUserService;

    @Autowired
    protected UserPersistenceService userPersistenceService;

    @Autowired
    protected UserMessageDefaultService userMessageDefaultService;

    @Autowired
    protected UserMessageDeletionService deletionService;
    @Autowired
    protected UserMessageLogDao userMessageLogDao;

    @Autowired
    protected SubmissionUtil submissionUtil;

    @Autowired
    SoapMessageSecurityUtil soapMessageSecurityUtil;

    @Autowired
    ReliabilityChecker reliabilityChecker;

    @Autowired
    protected MessageSubmitterImpl messageSubmitter;

    @Autowired
    protected MpcDao mpcDao;

    @Autowired
    protected MshRoleDao mshRoleDao;

    @Autowired
    protected PartyIdDao partyIdDao;

    @Autowired
    protected PartyRoleDao partyRoleDao;

    @Autowired
    protected AuthenticationDAO userDao;

    @Autowired
    protected DomibusTaskExecutor domainTaskExecutor;

    @Autowired
    protected ActionDao actionDao;

    @Autowired
    protected MSHDispatcher mshDispatcher;

    @Autowired
    protected ServiceDao serviceDao;

    @Autowired
    protected AgreementDao agreementDao;

    @Autowired
    protected UserMessageDao userMessageDao;

    @Autowired
    protected DomainContextProvider domainContextProvider;

    @Autowired
    protected MessagePropertyDao messagePropertyDao;

    @Autowired
    protected SoapSampleUtil soapSampleUtil;

    @Autowired
    protected MSHWebservice mshWebserviceTest;

    @Autowired
    protected UserRoleDaoImpl userRoleDao;

    @Autowired
    protected DomibusConfigurationService domibusConfigurationService;

    @Autowired
    protected MessageSenderListener messageSenderListener;

    @Autowired
    protected UserDomainDao userDomainDao;

    @Autowired
    MultiDomainPModeProvider pModeProvider;

    @Transactional
    public String sendMessageWithStatus(MessageStatus endStatus) throws MessagingProcessingException {
        UserMessageLog userMessageLog = sendMessageWithStatus(endStatus, null);
        return userMessageLog.getUserMessage().getMessageId();
    }

    @Transactional
    public UserMessageLog sendMessageWithStatus(MessageStatus endStatus, String messageId) throws MessagingProcessingException {

        Submission submission = submissionUtil.createSubmission(messageId);
        final String dbMessageId = messageSubmitter.submit(submission, BackendConnectorMock.BACKEND_CONNECTOR_NAME);

        final UserMessageLog userMessageLog = userMessageLogDao.findByMessageId(dbMessageId, MSHRole.SENDING);
        userMessageLogDao.setMessageStatus(userMessageLog, endStatus);

        return userMessageLog;
    }

    @Transactional
    public void changeMessageStatus(Long messageEntityId, MessageStatus messageStatus) {
        final UserMessageLog userMessageLog = userMessageLogDao.findByEntityId(messageEntityId);
        userMessageLogDao.setMessageStatus(userMessageLog, messageStatus);
    }

    @Transactional
    public UserMessage getUserMessage() {
        final MessageTestUtility messageTestUtility = new MessageTestUtility();
        final UserMessage userMessage = messageTestUtility.createSampleUserMessage();
        userMessage.setMshRole(mshRoleDao.findOrCreate(MSHRole.SENDING));

        PartyId senderPartyId = messageTestUtility.createSenderPartyId();
        userMessage.getPartyInfo().getFrom().setFromPartyId(partyIdDao.findOrCreateParty(senderPartyId.getValue(), senderPartyId.getType()));

        userMessage.getPartyInfo().getFrom().setFromRole(partyRoleDao.findOrCreateRole(messageTestUtility.createSenderPartyRole().getValue()));

        final PartyId receiverPartyId = messageTestUtility.createReceiverPartyId();
        userMessage.getPartyInfo().getTo().setToPartyId(partyIdDao.findOrCreateParty(receiverPartyId.getValue(), receiverPartyId.getType()));

        userMessage.getPartyInfo().getTo().setToRole(partyRoleDao.findOrCreateRole(messageTestUtility.createReceiverPartyRole().getValue()));

        userMessage.setAction(actionDao.findOrCreateAction(messageTestUtility.createActionEntity().getValue()));

        final ServiceEntity serviceEntity = messageTestUtility.createServiceEntity();
        userMessage.setService(serviceDao.findOrCreateService(serviceEntity.getValue(), serviceEntity.getType()));

        final AgreementRefEntity agreementRefEntity = messageTestUtility.createAgreementRefEntity();
        userMessage.setAgreementRef(agreementDao.findOrCreateAgreement(agreementRefEntity.getValue(), agreementRefEntity.getType()));

        userMessage.setMpc(mpcDao.findOrCreateMpc(messageTestUtility.createMpcEntity().getValue()));

        HashSet<MessageProperty> messageProperties = new HashSet<>();
        for (MessageProperty messageProperty : userMessage.getMessageProperties()) {
            messageProperties.add(messagePropertyDao.findOrCreateProperty(messageProperty.getName(), messageProperty.getValue(), messageProperty.getType()));
        }

        userMessage.setMessageProperties(messageProperties);

        userMessageDao.create(userMessage);
        return userMessage;
    }

    @Transactional
    public SOAPMessage receiveMessage(String filename, String messageId) throws SOAPException, IOException, ParserConfigurationException, SAXException {
        return receiveMessage(filename, messageId, false);
    }

    @Transactional
    public SOAPMessage receiveMessage(String filename, String messageId, boolean compression) throws SOAPException, IOException, ParserConfigurationException, SAXException {
        SOAPMessage soapMessage = soapSampleUtil.createSOAPMessage(filename, messageId, compression);
        soapMessageSecurityUtil.setEbms3MessagingWithSecurityProfileMetadata(soapMessage);
        mshWebserviceTest.invoke(soapMessage);
        return soapMessage;
    }

    @Transactional
    public SOAPMessage receiveMessage(String filename, String messageId, boolean compression, List<SoapAttachmentTest> attachments) throws SOAPException, IOException, ParserConfigurationException, SAXException {
        SOAPMessage soapMessage = soapSampleUtil.createSOAPMessage(filename, messageId, attachments, compression);
        soapMessageSecurityUtil.setEbms3MessagingWithSecurityProfileMetadata(soapMessage);
        mshWebserviceTest.invoke(soapMessage);
        return soapMessage;
    }


    @Transactional
    public SOAPMessage receiveMessage(String messageId) throws SOAPException, IOException, ParserConfigurationException, SAXException {
        String filename = "SOAPMessage2.xml";
        return receiveMessage(filename, messageId);
    }

    @Transactional
    public void deleteAllMessages(String... messageIds) {
        List<UserMessageLogDto> allMessages = new ArrayList<>();
        for (String messageId : messageIds) {
            if (StringUtils.isNotBlank(messageId)) {
                UserMessageLog byMessageId = userMessageLogDao.findByMessageId(messageId);
                if (byMessageId != null) {

                    UserMessageLogDto userMessageLogDto = new UserMessageLogDto(byMessageId.getUserMessage().getEntityId(), byMessageId.getUserMessage().getMessageId(), byMessageId.getBackend());
                    userMessageLogDto.setProperties(userMessageDefaultServiceHelper.getProperties(byMessageId.getUserMessage()));
                    allMessages.add(userMessageLogDto);
                } else {
                    LOG.warn("MessageId [{}] not found", messageId);
                }
            }
        }
        if (!allMessages.isEmpty()) {
            deletionService.deleteMessages(allMessages);
        }
    }

    public Object modifyJmsManagerToSubmitUserMessage() {
        //we save the JMS manager to restore it later
        Object savedJmsManager = getOriginalJmsManager();

        ReflectionTestUtils.setField(userMessageDefaultService, JMS_MANAGER, new JMSManagerImpl() {
            @Override
            public void sendMessageToQueue(JmsMessage message, Queue destination) {
                LOG.info("Jms override [{}] [{}]", message, destination);

                sendMessage("default", message.getStringProperty(MessageConstants.MESSAGE_ENTITY_ID), message.getStringProperty(MessageConstants.MESSAGE_ID));
                LOG.putMDC(DomibusLogger.MDC_DOMAIN, "default");
            }
        });
        return savedJmsManager;
    }

    public Object getOriginalJmsManager() {
        return ReflectionTestUtils.getField(userMessageDefaultService, JMS_MANAGER);
    }

    public void resetBackJmsManager(Object jmsManager) {
        ReflectionTestUtils.setField(userMessageDefaultService, JMS_MANAGER, jmsManager);
    }

    private void sendMessage(String domain, String messageEntityId, String messageId) {
        messageSenderListener.onMessage(getActiveMQTextMessage(domain, messageEntityId, messageId));
    }

    public ActiveMQTextMessage getActiveMQTextMessage(String domain, String messageEntityId, String messageId) {
        ActiveMQTextMessage activeMQTextMessage = new ActiveMQTextMessage();

        try {
            activeMQTextMessage.setStringProperty(MessageConstants.DOMAIN, domain);
            activeMQTextMessage.setStringProperty(MessageConstants.MESSAGE_ENTITY_ID, messageEntityId);
            activeMQTextMessage.setStringProperty(MessageConstants.MESSAGE_ID, messageId);
        } catch (JMSException e) {
            throw new IllegalStateException("Could not create getActiveMQTextMessage", e);
        }
        return activeMQTextMessage;
    }

    public void setJmsManager(Object jmsManager) {

        //put the real manager back
        ReflectionTestUtils.setField(userMessageDefaultService, JMS_MANAGER, jmsManager);
    }

    public Object prepareMocksForSendingMessage(String returnedAs4MessageFile,
                                                String userMessageId,
                                                ReliabilityChecker.CheckResult returnedCheckResult) throws SOAPException, IOException, ParserConfigurationException, SAXException, EbMS3Exception {
        LOG.info("Set Jms manager from [{}] ", getCallerClass(1));
        Object savedJmsManagerField = modifyJmsManagerToSubmitUserMessage();

        final SOAPMessage soapMessage = soapSampleUtil.createSOAPMessage(returnedAs4MessageFile, userMessageId);
        Mockito.when(mshDispatcher.dispatch(ArgumentMatchers.isA(SOAPMessage.class), ArgumentMatchers.anyString(), ArgumentMatchers.isA(SecurityProfileConfiguration.class), ArgumentMatchers.isA(LegConfiguration.class), ArgumentMatchers.anyString())).thenReturn(soapMessage);

        //reliability is OK
        Mockito.when(reliabilityChecker.check(ArgumentMatchers.isA(SOAPMessage.class), ArgumentMatchers.isA(SOAPMessage.class), ArgumentMatchers.isA(ResponseResult.class), ArgumentMatchers.isA(LegConfiguration.class))).thenReturn(returnedCheckResult);

        return savedJmsManagerField;
    }

    public Object prepareMocksForFailingSendingMessage(Exception submissionThrownException) throws EbMS3Exception {
        Object savedJmsManagerField = modifyJmsManagerToSubmitUserMessage();
        Mockito.when(mshDispatcher.dispatch(ArgumentMatchers.isA(SOAPMessage.class), ArgumentMatchers.anyString(), ArgumentMatchers.isA(SecurityProfileConfiguration.class), ArgumentMatchers.isA(LegConfiguration.class), ArgumentMatchers.anyString())).thenThrow(submissionThrownException);
        return savedJmsManagerField;
    }

    public void updatePmodeWithZeroRetries() {
        //we modify the retry policy so that the message fails immediately and not goes in WAITING_FOR_RETRY
        final CachingPModeProvider currentPModeProvider = (CachingPModeProvider) pModeProvider.getCurrentPModeProvider();
        final ReceptionAwareness receptionAwareness = currentPModeProvider.getConfiguration().getBusinessProcesses().getAs4ConfigReceptionAwareness().stream().findFirst().orElse(null);
        if (receptionAwareness != null) {
            receptionAwareness.setRetryCount(1);
            receptionAwareness.setRetryTimeout(0);
        }
    }

    public void modifyPmodeRetryParameters(int retryTimeout, int retryCount) {
        //we modify the retry policy so that the message fails immediately and not goes in WAITING_FOR_RETRY
        final CachingPModeProvider currentPModeProvider = (CachingPModeProvider) pModeProvider.getCurrentPModeProvider();
        final ReceptionAwareness receptionAwareness = currentPModeProvider.getConfiguration().getBusinessProcesses().getAs4ConfigReceptionAwareness().stream().findFirst().orElse(null);
        if (receptionAwareness != null) {
            receptionAwareness.setRetryCount(retryTimeout);
            receptionAwareness.setRetryTimeout(retryCount);
        }
    }

    public void modifyPmodeRetentionParameters(Integer retentionDownloaded, Integer retentionUnDownloaded) {
        //we modify the retry policy so that the message fails immediately and not goes in WAITING_FOR_RETRY
        final CachingPModeProvider currentPModeProvider = (CachingPModeProvider) pModeProvider.getCurrentPModeProvider();
        final Mpc mpc = currentPModeProvider.getConfiguration().getMpcs().iterator().next();
        mpc.setRetentionUndownloaded(retentionUnDownloaded);
        mpc.setRetentionDownloaded(retentionDownloaded);
    }

    /**
     * Sets the connector to null for all async notifications; this is useful in tests to force delivering the messages from Domibus to the plugins in a synchronous way
     */
    public void clearConnectorFromAsyncNotification() {
        asyncNotificationConfigurations.forEach(asyncNotificationConfiguration -> ReflectionTestUtils.setField(asyncNotificationConfiguration, "backendConnector", null));
    }

    public AuthenticationEntity createPluginUser(AuthRole authRole, String userName, String password) {
        AuthenticationEntity user = new AuthenticationEntity();
        user.setUserName(userName);
        user.setPassword(password);
        user.setAuthRoles(authRole.name());
        user.setActive(true);

        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        pluginUserService.insertNewUser(user, currentDomain);

        return user;
    }
}
