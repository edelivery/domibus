package eu.domibus.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.domibus.api.datasource.DataSourceConstants;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.*;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.proxy.DomibusProxyService;
import eu.domibus.api.security.AuthRole;
import eu.domibus.api.user.User;
import eu.domibus.api.user.UserState;
import eu.domibus.common.JPAConstants;
import eu.domibus.core.crypto.TruststoreDao;
import eu.domibus.core.crypto.TruststoreEntity;
import eu.domibus.core.message.UserMessageDao;
import eu.domibus.core.message.UserMessageLogDao;
import eu.domibus.core.plugin.BackendConnectorProvider;
import eu.domibus.core.pmode.ConfigurationDAO;
import eu.domibus.core.pmode.ConfigurationRawDAO;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.spring.DomibusApplicationContextListener;
import eu.domibus.core.spring.DomibusRootConfiguration;
import eu.domibus.core.spring.lock.LockDao;
import eu.domibus.core.user.multitenancy.SuperUserManagementServiceImpl;
import eu.domibus.core.user.ui.UserManagementServiceImpl;
import eu.domibus.core.user.ui.UserRoleDao;
import eu.domibus.core.util.WarningUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.BackendConnector;
import eu.domibus.test.common.BackendConnectorMock;
import eu.domibus.test.common.DomibusMTTestDatasourceConfiguration;
import eu.domibus.web.spring.DomibusWebConfiguration;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.retry.RecoveryCallback;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.SocketUtils;
import org.w3c.dom.Document;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static eu.domibus.test.common.DomibusMTTestDatasourceConfiguration.ALL_DOMAINS;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.awaitility.Awaitility.await;

/**
 * Created by feriaad on 02/02/2016.
 */
@EnableMethodSecurity
@WebAppConfiguration
@ExtendWith(SpringExtension.class)
@ContextConfiguration(initializers = PropertyOverrideContextInitializer.class,
        classes = {DomibusRootConfiguration.class, DomibusWebConfiguration.class,
                DomibusMTTestDatasourceConfiguration.class, DomibusTestMocksConfiguration.class})
@TestPropertySource(properties = {"domibus.deployment.clustered=true"})
public abstract class AbstractIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AbstractIT.class);

    public static final int SERVICE_PORT = 8892;

    public static final String TEST_ADMIN_USERNAME = "admin";

    public static final String TEST_ADMIN_PASSWORD = "123456";

    public static final String TEST_ROLE_USER_USERNAME = "user";
    public static final String TEST_ROLE_USER_PASSWORD = "123456";

    public static final String TEST_SUPER_USERNAME = "super";
    public static final String TEST_SUPER_PASSWORD = "123456";
    public static final String DOMIBUS_CONFIG_LOCATION = "target/test-classes";
    public static final String RETRY_STRING = "Retry [{}] [{}]";

    public final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    protected ITTestsService itTestsService;

    @Autowired
    protected SuperUserManagementServiceImpl superUserManagementService;

    @Autowired
    protected DomibusConditionUtil domibusConditionUtil;

    @Autowired
    protected UserManagementServiceImpl userManagementService;

    @Autowired
    protected UserMessageDao userMessageDao;

    @Autowired
    protected DomibusApplicationContextListener domibusApplicationContextListener;

    @Autowired
    protected UserMessageLogDao userMessageLogDao;

    @Autowired
    protected PModeProvider pModeProvider;

    @Autowired
    protected ConfigurationDAO configurationDAO;

    @Autowired
    LockDao lockDao;

    @Autowired
    @Qualifier(DataSourceConstants.DOMIBUS_JDBC_DATA_SOURCE)
    DataSource dataSource;

    @Autowired
    protected ConfigurationRawDAO configurationRawDAO;

    @Autowired
    protected PModeTestService pModeTestService;

    @Autowired
    protected DomainContextProvider domainContextProvider;

    @Autowired
    protected DomibusProxyService domibusProxyService;

    @Autowired
    protected TruststoreDao truststoreDao;

    @Autowired
    protected UserRoleDao userRoleDao;

    @Autowired
    protected DomainService domainService;

    @Autowired
    protected DomibusTaskExecutor domainTaskExecutor;

    @Autowired
    protected BackendConnectorProvider backendConnectorProvider;

    @Autowired
    public DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    protected ObjectProvider<BackendConnectorMock> backendConnectorMocksProvider;

    @PersistenceContext(unitName = JPAConstants.PERSISTENCE_UNIT_NAME)
    protected EntityManager em;

    public static boolean springContextInitialized = false;

    @BeforeAll
    public static void init() throws IOException {
        springContextInitialized = false;
        LOG.info(WarningUtil.warnOutput("Initializing Spring context"));

        FileUtils.deleteDirectory(new File("target/temp"));
        final File domibusConfigLocation = new File(DOMIBUS_CONFIG_LOCATION);
        String absolutePath = domibusConfigLocation.getAbsolutePath();
        System.setProperty("domibus.config.location", absolutePath);

        RetryTemplate retryTemplate = retryTemplate();
        copyActiveMQFile(domibusConfigLocation);

        //Retry is performed to avoid failures when running on windows
        retryTemplate.execute(retryContext -> {
            copyKeystores(domibusConfigLocation);
            return null;
        }, (RecoveryCallback<Void>) retryContext -> {
            LOG.warn(RETRY_STRING, "copyKeystores", retryContext.getRetryCount(), retryContext.getLastThrowable());
            return null;
        });

        retryTemplate.execute(retryContext -> {
            copyPolicies(domibusConfigLocation);
            return null;
        }, (RecoveryCallback<Void>) retryContext -> {
            LOG.warn(RETRY_STRING, "copyPolicies", retryContext.getRetryCount(), retryContext.getLastThrowable());
            return null;
        });

        retryTemplate.execute(retryContext -> {
            copyDomibusProperties(domibusConfigLocation);
            return null;
        }, (RecoveryCallback<Void>) retryContext -> {
            LOG.warn(RETRY_STRING, "copyDomibusProperties", retryContext.getRetryCount(), retryContext.getLastThrowable());
            return null;
        });

        retryTemplate.execute(retryContext -> {
            copyDomainResourceFolders(domibusConfigLocation);
            return null;
        }, (RecoveryCallback<Void>) retryContext -> {
            LOG.warn(RETRY_STRING, "copyDomainResourceFolders", retryContext.getRetryCount(), retryContext.getLastThrowable());
            return null;
        });

        //we are using randomly available port in order to allow run in parallel
        String activeMQBrokerName = "localhost" + SocketUtils.findAvailableTcpPort(61616, 62690);
        System.setProperty(DomibusPropertyMetadataManagerSPI.ACTIVE_MQ_TRANSPORT_CONNECTOR_URI, "vm://" + activeMQBrokerName + "?broker.persistent=false&create=false");
        System.setProperty(DomibusPropertyMetadataManagerSPI.ACTIVE_MQ_BROKER_NAME, activeMQBrokerName);
        LOG.info("activeMQBrokerName=[{}]", activeMQBrokerName);
    }

    public static RetryTemplate retryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();

        FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
        fixedBackOffPolicy.setBackOffPeriod(2000L);
        retryTemplate.setBackOffPolicy(fixedBackOffPolicy);

        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(10);
        retryTemplate.setRetryPolicy(retryPolicy);

        return retryTemplate;
    }

    @BeforeEach
    public void initInstance() {
        setAuth();

        if (!springContextInitialized) {
            //the code below must run for general schema(without a domain)
            domainContextProvider.clearCurrentDomain();

            try {

                final List<Domain> domains = ALL_DOMAINS;
                for (Domain domain : domains) {
                    waitUntilDatabaseIsInitialized(() ->
                            domainTaskExecutor.submitCallable(databaseIsInitialized(), domain, null, ExecutorWaitPolicy.WAIT, 3L, TimeUnit.MINUTES, null));
                }
                final String generalSchema = domibusPropertyProvider.getProperty(DomainService.GENERAL_SCHEMA_PROPERTY);
                waitUntilDatabaseIsInitialized(() -> domainTaskExecutor.submitCallable(databaseIsInitialized(), new Domain(generalSchema, generalSchema), null, ExecutorWaitPolicy.WAIT, 3L, TimeUnit.MINUTES, null));
                LOG.info("Finished waiting for database initialization");

                LOG.info("Executing the ApplicationContextListener initialization");
                //needed to avoid an error in eu.domibus.core.plugin.routing.BackendFilterInitializerService.updateMessageFilters
                final List<BackendConnector<?, ?>> backendConnectors = getBackendConnectors();
                Mockito.when(backendConnectorProvider.getBackendConnectors()).thenReturn(backendConnectors);

                domibusApplicationContextListener.initializeForTests();

                createSuperUser();
            } catch (Exception ex) {
                LOG.warn("Domibus Application Context initialization failed", ex);
            } finally {
                springContextInitialized = true;
                domibusPropertyProvider.setProperty("domibus.deployment.clustered", "false");
            }
        }
        //set the default domain as the current domain
        domainContextProvider.setCurrentDomain(DomainService.DEFAULT_DOMAIN);
    }

    protected abstract List<BackendConnector<?, ?>> getBackendConnectors();

    protected static void copyActiveMQFile(File domibusConfigLocation) throws IOException {
        DomibusResourcesUtil.copyActiveMQFile(domibusConfigLocation);
    }

    protected static void copyKeystores(File domibusConfigLocation) throws IOException {
        LOG.info("Copying keystores");
        DomibusResourcesUtil.copyResourceToDisk(domibusConfigLocation, "abstractIT/keystores", "keystores");
    }

    protected static void copyPolicies(File domibusConfigLocation) throws IOException {
        LOG.info("Copying policies");
        DomibusResourcesUtil.copyResourceToDisk(domibusConfigLocation, "abstractIT/policies", "policies");
    }

    protected static void copyDomibusProperties(File domibusConfigLocation) throws IOException {
        DomibusResourcesUtil.copyDomibusProperties(domibusConfigLocation);
    }

    protected static void copyDomainResourceFolders(File domibusConfigLocation) throws IOException {
        final String[] folders = {"domains",
                "domains/default", "domains/default/encrypt", "domains/default/keystores",
                "domains/red", "domains/red/encrypt", "domains/red/keystores"};

        for (String folder : folders) {
            DomibusResourcesUtil.copyResourceToDisk(domibusConfigLocation, "abstractIT/" + folder, folder);
        }
    }

    protected void createSuperUser() {
        final List<User> usersWithFilters = superUserManagementService.findUsersWithFilters(AuthRole.ROLE_AP_ADMIN, TEST_SUPER_USERNAME, null, 0, 10);
        if (!usersWithFilters.isEmpty()) {
            return;
        }

        final ArrayList<User> superUsers = new ArrayList<>();
        final User superUser = new User();
        superUser.setStatus(UserState.NEW.name());
        superUser.setUserName(TEST_SUPER_USERNAME);
        superUser.setPassword(TEST_SUPER_PASSWORD);
        superUser.setActive(true);
        superUser.setAuthorities(Arrays.asList(AuthRole.ROLE_AP_ADMIN.name()));
        superUsers.add(superUser);
        superUserManagementService.updateUsers(superUsers);
    }

    protected void createUser() {
        final List<User> usersWithFilters = userManagementService.findUsersWithFilters(AuthRole.ROLE_USER, TEST_ROLE_USER_USERNAME, null, 0, 10);
        if (!usersWithFilters.isEmpty()) {
            return;
        }

        final ArrayList<User> superUsers = new ArrayList<>();
        final User superUser = new User();
        superUser.setStatus(UserState.NEW.name());
        superUser.setUserName(TEST_ROLE_USER_USERNAME);
        superUser.setPassword(TEST_ROLE_USER_PASSWORD);
        superUser.setActive(true);
        superUser.setAuthorities(Arrays.asList(AuthRole.ROLE_USER.name()));
        superUsers.add(superUser);
        userManagementService.updateUsers(superUsers);
    }


    protected void setGlobalProperty(String name, String value) {
        final Domain currentDomainSafely = domainContextProvider.getCurrentDomainSafely();
        domainContextProvider.clearCurrentDomain();
        domibusPropertyProvider.setProperty(name, value);

        domainContextProvider.setCurrentDomain(currentDomainSafely);
    }

    protected void setClusteredProperty(boolean value) {
        setGlobalProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DEPLOYMENT_CLUSTERED, value + "");
    }

    protected void setAuth() {
        SecurityContextHolder.getContext()
                .setAuthentication(new UsernamePasswordAuthenticationToken(
                        "test_user",
                        "test_password",
                        Collections.singleton(new SimpleGrantedAuthority(AuthRole.ROLE_ADMIN.name()))));
    }

    protected void authWithSuper() {
        SecurityContextHolder.getContext()
                .setAuthentication(new UsernamePasswordAuthenticationToken(
                        "super",
                        "123456",
                        Collections.singleton(new SimpleGrantedAuthority(AuthRole.ROLE_AP_ADMIN.name()))));
    }

    protected void uploadPMode(Integer redHttpPort) throws IOException, XmlProcessingException {
        uploadPMode(redHttpPort, null);
    }

    protected void uploadPMode(Integer redHttpPort, Map<String, String> toReplace) throws IOException, XmlProcessingException {
        uploadPMode(redHttpPort, "dataset/pmode/PModeTemplate.xml", toReplace);
    }

    protected void uploadPMode(Integer redHttpPort, String pModeFilepath, Map<String, String> toReplace) throws IOException, XmlProcessingException {
        pModeTestService.uploadPMode(redHttpPort, pModeFilepath, toReplace);
    }

    public void deleteCurrentPMode() {
        pModeTestService.deleteCurrentPMode();
    }

    protected void uploadPMode() throws IOException, XmlProcessingException {
        uploadPMode(null);
    }

    protected UserMessage getUserMessageTemplate() throws IOException {
        Resource userMessageTemplate = new ClassPathResource("dataset/messages/UserMessageTemplate.json");
        String jsonStr = new String(IOUtils.toByteArray(userMessageTemplate.getInputStream()), UTF_8);
        return new ObjectMapper().readValue(jsonStr, UserMessage.class);
    }


    protected void waitUntilDatabaseIsInitialized(Callable<Boolean> conditionEvaluator) {
        await().atMost(1, TimeUnit.MINUTES).pollInterval(500, TimeUnit.MILLISECONDS).until(conditionEvaluator);
    }

    protected Callable<Boolean> databaseIsInitialized() {
        return () -> {
            try {
                final int size = lockDao.findAll().size();
                return size > 0;
            } catch (Exception e) {
                LOG.warn("Could not get the lock entries");
                return false;
            }
        };
    }

    /**
     * Convert the given file to a string
     */
    protected String getAS4Response(String file) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputStream is = getClass().getClassLoader().getResourceAsStream("dataset/as4/" + file);
            Document doc = db.parse(is);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            return writer.getBuffer().toString().replaceAll("\n|\r", "");
        } catch (Exception exc) {
            Assertions.fail(exc.getMessage());
            LOG.error("Exception occurred: ", exc);
        }
        return null;
    }

    public void prepareSendMessage(String responseFileName) {
        prepareSendMessage(responseFileName, null);
    }

    public void prepareSendMessage(String responseFileName, Map<String, String> toReplace) {
        String body = getAS4Response(responseFileName);
        if (toReplace != null) {
            body = replace(body, toReplace);
        }

        // Mock the response from the recipient MSH
        stubFor(post(urlEqualTo("/domibus/services/msh"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/soap+xml")
                        .withBody(body)));
    }

    protected String replace(String body, Map<String, String> toReplace) {
        for (String key : toReplace.keySet()) {
            body = body.replaceAll(key, toReplace.get(key));
        }
        return body;
    }

    protected void createStore(String storeName, String filePath) {
        if (truststoreDao.existsWithName(storeName)) {
            LOG.info("truststore already created");
            return;
        }
        LOG.info("create truststore [{}]", storeName);
        try {
            TruststoreEntity domibusTruststoreEntity = new TruststoreEntity();
            domibusTruststoreEntity.setName(storeName);
            domibusTruststoreEntity.setType("JKS");
            domibusTruststoreEntity.setPassword("test123");
            try (InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(filePath)) {
                if (resourceAsStream == null) {
                    throw new IllegalStateException("File not found :" + filePath);
                }
                byte[] trustStoreBytes = IOUtils.toByteArray(resourceAsStream);
                domibusTruststoreEntity.setContent(trustStoreBytes);
                truststoreDao.create(domibusTruststoreEntity);
            }
        } catch (Exception ex) {
            LOG.info("Error creating store entity [{}]", storeName, ex);
        }
    }

    public static MockMultipartFile getMultiPartFile(String originalFilename, InputStream resourceAsStream) throws IOException {
        return new MockMultipartFile("file", originalFilename, "octetstream", IOUtils.toByteArray(resourceAsStream));
    }

    public String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public void deleteAllMessages(String... messageIds) {
        itTestsService.deleteAllMessages(messageIds);
    }

    protected void deleteAllMessages() {
        final List<UserMessage> userMessages = userMessageDao.findAll();
        if (CollectionUtils.isEmpty(userMessages)) {
            return;
        }
        deleteAllMessages(userMessages.stream().map(UserMessage::getMessageId).toArray(String[]::new));
    }

    protected byte[] getKeystoreContentForDomainFromClasspath(String resourceName, Domain domain) throws IOException {
        String fullClasspath = "domains/" + domain.getCode() + "/keystores/" + resourceName;
        return getResourceFromClasspath(fullClasspath);
    }

    protected byte[] getResourceFromClasspath(String resourceClasspathLocation) throws IOException {
        final InputStream contentInputStream = this.getClass().getClassLoader().getResourceAsStream(resourceClasspathLocation);
        if (contentInputStream == null) {
            throw new IOException("Could not get resource from classpath [" + resourceClasspathLocation + "]");
        }
        return IOUtils.toByteArray(contentInputStream);
    }
}
