package eu.domibus.test;

import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.message.reliability.ReliabilityChecker;
import eu.domibus.core.plugin.BackendConnectorProvider;
import eu.domibus.ext.web.interceptor.CleanMDCKeysInterceptor;
import eu.domibus.plugin.notification.PluginAsyncNotificationConfiguration;
import eu.domibus.web.converter.CustomMappingJackson2HttpMessageConverter;
import org.apache.activemq.command.ActiveMQQueue;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Primary;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Ion Perpegel
 * @since 4.2
 */
@Configuration
@ImportResource({
        "classpath:config/commonsTestContext.xml"
})
public class DomibusTestMocksConfiguration {

    //override the default Domibus converter so that the UI rest resources do not prepend )]}', in the response JSON
    @Primary
    @Bean
    CustomMappingJackson2HttpMessageConverter customMappingJackson2HttpMessageConverter() {
        return new CustomMappingJackson2HttpMessageConverter();
    }

    /**
     * Disable clean MDCKey for tests
     */
    @Primary
    @Bean
    CleanMDCKeysInterceptor cleanMDCKeyInterceptor() {
        return new CleanMDCKeysInterceptor() {
            @Override
            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
                //do nothing
                return true;
            }

            @Override
            public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
                //do nothing
            }
        };
    }

    @Primary
    @Bean
    public BackendConnectorProvider backendConnectorProvider() {
        return Mockito.mock(BackendConnectorProvider.class);
    }

    @Primary
    @Bean
    PluginAsyncNotificationConfiguration pluginAsyncNotificationConfiguration() {
        return Mockito.mock(PluginAsyncNotificationConfiguration.class);
    }

    @Primary
    @Bean("notifyBackendWebServiceQueue")
    public ActiveMQQueue notifyBackendWSQueue() {
        return new ActiveMQQueue("domibus.notification.webservice");
    }

    @Primary
    @Bean
    MSHDispatcher mshDispatcher() {
        return Mockito.mock(MSHDispatcher.class);
    }

    @Primary
    @Bean
    ReliabilityChecker reliabilityChecker() {
        return Mockito.mock(ReliabilityChecker.class);
    }

}
