package eu.domibus.test;

import eu.domibus.api.ebms3.model.Ebms3MessagingWithSecurityProfileMetadata;
import eu.domibus.core.ebms3.mapper.Ebms3Converter;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.util.MessageUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.hibernate.loader.plan.build.internal.LoadGraphLoadPlanBuildingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.xml.soap.SOAPMessage;
import javax.xml.ws.WebServiceException;

import static eu.domibus.core.ebms3.sender.client.DispatchClientDefaultProvider.MESSAGING_WITH_SECURITY_PROFILE_METADATA;

@Primary
@Service
public class MSHWebserviceOverride extends MSHWebservice {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MSHWebserviceOverride.class);

    protected Ebms3MessagingWithSecurityProfileMetadata ebms3MessagingWithSecurityProfileMetadata;

    @Autowired
    MessageUtil messageUtil;

    @Autowired
    Ebms3Converter ebms3Converter;

    @Override
    protected Ebms3MessagingWithSecurityProfileMetadata getMessagingWithSecurityProfileMetadata() {
        return ebms3MessagingWithSecurityProfileMetadata;
    }

    @Override
    public SOAPMessage invoke(SOAPMessage request) {
        try {
            ebms3MessagingWithSecurityProfileMetadata = (Ebms3MessagingWithSecurityProfileMetadata) request.getProperty(MESSAGING_WITH_SECURITY_PROFILE_METADATA);
            //if the messaging is already set on the soap message we don't need to get it from the request
            if (ebms3MessagingWithSecurityProfileMetadata == null) {
                LOG.debug("Getting Messaging from SOAPMessage request");
                ebms3MessagingWithSecurityProfileMetadata = messageUtil.getMessagingWithSecurityProfileMetadata(request);
            }
        } catch (Exception e) {
            throw new WebServiceException("Error getting Messaging", e);
        }
        return super.invoke(request);
    }

    @Override
    protected void setUserMessageEntityIdOnContext() {

    }
}
