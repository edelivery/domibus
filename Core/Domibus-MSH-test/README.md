# IT Tests

## Guidelines

Create an IT test is useful for at least 2 reasons:

- Helps you to develop a new feature or test a bug
- In general, you don't need to start a real Domibus instance at the implementation phase
- Helps to detect regressions following a code change or a refactoring

## Database changes
Each database change must be propagated in the `domibus-sql` repository but also in the Domibus repository `Core/Domibus-MSH-db/src/main/resources/db` in the files:
- `changelog.xml`
- `changelog-data.xml`
- `changelog-multi-tenancy.xml`
- `changelog-multi-tenancy-data.xml`
- `changelog-quartz.xml`

Propagation of the database changes in the Domibus repository is needed for the IT tests.

## Abstract class to be used when creating IT tests

Extend one of the following classes to create an IT test depending on what you need to test:

| Module       | Class                   |                                                                                     
|--------------|-------------------------|
| Domibus core | AbstractTomcatTestIT    |
| WS Plugin    | AbstractWSPluginTestIT  |
| FS Plugin    | AbstractFSPluginTestIT  |
| JMS Plugin   | AbstractJMSPluginTestIT |

The base class for all tests is `eu.domibus.test.AbstractIT`. When running an IT test the AbstractIT sets up the prerequisites for running the IT test:

- `domibus.config.location` is set to `target/test-classes`
- sets up the Domibus configuration: activemq.xml file, keystore, truststore, domibus.properties
- creates the H2 in-memory database and waits for 5 seconds to give enough time for the database to be fully created
- creates dictionary entities
- updates the plugins order
- payload storage initialization

## Connecting to the in-memory H2 database while running an IT test

Sometimes you need to connect to the H2 database when running an IT test to debug an issue.

In order to connect to the in-memory database you need to when running in IT test you need the following:

- make sure the IT test method is **not** starting a transaction eg is not annotated with @Transactional
- start an H2 listener in the IT test
```java
@Test
public void myITTest() {
    Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082").start();    
    
    //the code of the IT test 
}
```
- put a breakpoint in the code and make sure the breakpoint suspends only the current thread; this is very important, otherwise you will not be able to open the H2 database console
- open the database console link in the browser: http://localhost:8082/login.jsp
- fill in the following in JDBC URL field: `jdbc:h2:mem:DATABASE_NAME;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=false` where DATABASE_NAME is the name of the database eg test_general_1711555209886. You can get the database name from the logs, just search for the string `Using schema`
- Leave the default username `sa` and empty password and press `Connect`

> **NOTE** Don't forget to comment out the line which starts the H2 listener after you finish the investigation, otherwise all IT tests will fail due to port collision



## Utility classes used when implementing IT tests

### ITTestsService
Contains methods for creating/deleting/sending/receiving UserMessages:
 
- receiveMessage: receive a UserMessage on the /msh endpoint
- prepareMocksForSendingMessage: prepare tests for sending a UserMessage from C2 to C3. See an examples here for: 
  - successful sending: eu.domibus.core.plugin.handler.MessageSubmitterTestIT.messageSendSuccessWithStaticDiscovery
  - failed sending: eu.domibus.plugin.ws.webservice.SubmitMessageIT.testSubmitMessageAndFailToSendMessage
- deleteAllMessages: delete all UserMessages(and their associated entities eg UserMessageLog, SignalMessage) from the database
- other useful methods

### MessageDaoTestUtil

Contains methods for creating/deleting UserMessages/SignalMessages

### DomibusConditionUtil

Contains methods used for waiting for different conditions related to the database data. Methods are multitenancy aware. 
Examples:
- waitUntilMessageIsAcknowledged
- waitUntilMessageIsSentFailed
- waitUntilMessageIsReceived
- waitUntilMessageIsInWaitingForRetry

### SubmissionUtil

Contains methods for creating Submission instances.


### SoapSampleUtil

Contains methods to create SOAPMessages

### JMSMessageUtil

Contains methods to consume JMS messages

## Short-circuit JMSManager to send JMS messages synchronously

When testing methods which are sending JMS messages, in the IT tests we need to short-circuit the JMSManager so that it sends the JMS message synchronously.

The method `eu.domibus.test.ITTestsService#modifyJmsManagerToSubmitUserMessage` shows an example how to short-circuit the JMSManager so that Domibus C2 sends the message to C3 synchronously. 

The methods `eu.domibus.test.ITTestsService#prepareMocksForSendingMessage` and `eu.domibus.test.ITTestsService#prepareMocksForFailingSendingMessage` use the `modifyJmsManagerToSubmitUserMessage` method to prepare an IT test for sending a message from C2 to C3 with a successful or failed outcome.

# Examples of IT tests

- MessageSubmitterTestIT: sending a message from C2 to C3 
- MshWebServiceTestIT: receiving a message from C3 to C2 
- WS Plugin(tests from eu.domibus.plugin.ws.webservice package):
  - SubmitMessageIT: submit a message from WSPlugin to Domibus C2 which sends it to C3 and wait until the message is acknowledged
  - RetrieveMessageIT: Domibus as C3 receives a message from C2; Domibus C3 delivers the message to the WS Plugin and C4 retrieves the message
  - RetrieveMessageIT: Domibus as C3 receives a message from C2; Domibus C3 delivers the message to the WS Plugin and C4 marks the message as downloaded: testMarkMessageAsDownloadedUsingMessageId and testMarkMessageAsDownloadedUsingMessageEntityId
  - PendingMessagesListIT: Domibus as C3 receives several messages from C2; C4 calls listPendingMessages based on the message id and message entity id 
- JMS Plugin
  - SendMessageJMSIT: submit a message from JMSPlugin to Domibus C2 which sends it to C3; checks that submit and send success JMS messages are sent on the reply queue
  - DownloadMessageJMSIT: Domibus as C3 receives a message from C2; Domibus C3 delivers the message to the JMS Plugin; C4 downloads the messages from the out queue
- FS Plugin
  - FSPluginTestIT
    - submitMessage: submit a message from FSPlugin to Domibus C2 which sends it to C3; we check that the file name was modified and moved to the SENT folder
    - receiveMessage: Domibus as C3 receives a message from C2; Domibus C3 delivers the message to the FS Plugin; we check that the metadata.xml and the payload are created in the IN folder
