package eu.domibus.ext.delegate.services.message;

import eu.domibus.api.message.SignalMessageSoapEnvelopeSpiDelegate;
import eu.domibus.api.message.SoapMessageMetadata;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.spi.soapenvelope.HttpMetadata;
import eu.domibus.core.spi.soapenvelope.SignalMessageSoapEnvelopeSpi;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPMessage;

/**
 * @author Cosmin Baciu
 * @since 5.0.2
 */
@Service
public class SignalMessageSoapEnvelopeSpiDelegateImpl implements SignalMessageSoapEnvelopeSpiDelegate {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SignalMessageSoapEnvelopeSpiDelegateImpl.class);

    protected SignalMessageSoapEnvelopeSpi soapEnvelopeSpi;
    protected DomibusPropertyProvider domibusPropertyProvider;
    protected SoapMessageSpiHelper messageSpiHelper;

    public SignalMessageSoapEnvelopeSpiDelegateImpl(@Autowired(required = false) SignalMessageSoapEnvelopeSpi soapEnvelopeSpi,
                                                    SoapMessageSpiHelper messageSpiHelper,
                                                    DomibusPropertyProvider domibusPropertyProvider) {
        this.soapEnvelopeSpi = soapEnvelopeSpi;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.messageSpiHelper = messageSpiHelper;
    }

    @Override
    public SOAPMessage beforeSigningAndEncryption(SOAPMessage soapMessage,
                                                  SoapMessageMetadata soapMessageMetadata,
                                                  HttpServletRequest httpServletRequest,
                                                  HttpServletResponse httpServletResponse) {
        final boolean soapEnvelopeSpiActive = isSoapEnvelopeSpiActive();
        if (!soapEnvelopeSpiActive) {
            LOG.debug("BeforeSigningAndEncryption hook skipped: SPI is not active");
            return soapMessage;
        }

        final Boolean httpHeaderMetadataActive = domibusPropertyProvider.getBooleanProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DISPATCHER_HTTP_HEADER_METADATA_ACTIVE);

        HttpMetadata httpMetadata = null;
        if (httpHeaderMetadataActive) {
            httpMetadata = messageSpiHelper.extractHttpMetadata(
                    soapMessageMetadata.getContentType(),
                    soapMessageMetadata.getHttpProtocolHeadersJson(),
                    soapMessageMetadata.getMessagePropertiesValueJson());
        }

        LOG.debug("Executing beforeSigningAndEncryption hook");
        final SOAPMessage resultSoapMessage = soapEnvelopeSpi.beforeSigningAndEncryption(soapMessage, httpMetadata, httpServletRequest, httpServletResponse);
        LOG.debug("Finished executing beforeSigningAndEncryption hook");

        return resultSoapMessage;
    }

    @Override
    public SOAPMessage afterReceiving(SOAPMessage responseMessage) {
        final boolean soapEnvelopeSpiActive = isSoapEnvelopeSpiActive();
        if (!soapEnvelopeSpiActive) {
            LOG.debug("afterReceiving hook skipped: SPI is not active");
            return responseMessage;
        }

        final Boolean httpHeaderMetadataActive = domibusPropertyProvider.getBooleanProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DISPATCHER_HTTP_HEADER_METADATA_ACTIVE);

        HttpMetadata httpMetadata = null;
        if (httpHeaderMetadataActive) {
            final String contentType = LOG.getMDC(MessageConstants.HTTP_CONTENT_TYPE);
            final String httpProtocolHeadersJson = LOG.getMDC(MessageConstants.HTTP_PROTOCOL_HEADERS);
            final String messagePropertiesValueJson = LOG.getMDC(MessageConstants.HTTP_MESSAGE_PROPERTIES);
            httpMetadata = messageSpiHelper.extractHttpMetadata(contentType, httpProtocolHeadersJson, messagePropertiesValueJson);
        }

        LOG.debug("Executing afterReceiving with http metadata hook");
        final SOAPMessage resultSoapMessage = soapEnvelopeSpi.afterReceiving(responseMessage, httpMetadata);
        LOG.debug("Finished executing afterReceiving http metadata hook");

        return resultSoapMessage;
    }

    protected boolean isSoapEnvelopeSpiActive() {
        return soapEnvelopeSpi != null;
    }
}
