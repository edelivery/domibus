package eu.domibus.ext.web.interceptor;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author François Gautier
 * @since 5.2
 */
public class CleanMDCKeysInterceptor implements HandlerInterceptor {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(CleanMDCKeysInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        LOG.debug("preHandle request for {} clean MDCKeys", request.getRequestURI());
        LOG.clearCustomKeys();
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        LOG.debug("afterCompletion request for {} clean MDCKeys", request.getRequestURI());
        LOG.clearCustomKeys();
    }
}