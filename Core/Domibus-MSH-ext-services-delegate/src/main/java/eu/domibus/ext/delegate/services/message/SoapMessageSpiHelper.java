package eu.domibus.ext.delegate.services.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.domibus.core.spi.soapenvelope.HttpMetadata;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static eu.domibus.api.property.DomibusGeneralConstants.JSON_MAPPER_BEAN;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
@Service
public class SoapMessageSpiHelper {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SoapMessageSpiHelper.class);

    protected ObjectMapper objectMapper;

    public SoapMessageSpiHelper(@Qualifier(JSON_MAPPER_BEAN) ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public HttpMetadata extractHttpMetadata(final String contentType,
                                            final String httpProtocolHeadersJson,
                                            final String messagePropertiesValueJson) {
        LOG.debug("Extracting HTTP metadata");

        HttpMetadata httpMetadata = new HttpMetadata();

        LOG.debug("Content-Type value is [{}]", contentType);
        httpMetadata.setContentType(contentType);

        LOG.debug("HTTP Headers value is [{}]", httpProtocolHeadersJson);
        try {
            Map<String, List<String>> headers = objectMapper.readValue(httpProtocolHeadersJson, Map.class);
            httpMetadata.setHeaders(headers);
        } catch (JsonProcessingException e) {
            LOG.error("Could not extract protocol headers from JSON [{}]", httpProtocolHeadersJson, e);
        }

        LOG.debug("Message properties value [{}]", messagePropertiesValueJson);
        try {
            Map<String, Object> messageProperties = objectMapper.readValue(messagePropertiesValueJson, Map.class);
            httpMetadata.setMessageProperties(messageProperties);
        } catch (JsonProcessingException e) {
            LOG.error("Could not extract message properties from JSON [{}]", messagePropertiesValueJson, e);
        }

        return httpMetadata;
    }
}
