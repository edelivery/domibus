package eu.domibus.ext.delegate.services.cxf;

import eu.domibus.api.crypto.TLSGenericCertificateManager;
import eu.domibus.api.crypto.TLSGenericSSLContext;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.ext.services.TLSGenericReaderExtService;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

/**
 * Delegate class allowing Domibus extension/plugin to use TLS Generic configuration of Domibus for the current domain.
 *
 * @author Breaz Ionut
 * @since 5.2
 *
 */
@Service
public class TLSGenericReaderDelegate implements TLSGenericReaderExtService {

    protected TLSGenericSSLContext tlsGenericSSLContext;
    protected TLSGenericCertificateManager tlsGenericCertificateManager;
    protected DomibusPropertyProvider domibusPropertyProvider;

    public TLSGenericReaderDelegate(TLSGenericSSLContext tlsGenericSSLContext,
                                    TLSGenericCertificateManager tlsGenericCertificateManager,
                                    DomibusPropertyProvider domibusPropertyProvider) {
        this.tlsGenericSSLContext = tlsGenericSSLContext;
        this.tlsGenericCertificateManager = tlsGenericCertificateManager;
        this.domibusPropertyProvider = domibusPropertyProvider;
    }

    @Override
    public SSLContext getTLSGenericSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        return tlsGenericSSLContext.getNewTLSGenericSSLContext();
    }

    @Override
    public KeyStore getTLSGenericTruststore() {
        return tlsGenericCertificateManager.getTLSGenericTruststore();
    }

    @Override
    public boolean useCacertsWithTLSGenericTruststore() {
        return Boolean.TRUE.equals(domibusPropertyProvider.getBooleanProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_USE_CACERTS));
    }
}
