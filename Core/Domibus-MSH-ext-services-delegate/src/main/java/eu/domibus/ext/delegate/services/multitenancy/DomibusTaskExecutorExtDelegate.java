package eu.domibus.ext.delegate.services.multitenancy;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.multitenancy.ExecutorExceptionHandler;
import eu.domibus.api.multitenancy.ExecutorWaitPolicy;
import eu.domibus.ext.delegate.mapper.DomibusExtMapper;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomibusTaskExtExecutor;
import eu.domibus.ext.services.ExecutorExceptionHandlerExt;
import eu.domibus.ext.services.ExecutorWaitPolicyExt;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
@Service
public class DomibusTaskExecutorExtDelegate implements DomibusTaskExtExecutor {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusTaskExecutorExtDelegate.class);

    protected DomibusExtMapper domibusExtMapper;

    protected DomibusTaskExecutor domainTaskExecutor;

    public DomibusTaskExecutorExtDelegate(DomibusExtMapper domibusExtMapper, DomibusTaskExecutor domainTaskExecutor) {
        this.domibusExtMapper = domibusExtMapper;
        this.domainTaskExecutor = domainTaskExecutor;
    }

    @Override
    public void submitLongRunningTask(Runnable task, Runnable errorHandler, DomainDTO domainDTO) {
        LOG.trace("Submitting long running task with error handler for domain [{}]", domainDTO);

        final Domain domain = domibusExtMapper.domainDTOToDomain(domainDTO);
        ExecutorExceptionHandler executorExceptionHandler = exception -> {
            errorHandler.run();
        };
        domainTaskExecutor.submitLongRunningTask(task, domain, null, executorExceptionHandler);

        LOG.trace("Submitted long running task with error handler for domain [{}]", domainDTO);
    }

    @Override
    public void submitLongRunningTask(Runnable task, DomainDTO domainDTO) {
        LOG.trace("Submitting long running task for domain [{}]", domainDTO);

        final Domain domain = domibusExtMapper.domainDTOToDomain(domainDTO);
        domainTaskExecutor.submitLongRunningTask(task, domain, null, null);

        LOG.trace("Submitted long running task for domain [{}]", domainDTO);
    }

    @Override
    public void submitLongRunningTask(Runnable task,
                                      DomainDTO domainDTO,
                                      Authentication authentication,
                                      ExecutorExceptionHandlerExt errorHandler) {
        final Domain domain = domibusExtMapper.domainDTOToDomain(domainDTO);
        ExecutorExceptionHandler executorExceptionHandler = convertExecutorExceptionHandlerExt(errorHandler);
        domainTaskExecutor.submitLongRunningTask(task, domain, authentication, executorExceptionHandler);
    }

    protected ExecutorExceptionHandler convertExecutorExceptionHandlerExt(ExecutorExceptionHandlerExt errorHandler) {
        if (errorHandler == null) {
            return null;
        }
        ExecutorExceptionHandler executorExceptionHandler = exception -> errorHandler.handleException(exception);
        return executorExceptionHandler;
    }

    protected ExecutorWaitPolicy convert(ExecutorWaitPolicyExt executorWaitPolicyExt) {
        return ExecutorWaitPolicy.valueOf(executorWaitPolicyExt.name());
    }

    @Override
    public <T> T submitCallable(Callable<T> task,
                                DomainDTO domainDTO,
                                Authentication authentication,
                                ExecutorWaitPolicyExt waitPolicy,
                                Long timeout,
                                TimeUnit timeUnit,
                                ExecutorExceptionHandlerExt errorHandler) {
        final Domain domain = domibusExtMapper.domainDTOToDomain(domainDTO);
        ExecutorExceptionHandler executorExceptionHandler = convertExecutorExceptionHandlerExt(errorHandler);
        final ExecutorWaitPolicy executorWaitPolicy = convert(waitPolicy);
        return domainTaskExecutor.submitCallable(task, domain, authentication, executorWaitPolicy, timeout, timeUnit, executorExceptionHandler);
    }

    @Override
    public void submit(Runnable task, DomainDTO domainDTO, ExecutorWaitPolicyExt waitPolicy, Long timeout, TimeUnit timeUnit) {
        final Domain domain = domibusExtMapper.domainDTOToDomain(domainDTO);
        final ExecutorWaitPolicy executorWaitPolicy = convert(waitPolicy);
        domainTaskExecutor.submit(task, domain, executorWaitPolicy, timeout, timeUnit);

    }

    @Override
    public <R> R executeWithLock(Callable<R> task,
                                 String dbLockKey,
                                 String javaLockKey,
                                 DomainDTO domainDTO,
                                 Authentication authentication,
                                 ExecutorWaitPolicyExt waitPolicy,
                                 Long timeout,
                                 TimeUnit timeUnit,
                                 ExecutorExceptionHandlerExt errorHandler) {
        final Domain domain = domibusExtMapper.domainDTOToDomain(domainDTO);
        ExecutorExceptionHandler executorExceptionHandler = convertExecutorExceptionHandlerExt(errorHandler);
        final ExecutorWaitPolicy executorWaitPolicy = convert(waitPolicy);
        return domainTaskExecutor.executeWithLock(
                task,
                dbLockKey,
                javaLockKey,
                domain,
                authentication,
                executorWaitPolicy,
                timeout,
                timeUnit,
                executorExceptionHandler);
    }

    @Override
    public <R> R executeWithLock(Callable<R> task, String dbLockKey, String javaLockKey) {
        return domainTaskExecutor.executeWithLock(task, dbLockKey, javaLockKey);
    }
}
