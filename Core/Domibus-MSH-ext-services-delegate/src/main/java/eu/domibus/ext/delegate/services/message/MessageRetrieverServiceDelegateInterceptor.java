package eu.domibus.ext.delegate.services.message;

import eu.domibus.api.security.AuthenticationException;
import eu.domibus.ext.delegate.services.interceptor.ServiceInterceptor;
import eu.domibus.ext.exceptions.AuthenticationExtException;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MessageRetrieverServiceDelegateInterceptor extends ServiceInterceptor {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageRetrieverServiceDelegateInterceptor.class);

    @Around(value = "execution(public * eu.domibus.ext.delegate.services.message.MessageRetrieverServiceDelegate.*(..))")
    @Override
    public Object intercept(ProceedingJoinPoint joinPoint) throws Throwable {
        return super.intercept(joinPoint);
    }

    @Override
    public Exception convertCoreException(Exception e) {
        if (e instanceof AuthenticationException) {
            return new AuthenticationExtException(e);
        }
        if (e instanceof eu.domibus.api.messaging.MessageNotFoundException) {
            return new eu.domibus.messaging.MessageNotFoundException(e);
        }
        return e;
    }

    @Override
    public DomibusLogger getLogger() {
        return LOG;
    }
}
