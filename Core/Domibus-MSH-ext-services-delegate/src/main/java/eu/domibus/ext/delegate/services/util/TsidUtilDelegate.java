package eu.domibus.ext.delegate.services.util;

import eu.domibus.api.util.TsidUtil;
import eu.domibus.ext.services.TsidUtilExtService;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

/**
 * @author François Gautier
 * @since 5.2
 */
@Service
public class TsidUtilDelegate implements TsidUtilExtService {

    protected TsidUtil tsidUtil;

    public TsidUtilDelegate(TsidUtil tsidUtil) {
        this.tsidUtil = tsidUtil;
    }

    public ZonedDateTime getZonedDateTimeFromTsid(Long tsid) {
        return tsidUtil.getZonedDateTimeFromTsid(tsid);
    }
}
