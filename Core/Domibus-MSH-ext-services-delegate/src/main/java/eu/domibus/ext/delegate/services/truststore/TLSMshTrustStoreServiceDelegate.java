package eu.domibus.ext.delegate.services.truststore;

import eu.domibus.api.crypto.SameResourceCryptoException;
import eu.domibus.api.crypto.TLSMshCertificateManager;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.security.TrustStoreEntry;
import eu.domibus.ext.delegate.mapper.DomibusExtMapper;
import eu.domibus.ext.domain.KeyStoreContentInfoDTO;
import eu.domibus.ext.domain.TrustStoreDTO;
import eu.domibus.ext.services.TLSMshTrustStoreExtService;
import org.springframework.stereotype.Service;

import java.util.List;

import static eu.domibus.api.crypto.TLSMshCertificateManager.TLS_MSH_TRUSTSTORE_NAME;

/**
 * @author Soumya Chandran
 * @since 5.1
 */
@Service
public class TLSMshTrustStoreServiceDelegate implements TLSMshTrustStoreExtService {

    private final TLSMshCertificateManager tlsMshCertificateManager;

    private final DomibusExtMapper domibusExtMapper;

    public TLSMshTrustStoreServiceDelegate(TLSMshCertificateManager tlsMshCertificateManager, DomibusExtMapper domibusExtMapper) {
        this.tlsMshCertificateManager = tlsMshCertificateManager;
        this.domibusExtMapper = domibusExtMapper;
    }

    @Override
    public KeyStoreContentInfoDTO downloadTruststoreContent() {
        KeyStoreContentInfo contentInfo = tlsMshCertificateManager.getTruststoreContent();
        return domibusExtMapper.keyStoreContentInfoToKeyStoreContentInfoDTO(contentInfo);
    }

    @Override
    public List<TrustStoreDTO> getTrustStoreEntries() {
        List<TrustStoreEntry> trustStoreEntries = tlsMshCertificateManager.getTrustStoreEntries();
        return domibusExtMapper.trustStoreEntriesToTrustStoresDTO(trustStoreEntries);
    }

    @Override
    public void uploadTruststoreFile(KeyStoreContentInfoDTO contentInfoDTO) {
        KeyStoreContentInfo storeContentInfo = domibusExtMapper.keyStoreContentInfoDTOToKeyStoreContentInfo(contentInfoDTO);
        tlsMshCertificateManager.replaceTrustStore(storeContentInfo);
    }

    @Override
    public void addCertificate(byte[] fileContent, String alias) {
        boolean added = tlsMshCertificateManager.addCertificate(fileContent, alias);
        if (!added) {
            throw new SameResourceCryptoException(alias, null,
                    "Certificate [" + alias + "] was not added to the [" + TLS_MSH_TRUSTSTORE_NAME + "] most probably because it already contains the same certificate.");
        }
    }

    @Override
    public void removeCertificate(String alias) {
        boolean removed = tlsMshCertificateManager.removeCertificate(alias);
        if (!removed) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_009,
                    "Certificate [" + alias + "] was not removed from the [" + TLS_MSH_TRUSTSTORE_NAME + "] because it does not exist.");
        }
    }

    @Override
    public String getStoreFileExtension() {
        return tlsMshCertificateManager.getStoreFileExtension();
    }

}

