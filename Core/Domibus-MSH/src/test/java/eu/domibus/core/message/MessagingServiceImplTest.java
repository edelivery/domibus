package eu.domibus.core.message;

import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.PartInfo;
import eu.domibus.api.model.PartProperty;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.usermessage.UserMessageService;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.sender.retry.UpdateRetryLoggingService;
import eu.domibus.core.message.compression.CompressionService;
import eu.domibus.core.message.splitandjoin.SplitAndJoinService;
import eu.domibus.core.payload.persistence.DomibusPayloadManagerSpiProviderImpl;
import eu.domibus.core.payload.persistence.PayloadPersistence;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import eu.domibus.core.plugin.transformer.SubmissionAS4Transformer;
import mockit.*;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.HashSet;

import static eu.domibus.common.model.configuration.Payload_.MIME_TYPE;
import static eu.domibus.core.message.MessagingServiceImpl.MIME_TYPE_APPLICATION_UNKNOWN;

/**
 * @author Ioana Dragusanu
 * @author Cosmin Baciu
 * @since 3.3
 */

@SuppressWarnings("ResultOfMethodCallIgnored")
@ExtendWith(JMockitExtension.class)
class MessagingServiceImplTest {

    @Tested
    MessagingServiceImpl messagingService;

    @Injectable
    DomibusPayloadManagerSpiProviderImpl domibusPayloadManagerSpiProvider;

    @Injectable
    UserMessagePayloadService userMessagePayloadService;

    @Injectable
    PayloadPersistence payloadPersistence;

    @Injectable
    private DomainContextProvider domainContextProvider;

    @Injectable
    LegConfiguration legConfiguration;

    @Injectable
    SplitAndJoinService splitAndJoinService;

    @Injectable
    CompressionService compressionService;

    @Injectable
    DomibusTaskExecutor domainTaskExecutor;

    @Injectable
    UserMessageService userMessageService;

    @Injectable
    BackendNotificationService backendNotificationService;

    @Injectable
    UserMessageLogDao userMessageLogDao;

    @Injectable
    PartInfoServiceImpl partInfoService;

    @Injectable
    UserMessageDao userMessageDao;

    @Injectable
    SubmissionAS4Transformer transformer;

    @Injectable
    UpdateRetryLoggingService updateRetryLoggingService;


    @Test
    void testStoreSourceMessagePayloads(@Injectable UserMessage userMessage,
                                               @Injectable MSHRole mshRole,
                                               @Injectable LegConfiguration legConfiguration,
                                               @Injectable String backendName) {

        String messageId = "123";
        Long messageEntityId = 1L;
        new Expectations(messagingService) {{
            userMessage.getMessageId();
            result = messageId;

            userMessage.getEntityId();
            result = messageEntityId;

            messagingService.storePayloads(userMessage, null, mshRole, legConfiguration, backendName, false);
        }};

        messagingService.storeSourceMessagePayloads(userMessage, null, mshRole, legConfiguration, backendName, false);

        new Verifications() {{
            userMessageService.scheduleSourceMessageSending(messageId, messageEntityId);
        }};
    }

    @Test
    void testStoreMessageCalls(@Injectable final UserMessage userMessage) {
        messagingService.storeMessagePayloads(userMessage, null, MSHRole.SENDING, legConfiguration, "backend");
    }

    @Test
    void setContentType_unknown(@Injectable PartInfo partInfo) {
        new Expectations() {{
            partInfo.getPartProperties();
            result = null;

            partInfo.getPayloadDatahandler().getContentType();
            result = null;

            partInfo.getHref();
            result = "href";
        }};

        messagingService.setContentType(partInfo);

        new FullVerifications() {{
            partInfo.setMime(MIME_TYPE_APPLICATION_UNKNOWN);
            times = 1;
        }};
    }

    @Test
    void setContentType(@Injectable PartInfo partInfo,
                               @Injectable PartProperty partProperty) {

        HashSet<PartProperty> partProperties = new HashSet<>();
        partProperties.add(partProperty);

        new Expectations() {{
            partInfo.getPayloadDatahandler().getContentType();
            result = null;

            partInfo.getPartProperties();
            result = partProperties;

            partProperty.getName();
            result = MIME_TYPE;

            partProperty.getValue();
            result = "application/json";

            partInfo.getHref();
            result = "href";
        }};

        messagingService.setContentType(partInfo);

        new FullVerifications() {{
            partInfo.setMime("application/json");
            times = 1;
        }};
    }
}
