package eu.domibus.core.util;

import mockit.Tested;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RegexUtilImplTest {

    @Tested
    RegexUtilImpl regexUtil;

    @Test
    public void testWildflyQueueNameIsInternal() {
        final String queueName = "jms.queue.DomibusAlertMessageQueue";
        final String internalQueueExpression = ".*jms.queue.(Domibus[a-zA-Z]|DLQ|ExpiryQueue|internal|backend.jms|notification.jms|notification.webservice|notification.sutController|notification.filesystem).*";

        boolean internal = regexUtil.matches(internalQueueExpression, queueName);
        Assertions.assertTrue(internal);
    }

    @Test
    public void testWildflyQueueNameIsNotInternal() {
        final String queueName = "jms.DomibusAlertMessageQueue";
        final String internalQueueExpression = ".*jms.queue.(Domibus[a-zA-Z]|DLQ|ExpiryQueue|internal|backend.jms|notification.jms|notification.webservice|notification.sutController|notification.filesystem).*";

        boolean internal = regexUtil.matches(internalQueueExpression, queueName);
        Assertions.assertFalse(internal);
    }

    @Test
    public void testWeblogicQueueNameIsInternal() {
        final String queueName = "domibus.backend.jms.inQueue ";
        final String internalQueueExpression = ".*domibus.(internal|DLQ|backend|.jms|notification|.jms|notification|.webservice|notification|.sutController|notification|.filesystem).*";

        boolean internal = regexUtil.matches(internalQueueExpression, queueName);
        Assertions.assertTrue(internal);
    }

    @Test
    public void testWeblogicQueueNameIsNotInternal() {
        final String queueName = "backend.jms.inQueue ";
        final String internalQueueExpression = ".*domibus.(internal|DLQ|backend|.jms|notification|.jms|notification|.webservice|notification|.sutController|notification|.filesystem).*";

        boolean internal = regexUtil.matches(internalQueueExpression, queueName);
        Assertions.assertFalse(internal);
    }

    @Test
    public void testTomcatQueueNameIsInternal() {
        final String queueName = "domibus.backend.jms.outQueue";
        final String internalQueueExpression = ".*domibus.(internal|DLQ|backend|.jms|notification|.jms|notification|.webservice|notification|.sutController|notification|.filesystem).*";

        boolean internal = regexUtil.matches(internalQueueExpression, queueName);
        Assertions.assertTrue(internal);
    }

    @Test
    public void testTomcatQueueNameIsNotInternal() {
        final String queueName = "jms.outQueue";
        final String internalQueueExpression = ".*domibus.(internal|DLQ|backend|.jms|notification|.jms|notification|.webservice|notification|.sutController|notification|.filesystem).*";

        boolean internal = regexUtil.matches(internalQueueExpression, queueName);
        Assertions.assertFalse(internal);
    }

}
