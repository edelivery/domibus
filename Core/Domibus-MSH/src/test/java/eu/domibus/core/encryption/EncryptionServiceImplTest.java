package eu.domibus.core.encryption;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.encryption.PasswordEncryptionService;
import eu.domibus.api.payload.encryption.PayloadEncryptionService;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Cosmin Baciu
 * @since 4.1.3
 */
class EncryptionServiceImplTest {

    @Injectable
    protected PayloadEncryptionService payloadEncryptionService;

    @Injectable
    protected PasswordEncryptionService passwordEncryptionService;

    @Injectable
    protected DomibusConfigurationService domibusConfigurationService;

    @Injectable
    protected DomainContextProvider domainContextProvider;

    @Injectable
    protected DomainService domainService;

    @Tested
    EncryptionServiceImpl encryptionService;

    @Test
    void isAnyEncryptionActiveWithGeneralEncryptionActive() {
        new Expectations() {{
            domibusConfigurationService.isPasswordEncryptionActive();
            result = true;
        }};

        assertTrue(encryptionService.isAnyEncryptionActive());
    }

    @Test
    void isAnyEncryptionActiveWithOneDomainActive() {
        List<Domain> domains = new ArrayList<>();
        Domain domain = new Domain();
        domains.add(domain);

        new Expectations() {{
            domibusConfigurationService.isPasswordEncryptionActive();
            result = false;

            domainService.getDomains();
            result = domains;

            domibusConfigurationService.isPayloadEncryptionActive(domain);
            result = true;
        }};

        assertTrue(encryptionService.isAnyEncryptionActive());
    }

    @Test
    void handleEncryption() {
        new Expectations(encryptionService) {{
            encryptionService.isAnyEncryptionActive();
            result = true;
            encryptionService.doHandleEncryption();
        }};

        encryptionService.handleEncryption();

        new Verifications() {{
            encryptionService.doHandleEncryption();
            times = 1;
        }};
    }

    @Test
    void handleEncryption_not() {
        new Expectations(encryptionService) {{
            encryptionService.isAnyEncryptionActive();
            result = false;
        }};

        encryptionService.handleEncryption();

        new Verifications() {{
            encryptionService.doHandleEncryption();
            times = 0;
        }};
    }

}
