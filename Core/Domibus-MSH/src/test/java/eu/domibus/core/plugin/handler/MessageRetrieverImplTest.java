package eu.domibus.core.plugin.handler;

import eu.domibus.api.message.UserMessageSecurityService;
import eu.domibus.api.messaging.DuplicateMessageFoundException;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.MessageStatus;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.model.UserMessageLog;
import eu.domibus.api.security.AuthUtils;
import eu.domibus.common.ErrorResult;
import eu.domibus.core.error.ErrorLogEntry;
import eu.domibus.core.error.ErrorLogService;
import eu.domibus.core.message.MessagingService;
import eu.domibus.core.message.UserMessageDefaultService;
import eu.domibus.core.message.UserMessageLogDefaultService;
import eu.domibus.messaging.DuplicateMessageException;
import eu.domibus.messaging.MessageNotFoundException;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ApplicationEventPublisher;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings({"ResultOfMethodCallIgnored", "ConstantConditions"})
@ExtendWith(JMockitExtension.class)
class MessageRetrieverImplTest {
    private static final String MESS_ID = UUID.randomUUID().toString();

    @Tested
    MessageRetrieverImpl messageRetriever;

    @Injectable
    protected UserMessageSecurityService userMessageSecurityService;

    @Injectable
    protected UserMessageDefaultService userMessageService;

    @Injectable
    private MessagingService messagingService;

    @Injectable
    private UserMessageLogDefaultService userMessageLogService;

    @Injectable
    private ErrorLogService errorLogService;

    @Injectable
    protected ApplicationEventPublisher applicationEventPublisher;

    @Injectable
    AuthUtils authUtils;

    @Test
    void testDownloadMessageOK(@Injectable UserMessage userMessage,
                                      @Injectable UserMessageLog userMessageLog) throws Exception {

        new Expectations() {{
            userMessageService.getByMessageId(MESS_ID, MSHRole.RECEIVING);
            result = userMessage;
            userMessageLogService.findById(anyLong);
            result = userMessageLog;
            userMessageLog.getMessageStatus();
            result = MessageStatus.RECEIVED;
        }};

        messageRetriever.downloadMessage(MESS_ID);

        new Verifications() {{
            userMessageLogService.setMessageAsDownloaded(userMessage, userMessageLog);

        }};
    }

    @Test
    void testDownloadMessageOK_RetentionNonZero(@Injectable UserMessage userMessage,
                                                       @Injectable final UserMessageLog messageLog) throws Exception {
        new Expectations(messageRetriever) {{
            userMessageService.getByMessageId(MESS_ID, MSHRole.RECEIVING);
            result = userMessage;

            userMessageLogService.findById(anyLong);
            result = messageLog;

            messageLog.getMessageStatus();
            result = MessageStatus.RECEIVED;

        }};

        messageRetriever.downloadMessage(MESS_ID);

        new Verifications() {{
            userMessageLogService.setMessageAsDownloaded(userMessage, messageLog);
        }};
    }

    @Test
    void testDownloadMessageNoMsgFound() {
        new Expectations() {{
            userMessageService.getByMessageId(MESS_ID, MSHRole.RECEIVING);
            result = new eu.domibus.messaging.MessageNotFoundException(MESS_ID);
        }};

        try {
            messageRetriever.downloadMessage(MESS_ID);
            Assertions.fail("It should throw " + MessageNotFoundException.class.getCanonicalName());
        } catch (eu.domibus.messaging.MessageNotFoundException mnfEx) {
            //OK
        }

        new Verifications() {{
            userMessageLogService.findById(anyLong);
            times = 0;
        }};
    }

    @Test
    void testGetErrorsForMessageOk(@Injectable ErrorLogEntry errorLogEntry, @Injectable UserMessage userMessage) throws MessageNotFoundException, DuplicateMessageException {
        List<ErrorLogEntry> list = new ArrayList<>();
        list.add(errorLogEntry);
        new Expectations() {{
            errorLogService.getErrorsForMessage(MESS_ID);
            result = list;

        }};

        final List<? extends ErrorResult> results = messageRetriever.getErrorsForMessage(MESS_ID);

        new Verifications() {{
            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(MESS_ID);
            times = 1;

            errorLogService.convert(errorLogEntry);
            times = 1;
            Assertions.assertNotNull(results);
        }};

    }

    @Test
    void testGetErrorsForMessageOk_Exception(@Injectable ErrorLogEntry errorLogEntry, @Injectable UserMessageLog userMessageLog) {
        new Expectations() {{
            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(MESS_ID);
            result = new DuplicateMessageFoundException(MESS_ID);
        }};

        Assertions.assertThrows(DuplicateMessageException.class, () -> messageRetriever.getErrorsForMessage(MESS_ID));

        new Verifications() {{
            errorLogService.convert(errorLogEntry);
            times = 0;
        }};

    }

    @Test
    void testGetErrorsForMessageOk_NotFound(@Injectable ErrorLogEntry errorLogEntry, @Injectable UserMessageLog userMessageLog) throws MessageNotFoundException, DuplicateMessageException {
        List<ErrorLogEntry> list = new ArrayList<>();
        list.add(errorLogEntry);
        new Expectations() {{
            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(MESS_ID);
            result = new eu.domibus.api.messaging.MessageNotFoundException(MESS_ID);

            errorLogService.getErrorsForMessage(MESS_ID);
            result = list;
        }};

        final List<? extends ErrorResult> results = messageRetriever.getErrorsForMessage(MESS_ID);

        new Verifications() {{
            errorLogService.convert(errorLogEntry);
            times = 1;
        }};
        Assertions.assertNotNull(results);

    }

    @Test
    void browseMessage(@Injectable UserMessage userMessage) throws MessageNotFoundException {
        String messageId = "123";

        new Expectations(messageRetriever) {{
            userMessageService.getByMessageId(messageId, MSHRole.RECEIVING);
            result = userMessage;
        }};

        messageRetriever.browseMessage(messageId);

        new Verifications() {{
            messagingService.getSubmission(userMessage);
        }};
    }

}
