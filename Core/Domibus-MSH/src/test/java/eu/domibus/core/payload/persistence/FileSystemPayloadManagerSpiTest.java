package eu.domibus.core.payload.persistence;

import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.spi.payload.DeleteFolderResult;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static eu.domibus.api.multitenancy.DomainService.DEFAULT_DOMAIN;

class FileSystemPayloadManagerSpiTest {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(FileSystemPayloadManagerSpi.class);
    public static final String FOLDER_NAME = "2021/09/28_14/";
    public static final String FILE_NAME = "testFile.txt";

    @Tested
    FileSystemPayloadManagerSpi fileSystemPayloadManagerSpi;

    @Injectable
    protected DomibusPropertyProvider domibusPropertyProvider;

    private File payloadStorageDirectoryForDomain;

    @BeforeEach
    void setUp() throws IOException {
        payloadStorageDirectoryForDomain = Files.createTempDirectory(Paths.get("target"), "tmpDirPrefix").toFile();
        LOG.info("temp folder created: [{}]", payloadStorageDirectoryForDomain.getAbsolutePath());
    }

    @AfterEach
    void tearDown() {
        FileUtils.deleteQuietly(payloadStorageDirectoryForDomain);
    }

    @Test
    void deleteFolder() throws IOException {

        File folderToDelete = new File(payloadStorageDirectoryForDomain.getAbsoluteFile() + "/" + FOLDER_NAME);
        FileUtils.forceMkdir(folderToDelete);

        Assertions.assertTrue(folderToDelete.exists());

        new Expectations(fileSystemPayloadManagerSpi) {{
            fileSystemPayloadManagerSpi.getPayloadStorageDirectory(DEFAULT_DOMAIN.getCode());
            result = payloadStorageDirectoryForDomain;
        }};

        DeleteFolderResult deleteFolderResult = fileSystemPayloadManagerSpi.deleteFolder(DEFAULT_DOMAIN.getCode(), FOLDER_NAME);

        Assertions.assertEquals(DeleteFolderResult.Result.OK, deleteFolderResult.getResult());
        Assertions.assertFalse(folderToDelete.exists());
    }
}