package eu.domibus.core.payload;

import eu.domibus.api.model.PartInfo;
import eu.domibus.api.model.PartProperty;
import eu.domibus.api.model.UserMessage;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.common.model.configuration.Payload;
import eu.domibus.common.model.configuration.PayloadProfile;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.plugin.notification.BackendNotificationServiceTest;
import eu.domibus.core.pmode.provider.PModeProvider;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit5.JMockitExtension;
import org.hibernate.validator.internal.util.CollectionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static eu.domibus.api.model.Property.MIME_TYPE;
import static eu.domibus.messaging.MessageConstants.COMPRESSION_PROPERTY_KEY;
import static eu.domibus.messaging.MessageConstants.COMPRESSION_PROPERTY_VALUE;

@SuppressWarnings("ResultOfMethodCallIgnored")
@ExtendWith(JMockitExtension.class)
class PayloadProfileValidatorTest {
    private static final String MIME_TYPE_VALUE = "gzip";
    private static final String PMODE_KEY = "pmodeKey";
    private static final String PART_HREF = "cid:message";

    @Tested
    PayloadProfileValidator payloadProfileValidator;

    @Injectable
    PModeProvider pModeProvider;

    UserMessage userMessage;


    @BeforeEach
    void setUp() {
        userMessage = new UserMessage();
        userMessage.setMessageId("messageId");
    }

    private PartInfo getPartInfo(String href, PartProperty partProperty) {
        PartInfo partInfo = new PartInfo();
        if (partProperty != null) {
            partInfo.setPartProperties(Collections.singleton(partProperty));
        }
        partInfo.setHref(href);
        partInfo.setMime(MIME_TYPE_VALUE);
        return partInfo;
    }

    private PartProperty getPartProperty(String someOtherValue) {
        PartProperty property = new PartProperty();
        property.setName(COMPRESSION_PROPERTY_KEY);
        property.setValue(someOtherValue);

        return property;
    }

    @Test
    void validateCompressPartInfoUnexpectedCompressionType() {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(null);
        }};
        PartInfo partInfo = getPartInfo(BackendNotificationServiceTest.HREF, getPartProperty("someOtherValue"));

        Assertions.assertThrows(EbMS3Exception.class,
                () -> payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY)));
    }

    @Test
    void validateCompressPartInfoMissingMimeType() {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(null);
        }};
        PartInfo partInfo = getPartInfo(BackendNotificationServiceTest.HREF, getPartProperty(COMPRESSION_PROPERTY_VALUE));

        Assertions.assertThrows(EbMS3Exception.class,
                () -> payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY)));
    }

    @Test
    void validatePayloadProfileNotMatchingCid(@Injectable PayloadProfile payloadProfile) {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);

            Payload payload = getPayload("", "");
            payloadProfile.getPayloads();
            result = Collections.singleton(payload);
        }};
        PartInfo partInfo = new PartInfo();
        PartProperty propertyCompression = new PartProperty();
        propertyCompression.setName(COMPRESSION_PROPERTY_KEY);
        propertyCompression.setValue(COMPRESSION_PROPERTY_VALUE);
        PartProperty propertyMimeType = new PartProperty();
        propertyMimeType.setName(MIME_TYPE);
        propertyMimeType.setValue(MIME_TYPE_VALUE);
        partInfo.setPartProperties(new HashSet<>(Arrays.asList(propertyCompression, propertyMimeType)));

        Assertions.assertThrows(EbMS3Exception.class,
                () -> payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY)));
    }

    @Test
    void validatePayloadProfileWithoutPayloads_null(@Injectable PayloadProfile payloadProfile) throws EbMS3Exception {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);

            payloadProfile.getPayloads();
            result = null;
        }};


        payloadProfileValidator.validate(userMessage, new ArrayList<>(), pModeProvider.getLegConfiguration(PMODE_KEY));
    }

    @Test
    void validatePayloadProfileWithoutPayloads_empty(@Injectable PayloadProfile payloadProfile) throws EbMS3Exception {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);

            payloadProfile.getPayloads();
            result = new HashSet<>();
        }};


        payloadProfileValidator.validate(userMessage, new ArrayList<>(), pModeProvider.getLegConfiguration(PMODE_KEY));
    }

    @Test
    void validatePayloadProfileNotMatchingMimeType(@Injectable PayloadProfile payloadProfile) {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);

            payloadProfile.getPayloads();
            Payload payload = getPayload("", "otherMimeType");
            result = Collections.singleton(payload);
        }};
        PartInfo partInfo = buildPartInfo();

        Assertions.assertThrows(EbMS3Exception.class,
                () -> payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY)));
    }

    private LegConfiguration getLegConfiguration(PayloadProfile payloadProfile) {
        LegConfiguration legConfiguration = new LegConfiguration();
        legConfiguration.setCompressPayloads(true);
        legConfiguration.setPayloadProfile(payloadProfile);
        return legConfiguration;
    }

    private PartInfo buildPartInfo() {
        return buildPartInfo(PayloadProfileValidatorTest.PART_HREF);
    }

    private PartInfo buildPartInfo(String partHref) {
        PartInfo partInfo = new PartInfo();
        partInfo.setHref(partHref);
        PartProperty propertyCompression = new PartProperty();
        propertyCompression.setName(COMPRESSION_PROPERTY_KEY);
        propertyCompression.setValue(COMPRESSION_PROPERTY_VALUE);
        PartProperty propertyMimeType = new PartProperty();
        propertyMimeType.setName(MIME_TYPE);
        propertyMimeType.setValue(MIME_TYPE_VALUE);
        partInfo.setPartProperties(new HashSet<>(Arrays.asList(propertyCompression, propertyMimeType)));
        return partInfo;
    }

    @Test
    void validatePayloadProfileRequiredLegPayloadIsMissing(@Injectable PayloadProfile payloadProfile) {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);

            payloadProfile.getPayloads();
            Payload payload = getPayload("mimeTypePayload", MIME_TYPE_VALUE);
            Payload requiredPartInfo = getPayload("requiredPayload", "");
            requiredPartInfo.setRequired(true);
            result = new HashSet<>(Arrays.asList(payload, requiredPartInfo));

        }};
        PartInfo partInfo = buildPartInfo();

        Assertions.assertThrows(EbMS3Exception.class,
                () -> payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY)));
    }

    @Test
    void validatePayloadProfileNotRequiredLegPayloadIsMissing(@Injectable PayloadProfile payloadProfile) throws EbMS3Exception {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);

            payloadProfile.getPayloads();
            Payload payload = getPayload("mimeTypePayload", MIME_TYPE_VALUE);
            Payload requiredPartInfo = getPayload("requiredPayload", "");
            requiredPartInfo.setRequired(false);
            result = new HashSet<>(Arrays.asList(payload, requiredPartInfo));

        }};
        PartInfo partInfo = buildPartInfo();

        payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY));
    }

    @Test
    void validatePayloadProfileNoPayloadIsMissing(@Injectable PayloadProfile payloadProfile) throws EbMS3Exception {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);

            payloadProfile.getPayloads();
            Payload payload = getPayload("", MIME_TYPE_VALUE);
            result = Collections.singleton(payload);
        }};
        PartInfo partInfo = buildPartInfo();

        payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY));
    }

    @Test
    void validateCidRegex() throws EbMS3Exception {
        PayloadProfile payloadProfile = getPayloadProfile(getPayload("Regex Cid", MIME_TYPE_VALUE, "regex((cid):([A-Za-z0-9._-])*)"));

        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);
        }};
        PartInfo partInfo = buildPartInfo("cid:test");

        payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY));
    }

    @Test
    void validateCidMimeTypeRegex() throws EbMS3Exception {
        PayloadProfile payloadProfile = getPayloadProfile(getPayload("Regex Cid", "regex(.*)", "regex((cid):([A-Za-z0-9._-])*)"));

        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);
        }};
        PartInfo partInfo = buildPartInfo("cid:test");

        payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY));
    }

    private PayloadProfile getPayloadProfile(Payload... payloads) {
        PayloadProfile payloadProfile = new PayloadProfile();
        ReflectionTestUtils.setField(payloadProfile, "payloads", CollectionHelper.asSet(payloads));
        return payloadProfile;
    }

    @Test
    void invalidateCidRegex(@Injectable PayloadProfile payloadProfile) {
        new Expectations() {{
            pModeProvider.getLegConfiguration(PMODE_KEY);
            result = getLegConfiguration(payloadProfile);

            payloadProfile.getPayloads();
            Payload payload = getPayload("", MIME_TYPE_VALUE, "(noMatch):([A-Za-z0-9._-])*");
            result = Collections.singleton(payload);
        }};
        PartInfo partInfo = buildPartInfo("cid:test");

        Assertions.assertThrows(EbMS3Exception.class,
                () -> payloadProfileValidator.validate(userMessage, Collections.singletonList(partInfo), pModeProvider.getLegConfiguration(PMODE_KEY)));
    }

    private Payload getPayload(String name, String mimeType) {
        return getPayload(name, mimeType, PART_HREF);
    }

    private Payload getPayload(String name, String mimeType, String cid) {
        Payload payload = new Payload();
        payload.setName(name);
        payload.setCid(cid);
        payload.setMimeType(mimeType);
        return payload;
    }

}
