package eu.domibus.core.payload.persistence;

import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.payload.encryption.PayloadEncryptionService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.common.model.configuration.LegConfiguration;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Catalin Enache
 * @since 4.2
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
@ExtendWith(JMockitExtension.class)
class PayloadPersistenceHelperTest {

    @Tested
    PayloadPersistenceHelper payloadPersistenceHelper;

    @Injectable
    protected DomainContextProvider domainContextProvider;

    @Injectable
    protected DomibusConfigurationService domibusConfigurationService;

    @Injectable
    protected PayloadEncryptionService payloadEncryptionService;

    @Test
    void testValidatePayloadSize_PayloadSizeGreater_noMaxSize(
            final @Injectable LegConfiguration legConfiguration) {
        final int partInfoLength = 100000;
        final String payloadProfileName = "testProfile";
        new Expectations() {{
            legConfiguration.getPayloadProfile().getName();
            result = payloadProfileName;

            legConfiguration.getPayloadProfile().getMaxSize();
            result = null;
        }};

        payloadPersistenceHelper.validatePayloadSize(legConfiguration, partInfoLength, false);

    }

    @Test
    void testValidatePayloadSize_PayloadSizeGreater_ExpectedException(
            final @Injectable LegConfiguration legConfiguration) {
        final int partInfoLength = 100000;
        final long payloadProfileMaxSize = 40L;
        final String payloadProfileName = "testProfile";
        new Expectations() {{
            legConfiguration.getPayloadProfile().getName();
            result = payloadProfileName;

            legConfiguration.getPayloadProfile().getMaxSize();
            result = payloadProfileMaxSize;
        }};

        InvalidPayloadSizeException e = assertThrows(InvalidPayloadSizeException.class,
                () -> payloadPersistenceHelper.validatePayloadSize(legConfiguration, partInfoLength, false));
        Assertions.assertEquals("[DOM_007]:Payload size [" + partInfoLength + " B] is greater than the maximum value defined [" + payloadProfileMaxSize + " kB] for profile [" + payloadProfileName + "]",
                e.getMessage());
    }
}
