package eu.domibus.core.pmode.validation.validators;

import eu.domibus.api.pmode.ValidationIssue;
import eu.domibus.common.model.configuration.Attachment;
import eu.domibus.common.model.configuration.Configuration;
import eu.domibus.common.model.configuration.Payload;
import eu.domibus.common.model.configuration.PayloadProfile;
import eu.domibus.core.pmode.validation.PModeValidationHelper;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.*;

/**
 * @author Catalin Enache
 * @since 4.2
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
@ExtendWith(JMockitExtension.class)
class PayloadProfilesValidatorTest {

    @Tested
    PayloadProfilesValidator payloadProfilesValidator;

    @Injectable
    PModeValidationHelper pModeValidationHelper;

    @Test
    void test_validate(final @Injectable Configuration configuration) {

        PayloadProfile payloadProfile = getPayloadProfile(20L);

        final Set<PayloadProfile> payloadProfileList = Collections.singleton(payloadProfile);

        new Expectations() {{
            configuration.getBusinessProcesses().getPayloadProfiles();
            result = payloadProfileList;

            configuration.getBusinessProcesses().getPayloads();
            result = new HashSet<>();
        }};

        List<ValidationIssue> validate = payloadProfilesValidator.validate(configuration);
        Assertions.assertEquals(0, validate.size());

        new FullVerifications() {{
            payloadProfilesValidator.validatePayloadProfile(payloadProfile, (Set<Payload>) any);
        }};
    }

    @Test
    void test_validateEmptyPayloadProfile(final @Injectable Configuration configuration) {


        new Expectations(payloadProfilesValidator) {{
            configuration.getBusinessProcesses().getPayloadProfiles();
            result = null;

            configuration.getBusinessProcesses().getPayloads();
            result = new HashSet<>();
        }};


        List<ValidationIssue> validate = payloadProfilesValidator.validate(configuration);
        Assertions.assertEquals(0, validate.size());

        new FullVerifications() {
        };
    }

    @Test
    void test_validatePayloadProfile() {
        final List<Attachment> attachmentList = new ArrayList<>();
        PayloadProfile payloadProfile = getPayloadProfile(400L);

        new Expectations() {{
            pModeValidationHelper.getAttributeValue(payloadProfile, "attachment", List.class);
            result = attachmentList;
        }};

        List<ValidationIssue> validationIssues = payloadProfilesValidator.validatePayloadProfile(payloadProfile, new HashSet<>());
        Assertions.assertEquals(0, validationIssues.size());

        new FullVerifications() {
        };
    }

    @Test
    void test_validatePayloadProfile_MaxSizeNegative() {
        PayloadProfile payloadProfile = getPayloadProfile(-20L);

        new Expectations(payloadProfilesValidator) {{
            pModeValidationHelper.getAttributeValue(payloadProfile, "attachment", List.class);
            result = Collections.singletonList(getAttachment("businessContentAttachment"));
        }};

        List<ValidationIssue> validationIssues = payloadProfilesValidator.validatePayloadProfile(payloadProfile, Collections.singleton(getPayload("test payload")));
        Assertions.assertEquals(1, validationIssues.size());

        new FullVerifications(payloadProfilesValidator) {{
            pModeValidationHelper.createValidationIssue(anyString, anyString, anyString, anyString);
        }};
    }

    @Test
    void test_createIssue() {
        final String message = "message";
        final String name = "name";
        PayloadProfile payloadProfile = getPayloadProfile(20L);

        //tested method
        payloadProfilesValidator.createIssue(payloadProfile, name, message);

        new FullVerifications() {{
            pModeValidationHelper.createValidationIssue(message, name, payloadProfile.getName());
        }};
    }

    @Test
    void test_validatePayloadProfileCaseInsensitive() {

        List<Attachment> attachmentList = new ArrayList<>();
        attachmentList.add(getAttachment("businessContentAttachment"));
        Set<Payload> validPayloads = new HashSet<>();
        validPayloads.add(getPayload("BusinessContentAttachment"));
        PayloadProfile payloadProfile = getPayloadProfile(20L);

        new Expectations() {{
            pModeValidationHelper.getAttributeValue(payloadProfile, "attachment", List.class);
            result = attachmentList;
        }};

        List<ValidationIssue> validationIssues = payloadProfilesValidator.validatePayloadProfile(payloadProfile, validPayloads);

        Assertions.assertEquals(0, validationIssues.size());

        new FullVerifications() {
        };
    }

    @Test
    void test_validatePayloadProfileCaseRegex_ok() {

        List<Attachment> attachmentList = new ArrayList<>();
        attachmentList.add(getAttachment("businessContentAttachment"));
        attachmentList.add(getAttachment("message"));
        Set<Payload> validPayloads = new HashSet<>();
        validPayloads.add(getPayload("BusinessContentAttachment", "cid", "mimetype"));
        validPayloads.add(getPayload("message", "regex(.*)", "regex(.*)"));
        PayloadProfile payloadProfile = getPayloadProfile(20L);

        new Expectations() {{
            pModeValidationHelper.getAttributeValue(payloadProfile, "attachment", List.class);
            result = attachmentList;
        }};

        List<ValidationIssue> validationIssues = payloadProfilesValidator.validatePayloadProfile(payloadProfile, validPayloads);

        Assertions.assertEquals(0, validationIssues.size());
        new FullVerifications() {
        };
    }

    @Test
    void test_validatePayloadProfileCaseRegex_nok() {

        List<Attachment> attachmentList = new ArrayList<>();
        attachmentList.add(getAttachment("businessContentAttachment"));
        attachmentList.add(getAttachment("message"));
        attachmentList.add(getAttachment("message2"));
        Set<Payload> validPayloads = new HashSet<>();
        validPayloads.add(getPayload("BusinessContentAttachment", "cid", "mimetype"));
        validPayloads.add(getPayload("message", "regex(.*)", "regex(.*)"));
        validPayloads.add(getPayload("message2", "regex(.*)", "regex(.*)"));
        PayloadProfile payloadProfile = getPayloadProfile(20L);

        new Expectations() {{
            pModeValidationHelper.getAttributeValue(payloadProfile, "attachment", List.class);
            result = attachmentList;

            pModeValidationHelper.createValidationIssue(anyString, anyString, anyString);
            times = 2;
        }};

        List<ValidationIssue> validationIssues = payloadProfilesValidator.validatePayloadProfile(payloadProfile, validPayloads);

        Assertions.assertEquals(2, validationIssues.size());

        new FullVerifications() {
        };
    }

    private PayloadProfile getPayloadProfile(Long value) {
        PayloadProfile payloadProfile = new PayloadProfile();
        payloadProfile.setMaxSize(value);
        return payloadProfile;
    }

    private Attachment getAttachment(String attachment1) {
        Attachment attachment = new Attachment();
        attachment.setName(attachment1);
        return attachment;
    }

    private Payload getPayload(String businessContentAttachment) {
        return getPayload(businessContentAttachment, "cid", "mimetype");
    }

    private Payload getPayload(String businessContentAttachment, String cid, String mimeType) {
        Payload payload = new Payload();
        payload.setName(businessContentAttachment);
        payload.setCid(cid);
        payload.setMimeType(mimeType);
        return payload;
    }
}
