package eu.domibus.core.ebms3.receiver.handler;

import com.codahale.metrics.MetricRegistry;
import eu.domibus.api.ebms3.model.Ebms3MessagingWithSecurityProfileMetadata;
import eu.domibus.api.message.validation.UserMessageValidatorSpiService;
import eu.domibus.api.model.ProcessingType;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.SecurityProfileProvider;
import eu.domibus.api.pki.SecurityProfileService;
import eu.domibus.api.pmode.PModeConstants;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.util.TsidUtil;
import eu.domibus.common.ErrorResult;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.SoapElementsExtractorUtilImpl;
import eu.domibus.core.ebms3.mapper.Ebms3Converter;
import eu.domibus.core.ebms3.ws.attachment.AttachmentCleanupService;
import eu.domibus.core.message.TestMessageValidator;
import eu.domibus.core.message.UserMessageErrorCreator;
import eu.domibus.core.message.UserMessageHandlerService;
import eu.domibus.core.message.UserMessagePayloadService;
import eu.domibus.core.message.dictionary.MshRoleDao;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.security.AuthorizationServiceImpl;
import eu.domibus.core.util.MessageUtil;
import eu.domibus.core.util.SoapUtil;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import javax.xml.ws.WebServiceException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * @author Cosmin Baciu
 * @since 4.1
 */

@SuppressWarnings("ResultOfMethodCallIgnored")
@ExtendWith(JMockitExtension.class)
class IncomingUserMessageHandlerTest {

    @Tested
    IncomingUserMessageHandler incomingUserMessageHandler;

    @Injectable
    TsidUtil tsidUtil;

    @Injectable
    UserMessagePayloadService userMessagePayloadService;

    @Injectable
    BackendNotificationService backendNotificationService;

    @Injectable
    UserMessageHandlerService userMessageHandlerService;

    @Injectable
    MessageUtil messageUtil;

    @Injectable
    PModeProvider pModeProvider;

    @Injectable
    Ebms3Converter ebms3Converter;

    @Injectable
    SOAPMessage soapRequestMessage;

    @Injectable
    SOAPMessage soapResponseMessage;

    @Injectable
    AttachmentCleanupService attachmentCleanupService;

    @Injectable
    AuthorizationServiceImpl authorizationService;

    @Injectable
    UserMessageValidatorSpiService userMessageValidatorSpiService;

    @Injectable
    TestMessageValidator testMessageValidator;

    @Injectable
    UserMessageErrorCreator userMessageErrorCreator;

    @Injectable
    MshRoleDao mshRoleDao;

    @Injectable
    MetricRegistry metricRegistry;

    @Injectable
    SoapUtil soapUtil;

    @Injectable
    SoapElementsExtractorUtilImpl soapElementsExtractorUtil;

    @Injectable
    SecurityProfileService securityProfileService;

    @Injectable
    DomibusPropertyProvider domibusPropertyProvider;

    @Injectable
    DomainContextProvider domainContextProvider;

    @Injectable
    SecurityProfileProvider securityProfileProvider;

    /**
     * Happy flow unit testing with actual data
     */
    @Test
    void testInvoke_tc1Process_HappyFlow(@Injectable Ebms3MessagingWithSecurityProfileMetadata messaging,
                                         @Injectable final UserMessage userMessage) throws Exception {

        final String pmodeKey = "blue_gw:red_gw:testService1:tc1Action:OAE:pushTestcase1tc1Action";

        new Expectations() {{
            soapRequestMessage.getProperty(PModeConstants.PMODE_KEY_CONTEXT_PROPERTY);
            result = pmodeKey;

            userMessageHandlerService.handleNewUserMessage(withEqual(pmodeKey), withEqual(ProcessingType.PUSH), withEqual(soapRequestMessage), withEqual(userMessage), null, null);
            result = soapResponseMessage;
        }};

        incomingUserMessageHandler.processMessage(soapRequestMessage, messaging);

        new Verifications() {{
            backendNotificationService.notifyMessageReceivedFailure(userMessage, (ErrorResult) any, anyBoolean);
            times = 0;
        }};
    }


    /**
     * Unit testing with actual data.
     */
    @Test
    void testInvoke_ErrorInNotifyingIncomingMessage(@Injectable final Ebms3MessagingWithSecurityProfileMetadata messaging,
                                                    @Injectable final UserMessage userMessage) throws EbMS3Exception, JAXBException, SOAPException, IOException, TransformerException {

        final String pmodeKey = "blue_gw:red_gw:testService1:tc1Action:OAE:pushTestcase1tc1Action";

        new Expectations() {{
            userMessageHandlerService.handleNewUserMessage(withAny(pmodeKey), withAny(ProcessingType.PUSH), withAny(soapRequestMessage), withAny(userMessage), null, null);
            result = EbMS3ExceptionBuilder.getInstance().build();
        }};

        try {
            incomingUserMessageHandler.processMessage(soapRequestMessage, messaging);
            fail();
        } catch (WebServiceException e) {
            //OK
        }
    }
}
