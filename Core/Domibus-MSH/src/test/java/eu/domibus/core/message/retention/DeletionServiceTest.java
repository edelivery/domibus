package eu.domibus.core.message.retention;

import eu.domibus.api.model.MessageStatus;
import eu.domibus.api.model.PartInfoFileNameSpiDto;
import eu.domibus.api.model.UserMessageLog;
import eu.domibus.api.model.UserMessageLogDto;
import eu.domibus.api.payload.PartInfoService;
import eu.domibus.core.error.ErrorLogService;
import eu.domibus.core.message.ReceiptDao;
import eu.domibus.core.message.UserMessageDao;
import eu.domibus.core.message.UserMessageLogDefaultService;
import eu.domibus.core.message.acknowledge.MessageAcknowledgementDao;
import eu.domibus.core.message.attempt.MessageAttemptDao;
import eu.domibus.core.message.nonrepudiation.SignalMessageRawEnvelopeDao;
import eu.domibus.core.message.nonrepudiation.UserMessageRawEnvelopeDao;
import eu.domibus.core.message.signal.SignalMessageDao;
import eu.domibus.core.message.signal.SignalMessageLogDao;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit5.JMockitExtension;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static eu.domibus.core.message.UserMessageDefaultService.BATCH_SIZE;

/**
 * @author François Gautier
 * @since 5.2
 */
@ExtendWith(JMockitExtension.class)
class DeletionServiceTest {


    public static final String MESSAGE_ID = "1000";

    @Tested
    UserMessageDeletionService deletionService;

    @Injectable
    private SignalMessageLogDao signalMessageLogDao;

    @Injectable
    private SignalMessageDao signalMessageDao;

    @Injectable
    private BackendNotificationService backendNotificationService;

    @Injectable
    protected PartInfoService partInfoService;

    @Injectable
    private MessageAttemptDao messageAttemptDao;

    @Injectable
    private ErrorLogService errorLogService;

    @Injectable
    private MessageAcknowledgementDao messageAcknowledgementDao;

    @Injectable
    UserMessageDao userMessageDao;

    @Injectable
    private SignalMessageRawEnvelopeDao signalMessageRawEnvelopeDao;

    @Injectable
    private UserMessageRawEnvelopeDao userMessageRawEnvelopeDao;

    @Injectable
    private ReceiptDao receiptDao;

    @Injectable
    UserMessageLogDefaultService userMessageLogDefaultService;

    @Injectable
    EntityManager em;

    @Test
    public void testDeleteMessages(@Injectable UserMessageLogDto uml1,
                                   @Injectable UserMessageLogDto uml2,
                                   @Injectable Session session,
                                   @Injectable UserMessageLog userMessageLog) {
        List<UserMessageLogDto> userMessageLogDtos = Arrays.asList(uml1, uml2);
        List<PartInfoFileNameSpiDto> filenames = new ArrayList<>();
        filenames.add(new PartInfoFileNameSpiDto("file1", null));
        new Expectations() {{
            uml1.getEntityId();
            result = 1L;
            uml2.getEntityId();
            result = 2L;
            em.unwrap(Session.class);
            result = session;

            partInfoService.findFileSystemPayloadFilenames((List<Long>) any);
            result = filenames;

            userMessageLogDefaultService.deleteMessageLogs((List<Long>) any);
            result = 1;
            signalMessageLogDao.deleteMessageLogs((List<Long>) any);
            result = 1;
            signalMessageRawEnvelopeDao.deleteMessages((List<Long>) any);
            result = 1;
            receiptDao.deleteReceipts((List<Long>) any);
            result = 1;
            signalMessageDao.deleteMessages((List<Long>) any);
            result = 1;
            userMessageRawEnvelopeDao.deleteMessages((List<Long>) any);
            result = 1;
            messageAttemptDao.deleteAttemptsByMessageIds((List<Long>) any);
            result = 1;

            errorLogService.deleteErrorLogsByMessageIdInError((List<Long>) any);
            result = 1;

            messageAcknowledgementDao.deleteMessageAcknowledgementsByMessageIds((List<Long>) any);
            result = 1;

            userMessageLog.getAsyncNotificationOverride();
            result = true;

            userMessageDao.deleteMessages((List<Long>) any);
            result = 1;
        }};

        deletionService.deleteMessages(userMessageLogDtos);

        new FullVerifications() {{
            session.setJdbcBatchSize(BATCH_SIZE);

            userMessageLogDefaultService.findByEntityId(1L);
            userMessageLogDefaultService.findByEntityId(2L);
            backendNotificationService.notifyOfMessageStatusChange((UserMessageLog) any, MessageStatus.DELETED, (Timestamp) any, null);
            backendNotificationService.notifyMessageDeleted((List<UserMessageLogDto>) any);

            partInfoService.deletePayloadFiles(filenames);

            em.flush();
            times = 2;
        }};
    }
}
