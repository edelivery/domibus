package eu.domibus.core.property;

import mockit.*;
import mockit.integration.junit5.JMockitExtension;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.servlet.ServletContext;

/**
 * @author Cosmin Baciu
 * @since 4.2
 */
@Disabled
@ExtendWith(JMockitExtension.class)
public class DomibusConfigLocationProviderTest {

    @Tested
    DomibusConfigLocationProvider domibusConfigLocationProvider;

    @Test
    public void getDomibusConfigLocation(@Injectable ServletContext servletContext) {
        String systemConfigLocation = "systemConfigLocation";

        new MockUp<System>() {
            @Mock
            String getProperty(String key) {
                if(StringUtils.contains(key, "domibus.config.location")) {
                    return systemConfigLocation;

                }
                return null;
            }
        };


        Assertions.assertTrue(domibusConfigLocationProvider.getDomibusConfigLocation().contains(systemConfigLocation));
    }
}
