package eu.domibus.core.spring;

import mockit.*;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.env.PropertySource;

import java.security.Provider;
import java.security.Security;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author G. Maier
 * @since 5.2
 */
@ExtendWith(JMockitExtension.class)
public class HsmInitializerTest {
    @Injectable
    private PropertySource propertySource;
    @Tested
    private HsmInitializer hsmInitializer;

    private class ProviderMock extends Provider {
        private boolean configured;

        protected ProviderMock() {
            super("", "", "");
            configured = false;
        }

        @Override
        public Provider configure(String configArg) {
            configured = true;
            return this;
        }

        @Override
        public boolean isConfigured() {
            return configured;
        }
    }

    @Test
    public void registerShouldAddAndConfigureANewProvider(@Mocked Security security) {
        Provider provider = new ProviderMock();
        new Expectations() {{
            Security.getProvider(withEqual("AbcProvider"));
            result = null;
            Security.getProvider(withEqual(HsmInitializer.SUN_PKCS_11));
            result = provider;
            Security.addProvider(provider);
            result = 1;
        }};
        assertFalse(provider.isConfigured());

        Provider actualProvider = hsmInitializer.register("AbcProvider", "some/path");

        new Verifications() {{
           assertEquals(provider, actualProvider);
           assertTrue(actualProvider.isConfigured());
        }};
    }
}
