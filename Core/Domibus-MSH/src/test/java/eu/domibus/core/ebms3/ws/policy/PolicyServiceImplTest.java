package eu.domibus.core.ebms3.ws.policy;

import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.common.model.configuration.Security;
import eu.domibus.core.cxf.DomibusBus;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.core.property.DomibusConfigLocationProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import mockit.*;
import mockit.integration.junit5.JMockitExtension;
import org.apache.cxf.ws.policy.PolicyBuilder;
import org.apache.cxf.ws.policy.PolicyBuilderImpl;
import org.apache.neethi.Policy;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Arun Raj
 * @since 3.3
 */
@ExtendWith(JMockitExtension.class)
public class PolicyServiceImplTest {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PolicyServiceImplTest.class);
    private static final String TEST_RESOURCES_DIR = "src/test/resources/policy";

    @Injectable
    DomibusConfigurationService domibusConfigurationService;

    @Injectable
    DomibusBus busCore;

    @Tested
    PolicyServiceImpl policyService;

    @Test
    public void testIsNoSecurityPolicy_NullPolicy() {
        //when null policy is specified
        final SecurityProfileConfiguration securityProfileConfiguration = new SecurityProfileConfiguration();
        securityProfileConfiguration.encryptionEnabled(false);
        securityProfileConfiguration.signatureEnabled(false);
        boolean result1 = policyService.isNoSecurityPolicy(securityProfileConfiguration);
        assertTrue(result1);
    }
}
