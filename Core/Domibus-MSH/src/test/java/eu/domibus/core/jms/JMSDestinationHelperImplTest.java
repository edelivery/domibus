package eu.domibus.core.jms;

import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.util.RegexUtil;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @author Cosmin Baciu
 * @since 3.2
 */
@ExtendWith(JMockitExtension.class)
public class JMSDestinationHelperImplTest {

    @Injectable
    RegexUtil regexUtil;

    @Injectable
    DomibusPropertyProvider domibusPropertyProvider;

    @Tested
    JMSDestinationHelperImpl jmsDestinationHelper;

    @Test
    public void testIsInternalWithNoInternalExpressionDefined() {
        String queueName = "myQueue";
        new Expectations() {{
            domibusPropertyProvider.getProperty(anyString);
            result = null;
        }};

        boolean internal = jmsDestinationHelper.isInternal(queueName);
        Assertions.assertFalse(internal);
    }

    @Test
    public void testIsInternal() {
        final String queueName = "myQueue";
        new Expectations() {{
            domibusPropertyProvider.getProperty(anyString);
            result = "myexpression";

            regexUtil.matches("myexpression", queueName);
            result = true;
        }};

        boolean internal = jmsDestinationHelper.isInternal(queueName);
        Assertions.assertTrue(internal);

    }

    @Test
    public void testWildflyQueueNameIsInternal() {
        final String queueName = "jms.queue.DomibusAlertMessageQueue";
        final String internalQueueExpression = ".*jms.queue.(Domibus[a-zA-Z]|DLQ|ExpiryQueue|internal|backend.jms|notification.jms|notification.webservice|notification.sutController|notification.filesystem).*";
        new Expectations() {{
            domibusPropertyProvider.getProperty(anyString);
            result = internalQueueExpression;

            regexUtil.matches(internalQueueExpression, queueName);
            result = true;
        }};

        boolean internal = jmsDestinationHelper.isInternal(queueName);
        Assertions.assertTrue(internal);
    }

    @Test
    public void testWeblogicQueueNameIsInternal() {
        final String queueName = "domibus.backend.jms.inQueue ";
        final String internalQueueExpression = ".*domibus.(internal|DLQ|backend|.jms|notification|.jms|notification|.webservice|notification|.sutController|notification|.filesystem).*";
        new Expectations() {{
            domibusPropertyProvider.getProperty(anyString);
            result = internalQueueExpression;

            regexUtil.matches(internalQueueExpression, queueName);
            result = true;
        }};

        boolean internal = jmsDestinationHelper.isInternal(queueName);
        Assertions.assertTrue(internal);
    }

    @Test
    public void testTomcatQueueNameIsInternal() {
        final String queueName = "domibus.backend.jms.outQueue";
        final String internalQueueExpression = ".*domibus.(internal|DLQ|backend|.jms|notification|.jms|notification|.webservice|notification|.sutController|notification|.filesystem).*";
        new Expectations() {{
            domibusPropertyProvider.getProperty(anyString);
            result = internalQueueExpression;

            regexUtil.matches(internalQueueExpression, queueName);
            result = true;
        }};

        boolean internal = jmsDestinationHelper.isInternal(queueName);
        Assertions.assertTrue(internal);
    }
}
