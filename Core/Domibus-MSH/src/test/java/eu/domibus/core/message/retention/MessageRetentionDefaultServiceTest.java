package eu.domibus.core.message.retention;

import eu.domibus.api.jms.JMSManager;
import eu.domibus.api.model.DatabasePartition;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.payload.PartInfoService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.util.DatabaseUtil;
import eu.domibus.api.util.DbSchemaUtil;
import eu.domibus.core.message.*;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import eu.domibus.core.pmode.provider.PModeProvider;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.jms.Queue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static eu.domibus.core.message.retention.PartitionService.DEFAULT_PARTITION;
import static eu.domibus.jms.spi.InternalJMSConstants.RETENTION_MESSAGE_QUEUE;

/**
 * @author soumya chandran
 * @since 5.2
 */
@ExtendWith(JMockitExtension.class)
public class MessageRetentionDefaultServiceTest {
    public static final DatabasePartition DB_PARTITION_DEFAULT = new DatabasePartition(DEFAULT_PARTITION, 220000000000000000L);
    public static final DatabasePartition DB_PARTITION_MESSAGES_BEFORE_PARTIONING = new DatabasePartition("P123", 230701090000000000L);
    public static final DatabasePartition DB_PARTITION_UNTIL_NOW_MINUS_1H = new DatabasePartition("SYS_P111", 230702080000000000L);

    @Tested
    MessageRetentionDefaultService messageRetentionDefaultService;
    @Injectable
    PModeProvider pModeProvider;

    @Injectable
    UserMessageDao userMessageDao;
    @Injectable
    PartitionService partitionService;
    @Injectable
    DomibusConfigurationService domibusConfigurationService;
    @Injectable
    MessageRetentionPartitionsService retentionPartitionsService;
    @Injectable
    DomibusPropertyProvider domibusPropertyProvider;
    @Injectable
    UserMessageLogDao userMessageLogDao;
    @Injectable
    JMSManager jmsManager;
    @Injectable
    @Qualifier(RETENTION_MESSAGE_QUEUE)
    Queue retentionMessageQueue;
    @Injectable
    UserMessageDeletionService deletionService;
    @Injectable
    UserMessageServiceHelper userMessageServiceHelper;
    @Injectable
    UserMessageDefaultServiceHelper userMessageDefaultServiceHelper;
    @Injectable
    PartInfoService partInfoService;
    @Injectable
    BackendNotificationService backendNotificationService;
    @Injectable
    DbSchemaUtil dbSchemaUtil;
    @Injectable
    DomainContextProvider domainContextProvider;
    @Injectable
    DatabaseUtil databaseUtil;

    @Test
    public void deleteExpiredMessagesTest_WithPartitionedDB() {
        List<String> mpcs = new ArrayList<>();
        mpcs.add("mpc1");
        mpcs.add("mpc2");

        List<DatabasePartition> allPartitionNames = Arrays.asList(
                DB_PARTITION_DEFAULT,
                DB_PARTITION_MESSAGES_BEFORE_PARTIONING,
                DB_PARTITION_UNTIL_NOW_MINUS_1H
        );

        List<DatabasePartition> expiredPartitionNames = new ArrayList<>();
        expiredPartitionNames.add(new DatabasePartition("P123", 1L));
        expiredPartitionNames.add(new DatabasePartition("SYS_P111", 2L));

        List<String> emptyPartitions = new ArrayList<>();
        emptyPartitions.add("P123");

        new Expectations(messageRetentionDefaultService) {{
            pModeProvider.getRetentionDownloadedByMpcURI("mpc1");
            result = 1200;

            pModeProvider.getRetentionDownloadedByMpcURI("mpc2");
            result = 1440;

            pModeProvider.getMpcURIList();
            result = mpcs;

            databaseUtil.isPartitionable();
            result = true;

            retentionPartitionsService.getAllPartitionNames();
            result = allPartitionNames;

            retentionPartitionsService.getExpiredPartitionNames(allPartitionNames);
            result = expiredPartitionNames;

            messageRetentionDefaultService.getEmptyPartitionNames(expiredPartitionNames.stream().map(DatabasePartition::getPartitionName).collect(Collectors.toList()));
            result = emptyPartitions;
        }};

        messageRetentionDefaultService.deleteExpiredMessages();

        new Verifications() {{
            userMessageDao.dropPartitions("P123");
            times = 1;
        }};
    }

    @Test
    public void deleteExpiredMessagesTest_WithOutPartitionedDB() {
        List<String> mpcs = new ArrayList<>();
        mpcs.add("mpc1");
        mpcs.add("mpc2");

        List<DatabasePartition> allPartitionNames = new ArrayList<>();

        new Expectations(messageRetentionDefaultService) {{
            pModeProvider.getRetentionDownloadedByMpcURI("mpc1");
            result = 1200;

            pModeProvider.getRetentionDownloadedByMpcURI("mpc2");
            result = 1440;

            pModeProvider.getMpcURIList();
            result = mpcs;

            databaseUtil.isPartitionable();
            result = true;

            retentionPartitionsService.getAllPartitionNames();
            result = allPartitionNames;
        }};

        messageRetentionDefaultService.deleteExpiredMessages();

        new Verifications() {{
            userMessageDao.dropPartitions(anyString);
            times = 0;
        }};
    }
}
