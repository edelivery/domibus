package eu.domibus.core.crypto;

import eu.domibus.api.cache.DomibusLocalCacheService;
import eu.domibus.api.cluster.SignalService;
import eu.domibus.api.crypto.NoKeyStoreContentInformationException;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.pki.CertificateService;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.TrustStoreEntry;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.core.property.DomibusRawPropertyProvider;
import eu.domibus.core.util.TLSMshTrustManager;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit5.JMockitExtension;
import org.apache.cxf.configuration.security.KeyStoreType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Ion Perpegel
 * @since 5.0
 */
@ExtendWith(JMockitExtension.class)
class TLSMshCertificateManagerImplTest {

    @Tested
    TLSMshCertificateManagerImpl tlsMshCertificateManager;

    @Injectable
    private CertificateService certificateService;

    @Injectable
    DomibusLocalCacheService domibusLocalCacheService;

    @Injectable
    DomainContextProvider domainContextProvider;

    @Injectable
    private SignalService signalService;

    @Injectable
    private DomibusConfigurationService domibusConfigurationService;

    @Injectable
    DomainService domainService;

    @Injectable
    CertificateHelper certificateHelper;

    @Injectable
    DomibusPropertyProvider domibusPropertyProvider;

    @Injectable
    DomibusRawPropertyProvider domibusRawPropertyProvider;

    @Injectable
    TLSMshCertificateManagerHelper tlsMshCertificateManagerHelper;

    @Injectable
    TLSMshTrustManager tlsMshTrustManager;

    @Injectable
    TLSMshTrustStorePersistenceInfoImpl tlsMshTruststorePersistenceInfo;

    @Injectable
    TLSMshKeyStorePersistenceInfoImpl tlsMshKeystorePersistenceInfo;

    @Test
    void replaceTrustStore(@Injectable KeystorePersistenceInfo persistenceInfo,
                                  @Injectable KeyStoreContentInfo contentInfo) {
        new Expectations() {{
            certificateService.createOrReplaceStore(contentInfo, (KeystorePersistenceInfo) any);
        }};

        tlsMshCertificateManager.replaceTrustStore(contentInfo);

        new Verifications() {{
            tlsMshCertificateManager.resetTLSTruststore();
        }};
    }

    @Test
    void getTrustStoreEntries(@Injectable KeystorePersistenceInfo persistenceInfo) {
        List<Object> entries = new ArrayList<>();

        new Expectations(tlsMshCertificateManager) {{
            tlsMshCertificateManager.getPersistenceInfo();
            result = persistenceInfo;
            certificateService.getStoreEntries(persistenceInfo);
            result = entries;
        }};

        List<TrustStoreEntry> result = tlsMshCertificateManager.getTrustStoreEntries();

        assertEquals(entries, result);
        new Verifications() {{
            certificateService.getStoreEntries(persistenceInfo);
        }};
    }

    @Test
    void getTrustStoreEntries_throwsExceptionForMissingTrustStore(@Injectable KeystorePersistenceInfo persistenceInfo) {
        new Expectations(tlsMshCertificateManager) {{
            tlsMshCertificateManager.getPersistenceInfo();
            result = persistenceInfo;

            certificateService.getStoreEntries(persistenceInfo);
            result = new NoKeyStoreContentInformationException("");
        }};

        assertEquals(new ArrayList<>(), tlsMshCertificateManager.getTrustStoreEntries());
    }

    @Test
    void addCertificate(@Injectable KeyStoreType trustStore, @Injectable byte[] certificateData, @Injectable KeystorePersistenceInfo persistenceInfo) {
        String alias = "mockalias";

        new Expectations(tlsMshCertificateManager) {{
            tlsMshCertificateManager.getPersistenceInfo();
            result = persistenceInfo;
            certificateService.addCertificate(persistenceInfo, certificateData, alias, true);
            result = true;
        }};

        boolean result = tlsMshCertificateManager.addCertificate(certificateData, alias);

        Assertions.assertTrue(result);
        new Verifications() {{
            certificateService.addCertificate(persistenceInfo, certificateData, alias, true);
            tlsMshCertificateManager.resetTLSTruststore();
        }};
    }

    @Test
    void removeCertificate(@Injectable KeyStoreType trustStore, @Injectable KeystorePersistenceInfo persistenceInfo) {
        String alias = "mockalias";

        new Expectations(tlsMshCertificateManager) {{
            tlsMshCertificateManager.getPersistenceInfo();
            result = persistenceInfo;
            certificateService.removeCertificate(persistenceInfo, alias);
            result = true;
        }};

        boolean result = tlsMshCertificateManager.removeCertificate(alias);

        Assertions.assertTrue(result);
        new Verifications() {{
            certificateService.removeCertificate(persistenceInfo, alias);
            tlsMshCertificateManager.resetTLSTruststore();
        }};
    }


    @Test
    void resetCacheTLSTruststore(@Injectable Domain domain) {
        new Expectations(tlsMshCertificateManager) {{
            domainContextProvider.getCurrentDomain();
            result = domain;
        }};

        tlsMshCertificateManager.resetTLSTruststore();

        new Verifications() {{
            signalService.signalTLSTrustStoreUpdate(domain);
        }};
    }
}
