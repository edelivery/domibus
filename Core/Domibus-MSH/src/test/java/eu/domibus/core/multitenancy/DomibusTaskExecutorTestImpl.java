package eu.domibus.core.multitenancy;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.multitenancy.ExecutorExceptionHandler;
import eu.domibus.api.multitenancy.ExecutorWaitPolicy;
import eu.domibus.api.multitenancy.lock.DomibusSynchronizationException;
import org.springframework.security.core.Authentication;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * @author François Gautier
 * @version 5.1
 */
public class DomibusTaskExecutorTestImpl implements DomibusTaskExecutor {

    @Override
    public <T> T submit(Callable<T> task) {
        try {
            return task.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T submit(Callable<T> task, Domain domain) {
        try {
            return task.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T submitWithSecurityContext(Callable<T> task) {
        return submit(task);
    }

    @Override
    public <T> T submitCallable(Callable<T> task, Domain domain, Authentication authentication, ExecutorWaitPolicy waitPolicy, Long timeout, TimeUnit timeUnit, ExecutorExceptionHandler errorHandler) {
        try {
            return task.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void submit(Runnable task) {
        try {
            task.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void submitWithSecurityContext(Runnable task) {

    }


    @Override
    public void submit(Runnable task, Domain domain) {
        try {
            task.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void submit(Runnable task, Domain domain, ExecutorWaitPolicy waitPolicy, Long timeout, TimeUnit timeUnit) {
        try {
            task.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void submitLongRunningTask(Runnable task, Domain domain, Authentication authentication, ExecutorExceptionHandler errorHandler) {
        try {
            task.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <R> R executeWithLock(Callable<R> task, String dbLockKey, String javaLockKey, Domain domain, Authentication authentication, ExecutorWaitPolicy waitPolicy, Long timeout, TimeUnit timeUnit, ExecutorExceptionHandler errorHandler) {
        try {
            return task.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <R> R executeWithLock(Callable<R> task, String dbLockKey, String javaLockKey) throws DomibusSynchronizationException {
        try {
            return task.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
