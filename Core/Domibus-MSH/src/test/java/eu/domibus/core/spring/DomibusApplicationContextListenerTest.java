package eu.domibus.core.spring;

import eu.domibus.api.cluster.ClusterNodeIdentifierService;
import eu.domibus.api.crypto.TLSMshCertificateManager;
import eu.domibus.api.encryption.EncryptionService;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.pki.MultiDomainCryptoService;
import eu.domibus.api.plugin.BackendConnectorService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.core.earchive.storage.EArchiveFileStorageProvider;
import eu.domibus.core.jms.MessageListenerContainerInitializer;
import eu.domibus.core.message.dictionary.StaticDictionaryService;
import eu.domibus.core.metrics.JmsQueueCountSetScheduler;
import eu.domibus.core.payload.persistence.DomibusPayloadManagerSpiProviderImpl;
import eu.domibus.core.payload.persistence.PayloadPersistence;
import eu.domibus.core.plugin.initializer.PluginInitializerProvider;
import eu.domibus.core.plugin.routing.BackendFilterInitializerService;
import eu.domibus.core.plugin.routing.RoutingService;
import eu.domibus.core.property.DomibusPropertyValidatorService;
import eu.domibus.core.property.GatewayConfigurationValidator;
import eu.domibus.core.scheduler.DomibusQuartzStarter;
import eu.domibus.core.user.multitenancy.SuperUserManagementServiceImpl;
import eu.domibus.core.user.ui.UserManagementServiceImpl;
import mockit.*;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.xml.ws.Endpoint;

/**
 * @author Cosmin Baciu
 * @since 4.1.1
 */
@ExtendWith(JMockitExtension.class)
public class DomibusApplicationContextListenerTest {

    @Tested
    DomibusApplicationContextListener domibusApplicationContextListener;

    @Injectable
    protected BackendFilterInitializerService backendFilterInitializerService;

    @Injectable
    protected EncryptionService encryptionService;

    @Injectable
    DomibusPayloadManagerSpiProviderImpl domibusPayloadManagerSpiProvider;

    @Injectable
    protected MessageListenerContainerInitializer messageListenerContainerInitializer;

    @Injectable
    protected JmsQueueCountSetScheduler jmsQueueCountSetScheduler;

    @Injectable
    PayloadPersistence payloadPersistence;

    @Injectable
    protected RoutingService routingService;

    @Injectable
    protected DomibusQuartzStarter domibusQuartzStarter;

    @Injectable
    protected EArchiveFileStorageProvider eArchiveFileStorageProvider;

    @Injectable
    protected PluginInitializerProvider pluginInitializerProvider;

    @Injectable
    protected StaticDictionaryService staticDictionaryService;

    @Injectable
    protected DomibusTaskExecutor domainTaskExecutor;

    @Injectable
    protected DomibusConfigurationService domibusConfigurationService;

    @Injectable
    GatewayConfigurationValidator gatewayConfigurationValidator;

    @Injectable
    MultiDomainCryptoService multiDomainCryptoService;

    @Injectable
    TLSMshCertificateManager tlsMshCertificateManager;

    @Injectable
    UserManagementServiceImpl userManagementService;

    @Injectable
    DomibusPropertyValidatorService domibusPropertyValidatorService;

    @Injectable
    BackendConnectorService backendConnectorService;

    @Injectable
    SuperUserManagementServiceImpl superUserManagementService;

    @Injectable
    Endpoint mshEndpoint;

    @Injectable
    ClusterNodeIdentifierService clusterNodeIdentifierService;

    @Test
    public void onApplicationEventThatShouldBeDiscarded(@Injectable ContextRefreshedEvent event,
                                                        @Injectable ApplicationContext applicationContext) {
        new Expectations() {{
            event.getApplicationContext();
            result = applicationContext;

            applicationContext.getParent();
            result = null;
        }};

        domibusApplicationContextListener.onApplicationEvent(event);

        new FullVerifications() {{
            encryptionService.handleEncryption();
            times = 0;

            backendFilterInitializerService.updateMessageFilters();
            times = 0;
        }};
    }

    @Test
    public void onApplicationEvent(@Injectable ContextRefreshedEvent event,
                                   @Injectable ApplicationContext applicationContext,
                                   @Injectable ApplicationContext parent) {

        domibusApplicationContextListener.executeSynchronized(true);

        new Verifications() {{
            tlsMshCertificateManager.saveStoresFromDBToDisk();
            times = 1;

            multiDomainCryptoService.saveStoresFromDBToDisk();
            times = 1;

            userManagementService.createDefaultUserIfApplicable();
            times = 1;

            staticDictionaryService.createStaticDictionaryEntries();
            times = 1;

            encryptionService.handleEncryption();
            times = 1;

            backendFilterInitializerService.updateMessageFilters();
            times = 1;
        }};
    }

}
