package eu.domibus.core.crypto;

import eu.domibus.api.cluster.Command;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Ionut Breaz
 * @since 5.2
 */

public class ReloadTLSGenericTruststoreCommandTaskTest {

    @Tested
    ReloadTLSGenericTruststoreCommandTask reloadTLSGenericTruststoreCommandTask;

    @Injectable
    TLSGenericCertificateManagerHelper tlsGenericCertificateManagerHelper;

    @Test
    public void canHandle() {
        assertTrue(reloadTLSGenericTruststoreCommandTask.canHandle(Command.RELOAD_TLS_GENERIC_TRUSTSTORE));
    }

    @Test
    public void canHandleWithDifferentCommand() {
        assertFalse(reloadTLSGenericTruststoreCommandTask.canHandle("another_command"));
    }

    @Test
    public void execute() {
        reloadTLSGenericTruststoreCommandTask.execute(new HashMap<>());

        new Verifications() {{
            tlsGenericCertificateManagerHelper.resetCache();
        }};
    }
}
