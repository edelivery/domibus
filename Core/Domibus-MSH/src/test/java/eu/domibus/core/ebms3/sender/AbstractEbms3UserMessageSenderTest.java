package eu.domibus.core.ebms3.sender;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.message.SignalMessageSoapEnvelopeSpiDelegate;
import eu.domibus.api.message.UserMessageSoapEnvelopeSpiDelegate;
import eu.domibus.api.message.attempt.MessageAttempt;
import eu.domibus.api.message.attempt.MessageAttemptService;
import eu.domibus.api.model.*;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pmode.PModeService;
import eu.domibus.api.security.ChainCertificateInvalidException;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.common.ErrorCode;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.common.model.configuration.Party;
import eu.domibus.core.crypto.SecurityProfileProviderImpl;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.ebms3.ws.policy.PolicyService;
import eu.domibus.core.error.ErrorLogService;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.core.message.MessageExchangeService;
import eu.domibus.core.message.PartInfoDao;
import eu.domibus.core.message.UserMessageLogDao;
import eu.domibus.core.message.UserMessageServiceHelper;
import eu.domibus.core.message.dictionary.MshRoleDao;
import eu.domibus.core.message.nonrepudiation.NonRepudiationService;
import eu.domibus.core.message.reliability.ReliabilityChecker;
import eu.domibus.core.message.reliability.ReliabilityService;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.util.SoapUtil;
import eu.domibus.logging.DomibusLoggerFactory;
import mockit.*;
import mockit.integration.junit5.JMockitExtension;
import org.apache.neethi.Policy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.xml.soap.SOAPMessage;
import java.util.UUID;

/**
 * @author Catalin Enache
 * @since 4.1
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
@ExtendWith(JMockitExtension.class)
public class AbstractEbms3UserMessageSenderTest {

    @Tested
    AbstractUserMessageSender abstractUserMessageSender;

    @Injectable
    protected UserMessageSoapEnvelopeSpiDelegate userMessageSoapEnvelopeSpiDelegate;

    @Injectable
    SecurityProfileProviderImpl securityProfileProvider;

    @Injectable
    protected PModeProvider pModeProvider;

    @Injectable
    DomainContextProvider domainContextProvider;

    @Injectable
    protected PModeService pModeService;

    @Injectable
    protected MSHDispatcher mshDispatcher;

    @Injectable
    protected EbMS3MessageBuilder messageBuilder;

    @Injectable
    protected ReliabilityChecker reliabilityChecker;

    @Injectable
    protected ResponseHandler responseHandler;

    @Injectable
    protected MessageAttemptService messageAttemptService;

    @Injectable
    protected MessageExchangeService messageExchangeService;

    @Injectable
    protected PolicyService policyService;

    @Injectable
    protected ReliabilityService reliabilityService;

    @Injectable
    protected UserMessageLogDao userMessageLogDao;

    @Injectable
    protected ErrorLogService errorLogService;

    @Injectable
    protected MshRoleDao mshRoleDao;

    @Injectable
    protected PartInfoDao partInfoDao;

    @Injectable
    NonRepudiationService nonRepudiationService;

    @Injectable
    SoapUtil soapUtil;

    @Injectable
    protected UserMessageServiceHelper userMessageServiceHelper;

    @Injectable
    MessageSenderService messageSenderService;

    @Injectable
    protected SignalMessageSoapEnvelopeSpiDelegate signalMessageSoapEnvelopeSpiDelegate;

    private final String messageId = UUID.randomUUID().toString();

    private final String senderName = "domibus-blue";
    private final String receiverName = "domibus-red";
    private final String pModeKey = "toto";
    private final String configPolicy = "tototiti";
    static final String POLICIES = "policies/";


    @Test
    public void testSendMessage(@Injectable Messaging messaging,
                                @Injectable UserMessage userMessage,
                                @Injectable UserMessageLog userMessageLog,
                                @Injectable LegConfiguration legConfiguration,
                                @Injectable Policy policy,
                                @Injectable Party senderParty,
                                @Injectable Party receiverParty,
                                @Injectable SOAPMessage soapMessage,
                                @Injectable SOAPMessage response,
                                @Injectable ResponseResult responseResult,
                                @Injectable SecurityProfileConfiguration securityProfileConfiguration) throws Exception {

        final ReliabilityChecker.CheckResult reliabilityCheckSuccessful = ReliabilityChecker.CheckResult.SEND_FAIL;
        String messageId = "123";
        String finalRecipient = "0151:123";
        String receiverURL = "http://localhost";

        new Expectations(abstractUserMessageSender) {{
            userMessage.getMessageId();
            result = messageId;

            abstractUserMessageSender.getLog();
            result = DomibusLoggerFactory.getLogger(AbstractEbms3UserMessageSenderTest.class);

            userMessage.getMessageId();
            result = messageId;

            pModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING).getPmodeKey();
            result = pModeKey;

            pModeProvider.getLegConfiguration(pModeKey);
            result = legConfiguration;

            pModeProvider.getSenderParty(pModeKey);
            result = senderParty;

            pModeProvider.getReceiverParty(pModeKey);
            result = receiverParty;

            receiverParty.getName();
            result = receiverName;

            senderParty.getName();
            result = senderName;

            abstractUserMessageSender.createSOAPMessage(userMessage, legConfiguration);
            result = soapMessage;

            userMessageServiceHelper.getFinalRecipientValue(userMessage);
            result = finalRecipient;

            userMessageSoapEnvelopeSpiDelegate.beforeSigningAndEncryption(soapMessage);
            result = soapMessage;

            pModeService.getReceiverPartyEndpoint(receiverName, receiverParty.getEndpoint(), finalRecipient);
            result = receiverURL;

            mshDispatcher.dispatch(soapMessage, receiverURL, securityProfileConfiguration, legConfiguration, pModeKey);
            result = response;

            responseHandler.verifyResponse(response, messageId);
            result = responseResult;

            reliabilityChecker.check(soapMessage, response, responseResult, legConfiguration);
            result = reliabilityCheckSuccessful;

        }};

        //tested method
        abstractUserMessageSender.sendMessage(userMessage, userMessageLog);

        new FullVerifications(abstractUserMessageSender) {{
            LegConfiguration legConfigurationActual;
            String receiverPartyNameActual;
            SecurityProfileConfiguration capturedSecurityProfileConfiguration = null;

            messageExchangeService.verifyReceiverCertificate(capturedSecurityProfileConfiguration = withCapture(), legConfigurationActual = withCapture(), receiverPartyNameActual = withCapture());
//            Assertions.assertEquals(legConfiguration.getName(), legConfigurationActual.getName());
            Assertions.assertEquals(receiverName, receiverPartyNameActual);


            String senderPartyNameActual;

            messageExchangeService.verifySenderCertificate(capturedSecurityProfileConfiguration = withCapture(), legConfigurationActual = withCapture(), senderPartyNameActual = withCapture());
//            Assertions.assertEquals(legConfiguration.getName(), legConfigurationActual.getName());
            Assertions.assertEquals(senderName, senderPartyNameActual);

            ReliabilityChecker.CheckResult checkResultActual;

            reliabilityService.handleReliability(userMessage, userMessageLog, checkResultActual = withCapture(), null, response, responseResult, legConfiguration, null);
            Assertions.assertEquals(reliabilityCheckSuccessful, checkResultActual);

        }};
    }

    @Test
    public void testSendMessage_ChainCertificateInvalid_Exception(@Injectable final Messaging messaging,
                                                                  @Injectable final UserMessage userMessage,
                                                                  @Injectable final UserMessageLog userMessageLog,
                                                                  @Injectable final LegConfiguration legConfiguration,
                                                                  @Injectable final Party senderParty,
                                                                  @Injectable final Party receiverParty,
                                                                  @Injectable SOAPMessage response,
                                                                  @Injectable SecurityProfileConfiguration securityProfileConfiguration) throws Exception {
        final String chainExceptionMessage = "certificate invalid";
        final ChainCertificateInvalidException chainCertificateInvalidException = new ChainCertificateInvalidException(DomibusCoreErrorCode.DOM_001, chainExceptionMessage);
        final ReliabilityChecker.CheckResult reliabilityCheckSuccessful = ReliabilityChecker.CheckResult.SEND_FAIL;


        new Expectations(abstractUserMessageSender) {{

            abstractUserMessageSender.getLog();
            result = DomibusLoggerFactory.getLogger(AbstractEbms3UserMessageSenderTest.class);

            userMessage.getMessageId();
            result = messageId;

            pModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING).getPmodeKey();
            result = pModeKey;

            pModeProvider.getLegConfiguration(pModeKey);
            result = legConfiguration;

            pModeProvider.getSenderParty(pModeKey);
            result = senderParty;

            pModeProvider.getReceiverParty(pModeKey);
            result = receiverParty;

            receiverParty.getName();
            result = receiverName;

            messageExchangeService.verifyReceiverCertificate(securityProfileConfiguration, legConfiguration, receiverParty.getName());
            result = chainCertificateInvalidException;
        }};

        //tested method
        abstractUserMessageSender.sendMessage(userMessage, userMessageLog);

        new FullVerifications(abstractUserMessageSender) {{
            ReliabilityChecker.CheckResult checkResultActual;
            reliabilityService.handleReliability(userMessage, userMessageLog, checkResultActual = withCapture(), null, null, null, legConfiguration, null);
            errorLogService.createErrorLog(messageId, ErrorCode.EBMS_0004, chainCertificateInvalidException.getMessage(), MSHRole.SENDING, userMessage);
            Assertions.assertEquals(reliabilityCheckSuccessful, checkResultActual);

        }};
    }

    private UserMessage getUserMessage() {
        UserMessage userMessage = new UserMessage();
        userMessage.setMessageId(messageId);

        PartyInfo partyInfo = getPartyInfo();
        userMessage.setPartyInfo(partyInfo);
        return userMessage;
    }

    private PartyInfo getPartyInfo() {
        PartyInfo partyInfo = new PartyInfo();
        To value = new To();
        value.setToPartyId(getPartyId());
        value.setToRole(getRole());
        partyInfo.setTo(value);
        return partyInfo;
    }

    private PartyRole getRole() {
        PartyRole partyRole = new PartyRole();
        partyRole.setEntityId(156L);
        partyRole.setValue("value");
        return partyRole;
    }

    private PartyId getPartyId() {
        PartyId partyId = new PartyId();
        partyId.setType("type");
        partyId.setValue("value");
        return partyId;
    }

   /* private LegConfiguration getLegConfiguration() {
        LegConfiguration legConfiguration = new LegConfiguration();
        legConfiguration.setName("legconfiguration");
        Security security = new Security();
        security.setPolicy(configPolicy);
        security.setProfile(RSA);
        legConfiguration.setSecurity(security);
        return legConfiguration;
    }*/

    @Test
    public void testSendMessage_DispatchError_Exception(final @Injectable Messaging messaging,
                                                        @Injectable final UserMessage userMessage,
                                                        @Injectable final UserMessageLog userMessageLog,
                                                        @Injectable final LegConfiguration legConfiguration,
                                                        @Injectable final Policy policy,
                                                        @Injectable final Party senderParty,
                                                        @Injectable final Party receiverParty,
                                                        @Injectable final SOAPMessage soapMessage,
                                                        @Injectable SOAPMessage response,
                                                        @Injectable ResponseResult responseResult,
                                                        @Injectable SecurityProfileConfiguration securityProfileConfiguration) throws Exception {

        final ReliabilityChecker.CheckResult reliabilityCheckSuccessful = ReliabilityChecker.CheckResult.SEND_FAIL;

        String attemptError = "OutOfMemory mock error occurred while dispatching messages";
        String finalRecipient = "0151:123";
        String receiverURL = "http://localhost";

        new Expectations(abstractUserMessageSender) {{

            userMessage.getMessageId();
            result = messageId;

            abstractUserMessageSender.getLog();
            result = DomibusLoggerFactory.getLogger(AbstractEbms3UserMessageSenderTest.class);

            pModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING).getPmodeKey();
            result = pModeKey;

            pModeProvider.getLegConfiguration(pModeKey);
            result = legConfiguration;

//            legConfiguration.getSecurity().getPolicy();
//            result = configPolicy;

//            policyService.parsePolicy(POLICIES + legConfiguration.getSecurity().getPolicy(), legConfiguration.getSecurity().getSecurityProfile());
//            result = policy;

         /*   policyService.isNoSecurityPolicy(policy);
            result = false;*/

            pModeProvider.getSenderParty(pModeKey);
            result = senderParty;

            pModeProvider.getReceiverParty(pModeKey);
            result = receiverParty;

            receiverParty.getName();
            result = receiverName;

            senderParty.getName();
            result = senderName;

            abstractUserMessageSender.createSOAPMessage(userMessage, legConfiguration);
            result = soapMessage;

            userMessageSoapEnvelopeSpiDelegate.beforeSigningAndEncryption(soapMessage);
            result = soapMessage;

            userMessageServiceHelper.getFinalRecipientValue(userMessage);
            result = finalRecipient;

            pModeService.getReceiverPartyEndpoint(receiverParty.getName(), receiverParty.getEndpoint(), finalRecipient);
            result = receiverURL;

            mshDispatcher.dispatch(soapMessage, receiverURL, securityProfileConfiguration, legConfiguration, pModeKey);
            result = new OutOfMemoryError(attemptError);

        }};

        //tested method
        abstractUserMessageSender.sendMessage(userMessage, userMessageLog);

        new Verifications() {{
            ReliabilityChecker.CheckResult checkResultActual;
            reliabilityService.handleReliability(userMessage, userMessageLog, checkResultActual = withCapture(), null, null, null, legConfiguration, null);
            Assertions.assertEquals(reliabilityCheckSuccessful, checkResultActual);
        }};
    }
}
