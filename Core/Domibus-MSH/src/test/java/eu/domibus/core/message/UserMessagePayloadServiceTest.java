package eu.domibus.core.message;

import com.sun.xml.messaging.saaj.soap.AttachmentPartImpl;
import eu.domibus.api.ebms3.model.*;
import eu.domibus.api.model.PartInfo;
import eu.domibus.api.model.PartProperty;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.util.xml.XMLUtil;
import eu.domibus.common.ErrorCode;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.message.dictionary.PartPropertyDictionaryService;
import eu.domibus.core.util.SoapUtil;
import mockit.*;
import mockit.integration.junit5.JMockitExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.w3c.dom.Node;

import javax.activation.DataHandler;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ResultOfMethodCallIgnored")
@ExtendWith(JMockitExtension.class)
class UserMessagePayloadServiceTest {

    @Injectable
    SOAPMessage soapRequestMessage;

    @Injectable
    protected SoapUtil soapUtil;

    @Injectable
    protected XMLUtil xmlUtil;

    @Injectable
    protected PartInfoDao partInfoDao;

    @Injectable
    protected PartPropertyDictionaryService partPropertyDictionaryService;

    @Tested
    UserMessagePayloadServiceImpl userMessagePayloadService;

    @Test
    void test_HandlePayLoads_HappyFlowUsingEmptyCID(@Injectable final Ebms3Messaging ebms3Messaging,
                                                           @Injectable final PartInfo partInfo) throws SOAPException, TransformerException, EbMS3Exception {

        Ebms3PartInfo ebms3PartInfo = new Ebms3PartInfo();
        ebms3PartInfo.setHref(null);
        Ebms3Description value1 = new Ebms3Description();
        value1.setValue("description");
        value1.setLang("en");
        ebms3PartInfo.setDescription(value1);
        Ebms3Schema value = new Ebms3Schema();
        value.setLocation("location");
        value.setNamespace("namespace");
        value.setVersion("version");
        ebms3PartInfo.setSchema(value);

        Ebms3PayloadInfo ebms3PayloadInfo = new Ebms3PayloadInfo();
        ebms3PayloadInfo.getPartInfo().add(ebms3PartInfo);
        new Expectations(userMessagePayloadService) {{
            ebms3Messaging.getUserMessage().getPayloadInfo();
            result = ebms3PayloadInfo;

            ebms3Messaging.getUserMessage().getMessageInfo().getMessageId();
            result = "messageId";

            userMessagePayloadService.convert(ebms3PartInfo);
            result = partInfo;
        }};

        userMessagePayloadService.handlePayloads(soapRequestMessage, ebms3Messaging, null);

        new Verifications() {{
            partInfo.setInBody(true);
            partInfo.setPayloadDatahandler((DataHandler) any);
        }};
    }

    @Test
    void test_HandlePayLoads_EmptyCIDAndBodyContent(@Injectable final Ebms3Messaging ebms3Messaging,
                                                           @Injectable final Node bodyContent,
                                                           @Injectable final PartInfo partInfo)
            throws SOAPException, TransformerException, EbMS3Exception {

        new Expectations() {{

            ebms3Messaging.getUserMessage().getMessageInfo().getMessageId();
            result = "messageId";

            ebms3Messaging.getUserMessage().getPayloadInfo();
            result = null;
        }};

        userMessagePayloadService.handlePayloads(soapRequestMessage, ebms3Messaging, null);

        new FullVerifications() {
        };
    }


    /**
     * A single message having multiple PartInfo's with no or special cid.
     */
    @Test
    void test_HandlePayLoads_NullCIDMultiplePartInfo(
            @Injectable final Ebms3Messaging ebms3Messaging,
            @Injectable final Node bodyContent1,
            @Injectable final DataHandler dataHandler)
            throws SOAPException, TransformerException {

        Ebms3PartInfo part1 = UserMessageHandlerServiceImplTest.getPartInfo("MimeType", "text/xml", "");
        Ebms3PartInfo part2 = UserMessageHandlerServiceImplTest.getPartInfo("MimeType", "text/xml", "#1234");
        List<Ebms3PartInfo> ebms3PartInfos = asList(
                part1,
                part2);

        new Expectations(userMessagePayloadService) {{
            userMessagePayloadService.getDataHandler((Node) any);
            result = dataHandler;

            ebms3Messaging.getUserMessage().getMessageInfo().getMessageId();
            result = "messageId";

            ebms3Messaging.getUserMessage().getPayloadInfo().getPartInfo();
            result = ebms3PartInfos;

            partPropertyDictionaryService.findOrCreatePartProperty(anyString, anyString, anyString);
            result = new PartProperty();
            times = 2;
        }};

        try {
            userMessagePayloadService.handlePayloads(soapRequestMessage, ebms3Messaging, null);
            fail("Expecting error that - More than one Partinfo referencing the soap body found!");
        } catch (EbMS3Exception e) {
            Assertions.assertEquals(ErrorCode.EbMS3ErrorCode.EBMS_0003, e.getEbMS3ErrorCode());
        }

        new Verifications() {
        };
    }

    @Test
    void test_HandlePayLoads_HappyFlowUsingCID(@Injectable final UserMessage userMessage,
                                                      @Injectable final Ebms3Messaging ebms3Messaging,
                                                      @Injectable final DataHandler attachmentPart2DH) throws SOAPException, TransformerException, EbMS3Exception {

        final Ebms3PartInfo partInfo = getEbms3PartInfo();

        Ebms3PartProperties partProperties = getEbms3PartProperties();
        partInfo.setPartProperties(partProperties);

        List<AttachmentPart> attachmentPartList = new ArrayList<>();
        attachmentPartList.add(getAttachmentPart("AnotherContentID", null));
        attachmentPartList.add(getAttachmentPart("message", attachmentPart2DH));
        final Iterator<AttachmentPart> attachmentPartIterator = attachmentPartList.iterator();

        new Expectations(userMessagePayloadService) {{
            ebms3Messaging.getUserMessage().getMessageInfo().getMessageId();
            result = "messageId";

            ebms3Messaging.getUserMessage().getPayloadInfo().getPartInfo();
            result = List.of(partInfo);

            soapRequestMessage.getAttachments();
            result = attachmentPartIterator;

            partPropertyDictionaryService.findOrCreatePartProperty(anyString, anyString, anyString);
            result = new PartProperty();
            times = 1;
        }};

        userMessagePayloadService.handlePayloads(soapRequestMessage, ebms3Messaging, null);

        new Verifications() {
        };
    }

    private AttachmentPartImpl getAttachmentPart(String contentId, DataHandler dataHandler) {
        AttachmentPartImpl attachmentPart = new AttachmentPartImpl();
        attachmentPart.setContentId(contentId);
        if(dataHandler != null) {
            attachmentPart.setDataHandler(dataHandler);
        }
        return attachmentPart;
    }

    private static Ebms3PartProperties getEbms3PartProperties() {
        Ebms3PartProperties partProperties = new Ebms3PartProperties();
        partProperties.getProperties().add(getEbms3Property());
        return partProperties;
    }

    private static Ebms3PartInfo getEbms3PartInfo() {
        final Ebms3PartInfo partInfo = new Ebms3PartInfo();
        partInfo.setHref("cid:message");
        partInfo.setPartProperties(getEbms3PartProperties());
        return partInfo;
    }

    private static Ebms3Property getEbms3Property() {
        Ebms3Property property1 = new Ebms3Property();
        property1.setName("MimeType");
        property1.setValue("text/xml");
        return property1;
    }

    @Test
    void test_HandlePayLoads_NoPayloadFound(
            @Injectable final UserMessage userMessage,
            @Injectable final Ebms3Messaging ebms3Messaging,
            @Injectable final PartInfo partInfo,
            @Injectable final AttachmentPart attachmentPart1,
            @Injectable final AttachmentPart attachmentPart2) throws TransformerException, SOAPException {

        List<AttachmentPart> attachmentPartList = new ArrayList<>();
        attachmentPartList.add(attachmentPart1);
        attachmentPartList.add(attachmentPart2);
        final Iterator<AttachmentPart> attachmentPartIterator = attachmentPartList.iterator();

        new Expectations(userMessagePayloadService) {{
            ebms3Messaging.getUserMessage().getMessageInfo().getMessageId();
            result = "messageId";

            userMessagePayloadService.getPartInfoList(ebms3Messaging);
            result = Collections.singletonList(partInfo);

            partInfo.getHref();
            result = "cid:message";

            soapRequestMessage.getAttachments();
            result = attachmentPartIterator;

            attachmentPart1.getContentId();
            result = "AnotherContentID";

            attachmentPart2.getContentId();
            result = "message123";

        }};

        try {
            userMessagePayloadService.handlePayloads(soapRequestMessage, ebms3Messaging, null);
            fail("Expected Ebms3 exception that no matching payload was found!");
        } catch (EbMS3Exception e) {
            Assertions.assertEquals(ErrorCode.EbMS3ErrorCode.EBMS_0011, e.getEbMS3ErrorCode());
        }

        new FullVerifications() {{
            attachmentPart1.setContentId(anyString);
            attachmentPart2.setContentId(anyString);
        }};
    }

    private static PartInfo getPartInfo(String href) {
        PartInfo partInfo = new PartInfo();
        partInfo.setHref(href);
        return partInfo;
    }
}
