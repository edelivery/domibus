package eu.domibus.core.message;

import eu.domibus.api.jms.JMSManager;
import eu.domibus.api.jms.JmsMessage;
import eu.domibus.api.message.UserMessageException;
import eu.domibus.api.messaging.MessageNotFoundException;
import eu.domibus.api.messaging.MessagingException;
import eu.domibus.api.model.*;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.payload.PartInfoService;
import eu.domibus.api.pmode.PModeService;
import eu.domibus.api.pmode.PModeServiceHelper;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.usermessage.UserMessageService;
import eu.domibus.api.util.DateUtil;
import eu.domibus.api.util.DomibusStringUtil;
import eu.domibus.api.util.FileServiceUtil;
import eu.domibus.core.audit.AuditService;
import eu.domibus.core.converter.DomibusCoreMapper;
import eu.domibus.core.converter.MessageCoreMapper;
import eu.domibus.core.error.ErrorLogService;
import eu.domibus.core.jms.DispatchMessageCreator;
import eu.domibus.core.message.acknowledge.MessageAcknowledgementDao;
import eu.domibus.core.message.attempt.MessageAttemptDao;
import eu.domibus.core.message.converter.MessageConverterService;
import eu.domibus.core.message.dictionary.MessagePropertyDao;
import eu.domibus.core.message.nonrepudiation.NonRepudiationService;
import eu.domibus.core.message.nonrepudiation.SignalMessageRawEnvelopeDao;
import eu.domibus.core.message.nonrepudiation.UserMessageRawEnvelopeDao;
import eu.domibus.core.message.pull.PullMessageService;
import eu.domibus.core.message.signal.SignalMessageDao;
import eu.domibus.core.message.signal.SignalMessageLogDao;
import eu.domibus.core.message.splitandjoin.MessageGroupDao;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import eu.domibus.core.plugin.routing.RoutingService;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.scheduler.ReprogrammableService;
import eu.domibus.messaging.MessageConstants;
import mockit.*;
import mockit.integration.junit5.JMockitExtension;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.transaction.PlatformTransactionManager;

import javax.jms.Queue;
import javax.persistence.EntityManager;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_MESSAGE_DOWNLOAD_MAX_SIZE;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_RESEND_BUTTON_ENABLED_RECEIVED_MINUTES;
import static eu.domibus.core.message.UserMessageDefaultService.MIME_TYPE;
import static eu.domibus.core.message.UserMessageDefaultService.PAYLOAD_NAME;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Cosmin Baciu, Soumya
 * @since 3.3
 */
@SuppressWarnings({"ResultOfMethodCallIgnored", "unchecked", "ConstantConditions"})
@ExtendWith(JMockitExtension.class)
public class UserMessageDefaultServiceTest {

    public static final String MESSAGE_ID = "1000";

    @Tested
    UserMessageDefaultService userMessageDefaultService;

    @Injectable
    private Queue sendMessageQueue;

    @Injectable
    private Queue sendLargeMessageQueue;

    @Injectable
    private Queue splitAndJoinQueue;

    @Injectable
    private Queue sendPullReceiptQueue;

    @Injectable
    private Queue retentionMessageQueue;

    @Injectable
    private UserMessageLogDao userMessageLogDao;

    @Injectable
    private SignalMessageLogDao signalMessageLogDao;

    @Injectable
    private UserMessageLogDefaultService userMessageLogService;

    @Injectable
    private UserMessageServiceHelper userMessageServiceHelper;

    @Injectable
    private SignalMessageDao signalMessageDao;

    @Injectable
    private BackendNotificationService backendNotificationService;

    @Injectable
    protected RoutingService routingService;

    @Injectable
    protected PartInfoService partInfoService;

    @Injectable
    private JMSManager jmsManager;

    @Injectable
    DomibusCoreMapper coreMapper;

    @Injectable
    PModeService pModeService;

    @Injectable
    PModeServiceHelper pModeServiceHelper;

    @Injectable
    MessageExchangeService messageExchangeService;

    @Injectable
    DomainContextProvider domainContextProvider;

    @Injectable
    private PullMessageService pullMessageService;

    @Injectable
    MessageGroupDao messageGroupDao;

    @Injectable
    UserMessageFactory userMessageFactory;

    @Injectable
    PModeProvider pModeProvider;

    @Injectable
    MessageConverterService messageConverterService;

    @Injectable
    AuditService auditService;

    @Injectable
    UserMessagePriorityService userMessagePriorityService;

    @Injectable
    DomibusPropertyProvider domibusPropertyProvider;

    @Injectable
    DateUtil dateUtil;

    @Injectable
    private PlatformTransactionManager transactionManager;

    @Injectable
    private MessageAttemptDao messageAttemptDao;

    @Injectable
    private ErrorLogService errorLogService;

    @Injectable
    private MessagePropertyDao propertyDao;

    @Injectable
    private MessageAcknowledgementDao messageAcknowledgementDao;

    @Injectable
    NonRepudiationService nonRepudiationService;

    @Injectable
    UserMessageDao userMessageDao;

    @Injectable
    MessageCoreMapper messageCoreMapper;

    @Injectable
    PartInfoDao partInfoDao;

    @Injectable
    MessagePropertyDao messagePropertyDao;

    @Injectable
    private ReprogrammableService reprogrammableService;

    @Injectable
    private SignalMessageRawEnvelopeDao signalMessageRawEnvelopeDao;

    @Injectable
    private UserMessageRawEnvelopeDao userMessageRawEnvelopeDao;

    @Injectable
    private ReceiptDao receiptDao;

    @Injectable
    UserMessageDefaultRestoreService userMessageDefaultRestoreService;

    @Injectable
    EntityManager em;

    @Injectable
    private MessagesLogService messagesLogService;

    @Injectable
    private FileServiceUtil fileServiceUtil;

    @Injectable
    private DomibusStringUtil domibusStringUtil;

    @Test
    void testGetFinalRecipient(@Injectable final UserMessage userMessage) {
        final String messageId = "1";

        userMessageDefaultService.getFinalRecipient(messageId, MSHRole.SENDING);

        new Verifications() {{
            userMessageServiceHelper.getFinalRecipientValue(userMessage);
        }};
    }

    @Test
    void testGetFinalRecipientWhenNoMessageIsFound(@Injectable final UserMessage userMessage) {
        final String messageId = "1";

        new Expectations() {{
            userMessageDao.findByMessageId(messageId, MSHRole.SENDING);
            result = null;
        }};

        Assertions.assertNull(userMessageDefaultService.getFinalRecipient(messageId, MSHRole.SENDING));
    }

    @Test
    void testFailedMessages(@Injectable final UserMessage userMessage) {
        final String finalRecipient = "C4";

        userMessageDefaultService.getFailedMessages(finalRecipient, null);

        new Verifications() {{
            userMessageLogService.findFailedMessages(finalRecipient, null);
        }};
    }

    @Test
    void testGetFailedMessageElapsedTime(@Injectable final UserMessageLog userMessageLog) {
        final String messageId = "1";
        final Date failedDate = new Date();

        new Expectations(userMessageDefaultService) {{
            userMessageDefaultService.getFailedMessage(messageId);
            result = userMessageLog;

            userMessageLog.getFailed();
            result = failedDate;
        }};

        final Long failedMessageElapsedTime = userMessageDefaultService.getFailedMessageElapsedTime(messageId);
        assertNotNull(failedMessageElapsedTime);
    }

    @Test
    void testGetFailedMessageElapsedTimeWhenFailedDateIsNull(@Injectable final UserMessageLog userMessageLog) {
        final String messageId = "1";

        new Expectations(userMessageDefaultService) {{
            userMessageDefaultService.getFailedMessage(messageId);
            result = userMessageLog;

            userMessageLog.getFailed();
            result = null;
        }};
        Assertions.assertThrows(UserMessageException.class,
                () -> userMessageDefaultService.getFailedMessageElapsedTime(messageId));
    }

    @Test
    void testScheduleSending(@Injectable final JmsMessage jmsMessage,
                                    @Injectable DispatchMessageCreator dispatchMessageCreator,
                                    @Injectable UserMessageLog userMessageLog,
                                    @Injectable UserMessage userMessage) {
        final String messageId = "1";
        Long messageEntityId = 1L;

        new Expectations(userMessageDefaultService) {{
            userMessage.getMessageId();
            result = messageId;

            userMessage.getEntityId();
            result = messageEntityId;

            userMessageDefaultService.getDispatchMessageCreator(messageId, messageEntityId);
            result = dispatchMessageCreator;

            dispatchMessageCreator.createMessage();
            result = jmsMessage;

            userMessageDefaultService.scheduleSending(userMessage, userMessageLog, jmsMessage);
            times = 1;

        }};

        userMessageDefaultService.scheduleSending(userMessage, userMessageLog);

        new FullVerifications() {
        };

    }

    @Test
    void testSchedulePullReceiptSending(@Injectable final JmsMessage jmsMessage) {
        final String messageId = "1";
        final String pModeKey = "pModeKey";


        userMessageDefaultService.scheduleSendingPullReceipt(messageId, pModeKey);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, sendPullReceiptQueue);
        }};

    }

    @Test
    void testFailedMessageWhenNoMessageIsFound() {
        final String messageId = "1";

        new Expectations() {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = null;
        }};

        assertThrows(MessageNotFoundException.class, () -> userMessageDefaultService.getFailedMessage(messageId));
    }

    @Test
    void testFailedMessageWhenStatusIsNotFailed(@Injectable final UserMessageLog userMessageLog) {
        final String messageId = "1";

        new Expectations() {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = userMessageLog;

            userMessageLog.getMessageStatus();
            result = MessageStatus.RECEIVED;
        }};

        Assertions.assertThrows(UserMessageException.class,
                () -> userMessageDefaultService.getFailedMessage(messageId));
    }

    @Test
    void testGetFailedMessage(@Injectable final UserMessageLog userMessageLog) {
        final String messageId = "1";

        new Expectations() {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = userMessageLog;

            userMessageLog.getMessageStatus();
            result = MessageStatus.SEND_FAILURE;
        }};

        final UserMessageLog failedMessage = userMessageDefaultService.getFailedMessage(messageId);
        Assertions.assertNotNull(failedMessage);
    }

    @Test
    void testDeleteMessaged(@Injectable UserMessageLog userMessageLog,
                                   @Injectable UserMessage userMessage) {
        final String messageId = "1";

        new Expectations() {{
            backendNotificationService.notifyMessageDeleted(userMessage, userMessageLog, false);
        }};

        userMessageDefaultService.deleteMessage(messageId, MSHRole.SENDING);

        new Verifications() {{
            backendNotificationService.notifyMessageDeleted(userMessage, userMessageLog, false);
        }};
    }

    @Test
    void marksTheUserMessageAsDeleted(@Injectable UserMessage userMessage,
                                             @Injectable UserMessageLog userMessageLog,
                                             @Injectable SignalMessage signalMessage) {
        final String messageId = "1";

        new Expectations() {{
            userMessageLogService.findByMessageIdSafely(messageId, MSHRole.SENDING);
            result = userMessageLog;

            userMessageLog.getEntityId();
            result = 42L;

            signalMessageDao.findByUserMessageIdWithUserMessage(messageId, MSHRole.SENDING);
            result = signalMessage;

            signalMessage.getUserMessage();
            result = userMessage;

        }};

        userMessageDefaultService.deleteMessage(messageId, MSHRole.SENDING);

        new Verifications() {{
            userMessageLog.setDeleted((Date) any);
            userMessageLogService.setMessageAsDeleted(userMessage, userMessageLog);
            userMessageLogService.setSignalMessageAsDeleted(signalMessage);
            userMessageLog.getMessageStatus();
            backendNotificationService.notifyMessageDeleted(userMessage, userMessageLog, false);
            times = 1;

            partInfoService.clearPayloadData(userMessage.getEntityId());
        }};
    }

    @Test
    void marksTheUserMessageAsDeleted_emptySignal(@Injectable UserMessage userMessage,
                                                         @Injectable UserMessageLog userMessageLog) {
        final String messageId = "1";

        new Expectations() {{
            userMessageLogService.findByMessageIdSafely(messageId, MSHRole.SENDING);
            result = userMessageLog;

            signalMessageDao.findByUserMessageIdWithUserMessage(messageId, MSHRole.SENDING);
            result = null;

            userMessageLog.getEntityId();
            result = 42L;

            userMessageLog.getUserMessage();
            result = userMessage;

            userMessageLog.getMessageStatus();
            result = MessageStatus.DOWNLOADED;

        }};

        userMessageDefaultService.deleteMessage(messageId, MSHRole.SENDING);

        new Verifications() {{
            partInfoService.clearPayloadData(userMessage.getEntityId());
            times = 1;

            userMessageLog.setDeleted((Date) any);
            times = 1;

            backendNotificationService.notifyMessageDeleted(userMessage, userMessageLog, false);
            times = 1;

            userMessageLogService.setMessageAsDeleted(userMessage, userMessageLog);
            times = 1;

            userMessageLogService.setSignalMessageAsDeleted((SignalMessage) null);
            times = 1;

        }};
    }

    @Test
    void test_sendEnqueued(final @Injectable UserMessageLog userMessageLog,
                                  final @Injectable UserMessage userMessage) {
        final String messageId = UUID.randomUUID().toString();

        new Expectations(userMessageDefaultService) {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = userMessageLog;

            userMessageLog.getMessageStatus();
            result = MessageStatus.SEND_ENQUEUED;

            domibusPropertyProvider.getIntegerProperty(DOMIBUS_RESEND_BUTTON_ENABLED_RECEIVED_MINUTES);
            result = 2;

            userMessageLog.getReceived();
            result = DateUtils.addMinutes(new Date(), -3);

            userMessageLog.getNextAttempt();
            result = null;

            userMessageDao.findByEntityId(userMessageLog.getEntityId());
            result = userMessage;

            userMessageDefaultService.scheduleSending(userMessage, userMessageLog);
            times = 1;

        }};

        //tested method
        userMessageDefaultService.sendEnqueuedMessage(messageId);

        new FullVerifications() {{
            reprogrammableService.setRescheduleInfo(userMessageLog, withAny(new Date()));
            userMessageLogService.update(userMessageLog);
        }};
    }

    @Test
    void test_sendEnqueued_nextAttemptBeforeNow(final @Injectable UserMessageLog userMessageLog,
                                                                final @Injectable UserMessage userMessage) {
        final String messageId = UUID.randomUUID().toString();

        new Expectations(userMessageDefaultService) {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = userMessageLog;

            userMessageLog.getMessageStatus();
            result = MessageStatus.SEND_ENQUEUED;

            userMessageLog.getEntityId();
            result = 12L;

            domibusPropertyProvider.getIntegerProperty(DOMIBUS_RESEND_BUTTON_ENABLED_RECEIVED_MINUTES);
            result = 2;

            userMessageLog.getReceived();
            result = DateUtils.addMinutes(new Date(), -3);

            userMessageLog.getNextAttempt();
            result = Date.from(ZonedDateTime
                    .now(ZoneOffset.UTC)
                    .minusMinutes(10)
                    .toInstant());

            userMessageDao.findByEntityId(12L);
            result = userMessage;

            userMessageDefaultService.scheduleSending(userMessage, userMessageLog);
            times = 1;

        }};

        //tested method
        userMessageDefaultService.sendEnqueuedMessage(messageId);

        new FullVerifications() {{
            reprogrammableService.setRescheduleInfo(userMessageLog, withAny(new Date()));
            userMessageLogService.update(userMessageLog);

        }};
    }

    @Test
    void test_sendEnqueued_nextAttemptAfterNowException(final @Injectable UserMessageLog userMessageLog,
                                                               final @Injectable UserMessage userMessage) {
        final String messageId = UUID.randomUUID().toString();

        new Expectations(userMessageDefaultService) {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = userMessageLog;

            userMessageLog.getMessageStatus();
            result = MessageStatus.SEND_ENQUEUED;

            domibusPropertyProvider.getIntegerProperty(DOMIBUS_RESEND_BUTTON_ENABLED_RECEIVED_MINUTES);
            result = 2;

            userMessageLog.getReceived();
            result = DateUtils.addMinutes(new Date(), -3);

            userMessageLog.getNextAttempt();
            result = Date.from(ZonedDateTime
                    .now(ZoneOffset.UTC)
                    .plusMinutes(10)
                    .toInstant());
        }};

        //tested method
        Assertions.assertThrows(UserMessageException.class, () -> userMessageDefaultService.sendEnqueuedMessage(messageId));

        new FullVerifications() {};
    }

    @Test
    void getUserMessagePriority(@Injectable UserMessage userMessage) {
        String service = "my service";
        String action = "my action";

        new Expectations() {{
            userMessageServiceHelper.getService(userMessage);
            result = service;

            userMessageServiceHelper.getAction(userMessage);
            result = action;
        }};

        userMessageDefaultService.getUserMessagePriority(userMessage);

        new Verifications() {{
            userMessagePriorityService.getPriority(service, action);
        }};
    }


    @Test
    void deleteFailedMessageTest() {
        final String messageId = UUID.randomUUID().toString();

        new Expectations(userMessageDefaultService) {{
            userMessageDefaultService.getFailedMessage(messageId);
            times = 1;
            userMessageDefaultService.deleteMessage(messageId, MSHRole.SENDING);
            times = 1;
        }};

        userMessageDefaultService.deleteFailedMessage(messageId);

        new VerificationsInOrder() {{
            userMessageDefaultService.getFailedMessage(messageId);
            userMessageDefaultService.deleteMessage(messageId, MSHRole.SENDING);
        }};
    }

    @Test
    void scheduleSendingPullReceiptWithRetryCountTest(@Injectable final JmsMessage jmsMessage,
                                                             @Injectable UserMessageService userMessageService) {
        final String messageId = UUID.randomUUID().toString();
        final String pModeKey = "pModeKey";
        final int retryCount = 3;

        userMessageDefaultService.scheduleSendingPullReceipt(messageId, pModeKey, retryCount);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, sendPullReceiptQueue);
        }};
    }

    @Test
    void scheduleSplitAndJoinReceiveFailedTest(@Injectable final JmsMessage jmsMessage,
                                                      @Injectable UserMessageService userMessageService) {
        final String sourceMessageId = UUID.randomUUID().toString();
        final String groupId = "groupId";
        final String errorCode = "ebms3ErrorCode";
        final String errorDetail = "ebms3ErrorDetail";

        userMessageDefaultService.scheduleSplitAndJoinReceiveFailed(groupId, sourceMessageId, errorCode, errorDetail);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, splitAndJoinQueue);
        }};

    }

    @Test
    void scheduleSendingSignalErrorTest(@Injectable final JmsMessage jmsMessage,
                                               @Injectable UserMessageService userMessageService) {
        final String messageId = UUID.randomUUID().toString();
        final String pmodeKey = "pmodeKey";
        final String ebMS3ErrorCode = "ebms3ErrorCode";
        final String errorDetail = "ebms3ErrorDetail";

        userMessageDefaultService.scheduleSendingSignalError(messageId, ebMS3ErrorCode, errorDetail, pmodeKey);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, splitAndJoinQueue);
        }};
    }

    @Test
    void scheduleSourceMessageReceiptTest(@Injectable final JmsMessage jmsMessage,
                                                 @Injectable UserMessageService userMessageService) {
        final String messageId = UUID.randomUUID().toString();
        final String pmodeKey = "pmodeKey";

        userMessageDefaultService.scheduleSourceMessageReceipt(messageId, pmodeKey);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, splitAndJoinQueue);
        }};
    }

    @Test
    void scheduleSourceMessageRejoinTest(@Injectable final JmsMessage jmsMessage,
                                                @Injectable UserMessageService userMessageService) {
        final String groupId = "groupId";
        final String file = "SourceMessageFile";
        final String backendName = "backendName";

        userMessageDefaultService.scheduleSourceMessageRejoin(groupId, file, backendName);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, splitAndJoinQueue);
        }};
    }

    @Test
    void scheduleSourceMessageRejoinFileTest(@Injectable final JmsMessage jmsMessage,
                                                    @Injectable UserMessageService userMessageService) {
        final String groupId = "groupId";
        final String backendName = "backendName";

        userMessageDefaultService.scheduleSourceMessageRejoinFile(groupId, backendName);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, splitAndJoinQueue);
        }};
    }

    @Test
    void scheduleSetUserMessageFragmentAsFailedTest(@Injectable final JmsMessage jmsMessage,
                                                           @Injectable UserMessageService userMessageService) {
        final String messageId = UUID.randomUUID().toString();

        userMessageDefaultService.scheduleSetUserMessageFragmentAsFailed(messageId, MSHRole.SENDING);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, splitAndJoinQueue);
        }};
    }

    @Test
    void scheduleSplitAndJoinSendFailedTest(@Injectable final JmsMessage jmsMessage,
                                                   @Injectable UserMessageService userMessageService) {
        final String groupId = "groupId";
        final String errorDetail = "ebms3ErrorDetail";

        userMessageDefaultService.scheduleSplitAndJoinSendFailed(groupId, errorDetail);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, splitAndJoinQueue);
        }};
    }

    @Test
    void scheduleSourceMessageSendingTest(@Injectable final JmsMessage jmsMessage,
                                                 @Injectable DispatchMessageCreator dispatchMessageCreator) {
        final String messageId = UUID.randomUUID().toString();
        Long messageEntityId = 1L;

        new Expectations(userMessageDefaultService) {{
            userMessageDefaultService.getDispatchMessageCreator(messageId, messageEntityId);
            result = dispatchMessageCreator;

            dispatchMessageCreator.createMessage();
            result = jmsMessage;
        }};

        userMessageDefaultService.scheduleSourceMessageSending(messageId, messageEntityId);

        new Verifications() {{
            jmsManager.sendMessageToQueue((JmsMessage) any, sendLargeMessageQueue);
        }};
    }


    @Test
    void testPayloadExtension(@Injectable final PartInfo partInfoNotCompressed, @Injectable final PartInfo partInfoCompressed) {
        final String originalExtension = ".xml";
        final String originalMimeType = "text/xml";
        final PartProperty mimeTypeProperty = new PartProperty();
        mimeTypeProperty.setName(MIME_TYPE);
        mimeTypeProperty.setValue(originalMimeType);
        final PartProperty compressionProperty = new PartProperty();
        compressionProperty.setName(MessageConstants.COMPRESSION_PROPERTY_KEY);
        compressionProperty.setValue(MessageConstants.COMPRESSION_PROPERTY_VALUE);
        new Expectations(userMessageDefaultService) {{
            partInfoNotCompressed.getPartProperties();
            result = Collections.singleton(mimeTypeProperty);
            fileServiceUtil.getExtension(originalMimeType);
            result = originalExtension;
            partInfoCompressed.getPartProperties();
            result = new HashSet<>(Arrays.asList(mimeTypeProperty, compressionProperty));
        }};

        Assertions.assertEquals(originalExtension, userMessageDefaultService.getPayloadExtension(partInfoNotCompressed));
        Assertions.assertEquals(originalExtension, userMessageDefaultService.getPayloadExtension(partInfoCompressed));
    }

    @Test
    void testPayloadName(@Injectable final PartInfo partInfoWithBodyload, @Injectable final PartInfo partInfoWithPayload,
                                @Injectable final PartInfo partInfoWithPartProperties, @Injectable PartProperty partProperty) {

        Set<PartProperty> partProperties = new HashSet<>();
        partProperties.add(partProperty);
        new Expectations(userMessageDefaultService) {{
            partInfoWithBodyload.getHref();
            result = null;
            partInfoWithPayload.getHref();
            result = "cid:1234";
            partInfoWithPartProperties.getHref();
            result = "cid:1234";
            partInfoWithPartProperties.getPartProperties();
            result = partProperties;
            partProperty.getName();
            result = PAYLOAD_NAME;
            partProperty.getValue();
            result = "test.txt";
            userMessageDefaultService.getPayloadExtension(partInfoWithPayload);
            result = ".xml";

        }};

        Assertions.assertEquals("bodyload.xml", userMessageDefaultService.getPayloadName(partInfoWithBodyload));
        Assertions.assertEquals("1234Payload.xml", userMessageDefaultService.getPayloadName(partInfoWithPayload));
        Assertions.assertEquals("test.txt", userMessageDefaultService.getPayloadName(partInfoWithPartProperties));
    }

    @Test
    void getUserMessageById() {
        final String messageId = UUID.randomUUID().toString();
        UserMessage userMessage = new UserMessage();

        new Expectations() {
        };

        UserMessage result = userMessageDefaultService.getUserMessageById(messageId, MSHRole.RECEIVING);

        Assertions.assertEquals(userMessage, result);
    }

    @Test
    void getUserMessageById_notFound() {
        final String messageId = UUID.randomUUID().toString();

        new Expectations() {{
            userMessageDao.findByMessageId(messageId, MSHRole.RECEIVING);
            result = null;
        }};

        Assertions.assertThrows(MessageNotFoundException.class,
                () ->  userMessageDefaultService.getUserMessageById(messageId, MSHRole.RECEIVING));

        new Verifications() {{
            auditService.addMessageDownloadedAudit(messageId, MSHRole.RECEIVING);
            times = 0;
        }};
    }

    @Test
    void getMessageInFinalStatus() {
        final String messageId = "1";

        new Expectations() {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = null;
        }};

        MessageNotFoundException ex = assertThrows(MessageNotFoundException.class,
                () -> userMessageDefaultService.getMessageNotInFinalStatus(messageId, MSHRole.SENDING));
        Assertions.assertTrue(ex.getMessage().contains("Message [1] does not exist"));
    }

    @Test
    void getMessageNotInFinalStatus(@Injectable final UserMessageLog userMessageLog) {
        final String messageId = "1";

        new Expectations(userMessageDefaultService) {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = userMessageLog;

            userMessageLog.getMessageStatus();
            result = MessageStatus.SEND_ENQUEUED;
        }};

        final UserMessageLog message = userMessageDefaultService.getMessageNotInFinalStatus(messageId, MSHRole.SENDING);
        Assertions.assertNotNull(message);
    }

    @Test
    void getMessageNotInFinalStatus_FinalState(@Injectable final UserMessageLog userMessageLog) {
        final String messageId = "1";

        new Expectations() {{
            userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
            result = userMessageLog;

            userMessageLog.getMessageStatus();
            result = MessageStatus.ACKNOWLEDGED;
        }};

        try {
            userMessageDefaultService.getMessageNotInFinalStatus(messageId, MSHRole.SENDING);
            fail();
        } catch (UserMessageException ex) {
            Assertions.assertTrue(ex.getMessage().contains("Message [1] is in final state [" + MessageStatus.ACKNOWLEDGED.name() + "]"));
        }
    }

    @Test
    void deleteMessageNotInFinalStatus(@Injectable UserMessageLog mes) {
        final String messageId = UUID.randomUUID().toString();

        new Expectations(userMessageDefaultService) {{
            mes.getMshRole().getRole();
            result = MSHRole.SENDING;
            userMessageDefaultService.getMessageNotInFinalStatus(messageId, MSHRole.SENDING);
            result = mes;
            userMessageDefaultService.deleteMessage(messageId, mes.getMshRole().getRole());
            times = 1;
        }};

        userMessageDefaultService.deleteMessageNotInFinalStatus(messageId, MSHRole.SENDING);

        new VerificationsInOrder() {{
            userMessageDefaultService.getMessageNotInFinalStatus(messageId, MSHRole.SENDING);
            userMessageDefaultService.deleteMessage(messageId, mes.getMshRole().getRole());
        }};
    }

    @Test
    void deleteMessagesDuringPeriod(@Injectable UserMessageLogDto userMessageLogDto) {
        final String messageId = "1";
        final List<UserMessageLogDto> messagesToDelete = new ArrayList<>();
        messagesToDelete.add(userMessageLogDto);

        final String originalUserFromSecurityContext = "C4";

        new Expectations(userMessageDefaultService) {{
            userMessageLogDto.getMessageId();
            result = messageId;
            userMessageLogDto.getMshRole();
            result = MSHRole.SENDING;
            userMessageLogService.findMessagesToDeleteNotInFinalStatus(originalUserFromSecurityContext, 1L, 2L);
            result = messagesToDelete;
            userMessageDefaultService.deleteMessage(messageId, MSHRole.SENDING);
            times = 1;
        }};

        userMessageDefaultService.deleteMessagesNotInFinalStatusDuringPeriod(1L, 2L, originalUserFromSecurityContext);

        new FullVerifications() {
        };
    }

    @Test
    void test_checkCanDownloadWithMaxDownLoadSize(@Injectable UserMessageLog existingMessage, @Injectable UserMessage userMessage) {
        String messageId = "messageId";

        new Expectations(userMessageDefaultService) {{
            userMessageLogService.findByMessageId(anyString, (MSHRole) any);
            result = existingMessage;
            existingMessage.getDeleted();
            result = null;
            domibusPropertyProvider.getIntegerProperty(DOMIBUS_MESSAGE_DOWNLOAD_MAX_SIZE);
            result = 1;
            partInfoService.findPartInfoTotalLength(existingMessage.getEntityId());
            result = 1000;
        }};

        MessagingException ex = assertThrows(MessagingException.class,
                () -> userMessageDefaultService.checkCanGetMessageContent(messageId, MSHRole.RECEIVING));
        Assertions.assertEquals("[DOM_001]:The message size exceeds maximum download size limit: 1", ex.getMessage());
    }

    @Test
    void test_checkCanDownloadWithDeletedMessage(@Injectable UserMessageLog deletedMessage) {
        new Expectations(userMessageDefaultService) {{
            userMessageLogService.findByMessageId(anyString, (MSHRole) any);
            result = deletedMessage;
            deletedMessage.getDeleted();
            result = new Date();

        }};

        MessagingException ex = assertThrows(MessagingException.class,
                () -> userMessageDefaultService.checkCanGetMessageContent("messageId", MSHRole.RECEIVING));
        Assertions.assertTrue(ex.getMessage().contains("[DOM_001]:Message content is no longer available for message id:"));
    }

    @Test
    void test_checkCanDownloadWithExistingMessage(@Injectable UserMessageLog existingMessage) {
        new Expectations(userMessageDefaultService) {{
            userMessageLogService.findByMessageId(anyString, (MSHRole) any);
            result = existingMessage;
            existingMessage.getDeleted();
            result = null;
        }};

        userMessageDefaultService.checkCanGetMessageContent("messageId", MSHRole.RECEIVING);
    }

    @Test
    void test_checkCanDownloadWhenNoMessage() {
        new Expectations() {{
            userMessageLogService.findByMessageId(anyString, (MSHRole) any);
            result = null;
        }};

        MessagingException ex = assertThrows(MessagingException.class,
                () -> userMessageDefaultService.checkCanGetMessageContent("messageId", MSHRole.RECEIVING));
        Assertions.assertEquals("[DOM_001]:No message found for message id: messageId", ex.getMessage());
    }
}
