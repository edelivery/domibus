package eu.domibus.web.rest;

import eu.domibus.api.property.DomibusProperty;
import eu.domibus.api.property.DomibusPropertyException;
import eu.domibus.core.property.DomibusPropertyMetadataMapper;
import eu.domibus.core.property.DomibusPropertyResourceHelper;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.web.rest.error.ErrorHandlerService;
import eu.domibus.web.rest.ro.DomibusPropertyRO;
import eu.domibus.web.rest.ro.ErrorRO;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

/**
 * @author Ion Perpegel
 * @since 5.2
 * <p>
 * Resource responsible for getting the domibus properties accessible to simple users (with USER_ROLE)
 */
@RestController
@RequestMapping(value = "/rest/internal/user/configuration/properties")
@Validated
public class DomibusAccessiblePropertiesResource extends BaseResource {
    private static final Logger LOG = DomibusLoggerFactory.getLogger(DomibusAccessiblePropertiesResource.class);

    /**
     * This is the list of all properties accessible for normal users
     * They are used in MesssageLog and ErrorLogs pages of admin console
     */
    private final List<String> properties = Arrays.asList(
            DOMIBUS_UI_CSV_MAX_ROWS,
            DOMIBUS_UI_MESSAGE_LOGS_DEFAULT_INTERVAL,
            DOMIBUS_UI_MESSAGE_LOGS_LANDING_PAGE,
            DOMIBUS_UI_MESSAGE_LOGS_SEARCH_ADVANCED_ENABLED
    );

    private final DomibusPropertyResourceHelper domibusPropertyResourceHelper;

    private final DomibusPropertyMetadataMapper domibusPropertyMetadataMapper;

    private final ErrorHandlerService errorHandlerService;

    public DomibusAccessiblePropertiesResource(DomibusPropertyResourceHelper domibusPropertyResourceHelper,
                                               DomibusPropertyMetadataMapper domibusPropertyMetadataMapper,
                                               ErrorHandlerService errorHandlerService) {
        this.domibusPropertyResourceHelper = domibusPropertyResourceHelper;
        this.domibusPropertyMetadataMapper = domibusPropertyMetadataMapper;
        this.errorHandlerService = errorHandlerService;
    }

    @ExceptionHandler({DomibusPropertyException.class})
    public ResponseEntity<ErrorRO> handleDomibusPropertyException(DomibusPropertyException ex) {
        Throwable rootCause = ExceptionUtils.getRootCause(ex);
        String message = rootCause == null ? ex.getMessage() : rootCause.getMessage();
        return errorHandlerService.createResponse(message, HttpStatus.BAD_REQUEST);
    }

    /**
     *
     * Returns the property metadata and the current value for a property accessible for normal users (with role USER_ROLE)
     *
     * @param propertyName the name of the property
     * @return object containing both metadata and value
     */
    @GetMapping(path = "/{propertyName:.+}")
    public DomibusPropertyRO getProperty(@Valid @PathVariable String propertyName) {
        if (!properties.contains(propertyName)) {
            throw new DomibusPropertyException("Could not find property " + propertyName);
        }

        DomibusProperty prop = domibusPropertyResourceHelper.getProperty(propertyName);
        return domibusPropertyMetadataMapper.propertyApiToPropertyRO(prop);
    }

}
