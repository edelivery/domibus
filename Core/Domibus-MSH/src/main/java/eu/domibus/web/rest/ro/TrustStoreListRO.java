package eu.domibus.web.rest.ro;

import java.util.List;

/**
 * @author G. Maier
 * @since 5.2
 */
public class TrustStoreListRO {
    private List<TrustStoreRO> trustStoreList;
    private boolean isReadOnly = false;
    private boolean isDownloadable = true;


    public List<TrustStoreRO> getTrustStoreList() {
        return trustStoreList;
    }

    public TrustStoreListRO setTrustStoreList(List<TrustStoreRO> trustStoreList) {
        this.trustStoreList = trustStoreList;
        return this;
    }

    public boolean isReadOnly() {
        return isReadOnly;
    }

    public TrustStoreListRO setReadOnly(boolean readOnly) {
        isReadOnly = readOnly;
        return this;
    }

    public boolean isDownloadable() {
        return isDownloadable;
    }

    public TrustStoreListRO setDownloadable(boolean downloadable) {
        isDownloadable = downloadable;
        return this;
    }
}
