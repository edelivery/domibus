package eu.domibus.web.rest;

import eu.domibus.api.crypto.TLSGenericCertificateManager;
import eu.domibus.api.exceptions.RequestValidationException;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceService;
import eu.domibus.api.pki.SecurityProfileProvider;
import eu.domibus.api.pki.SecurityProfileService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.security.TrustStoreEntry;
import eu.domibus.api.util.MultiPartFileUtil;
import eu.domibus.api.validators.SkipWhiteListed;
import eu.domibus.core.audit.AuditService;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.core.converter.PartyCoreMapper;
import eu.domibus.web.rest.error.ErrorHandlerService;
import eu.domibus.web.rest.ro.TrustStoreListRO;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.KeyStore;
import java.util.List;

import static eu.domibus.api.crypto.TLSGenericCertificateManager.TLS_GENERIC_TRUSTSTORE_NAME;

/**
 * @author Ionut Breaz
 * @since 5.2
 */

@RestController
@RequestMapping(value = "/rest/internal/admin/tlsgenerictruststore")
public class TLSGenericTruststoreResource extends TruststoreResourceBase {

    private final TLSGenericCertificateManager tlsGenericCertificateManager;

    public TLSGenericTruststoreResource(PartyCoreMapper coreMapper,
                                        ErrorHandlerService errorHandlerService,
                                        MultiPartFileUtil multiPartFileUtil,
                                        AuditService auditService,
                                        DomainContextProvider domainContextProvider,
                                        DomibusConfigurationService domibusConfigurationService,
                                        CertificateHelper certificateHelper,
                                        KeystorePersistenceService keystorePersistenceService,
                                        SecurityProfileProvider securityProfileProvider,
                                        SecurityProfileService securityProfileService,
                                        TLSGenericCertificateManager tlsGenericCertificateManager) {
        super(coreMapper, errorHandlerService, multiPartFileUtil, auditService, domainContextProvider, domibusConfigurationService,
                certificateHelper, keystorePersistenceService, securityProfileProvider, securityProfileService);
        this.tlsGenericCertificateManager = tlsGenericCertificateManager;
    }

    @PostMapping()
    public String uploadTLSTruststoreFile(@RequestPart("file") MultipartFile file,
                                          @SkipWhiteListed @RequestParam("password") String password,
                                          @RequestParam("allowChangingDiskStoreProps") Boolean allowChangingDiskStoreProps) throws RequestValidationException {
        uploadStore(file, password, allowChangingDiskStoreProps);
        return "TLS Generic truststore file has been successfully uploaded.";
    }

    @GetMapping(produces = "application/octet-stream")
    public ResponseEntity<ByteArrayResource> downloadTLSTrustStore() {
        return downloadTruststoreContent();
    }

    @GetMapping(value = {"/entries"})
    public TrustStoreListRO getTLSTruststoreEntries() {
        return new TrustStoreListRO()
                .setTrustStoreList(getTrustStoreEntries());
    }

    @GetMapping(path = "/entries/csv")
    public ResponseEntity<String> getTLSEntriesAsCsv() {
        return getEntriesAsCSV(getStoreName());
    }

    @PostMapping(value = "/entries")
    public String addTLSCertificate(@RequestPart("file") MultipartFile certificateFile,
                                    @RequestParam("alias") @Valid @NotNull String alias) throws RequestValidationException {
        return addCertificate(certificateFile, alias);
    }

    @DeleteMapping(value = "/entries/{alias:.+}")
    public String removeTLSCertificate(@PathVariable String alias) throws RequestValidationException {
        return removeCertificate(alias);
    }

    @Override
    protected void doUploadStore(KeyStoreContentInfo storeInfo) {
        tlsGenericCertificateManager.replaceTrustStore(storeInfo);
    }

    @Override
    protected KeyStoreContentInfo getTrustStoreContent() {
        return tlsGenericCertificateManager.getTruststoreContent();
    }

    @Override
    protected List<TrustStoreEntry> doGetStoreEntries() {
        return tlsGenericCertificateManager.getTrustStoreEntries();
    }

    @Override
    protected String getStoreName() {
        return TLS_GENERIC_TRUSTSTORE_NAME;
    }

    @Override
    protected boolean doAddCertificate(String alias, byte[] fileContent) {
        return tlsGenericCertificateManager.addCertificate(fileContent, alias);
    }

    @Override
    protected boolean doRemoveCertificate(String alias) {
        return tlsGenericCertificateManager.removeCertificate(alias);
    }
}
