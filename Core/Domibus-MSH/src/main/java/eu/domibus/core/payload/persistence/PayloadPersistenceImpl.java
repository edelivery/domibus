package eu.domibus.core.payload.persistence;

import eu.domibus.api.model.PartInfo;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.multitenancy.DomainsAware;
import eu.domibus.api.payload.PartInfoService;
import eu.domibus.api.payload.encryption.PayloadEncryptionService;
import eu.domibus.api.util.DateUtil;
import eu.domibus.api.util.TsidUtil;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.message.compression.CompressionService;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import eu.domibus.core.property.DomibusPropertyProviderImpl;
import eu.domibus.core.spi.payload.DomibusPayloadManagerSpi;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_PAYLOAD_FILE_SYSTEM_FOLDER_ADDED_MINUTES;

/**
 * @author Cosmin Baciu
 * @since 4.1
 */
@Service
public class PayloadPersistenceImpl implements PayloadPersistence, DomainsAware {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PayloadPersistenceImpl.class);

    public static final String PAYLOAD_EXTENSION = ".payload";

    @Autowired
    protected BackendNotificationService backendNotificationService;

    @Autowired
    protected CompressionService compressionService;

    @Autowired
    protected DateUtil dateUtil;

    @Autowired
    protected PayloadPersistenceHelper payloadPersistenceHelper;

    @Autowired
    protected PayloadEncryptionService payloadEncryptionActive;

    @Autowired
    DomibusPayloadManagerSpiProviderImpl domibusPayloadManagerSpiProvider;

    @Autowired
    protected DomainService domainService;

    @Autowired
    protected DomainContextProvider domainContextProvider;

    @Autowired
    protected TsidUtil tsidUtil;
    @Autowired
    private PartInfoService partInfoService;
    @Autowired
    private DomibusPropertyProviderImpl domibusPropertyProvider;

    @Override
    public void initializePayloadPersistence() {
        LOG.info("Initializing payload storage providers");

        final List<Domain> domains = domainService.getDomains();
        for (Domain domain : domains) {
            final DomibusPayloadManagerSpi domibusPayloadManagerSpi = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpi(domain);
            try {
                domibusPayloadManagerSpi.initialize(domain.getCode());
            } catch (Exception e) {
                LOG.error("Error initializing payload persistence for domain [" + domain + "]", e);
            }
        }
    }

    @Override
    public void storeIncomingPayload(PartInfo partInfo, UserMessage userMessage, LegConfiguration legConfiguration) throws IOException {
        final boolean encryptionActive = BooleanUtils.isTrue(payloadEncryptionActive.isPayloadEncryptionActive(userMessage));
        saveIncomingPayload(partInfo, encryptionActive);
        LOG.businessInfo(DomibusMessageCode.BUS_PAYLOAD_PERSISTED_ON_FILE_SYSTEM, partInfo.getHref());
    }

    protected void saveIncomingPayload(PartInfo partInfo, final boolean encryptionActive) throws IOException {
        final DomibusPayloadManagerSpi domibusPayloadManagerSpiForCurrentDomain = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpiForCurrentDomain();
        final String payloadManagerSpiForCurrentDomainIdentifier = domibusPayloadManagerSpiForCurrentDomain.getIdentifier();

        LOG.debug("Saving incoming payload [{}] using persistence [{}]", partInfo.getHref(), payloadManagerSpiForCurrentDomainIdentifier);

        final String payloadRelativeLocation = createPayloadRelativeLocation();

        final boolean storedInDatabase = domibusPayloadManagerSpiForCurrentDomain.arePayloadsStoredInDomibusDatabase();

        try (final InputStream inputStream = partInfo.getPayloadDatahandler().getInputStream()) {
            FileWriteResult fileWriteResult = saveIncomingFile(payloadRelativeLocation, inputStream, encryptionActive, domibusPayloadManagerSpiForCurrentDomain, partInfo.getCompressed());
            partInfo.setLength(fileWriteResult.getTotalWrittenBytes());
            partInfo.setEncrypted(encryptionActive);
            partInfo.setPayloadManagerSPI(payloadManagerSpiForCurrentDomainIdentifier);

            if (storedInDatabase) {
                partInfo.setFileName(null);
                partInfo.setBinaryData(fileWriteResult.getBinaryData());
            } else {
                final Domain currentDomain = domainContextProvider.getCurrentDomain();
                //SPI get absolute location
                final String absolutePayloadLocation = domibusPayloadManagerSpiForCurrentDomain.getAbsolutePayloadLocation(currentDomain.getCode(), payloadRelativeLocation);
                partInfo.setFileName(absolutePayloadLocation);
            }

            //initialize the payloadDatahandler with the binaryData in order to avoid that the payload is decompressed again
            partInfo.loadBinary();
        }

        LOG.debug("Finished saving incoming payload [{}]", partInfo.getHref());
    }

    protected String createPayloadRelativeLocation() {
        final String uuid = UUID.randomUUID() + PAYLOAD_EXTENSION;
        Integer integerProperty = domibusPropertyProvider.getIntegerProperty(DOMIBUS_PAYLOAD_FILE_SYSTEM_FOLDER_ADDED_MINUTES);
        return partInfoService.getPayloadFolder(ZonedDateTime.now(ZoneOffset.UTC).plusMinutes(integerProperty)) + uuid;

    }

    private FileWriteResult saveIncomingFile(String relativeFileLocation,
                                             InputStream inputStream,
                                             boolean encryptionActive,
                                             DomibusPayloadManagerSpi domibusPayloadManagerSpiForCurrentDomain,
                                             Boolean compressed) throws IOException {

        final Domain currentDomain = domainContextProvider.getCurrentDomain();

        if (BooleanUtils.isTrue(compressed)) {
            // Uncompress the inputStream so IOUtils.copy returns the length of the uncompressed data
            inputStream = new GZIPInputStream(inputStream);
        }

        OutputStream originalOutputStream = null;
        OutputStream outputStream = null;
        try {
            originalOutputStream = domibusPayloadManagerSpiForCurrentDomain.getStreamForStoringPayload(currentDomain.getCode(), relativeFileLocation);
            outputStream = originalOutputStream;

            if (encryptionActive) {
                LOG.debug("Using encryption for file [{}]", relativeFileLocation);
                final Cipher encryptCipherForPayload = payloadEncryptionActive.getEncryptCipherForPayload();
                outputStream = new CipherOutputStream(outputStream, encryptCipherForPayload);
            }
            if (BooleanUtils.isTrue(compressed)) {
                // Uncompress the inputStream so IOUtils.copy returns the length of the uncompressed data
                outputStream = new GZIPOutputStream(outputStream);
            }
            final long total = IOUtils.copy(inputStream, outputStream, DEFAULT_BUFFER_SIZE);
            LOG.debug("Done writing file [{}]. Written [{}] bytes.", relativeFileLocation, total);

            if (domibusPayloadManagerSpiForCurrentDomain.arePayloadsStoredInDomibusDatabase()) {
                if (BooleanUtils.isTrue(compressed)) {
                    // if the GZipOutputStream is not closed, the originalOutputStream.toByteArray() will not be complete
                    outputStream.close();
                }
                //we need to get the byte array from the underlying ByteArrayOutputStream so that we can write the bytes in the database
                ByteArrayOutputStream byteArrayOutputStream = (ByteArrayOutputStream) originalOutputStream;
                final byte[] byteArray = byteArrayOutputStream.toByteArray();
                return new FileWriteResult(total, byteArray);
            } else {
                return new FileWriteResult(total, null);
            }
        } finally {
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
        }
    }

    @Override
    public void storeOutgoingPayload(PartInfo partInfo, UserMessage userMessage, LegConfiguration legConfiguration, String backendName) throws IOException, EbMS3Exception {
        //message fragment files are already saved on the file system
        if (!userMessage.isMessageFragment()) {
            saveOutgoingPayload(partInfo, userMessage, legConfiguration, backendName);
        }
    }

    protected void saveOutgoingPayload(PartInfo partInfo,
                                       UserMessage userMessage,
                                       LegConfiguration legConfiguration,
                                       String backendName) throws IOException, EbMS3Exception {
        final DomibusPayloadManagerSpi domibusPayloadManagerSpiForCurrentDomain = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpiForCurrentDomain();
        final String payloadManagerSpiForCurrentDomainIdentifier = domibusPayloadManagerSpiForCurrentDomain.getIdentifier();

        LOG.debug("Saving outgoing payload [{}] for file", partInfo.getHref());
        final String originalFileName = partInfo.getFileName();
        try (InputStream is = partInfo.getPayloadDatahandler().getInputStream()) {

            backendNotificationService.notifyPayloadSubmitted(userMessage, originalFileName, partInfo, backendName, legConfiguration.isAsyncNotification());

            final boolean storedInDatabase = domibusPayloadManagerSpiForCurrentDomain.arePayloadsStoredInDomibusDatabase();
            final String payloadRelativeLocation = createPayloadRelativeLocation();

            final Boolean encryptionActive = payloadEncryptionActive.isPayloadEncryptionActive(userMessage);
            FileWriteResult fileWriteResult = saveOutgoingFile(payloadRelativeLocation, partInfo, is, userMessage, legConfiguration, encryptionActive, domibusPayloadManagerSpiForCurrentDomain);
            partInfo.setLength(fileWriteResult.getTotalWrittenBytes());
            partInfo.setPayloadManagerSPI(payloadManagerSpiForCurrentDomainIdentifier);

            if (storedInDatabase) {
                partInfo.setFileName(null);
                partInfo.setBinaryData(fileWriteResult.getBinaryData());
            } else {
                final Domain currentDomain = domainContextProvider.getCurrentDomain();
                final String absolutePayloadLocation = domibusPayloadManagerSpiForCurrentDomain.getAbsolutePayloadLocation(currentDomain.getCode(), payloadRelativeLocation);
                partInfo.setFileName(absolutePayloadLocation);
            }
            payloadPersistenceHelper.validatePayloadSize(partInfo, false);
            LOG.debug("Finished saving outgoing payload [{}] for file", partInfo.getHref());
        }
        backendNotificationService.notifyPayloadProcessed(userMessage, originalFileName, partInfo, backendName, legConfiguration.isAsyncNotification());

    }

    protected FileWriteResult saveOutgoingFile(String relativeFileLocation,
                                               PartInfo partInfo,
                                               InputStream is,
                                               UserMessage userMessage,
                                               final LegConfiguration legConfiguration,
                                               final Boolean encryptionActive,
                                               DomibusPayloadManagerSpi domibusPayloadManagerSpiForCurrentDomain) throws IOException, EbMS3Exception {
        boolean useCompression = compressionService.handleCompression(userMessage.getMessageId(), partInfo, legConfiguration);
        LOG.debug("Compression for message with id: [{}] applied: [{}]", userMessage.getMessageId(), useCompression);

        final Domain currentDomain = domainContextProvider.getCurrentDomain();

        OutputStream originalOutputStream = null;
        OutputStream outputStream = null;
        long totalWrittenBytes = 0;
        try {
            originalOutputStream = domibusPayloadManagerSpiForCurrentDomain.getStreamForStoringPayload(currentDomain.getCode(), relativeFileLocation);//NOSONAR the stream is closed in the finally block
            outputStream = originalOutputStream;

            if (encryptionActive) {
                LOG.debug("Using encryption for file [{}]", relativeFileLocation);
                final Cipher encryptCipherForPayload = payloadEncryptionActive.getEncryptCipherForPayload();
                outputStream = new CipherOutputStream(outputStream, encryptCipherForPayload); //NOSONAR the stream is closed in the finally block
            }

            if (useCompression) {
                LOG.debug("Using compression for storing the file [{}]", relativeFileLocation);
                outputStream = new GZIPOutputStream(outputStream); //NOSONAR the stream is closed in the finally block
            }

            totalWrittenBytes = IOUtils.copy(is, outputStream, PayloadPersistence.DEFAULT_BUFFER_SIZE);
            LOG.debug("Done writing file [{}]. Written [{}] bytes.", partInfo.getHref(), totalWrittenBytes);
        } finally {
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
        }

        if (domibusPayloadManagerSpiForCurrentDomain.arePayloadsStoredInDomibusDatabase()) {
            //we need to get the byte array from the underlying ByteArrayOutputStream so that we can write the bytes in the database
            ByteArrayOutputStream byteArrayOutputStream = (ByteArrayOutputStream) originalOutputStream;
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            return new FileWriteResult(totalWrittenBytes, byteArray);
        } else {
            return new FileWriteResult(totalWrittenBytes, null);
        }
    }

    @Override
    public void onDomainAdded(Domain domain) {
        LOG.info("Initializing payload storage provider for domain [{}]", domain.getCode());

        final DomibusPayloadManagerSpi domibusPayloadManagerSpi = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpi(domain);
        domibusPayloadManagerSpi.initialize(domain.getCode());

        LOG.info("Finished initializing payload storage provider for domain [{}]", domain.getCode());
    }

    @Override
    public void onDomainRemoved(Domain domain) {

    }

    private static class FileWriteResult {
        long totalWrittenBytes;
        byte[] binaryData;

        public FileWriteResult(long totalWrittenBytes, byte[] binaryData) {
            this.totalWrittenBytes = totalWrittenBytes;
            this.binaryData = binaryData;
        }

        public long getTotalWrittenBytes() {
            return totalWrittenBytes;
        }

        public byte[] getBinaryData() {
            return binaryData;
        }
    }

}
