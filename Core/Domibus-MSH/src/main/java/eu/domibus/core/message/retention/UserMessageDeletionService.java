package eu.domibus.core.message.retention;

import eu.domibus.api.model.MessageStatus;
import eu.domibus.api.model.PartInfoFileNameSpiDto;
import eu.domibus.api.model.UserMessageLog;
import eu.domibus.api.model.UserMessageLogDto;
import eu.domibus.api.payload.PartInfoService;
import eu.domibus.common.JPAConstants;
import eu.domibus.core.error.ErrorLogService;
import eu.domibus.core.message.ReceiptDao;
import eu.domibus.core.message.UserMessageDao;
import eu.domibus.core.message.UserMessageLogDefaultService;
import eu.domibus.core.message.acknowledge.MessageAcknowledgementDao;
import eu.domibus.core.message.attempt.MessageAttemptDao;
import eu.domibus.core.message.nonrepudiation.SignalMessageRawEnvelopeDao;
import eu.domibus.core.message.nonrepudiation.UserMessageRawEnvelopeDao;
import eu.domibus.core.message.signal.SignalMessageDao;
import eu.domibus.core.message.signal.SignalMessageLogDao;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author François Gautier
 * @since 5.2
 */
@Service
public class UserMessageDeletionService {

    public static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(UserMessageDeletionService.class);
    public static final int BATCH_SIZE = 100;

    @Autowired
    private UserMessageDao userMessageDao;

    @Autowired
    private SignalMessageLogDao signalMessageLogDao;

    @Autowired
    private SignalMessageDao signalMessageDao;

    @Autowired
    private MessageAttemptDao messageAttemptDao;

    @Autowired
    private MessageAcknowledgementDao messageAcknowledgementDao;

    @Autowired
    private UserMessageLogDefaultService userMessageLogService;

    @Autowired
    private BackendNotificationService backendNotificationService;

    @Autowired
    private ErrorLogService errorLogService;

    @Autowired
    protected PartInfoService partInfoService;

    @Autowired
    SignalMessageRawEnvelopeDao signalMessageRawEnvelopeDao;

    @Autowired
    UserMessageRawEnvelopeDao userMessageRawEnvelopeDao;

    @Autowired
    ReceiptDao receiptDao;


    @PersistenceContext(unitName = JPAConstants.PERSISTENCE_UNIT_NAME)
    protected EntityManager em;

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteMessages(List<UserMessageLogDto> userMessageLogs) {
        em.unwrap(Session.class)
                .setJdbcBatchSize(BATCH_SIZE);

        List<Long> ids = userMessageLogs
                .stream()
                .map(UserMessageLogDto::getEntityId)
                .collect(Collectors.toList());

        deleteMessagesWithIDs(ids);

        try {
            ids.forEach(eid -> {
                UserMessageLog userMessageLog = userMessageLogService.findByEntityId(eid);
                backendNotificationService.notifyOfMessageStatusChange(userMessageLog, MessageStatus.DELETED, new Timestamp(System.currentTimeMillis()), userMessageLog.getAsyncNotificationOverride());
            });

        } catch (RuntimeException e) {
            LOG.warn("Error occurred while notifying message status change.", e);
            throw e;
        }

        backendNotificationService.notifyMessageDeleted(userMessageLogs);
        em.flush();
    }

    @Transactional
    public void deleteMessagesWithIDs(List<Long> ids) {

        LOG.debug("Deleting [{}] user messages", ids.size());
        LOG.trace("Deleting user messages [{}]", ids);

        List<PartInfoFileNameSpiDto> filenames = partInfoService.findFileSystemPayloadFilenames(ids);
        partInfoService.deletePayloadFiles(filenames);

        em.flush();
        int deleteResult = userMessageLogService.deleteMessageLogs(ids);
        logDeleted("Deleted [{}] userMessageLogs.", deleteResult);
        deleteResult = signalMessageLogDao.deleteMessageLogs(ids);
        logDeleted("Deleted [{}] signalMessageLogs.", deleteResult);
        deleteResult = signalMessageRawEnvelopeDao.deleteMessages(ids);
        logDeleted("Deleted [{}] signalMessageRaws.", deleteResult);
        deleteResult = receiptDao.deleteReceipts(ids);
        logDeleted("Deleted [{}] receipts.", deleteResult);
        deleteResult = signalMessageDao.deleteMessages(ids);
        logDeleted("Deleted [{}] signalMessages.", deleteResult);
        deleteResult = userMessageRawEnvelopeDao.deleteMessages(ids);
        logDeleted("Deleted [{}] userMessageRaws.", deleteResult);
        deleteResult = messageAttemptDao.deleteAttemptsByMessageIds(ids);
        logDeleted("Deleted [{}] attempts.", deleteResult);

        deleteResult = errorLogService.deleteErrorLogsByMessageIdInError(ids);
        logDeleted("Deleted [{}] deleteErrorLogsByMessageIdInError.", deleteResult);
        deleteResult = messageAcknowledgementDao.deleteMessageAcknowledgementsByMessageIds(ids);
        logDeleted("Deleted [{}] deleteMessageAcknowledgementsByMessageIds.", deleteResult);

        deleteResult = userMessageDao.deleteMessages(ids);
        logDeleted("Deleted [{}] userMessages.", deleteResult);
    }

    private void logDeleted(String message, int deletedNr) {
        if (deletedNr > 0) {
            LOG.info(message, deletedNr);
        }
    }

    @Transactional
    public void clearPayloadData(List<Long> entityIds) {
        if (CollectionUtils.isEmpty(entityIds)) {
            return;
        }
        try {
            entityIds.forEach(partInfoService::clearPayloadData);
            entityIds.forEach(eid -> {
                UserMessageLog userMessageLog = userMessageLogService.findByEntityId(eid);
                backendNotificationService.notifyOfMessageStatusChange(userMessageLog, MessageStatus.DELETED, new Timestamp(System.currentTimeMillis()), userMessageLog.getAsyncNotificationOverride());
            });
            userMessageLogService.update(entityIds, userMessageLogService::updateDeletedBatched);
        } catch (RuntimeException e) {
            LOG.warn("Cleaning payload failed with exception", e);
            throw e;
        }
    }
}
