package eu.domibus.core.configuration;

import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.configuration.spi.DomibusConfigurationManagerSpi;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author Cosmin Baciu
 * @since 5.2
 * Responsible for getting the Domibus configuration provider: it first tries to locate the configuration provider from the jar provided by the user; if it fails, it loads the default file system configuration provider
 */
public class DomibusConfigurationManagerSpiProvider {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusConfigurationManagerSpiProvider.class);

    private static DomibusConfigurationManagerSpi configurationProvider;

    public static DomibusConfigurationManagerSpi getConfigurationProvider() {
        if (configurationProvider == null) {
            configurationProvider = doGetConfigurationProvider();

            LOG.info("Initializing the DomibusConfigurationProvider");
            configurationProvider.initialize();
            LOG.info("Finished initializing the DomibusConfigurationProvider");
        }
        return configurationProvider;
    }

    public static boolean isCustomConfigurationProviderUsed() {
        String configurationProviderJarLocation = System.getProperty(DomibusConfigurationManagerSpi.DOMIBUS_CONFIGURATION_PROVIDER_LOCATION);
        if (StringUtils.isNotBlank(configurationProviderJarLocation)) {
            return true;
        }
        return false;
    }

    protected static DomibusConfigurationManagerSpi doGetConfigurationProvider() {
        LOG.info("Getting the DomibusConfigurationProvider");

        String configurationProviderJarLocation = System.getProperty(DomibusConfigurationManagerSpi.DOMIBUS_CONFIGURATION_PROVIDER_LOCATION);
        if (StringUtils.isNotBlank(configurationProviderJarLocation)) {
            return getCustomConfigurationProvider(configurationProviderJarLocation);
        }

        LOG.info("Using the default local file system provider");
        //default file system provider
        DomibusConfigurationManagerSpi domibusConfigurationProvider = new DomibusConfigurationProviderFileSystem();
        return domibusConfigurationProvider;
    }

    protected static DomibusConfigurationManagerSpi getCustomConfigurationProvider(String configurationProviderJarLocation) {
        LOG.info("Using Domibus configuration provider jar [{}]", configurationProviderJarLocation);

        final File file = new File(configurationProviderJarLocation);
        if (!file.exists()) {
            final String errorMessage = "Provided configuration provider jar [" + configurationProviderJarLocation + "] does not exist";
            LOG.error(errorMessage);
            throw new DomibusCoreException(errorMessage);
        }

        String configurationProviderClass = System.getProperty(DomibusConfigurationManagerSpi.DOMIBUS_CONFIGURATION_PROVIDER_CLASS);
        if (configurationProviderClass == null) {
            final String errorMessage = "Configuration class provided under system property  [" + DomibusConfigurationManagerSpi.DOMIBUS_CONFIGURATION_PROVIDER_CLASS + "] is empty";
            LOG.error(errorMessage);
            throw new DomibusCoreException(errorMessage);
        }

        URL configurationProviderJarURL = null;
        try {
            configurationProviderJarURL = file.toPath().toUri().toURL();
        } catch (MalformedURLException e) {
            final String errorMessage = "Could not get the URL value for the file [" + file + "]";
            LOG.error(errorMessage);
            throw new DomibusCoreException(errorMessage);
        }

        LOG.info("Instantiating Domibus configuration provider [{}]", configurationProviderClass);

        URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{configurationProviderJarURL}, Thread.currentThread().getContextClassLoader());
        try {
            final Class<?> configurationProviderImplementation = urlClassLoader.loadClass(configurationProviderClass);
            final DomibusConfigurationManagerSpi configurationProviderImplementationInstance = (DomibusConfigurationManagerSpi) configurationProviderImplementation.getDeclaredConstructor().newInstance();
            return configurationProviderImplementationInstance;
        } catch (Exception e) {
            final String errorMessage = "Could not instantiate class [" + configurationProviderClass + "]";
            LOG.error(errorMessage);
            throw new DomibusCoreException(errorMessage);
        }
    }
}
