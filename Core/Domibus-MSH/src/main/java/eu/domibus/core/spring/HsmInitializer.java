package eu.domibus.core.spring;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import java.security.Provider;
import java.security.Security;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author G. Maier
 * @since 5.2
 */
public class HsmInitializer {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(HsmInitializer.class);
    static final String SUN_PKCS_11 = "SunPKCS11";
    private static final int PROVIDER_WAS_ALREADY_CONFIGURED = -1;

    /**
     * @param providerName
     * @param pkcs11ConfigPath
     * @return Pkcs11 Security provider with the given name.
     * Standard PKCS11 providers can be configured and registered here from the standard Pkcs11 configuration file
     * and the provider name has to match the name in the config file prefixed with "SunPKCS11-".
     * Vendor specific providers that support PKCS11 need to be registered and configured externally
     * (for eg. in java.security file) and ready to use when Domibus starts.
     * If the provider with the given name is not found then it is assumed to be a PKCS11 standard provider to be configured here.
     */
    public Provider register (String providerName, String pkcs11ConfigPath) {
        LOG.info("Searching for HSM provider [{}]", providerName);
        Provider provider = Security.getProvider(providerName);
        if (provider == null) {
            LOG.info("The HSM provider was not created yet. Registering a new [{}] HSM provider based on [{}] at the last position in the list of security providers.", SUN_PKCS_11, pkcs11ConfigPath);
            provider = Security.getProvider(SUN_PKCS_11);
            provider = provider.configure(pkcs11ConfigPath);
            if (Security.addProvider(provider) == PROVIDER_WAS_ALREADY_CONFIGURED) {
                String providerNames = Arrays.stream(Security.getProviders()).map(Provider::getName).collect(Collectors.joining(","));
                LOG.debug("The [{}] provider was already configured. Available providers are [{}]", SUN_PKCS_11, providerNames);
            }
        } else {
            LOG.info("HSM provider found, ignoring configuration file [{}] ", pkcs11ConfigPath);
        }
        return provider;
    }
}
