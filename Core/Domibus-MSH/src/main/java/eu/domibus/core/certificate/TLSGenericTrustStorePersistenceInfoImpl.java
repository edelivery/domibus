package eu.domibus.core.certificate;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.property.DomibusRawPropertyProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import static eu.domibus.api.crypto.TLSGenericCertificateManager.TLS_GENERIC_TRUSTSTORE_NAME;
import static eu.domibus.api.multitenancy.DomainService.DOMAINS_HOME;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

@Service
public class TLSGenericTrustStorePersistenceInfoImpl implements KeystorePersistenceInfo {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSGenericTrustStorePersistenceInfoImpl.class);

    protected final DomibusPropertyProvider domibusPropertyProvider;
    protected final DomibusRawPropertyProvider domibusRawPropertyProvider;
    protected final DomibusConfigurationService domibusConfigurationService;
    protected final DomainContextProvider domainContextProvider;
    protected final CertificateHelper certificateHelper;

    public TLSGenericTrustStorePersistenceInfoImpl(DomibusPropertyProvider domibusPropertyProvider,
                                                   DomibusRawPropertyProvider domibusRawPropertyProvider,
                                                   DomibusConfigurationService domibusConfigurationService,
                                                   DomainContextProvider domainContextProvider,
                                                   CertificateHelper certificateHelper) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domibusRawPropertyProvider = domibusRawPropertyProvider;
        this.domibusConfigurationService = domibusConfigurationService;
        this.domainContextProvider = domainContextProvider;
        this.certificateHelper = certificateHelper;
    }

    @Override
    public String getName() {
        return TLS_GENERIC_TRUSTSTORE_NAME;
    }

    @Override
    public boolean isOptional() {
        return true;
    }

    @Override
    public String getFileLocation() {
        return domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_LOCATION);
    }

    @Override
    public String getType() {
        return domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_TYPE);
    }

    @Override
    public String getPassword() {
        return domibusRawPropertyProvider.getRawPropertyValue(DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_PASSWORD);
    }

    @Override
    public String toString() {
        return getName() + ":" + getFileLocation() + ":" + getType() + ":" + getPassword();
    }

    @Override
    public void updatePassword(String password) {
        domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_PASSWORD, password);
    }

    @Override
    public void updateType(String type) {
        domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_TYPE, type);
    }

    @Override
    public void updateFileLocation(String fileLocation) {
        domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_LOCATION, fileLocation);
    }

    @Override
    public boolean isDownloadable() {
        return true;
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public KeystorePersistenceInfo createDefault(String storeFileName, String password) {
        LOG.debug("Creating a default TLS Generic truststore with file name [{}].", storeFileName);

        return new TLSGenericTrustStorePersistenceInfoImpl(domibusPropertyProvider, domibusRawPropertyProvider,
                domibusConfigurationService, domainContextProvider, certificateHelper) {
            @Override
            public String getFileLocation() {
                if (domibusConfigurationService.isMultiTenantAware()) {
                    Domain currentDomain = domainContextProvider.getCurrentDomain();
                    String storeFilePath = DOMAINS_HOME + "/" + currentDomain + "/keystores/" + storeFileName;
                    return storeFilePath;
                }
                return "keystores/" + storeFileName;
            }

            @Override
            public String getPassword() {
                return password;
            }

            @Override
            public String getType() {
                return certificateHelper.getStoreType(storeFileName);
            }
        };
    }
}
