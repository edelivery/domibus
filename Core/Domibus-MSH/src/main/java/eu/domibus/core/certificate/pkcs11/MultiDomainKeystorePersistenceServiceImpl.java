package eu.domibus.core.certificate.pkcs11;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.pki.KeystorePersistenceService;
import eu.domibus.core.security.configuration.KeystorePersistenceServiceFactory;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Component;

import java.security.KeyStore;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * This component selects the KeystorePersistenceService implementation specific to the current domain and delegates the execution to that service
 *
 * @author G. Maier
 * @since 5.2
 */

@Component
public class MultiDomainKeystorePersistenceServiceImpl implements KeystorePersistenceService {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MultiDomainKeystorePersistenceServiceImpl.class);
    private final KeystorePersistenceServiceFactory keystorePersistenceServiceFactory;
    private final DomainContextProvider domainProvider;

    protected final ConcurrentMap<Domain, KeystorePersistenceService> domainKeystorePersistenceServiceMap = new ConcurrentHashMap<>();

    public MultiDomainKeystorePersistenceServiceImpl(KeystorePersistenceServiceFactory keystorePersistenceServiceFactory, DomainContextProvider domainProvider) {
        this.keystorePersistenceServiceFactory = keystorePersistenceServiceFactory;
        this.domainProvider = domainProvider;
    }

    protected KeystorePersistenceService getKeystorePersistenceService() {
        Domain domain = domainProvider.getCurrentDomainSafely();
        if (domain == null) {
            domain = new Domain();
            LOG.warn("Using empty current domain");
        }

        KeystorePersistenceService keystorePersistenceService = domainKeystorePersistenceServiceMap.computeIfAbsent(domain,
                keystorePersistenceServiceFactory::getKeystorePersistenceService);
        LOG.debug("Domain [{}] is using a [{}] keystore persistence service", domain, keystorePersistenceService.getClass().getName());
        return keystorePersistenceService;
    }

    @Override
    public KeystorePersistenceInfo getTrustStorePersistenceInfo() {
        return getKeystorePersistenceService().getTrustStorePersistenceInfo();
    }

    @Override
    public KeystorePersistenceInfo getKeyStorePersistenceInfo() {
        return getKeystorePersistenceService().getKeyStorePersistenceInfo();
    }

    @Override
    public KeyStoreContentInfo loadStore(KeystorePersistenceInfo keystorePersistenceInfo) {
        return getKeystorePersistenceService().loadStore(keystorePersistenceInfo);
    }

    @Override
    public void saveStoreFromDBToDisk(KeystorePersistenceInfo keystorePersistenceInfo) {
        getKeystorePersistenceService().saveStoreFromDBToDisk(keystorePersistenceInfo);
    }

    @Override
    public void saveStore(KeyStoreContentInfo contentInfo, KeystorePersistenceInfo persistenceInfo) {
        getKeystorePersistenceService().saveStore(contentInfo, persistenceInfo);
    }

    @Override
    public void saveStore(KeyStore store, KeystorePersistenceInfo persistenceInfo) {
        getKeystorePersistenceService().saveStore(store, persistenceInfo);
    }

    @Override
    public void saveStore(byte[] content, KeystorePersistenceInfo persistenceInfo) {
        getKeystorePersistenceService().saveStore(content, persistenceInfo);
    }
}
