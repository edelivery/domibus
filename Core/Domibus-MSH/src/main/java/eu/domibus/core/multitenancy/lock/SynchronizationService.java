package eu.domibus.core.multitenancy.lock;

import eu.domibus.api.multitenancy.lock.DbClusterSynchronizedRunnableFactory;
import eu.domibus.api.multitenancy.lock.DomibusSynchronizationException;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Executes a task with a lock (either a db lock for cluster deployment or a simple java lock otherwise)
 *
 * @author Ion Perpegel
 * @since 5.1.1
 */
@Service
public class SynchronizationService {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SynchronizationService.class);

    private final Map<String, Object> locks = new ConcurrentHashMap<>();

    private final DomibusConfigurationService domibusConfigurationService;

    private final DbClusterSynchronizedRunnableFactory dbClusterSynchronizedRunnableFactory;

    public SynchronizationService(DomibusConfigurationService domibusConfigurationService,
                                  DbClusterSynchronizedRunnableFactory dbClusterSynchronizedRunnableFactory) {
        this.domibusConfigurationService = domibusConfigurationService;
        this.dbClusterSynchronizedRunnableFactory = dbClusterSynchronizedRunnableFactory;
    }

    /**
     * Returns a task that is a synchronized wrapper of the original task,meaning that this task will sync the execution of the task received as a parameter
     *
     * @param task the task to excute with lock
     * @param dbLockKey the lock key in the TB_LOCK table
     * @param javaLockKey java object instance to sync on
     * @param <T> the type of the returned value of the task itself
     * @return the synchronized wrapper of the original task
     */
    public <T> Callable<T> getSynchronizedCallable(Callable<T> task, String dbLockKey, String javaLockKey) {
        Callable<T> synchronizedRunnable;
        if (domibusConfigurationService.isClusterDeployment()) {
            synchronizedRunnable = dbClusterSynchronizedRunnableFactory.synchronizedCallable(task, dbLockKey);
        } else {
            synchronizedRunnable = javaSyncCallable(task, javaLockKey);
        }
        return synchronizedRunnable;
    }

    /**
     * Executes the given task synchronously, with one of the following locking mechanisms:
     * Java synchronized locking in case of standalone deployment
     * Database locking in case of a cluster deployment
     *
     * @param task the task to execute with lock
     * @param dbLockKey the lock key in the TB_LOCK table
     * @param javaLockKey java object instance to sync on
     * @param <T> the type of the returned value of the task itself
     * @return the returned value of the task
     */
    public <T> T execute(Callable<T> task, String dbLockKey, String javaLockKey) {
        Callable<T> synchronizedRunnable = getSynchronizedCallable(task, dbLockKey, javaLockKey);
        try {
            return synchronizedRunnable.call();
        } catch (Exception e) {
            throw new DomibusSynchronizationException(e);
        }
    }

    public void execute(Runnable task, String dbLockKey, String javaLockKey) {
        Callable<Boolean> synchronizedRunnable = getSynchronizedCallable(() -> {
            task.run();
            return true;
        }, dbLockKey, javaLockKey);
        try {
            synchronizedRunnable.call();
        } catch (DomibusSynchronizationException se) {
            throw se;
        } catch (Exception e) {
            throw new DomibusSynchronizationException("Error executing a task with locks:" + dbLockKey + ", " + javaLockKey, e);
        }
    }

    public void execute(Runnable task, String dbLockKey) {
        execute(task, dbLockKey, null);
    }

    private <T> Callable<T> javaSyncCallable(Callable<T> task, String javaLockKey) {
        return () -> {
            if (javaLockKey != null) {
                synchronized (locks.computeIfAbsent(javaLockKey, k -> new Object())) {
                    return executeTask(task);
                }
            } else {
                return executeTask(task);
            }
        };
    }

    private <T> T executeTask(Callable<T> task) {
        try {
            LOG.debug("Handling sync execution with java lock.");
            T res = task.call();
            LOG.debug("Finished handling sync execution with java lock.");
            return res;
        } catch (Exception e) {
            throw new DomibusSynchronizationException("Error executing a callable task with java lock.", e);
        }
    }
}
