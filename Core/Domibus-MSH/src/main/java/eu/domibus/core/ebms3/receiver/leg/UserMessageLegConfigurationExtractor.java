package eu.domibus.core.ebms3.receiver.leg;

import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.MessageType;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.mapper.Ebms3Converter;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.message.MessageExchangeConfiguration;
import eu.domibus.core.pmode.provider.PModeProvider;
import org.apache.cxf.binding.soap.SoapMessage;

/**
 * @author Thomas Dussart
 * @since 3.3
 * <p>
 * Loading legconfiguration for an incoming usermessage.
 */

public class UserMessageLegConfigurationExtractor extends AbstractLegConfigurationExtractor {

    private PModeProvider pModeProvider;
    private Ebms3Converter ebms3Converter;

    public UserMessageLegConfigurationExtractor(SoapMessage message, Ebms3Messaging messaging) {
        super(message, messaging);
    }

    @Override
    protected String getMessageId() {
        return ebms3Messaging.getUserMessage().getMessageInfo().getMessageId();
    }

    @Override
    public LegConfiguration extractMessageConfiguration(SecurityProfile securityProfile) throws EbMS3Exception {
        final String pModeKey = getPModeKey(securityProfile);
        setUpMessage(pModeKey);
        return this.pModeProvider.getLegConfiguration(pModeKey);
    }

    protected String getPModeKey(SecurityProfile securityProfile) throws EbMS3Exception {
        message.put(MSHDispatcher.MESSAGE_TYPE_IN, MessageType.USER_MESSAGE);
        final UserMessage userMessage = ebms3Converter.convertFromEbms3(ebms3Messaging.getUserMessage());
        //TODO: EDELIVERY-13848: update DynamicDiscoveryPModeProvider.findUserMessageExchangeContext() to implement the method containing the SecurityProfile parameter as well
        //until then temporarily disable the support for multiple legs with same configuration but different security profiles
        final MessageExchangeConfiguration userMessageExchangeContext = this.pModeProvider.findUserMessageExchangeContext(userMessage, securityProfile, MSHRole.RECEIVING); // FIXME: This does not work for signalmessages
        return userMessageExchangeContext.getPmodeKey();
    }

    @Override
    public void accept(MessageLegConfigurationVisitor visitor) {
        visitor.visit(this);
    }

    public void setpModeProvider(PModeProvider pModeProvider) {
        this.pModeProvider = pModeProvider;
    }

    public void setEbms3Converter(Ebms3Converter ebms3Converter) {
        this.ebms3Converter = ebms3Converter;
    }
}
