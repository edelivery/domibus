package eu.domibus.core.pmode.validation.validators;

import eu.domibus.api.pmode.ValidationIssue;
import eu.domibus.common.model.configuration.Attachment;
import eu.domibus.common.model.configuration.Configuration;
import eu.domibus.common.model.configuration.Payload;
import eu.domibus.common.model.configuration.PayloadProfile;
import eu.domibus.core.pmode.validation.PModeValidationHelper;
import eu.domibus.core.pmode.validation.PModeValidator;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static eu.domibus.common.model.configuration.Payload.REGEX_MARKER;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

/**
 * Validator for Payload Profiles section of PMode
 *
 * @author Catalin Enache
 * @since 4.2
 */
@Component
@Order(8)
public class PayloadProfilesValidator implements PModeValidator {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PayloadProfilesValidator.class);

    final PModeValidationHelper pModeValidationHelper;

    public PayloadProfilesValidator(PModeValidationHelper pModeValidationHelper) {
        this.pModeValidationHelper = pModeValidationHelper;
    }

    @Override
    public List<ValidationIssue> validate(Configuration pMode) {
        List<ValidationIssue> issues = new ArrayList<>();

        Set<PayloadProfile> payloadProfiles = pMode.getBusinessProcesses().getPayloadProfiles();
        Set<Payload> validPayloads = pMode.getBusinessProcesses().getPayloads();
        if (payloadProfiles != null) {
            payloadProfiles.forEach(
                    payloadProfile -> issues.addAll(validatePayloadProfile(payloadProfile, validPayloads)));
        }
        return issues;
    }

    protected List<ValidationIssue> validatePayloadProfile(PayloadProfile payloadProfile, Set<Payload> validPayloads) {
        List<ValidationIssue> issues = new ArrayList<>();
        List<Attachment> attachmentList = pModeValidationHelper.getAttributeValue(payloadProfile, "attachment", List.class);

        checkMaxSize(payloadProfile);

        if (CollectionUtils.isEmpty(attachmentList)) {
            LOG.debug("No attachment in payloadProfile [{}]", payloadProfile.getName());
            return issues;
        }

        issues.addAll(validateAttachmentExists(payloadProfile, validPayloads, attachmentList));

        List<Payload> selectedPayload = getSelectedPayloads(validPayloads, attachmentList);
        LOG.debug("PayloadProfile [{}] has attachments : [{}] matching with payloads [{}]", payloadProfile.getName(), attachmentList, validPayloads);

        issues.addAll(validateRegexPayload(payloadProfile, selectedPayload));

        return issues;
    }

    private List<ValidationIssue> validateAttachmentExists(PayloadProfile payloadProfile, Set<Payload> validPayloads, List<Attachment> attachmentList) {
        List<ValidationIssue> issues = new ArrayList<>();
        for (Attachment attachment : attachmentList) {
            if (noneMatch(validPayloads, attachment)) {
                issues.add(getValidationIssue(payloadProfile, validPayloads, attachment));
            }
        }
        return issues;
    }

    private List<Payload> getSelectedPayloads(Set<Payload> validPayloads, List<Attachment> attachmentList) {
        // attachments should correspond to existing payloads
        List<Payload> selectedPayload = new ArrayList<>();
        for (Attachment attachment : attachmentList) {
            findMatch(validPayloads, attachment).ifPresent(selectedPayload::add);
        }
        return selectedPayload;
    }

    private void checkMaxSize(PayloadProfile payloadProfile) {
        Long maxSize = payloadProfile.getMaxSize();
        if (maxSize == null || maxSize < 0) {
            LOG.debug("No validation will be made for payloadProfile [{}] as maxSize has the value [{}]", payloadProfile.getName(), maxSize);
        }
    }

    private ValidationIssue getValidationIssue(PayloadProfile payloadProfile, Set<Payload> validPayloads, Attachment attachment) {
        return pModeValidationHelper.createValidationIssue("Attachment [%s] of payload profile [%s] not found among the defined payloads [%s]", attachment.getName(), payloadProfile.getName(), "" + validPayloads);
    }

    private Optional<Payload> findMatch(Set<Payload> validPayloads, Attachment attachment) {
        return validPayloads
                .stream()
                .filter(matchName(attachment))
                .findAny();
    }

    private Predicate<Payload> matchName(Attachment attachment) {
        return payload -> equalsIgnoreCase(payload.getName(), attachment.getName());
    }

    private boolean noneMatch(Set<Payload> validPayloads, Attachment attachment) {
        return validPayloads
                .stream()
                .noneMatch(matchName(attachment));
    }

    private List<ValidationIssue> validateRegexPayload(PayloadProfile payloadProfile, List<Payload> selectedPayload) {
        List<ValidationIssue> issues = new ArrayList<>();
        LOG.debug("Validate Unique regex payload in payload profile [{}] in list [{}] ", payloadProfile.getName(), selectedPayload);
        List<Payload> collect = selectedPayload
                .stream()
                .filter(payload -> StringUtils.containsIgnoreCase(payload.getCid(), REGEX_MARKER) || StringUtils.containsIgnoreCase(payload.getMimeType(), REGEX_MARKER))
                .collect(Collectors.toList());

        if (collect.size() > 1) {
            issues.addAll(
                    collect
                            .stream()
                            .map(payload -> createIssue(payloadProfile, payload.getName(),
                                    "Only one payload is allowed with regex value in the cid AND/OR mimeType. See [%s]"))
                            .collect(Collectors.toList()));
        }
        return issues;
    }

    protected ValidationIssue createIssue(PayloadProfile payloadProfile, String name, String message) {
        return pModeValidationHelper.createValidationIssue(message, name, payloadProfile.getName());
    }
}
