package eu.domibus.core.util;

import eu.domibus.api.ebms3.Ebms3Constants;
import eu.domibus.api.ebms3.model.*;
import eu.domibus.api.ebms3.model.mf.Ebms3MessageFragmentType;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusDateTimeException;
import eu.domibus.api.message.UserMessageException;
import eu.domibus.api.messaging.MessagingException;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.SecurityProfileMetadata;
import eu.domibus.api.util.xml.XMLUtil;
import eu.domibus.common.ErrorCode;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.SoapElementsExtractorUtilImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.WebServiceException;
import java.io.StringWriter;
import java.util.*;

import static eu.domibus.core.ebms3.sender.client.DispatchClientDefaultProvider.MESSAGING_WITH_SECURITY_PROFILE_METADATA;
import static org.apache.wss4j.common.WSS4JConstants.WSU_NS;
import static org.apache.wss4j.common.WSS4JConstants.WSU_PREFIX;

/**
 * @author Thomas Dussart
 * @author Cosmin Baciu
 * @since 3.3
 */
@Service
public class MessageUtil {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageUtil.class);

    public static final String LOCAL_NAME = "Id";
    public static final String MESSAGE_INFO = "MessageInfo";
    public static final String TIMESTAMP = "Timestamp";
    public static final String MESSAGE_ID = "MessageId";
    public static final String REF_TO_MESSAGE_ID = "RefToMessageId";
    public static final String RECEIPT = "Receipt";
    public static final String PULL_REQUEST = "PullRequest";
    public static final String NON_REPUDIATION_INFORMATION = "NonRepudiationInformation";
    public static final String ERROR_DETAIL = "ErrorDetail";
    public static final String DESCRIPTION = "Description";
    public static final String LANG = "lang";
    public static final String SHORT_DESCRIPTION = "shortDescription";
    public static final String SEVERITY = "severity";
    public static final String REF_TO_MESSAGE_IN_ERROR = "refToMessageInError";
    public static final String ORIGIN = "origin";
    public static final String ERROR_CODE = "errorCode";
    public static final String CATEGORY = "category";
    public static final String ERROR = "Error";
    public static final String SIGNAL_MESSAGE = "SignalMessage";
    public static final String USER_MESSAGE = "UserMessage";
    public static final String PARTY_INFO = "PartyInfo";
    public static final String ROLE = "Role";
    public static final String FROM = "From";
    public static final String PARTY_ID = "PartyId";
    public static final String TO = "To";
    public static final String COLLABORATION_INFO = "CollaborationInfo";
    public static final String CONVERSATION_ID = "ConversationId";
    public static final String ACTION = "Action";
    public static final String SERVICE = "Service";
    public static final String MESSAGE_PROPERTIES = "MessageProperties";
    public static final String PAYLOAD_INFO = "PayloadInfo";
    public static final String PART_INFO = "PartInfo";
    public static final String PROPERTY = "Property";
    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String HREF = "href";
    public static final String PART_PROPERTIES = "PartProperties";
    public static final String SIGNATURE = "Signature";

    protected final JAXBContext jaxbContext;

    protected final JAXBContext jaxbContextMessageFragment;

    protected final DomibusDateFormatter domibusDateFormatter;

    protected final SoapUtil soapUtil;

    protected XMLUtil xmlUtil;

    protected DomibusPropertyProvider domibusPropertyProvider;

    protected DomibusDomUtil domibusDomUtil;

    protected SoapElementsExtractorUtilImpl soapElementsExtractorUtil;

    public MessageUtil(@Qualifier("jaxbContextEBMS") JAXBContext jaxbContext,
                       @Qualifier("jaxbContextMessageFragment") JAXBContext jaxbContextMessageFragment,
                       DomibusDateFormatter domibusDateFormatter,
                       SoapUtil soapUtil,
                       XMLUtil xmlUtil,
                       DomibusPropertyProvider domibusPropertyProvider,
                       SoapElementsExtractorUtilImpl soapElementsExtractorUtil,
                       DomibusDomUtil domibusDomUtil) {
        this.jaxbContext = jaxbContext;
        this.jaxbContextMessageFragment = jaxbContextMessageFragment;
        this.domibusDateFormatter = domibusDateFormatter;
        this.soapUtil = soapUtil;
        this.xmlUtil = xmlUtil;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.soapElementsExtractorUtil = soapElementsExtractorUtil;
        this.domibusDomUtil = domibusDomUtil;
    }

    @SuppressWarnings("unchecked")
    public Ebms3Messaging getMessaging(final SOAPMessage soapMessage) throws SOAPException, JAXBException, XMLStreamException, TransformerException {
        LOG.debug("Unmarshalling the Messaging instance from the SOAPMessage");

        final Node messagingXml = soapMessage.getSOAPHeader().getChildElements(ObjectFactory._Messaging_QNAME).next();

        XMLStreamReader reader = xmlUtil.getXmlStreamReaderFromNode(messagingXml);

        Ebms3Messaging ebms3Messaging;
        try {
            final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller(); //Those are not thread-safe, therefore a new one is created each call
            final JAXBElement<Ebms3Messaging> root = (JAXBElement<Ebms3Messaging>) unmarshaller.unmarshal(reader);
            ebms3Messaging = root.getValue();
        } finally {
            reader.close();
        }
        return ebms3Messaging;
    }

    /**
     * Extract the Messaging object using DOM API instead of JAXB
     *
     * @throws SOAPException  in the case of a Technical error while parsing the {@link SOAPMessage}
     * @throws EbMS3Exception in the case of a Business error while parsing the {@link SOAPMessage}
     */
    public Ebms3Messaging getMessagingWithDom(final SOAPMessage soapMessage) throws SOAPException, EbMS3Exception {
        final Node messagingNode = getMessagingNode(soapMessage);
        return getMessagingWithDom(messagingNode);
    }

    public Node getMessagingNode(SOAPMessage soapMessage) throws SOAPException {
        return soapMessage.getSOAPHeader().getChildElements(ObjectFactory._Messaging_QNAME).next();
    }

    public Ebms3MessagingWithSecurityProfileMetadata getMessagingAndSecurityProfileMetadataWithDom(final Element soapEnvelope,
                                                                                                   final Node messagingNode) throws SOAPException, EbMS3Exception {
        SecurityProfileMetadata securityProfileMetadata = getSecurityProfileMetadata(soapEnvelope);
        final Ebms3Messaging messagingWithDom = getMessagingWithDom(messagingNode);
        return new Ebms3MessagingWithSecurityProfileMetadata(messagingWithDom, securityProfileMetadata);
    }

    private SecurityProfileMetadata getSecurityProfileMetadata(Element soapEnvelope) {
        final Node soapEnvelopeHeader = domibusDomUtil.getFirstChild(soapEnvelope, "Header");
        if (soapEnvelopeHeader == null) {
            LOG.warn("No Soap Envelope Header found in the messaging node");
            return null;
        }

        final Node securityHeaderNode = domibusDomUtil.getFirstChild(soapEnvelopeHeader, "Security");
        if (securityHeaderNode == null) {
            LOG.warn("No Security header found in the Soap Envelope");
            return null;
        }
        return soapElementsExtractorUtil.extractSecurityProfileMetadata(securityHeaderNode);
    }

    public Ebms3Messaging getMessagingWithDom(final Node messagingNode) throws SOAPException, EbMS3Exception {
        LOG.debug("Creating the Messaging instance from the SOAPMessage using DOM processing");

        if (messagingNode == null) {
            throw new UserMessageException(DomibusCoreErrorCode.DOM_007, "Could not found Messaging node");
        }

        try {
            Ebms3Messaging ebms3Messaging = new Ebms3Messaging();

            final Ebms3SignalMessage ebms3SignalMessage = createSignalMessage(messagingNode);
            ebms3Messaging.setSignalMessage(ebms3SignalMessage);

            final Ebms3UserMessage ebms3UserMessage = createUserMessage(messagingNode);
            ebms3Messaging.setUserMessage(ebms3UserMessage);

            final Map<QName, String> otherAttributes = getOtherAttributes(messagingNode);
            if (otherAttributes != null) {
                ebms3Messaging.getOtherAttributes().putAll(otherAttributes);
            }

            LOG.debug("Finished creating the Messaging instance from the SOAPMessage using DOM processing");
            return ebms3Messaging;
        } catch (DomibusDateTimeException e) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0003)
                    .message(e.getMessage())
                    .cause(e)
                    .build();
        }
    }

    protected Ebms3UserMessage createUserMessage(Node messagingNode) {
        final Node userMessageNode = domibusDomUtil.getFirstChild(messagingNode, USER_MESSAGE);
        if (userMessageNode == null) {
            LOG.debug("UserMessage is null");
            return null;
        }
        Ebms3UserMessage result = new Ebms3UserMessage();

        final String mpc = getAttribute(userMessageNode, "mpc");
        LOG.debug("Check incoming message mpc value:[{}] and assign default mpc value:[{}] if empty", mpc, Ebms3Constants.DEFAULT_MPC);
        result.setMpc(StringUtils.isEmpty(mpc) ? Ebms3Constants.DEFAULT_MPC : mpc);

        final Ebms3MessageInfo ebms3MessageInfo = createMessageInfo(userMessageNode);
        result.setMessageInfo(ebms3MessageInfo);

        Ebms3PartyInfo ebms3PartyInfo = createPartyInfo(userMessageNode);
        result.setPartyInfo(ebms3PartyInfo);

        Ebms3CollaborationInfo ebms3CollaborationInfo = createCollaborationInfo(userMessageNode);
        result.setCollaborationInfo(ebms3CollaborationInfo);

        Ebms3MessageProperties ebms3MessageProperties = createMessageProperties(userMessageNode);
        result.setMessageProperties(ebms3MessageProperties);

        Ebms3PayloadInfo ebms3PayloadInfo = createPayloadInfo(userMessageNode);
        result.setPayloadInfo(ebms3PayloadInfo);

        return result;
    }

    protected Ebms3PayloadInfo createPayloadInfo(Node userMessageNode) {
        final Node payloadInfoNode = domibusDomUtil.getFirstChild(userMessageNode, PAYLOAD_INFO);
        if (payloadInfoNode == null) {
            LOG.debug("PayloadInfo is null");
            return null;
        }
        LOG.debug("Creating PayloadInfo");

        Ebms3PayloadInfo result = new Ebms3PayloadInfo();

        final List<Node> partInfoNodes = domibusDomUtil.getChildren(payloadInfoNode, PART_INFO);
        for (Node partInfoNode : partInfoNodes) {
            Ebms3PartInfo ebms3PartInfo = createPartInfo(partInfoNode);
            result.getPartInfo().add(ebms3PartInfo);
        }

        return result;
    }

    private Ebms3PartInfo createPartInfo(Node partInfoNode) {
        Ebms3PartInfo result = new Ebms3PartInfo();

        final String href = getAttribute(partInfoNode, HREF);
        result.setHref(href);
        Ebms3PartProperties ebms3PartProperties = createPartProperties(partInfoNode);
        result.setPartProperties(ebms3PartProperties);

        return result;
    }

    protected Ebms3PartProperties createPartProperties(Node partInfoNode) {
        final Node partPropertiesNode = domibusDomUtil.getFirstChild(partInfoNode, PART_PROPERTIES);
        if (partPropertiesNode == null) {
            LOG.debug("PartProperties is null");
            return null;
        }
        LOG.debug("Creating createPartProperties");

        Ebms3PartProperties result = new Ebms3PartProperties();
        Set<Ebms3Property> properties = new HashSet<>();


        final List<Node> propertyNodes = domibusDomUtil.getChildren(partPropertiesNode, PROPERTY);
        for (Node propertyNode : propertyNodes) {
            final Ebms3Property ebms3Property = createProperty(propertyNode);
            properties.add(ebms3Property);
        }
        result.setProperty(properties);

        return result;
    }

    protected Ebms3MessageProperties createMessageProperties(Node userMessageNode) {
        final Node messagePropertiesNode = domibusDomUtil.getFirstChild(userMessageNode, MESSAGE_PROPERTIES);
        if (messagePropertiesNode == null) {
            LOG.debug("MessageProperties is null");
            return null;
        }
        LOG.debug("Creating MessageProperties");

        Ebms3MessageProperties result = new Ebms3MessageProperties();

        final List<Node> propertyNodes = domibusDomUtil.getChildren(messagePropertiesNode, PROPERTY);
        for (Node propertyNode : propertyNodes) {
            final Ebms3Property ebms3Property = createProperty(propertyNode);
            result.getProperty().add(ebms3Property);
        }

        return result;
    }

    protected Ebms3Property createProperty(Node propertyNode) {
        Ebms3Property result = new Ebms3Property();
        final String name = getAttribute(propertyNode, NAME);
        final String type = getAttribute(propertyNode, TYPE);
        final String value = domibusDomUtil.getTextContent(propertyNode);

        result.setName(name);
        result.setType(type);
        result.setValue(value);

        return result;
    }

    protected Ebms3CollaborationInfo createCollaborationInfo(Node userMessageNode) {
        final Node collaborationInfoNode = domibusDomUtil.getFirstChild(userMessageNode, COLLABORATION_INFO);
        if (collaborationInfoNode == null) {
            LOG.debug("CollaborationInfo is null");
            return null;
        }
        LOG.debug("Creating CollaborationInfo");

        Ebms3CollaborationInfo result = new Ebms3CollaborationInfo();
        Ebms3Service ebms3Service = createService(collaborationInfoNode);
        result.setService(ebms3Service);

        final String conversationId = domibusDomUtil.getFirstChildValue(collaborationInfoNode, CONVERSATION_ID);
        result.setConversationId(conversationId);

        final String action = domibusDomUtil.getFirstChildValue(collaborationInfoNode, ACTION);
        result.setAction(action);

        Ebms3AgreementRef agreement = createAgreementRef(collaborationInfoNode);
        result.setAgreementRef(agreement);

        return result;
    }

    protected Ebms3AgreementRef createAgreementRef(Node collaborationInfoNode) {
        final Node agreementRefNode = domibusDomUtil.getFirstChild(collaborationInfoNode, "AgreementRef");
        if (agreementRefNode == null) {
            LOG.debug("AgreementRef is null");
            return null;
        }
        LOG.debug("Creating AgreementRef");

        final String serviceType = getAttribute(agreementRefNode, "type");
        final String serviceValue = domibusDomUtil.getTextContent(agreementRefNode);

        Ebms3AgreementRef result = new Ebms3AgreementRef();
        result.setValue(serviceValue);
        result.setType(serviceType);
        return result;
    }

    protected Ebms3Service createService(Node collaborationInfoNode) {
        final Node serviceNode = domibusDomUtil.getFirstChild(collaborationInfoNode, SERVICE);
        if (serviceNode == null) {
            LOG.debug("Service is null");
            return null;
        }
        LOG.debug("Creating Service");

        final String serviceType = getAttribute(serviceNode, "type");
        final String serviceValue = domibusDomUtil.getTextContent(serviceNode);

        Ebms3Service result = new Ebms3Service();
        result.setValue(serviceValue);
        result.setType(serviceType);
        return result;
    }

    protected Ebms3PartyInfo createPartyInfo(Node userMessageNode) {
        final Node partyInfoNode = domibusDomUtil.getFirstChild(userMessageNode, PARTY_INFO);
        if (partyInfoNode == null) {
            LOG.debug("PartyInfo is null");
            return null;
        }
        LOG.debug("Creating PartyInfo");

        Ebms3PartyInfo result = new Ebms3PartyInfo();

        final Ebms3From ebms3From = createFrom(partyInfoNode);
        result.setFrom(ebms3From);

        final Ebms3To ebms3To = createTo(partyInfoNode);
        result.setTo(ebms3To);

        return result;
    }

    protected Ebms3To createTo(Node partyInfoNode) {
        final Node toNode = domibusDomUtil.getFirstChild(partyInfoNode, TO);
        if (toNode == null) {
            LOG.debug("To is null");
            return null;
        }
        Ebms3To result = new Ebms3To();
        final String role = domibusDomUtil.getFirstChildValue(toNode, ROLE);
        result.setRole(role);
        final Set<Ebms3PartyId> ebms3PartyIds = createPartyIds(toNode);
        result.getPartyId().addAll(ebms3PartyIds);

        return result;
    }

    protected Ebms3From createFrom(final Node partyInfoNode) {
        final Node fromNode = domibusDomUtil.getFirstChild(partyInfoNode, FROM);
        if (fromNode == null) {
            LOG.debug("From is null");
            return null;
        }
        Ebms3From result = new Ebms3From();
        final String role = domibusDomUtil.getFirstChildValue(fromNode, ROLE);
        result.setRole(role);
        final Set<Ebms3PartyId> ebms3PartyIds = createPartyIds(fromNode);
        result.getPartyId().addAll(ebms3PartyIds);


        return result;
    }

    protected Set<Ebms3PartyId> createPartyIds(Node parent) {
        final List<Node> partyIdNodes = domibusDomUtil.getChildren(parent, PARTY_ID);

        Set<Ebms3PartyId> result = new HashSet<>();
        for (Node partyIdNode : partyIdNodes) {
            final Ebms3PartyId ebms3PartyId = createPartyId(partyIdNode);
            result.add(ebms3PartyId);
        }

        return result;
    }

    protected Ebms3PartyId createPartyId(Node partyIdNode) {
        final String partyType = getAttribute(partyIdNode, TYPE);
        final String partyValue = domibusDomUtil.getTextContent(partyIdNode);

        Ebms3PartyId result = new Ebms3PartyId();
        result.setType(partyType);
        result.setValue(partyValue);

        return result;
    }

    protected Ebms3SignalMessage createSignalMessage(final Node messagingNode) throws SOAPException {
        final Node signalNode = domibusDomUtil.getFirstChild(messagingNode, SIGNAL_MESSAGE);
        if (signalNode == null) {
            LOG.debug("SignalMessage is null");
            return null;
        }
        LOG.debug("Creating SignalMessage");

        Ebms3SignalMessage result = new Ebms3SignalMessage();

        final Ebms3MessageInfo ebms3MessageInfo = createMessageInfo(signalNode);
        result.setMessageInfo(ebms3MessageInfo);

        final Ebms3Receipt ebms3Receipt = createReceipt(signalNode);
        result.setReceipt(ebms3Receipt);

        Ebms3PullRequest ebms3PullRequest = createPullRequest(signalNode);
        result.setPullRequest(ebms3PullRequest);

        Set<Ebms3Error> ebms3Error = createErrors(signalNode);
        if (CollectionUtils.isNotEmpty(ebms3Error)) {
            result.getError().addAll(ebms3Error);
        }
        return result;
    }

    protected Map<QName, String> getOtherAttributes(final Node messagingNode) {
        Map<QName, String> result = new HashMap<>();

        final NamedNodeMap attributes = messagingNode.getAttributes();
        if (attributes == null) {
            LOG.debug("Messaging node attributes is empty");
            return null;
        }

        final Node namedItemNS = attributes.getNamedItemNS(WSU_NS, LOCAL_NAME);
        if (namedItemNS == null) {
            LOG.debug("No named item found with namespace [{}] and local name [{}]", WSU_NS, LOCAL_NAME);
            return null;
        }

        final String nodeValue = namedItemNS.getNodeValue();
        LOG.debug("Value for named item [{}] with namespace [{}] is [{}]", LOCAL_NAME, WSU_NS, nodeValue);

        final QName name = new QName(WSU_NS, LOCAL_NAME, WSU_PREFIX);
        result.put(name, nodeValue);

        return result;
    }

    protected Set<Ebms3Error> createErrors(Node signalNode) {
        Set<Ebms3Error> result = new HashSet<>();

        final List<Node> errorNodeList = domibusDomUtil.getChildren(signalNode, ERROR);
        if (CollectionUtils.isEmpty(errorNodeList)) {
            LOG.debug("Errors node is null");
            return result;
        }

        LOG.debug("Creating Errors");

        for (Node errorNode : errorNodeList) {
            Ebms3Error ebms3Error = createError(errorNode);
            result.add(ebms3Error);
        }

        return result;
    }

    protected Ebms3Error createError(Node errorNode) {
        Ebms3Error result = new Ebms3Error();
        final String category = getAttribute(errorNode, CATEGORY);
        result.setCategory(category);
        final String errorCode = getAttribute(errorNode, ERROR_CODE);
        result.setErrorCode(errorCode);
        final String origin = getAttribute(errorNode, ORIGIN);
        result.setOrigin(origin);
        final String refToMessageInError = getAttribute(errorNode, REF_TO_MESSAGE_IN_ERROR);
        result.setRefToMessageInError(refToMessageInError);
        final String severity = getAttribute(errorNode, SEVERITY);
        result.setSeverity(severity);
        final String shortDescription = getAttribute(errorNode, SHORT_DESCRIPTION);
        result.setShortDescription(shortDescription);

        final Ebms3Description ebms3Description = createDescription(errorNode);
        if (ebms3Description != null) {
            result.setDescription(ebms3Description);
        }
        final String errorDetail = getErrorDetail(errorNode);
        result.setErrorDetail(errorDetail);

        return result;
    }


    protected String getErrorDetail(Node errorNode) {
        final Node errorDetailNode = domibusDomUtil.getFirstChild(errorNode, ERROR_DETAIL);
        if (errorDetailNode == null) {
            LOG.debug("ErrorDetail node is null");
            return null;
        }
        return domibusDomUtil.getTextContent(errorDetailNode);
    }

    protected Ebms3Description createDescription(Node errorNode) {
        final Node description = domibusDomUtil.getFirstChild(errorNode, DESCRIPTION);
        if (description == null) {
            return null;
        }
        final String lang = getAttribute(errorNode, LANG);
        final String textContent = domibusDomUtil.getTextContent(description);
        Ebms3Description result = new Ebms3Description();
        result.setLang(lang);
        result.setValue(textContent);

        return result;
    }

    protected String getAttribute(Node referenceNode, String attributeName) {
        final NamedNodeMap attributes = referenceNode.getAttributes();
        if (attributes == null) {
            return null;
        }
        final Node uri = attributes.getNamedItem(attributeName);
        if (uri == null) {
            return null;
        }
        return domibusDomUtil.getTextContent(uri);
    }

    protected Ebms3Receipt createReceipt(final Node signalNode) throws SOAPException {
        final Node receiptNode = domibusDomUtil.getFirstChild(signalNode, RECEIPT);
        if (receiptNode == null) {
            LOG.debug("Receipt node is null");
            return null;
        }

        LOG.debug("Creating Receipt");
        Ebms3Receipt ebms3Receipt = new Ebms3Receipt();

        final String nonRepudiationInformation = getNonRepudiationInformationFromReceipt(receiptNode);
        if (StringUtils.isNotEmpty(nonRepudiationInformation)) {
            LOG.debug("Adding [{}] to the Receipt", NON_REPUDIATION_INFORMATION);
            ebms3Receipt.getAny().add(nonRepudiationInformation);
        }
        String userMessageFromReceipt = getUserMessageFromReceipt(receiptNode);
        if (StringUtils.isNotEmpty(userMessageFromReceipt)) {
            LOG.debug("Adding [{}] to the Receipt", USER_MESSAGE);
            ebms3Receipt.getAny().add(userMessageFromReceipt);
        }

        return ebms3Receipt;
    }

    protected String getNonRepudiationInformationFromReceipt(final Node receiptNode) throws SOAPException {
        final Node nonRepudiationInformationNode = domibusDomUtil.getFirstChild(receiptNode, NON_REPUDIATION_INFORMATION);
        if (nonRepudiationInformationNode == null) {
            LOG.debug("No [{}] found", NON_REPUDIATION_INFORMATION);
            return null;
        }

        try {
            return nodeToString(nonRepudiationInformationNode);
        } catch (TransformerException e) {
            throw new SOAPException("Error while getting NonRepudiationInformation", e);
        }
    }

    protected String getUserMessageFromReceipt(final Node receiptNode) throws SOAPException {
        final Node userMessageNode = domibusDomUtil.getFirstChild(receiptNode, USER_MESSAGE);
        if (userMessageNode == null) {
            LOG.debug("No [{}] found", USER_MESSAGE);
            return null;
        }

        try {
            return nodeToString(userMessageNode);
        } catch (TransformerException e) {
            throw new SOAPException("Error while getting UserMessage", e);
        }
    }

    protected Ebms3PullRequest createPullRequest(Node signalNode) {
        final Node pullRequestNode = domibusDomUtil.getFirstChild(signalNode, PULL_REQUEST);
        if (pullRequestNode == null) {
            LOG.debug("PullRequest is null");
            return null;
        }
        LOG.debug("Creating PullRequest");

        Ebms3PullRequest result = new Ebms3PullRequest();
        final String mpc = getAttribute(pullRequestNode, "mpc");
        result.setMpc(StringUtils.isEmpty(mpc) ? Ebms3Constants.DEFAULT_MPC : mpc);

        return result;
    }

    protected String nodeToString(final Node node) throws TransformerException {
        final StringWriter sw = new StringWriter();
        final Transformer t = xmlUtil.getTransformerFactory().newTransformer();
        t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        t.transform(new DOMSource(node), new StreamResult(sw));
        return sw.toString();
    }

    protected Ebms3MessageInfo createMessageInfo(final Node signalNode) {
        final Node messageInfoNode = domibusDomUtil.getFirstChild(signalNode, MESSAGE_INFO);
        if (messageInfoNode == null) {
            LOG.debug("MessageInfo is null");
            return null;
        }
        LOG.debug("Creating MessageInfo");

        Ebms3MessageInfo ebms3MessageInfo = new Ebms3MessageInfo();
        final String timestampString = domibusDomUtil.getFirstChildValue(messageInfoNode, TIMESTAMP);
        if (timestampString != null) {
            final Date date = domibusDateFormatter.fromString(timestampString);
            ebms3MessageInfo.setTimestamp(date);
        }

        final String messageId = domibusDomUtil.getFirstChildValue(messageInfoNode, MESSAGE_ID);
        ebms3MessageInfo.setMessageId(messageId);

        final String refToMessageId = domibusDomUtil.getFirstChildValue(messageInfoNode, REF_TO_MESSAGE_ID);
        ebms3MessageInfo.setRefToMessageId(refToMessageId);

        return ebms3MessageInfo;
    }

    @SuppressWarnings("unchecked")
    public Ebms3MessageFragmentType getMessageFragment(final SOAPMessage request) {
        try {
            LOG.debug("Unmarshalling the MessageFragmentType instance from the request");
            final Iterator iterator = request.getSOAPHeader().getChildElements(eu.domibus.api.ebms3.model.mf.ObjectFactory._MessageFragment_QNAME);
            if (!iterator.hasNext()) {
                return null;
            }

            final Node messagingXml = (Node) iterator.next();
            XMLStreamReader reader = xmlUtil.getXmlStreamReaderFromNode(messagingXml);

            final Unmarshaller unmarshaller = jaxbContextMessageFragment.createUnmarshaller(); //Those are not thread-safe, therefore a new one is created each call
            final JAXBElement<Ebms3MessageFragmentType> root = (JAXBElement<Ebms3MessageFragmentType>) unmarshaller.unmarshal(reader);
            final Ebms3MessageFragmentType ebms3MessageFragmentType = root.getValue();
            reader.close();
            return ebms3MessageFragmentType;
        } catch (SOAPException | JAXBException | XMLStreamException | TransformerException e) {
            throw new MessagingException("Not possible to get the MessageFragmentType", e);
        }
    }

    public Ebms3Messaging getMessage(SOAPMessage request) {
        Ebms3Messaging ebms3Messaging;
        try {
            ebms3Messaging = getMessaging(request);
        } catch (SOAPException | JAXBException | XMLStreamException | TransformerException e) {
            throw new MessagingException("Not possible to getMessage", e);
        }
        return ebms3Messaging;
    }

    public Ebms3MessagingWithSecurityProfileMetadata getMessagingWithSecurityProfileMetadata(SOAPMessage request) {
        try {
            Ebms3Messaging ebms3Messaging = getMessaging(request);
            LOG.trace("Getting property [{}] from the message exchange", MESSAGING_WITH_SECURITY_PROFILE_METADATA);

            Ebms3MessagingWithSecurityProfileMetadata ebms3MessagingWithSecurityProfileMetadata = getEbms3MessagingWithSecurityProfileMetadataFromExchange(request);
            SecurityProfileMetadata securityProfileMetadata = ebms3MessagingWithSecurityProfileMetadata != null ? ebms3MessagingWithSecurityProfileMetadata.getSecurityProfileMetadata() : null;
            return new Ebms3MessagingWithSecurityProfileMetadata(ebms3Messaging, securityProfileMetadata);
        } catch (Exception e) {
            throw new WebServiceException("Error getting Messaging", e);
        }
    }

    private Ebms3MessagingWithSecurityProfileMetadata getEbms3MessagingWithSecurityProfileMetadataFromExchange(SOAPMessage request) {
        Ebms3MessagingWithSecurityProfileMetadata ebms3MessagingWithSecurityProfileMetadata = null;

        //we first try to get the ebms3MessagingWithSecurityProfileMetadata from the SOAP message
        try {
            final Ebms3MessagingWithSecurityProfileMetadata ebms3MessagingWithSecurityProfileMetadataFromSoapMessage = (Ebms3MessagingWithSecurityProfileMetadata) request.getProperty(MESSAGING_WITH_SECURITY_PROFILE_METADATA);
            if (ebms3MessagingWithSecurityProfileMetadataFromSoapMessage != null) {
                LOG.debug("Found property [{}] on the SOAP message", MESSAGING_WITH_SECURITY_PROFILE_METADATA);
                return ebms3MessagingWithSecurityProfileMetadataFromSoapMessage;
            }
        } catch (SOAPException e) {
            throw new WebServiceException("Error getting value for property " + MESSAGING_WITH_SECURITY_PROFILE_METADATA, e);
        }

        //if the ebms3MessagingWithSecurityProfileMetadata is not on the SoapMessage we try to get it from the exchange
        if (PhaseInterceptorChain.getCurrentMessage() != null && PhaseInterceptorChain.getCurrentMessage().getExchange() != null) {
            ebms3MessagingWithSecurityProfileMetadata = (Ebms3MessagingWithSecurityProfileMetadata) PhaseInterceptorChain.getCurrentMessage().getExchange().get(MESSAGING_WITH_SECURITY_PROFILE_METADATA);
        }
        return ebms3MessagingWithSecurityProfileMetadata;
    }

}
