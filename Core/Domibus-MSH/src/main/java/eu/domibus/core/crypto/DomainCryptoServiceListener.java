package eu.domibus.core.crypto;

import eu.domibus.api.multitenancy.Domain;

import java.security.KeyStore;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
public interface DomainCryptoServiceListener {

    void afterInit(Domain domain);

    void onValidateKeystore(Domain domain, KeyStore keystore);
}
