package eu.domibus.core.ebms3.receiver;

import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.ebms3.model.Ebms3MessagingWithSecurityProfileMetadata;
import eu.domibus.api.message.SignalMessageSoapEnvelopeSpiDelegate;
import eu.domibus.api.message.SoapMessageMetadata;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomainTaskException;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.common.ErrorCode;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.receiver.handler.IncomingMessageHandler;
import eu.domibus.core.ebms3.receiver.handler.IncomingMessageHandlerFactory;
import eu.domibus.core.ebms3.sender.client.DispatchClientDefaultProvider;
import eu.domibus.core.metrics.Counter;
import eu.domibus.core.metrics.Timer;
import eu.domibus.core.util.MessageUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import eu.domibus.logging.MDCKey;
import eu.domibus.messaging.MessageConstants;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.*;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.SOAPBinding;


/**
 * This method is responsible for the receiving of ebMS3 messages and the sending of signal messages like receipts or ebMS3 errors in return
 *
 * @author Christian Koch, Stefan Mueller
 * @author Cosmin Baciu
 * @since 3.0
 */

@WebServiceProvider(portName = "mshPort", serviceName = "mshService")
@ServiceMode(Service.Mode.MESSAGE)
@BindingType(SOAPBinding.SOAP12HTTP_BINDING)
public class MSHWebservice implements Provider<SOAPMessage> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MSHWebservice.class);

    @Autowired
    protected MessageUtil messageUtil;

    @Autowired
    protected IncomingMessageHandlerFactory incomingMessageHandlerFactory;

    @Autowired
    private DomainContextProvider domainContextProvider;

    @Autowired
    protected DomibusPropertyProvider domibusPropertyProvider;

    @Resource
    private WebServiceContext context;

    @Autowired
    protected SignalMessageSoapEnvelopeSpiDelegate signalMessageSoapEnvelopeSpiDelegate;

    @Timer(clazz = MSHWebservice.class, value = "incoming_user_message")
    @Counter(clazz = MSHWebservice.class, value = "incoming_user_message")
    @MDCKey(value = {DomibusLogger.MDC_MESSAGE_ID, DomibusLogger.MDC_MESSAGE_ROLE, DomibusLogger.MDC_MESSAGE_ENTITY_ID}, cleanOnStart = true)
    @Override
    public SOAPMessage invoke(final SOAPMessage request) {
        try {
            LOG.trace("Message received");
            setCurrentDomain(request);

            Ebms3MessagingWithSecurityProfileMetadata ebms3MessagingWithSecurityProfileMetadata = getMessagingWithSecurityProfileMetadata();
            if (ebms3MessagingWithSecurityProfileMetadata == null) {
                LOG.error("Error getting Messaging");
                throw new WebServiceException("Error getting Messaging");
            }
            Ebms3Messaging ebms3Messaging = ebms3MessagingWithSecurityProfileMetadata.getEbms3Messaging();

            final IncomingMessageHandler messageHandler = incomingMessageHandlerFactory.getMessageHandler(request, ebms3Messaging);
            if (messageHandler == null) {
                throw new WebServiceException(EbMS3ExceptionBuilder.getInstance()
                        .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0003)
                        .message("Unrecognized message")
                        .refToMessageId(ebms3Messaging.getUserMessage().getMessageInfo().getMessageId())
                        .mshRole(MSHRole.RECEIVING)
                        .build());
            }
            SOAPMessage soapMessage;
            try {
                soapMessage = messageHandler.processMessage(request, ebms3MessagingWithSecurityProfileMetadata);
            } catch (EbMS3Exception e) {
                LOG.warn("Error processing message!");
                throw new WebServiceException(e);
            }
            setUserMessageEntityIdOnContext();

            //when running IT tests the message context is null
            final MessageContext messageContext = context.getMessageContext();
            HttpServletRequest httpServletRequest = null;
            HttpServletResponse httpServletResponse = null;
            if (messageContext != null) {
                httpServletRequest = (HttpServletRequest) messageContext.get(AbstractHTTPDestination.HTTP_REQUEST);
                httpServletResponse = (HttpServletResponse) messageContext.get(AbstractHTTPDestination.HTTP_RESPONSE);
            }

            final SoapMessageMetadata soapMessageMetadata = getSoapMessageMetadata(request);
            soapMessage = signalMessageSoapEnvelopeSpiDelegate.beforeSigningAndEncryption(soapMessage, soapMessageMetadata, httpServletRequest, httpServletResponse);

            return soapMessage;
        } catch (Exception e) {
            LOG.businessError(DomibusMessageCode.BUS_MSG_NOT_RECEIVED, e);
            throw e;
        }

    }

    protected void setCurrentDomain(final SOAPMessage request) {
        LOG.trace("Setting the current domain");
        try {
            final String domainCode = (String) request.getProperty(DomainContextProvider.HEADER_DOMIBUS_DOMAIN);
            domainContextProvider.setCurrentDomainWithValidation(domainCode);
        } catch (SOAPException se) {
            throw new DomainTaskException("Could not get current domain from request header " + DomainContextProvider.HEADER_DOMIBUS_DOMAIN, se);
        }
    }

    protected void setUserMessageEntityIdOnContext() {
        final String userMessageEntityId = LOG.getMDC(DomibusLogger.MDC_MESSAGE_ENTITY_ID);
        PhaseInterceptorChain.getCurrentMessage().getExchange().put(UserMessage.USER_MESSAGE_ID_KEY_CONTEXT_PROPERTY, userMessageEntityId);
    }

    protected Ebms3MessagingWithSecurityProfileMetadata getMessagingWithSecurityProfileMetadata() {
        return (Ebms3MessagingWithSecurityProfileMetadata) PhaseInterceptorChain.getCurrentMessage().get(DispatchClientDefaultProvider.MESSAGING_WITH_SECURITY_PROFILE_METADATA);
    }

    protected SoapMessageMetadata getSoapMessageMetadata(SOAPMessage request) {
        final Boolean httpHeaderMetadataActive = domibusPropertyProvider.getBooleanProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DISPATCHER_HTTP_HEADER_METADATA_ACTIVE);
        if (!httpHeaderMetadataActive) {
            LOG.debug("Skip extracting message metadata: property [{}] is not active", DomibusPropertyMetadataManagerSPI.DOMIBUS_DISPATCHER_HTTP_HEADER_METADATA_ACTIVE);
            return null;
        }

        try {
            final String contentType = (String) request.getProperty(MessageConstants.HTTP_CONTENT_TYPE);
            final String protocolHeaders = (String) request.getProperty(MessageConstants.HTTP_PROTOCOL_HEADERS);
            final String messageProperties = (String) request.getProperty(MessageConstants.HTTP_MESSAGE_PROPERTIES);
            return new SoapMessageMetadata(contentType, protocolHeaders, messageProperties);
        } catch (SOAPException se) {
            throw new DomainTaskException("Could not get the soap message metadata from soap message", se);
        }

    }
}
