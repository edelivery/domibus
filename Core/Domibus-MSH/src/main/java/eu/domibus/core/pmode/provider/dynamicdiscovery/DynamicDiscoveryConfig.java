package eu.domibus.core.pmode.provider.dynamicdiscovery;

import eu.domibus.api.pki.CertificateService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.dynamicdiscovery.core.extension.IExtension;
import eu.europa.ec.dynamicdiscovery.core.fetcher.IMetadataFetcher;
import eu.europa.ec.dynamicdiscovery.core.fetcher.impl.DefaultURLFetcher;
import eu.europa.ec.dynamicdiscovery.core.locator.dns.impl.DefaultDNSLookup;
import eu.europa.ec.dynamicdiscovery.core.locator.impl.DefaultBDXRLocator;
import eu.europa.ec.dynamicdiscovery.core.locator.impl.StaticMapMetadataLocator;
import eu.europa.ec.dynamicdiscovery.core.provider.impl.DefaultProvider;
import eu.europa.ec.dynamicdiscovery.core.reader.IMetadataReader;
import eu.europa.ec.dynamicdiscovery.core.reader.impl.DefaultBDXRReader;
import eu.europa.ec.dynamicdiscovery.core.security.impl.DefaultProxy;
import eu.europa.ec.dynamicdiscovery.core.security.impl.DefaultSignatureValidator;
import eu.europa.ec.dynamicdiscovery.enums.DNSLookupType;
import eu.europa.ec.dynamicdiscovery.exception.ConnectionException;
import eu.europa.ec.dynamicdiscovery.model.SMPTransportProfile;
import eu.europa.ec.dynamicdiscovery.model.identifiers.SMPDocumentIdentifier;
import eu.europa.ec.dynamicdiscovery.model.identifiers.SMPParticipantIdentifier;
import eu.europa.ec.dynamicdiscovery.model.identifiers.SMPProcessIdentifier;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.net.ssl.SSLContext;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

/**
 * A configuration containing definitions for dynamic discovery services.
 *
 * @author Sebastian-Ion TINCU
 * @since 5.0
 */
@Configuration
public class DynamicDiscoveryConfig {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DynamicDiscoveryConfig.class);

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DomibusCertificateValidator domibusCertificateValidator(CertificateService certificateService, KeyStore trustStore, String certRegex, List<String> allowedCertificatePolicyOIDs) {
        return new DomibusCertificateValidator(certificateService, trustStore, certRegex, allowedCertificatePolicyOIDs);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DefaultBDXRLocator bdxrLocator(List<String> smlInfoList,
                                          List<DNSLookupType> dnsLookupTypes,
                                          Map<String, String> dnsLookupTypeMap) {
        final DefaultBDXRLocator.Builder builder = new DefaultBDXRLocator.Builder()
                .addDnsLookupTypes(dnsLookupTypes)
                .addTopDnsDomains(smlInfoList);

        if (MapUtils.isNotEmpty(dnsLookupTypeMap)) {
            LOG.debug("Adding DNS lookup types to the BDXR locator: [{}]", dnsLookupTypeMap);

            final DefaultDNSLookup.Builder dnsLookupBuilder = new DefaultDNSLookup.Builder();
            dnsLookupTypeMap.forEach((key, value) -> {
                LOG.debug("Adding DNS lookup type [{}] with value [{}]", key, value);
                dnsLookupBuilder.addRequiredNaptrService(key);
                dnsLookupBuilder.addRequiredNaptrServiceHttpBinding(key, value);
            });
            builder.dnsLookup(dnsLookupBuilder.build());
        }

        return builder.build();
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public StaticMapMetadataLocator staticBdxrLocator(String smpUrl) throws URISyntaxException {
        return new StaticMapMetadataLocator(smpUrl);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DefaultProvider defaultProvider(IMetadataFetcher metadataFetcher, IMetadataReader metadataReader, List<String> wildcardSchemes) {
        return new DefaultProvider.Builder()
                .metadataFetcher(metadataFetcher)
                .metadataReader(metadataReader)
                .wildcardSchemes(wildcardSchemes)
                .build();
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DefaultURLFetcher urlFetcher(DomibusHttpRoutePlanner domibusHttpRoutePlanner,
                                        DefaultProxy proxy,
                                        SSLContext sslContext,
                                        String[] supportedSSLProtocols) {
        DefaultURLFetcher.Builder builder = new DefaultURLFetcher.Builder(sslContext)
                .routePlanner(domibusHttpRoutePlanner)
                .tlsVersions(supportedSSLProtocols)
                .proxyConfiguration(proxy);
        return builder.build();
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DefaultBDXRReader bdxrReader(DefaultSignatureValidator defaultSignatureValidator, List<IExtension> extensions) {
        return new DefaultBDXRReader.Builder()
                .addExtensions(extensions)
                .signatureValidator(defaultSignatureValidator)
                .build();
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DefaultSignatureValidator defaultSignatureValidator(DomibusCertificateValidator domibusCertificateValidator) {
        return new DefaultSignatureValidator(domibusCertificateValidator);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public SMPDocumentIdentifier documentIdentifier(String identifier, String scheme) {
        return new SMPDocumentIdentifier(identifier, scheme);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public SMPParticipantIdentifier participantIdentifier(String identifier, String scheme) {
        return new SMPParticipantIdentifier(identifier, scheme);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public SMPProcessIdentifier processIdentifier(String identifier, String scheme) {
        return new SMPProcessIdentifier(identifier, scheme);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public SMPTransportProfile transportProfile(String identifier) {
        return new SMPTransportProfile(identifier);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DefaultProxy proxy(String serverAddress, int serverPort, String user, String password, String nonProxyHosts) throws ConnectionException {
        return new DefaultProxy(serverAddress, serverPort, user, password, nonProxyHosts);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public EndpointInfo endpointInfo(String transportProfile, String address, Map<String, X509Certificate> certificates) {
        return new EndpointInfo(transportProfile, address, certificates);
    }


}
