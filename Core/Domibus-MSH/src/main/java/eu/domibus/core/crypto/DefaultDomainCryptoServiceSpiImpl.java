package eu.domibus.core.crypto;

import eu.domibus.api.cluster.SignalService;
import eu.domibus.api.crypto.CryptoException;
import eu.domibus.api.crypto.DomibusCryptoType;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.multitenancy.lock.DomibusSynchronizationException;
import eu.domibus.api.party.PartyService;
import eu.domibus.api.pki.*;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.CertificateException;
import eu.domibus.api.security.CertificatePurpose;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.configuration.spi.DomibusConfigurationManagerSpi;
import eu.domibus.configuration.spi.DomibusResource;
import eu.domibus.configuration.spi.DomibusResourceType;
import eu.domibus.core.audit.AuditService;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.core.configuration.DomibusConfigurationManagerSpiProvider;
import eu.domibus.core.converter.DomibusCoreMapper;
import eu.domibus.core.crypto.spi.*;
import eu.domibus.core.crypto.spi.model.KeyStoreContentInfoDTO;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.core.multitenancy.lock.SynchronizationService;
import eu.domibus.core.spring.DomibusResourceUtil;
import eu.domibus.core.util.SecurityUtilImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.wss4j.common.crypto.Crypto;
import org.apache.wss4j.common.crypto.CryptoType;
import org.apache.wss4j.common.crypto.Merlin;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.cryptacular.util.CertUtil;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.security.auth.callback.CallbackHandler;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;
import static eu.domibus.core.crypto.MultiDomainCryptoServiceImpl.DOMIBUS_KEYSTORE_NAME;
import static eu.domibus.core.crypto.MultiDomainCryptoServiceImpl.DOMIBUS_TRUSTSTORE_NAME;

/**
 * @author Cosmin Baciu
 * @since 4.0
 * <p>
 * Default authentication implementation of the SPI. Cxf-Merlin.
 */
@Component(AbstractCryptoServiceSpi.DEFAULT_AUTHENTICATION_SPI)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultDomainCryptoServiceSpiImpl implements DomainCryptoServiceSpi {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DefaultDomainCryptoServiceSpiImpl.class);

    /**
     * the following locks are used for performing changes to the keystore/truststore in a synchronized way; see also {@link SynchronizationService}
     */

    private static final String JAVA_CHANGE_LOCK = "changeLock";

    private static final String DB_SYNC_LOCK_KEY = "keystore-synchronization.lock";
    private static final String PRIVATE_KEY_SUFFIX_FOR_HSM = "_key";

    protected Domain domain;

    protected final DomibusPropertyProvider domibusPropertyProvider;

    protected final CertificateService certificateService;

    protected final SignalService signalService;

    protected final DomibusCoreMapper coreMapper;

    protected final DomibusTaskExecutor domainTaskExecutor;

    protected final SecurityProfileHelper securityProfileHelper;

    protected final SecurityProfileService securityProfileService;

    protected final SecurityProfileProvider securityProfileProvider;

    protected Merlin merlin = new Merlin();

    protected final SecurityUtilImpl securityUtil;

    protected final SecurityProfileValidatorService securityProfileValidatorService;

    private final KeystorePersistenceService keystorePersistenceService;

    private final CertificateHelper certificateHelper;

    private final ObjectProvider<DomibusCryptoType> domibusCryptoTypes;
    private final PartyService partyService;

    private final AuditService auditService;

    protected List<DomainCryptoServiceListener> domainCryptoServiceListeners;

    public DefaultDomainCryptoServiceSpiImpl(DomibusPropertyProvider domibusPropertyProvider,
                                             CertificateService certificateService,
                                             SignalService signalService,
                                             DomibusCoreMapper coreMapper,
                                             DomibusTaskExecutor domainTaskExecutor,
                                             SecurityUtilImpl securityUtil,
                                             SecurityProfileValidatorService securityProfileValidatorService,
                                             KeystorePersistenceService keystorePersistenceService,
                                             CertificateHelper certificateHelper,
                                             AuditService auditService,
                                             ObjectProvider<DomibusCryptoType> domibusCryptoTypes,
                                             PartyService partyService,
                                             SecurityProfileService securityProfileService,
                                             SecurityProfileProvider securityProfileProvider,
                                             SecurityProfileHelper securityProfileHelper,
                                             @Lazy @Autowired(required = false) List<DomainCryptoServiceListener> domainCryptoServiceListeners) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.certificateService = certificateService;
        this.signalService = signalService;
        this.coreMapper = coreMapper;
        this.domainTaskExecutor = domainTaskExecutor;
        this.securityUtil = securityUtil;
        this.securityProfileValidatorService = securityProfileValidatorService;
        this.keystorePersistenceService = keystorePersistenceService;
        this.certificateHelper = certificateHelper;
        this.auditService = auditService;
        this.domibusCryptoTypes = domibusCryptoTypes;
        this.partyService = partyService;
        this.domainCryptoServiceListeners = domainCryptoServiceListeners;
        this.securityProfileService = securityProfileService;
        this.securityProfileProvider = securityProfileProvider;
        this.securityProfileHelper = securityProfileHelper;
    }

    public void init() {
        LOG.debug("Initializing the certificate provider for domain [{}]", domain);

        try {
            initTrustStore();
        } catch (Exception ex) {
            LOG.error("Exception while trying to initialize the domibus truststore", ex);
        }

        try {
            initKeyStore();
        } catch (Exception ex) {
            LOG.error("Exception while trying to initialize the domibus keystore", ex);
        }

        LOG.debug("Finished initializing the certificate provider for domain [{}]", domain);


        if (domainCryptoServiceListeners != null) {
            domainCryptoServiceListeners.forEach(listener -> listener.afterInit(domain));

        }
    }

    @Override
    public X509Certificate getCertificateFromKeyStore(String alias) throws KeyStoreException {
        return (X509Certificate) getKeyStore().getCertificate(alias);
    }

    @Override
    public X509Certificate getCertificateFromTrustStore(String alias) throws KeyStoreException {
        return (X509Certificate) getTrustStore().getCertificate(alias);
    }

    @Override
    public X509Certificate[] getX509Certificates(CryptoType cryptoType) throws WSSecurityException {
        LOG.info("Legacy single keystore alias is used for domain [{}] for crypto type [{}]", domain, domibusCryptoTypes.getObject(cryptoType).asString());
        X509Certificate[] certificates = merlin.getX509Certificates(cryptoType);
        if (ArrayUtils.isNotEmpty(certificates) && certificates[0] != null) {
            Boolean encryptionCertificatesPrintingEnabled = domibusPropertyProvider.getBooleanProperty(DOMIBUS_LOGGING_REMOTE_CERTIFICATES_PRINT);
            if (encryptionCertificatesPrintingEnabled) {
                if (isRemoteCertificate(certificates[0], cryptoType)) {
                    logRemoteCertificate(Optional.of(certificates[0]));
                }
            }
        }
        return certificates;
    }

    private boolean isRemoteCertificate(X509Certificate certificate, CryptoType cryptoType) throws WSSecurityException {
        if (certificate == null) {
            LOG.trace("Cannot verify whether the certificate is remote because it is undefined");
            return false;
        }

        if (cryptoType == null) {
            LOG.trace("Cannot verify whether the certificate is remote because the provided crypto type is undefined");
            return false;
        }

        String localPartyName = partyService.getGatewayParty().getName();
        String alias = CertUtil.subjectCN(certificate);

        // The certificate of the remote receiver is used on the sender to encrypt (CryptoType of type ALIAS) while
        // the certificate of the remote sender is used on the receiver to verify trust (CryptoType of type SKI_BYTES)
        return (cryptoType.getType() == CryptoType.TYPE.ALIAS || cryptoType.getType() == CryptoType.TYPE.SKI_BYTES)
                && !StringUtils.equalsIgnoreCase(localPartyName, alias);
    }

    private void logRemoteCertificate(Optional<X509Certificate> certificate) {
        logCertificate(certificate, "Found the certificate of the remote entity used during the encryption or the trust verification phase having CN [{}]");
    }


    @Override
    public String getX509Identifier(X509Certificate cert) throws WSSecurityException {
        return merlin.getX509Identifier(cert);
    }

    @Override
    public PrivateKey getPrivateKey(X509Certificate certificate, CallbackHandler callbackHandler) throws WSSecurityException {
        Boolean signingCertificatesPrintingEnabled = domibusPropertyProvider.getBooleanProperty(DOMIBUS_LOGGING_LOCAL_CERTIFICATES_PRINT);
        if (signingCertificatesPrintingEnabled) {
            logLocalCertificate(Optional.ofNullable(certificate));
        }
        return merlin.getPrivateKey(certificate, callbackHandler);
    }

    @Override
    public PrivateKey getPrivateKey(PublicKey publicKey, CallbackHandler callbackHandler) throws WSSecurityException {
        return merlin.getPrivateKey(publicKey, callbackHandler);
    }

    @Override
    public PrivateKey getPrivateKey(String identifier, String password) throws WSSecurityException {
        Boolean signingCertificatesPrintingEnabled = domibusPropertyProvider.getBooleanProperty(DOMIBUS_LOGGING_LOCAL_CERTIFICATES_PRINT);
        if (signingCertificatesPrintingEnabled) {
            Optional<X509Certificate> certificate = Optional.empty();
            try {
                certificate = Optional.ofNullable(getCertificateFromKeyStore(identifier));
            } catch (KeyStoreException e) {
                LOG.error("Could not retrieve from the keystore the certificate corresponding to the private key using the identifier [{}]", identifier);
            }
            logLocalCertificate(certificate);
        }
        if (!keystorePersistenceService.getKeyStorePersistenceInfo().isDownloadable()) {
            LOG.debug("Adding a suffix to the sender's private key alias to support HSM key stores");
            identifier = identifier + PRIVATE_KEY_SUFFIX_FOR_HSM;
        }
        return merlin.getPrivateKey(identifier, password);
    }

    private void logLocalCertificate(Optional<X509Certificate> certificate) {
        logCertificate(certificate, "Found the certificate of the local entity corresponding to the private key used during the signing or the decryption phase having CN [{}]");
    }

    private void logCertificate(Optional<X509Certificate> certificate, String message) {
        if (!certificate.isPresent()) {
            LOG.info("Not logging any details because the certificate is absent");
            return;
        }

        X509Certificate x509Certificate = certificate.get();
        if (LOG.isInfoEnabled()) {
            LOG.info(message, CertUtil.subjectCN(x509Certificate));
        }

        // Print all certificate details in DEBUG mode
        LOG.debug("Certificate details: [{}]", x509Certificate);

        String fingerprint = certificateService.extractFingerprints(x509Certificate);
        LOG.info("Certificate details of most interest: Fingerprint=[{}], Subject=[{}], Validity=[From: {}, To: {}], Issuer=[{}], SerialNumber=[{}]",
                fingerprint,
                x509Certificate.getSubjectDN(),
                x509Certificate.getNotBefore(),
                x509Certificate.getNotAfter(),
                x509Certificate.getIssuerDN(),
                x509Certificate.getSerialNumber());
    }

    @Override
    public void verifyTrust(PublicKey publicKey) throws WSSecurityException {
        merlin.verifyTrust(publicKey);
    }

    @Override
    public void verifyTrust(X509Certificate[] certs, boolean enableRevocation, Collection<Pattern> subjectCertConstraints, Collection<Pattern> issuerCertConstraints) throws WSSecurityException {
        merlin.verifyTrust(certs, enableRevocation, subjectCertConstraints, issuerCertConstraints);
    }

    @Override
    public String getDefaultX509Identifier() throws WSSecurityException {
        throw new WSSecurityException(WSSecurityException.ErrorCode.FAILURE, "Default identifier should not be used: a specific alias is required for domain: " + domain);
    }

    @Override
    public KeyStore getKeyStore() {
        return merlin.getKeyStore();
    }

    @Override
    public KeyStore getTrustStore() {
        return merlin.getTrustStore();
    }

    @Override
    public Crypto getCrypto() {
        return merlin;
    }

    @Override
    public String getPrivateKeyPassword(String alias) {
        if (securityProfileHelper.areSecurityProfilesDisabled(domain)) {
            LOG.debug("Getting the password for the private key with alias [{}] from the legacy single alias keystore", alias);
            return domibusPropertyProvider.getProperty(domain, DOMIBUS_SECURITY_KEY_PRIVATE_PASSWORD);
        }
        LOG.debug("Getting the password when using security profiles for the private key with alias [{}] from the keystore", alias);

        final CertificatePurpose certificatePurpose = securityProfileService.extractCertificatePurpose(alias);
        if (certificatePurpose == null) {
            throw new CryptoSpiException(String.format("Could not get the password for alias [%s]: could not get the certificate purpose form alias [%s]", alias, alias));
        }

        final List<SecurityProfile> securityProfilesPriorityList = securityProfileProvider.getSecurityProfilesPriorityList(domain);

        SecurityProfile securityProfile = null;
        try {
            securityProfile = securityProfileService.extractSecurityProfile(securityProfilesPriorityList, alias);
        } catch (Exception e) {
            throw new CryptoSpiException(String.format("Could not get the password for alias [%s]: could not get the security profile form alias [%s]", alias, alias), e);
        }

        String passwordProperty = "domibus.security.profile." + securityProfile.getCode() + ".key.private." + certificatePurpose.getCertificatePurpose() + ".password";
        LOG.debug("Getting the password for the private key with alias [{}] using the property [{}]", alias, passwordProperty);

        return domibusPropertyProvider.getProperty(domain, passwordProperty);
    }

    @Override
    public void refreshTrustStore() {
        executeWithLock(this::reloadTrustStore);
    }

    @Override
    public void refreshKeyStore() {
        executeWithLock(this::reloadKeyStore);
    }

    @Override
    public void resetKeyStore() {
        executeWithLock(this::reloadKeyStore);
    }

    @Override
    public void resetTrustStore() {
        executeWithLock(this::reloadTrustStore);
    }

    @Override
    public void resetSecurityProfiles() {
        LOG.info("Resetting security profiles on domain [{}]", domain);
        init();
    }

    @Override
    public void replaceTrustStore(byte[] storeContent, String storeFileName, String storePassword) throws CryptoSpiException {
        executeWithLock(() ->
                replaceStore(storeContent, storeFileName, storePassword, false, DOMIBUS_TRUSTSTORE_NAME,
                        keystorePersistenceService::getTrustStorePersistenceInfo, this::reloadTrustStore,
                        null, this::getTrustStore)
        );
    }

    @Override
    public void replaceTrustStore(KeyStoreContentInfoDTO storeContentInfoDTO) {
        executeWithLock(() ->
                replaceStore(storeContentInfoDTO.getContent(), storeContentInfoDTO.getFileName(), storeContentInfoDTO.getPassword(),
                        storeContentInfoDTO.isAllowChangingDiskStoreProps(), DOMIBUS_TRUSTSTORE_NAME,
                        keystorePersistenceService::getTrustStorePersistenceInfo, this::reloadTrustStore,
                        null, this::getTrustStore)
        );

    }

    @Override
    public void replaceKeyStore(byte[] storeContent, String storeFileName, String storePassword) throws CryptoSpiException {
        executeWithLock(() ->
                replaceStore(storeContent, storeFileName, storePassword, false, DOMIBUS_KEYSTORE_NAME,
                        keystorePersistenceService::getKeyStorePersistenceInfo, this::reloadKeyStore,
                        this::validateKeyStoreCertificateTypes, this::getKeyStore)
        );
    }

    @Override
    public void replaceKeyStore(KeyStoreContentInfoDTO storeContentInfoDTO) {
        executeWithLock(() ->
                replaceStore(storeContentInfoDTO.getContent(), storeContentInfoDTO.getFileName(), storeContentInfoDTO.getPassword(),
                        storeContentInfoDTO.isAllowChangingDiskStoreProps(), DOMIBUS_KEYSTORE_NAME,
                        keystorePersistenceService::getKeyStorePersistenceInfo, this::reloadKeyStore,
                        this::validateKeyStoreCertificateTypes, this::getKeyStore)
        );
    }

    @Override
    public void replaceTrustStore(String storeFileLocation, String storePassword) throws CryptoSpiException {
        Path path = Paths.get(storeFileLocation);
        String storeName = path.getFileName().toString();
        byte[] storeContent = getContentFromFile(storeFileLocation, DomibusResourceType.DOMIBUS_TRUSTSTORE);
        replaceTrustStore(storeContent, storeName, storePassword);
    }

    @Override
    public void replaceKeyStore(String storeFileLocation, String storePassword) {
        Path path = Paths.get(storeFileLocation);
        String storeName = path.getFileName().toString();
        byte[] storeContent = getContentFromFile(storeFileLocation, DomibusResourceType.DOMIBUS_KEYSTORE);
        replaceKeyStore(storeContent, storeName, storePassword);
    }

    @Override
    public boolean isCertificateChainValid(String alias) throws DomibusCertificateSpiException {
        LOG.debug("Checking certificate validation for [{}]", alias);
        KeyStore trustStore = getTrustStore();
        return certificateService.isCertificateChainValid(trustStore, alias);
    }

    @Override
    public boolean addCertificate(X509Certificate certificate, String alias, boolean overwrite) {
        List<CertificateEntry> certificates = Collections.singletonList(new CertificateEntry(alias, certificate));
        return addCertificates(certificates, overwrite);
    }

    @Override
    public void addCertificate(List<CertificateEntrySpi> certs, boolean overwrite) {
        List<CertificateEntry> certificates = certs.stream().map(el -> new CertificateEntry(el.getAlias(), el.getCertificate()))
                .collect(Collectors.toList());
        addCertificates(certificates, overwrite);
    }

    @Override
    public boolean removeCertificate(String alias) {
        List<String> aliases = Collections.singletonList(alias);
        return removeCertificates(aliases);
    }

    @Override
    public void removeCertificate(List<String> aliases) {
        removeCertificates(aliases);
    }

    @Override
    public String getIdentifier() {
        return AbstractCryptoServiceSpi.DEFAULT_AUTHENTICATION_SPI;
    }

    @Override
    public void setDomain(DomainSpi domain) {
        this.domain = coreMapper.domainSpiToDomain(domain);
    }

    @Override
    public boolean isTrustStoreChanged() {
        return certificateService.isStoreChangedOnDisk(getTrustStore(), keystorePersistenceService.getTrustStorePersistenceInfo());
    }

    @Override
    public boolean isKeyStoreChanged() {
        return certificateService.isStoreChangedOnDisk(getKeyStore(), keystorePersistenceService.getKeyStorePersistenceInfo());
    }

    private boolean addCertificates(List<CertificateEntry> certificates, boolean overwrite) {
        return executeWithLock(() -> doAddCertificates(certificates, overwrite));
    }

    private boolean doAddCertificates(List<CertificateEntry> certificates, boolean overwrite) {
        BiFunction<KeystorePersistenceInfo, KeyStore, Boolean> storeChanger =
                (persistenceInfo, diskStore) -> certificateService.addCertificates(diskStore, certificates, overwrite);
        Consumer<KeystorePersistenceInfo> auditer = persistenceInfo -> auditService.addCertificateAddedAudit(persistenceInfo.getName());
        return changeCertificates(storeChanger, auditer);
    }

    private boolean removeCertificates(List<String> aliases) {
        return executeWithLock(() -> doRemoveCertificates(aliases));
    }

    private boolean doRemoveCertificates(List<String> aliases) {
        BiFunction<KeystorePersistenceInfo, KeyStore, Boolean> storeChanger =
                (persistenceInfo, diskStore) -> certificateService.removeCertificates(diskStore, aliases);
        Consumer<KeystorePersistenceInfo> auditer = persistenceInfo -> auditService.addCertificateRemovedAudit(persistenceInfo.getName());
        return changeCertificates(storeChanger, auditer);
    }

    private boolean changeCertificates(BiFunction<KeystorePersistenceInfo, KeyStore, Boolean> storeChanger, Consumer<KeystorePersistenceInfo> auditer) {
        KeystorePersistenceInfo persistenceInfo = keystorePersistenceService.getTrustStorePersistenceInfo();
        String storeName = persistenceInfo.getName();

        KeyStore diskStore = certificateService.getStore(persistenceInfo);

        boolean outcome = storeChanger.apply(persistenceInfo, diskStore);
        KeyStore newStore = diskStore;

        boolean identical = securityUtil.areKeystoresIdentical(getTrustStore(), newStore);

        if (!identical) {
            LOG.info("Current store [{}] is different from the persisted one.", storeName);

            LOG.debug("Saving store [{}] to disk.", storeName);
            keystorePersistenceService.saveStore(newStore, persistenceInfo);

            auditer.accept(persistenceInfo);

            LOG.info("Reloading store [{}].", storeName);
            reloadTrustStore();
        } else {
            LOG.info("Current store [{}] is identical with the persisted one, no reloading.", storeName);
        }
        return outcome;
    }

    protected boolean replaceStore(byte[] storeContent, String storeFileName, String storePassword, boolean allowChangingDiskStoreProps,
                                   String storeName, Supplier<KeystorePersistenceInfo> persistenceInfoGetter,
                                   Runnable storeReloader, Consumer<KeyStore> certificateTypeValidator,
                                   Supplier<KeyStore> storeGetter) throws CryptoSpiException {

        KeyStoreContentInfo storeContentInfo = certificateHelper.createStoreContentInfo(storeName, storeFileName, storeContent, storePassword, allowChangingDiskStoreProps);
        KeyStore newStore = certificateService.loadStore(storeContentInfo);
        try {
            KeyStore currentStore = storeGetter.get();
            if (securityUtil.areKeystoresIdentical(newStore, currentStore)) {
                throw new SameResourceCryptoSpiException(storeName, storeFileName,
                        String.format("Current store [%s] was not replaced with the content of the file [%s] because they are identical.", storeName, storeFileName));
            }
            LOG.info("Preparing to replace the current store [{}] having entries [{}] with entries [{}].",
                    storeName, certificateService.getStoreEntries(currentStore), certificateService.getStoreEntries(newStore));
        } catch (SameResourceCryptoSpiException sre) {
            throw sre;
        } catch (Exception ex) {
            LOG.warn("Could not retrieve the disk store, so no comparing them.", ex);
            LOG.info("Setting the store [{}] with entries [{}].", storeName, certificateService.getStoreEntries(newStore));
        }

        try {
            if (certificateTypeValidator != null) {
                certificateTypeValidator.accept(newStore);
            }

            KeystorePersistenceInfo persistenceInfo = persistenceInfoGetter.get();
            certificateService.replaceStore(storeContentInfo, persistenceInfo, false);
        } catch (CryptoException ex) {
            throw new CryptoSpiException(String.format("Error while replacing the store [%s] with content of the file named [%s].", storeName, storeFileName), ex);
        }

        LOG.debug("Store [{}] successfully replaced with entries [{}].", storeName, certificateService.getStoreEntries(newStore));
        storeReloader.run();
        return true;
    }

    protected void initTrustStore() {
        initStore(DOMIBUS_TRUSTSTORE_NAME, this::loadTrustStoreProperties, keystorePersistenceService::getTrustStorePersistenceInfo,
                (trustStore) -> merlin.setTrustStore(trustStore), null);
    }

    protected void loadTrustStoreProperties() {
        KeystorePersistenceInfo persistenceInfo = keystorePersistenceService.getTrustStorePersistenceInfo();
        loadStoreProperties(persistenceInfo, this::loadTrustStorePropertiesForMerlin);
    }

    protected void loadStoreProperties(KeystorePersistenceInfo persistenceInfo, Consumer<KeystorePersistenceInfo> storePropertiesLoader) {
        final String trustStoreType = persistenceInfo.getType();
        final String trustStorePassword = persistenceInfo.getPassword();

        if (StringUtils.isAnyEmpty(trustStoreType, trustStorePassword)) {
            String message = String.format("One of the [%s] property values is null for domain [%s]: Type=[%s], Password",
                    persistenceInfo.getName(), domain, trustStoreType);
            LOG.error(message);
            throw new ConfigurationException(message);
        }

        storePropertiesLoader.accept(persistenceInfo);
    }

    private byte[] getContentFromFile(String location, String name) {
        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();
        try {
            final DomibusResource resource = domibusConfigurationProvider.getResource(location, name);
            DomibusResourceUtil domibusResourceUtil = new DomibusResourceUtil();
            return domibusResourceUtil.domibusResourceToByteArray(resource);
        } catch (Exception e) {
            throw new DomibusCertificateException("Could not read store [" + name + "] from [" + location + "]");
        }
    }

    private void loadTrustStorePropertiesForMerlin(KeystorePersistenceInfo persistenceInfo) {
        try {
            final Properties trustStoreProperties = getTrustStoreProperties(persistenceInfo.getType(), persistenceInfo.getPassword());
            merlin.loadProperties(trustStoreProperties, Merlin.class.getClassLoader(), null);
        } catch (WSSecurityException | IOException e) {
            LOG.error("Error occurred when loading the properties of the TrustStore");
            throw new CryptoException(DomibusCoreErrorCode.DOM_001, "Error occurred when loading the properties of TrustStore: " + e.getMessage(), e);
        }
    }

    protected void validateKeyStoreCertificateTypes(KeyStore keystore) {
        this.domainCryptoServiceListeners.forEach(listener -> listener.onValidateKeystore(domain, keystore));
    }

    protected void initKeyStore() {
        initStore(DOMIBUS_KEYSTORE_NAME, this::loadKeyStoreProperties, keystorePersistenceService::getKeyStorePersistenceInfo,
                (keyStore) -> merlin.setKeyStore(keyStore), this::validateKeyStoreCertificateTypes);
    }

    protected void initStore(String storeName, Runnable propertiesLoader, Supplier<KeystorePersistenceInfo> persistenceInfoGetter,
                             Consumer<KeyStore> merlinStoreSetter, Consumer<KeyStore> certificateTypeValidator) {
        LOG.debug("Initializing the [{}] certificate provider for domain [{}]", storeName, domain);

        domainTaskExecutor.submit(() -> {
            propertiesLoader.run();

            KeyStore store = certificateService.getStore(persistenceInfoGetter.get());

            try {
                if (certificateTypeValidator != null) {
                    certificateTypeValidator.accept(store);
                }
            } catch (CertificateException e) {
                LOG.error("Error validating store [{}]: {}", storeName, e.getMessage(), e);
            }

            merlinStoreSetter.accept(store);
        }, domain);

        LOG.debug("Finished initializing the [{}] certificate provider for domain [{}]", storeName, domain);
    }

    protected void loadKeyStoreProperties() {
        KeystorePersistenceInfo persistenceInfo = keystorePersistenceService.getKeyStorePersistenceInfo();
        loadStoreProperties(persistenceInfo, this::loadKeyStorePropertiesForMerlin);
    }

    private void loadKeyStorePropertiesForMerlin(KeystorePersistenceInfo persistenceInfo) {
        try {
            merlin
                    .loadProperties(getKeyStoreProperties(persistenceInfo.getType(), persistenceInfo.getPassword()),
                            Merlin.class.getClassLoader(), null);
        } catch (WSSecurityException | IOException e) {
            LOG.error("Error occurred when loading the properties of the keystore");
            throw new CryptoException(DomibusCoreErrorCode.DOM_001, "Error occurred when loading the properties of keystore: " + e.getMessage(), e);
        }
    }

    protected Properties getKeyStoreProperties(String keystoreType, String keystorePassword) {
        Properties properties = new Properties();
        properties.setProperty(Merlin.PREFIX + Merlin.KEYSTORE_TYPE, keystoreType);
        final String keyStorePasswordProperty = Merlin.PREFIX + Merlin.KEYSTORE_PASSWORD; //NOSONAR
        properties.setProperty(keyStorePasswordProperty, keystorePassword);
//        properties.setProperty(Merlin.PREFIX + Merlin.KEYSTORE_ALIAS, alias);//we don't need to set the default certificate: it will be loaded at runtime based on the alias

        Properties logProperties = new Properties();
        logProperties.putAll(properties);
        logProperties.remove(keyStorePasswordProperty);
        LOG.debug("Keystore properties for domain [{}] [{}]are [{}]", domain, logProperties);

        return properties;
    }

    protected Properties getTrustStoreProperties(String trustStoreType, String trustStorePassword) {
        Properties result = new Properties();
        result.setProperty(Merlin.PREFIX + Merlin.TRUSTSTORE_TYPE, trustStoreType);
        final String trustStorePasswordProperty = Merlin.PREFIX + Merlin.TRUSTSTORE_PASSWORD; //NOSONAR
        result.setProperty(trustStorePasswordProperty, trustStorePassword);
        result.setProperty(Merlin.PREFIX + Merlin.LOAD_CA_CERTS, "false");

        Properties logProperties = new Properties();
        logProperties.putAll(result);
        logProperties.remove(trustStorePasswordProperty);
        LOG.debug("Truststore properties for domain [{}] are [{}]", domain, logProperties);

        return result;
    }

    private boolean reloadKeyStore() throws CryptoSpiException {
        return reloadStore(keystorePersistenceService::getKeyStorePersistenceInfo, this::getKeyStore, this::loadKeyStoreProperties,
                (keyStore) -> merlin.setKeyStore(keyStore),
                signalService::signalKeyStoreUpdate, this::validateKeyStoreCertificateTypes);
    }

    private boolean reloadTrustStore() throws CryptoSpiException {
        return reloadStore(keystorePersistenceService::getTrustStorePersistenceInfo, this::getTrustStore, this::loadTrustStoreProperties,
                (keyStore) -> merlin.setTrustStore(keyStore),
                signalService::signalTrustStoreUpdate, null);
    }

    private boolean reloadStore(Supplier<KeystorePersistenceInfo> persistenceGetter,
                                Supplier<KeyStore> storeGetter,
                                Runnable storePropertiesLoader,
                                Consumer<KeyStore> storeSetter,
                                Consumer<Domain> signaller,
                                Consumer<KeyStore> certificateTypeValidator) throws CryptoSpiException {
        KeystorePersistenceInfo persistenceInfo = persistenceGetter.get();
        String storeLocation = persistenceInfo.getFileLocation();
        try {
            final KeyStore newStore = certificateService.getStore(persistenceInfo);
            String storeName = persistenceInfo.getName();
            if (certificateTypeValidator != null) {
                certificateTypeValidator.accept(newStore);
            }

            try {
                KeyStore currentStore = storeGetter.get();
                if (securityUtil.areKeystoresIdentical(currentStore, newStore)) {
                    LOG.info("[{}] on disk and in memory are identical, so no reloading.", storeName);
                    return false;
                }

                LOG.info("Replacing the current [{}] with entries [{}] with the one from the file [{}] with entries [{}] on domain [{}].",
                        storeName, certificateService.getStoreEntries(currentStore), storeLocation, certificateService.getStoreEntries(newStore), domain);
            } catch (Exception ex) {
                LOG.warn("Could not retrieve the current store named [{}].", storeName, ex);
                LOG.info("Replacing the [{}] with the one from the file [{}] with entries [{}] on domain [{}].",
                        storeName, storeLocation, certificateService.getStoreEntries(newStore), domain);
            }
            storePropertiesLoader.run();

            storeSetter.accept(newStore);
            merlin.clearCache();

            signaller.accept(domain);
            return true;
        } catch (CryptoException ex) {
            throw new CryptoSpiException("Error while replacing the keystore from file " + storeLocation, ex);
        }
    }

    private <R> R executeWithLock(Callable<R> task) {
        try {
            return domainTaskExecutor.executeWithLock(
                    task,
                    DB_SYNC_LOCK_KEY,
                    JAVA_CHANGE_LOCK);
        } catch (DomibusSynchronizationException ex) {
            Throwable cause = ExceptionUtils.getRootCause(ex);
            if (cause instanceof CryptoSpiException) {
                throw (CryptoSpiException) cause;
            }
            throw new CryptoSpiException(cause);
        }
    }
}
