package eu.domibus.core.jpa;

import eu.domibus.api.datasource.DataSourceConstants;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.property.PrimitivePropertyTypesManager;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_DATASOURCE_REPLICA_URL;

/**
 * @author Ion Perpegel
 * @since 5.2
 * <p>
 * Base data source configuration that routes to either read-only or read-write datasources, depending on the readOnly property of a Transaction
 */
public abstract class BaseDatasourceConfiguration {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BaseDatasourceConfiguration.class);

    protected final PrimitivePropertyTypesManager primitivePropertyTypesManager;

    protected final DomibusPropertyProvider domibusPropertyProvider;

    protected BaseDatasourceConfiguration(DomibusPropertyProvider domibusPropertyProvider,
                                          PrimitivePropertyTypesManager primitivePropertyTypesManager) {
        this.primitivePropertyTypesManager = primitivePropertyTypesManager;
        this.domibusPropertyProvider = domibusPropertyProvider;
    }

    @Bean(DataSourceConstants.DOMIBUS_JDBC_DATA_SOURCE)
    public DataSource domibusDatasource() {
        return getActualDatasource();
    }

    @Bean
    public DataSource readWriteDataSource() {
        return getReadWriteDataSource();
    }

    @Bean
    public DataSource readOnlyDataSource() {
        return getActualReadOnlyDataSource();
    }

    protected abstract DataSource getReadWriteDataSource();

    protected abstract DataSource getReadOnlyDataSource();

    protected DataSource getActualDatasource() {
        TransactionRoutingDataSource routingDataSource = new TransactionRoutingDataSource();

        Map<Object, Object> dataSourceMap = new HashMap<>();

        DataSource readWriteDataSource = getReadWriteDataSource();
        dataSourceMap.put(DataSourceType.READ_WRITE, readWriteDataSource);

        DataSource readOnlyDataSource = getActualReadOnlyDataSource();
        dataSourceMap.put(DataSourceType.READ_ONLY, readOnlyDataSource);

        routingDataSource.setTargetDataSources(dataSourceMap);
        return routingDataSource;
    }

    private DataSource getActualReadOnlyDataSource() {
        DataSource readOnlyDataSource;
        final String readOnlyDataSourceUrl = domibusPropertyProvider.getProperty(DOMIBUS_DATASOURCE_REPLICA_URL);
        if (StringUtils.isNotBlank(readOnlyDataSourceUrl)) {
            LOG.info("Replica datasource url is set; creating read-only data source.");
            readOnlyDataSource = getReadOnlyDataSource();
        } else {
            // fallback to default datasource
            LOG.info("Replica datasource url is not set; falling back to read-write/default data source.");
            readOnlyDataSource = getReadWriteDataSource();
        }
        return readOnlyDataSource;
    }

}
