package eu.domibus.core.crypto;

import eu.domibus.api.crypto.TLSGenericCertificateManager;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.KeyStore;

/**
 * @author idragusa, Ionut Breaz
 * @since 4.1.1
 * <p>
 * X509TrustManager with the TLS Generic Truststore configured in Domibus and optionally the cacerts </p>
 * Use this for all https requests that do not involve communication between access points
 */
@Service
public class TLSGenericTrustManager extends AbstractTLSTrustManager {

    private final TLSGenericCertificateManager tlsGenericCertificateManager;
    private final DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    public TLSGenericTrustManager(TLSGenericCertificateManager tlsGenericCertificateManager, DomibusPropertyProvider domibusPropertyProvider) {
        this.tlsGenericCertificateManager = tlsGenericCertificateManager;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.tlsTruststoreType = "Generic";
    }

    @Override
    protected boolean shouldUseCacerts() {
        return Boolean.TRUE.equals(domibusPropertyProvider.getBooleanProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_USE_CACERTS));
    }

    @Override
    protected KeyStore getTrustStore() {
        return tlsGenericCertificateManager.getTLSGenericTruststore();
    }
}
