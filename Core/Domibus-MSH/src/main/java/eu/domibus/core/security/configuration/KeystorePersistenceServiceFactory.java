package eu.domibus.core.security.configuration;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.KeystorePersistenceService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.property.encryption.PasswordDecryptionService;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.core.certificate.KeyStorePersistenceServiceImpl;
import eu.domibus.core.certificate.pkcs11.HsmKeyStorePersistenceServiceImpl;
import eu.domibus.core.crypto.TruststoreDao;
import eu.domibus.core.property.DomibusRawPropertyProvider;
import eu.domibus.core.util.backup.BackupService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_KEYSTORE_PERSISTENCE_TYPE;
import static eu.domibus.core.crypto.spi.AbstractCryptoServiceSpi.KEYSTORE_PERSISTENCE_TYPE_PKCS11;

/**
 * @author G. Maier
 * @since 5.2
 */
@Service
public class KeystorePersistenceServiceFactory {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(KeystorePersistenceServiceFactory.class);

    private final CertificateHelper certificateHelper;

    protected final TruststoreDao truststoreDao;

    private final PasswordDecryptionService passwordDecryptionService;

    private final DomainContextProvider domainContextProvider;

    private final DomibusPropertyProvider domibusPropertyProvider;

    private final DomibusRawPropertyProvider domibusRawPropertyProvider;

    private final BackupService backupService;

    public KeystorePersistenceServiceFactory(CertificateHelper certificateHelper, TruststoreDao truststoreDao, PasswordDecryptionService passwordDecryptionService, DomainContextProvider domainContextProvider, DomibusPropertyProvider domibusPropertyProvider, DomibusRawPropertyProvider domibusRawPropertyProvider, BackupService backupService) {
        this.certificateHelper = certificateHelper;
        this.truststoreDao = truststoreDao;
        this.passwordDecryptionService = passwordDecryptionService;
        this.domainContextProvider = domainContextProvider;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domibusRawPropertyProvider = domibusRawPropertyProvider;
        this.backupService = backupService;
    }

    public KeystorePersistenceService getKeystorePersistenceService (Domain domain) {
        LOG.debug("Instantiating the KeystorePersistenceService for domain [{}]", domain);
        if (KEYSTORE_PERSISTENCE_TYPE_PKCS11.equals(domibusPropertyProvider.getProperty(domain, DOMIBUS_SECURITY_KEYSTORE_PERSISTENCE_TYPE))) {
            HsmKeyStorePersistenceServiceImpl hsmKeyStorePersistenceService = new HsmKeyStorePersistenceServiceImpl(certificateHelper, truststoreDao, passwordDecryptionService, domainContextProvider, domibusPropertyProvider, domibusRawPropertyProvider, backupService);
            hsmKeyStorePersistenceService.init();
            return hsmKeyStorePersistenceService;
        }
        return new KeyStorePersistenceServiceImpl(certificateHelper, truststoreDao, passwordDecryptionService, domainContextProvider, domibusPropertyProvider, domibusRawPropertyProvider, backupService);
    }
}
