package eu.domibus.core.ebms3.receiver.policy;

import eu.domibus.api.pmode.PModeConstants;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.core.ebms3.sender.interceptor.SetPolicyOutInterceptor;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.api.model.MessageType;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Exchange;
import org.springframework.stereotype.Service;

/**
 * @author Thomas Dussart
 * @since 3.3
 * In case of a pulled message, the outgoing is a user message with attachements which should
 * received the same processing as the outPut of a push message.
 */
@Service
public class SetPolicyOutInterceptorServer extends SetPolicyOutInterceptor {

    @Override
    public void handleMessage(final SoapMessage message) throws Fault {
        final Exchange exchange = message.getExchange();
        Object messageType = exchange.get(MSHDispatcher.MESSAGE_TYPE_OUT);
        if (MessageType.USER_MESSAGE.equals(messageType)) {
            super.handleMessage(message);
        } else {//Signal with non-repudiation
            SecurityProfileConfiguration originalSecurityProfileConfiguration = (SecurityProfileConfiguration) exchange.get(PModeConstants.SECURITY_PROFILE_CONFIGURATION);
            //we make a copy of the configuration because we modify the encryption property and to avoid modifying the original configuration.
            final SecurityProfileConfiguration securityProfileConfiguration = originalSecurityProfileConfiguration.copy();
            securityProfileConfiguration.encryptionEnabled(false);//Signal messages are not encrypted.
            final String pModeKey = (String) exchange.get(PModeConstants.PMODE_KEY_CONTEXT_PROPERTY);
            setSecurityPropertiesOnMessage(message, securityProfileConfiguration, pModeKey);
        }
    }
}
