package eu.domibus.core.crypto;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.MultiDomainCryptoService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.core.ebms3.receiver.SimpleKeystorePasswordCallback;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.wss4j.common.ConfigurationConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_PROFILE;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_PROFILES_ACTIVE;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
@Service
public class SecurityProfileHelper {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SecurityProfileHelper.class);

    public static final String DOMIBUS_CRYPTO = "domibusCrypto";

    protected SimpleKeystorePasswordCallback simpleKeystorePasswordCallback;
    protected MultiDomainCryptoService multiDomainCryptoService;
    protected DomibusPropertyProvider domibusPropertyProvider;
    protected DomainContextProvider domainContextProvider;

    public SecurityProfileHelper(SimpleKeystorePasswordCallback simpleKeystorePasswordCallback, MultiDomainCryptoService multiDomainCryptoService, DomibusPropertyProvider domibusPropertyProvider, DomainContextProvider domainContextProvider) {
        this.simpleKeystorePasswordCallback = simpleKeystorePasswordCallback;
        this.multiDomainCryptoService = multiDomainCryptoService;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domainContextProvider = domainContextProvider;
    }

    public boolean areSecurityProfilesDisabled(Domain domain) {
        return !domibusPropertyProvider.getBooleanProperty(domain, DOMIBUS_SECURITY_PROFILES_ACTIVE);
    }


    public boolean areSecurityProfilesDisabled() {
        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        return areSecurityProfilesDisabled(currentDomain);
    }


    public String getSecurityProfilePropertyName(String securityProfileCode, String propertySuffix) {
        return getSecurityProfilePropertyNamePrefix(securityProfileCode) + "." + propertySuffix;

    }

    private String getSecurityProfilePropertyNamePrefix(String securityProfileCode) {
        return DOMIBUS_SECURITY_PROFILE + "." + securityProfileCode;
    }

    public Map<String, Object> getIncomingSecurityConfigurationAsMap(SecurityProfileConfiguration securityProfileConfiguration, boolean attachmentsPresent) {
        Map<String, Object> result = new HashMap<>();

        final boolean signatureEnabled = securityProfileConfiguration.isSignatureEnabled();
        final boolean encryptionEnabled = securityProfileConfiguration.isEncryptionEnabled();
        final boolean useEncryption = attachmentsPresent && encryptionEnabled;

        result.put(ConfigurationConstants.ACTION, getSecurityActions(signatureEnabled, useEncryption));

        if (signatureEnabled) {
            result.put(ConfigurationConstants.SIG_ALGO, securityProfileConfiguration.getSignatureInSignatureAlgorithm());
            result.put(ConfigurationConstants.SIG_DIGEST_ALGO, securityProfileConfiguration.getSignatureInDigestAlgorithm());
        }

        if (useEncryption) {
            result.put(ConfigurationConstants.ENC_SYM_ALGO, securityProfileConfiguration.getEncryptionInSymAlgorithm());
            result.put(ConfigurationConstants.ENC_KEY_ID, securityProfileConfiguration.getEncryptionOutKeyIdentifier());
            result.put(ConfigurationConstants.ENCRYPTION_PARTS, securityProfileConfiguration.getEncryptionOutEncryptionParts());

            result.put(ConfigurationConstants.ENC_KEY_TRANSPORT, securityProfileConfiguration.getEncryptionInKeyTransportAlgorithm());

            //start RSA or similar
            result.put(ConfigurationConstants.ENC_MGF_ALGO, securityProfileConfiguration.getEncryptionInMGFAlgorithm());
            result.put(ConfigurationConstants.ENC_DIGEST_ALGO, securityProfileConfiguration.getEncryptionInDigestAlgorithm());
            //end RSA

            //start - for x25519 use the following; uncomment when upgrading to the latest CXF version
//            result.put(ConfigurationConstants.ENC_KEY_AGREEMENT_METHOD, keyAgreementMethod);
//            result.put(ConfigurationConstants.ENC_KEY_DERIVATION_FUNCTION, keyDerivationFunction);
            //end - for x25519
        }
        return result;
    }

    public Map<String, Object> getOutgoingSecurityConfigurationAsMap(SecurityProfileConfiguration securityProfileConfiguration) {
        Map<String, Object> result = new HashMap<>();

        final boolean signatureEnabled = securityProfileConfiguration.isSignatureEnabled();
        final boolean encryptionEnabled = securityProfileConfiguration.isEncryptionEnabled();
        final boolean useEncryption = encryptionEnabled;

        result.put(ConfigurationConstants.ACTION, getSecurityActions(signatureEnabled, useEncryption));
        result.put(ConfigurationConstants.PW_CALLBACK_REF, simpleKeystorePasswordCallback);

        if (signatureEnabled) {
            result.put(WSHandlerConstants.USE_SINGLE_CERTIFICATE, !securityProfileConfiguration.isUsePkiPath() + "");

            result.put(ConfigurationConstants.SIG_ALGO, securityProfileConfiguration.getSignatureOutSignatureAlgorithm());
            result.put(ConfigurationConstants.SIG_KEY_ID, securityProfileConfiguration.getSignatureOutKeyIdentifier());
            result.put(ConfigurationConstants.SIGNATURE_PARTS, securityProfileConfiguration.getSignatureOutSignedParts());
            result.put(ConfigurationConstants.SIG_DIGEST_ALGO, securityProfileConfiguration.getSignatureOutDigestAlgorithm());
            result.put(ConfigurationConstants.SIGNATURE_USER, securityProfileConfiguration.getSignaturePrivateKeyAlias());
            result.put(ConfigurationConstants.USER, securityProfileConfiguration.getSignaturePrivateKeyAlias());
            result.put(ConfigurationConstants.SIG_PROP_REF_ID, DOMIBUS_CRYPTO);
            result.put(DOMIBUS_CRYPTO, multiDomainCryptoService.getCrypto(securityProfileConfiguration.getDomain()));
        }

        if (useEncryption) {
            result.put(ConfigurationConstants.ENC_SYM_ALGO, securityProfileConfiguration.getEncryptionOutSymAlgorithm());
            result.put(ConfigurationConstants.ENC_KEY_ID, securityProfileConfiguration.getEncryptionOutKeyIdentifier());
            result.put(ConfigurationConstants.ENCRYPTION_PARTS, securityProfileConfiguration.getEncryptionOutEncryptionParts());

            result.put(ConfigurationConstants.ENC_KEY_TRANSPORT, securityProfileConfiguration.getEncryptionOutKeyTransportAlgorithm());

            //start RSA or similar
            result.put(ConfigurationConstants.ENC_MGF_ALGO, securityProfileConfiguration.getEncryptionOutMGFAlgorithm());
            result.put(ConfigurationConstants.ENC_DIGEST_ALGO, securityProfileConfiguration.getEncryptionOutDigestAlgorithm());
            //end RSA

            result.put(ConfigurationConstants.ENC_PROP_REF_ID, DOMIBUS_CRYPTO);
            result.put(DOMIBUS_CRYPTO, multiDomainCryptoService.getCrypto(securityProfileConfiguration.getDomain()));

            //start - for x25519 use the following; uncomment when upgrading to the latest CXF version
//            result.put(ConfigurationConstants.ENC_KEY_AGREEMENT_METHOD, keyAgreementMethod);
//            result.put(ConfigurationConstants.ENC_KEY_DERIVATION_FUNCTION, keyDerivationFunction);
            //end - for x25519
        }

        return result;
    }

    public String getSecurityActions(boolean signatureEnabled, boolean encryptionEnable) {
        if (!signatureEnabled && !encryptionEnable) {
            LOG.debug("No security enabled");
            return WSHandlerConstants.NO_SECURITY;
        }

        String result = "";
        if (signatureEnabled) {
            result += ConfigurationConstants.SIGNATURE;
        }
        if (encryptionEnable) {
            result += " " + ConfigurationConstants.ENCRYPTION;
        }
        return result;
    }
}
