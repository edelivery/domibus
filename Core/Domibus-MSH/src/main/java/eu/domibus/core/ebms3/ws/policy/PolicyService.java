
package eu.domibus.core.ebms3.ws.policy;

import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.common.model.configuration.LegConfiguration;
import org.apache.neethi.Policy;

/**
 * @author Arun Raj
 * @since 3.3
 */
public interface PolicyService {

    boolean isSecurityPolicySet(SecurityProfileConfiguration securityProfileConfiguration);

    boolean isNoSecurityPolicy(SecurityProfileConfiguration securityAlgorithm);

    boolean isNoEncryptionPolicy(SecurityProfileConfiguration securityProfileConfiguration);
}
