package eu.domibus.core.plugin.handler;

import eu.domibus.api.message.UserMessageSecurityService;
import eu.domibus.api.messaging.DuplicateMessageFoundException;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.MessageStatus;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.model.UserMessageLog;
import eu.domibus.api.security.AuthUtils;
import eu.domibus.api.usermessage.UserMessageDownloadEvent;
import eu.domibus.common.ErrorResult;
import eu.domibus.core.error.ErrorLogEntry;
import eu.domibus.core.error.ErrorLogService;
import eu.domibus.core.message.MessagingService;
import eu.domibus.core.message.UserMessageDefaultService;
import eu.domibus.core.message.UserMessageLogDefaultService;
import eu.domibus.core.metrics.Counter;
import eu.domibus.core.metrics.Timer;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.DuplicateMessageException;
import eu.domibus.messaging.MessageConstants;
import eu.domibus.messaging.MessageNotFoundException;
import eu.domibus.plugin.Submission;
import eu.domibus.plugin.handler.MessageRetriever;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Service used for retrieving messages (split from DatabaseMessageHandler)
 *
 * @author Ion Perpegel
 * @since 5.0
 */
@Service
public class MessageRetrieverImpl implements MessageRetriever {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageRetrieverImpl.class);

    protected final UserMessageDefaultService userMessageService;

    private final MessagingService messagingService;

    private final UserMessageLogDefaultService userMessageLogService;

    private final ErrorLogService errorLogService;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final UserMessageSecurityService userMessageSecurityService;

    private final AuthUtils authUtils;

    public MessageRetrieverImpl(UserMessageDefaultService userMessageService, MessagingService messagingService,
                                UserMessageLogDefaultService userMessageLogService, ErrorLogService errorLogService,
                                ApplicationEventPublisher applicationEventPublisher, UserMessageSecurityService userMessageSecurityService,
                                AuthUtils authUtils) {
        this.userMessageService = userMessageService;
        this.messagingService = messagingService;
        this.userMessageLogService = userMessageLogService;
        this.errorLogService = errorLogService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.userMessageSecurityService = userMessageSecurityService;
        this.authUtils = authUtils;
    }

    @Override
    @Transactional
    public Submission downloadMessage(String messageId) throws MessageNotFoundException {
        return downloadMessage(messageId, true);
    }

    @Override
    @Transactional
    @Timer(clazz = MessageRetrieverImpl.class, value = "downloadMessage")
    @Counter(clazz = MessageRetrieverImpl.class, value = "downloadMessage")
    public Submission downloadMessage(final String messageId, boolean markAsDownloaded) throws MessageNotFoundException {
        final UserMessage userMessage = checkMessageAuthorization(messageId, eu.domibus.common.MSHRole.RECEIVING);

        LOG.info("Downloading message with id [{}]", messageId);

        if (markAsDownloaded) {
            markMessageAsDownloaded(userMessage);
        }
        return messagingService.getSubmission(userMessage);
    }

    @Override
    @Transactional
    @Timer(clazz = MessageRetrieverImpl.class, value = "downloadMessage")
    @Counter(clazz = MessageRetrieverImpl.class, value = "downloadMessage")
    public Submission downloadMessage(final Long messageEntityId, boolean markAsDownloaded) throws MessageNotFoundException {
        final UserMessage userMessage = checkMessageAuthorization(messageEntityId);

        LOG.info("Downloading message with entity id [{}]", messageEntityId);

        if (markAsDownloaded) {
            markMessageAsDownloaded(userMessage);
        }
        return messagingService.getSubmission(userMessage);
    }

    @Override
    @Transactional
    @Timer(clazz = MessageRetrieverImpl.class, value = "downloadMessage")
    @Counter(clazz = MessageRetrieverImpl.class, value = "downloadMessage")
    public Submission downloadMessage(final Long messageEntityId) throws MessageNotFoundException {
        return downloadMessage(messageEntityId, true);
    }

    @Override
    @Transactional
    public Submission browseMessage(String messageId) throws MessageNotFoundException {
        try {
            final UserMessage userMessage = checkMessageAuthorization(messageId, eu.domibus.common.MSHRole.RECEIVING);
            return getSubmission(messageId, userMessage);
        } catch (eu.domibus.api.messaging.MessageNotFoundException e) {
            throw new MessageNotFoundException(e.getMessage(), e);
        }
    }

    private Submission getSubmission(String messageId, UserMessage userMessage) {
        try {
            return userMessageToSubmission(userMessage);
        } catch (eu.domibus.api.messaging.MessageNotFoundException ex) {
            LOG.info("Could not find message with id [{}] and RECEIVING role; trying the SENDING role.", messageId);
            return browseMessage(messageId, eu.domibus.common.MSHRole.SENDING);
        }
    }

    @Override
    @Transactional
    public Submission browseMessage(String messageId, eu.domibus.common.MSHRole mshRole) {
        final UserMessage userMessage = checkMessageAuthorization(messageId, mshRole);
        LOG.info("Browsing message with id [{}] and role [{}]", messageId, mshRole);
        return userMessageToSubmission(userMessage);
    }

    @Override
    @Transactional
    public Submission browseMessage(final Long messageEntityId) {
        final UserMessage userMessage = checkMessageAuthorization(messageEntityId);
        LOG.info("Browsing message with entity id [{}]", messageEntityId);
        return userMessageToSubmission(userMessage);
    }

    protected Submission userMessageToSubmission(UserMessage userMessage) {
        return messagingService.getSubmission(userMessage);
    }

    @Override
    @Transactional(rollbackFor = DuplicateMessageException.class)
    // rollback transaction in case of exception: this avoids overriding our exception with a framework one
    public eu.domibus.common.MessageStatus getStatus(final String messageId) throws DuplicateMessageException {
        try {
            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(messageId);

            final MessageStatus messageStatus = userMessageLogService.getMessageStatusById(messageId);
            return eu.domibus.common.MessageStatus.valueOf(messageStatus.name());
        } catch (eu.domibus.api.messaging.MessageNotFoundException exception) {
            return eu.domibus.common.MessageStatus.valueOf(MessageStatus.NOT_FOUND.name());
        } catch (DuplicateMessageFoundException exception) {
            throw new DuplicateMessageException(exception.getMessage());
        }
    }

    @Override
    @Transactional
    public eu.domibus.common.MessageStatus getStatus(String messageId, eu.domibus.common.MSHRole mshRole) {
        try {
            MSHRole role = MSHRole.valueOf(mshRole.name());
            UserMessage userMessage = userMessageService.getByMessageId(messageId, role);

            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(userMessage);

            final MessageStatus messageStatus = userMessageLogService.getMessageStatus(userMessage.getEntityId());
            return eu.domibus.common.MessageStatus.valueOf(messageStatus.name());
        } catch (eu.domibus.api.messaging.MessageNotFoundException exception) {
            return eu.domibus.common.MessageStatus.valueOf(MessageStatus.NOT_FOUND.name());
        }
    }

    @Override
    @Transactional
    public eu.domibus.common.MessageStatus getStatus(final Long messageEntityId) {
        try {
            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(messageEntityId);

            final MessageStatus messageStatus = userMessageLogService.getMessageStatus(messageEntityId);
            return eu.domibus.common.MessageStatus.valueOf(messageStatus.name());
        } catch (eu.domibus.api.messaging.MessageNotFoundException exception) {
            return eu.domibus.common.MessageStatus.valueOf(MessageStatus.NOT_FOUND.name());
        }
    }

    @Override
    @Transactional(rollbackFor = {MessageNotFoundException.class, DuplicateMessageException.class})
    // rollback transaction in case of exception: this avoids overriding our exception with a framework one
    public List<? extends ErrorResult> getErrorsForMessage(final String messageId) throws MessageNotFoundException, DuplicateMessageException {
        UserMessageLog userMessageLog = null;
        boolean messageExists = false;
        try {
            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(messageId);
            userMessageLog = userMessageLogService.findByMessageId(messageId, null);
            messageExists = true;
        } catch (DuplicateMessageFoundException e) {
            throw new DuplicateMessageException(e.getMessage(), e.getCause());
        } catch (eu.domibus.api.messaging.MessageNotFoundException messageNotFoundException) {
            LOG.debug("Message with id [{}] not found", messageId);
        }
        List<ErrorLogEntry> errorsForMessage = errorLogService.getErrorsForMessage(messageId);
        if (!messageExists && CollectionUtils.isEmpty(errorsForMessage)) {
            throw new MessageNotFoundException("Message [" + messageId + "] does not exist");
        }

        if (userMessageLog == null && CollectionUtils.isEmpty(errorsForMessage)) {
            throw new MessageNotFoundException("Message [" + messageId + "] does not exist");
        }
        return errorsForMessage.stream().map(errorLogService::convert).collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = MessageNotFoundException.class)
    // rollback transaction in case of exception: this avoids overriding our exception with a framework one
    public List<? extends ErrorResult> getErrorsForMessage(String messageId, eu.domibus.common.MSHRole mshRole) throws MessageNotFoundException {
        MSHRole role = MSHRole.valueOf(mshRole.name());
        boolean messageExists = false;
        try {
            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(messageId, role);
            messageExists = true;
        } catch (eu.domibus.api.messaging.MessageNotFoundException messageNotFoundException) {
            LOG.info("Message [" + messageId + "]-[" + role + "] does not exist");
        }

        List<? extends ErrorResult> errorResults = errorLogService.getErrors(messageId, role);
        if (!messageExists && CollectionUtils.isEmpty(errorResults)) {
            throw new MessageNotFoundException("Message [" + messageId + "] does not exist");
        }
        return errorResults;
    }

    @Override
    @Transactional(rollbackFor = MessageNotFoundException.class)
    public List<? extends ErrorResult> getErrorsForMessage(Long messageEntityId) throws MessageNotFoundException {
        boolean messageExists = false;
        try {
            userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(messageEntityId);
            messageExists = true;
        } catch (eu.domibus.api.messaging.MessageNotFoundException messageNotFoundException) {
            LOG.info("Message with entity id [" + messageEntityId + "] does not exist");
        }

        List<? extends ErrorResult> errorResults = errorLogService.getErrorsForMessage(messageEntityId);
        if (!messageExists && CollectionUtils.isEmpty(errorResults)) {
            throw new MessageNotFoundException("Message with entity id [" + messageEntityId + "] does not exist");
        }
        return errorResults;
    }

    @Transactional
    @Override
    public void markMessageAsDownloaded(String messageId) {
        final UserMessage userMessage = checkMessageAuthorization(messageId, eu.domibus.common.MSHRole.RECEIVING);
        LOG.info("Setting the status of the message with id [{}] to downloaded", messageId);

        //we cannot throw MessageNotFoundException as we break the backward compatibility;
        try {
            markMessageAsDownloaded(userMessage);
        } catch (MessageNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private void markMessageAsDownloaded(UserMessage userMessage) throws MessageNotFoundException {
        final long messageEntityId = userMessage.getEntityId();
        final UserMessageLog messageLog = userMessageLogService.findById(messageEntityId);
        if (messageLog == null) {
            throw new MessageNotFoundException("Could not find message with entity id [" + messageEntityId + "]");
        }

        if (MessageStatus.DOWNLOADED == messageLog.getMessageStatus()) {
            LOG.debug("Message [{}] is already downloaded", userMessage.getMessageId());
            return;
        }
        if (MessageStatus.RECEIVED == messageLog.getMessageStatus()) {
            MSHRole mshRole = userMessage.getMshRole().getRole();
            publishDownloadEvent(userMessage.getMessageId(), mshRole);
            userMessageLogService.setMessageAsDownloaded(userMessage, messageLog);
            return;
        }
        throw new MessageNotFoundException("Could not find message with entity id [" + messageEntityId + "] in status RECEIVED. Current status is [" + messageLog.getMessageStatus() + "]");
    }

    @Transactional
    @Override
    public void markMessageAsDownloaded(Long messageEntityId) throws MessageNotFoundException {
        final UserMessage userMessage = checkMessageAuthorization(messageEntityId);
        LOG.info("Setting the status of the message with entity id [{}] to downloaded", messageEntityId);

        markMessageAsDownloaded(userMessage);
    }

    /**
     * Publishes a download event to be caught in case of transaction rollback
     *
     * @param messageId message id of the message that is being downloaded
     * @param role
     */
    protected void publishDownloadEvent(String messageId, MSHRole role) {
        UserMessageDownloadEvent downloadEvent = new UserMessageDownloadEvent();
        downloadEvent.setMessageId(messageId);
        String roleName = role.name();
        downloadEvent.setMshRole(roleName);
        LOG.debug("Publishing [{}] for message [{}] and role [{}]", downloadEvent.getClass().getName(), messageId, roleName);
        applicationEventPublisher.publishEvent(downloadEvent);
    }

    protected UserMessage checkMessageAuthorization(String messageId, eu.domibus.common.MSHRole mshRole) {
        MSHRole role = MSHRole.valueOf(mshRole.name());
        return checkMessageAuthorization(() -> userMessageService.getByMessageId(messageId, role));
    }

    protected UserMessage checkMessageAuthorization(Long messageEntityId) {
        return checkMessageAuthorization(() -> userMessageService.getByMessageEntityId(messageEntityId));
    }

    protected UserMessage checkMessageAuthorization(Supplier<UserMessage> messageGetter) {
        checkUserRoleWithUnsecuredLoginAllowed();

        final UserMessage userMessage = messageGetter.get();
        userMessageSecurityService.checkMessageAuthorizationWithUnsecureLoginAllowed(userMessage, MessageConstants.FINAL_RECIPIENT);
        return userMessage;
    }

    private void checkUserRoleWithUnsecuredLoginAllowed() {
        if (authUtils.isUnsecureLoginAllowed()) {
            return;
        }
        authUtils.checkHasAdminRoleOrUserRoleWithOriginalUser();
    }

}
