package eu.domibus.core.crypto;

import eu.domibus.api.crypto.TLSMshCertificateManager;
import eu.domibus.api.property.DomibusPropertyChangeListener;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

/**
 * Resets the TLS cache when the TLS truststore property changes
 */
@Service
public class TLSMshTruststorePropertyChangeListener implements DomibusPropertyChangeListener {

    private static final Logger LOG = DomibusLoggerFactory.getLogger(TLSMshTruststorePropertyChangeListener.class);

    private final TLSMshCertificateManager tlsMshCertificateManager;

    public TLSMshTruststorePropertyChangeListener(TLSMshCertificateManager tlsMshCertificateManager) {
        this.tlsMshCertificateManager = tlsMshCertificateManager;
    }

    @Override
    public boolean handlesProperty(String propertyName) {
        return StringUtils.equalsIgnoreCase(propertyName, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION)
                || StringUtils.equalsIgnoreCase(propertyName, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_TRUSTSTORE_PASSWORD)
                || StringUtils.equalsIgnoreCase(propertyName, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_TRUSTSTORE_TYPE)
                || StringUtils.equalsIgnoreCase(propertyName, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_CIPHER_SUITES_INCLUDED)
                || StringUtils.equalsIgnoreCase(propertyName, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_CIPHER_SUITES_EXCLUDED)
                || StringUtils.equalsIgnoreCase(propertyName, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_SECURE_SOCKET_PROTOCOL)
                || StringUtils.equalsIgnoreCase(propertyName, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_DISABLE_CN_CHECK)
                ;
    }

    @Override
    public void propertyValueChanged(String domainCode, String propertyName, String propertyValue) {
        LOG.debug("Property [{}] value has changed: resetting TLS truststore cache.", propertyName);

        tlsMshCertificateManager.resetTLSTruststore();
    }

}
