package eu.domibus.core.payload.persistence;

import eu.domibus.api.payload.DomibusPayloadException;
import eu.domibus.core.spi.payload.DeleteFolderResult;
import eu.domibus.core.spi.payload.DomibusPayloadManagerSpi;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

@Service
public class DatabasePayloadManagerSpi implements DomibusPayloadManagerSpi {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DatabasePayloadManagerSpi.class);


    @Override
    public void initialize(String domain) {
        LOG.debug("Initializing DatabasePayloadManagerSpi: do nothing");
    }

    @Override
    public String getIdentifier() {
        return DOMIBUS_DATABASE_SPI;
    }

    @Override
    public boolean arePayloadsStoredInDomibusDatabase() {
        return true;
    }

    @Override
    public String getAbsolutePayloadLocation(String domain, String relativeLocation) {
        throw new DomibusPayloadException("getAbsolutePayloadLocation operation is not supported");
    }

    @Override
    public void deletePayload(String domain, String absoluteFileLocation) {
        throw new DomibusPayloadException("deletePayload operation is not supported");
    }

    @Override
    public InputStream readPayload(String domain, String absoluteFileLocation) {
        throw new DomibusPayloadException("getPayload operation is not supported");
    }

    @Override
    public OutputStream getStreamForStoringPayload(String domain, String relativeFileLocation) {
        return new ByteArrayOutputStream(PayloadPersistence.DEFAULT_BUFFER_SIZE);
    }

    @Override
    public DeleteFolderResult deleteFolder(String domain, String absoluteFolderLocation) {
        throw new DomibusPayloadException("deleteFolder operation is not supported");
    }
}
