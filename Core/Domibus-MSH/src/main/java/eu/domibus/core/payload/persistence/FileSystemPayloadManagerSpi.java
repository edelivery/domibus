package eu.domibus.core.payload.persistence;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.payload.DomibusPayloadException;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.spi.payload.DeleteFolderResult;
import eu.domibus.core.spi.payload.DomibusPayloadManagerSpi;
import eu.domibus.core.util.WarningUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_ATTACHMENT_STORAGE_LOCATION;


@Service
public class FileSystemPayloadManagerSpi implements DomibusPayloadManagerSpi {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(FileSystemPayloadManagerSpi.class);

    protected final DomibusPropertyProvider domibusPropertyProvider;

    public FileSystemPayloadManagerSpi(DomibusPropertyProvider domibusPropertyProvider) {
        this.domibusPropertyProvider = domibusPropertyProvider;
    }

    @Override
    public void initialize(String domain) {
        LOG.info("Initializing file system payload storage directory for domain [{}]", domain);

        getPayloadStorageDirectory(domain);
    }

    @Override
    public String getIdentifier() {
        return DOMIBUS_FILE_SYSTEM_SPI;
    }

    @Override
    public boolean arePayloadsStoredInDomibusDatabase() {
        return false;
    }

    @Override
    public String getAbsolutePayloadLocation(String domain, String relativeLocation) {
        final File payloadStorageDirectory = getPayloadStorageDirectory(domain);
        return new File(payloadStorageDirectory, relativeLocation).getAbsolutePath();
    }

    @Override
    public InputStream readPayload(String domain, String absoluteFileLocation) {
        try {
            return new FileInputStream(absoluteFileLocation);
        } catch (FileNotFoundException e) {
            throw new DomibusPayloadException("Could not open stream for [" + absoluteFileLocation + "]", e);
        }
    }

    @Override
    public void deletePayload(String domain, String absoluteFileLocation) {
        if (StringUtils.isBlank(absoluteFileLocation)) {
            LOG.warn("Empty filename used to delete payload on filesystem!");
            return;
        }

        try {
            Path path = Paths.get(absoluteFileLocation);
            if (path == null) {
                LOG.warn("Trying to delete an empty path, filename [{}]", absoluteFileLocation);
                return;
            }
            Files.delete(path);
        } catch (IOException e) {
            LOG.debug("Problem deleting payload data files", e);
        }
    }

    @Override
    public OutputStream getStreamForStoringPayload(String domain, String relativeFileLocation) {
        final File payloadStorageDirectory = getPayloadStorageDirectory(domain);
        final File payloadFile = new File(payloadStorageDirectory, relativeFileLocation);

        try {
            FileUtils.forceMkdir(payloadFile.getParentFile());
        } catch (IOException e) {
            throw new DomibusPayloadException("Could not create directory structure for storing payload [" + payloadFile + "]", e);
        }

        try {
            return new FileOutputStream(payloadFile);//NOSONAR the stream is closed in the finally block
        } catch (FileNotFoundException e) {
            throw new DomibusPayloadException("Could not open file stream for storing payload [" + payloadFile + "]", e);
        }
    }

    protected File getPayloadStorageDirectory(String domain) {
        final Domain domainInstance = new Domain(domain, domain);
        final String location = domibusPropertyProvider.getProperty(domainInstance, DOMIBUS_ATTACHMENT_STORAGE_LOCATION);
        if (StringUtils.isBlank(location)) {
            LOG.warn("No file storage defined. The property 'domibus.attachment.storage.location' is not configured.");
            return null;
        }

        LOG.info("Initializing payload storage directory [{}]", location);
        Path path = createOrGetLocation(location);
        if (path == null) {
            throw new DomibusPayloadException("There was an error initializing the payload folder, so Domibus will be using the database");
        }

        LOG.info("Initialized payload folder on path [{}] for domain [{}]", path, domain);
        return path.normalize().toFile();
    }

    /**
     * It attempts to create the directory whenever is not present.
     * It works also when the location is a symbolic link.
     */
    protected Path createOrGetLocation(String path) {
        Path payloadPath;
        try {
            payloadPath = Paths.get(path).normalize();
            if (!payloadPath.isAbsolute()) {
                throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Relative path [" + payloadPath + "] is forbidden. Please provide absolute path for payload storage");
            }
            // Checks if the path exists, if not it creates it
            if (Files.notExists(payloadPath)) {
                Files.createDirectories(payloadPath);
                LOG.info("The payload folder [{}] has been created!", payloadPath.toAbsolutePath());
            } else {
                if (Files.isSymbolicLink(payloadPath)) {
                    payloadPath = Files.readSymbolicLink(payloadPath);
                }

                if (!Files.isWritable(payloadPath)) {
                    throw new IOException("Write permission for payload folder " + payloadPath.toAbsolutePath() + " is not granted.");
                }
            }
        } catch (IOException ioEx) {
            LOG.error("Error creating/accessing the payload folder [{}]", path, ioEx);

            // Takes temporary folder by default if it faces any issue while creating defined path.
            payloadPath = Paths.get(System.getProperty("java.io.tmpdir")).normalize();
            LOG.warn(WarningUtil.warnOutput("The temporary payload folder " + payloadPath.toAbsolutePath() + " has been selected!"));
        }
        return payloadPath;
    }

    @Override
    public DeleteFolderResult deleteFolder(String currentDomain, String absoluteFolderLocation) {
        DeleteFolderResult deleteFolderResult = new DeleteFolderResult();
        deleteFolderResult.setTotal(1);//We delete one folder.
        String absolutePayloadLocation = getAbsolutePayloadLocation(currentDomain, absoluteFolderLocation);
        LOG.info("Delete folder [{}]", absolutePayloadLocation);
        try {
            FileUtils.deleteDirectory(new File(absolutePayloadLocation));
            deleteFolderResult.setResult(DeleteFolderResult.Result.OK);
        } catch (IOException e) {
            LOG.error("Folder could not be deleted [{}]", absolutePayloadLocation, e);
            deleteFolderResult.setResult(DeleteFolderResult.Result.ERROR);
            deleteFolderResult.addFailed(List.of(String.format("Folder could not be deleted [%s] [%s] for domain [%s]", absolutePayloadLocation, e.getMessage(), currentDomain)));
        }
        return deleteFolderResult;
    }
}
