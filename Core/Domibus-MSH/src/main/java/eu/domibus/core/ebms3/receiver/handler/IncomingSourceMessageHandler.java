package eu.domibus.core.ebms3.receiver.handler;

import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.PartInfo;
import eu.domibus.api.model.ProcessingType;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.common.ErrorCode;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.message.UserMessagePayloadService;
import eu.domibus.core.payload.persistence.DomibusPayloadManagerSpiProviderImpl;
import eu.domibus.core.spi.payload.DomibusPayloadManagerSpi;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.List;

/**
 * Handles the incoming source message for SplitAndJoin mechanism
 *
 * @author Cosmin Baciu
 * @since 4.1
 */
@Service
public class IncomingSourceMessageHandler extends AbstractIncomingMessageHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(IncomingSourceMessageHandler.class);


    protected final DomibusPayloadManagerSpiProviderImpl domibusPayloadManagerSpiProvider;

    protected final UserMessagePayloadService userMessagePayloadService;

    public IncomingSourceMessageHandler(DomibusPayloadManagerSpiProviderImpl domibusPayloadManagerSpiProvider,
                                        UserMessagePayloadService userMessagePayloadService) {
        this.domibusPayloadManagerSpiProvider = domibusPayloadManagerSpiProvider;
        this.userMessagePayloadService = userMessagePayloadService;
    }

    @Override
    protected SOAPMessage processMessage(SecurityProfile securityProfile, String pmodeKey, ProcessingType processingType, SOAPMessage request, Ebms3Messaging ebms3Messaging) throws EbMS3Exception, TransformerException, IOException, JAXBException, SOAPException {
        LOG.debug("Processing SourceMessage");

        final DomibusPayloadManagerSpi domibusPayloadManagerSpiForCurrentDomain = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpiForCurrentDomain();

        if (domibusPayloadManagerSpiForCurrentDomain.arePayloadsStoredInDomibusDatabase()) {
            final String message = "SplitAndJoin does not work when payloads are stored in the database. Please provide a different storage.";
            LOG.error(message);
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0002)
                    .message(message)
                    .refToMessageId(ebms3Messaging.getUserMessage().getMessageInfo().getMessageId())
                    .mshRole(MSHRole.RECEIVING)
                    .build();
        }

        List<PartInfo> partInfoList = userMessagePayloadService.handlePayloads(request, ebms3Messaging, null);
        UserMessage userMessage = ebms3Converter.convertFromEbms3(ebms3Messaging.getUserMessage());
        return userMessageHandlerService.handleNewSourceUserMessage(pmodeKey, processingType, request, userMessage, partInfoList);
    }
}
