package eu.domibus.core.crypto;

import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.DomibusCertificateException;
import eu.domibus.api.pki.SecurityProfileService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.CertificatePurpose;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.security.SecurityProfileException;
import eu.domibus.core.ebms3.SoapElementsExtractorUtilImpl;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_KEY_PRIVATE_ALIAS;

/**
 * Provides services needed by the Security Profiles feature
 *
 * @author Lucian FURCA
 * @since 5.1
 */
@Service
public class SecurityProfileServiceImpl implements SecurityProfileService {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SecurityProfileServiceImpl.class);


    protected final PModeProvider pModeProvider;

    protected final DomainContextProvider domainContextProvider;

    protected final SoapElementsExtractorUtilImpl soapElementsExtractorUtil;

    protected final DomibusPropertyProvider domibusPropertyProvider;

    protected SecurityProfileHelper securityProfileHelper;

    public SecurityProfileServiceImpl(PModeProvider pModeProvider,
                                      DomainContextProvider domainContextProvider,
                                      SoapElementsExtractorUtilImpl soapElementsExtractorUtil,
                                      DomibusPropertyProvider domibusPropertyProvider,
                                      SecurityProfileHelper securityProfileHelper) {
        this.pModeProvider = pModeProvider;
        this.domainContextProvider = domainContextProvider;
        this.soapElementsExtractorUtil = soapElementsExtractorUtil;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.securityProfileHelper = securityProfileHelper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCertificateAliasForPurpose(String partyName, SecurityProfileConfiguration securityProfileConfiguration, CertificatePurpose certificatePurpose) {
        switch (certificatePurpose) {
            case SIGN:
            case ENCRYPT:
            case DECRYPT:
                return getSecurityProfileAlias(partyName, securityProfileConfiguration, certificatePurpose);
            default:
                throw new DomibusCertificateException("Invalid certificate usage [" + certificatePurpose + "]");
        }
    }

    private String getSecurityProfileAlias(String partyName, SecurityProfileConfiguration securityProfileConfiguration, CertificatePurpose certificatePurpose) {
        String alias = partyName;
        if (securityProfileConfiguration != null && !securityProfileConfiguration.isLegacyConfiguration()) {
            final SecurityProfile securityProfile = securityProfileConfiguration.getSecurityProfile();
            alias = partyName + "_" + StringUtils.lowerCase(securityProfile.getCode()) + "_" + certificatePurpose.getCertificatePurpose().toLowerCase();
        }
        LOG.info("The following alias was determined for [{}]ing: [{}]", certificatePurpose.getCertificatePurpose().toLowerCase(), alias);
        return alias;
    }

    @Override
    public CertificatePurpose extractCertificatePurpose(String alias) {
        final String purposeValue = StringUtils.substringAfterLast(alias, "_");
        if(StringUtils.isEmpty(purposeValue)) {
            throw new DomibusCertificateException("No certificate purpose found for alias [" + alias + "]");
        }
        return CertificatePurpose.lookupByName(purposeValue);
    }

    @Override
    public SecurityProfile extractSecurityProfile(List<SecurityProfile> securityProfiles, String alias) {
        final String securityProfileCode = StringUtils.substringAfterLast(StringUtils.substringBeforeLast(alias, "_"), "_").toLowerCase();
        return securityProfiles.stream()
                .filter(securityProfile -> securityProfile.getCode().equalsIgnoreCase(securityProfileCode))
                .findFirst()
                .orElseThrow(() -> new DomibusCertificateException("No Security Profile found for alias [" + alias + "]"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SecurityProfile getTheHighestRankingDefinedSecurityProfile(List<SecurityProfile> definedSecurityProfilesList, List<SecurityProfile> securityProfilesOrderList)
            throws SecurityProfileException {

        if (securityProfilesOrderList == null) {
            throw new SecurityProfileException("No Security Profiles order list can't be null");
        }

        return securityProfilesOrderList.stream()
                .filter(definedSecurityProfilesList::contains)
                .findFirst()
                .orElseThrow(() -> new SecurityProfileException("No Security Profile from the Security Profiles priority list was defined in domibus.properties"));
    }
}
