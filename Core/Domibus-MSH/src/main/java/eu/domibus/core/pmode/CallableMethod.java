package eu.domibus.core.pmode;

import eu.domibus.core.ebms3.EbMS3Exception;

@FunctionalInterface
public interface CallableMethod {
    void execute() throws EbMS3Exception;
}
