package eu.domibus.core.crypto;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.pki.SecurityProfileService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.util.DomibusStringUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
@Service
public class DomainSecurityProfileProviderFactory {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomainSecurityProfileProviderFactory.class);

    @Autowired
    SecurityProfileService securityProfileService;

    @Autowired
    SecurityProfileValidatorService securityProfileValidatorService;

    @Autowired
    DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    SecurityProfileHelper securityProfileHelper;

    @Autowired
    DomibusStringUtil domibusStringUtil;

    public DomainSecurityProfileProvider domainSecurityProfileProvider(Domain domain) {
        LOG.debug("Instantiating the certificate provider for domain [{}]", domain);

        final DomainSecurityProfileProvider bean = new DomainSecurityProfileProvider(domain, securityProfileService, securityProfileValidatorService, domibusPropertyProvider, securityProfileHelper, domibusStringUtil);
        bean.init();
        return bean;
    }
}
