package eu.domibus.core.pmode.validation.validators;

import eu.domibus.api.pmode.ValidationIssue;
import eu.domibus.common.model.configuration.Configuration;
import eu.domibus.common.model.configuration.ErrorHandling;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.sender.retry.RetryStrategy;
import eu.domibus.core.pmode.validation.PModeValidationHelper;
import eu.domibus.core.pmode.validation.PModeValidator;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Ion Perpegel
 * @since 5.2
 * <p>
 * Validates maximum one default error handler and ideally one
 *
 */
@Component
@Order(10)
public class ErrorHandlerValidator implements PModeValidator {

    @Override
    public List<ValidationIssue> validate(Configuration pMode) {
        List<ValidationIssue> issues = new ArrayList<>();

        Long defaultEHCount = pMode.getBusinessProcesses().getErrorHandlings().stream()
                .filter(errorHandling -> BooleanUtils.isTrue(errorHandling.isDefault()))
                .count();

        if(defaultEHCount == 0) {
            issues.add( new ValidationIssue("It is advisable to have a default error handler", ValidationIssue.Level.WARNING));
        }

        if(defaultEHCount > 1) {
            issues.add(new ValidationIssue("No more than one default error handler is allowed", ValidationIssue.Level.ERROR));
        }

        return issues;
    }

}
