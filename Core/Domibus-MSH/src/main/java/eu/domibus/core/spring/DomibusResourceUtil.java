package eu.domibus.core.spring;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.configuration.spi.DomibusResource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DomibusResourceUtil {


    public void copyToFile(DomibusResource domibusResource,
                           File targetFile) {
        final byte[] bytes = domibusResourceToByteArray(domibusResource);

        try {
            FileUtils.writeByteArrayToFile(targetFile, bytes);
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not write Domibus resource [" + domibusResource.getName() + "] to file [" + targetFile + "]", e);
        }
    }

    public Resource domibusResourceToResource(DomibusResource domibusResource) {
        final byte[] bytes = domibusResourceToByteArray(domibusResource);
        return new ByteArrayResource(bytes);
    }

    public List<String> getAsLines(DomibusResource domibusResource) {
        List<String> lines = new ArrayList<>();

        final byte[] fileContentBytes = domibusResourceToByteArray(domibusResource);
        final String fileContent = new String(fileContentBytes, StandardCharsets.UTF_8);

        try (Scanner scanner = new Scanner(fileContent)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lines.add(line);
            }
        }
        return lines;
    }

    public byte[] domibusResourceToByteArray(DomibusResource domibusResource) {
        InputStream inputStream = domibusResource.getInputStream();
        try {
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not get the contents of the Domibus resource [" + domibusResource.getName() + "]", e);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not close input stream for Domibus resource [" + domibusResource.getName() + "]", e);
            }
        }
    }
}
