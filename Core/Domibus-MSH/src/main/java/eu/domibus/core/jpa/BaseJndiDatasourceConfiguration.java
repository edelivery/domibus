package eu.domibus.core.jpa;

import eu.domibus.api.datasource.DataSourceConstants;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.jndi.JndiObjectFactoryBean;

import javax.sql.DataSource;

/**
 * @author Ion Perpegel
 * @since 5.2
 *
 * This class should not be annotated with @Configuration because it is done only in weblogic and wildfly servers and not in tomcat
 */
public abstract class BaseJndiDatasourceConfiguration {

    private static final DomibusLogger LOGGER = DomibusLoggerFactory.getLogger(BaseJndiDatasourceConfiguration.class);

    private JndiObjectFactoryBean jndiObjectFactoryBean;
    private JndiObjectFactoryBean jndiObjectFactoryQuartzBean;

    @Bean(DataSourceConstants.DOMIBUS_JDBC_DATA_SOURCE)
    public JndiObjectFactoryBean domibusDatasource(DomibusPropertyProvider domibusPropertyProvider) {
        if (jndiObjectFactoryBean == null) {
            jndiObjectFactoryBean = new JndiObjectFactoryBean();
            jndiObjectFactoryBean.setExpectedType(DataSource.class);
            String jndiName = domibusPropertyProvider.getProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_JDBC_DATASOURCE_JNDI_NAME);

            LOGGER.debug("Configured property [{}] with [{}]", DomibusPropertyMetadataManagerSPI.DOMIBUS_JDBC_DATASOURCE_JNDI_NAME, jndiName);
            jndiObjectFactoryBean.setJndiName(jndiName);
        }
        return jndiObjectFactoryBean;
    }

    @Bean(DataSourceConstants.DOMIBUS_JDBC_QUARTZ_DATA_SOURCE)
    public JndiObjectFactoryBean quartzDatasource(DomibusPropertyProvider domibusPropertyProvider) {
        if (jndiObjectFactoryQuartzBean == null) {
            jndiObjectFactoryQuartzBean = new JndiObjectFactoryBean();
            jndiObjectFactoryQuartzBean.setExpectedType(DataSource.class);
            String jndiName = domibusPropertyProvider.getProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_JDBC_DATASOURCE_QUARTZ_JNDI_NAME);

            LOGGER.debug("Configured property [{}] with [{}]", DomibusPropertyMetadataManagerSPI.DOMIBUS_JDBC_DATASOURCE_QUARTZ_JNDI_NAME, jndiName);
            jndiObjectFactoryQuartzBean.setJndiName(jndiName);
        }
        return jndiObjectFactoryQuartzBean;
    }

}
