package eu.domibus.core.pmode.provider.dynamicdiscovery;

import eu.domibus.api.dynamicdyscovery.DynamicDiscoveryLookupEntity;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.pki.CertificateService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author Cosmin Baciu
 * @since 5.1.1
 */
@Service
public class DynamicDiscoveryLookupHelper {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DynamicDiscoveryLookupHelper.class);

    protected DynamicDiscoveryLookupDao dynamicDiscoveryLookupDao;
    protected CertificateService certificateService;

    public DynamicDiscoveryLookupHelper(DynamicDiscoveryLookupDao dynamicDiscoveryLookupDao, CertificateService certificateService) {
        this.dynamicDiscoveryLookupDao = dynamicDiscoveryLookupDao;
        this.certificateService = certificateService;
    }

    @Transactional
    public List<DynamicDiscoveryLookupEntity> deleteFromDatabaseExpiredDdcFinalRecipients(Date dateLimit) {
        LOG.info("Getting the DDC final recipients which were not discovered more recently than [{}]", dateLimit);

        final List<DynamicDiscoveryLookupEntity> finalRecipientsNotDiscoveredInTheLastPeriod = dynamicDiscoveryLookupDao.findFinalRecipientsNotDiscoveredInTheLastPeriod(dateLimit);
        final List<String> finalRecipients = finalRecipientsNotDiscoveredInTheLastPeriod.stream().map(discoveryLookupEntity -> discoveryLookupEntity.getFinalRecipientValue()).collect(Collectors.toList());

        LOG.info("Deleting [{}] from database the DDC final recipients not discovered more recently than [{}] with the following final recipients: [{}]", finalRecipients.size(), dateLimit, finalRecipients);
        dynamicDiscoveryLookupDao.deleteAll(finalRecipientsNotDiscoveredInTheLastPeriod);

        return finalRecipientsNotDiscoveredInTheLastPeriod;
    }

    /**
     * Saves the time when the DDC lookup was performed last time from SMP
     */
    @Transactional
    public void saveDynamicDiscoveryLookupTime(String finalRecipientValue,
                                               String finalRecipientUrl,
                                               String partyName,
                                               String partyType,
                                               List<String> partyProcessNames,
                                               String certificateCn,
                                               final X509Certificate certificate) {
        LOG.debug("Saving the certificate discovery date for certificate with alias [{}]", finalRecipientValue);

        DynamicDiscoveryLookupEntity lookupEntity = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipientValue);
        if (lookupEntity == null) {
            LOG.info("Creating DDC lookup entity for final recipient [{}]", finalRecipientValue);
            lookupEntity = new DynamicDiscoveryLookupEntity();
            lookupEntity.setFinalRecipientValue(finalRecipientValue);
        }

        lookupEntity.setFinalRecipientUrl(finalRecipientUrl);
        lookupEntity.setPartyName(partyName);
        lookupEntity.setPartyType(partyType);
        lookupEntity.setPartyProcesses(partyProcessNames);
        lookupEntity.setCn(certificateCn);

        if (certificate == null) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_004, "Could not save the dynamic discovery lookup entity: provided X509 certificate is null");
        }

        lookupEntity.setSubject(certificate.getSubjectDN().getName());
        lookupEntity.setSerial(certificate.getSerialNumber() + "");
        lookupEntity.setIssuerSubject(certificate.getIssuerDN().getName());
        final String fingerprint = certificateService.extractFingerprints(certificate);
        lookupEntity.setFingerprint(fingerprint);

        final Date dynamicDiscoveryUpdateTime = new Date();
        lookupEntity.setDynamicDiscoveryTime(dynamicDiscoveryUpdateTime);


        LOG.debug("Saving/updating DDC lookup details [{}]", lookupEntity);
        try {
            dynamicDiscoveryLookupDao.createOrUpdate(lookupEntity);
        } catch (DataIntegrityViolationException e) {
            //in a cluster environment, a DDC lookup entity can be created/updated in parallel and a unique constraint is raised
            //in case a constraint violation occurs we don't do anything because the other node added the latest data in parallel
            LOG.warn("Could not create or update DDC lookup entity with entity id [{}]. It could be that another node updated the same entity in parallel", lookupEntity.getEntityId(), e);
        }
    }

}
