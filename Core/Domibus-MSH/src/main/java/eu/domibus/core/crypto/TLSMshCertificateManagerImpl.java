package eu.domibus.core.crypto;

import eu.domibus.api.cluster.SignalService;
import eu.domibus.api.crypto.CryptoException;
import eu.domibus.api.crypto.NoKeyStoreContentInformationException;
import eu.domibus.api.crypto.TLSMshCertificateManager;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.pki.CertificateService;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.TrustStoreEntry;
import eu.domibus.common.DomibusCacheConstants;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.core.property.DomibusRawPropertyProvider;
import eu.domibus.core.util.TLSMshTrustManager;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.configuration.security.FiltersType;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

/**
 * @author Ion Perpegel
 * @since 5.0
 */
@Service
public class TLSMshCertificateManagerImpl implements TLSMshCertificateManager {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSMshCertificateManagerImpl.class);

    public static final String TLS_CACHE = "tlsCache";

    private final CertificateService certificateService;

    private final TLSMshCertificateManagerHelper tlsMshCertificateManagerHelper;

    private final SignalService signalService;

    protected final DomainService domainService;

    protected final CertificateHelper certificateHelper;

    protected final DomibusPropertyProvider domibusPropertyProvider;

    protected final DomibusRawPropertyProvider domibusRawPropertyProvider;

    protected final DomainContextProvider domainContextProvider;

    protected final TLSMshTrustManager tlsMshTrustManager;

    protected final TLSMshTrustStorePersistenceInfoImpl tlsMshTruststorePersistenceInfo;

    protected final TLSMshKeyStorePersistenceInfoImpl tlsMshKeystorePersistenceInfo;

    public TLSMshCertificateManagerImpl(CertificateService certificateService,
                                        SignalService signalService,
                                        DomainService domainService,
                                        CertificateHelper certificateHelper,
                                        DomibusPropertyProvider domibusPropertyProvider,
                                        DomibusRawPropertyProvider domibusRawPropertyProvider,
                                        DomainContextProvider domainContextProvider,
                                        TLSMshCertificateManagerHelper tlsMshCertificateManagerHelper,
                                        TLSMshTrustManager tlsMshTrustManager,
                                        TLSMshTrustStorePersistenceInfoImpl tlsMshTruststorePersistenceInfo,
                                        TLSMshKeyStorePersistenceInfoImpl tlsMshKeystorePersistenceInfo) {
        this.certificateService = certificateService;
        this.signalService = signalService;
        this.domainService = domainService;
        this.certificateHelper = certificateHelper;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domibusRawPropertyProvider = domibusRawPropertyProvider;
        this.domainContextProvider = domainContextProvider;
        this.tlsMshCertificateManagerHelper = tlsMshCertificateManagerHelper;
        this.tlsMshTrustManager = tlsMshTrustManager;
        this.tlsMshTruststorePersistenceInfo = tlsMshTruststorePersistenceInfo;
        this.tlsMshKeystorePersistenceInfo = tlsMshKeystorePersistenceInfo;
    }

    @Override
    public synchronized void replaceTrustStore(KeyStoreContentInfo contentInfo) {
        certificateService.createOrReplaceStore(contentInfo, tlsMshTruststorePersistenceInfo);
        resetTLSTruststore();
    }

    @Override
    public List<TrustStoreEntry> getTrustStoreEntries() {
        try {
            final KeystorePersistenceInfo persistenceInfo = getPersistenceInfo();
            return certificateService.getStoreEntries(persistenceInfo);
        } catch (NoKeyStoreContentInformationException ex) {
            LOG.debug("The TLS MSH truststore is not configured.", ex);
            return new ArrayList<>();
        } catch (CryptoException ex) {
            throw new CryptoException("Error getting the TLS MSH truststore entries", ex);
        }
    }

    @Override
    public KeyStoreContentInfo getTruststoreContent() {
        return certificateService.getStoreContent(getPersistenceInfo());
    }

    @Override
    public synchronized boolean addCertificate(byte[] certificateData, String alias) {
        boolean added = certificateService.addCertificate(getPersistenceInfo(), certificateData, alias, true);
        if (added) {
            LOG.debug("Added certificate [{}] to the TLS MSH truststore; resetting it.", alias);
            resetTLSTruststore();
        }

        return added;
    }

    @Override
    public synchronized boolean removeCertificate(String alias) {
        boolean removed = certificateService.removeCertificate(getPersistenceInfo(), alias);
        if (removed) {
            LOG.debug("Removed certificate [{}] from the TLS MSH truststore; resetting it.", alias);
            resetTLSTruststore();
        }

        return removed;
    }

    @Override
    public void saveStoresFromDBToDisk() {
        LOG.info("Saving the TLS MSH truststore from the database to disk.");
        final List<Domain> domains = domainService.getDomains();
        persistStores(domains);
        LOG.info("The TLS MSH truststore was saved from the database to disk.");
    }

    @Override
    public KeystorePersistenceInfo getPersistenceInfo() {
        return tlsMshTruststorePersistenceInfo;
    }

    @Override
    public String getStoreFileExtension() {
        return certificateHelper.getStoreFileExtension(getPersistenceInfo().getType());
    }

    @Override
    public void onDomainAdded(final Domain domain) {
        persistStores(Arrays.asList(domain));
    }

    @Override
    public void onDomainRemoved(Domain domain) {
    }

    private void persistStores(List<Domain> domains) {
        certificateService.saveStoresFromDBToDisk(getPersistenceInfo(), domains);
    }

    @Override
    public void resetTLSTruststore() {
        Domain domain = domainContextProvider.getCurrentDomain();
        String domainCode = domain != null ? domain.getCode() : null;
        tlsMshCertificateManagerHelper.resetCache(domainCode);
        signalService.signalTLSTrustStoreUpdate(domain);
    }



    @Cacheable(cacheManager = DomibusCacheConstants.CACHE_MANAGER, value = TLS_CACHE, key = "#currentDomainCode")
    @Override
    public TLSClientParameters getTlsClientParameters(String currentDomainCode) {
        final TLSClientParameters tlsClientParameters = new TLSClientParameters();
        final Domain currentDomain = domainService.getDomain(currentDomainCode);
        KeyManager keyManager = null;
        final String tlsKeystoreFileLocation = tlsMshKeystorePersistenceInfo.getFileLocation();
        if (StringUtils.isNotBlank(tlsKeystoreFileLocation)) {
            LOG.debug("Getting the KeyManager from location [{}] for domain [{}].", tlsKeystoreFileLocation, currentDomainCode);
            keyManager = getKeyManager();
            if (keyManager != null) {
                LOG.debug("Adding the key manager on the TLS client parameters for domain [{}].", currentDomainCode);
                tlsClientParameters.setKeyManagers(new KeyManager[]{keyManager});
                String certAlias = domibusPropertyProvider.getProperty(currentDomain, DOMIBUS_SECURITY_TLS_PRIVATE_ALIAS);
                if (StringUtils.isNotBlank(certAlias)) {
                    LOG.debug("Setting certAlias to [{}] for domain [{}]", certAlias, currentDomainCode);
                    tlsClientParameters.setCertAlias(certAlias);
                }
            }
        }

        if (tlsMshTrustManager != null) {
            LOG.debug("Adding the trust manager on the TLS MSH client parameters for domain [{}].", currentDomainCode);
            tlsClientParameters.setTrustManagers(new TrustManager[]{tlsMshTrustManager});
        }

        if (keyManager == null && tlsMshTrustManager == null) {
            LOG.debug("No TLS MSH keyManager and trustManager found for domain [{}].", currentDomainCode);
            return null;
        }


        boolean disableCNCheck = domibusPropertyProvider.getBooleanProperty(currentDomain, DOMIBUS_SECURITY_TLS_DISABLE_CN_CHECK);
        LOG.debug("Setting disableCNCheck to [{}] for domain [{}].", disableCNCheck, currentDomainCode);
        tlsClientParameters.setDisableCNCheck(disableCNCheck);

        String secureSecureSocketProtocol = domibusPropertyProvider.getProperty(currentDomain, DOMIBUS_SECURITY_TLS_SECURE_SOCKET_PROTOCOL);
        LOG.debug("Setting secureSocketProtocol to [{}] for domain [{}].", secureSecureSocketProtocol, currentDomainCode);
        tlsClientParameters.setSecureSocketProtocol(secureSecureSocketProtocol);

        final List<String> includedCipherSuites = getIncludedCipherSuites();
        final List<String> excludedCipherSuites = getExcludedCipherSuites();
        if (CollectionUtils.isNotEmpty(includedCipherSuites) || CollectionUtils.isNotEmpty(excludedCipherSuites)) {
            FiltersType cipherSuitesFilterType = new FiltersType();

            if (CollectionUtils.isNotEmpty(excludedCipherSuites)) {
                LOG.debug("Setting includedCipherSuites to [{}] for domain [{}].", includedCipherSuites, currentDomainCode);

                cipherSuitesFilterType.getExclude().addAll(excludedCipherSuites);
            }
            if (CollectionUtils.isNotEmpty(includedCipherSuites)) {
                LOG.debug("Setting excludedCipherSuites to [{}] for domain [{}].", excludedCipherSuites, currentDomainCode);
                cipherSuitesFilterType.getInclude().addAll(includedCipherSuites);
            }
            tlsClientParameters.setCipherSuitesFilter(cipherSuitesFilterType);
        }
        return tlsClientParameters;
    }

    protected List<String> getIncludedCipherSuites() {
        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        String cipherSuitesList = domibusPropertyProvider.getProperty(currentDomain, DOMIBUS_SECURITY_TLS_CIPHER_SUITES_INCLUDED);
        return getChiperSuitesAsList(cipherSuitesList);
    }

    protected List<String> getExcludedCipherSuites() {
        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        String cipherSuitesList = domibusPropertyProvider.getProperty(currentDomain, DOMIBUS_SECURITY_TLS_CIPHER_SUITES_EXCLUDED);
        return getChiperSuitesAsList(cipherSuitesList);
    }

    protected List<String> getChiperSuitesAsList(String cipherSuitesList) {
        if (StringUtils.isBlank(cipherSuitesList)) {
            return null;
        }
        return Arrays.asList(cipherSuitesList.split("#"));
    }


    private KeyManager getKeyManager() {
        try {
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            KeyStore keyStore = certificateService.getStore(tlsMshKeystorePersistenceInfo);
            String keyPassword = tlsMshKeystorePersistenceInfo.getKeyEntryPassword();
            if (StringUtils.isBlank(keyPassword)) {
                keyPassword = tlsMshKeystorePersistenceInfo.getPassword();
            }
            keyManagerFactory.init(keyStore, keyPassword.toCharArray());
            KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
            return keyManagers.length > 0 ? keyManagers[0] : null;
        } catch (Exception e) {
            throw new ConfigurationException("Error getting the KeyManager", e);
        }
    }
}
