package eu.domibus.core.property.listeners;

import eu.domibus.api.cache.DomibusLocalCacheService;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.property.DomibusPropertyChangeListener;
import eu.domibus.core.crypto.DomainSecurityProfileProvider;
import eu.domibus.core.crypto.SecurityProfileProviderImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.domibus.core.ebms3.sender.client.DispatchClientDefaultProvider.*;

/**
 * @author Ion Perpegel
 * @since 4.1.1
 * <p>
 * Handles the change of dispatcher related properties
 */
@Service
public class DispatchClientChangeListener implements DomibusPropertyChangeListener {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DispatchClientChangeListener.class);

    @Autowired
    private DomibusLocalCacheService domibusLocalCacheService;

    @Autowired
    SecurityProfileProviderImpl securityProfileProvider;

    @Autowired
    DomainService domainService;

    @Override
    public boolean handlesProperty(String propertyName) {
        return StringUtils.equalsAnyIgnoreCase(propertyName,
                DOMIBUS_DISPATCHER_CONNECTIONTIMEOUT,
                DOMIBUS_DISPATCHER_RECEIVETIMEOUT,
                DOMIBUS_DISPATCHER_ALLOWCHUNKING,
                DOMIBUS_DISPATCHER_CHUNKINGTHRESHOLD)
                || isSecurityProfileProperty(propertyName);
    }

    @Override
    public void propertyValueChanged(String domainCode, String propertyName, String propertyValue) {
        LOG.info("Clearing the cache [{}] for domain [{}] because property [{}] was changed", DomibusLocalCacheService.DISPATCH_CLIENT, domainCode, propertyName);

        // when the property of a security profile is changed we need to clear the cache because the dispatcher client is cached; see eu.domibus.core.ebms3.sender.client.DispatchClientDefaultProvider.getClient
        this.domibusLocalCacheService.clearCache(DomibusLocalCacheService.DISPATCH_CLIENT);

        if(isSecurityProfileProperty(propertyName)) {
            LOG.info("Initializing domain SecurityProfileProvider for domain [{}]", domainCode);
            final Domain domain = domainService.getDomain(domainCode);
            final DomainSecurityProfileProvider domainSecurityProfileProvider = securityProfileProvider.getDomainSecurityProfileProvider(domain);
            domainSecurityProfileProvider.init();
        }
    }

    protected boolean isSecurityProfileProperty(String propertyName) {
        return StringUtils.contains(propertyName, "domibus.security.profile") // security profile property
                || StringUtils.contains(propertyName, "domibus.security.key") // change in alias
                || StringUtils.contains(propertyName, "domibus.security.signature") // default security profile property
                || StringUtils.contains(propertyName, "domibus.security.encryption"); // default security profile property
    }
}
