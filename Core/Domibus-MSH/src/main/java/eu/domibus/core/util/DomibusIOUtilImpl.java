package eu.domibus.core.util;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.util.DomibusIOUtil;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DomibusIOUtilImpl implements DomibusIOUtil {

    @Override
    public List<String> readLines(InputStream inputStream) {
        try (final InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             final BufferedReader reader = new BufferedReader(streamReader)) {
            return reader.lines().collect(Collectors.toList());
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not read input stream as lines", e);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not close input stream", e);
            }
        }
    }

    @Override
    public ByteArrayInputStream linesToInputStream(List<String> lines) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8))) {
            for (CharSequence line : lines) {
                writer.append(line);
                writer.newLine();
            }
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not create input stream from lines", e);
        }
        return new ByteArrayInputStream(out.toByteArray());
    }
}
