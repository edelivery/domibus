package eu.domibus.core.ebms3.sender.interceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessageConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.*;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusGeneralConstants.JSON_MAPPER_BEAN;

/**
 * Abstract Interceptor for Apache CXF Http headers
 *
 * @author Catalin Enache
 * @since 4.2
 */
public abstract class HttpHeaderAbstractInterceptor extends AbstractPhaseInterceptor<Message> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(HttpHeaderAbstractInterceptor.class);

    static final String USER_AGENT_HTTP_HEADER_KEY = "user-agent";
    static final String USER_AGENT_HTTP_HEADER_VALUE_APACHE_CXF = "Apache-CXF";
    private static final List<String> MESSAGE_PROPERTIES_TO_EXTRACT = Arrays.asList(
            Message.BASE_PATH,
            Message.QUERY_STRING,
            Message.ENCODING,
            Message.REQUEST_URL,
            Message.REQUEST_URI,
            Message.CONTENT_TYPE,
            Message.ACCEPT_CONTENT_TYPE,
            Message.PATH_INFO,
            Message.MTOM_ENABLED,
            Message.HTTP_REQUEST_METHOD);


    @Autowired
    protected DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    @Qualifier(JSON_MAPPER_BEAN)
    protected ObjectMapper objectMapper;

    public HttpHeaderAbstractInterceptor(String phase) {
        super(phase);
    }

    /**
     * It removes the user-agent header if contains Apache-CXF information
     *
     * @param message
     * @throws Fault
     */
    @Override
    public void handleMessage(Message message) throws Fault {
        //get the headers
        Map<String, List<String>> headers = (Map<String, List<String>>) message.get(Message.PROTOCOL_HEADERS);

        final Boolean httpHeaderMetadataActive = domibusPropertyProvider.getBooleanProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DISPATCHER_HTTP_HEADER_METADATA_ACTIVE);
        if (httpHeaderMetadataActive) {
            final String contentType = (String) message.get("Content-Type");
            LOG.putMDC(MessageConstants.HTTP_CONTENT_TYPE, contentType);

            //we need to put the value also on the message so that we can retrieve it later in the PropertyValueExchangeInterceptor and in the MSHWebservice
            message.put(MessageConstants.HTTP_CONTENT_TYPE, contentType);

            final String httpHeadersAsJson = httpHeadersToJson(headers);
            LOG.putMDC(MessageConstants.HTTP_PROTOCOL_HEADERS, httpHeadersAsJson);
            message.put(MessageConstants.HTTP_PROTOCOL_HEADERS, httpHeadersAsJson);

            final Map<String, Object> messageProperties = extractMessageProperties(MESSAGE_PROPERTIES_TO_EXTRACT, message);
            final String messagePropertiesToJson = messagePropertiesToJson(messageProperties);
            LOG.putMDC(MessageConstants.HTTP_MESSAGE_PROPERTIES, messagePropertiesToJson);
            message.put(MessageConstants.HTTP_MESSAGE_PROPERTIES, messagePropertiesToJson);
        }

        if (headers == null) {
            getLogger().debug("no http headers to intercept");
            return;
        }

        boolean removed = headers.entrySet()
                .removeIf(e -> USER_AGENT_HTTP_HEADER_KEY.equalsIgnoreCase(e.getKey())
                        && StringUtils.containsIgnoreCase(Arrays.deepToString(e.getValue().toArray()), USER_AGENT_HTTP_HEADER_VALUE_APACHE_CXF)
                );

        getLogger().debug("httpHeader=[{}] {}", USER_AGENT_HTTP_HEADER_KEY, (removed ? " was successfully removed" : " not present or value not removed"));

        //logging of the remaining headers
        getLogger().debug("httpHeaders are: {}", httpHeadersToString(headers));
    }

    protected abstract DomibusLogger getLogger();

    private String httpHeadersToString(Map<String, List<String>> headers) {
        return headers.keySet().stream()
                .map(key -> key + "=" + Arrays.deepToString(headers.get(key).toArray()))
                .collect(Collectors.joining(", ", "{", "}"));
    }

    protected String httpHeadersToJson(Map<String, List<String>> headers) {
        try {
            return objectMapper.writeValueAsString(headers);
        } catch (JsonProcessingException e) {
            //we catch the error; should not impact the message exchange
            LOG.error("Could not write HTTP headers as JSON", e);
            return null;
        }
    }

    protected String messagePropertiesToJson(Map<String, Object> properties) {
        try {
            return objectMapper.writeValueAsString(properties);
        } catch (JsonProcessingException e) {
            //we catch the error; should not impact the message exchange
            LOG.error("Could not write message properties as JSON", e);
            return null;
        }
    }

    protected Map<String, Object> extractMessageProperties(List<String> propertyNames, Message message) {
        Map<String, Object> result = new HashMap<>();

        final Set<String> contextualPropertyKeys = message.getContextualPropertyKeys();
        for (String contextualPropertyKey : contextualPropertyKeys) {
            if (propertyNames.contains(contextualPropertyKey)) {
                final Object propertyValue = message.get(contextualPropertyKey);
                result.put(contextualPropertyKey, propertyValue);
            }
        }
        return result;
    }
}
