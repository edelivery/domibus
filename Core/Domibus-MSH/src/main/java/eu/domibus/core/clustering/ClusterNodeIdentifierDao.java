package eu.domibus.core.clustering;

import eu.domibus.common.JPAConstants;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Ion Perpegel
 * @since 5.2
 */
@Repository
public class ClusterNodeIdentifierDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ClusterNodeIdentifierDao.class);

    @PersistenceContext(unitName = JPAConstants.PERSISTENCE_UNIT_NAME)
    protected EntityManager em;

    public ClusterNodeIdentifierEntity findByNodeName(String nodeName) {
        final TypedQuery<ClusterNodeIdentifierEntity> namedQuery = em.createNamedQuery("ClusterNodeIdentifierEntity.findByNodeName",
                ClusterNodeIdentifierEntity.class);
        namedQuery.setParameter("NODE_NAME", nodeName);
        return namedQuery.getResultList().stream().findFirst().orElse(null);
    }

    @Transactional
    public void create(final ClusterNodeIdentifierEntity entity) {
        em.persist(entity);
        em.flush();
    }

    public int getMaxId() {
        final TypedQuery<ClusterNodeIdentifierEntity> namedQuery = em.createNamedQuery("ClusterNodeIdentifierEntity.getMaxId",
                ClusterNodeIdentifierEntity.class);
        ClusterNodeIdentifierEntity res = namedQuery.getResultList().stream().findFirst().orElse(null);
        if (res == null) {
            LOG.debug("No values found for node id in the db. Allocating value: 0");
            return 0;
        }
        return res.getNodeId();
    }

    public void detach(ClusterNodeIdentifierEntity entity) {
        LOG.debug("Detaching entity [{}]", entity);
        em.detach(entity);
    }

    public List<Integer> getAllIds() {
        final TypedQuery<Integer> namedQuery = em.createNamedQuery("ClusterNodeIdentifierEntity.getAllIds", Integer.class);
        return namedQuery.getResultList();
    }
}
