
package eu.domibus.core.ebms3.ws.policy;

import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import org.springframework.stereotype.Service;

/**
 * @author Arun Raj
 * @since 3.3
 * <p>
 * JIRA: EDELIVERY-6671 showed the {@link PolicyServiceImpl} has a runtime dependency to the bean algorithmSuiteLoader
 */
@Service
public class PolicyServiceImpl implements PolicyService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PolicyServiceImpl.class);

    @Override
    public boolean isSecurityPolicySet(SecurityProfileConfiguration securityProfileConfiguration) {
        return !isNoSecurityPolicy(securityProfileConfiguration);
    }

    /**
     * Checks whether the security policy specified has signature or encryption active.
     * A no security policy would be used to avoid certificate validation.
     *
     * @param securityProfileConfiguration the security policy
     * @return boolean true if no signature and no encryption is used
     */
    @Override
    public boolean isNoSecurityPolicy(SecurityProfileConfiguration securityProfileConfiguration) {
        if (!securityProfileConfiguration.isSignatureEnabled() && !securityProfileConfiguration.isEncryptionEnabled()) {
            LOG.securityWarn(DomibusMessageCode.SEC_NO_SECURITY_POLICY_USED, "Signature and encryption are disabled! No signature and no encryption will be used!");
            return true;
        }
        return false;
    }

    @Override
    public boolean isNoEncryptionPolicy(SecurityProfileConfiguration securityProfileConfiguration) {
        if (!securityProfileConfiguration.isEncryptionEnabled()) {
            LOG.securityWarn(DomibusMessageCode.SEC_NO_SECURITY_POLICY_USED, "Encryption is not enabled");
            return true;
        }
        return false;
    }
}
