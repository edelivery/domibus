package eu.domibus.core.message.dictionary;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.multitenancy.ExecutorWaitPolicy;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Ion Perpegel
 * @since 5.0
 */
@Service
public class StaticDictionaryServiceImpl implements StaticDictionaryService {

    public static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(StaticDictionaryServiceImpl.class);

    protected StaticDictionaryServiceHelper staticDictionaryServiceHelper;
    protected DomibusConfigurationService domibusConfigurationService;
    protected DomibusTaskExecutor domainTaskExecutor;
    protected DomainService domainService;

    public StaticDictionaryServiceImpl(StaticDictionaryServiceHelper staticDictionaryServiceHelper, DomibusConfigurationService domibusConfigurationService, DomibusTaskExecutor domainTaskExecutor, DomainService domainService) {
        this.staticDictionaryServiceHelper = staticDictionaryServiceHelper;
        this.domibusConfigurationService = domibusConfigurationService;
        this.domainTaskExecutor = domainTaskExecutor;
        this.domainService = domainService;
    }

    public void createStaticDictionaryEntries() {
        LOG.info("Start checking and creating static dictionary entries if missing");

        Runnable createEntriesCall = createEntriesCall();

        if (domibusConfigurationService.isSingleTenantAware()) {
            LOG.info("Start checking and creating static dictionary entries in single tenancy mode");
            createEntriesCall.run();
            LOG.info("Finished checking and creating static dictionary entries in single tenancy mode");
            return;
        }

        LOG.info("Start checking and creating static dictionary entries in multi tenancy mode");
        final List<Domain> domains = domainService.getDomains();
        createEntries(domains);
        LOG.info("Finished checking and creating static dictionary entries in multi tenancy mode");
    }

    @Override
    public void onDomainAdded(final Domain domain) {
        createEntries(domain);
    }

    @Override
    public void onDomainRemoved(Domain domain) {
        // I'd say nothing to do here
    }

    private void createEntries(Domain domain) {
        createEntries(Arrays.asList(domain));
    }

    private void createEntries(List<Domain> domains) {
        Runnable createEntriesCall = createEntriesCall();
        for (Domain domain : domains) {
            LOG.info("Start checking and creating static dictionary entries for domain [{}]", domain);
            domainTaskExecutor.submit(createEntriesCall, domain, ExecutorWaitPolicy.WAIT, 3L, TimeUnit.MINUTES);
            LOG.info("Finished checking and creating static dictionary entries for domain [{}]", domain);
        }
    }

    private Runnable createEntriesCall() {
        return () -> {
            try {
                staticDictionaryServiceHelper.createEntries();
            } catch (Exception e) {
                LOG.error("Error while creating static dictionary entries", e);
            }
        };
    }

}
