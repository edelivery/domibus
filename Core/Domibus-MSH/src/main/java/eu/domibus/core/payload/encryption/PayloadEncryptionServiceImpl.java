package eu.domibus.core.payload.encryption;

import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.payload.encryption.PayloadEncryptionService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.util.EncryptionUtil;
import eu.domibus.core.encryption.EncryptionKeyDao;
import eu.domibus.core.encryption.EncryptionKeyEntity;
import eu.domibus.core.encryption.EncryptionUsage;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import java.util.List;

/**
 * @author Cosmin Baciu
 * @since 4.1.1
 */
@Service("EncryptionServiceImpl")
public class PayloadEncryptionServiceImpl implements PayloadEncryptionService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PayloadEncryptionServiceImpl.class);

    protected final EncryptionKeyDao encryptionKeyDao;

    protected final DomainContextProvider domainContextProvider;

    protected final EncryptionUtil encryptionUtil;

    protected final DomainService domainService;

    protected final DomibusTaskExecutor domainTaskExecutor;

    protected final DomibusConfigurationService domibusConfigurationService;

    public PayloadEncryptionServiceImpl(EncryptionKeyDao encryptionKeyDao,
                                        DomainContextProvider domainContextProvider,
                                        EncryptionUtil encryptionUtil,
                                        DomainService domainService,
                                        DomibusTaskExecutor domainTaskExecutor,
                                        DomibusConfigurationService domibusConfigurationService) {
        this.encryptionKeyDao = encryptionKeyDao;
        this.domainContextProvider = domainContextProvider;
        this.encryptionUtil = encryptionUtil;
        this.domainService = domainService;
        this.domainTaskExecutor = domainTaskExecutor;
        this.domibusConfigurationService = domibusConfigurationService;
    }

    @Override
    public void createPayloadEncryptionKeyForAllDomainsIfNotExists() {
        final List<Domain> domains = domainService.getDomains();

        createPayloadEncryptionKeyForAllDomainsIfNotExists(domains);
    }

    @Override
    public void onDomainAdded(final Domain domain) {
        createPayloadEncryptionKeyIfNotExists(domain);
    }

    @Override
    public void onDomainRemoved(Domain domain) {
        removePayloadEncryptionKey(domain);
    }

    private void createPayloadEncryptionKeyForAllDomainsIfNotExists(List<Domain> domains) {
        LOG.info("Creating encryption key for all domains if not yet exists");

        for (Domain domain : domains) {
            createPayloadEncryptionKeyIfNotExists(domain);
        }

        LOG.info("Finished creating encryption key for all domains if not yet exists");
    }

    @Override
    public void createPayloadEncryptionKeyIfNotExists(Domain domain) {
        final boolean encryptionActive = BooleanUtils.isTrue(domibusConfigurationService.isPayloadEncryptionActive(domain));
        if (encryptionActive) {
            domainTaskExecutor.submit(this::createPayloadEncryptionKey, domain);
        } else {
            LOG.debug("Payload encryption is not activated for domain [{}]", domain);
        }
    }

    protected void createPayloadEncryptionKey() {
        LOG.debug("Checking if the encryption key should be created");

        final EncryptionKeyEntity payloadKey = encryptionKeyDao.findByUsage(EncryptionUsage.PAYLOAD);
        if (payloadKey != null) {
            LOG.debug("Payload encryption key already exists");
            return;
        }

        LOG.info("Creating payload encryption key");

        final SecretKey secretKey = encryptionUtil.generateSecretKey();
        final byte[] iv = encryptionUtil.generateIV();
        final EncryptionKeyEntity encryptionKeyEntity = new EncryptionKeyEntity();
        encryptionKeyEntity.setSecretKey(secretKey.getEncoded());
        encryptionKeyEntity.setInitVector(iv);
        encryptionKeyEntity.setUsage(EncryptionUsage.PAYLOAD);

        encryptionKeyDao.create(encryptionKeyEntity);

        LOG.info("Finished creating payload encryption key");
    }

    protected void removePayloadEncryptionKey(Domain domain) {
        domainTaskExecutor.submit(() -> doRemovePayloadEncryptionKey(domain), domain);
    }

    private void doRemovePayloadEncryptionKey(Domain domain) {
        final EncryptionKeyEntity payloadKey = encryptionKeyDao.findByUsage(EncryptionUsage.PAYLOAD);
        if (payloadKey == null) {
            LOG.debug("Payload encryption key for domain [{}] does not exist.", domain);
            return;
        }
        encryptionKeyDao.delete(payloadKey);
    }

    @Override
    public Cipher getEncryptCipherForPayload() {
        LOG.debug("Getting the encrypt cipher for payload");

        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        final EncryptionKeyEntity encryptionKeyEntity = encryptionKeyDao.findByUsageCacheable(currentDomain.getCode(), EncryptionUsage.PAYLOAD);
        final SecretKey secretKey = encryptionUtil.getSecretKey(encryptionKeyEntity.getSecretKey());
        final GCMParameterSpec secretKeySpec = encryptionUtil.getSecretKeySpec(encryptionKeyEntity.getInitVector());
        final Cipher encryptCipher = encryptionUtil.getEncryptCipher(secretKey, secretKeySpec);

        LOG.debug("Finished getting the encrypt cipher for payload");

        return encryptCipher;
    }

    @Override
    public Cipher getDecryptCipherForPayload() {
        LOG.debug("Getting the decrypt cipher for payload");

        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        final EncryptionKeyEntity encryptionKeyEntity = encryptionKeyDao.findByUsageCacheable(currentDomain.getCode(), EncryptionUsage.PAYLOAD);
        final SecretKey secretKey = encryptionUtil.getSecretKey(encryptionKeyEntity.getSecretKey());
        final GCMParameterSpec secretKeySpec = encryptionUtil.getSecretKeySpec(encryptionKeyEntity.getInitVector());
        final Cipher decryptCipher = encryptionUtil.getDecryptCipher(secretKey, secretKeySpec);

        LOG.debug("Finished getting the decrypt cipher for payload");

        return decryptCipher;
    }


    @Override
    public boolean isPayloadEncryptionActive(UserMessage userMessage) {
        //EDELIVERY-4749 - SplitAndJoin limitation
        final boolean isPayloadEncryptionActive = !userMessage.isSplitAndJoin() && domibusConfigurationService.isPayloadEncryptionActive(domainContextProvider.getCurrentDomain());
        LOG.debug("Is payload encryption active? [{}]", isPayloadEncryptionActive);
        return isPayloadEncryptionActive;
    }
}
