package eu.domibus.core.property.listeners;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.property.DomibusPropertyChangeListener;
import eu.domibus.core.payload.persistence.DomibusPayloadManagerSpiProviderImpl;
import eu.domibus.core.spi.payload.DomibusPayloadManagerSpi;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_ATTACHMENT_STORAGE_LOCATION;

/**
 * @author Ion Perpegel
 * @since 4.1.1
 * <p>
 * Handles the change of attachment storage location property
 */
@Service
public class StorageChangeListener implements DomibusPropertyChangeListener {

    @Autowired
    protected DomainService domainService;

    @Autowired
    DomibusPayloadManagerSpiProviderImpl domibusPayloadManagerSpiProvider;

    @Override
    public boolean handlesProperty(String propertyName) {
        return StringUtils.equalsIgnoreCase(propertyName, DOMIBUS_ATTACHMENT_STORAGE_LOCATION);
    }

    @Override
    public void propertyValueChanged(String domainCode, String propertyName, String propertyValue) {
        final Domain domain = domainService.getDomain(domainCode);

        final DomibusPayloadManagerSpi domibusPayloadManagerSpi = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpi(domain);
        domibusPayloadManagerSpi.initialize(domainCode);


    }
}
