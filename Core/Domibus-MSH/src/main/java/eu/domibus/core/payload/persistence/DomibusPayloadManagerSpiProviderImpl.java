package eu.domibus.core.payload.persistence;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.payload.DomibusPayloadException;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.spi.payload.DomibusPayloadManagerSpi;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_ATTACHMENT_STORAGE_LOCATION;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_EXTENSION_PAYLOAD_PERSISTENCE_IDENTIFIER;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */

@Service
public class DomibusPayloadManagerSpiProviderImpl {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusPayloadManagerSpiProviderImpl.class);

    protected DomibusPropertyProvider domibusPropertyProvider;

    protected DomainContextProvider domainContextProvider;

    private List<DomibusPayloadManagerSpi> domibusPayloadManagerSpis;

    private FileSystemPayloadManagerSpi fileSystemPayloadManagerSpi;

    private DatabasePayloadManagerSpi databasePayloadManagerSpi;

    public DomibusPayloadManagerSpiProviderImpl(DomibusPropertyProvider domibusPropertyProvider, DomainContextProvider domainContextProvider, List<DomibusPayloadManagerSpi> domibusPayloadManagerSpis, FileSystemPayloadManagerSpi fileSystemPayloadManagerSpi, DatabasePayloadManagerSpi databasePayloadManagerSpi) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domainContextProvider = domainContextProvider;
        this.domibusPayloadManagerSpis = domibusPayloadManagerSpis;
        this.fileSystemPayloadManagerSpi = fileSystemPayloadManagerSpi;
        this.databasePayloadManagerSpi = databasePayloadManagerSpi;
    }

    public DomibusPayloadManagerSpi getDomibusPayloadManagerSpi(String identifier) {
        final List<DomibusPayloadManagerSpi> domibusPayloadManagerSpiList = this.domibusPayloadManagerSpis.stream()
                .filter(domibusPayloadManagerSpi -> StringUtils.equalsIgnoreCase(identifier, domibusPayloadManagerSpi.getIdentifier()))
                .collect(Collectors.toList());

        if (domibusPayloadManagerSpiList.size() > 1) {
            throw new DomibusPayloadException(String.format("More than one payload SPI provider for identifier:[%s]", identifier));
        }
        if (domibusPayloadManagerSpiList.isEmpty()) {
            throw new DomibusPayloadException(String.format("No payload SPI provider found for given identifier:[%s]", identifier));
        }
        return domibusPayloadManagerSpiList.get(0);
    }

    public DomibusPayloadManagerSpi getDomibusPayloadManagerSpi(Domain domain) {
        final String domainPayloadPersistenceIdentifier = domibusPropertyProvider.getProperty(domain, DOMIBUS_EXTENSION_PAYLOAD_PERSISTENCE_IDENTIFIER);

        //check if we need to use database or file system persistence
        if (StringUtils.isBlank(domainPayloadPersistenceIdentifier)) {
            LOG.debug("Property [{}] is not configured for domain [{}]. Checking if property [{}] is configured", DOMIBUS_EXTENSION_PAYLOAD_PERSISTENCE_IDENTIFIER, domain, DOMIBUS_ATTACHMENT_STORAGE_LOCATION);

            final String attachmentLocation = domibusPropertyProvider.getProperty(domain, DOMIBUS_ATTACHMENT_STORAGE_LOCATION);
            if (StringUtils.isNotBlank(attachmentLocation)) {
                LOG.debug("Property [{}] is configured. Using file system provider for domain [{}]", DOMIBUS_ATTACHMENT_STORAGE_LOCATION, domain);
                return fileSystemPayloadManagerSpi;
            } else {
                LOG.debug("Property [{}] is not configured. Using database provider for domain [{}]", DOMIBUS_ATTACHMENT_STORAGE_LOCATION, domain);
                return databasePayloadManagerSpi;
            }
        }

        LOG.debug("Checking for the DomibusPayloadManagerSpi using identifier [{}] for domain [{}]", domainPayloadPersistenceIdentifier, domain);

        final DomibusPayloadManagerSpi domibusPayloadManagerSpi = getDomibusPayloadManagerSpi(domainPayloadPersistenceIdentifier);
        LOG.debug("Found payload manager SPI [{}] for domain [{}]", domainPayloadPersistenceIdentifier, domain);

        return domibusPayloadManagerSpi;
    }

    public DomibusPayloadManagerSpi getDomibusPayloadManagerSpiForCurrentDomain() {
        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        return getDomibusPayloadManagerSpi(currentDomain);
    }
}
