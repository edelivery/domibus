package eu.domibus.core.configuration;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.configuration.spi.DomibusConfigurationManagerSpi;
import eu.domibus.configuration.spi.DomibusResource;
import eu.domibus.configuration.spi.SearchStrategy;
import eu.domibus.core.property.DomibusConfigLocationProvider;
import eu.domibus.core.util.backup.BackupFileServiceImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static eu.domibus.core.property.DomibusPropertyConfiguration.MULTITENANT_DOMIBUS_PROPERTIES_SUFFIX;

public class DomibusConfigurationProviderFileSystem implements DomibusConfigurationManagerSpi {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusConfigurationProviderFileSystem.class);

    protected String domibusConfigLocation;
    protected PathMatchingResourcePatternResolver resolver;
    protected BackupFileServiceImpl backupFileService;

    @Override
    public String getName() {
        return "FileSystem";
    }

    @Override
    public void initialize() {
        String domibusConfigLocationValue = DomibusConfigLocationProvider.getDomibusConfigLocation();
        String normalizedDomibusConfigLocation = Paths.get(domibusConfigLocationValue).normalize().toString();
        LOG.info("Configured property [{}] with value [{}]", DomibusPropertyMetadataManagerSPI.DOMIBUS_CONFIG_LOCATION, normalizedDomibusConfigLocation);
        domibusConfigLocation = normalizedDomibusConfigLocation;

        resolver = new PathMatchingResourcePatternResolver();
        backupFileService = new BackupFileServiceImpl();
    }

    @Override
    public DomibusResource getResource(String location, String description) {
        final String absoluteLocationWithProtocol = getFileLocationWithProtocol(location);
        LOG.info("Getting Domibus resource for [{}]", absoluteLocationWithProtocol);

        final String absoluteLocation = getAbsoluteLocation(location);
        final File file = new File(absoluteLocation);

        Resource fileResource = resolver.getResource(absoluteLocationWithProtocol);
        try {
            final InputStream inputStream = fileResource.getInputStream();
            return new DomibusResource(file.getName(), description, absoluteLocation, file.getParentFile().getAbsolutePath(), inputStream, Instant.ofEpochMilli(file.lastModified()));
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not get the contents for file [" + absoluteLocation + "]", e);
        }
    }

    @Override
    public void deleteResource(DomibusResource domibusResource) {
        final String location = domibusResource.getLocation();
        final String absoluteLocation = getFileLocation(location);
        try {
            FileUtils.delete(new File(absoluteLocation));
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not delete file [" + absoluteLocation + "]", e);
        }
    }

    @Override
    public void storeResource(DomibusResource domibusResource) {
        final String location = domibusResource.getLocation();
        final String absoluteLocation = getFileLocation(location);
        try (final InputStream inputStream = domibusResource.getInputStream()) {
            Files.write(Paths.get(absoluteLocation), IOUtils.toByteArray(inputStream));
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not write file to [" + absoluteLocation + "]", e);
        }
    }

    @Override
    public void backupResource(DomibusResource domibusResource, String backupLocationValue) {
        final String originalFileLocation = getFileLocation(domibusResource.getLocation());
        File originalFile = new File(originalFileLocation);
        File parentFile = originalFile.getParentFile();
        if (parentFile == null) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not get parent file of [" + originalFileLocation + "]; no backing up.");

        }
        String backupLocation = Paths.get(parentFile.getPath(), backupLocationValue).toString();
        try {
            backupFileService.backupFileInLocation(originalFile, backupLocation);
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not backup file [" + originalFile + "] to [" + backupLocation + "]", e);
        }
    }

    protected String getFileLocation(String file) {
        if (StringUtils.startsWith(file, File.separator)) {
            return file;
        }
        String absoluteLocation = getAbsoluteLocation(file);
        LOG.debug("Using the absolute location for [{}]:[{}]", file, absoluteLocation);
        return absoluteLocation;
    }

    protected String getFileLocationWithProtocol(String file) {
        if (StringUtils.startsWith(file, "file:")) {
            LOG.trace("Getting the original file [{}] as it starts with file", file);
            return file;
        }
        String absoluteLocation = getAbsoluteLocationWithProtocol(file);
        LOG.debug("Using the absolute location for [{}]:[{}]", file, absoluteLocation);
        return absoluteLocation;
    }

    protected String getAbsoluteLocationWithProtocol(String file) {
        String fileLocation = file;
        if (!StringUtils.startsWith(fileLocation, File.separator)) {
            fileLocation = getAbsoluteLocation(file);
        }

        return "file:///" + fileLocation;
    }

    protected String getAbsoluteLocation(String file) {
        String result = file;

        if (!new File(file).isAbsolute()) {
            result = domibusConfigLocation + "/" + file;
            LOG.trace("File [{}] is relative, using absolute location [{}]", file, result);
        }
        return result;
    }

    @Override
    public List<String> getDomains() {
        File confDirectory = new File(domibusConfigLocation + File.separator + DomainService.DOMAINS_HOME);
        final File[] domainHomes = confDirectory.listFiles(File::isDirectory);
        if (domainHomes == null) {
            LOG.warn("Invalid domains path: [{}]", confDirectory);
            return null;
        }
        LOG.debug("Domain directories identified:[{}]", Arrays.stream(domainHomes).map(File::getName).collect(Collectors.toList()));
        //filter the domain home directories that contain files with name '-domibus.properties' only
        List<String> filteredDomainHomes = Arrays.stream(domainHomes)
                .filter(domainDir -> {
                    File[] files = domainDir.listFiles((FilenameFilter) FileFilterUtils.suffixFileFilter(MULTITENANT_DOMIBUS_PROPERTIES_SUFFIX, IOCase.INSENSITIVE));
                    return files != null && files.length > 0;
                })
                .map(File::getName)
                .sorted()
                .collect(Collectors.toList());
        LOG.debug("Filtered Domain Homes with property files: [{}]", filteredDomainHomes);
        return filteredDomainHomes;
    }

    @Override
    public List<DomibusResource> findFiles(String parent,
                                           String pattern,
                                           SearchStrategy searchStrategy,
                                           List<String> excludeIfContaining,
                                           String description) {
        List<DomibusResource> result = new ArrayList<>();
        Resource[] resources = null;

        final String absoluteLocationWithProtocol = getAbsoluteLocationWithProtocol(parent);

        try {
            final String filePattern = getFilePattern(searchStrategy, pattern);
            LOG.debug("Searching in [{}] using pattern [{}]", absoluteLocationWithProtocol, filePattern);
            resources = resolver.getResources(absoluteLocationWithProtocol + filePattern);
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not find configuration files for parent [" + parent + "] and pattern [" + pattern + "]", e);
        }

        for (Resource domainProperty : resources) {
            try {
                final File file = domainProperty.getFile();
                final String absolutePath = file.getAbsolutePath();
                if (!containsPattern(absolutePath, excludeIfContaining)) {
                    result.add(new DomibusResource(domainProperty.getFilename(), description, absolutePath, file.getParentFile().getAbsolutePath(), domainProperty.getInputStream(), Instant.ofEpochMilli(file.lastModified())));
                } else {
                    LOG.debug("File [{}] was excluded from matching", absolutePath);
                }
            } catch (IOException e) {
                throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not get the configuration resource for [" + domainProperty + "]", e);
            }
        }
        LOG.debug("Found files for parent [{}], pattern [{}], exclusions [{}]: [{}]", parent, pattern, excludeIfContaining, result);

        return result;
    }

    protected String getFilePattern(SearchStrategy searchStrategy, String searchTerm) {
        switch (searchStrategy) {
            case RECURSIVE_ENDING_WITH_PATTERN:
                return "/**/*" + searchTerm;
            case CURRENT_LOCATION_ENDING_WITH_PATTERN:
                return "/*" + searchTerm;
            case RECURSIVE_CONTAINING_PATTERN:
                return "/**/*" + searchTerm + "*";
            case CURRENT_LOCATION_CONTAINING_PATTERN:
                return "/*" + searchTerm + "*";
            default:
                throw new DomibusCoreException("Unrecognized search strategy " + searchStrategy);
        }
    }

    protected boolean containsPattern(String absolutePath, List<String> patterns) {
        final String matched = patterns.stream()
                .filter(value -> StringUtils.contains(absolutePath, value))
                .findFirst()
                .orElse(null);
        return matched != null;
    }
}
