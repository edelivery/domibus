package eu.domibus.core.clustering;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Ion Perpegel
 * @since 5.2
 */
@Entity
@Table(name = "TB_CLUSTER_NODE_ID")
@NamedQueries({
        @NamedQuery(name = "ClusterNodeIdentifierEntity.getAllIds", query = "SELECT c.nodeId FROM ClusterNodeIdentifierEntity c"),
        @NamedQuery(name = "ClusterNodeIdentifierEntity.findByNodeName", query = "SELECT c FROM ClusterNodeIdentifierEntity c WHERE c.nodeName = :NODE_NAME"),
        @NamedQuery(name = "ClusterNodeIdentifierEntity.getMaxId", query = "SELECT c FROM ClusterNodeIdentifierEntity c WHERE c.nodeId = (SELECT MAX(c.nodeId) FROM ClusterNodeIdentifierEntity c)"),
})
public class ClusterNodeIdentifierEntity {

    @Id
    @NotNull
    @Column(name = "NODE_NAME")
    private String nodeName;

    @NotNull
    @Column(name = "NODE_ID")
    private Integer nodeId;

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ClusterNodeIdentifierEntity lock = (ClusterNodeIdentifierEntity) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(nodeName, lock.nodeName)
                .append(nodeId, lock.nodeId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(nodeName)
                .append(nodeId)
                .toHashCode();
    }
}
