package eu.domibus.core.message;

import eu.domibus.api.datasource.AutoCloseDataSource;
import eu.domibus.api.encryption.DecryptDataSource;
import eu.domibus.api.message.compression.DecompressionDataSource;
import eu.domibus.api.model.*;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.payload.PartInfoService;
import eu.domibus.api.payload.encryption.PayloadEncryptionService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.spring.SpringContextProvider;
import eu.domibus.api.util.TsidUtil;
import eu.domibus.common.ErrorCode;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.payload.persistence.DomibusPayloadManagerSpiProviderImpl;
import eu.domibus.core.payload.persistence.PayloadPersistenceHelper;
import eu.domibus.core.spi.payload.DeleteFolderResult;
import eu.domibus.core.spi.payload.DomibusPayloadManagerSpi;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import eu.domibus.logging.MDCKey;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.crypto.Cipher;
import javax.mail.util.ByteArrayDataSource;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_DISPATCHER_SPLIT_AND_JOIN_PAYLOADS_SCHEDULE_THRESHOLD;

/**
 * @author François Gautier
 * @since 5.0
 */
@Service
public class PartInfoServiceImpl implements PartInfoService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PartInfoServiceImpl.class);

    public static final String PROPERTY_PAYLOADS_SCHEDULE_THRESHOLD = DOMIBUS_DISPATCHER_SPLIT_AND_JOIN_PAYLOADS_SCHEDULE_THRESHOLD;

    protected static final Long BYTES_IN_MB = 1048576L; // 1024 * 1024

    private final PartInfoDao partInfoDao;

    private final DomainContextProvider domainContextProvider;

    private final DomibusPropertyProvider domibusPropertyProvider;

    private final DomibusPayloadManagerSpiProviderImpl domibusPayloadManagerSpiProvider;

    protected final PayloadPersistenceHelper payloadPersistenceHelper;
    private final TsidUtil tsidUtil;

    public PartInfoServiceImpl(
            PartInfoDao partInfoDao,
            DomainContextProvider domainContextProvider,
            DomibusPropertyProvider domibusPropertyProvider,
            DomibusPayloadManagerSpiProviderImpl domibusPayloadManagerSpiProvider,
            PayloadPersistenceHelper payloadPersistenceHelper,
            TsidUtil tsidUtil) {
        this.partInfoDao = partInfoDao;
        this.domainContextProvider = domainContextProvider;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domibusPayloadManagerSpiProvider = domibusPayloadManagerSpiProvider;
        this.payloadPersistenceHelper = payloadPersistenceHelper;
        this.tsidUtil = tsidUtil;
    }

    @Override
    public void create(PartInfo partInfo, UserMessage userMessage) {
        partInfo.setUserMessage(userMessage);
        partInfoDao.create(partInfo);
    }

    @Override
    @Transactional
    public List<PartInfo> findPartInfo(UserMessage userMessage) {
        return partInfoDao.findPartInfoByUserMessageEntityId(userMessage.getEntityId());
    }

    @Override
    @Transactional(readOnly = true)
    public PartInfo findPartInfo(Long messageEntityId, String cid) {
        return partInfoDao.findPartInfoByUserMessageEntityIdAndCid(messageEntityId, getCid(cid));
    }

    protected String getCid(String cid) {
        if (StringUtils.startsWith(cid, "cid:")) {
            return cid;
        }
        return "cid:" + cid;
    }

    @Override
    @Transactional
    public List<PartInfo> findPartInfo(long entityId) {
        return partInfoDao.findPartInfoByUserMessageEntityId(entityId);
    }

    @Override
    @Transactional(readOnly = true)
    public Long findPartInfoTotalLength(long entityId) {
        List<Long> list = partInfoDao.findPartInfoLengthByUserMessageEntityId(entityId);
        return list.stream().filter(Objects::nonNull).mapToLong(Long::longValue).sum();
    }

    @Transactional(propagation = Propagation.MANDATORY)
    @MDCKey({DomibusLogger.MDC_MESSAGE_ID, DomibusLogger.MDC_MESSAGE_ROLE, DomibusLogger.MDC_MESSAGE_ENTITY_ID})
    public void clearPayloadData(long entityId) {
        LOG.debug("Start clearing payloadData");

        List<PartInfo> partInfos = partInfoDao.findPartInfoByUserMessageEntityId(entityId);

        if (CollectionUtils.isEmpty(partInfos)) {
            LOG.debug("No payloads to clear");
            return;
        }
        clearDatabasePayloads(partInfos);
        clearFileSystemPayloads(partInfos);

        LOG.businessInfo(DomibusMessageCode.BUS_MESSAGE_PAYLOAD_DATA_CLEARED);
    }

    @Override
    public void clearFileSystemPayloads(final List<PartInfo> partInfos) {
        List<PartInfo> fileSystemPayloads = getFileSystemPayloads(partInfos);
        if (CollectionUtils.isEmpty(fileSystemPayloads)) {
            LOG.debug("No file system payloads to clear");
            return;
        }

        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        final String domainCode = currentDomain.getCode();

        for (PartInfo result : fileSystemPayloads) {
            final String fileNameToBeDeleted = result.getFileName();
            deletePayload(result.getPayloadManagerSPI(), fileNameToBeDeleted, domainCode);
        }
    }

    private void deletePayload(String payloadManagerSPIIdentifier, String fileNameToBeDeleted, String domainCode) {
        if (StringUtils.isBlank(payloadManagerSPIIdentifier)) {
            LOG.error("Could not delete payload [{}]: using payload manager is null", fileNameToBeDeleted);
            return;
        }

        LOG.debug("Getting the payload SPI manager with identifier [{}] for payload [{}]", payloadManagerSPIIdentifier, fileNameToBeDeleted);
        DomibusPayloadManagerSpi domibusPayloadManagerSpiForCurrentDomain = null;
        try {
            domibusPayloadManagerSpiForCurrentDomain = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpi(payloadManagerSPIIdentifier);
        } catch (Exception e) {
            LOG.error("Could not delete payload [{}] using payload manager identifier [{}]", fileNameToBeDeleted, payloadManagerSPIIdentifier, e);
            return;
        }

        LOG.debug("Deleting payload [{}] using provider [{}]", fileNameToBeDeleted, domibusPayloadManagerSpiForCurrentDomain.getIdentifier());

        try {
            domibusPayloadManagerSpiForCurrentDomain.deletePayload(domainCode, fileNameToBeDeleted);
        } catch (Exception e) {
            LOG.error("Could not delete payload [{}] using payload manager identifier [{}] and class [{}]", fileNameToBeDeleted, payloadManagerSPIIdentifier, domibusPayloadManagerSpiForCurrentDomain.getClass(), e);
        }
    }

    protected List<PartInfo> getFileSystemPayloads(final List<PartInfo> partInfos) {
        Predicate<PartInfo> filenamePresentPredicate = getFilenameEmptyPredicate().negate();
        return getPayloads(partInfos, filenamePresentPredicate);
    }

    protected List<PartInfo> getPayloads(final List<PartInfo> partInfos, Predicate<PartInfo> partInfoPredicate) {
        return partInfos.stream().filter(partInfoPredicate).collect(Collectors.toList());
    }

    protected Predicate<PartInfo> getFilenameEmptyPredicate() {
        return partInfo -> StringUtils.isBlank(partInfo.getFileName());
    }

    @Override
    public void deletePayloadFiles(List<PartInfoFileNameSpiDto> filenames) {
        if (CollectionUtils.isEmpty(filenames)) {
            LOG.debug("No payload data file to delete from the filesystem");
            return;
        }


        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        final String domainCode = currentDomain.getCode();


        LOG.debug("There are [{}] payloads on filesystem to delete: [{}] ", filenames.size(), filenames);
        for (PartInfoFileNameSpiDto partInfoFileNameSpiDto : filenames) {
            final DomibusPayloadManagerSpi domibusPayloadManagerSpiForCurrentDomain = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpi(partInfoFileNameSpiDto.getPayloadSpiIdentifier());
            LOG.debug("Deleting payload [{}] using provider [{}]", partInfoFileNameSpiDto.getFileName(), domibusPayloadManagerSpiForCurrentDomain.getIdentifier());
            domibusPayloadManagerSpiForCurrentDomain.deletePayload(domainCode, partInfoFileNameSpiDto.getFileName());
        }
    }

    protected void clearDatabasePayloads(final List<PartInfo> partInfos) {
        List<PartInfo> databasePayloads = getDatabasePayloads(partInfos);
        if (CollectionUtils.isEmpty(databasePayloads)) {
            LOG.debug("No database payloads to clear");
            return;
        }

        partInfoDao.clearDatabasePayloads(databasePayloads);
    }

    protected List<PartInfo> getDatabasePayloads(final List<PartInfo> partInfos) {
        Predicate<PartInfo> filenameEmptyPredicate = getFilenameEmptyPredicate();
        return getPayloads(partInfos, filenameEmptyPredicate);
    }

    @Override
    @Transactional
    public List<PartInfoFileNameSpiDto> findFileSystemPayloadFilenames(List<Long> userMessageEntityIds) {
        return partInfoDao.findFileSystemPayloadFilenames(userMessageEntityIds);
    }

    @Override
    public boolean scheduleSourceMessagePayloads(List<PartInfo> partInfos) {
        if (CollectionUtils.isEmpty(partInfos)) {
            LOG.debug("SourceMessages does not have any payloads");
            return false;
        }

        long totalPayloadLength = 0;
        for (PartInfo partInfo : partInfos) {
            totalPayloadLength += partInfo.getLength();
        }
        LOG.debug("SourceMessage payloads totalPayloadLength(bytes) [{}]", totalPayloadLength);

        final Long payloadsScheduleThresholdMB = domibusPropertyProvider.getLongProperty(PROPERTY_PAYLOADS_SCHEDULE_THRESHOLD);
        LOG.debug("Using configured payloadsScheduleThresholdMB [{}]", payloadsScheduleThresholdMB);

        final long payloadsScheduleThresholdBytes = payloadsScheduleThresholdMB * BYTES_IN_MB;
        if (totalPayloadLength > payloadsScheduleThresholdBytes) {
            LOG.debug("The SourceMessage payloads will be scheduled for saving");
            return true;
        }
        return false;

    }

    /**
     * WARNING: for message sent, the compress flag is NOT set and the {@link eu.domibus.api.model.PartInfo#getPayloadDatahandler} will provide the zipped data
     */
    @Override
    public void loadBinaryData(PartInfo partInfo) {
        final String payloadManagerSPI = partInfo.getPayloadManagerSPI();
        if (StringUtils.isBlank(payloadManagerSPI)) {
            LOG.debug("No payload SPI provider found for payload [{}]. In case of Split and Join this is normal because the PartInfo instances are updated using a JPA merge and loadBinaryData is called before merge is executed", partInfo.getHref());
            return;
        }

        final DomibusPayloadManagerSpi domibusPayloadManagerSpi = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpi(payloadManagerSPI);

        final boolean arePayloadsStoredInDomibusDatabase = domibusPayloadManagerSpi.arePayloadsStoredInDomibusDatabase();
        if (arePayloadsStoredInDomibusDatabase) {
            LOG.debug("Loading payload [{}] from Domibus database", partInfo.getHref());
            /* Create payload data handler from binaryData (byte[]) */
            byte[] binaryData = partInfo.getBinaryData();
            if (binaryData == null) {
                LOG.debug("Payload is empty!");
                partInfo.setPayloadDatahandler(null);
            } else {
                DataSource dataSource = new ByteArrayDataSource(binaryData, partInfo.getMime());
                createPayloadDataHandler(partInfo, dataSource);
            }
            return;
        }

        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        String fileName = partInfo.getFileName();
        LOG.debug("Loading payload [{}] using payload SPI manager [{}]", fileName, payloadManagerSPI);

        //this stream will be closed by the AutoCloseDataSource
        InputStream inputStream = domibusPayloadManagerSpi.readPayload(currentDomain.getCode(), fileName);

        DataSource dataSource = new AutoCloseDataSource(partInfo.getFileName(), partInfo.getMime(), inputStream);
        createPayloadDataHandler(partInfo, dataSource);
    }

    @Override
    public void deleteAllPayloadFromFileSystem(List<DatabasePartition> toDeletePartition) {
        LOG.debug("delete All Payload From partitions [{}] present in the File System", toDeletePartition);

        DomibusPayloadManagerSpi domibusPayloadManagerSpiForCurrentDomain = domibusPayloadManagerSpiProvider.getDomibusPayloadManagerSpiForCurrentDomain();
        if (domibusPayloadManagerSpiForCurrentDomain.arePayloadsStoredInDomibusDatabase()) {
            LOG.info("Payloads are not from file system");
            return;
        }
        Domain currentDomain = domainContextProvider.getCurrentDomain();
        for (DatabasePartition databasePartition : toDeletePartition) {
            String payloadFolder = getPayloadFolder(databasePartition.getHighValue());
            //All payload are saved in a unique folder per hour similar to the partition segregation.
            DeleteFolderResult deleteFolderResult = domibusPayloadManagerSpiForCurrentDomain.deleteFolder(currentDomain.getCode(), payloadFolder);
            if (deleteFolderResult.getResult() != DeleteFolderResult.Result.OK) {
                LOG.error("Error while deleting folder [{}] for partition [{}]. Failed to delete [{}]/[{}] files",
                        payloadFolder,
                        databasePartition.getPartitionName(),
                        deleteFolderResult.getFailed().size(),
                        deleteFolderResult.getTotal());
                for (String fileFailed : deleteFolderResult.getFailed()) {
                    LOG.debug("File in error [{}]", fileFailed);
                }
            }
        }
    }


    @Override
    public String getPayloadFolder(long userMessageEntityId) {
        ZonedDateTime currentDate = tsidUtil.getZonedDateTimeFromTsid(userMessageEntityId);
        return getPayloadFolder(currentDate);
    }

    @Override
    public String getPayloadFolder(ZonedDateTime currentDate) {
        return currentDate.getYear() + "/" +
                padLeft(currentDate.getMonthValue()) + "/" +
                padLeft(currentDate.getDayOfMonth()) + "/" +
                padLeft(currentDate.getHour()) + "/";
    }

    /**
     * 1  -> 01
     * 12 -> 12
     *
     * @param i could be a month, day or hour
     * @return {@param i} left padded with '0'
     */
    protected String padLeft(int i) {
        return StringUtils.leftPad(i + "", 2, "0");
    }

    private void createPayloadDataHandler(PartInfo partInfo, DataSource fsDataSource) {
        String href = partInfo.getHref();
        if (partInfo.isEncrypted()) {
            LOG.debug("Using DecryptDataSource for payload [{}]", href);
            final Cipher decryptCipher = getDecryptCipher(partInfo);
            fsDataSource = new DecryptDataSource(fsDataSource, decryptCipher);
        }

        if (BooleanUtils.isTrue(partInfo.getCompressed())) {
            LOG.debug("Setting the decompressing handler on the the payload [{}]", href);
            fsDataSource = new DecompressionDataSource(fsDataSource, partInfo.getMime());
        }

        partInfo.setPayloadDatahandler(new DataHandler(fsDataSource));
    }

    private Cipher getDecryptCipher(PartInfo partInfo) {
        LOG.debug("Getting decrypt cipher for payload [{}]", partInfo.getHref());
        final PayloadEncryptionService encryptionService = SpringContextProvider.getApplicationContext().getBean("EncryptionServiceImpl", PayloadEncryptionService.class);
        return encryptionService.getDecryptCipherForPayload();
    }

    public void validatePayloadSize(LegConfiguration legConfiguration, List<PartInfo> partInfos, boolean async) {
        long total = partInfos.stream().map(PartInfo::getLength).mapToLong(Long::longValue).sum();
        payloadPersistenceHelper.validatePayloadSize(legConfiguration, total, async);
    }

    /**
     * @param userMessage the UserMessage received
     * @throws EbMS3Exception if an attachment with an invalid charset is received
     *                        (not {@link Property#CHARSET} or not matching {@link Property#CHARSET_PATTERN})
     */
    public void checkPartInfoCharset(final UserMessage userMessage, List<PartInfo> partInfoList) throws EbMS3Exception {
        LOG.debug("Checking charset for attachments");
        if (CollectionUtils.isEmpty(partInfoList)) {
            LOG.debug("No partInfo found");
            return;
        }

        for (final PartInfo partInfo : partInfoList) {
            if (CollectionUtils.isEmpty(partInfo.getPartProperties())) {
                continue;
            }
            for (final Property property : partInfo.getPartProperties()) {
                if (Property.CHARSET.equalsIgnoreCase(property.getName()) && !Property.CHARSET_PATTERN.matcher(property.getValue()).matches()) {
                    LOG.businessError(DomibusMessageCode.BUS_MESSAGE_CHARSET_INVALID, property.getValue(), userMessage.getMessageId());
                    throw EbMS3ExceptionBuilder.getInstance()
                            .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0003)
                            .message(property.getValue() + " is not a valid Charset")
                            .refToMessageId(userMessage.getMessageId())
                            .mshRole(MSHRole.RECEIVING)
                            .build();
                }
            }
        }
    }

    public void validatePayloadSizeBeforeSchedulingSave(LegConfiguration legConfiguration, List<PartInfo> partInfoList) {
        validatePayloadSize(legConfiguration, partInfoList, true);
        for (PartInfo partInfo : partInfoList) {
            payloadPersistenceHelper.validatePayloadSize(partInfo, false);
        }
    }
}
