package eu.domibus.core.crypto;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.pki.SecurityProfileProvider;
import eu.domibus.api.pmode.PModeException;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.security.SecurityProfileMetadata;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
@Service
public class SecurityProfileProviderImpl implements SecurityProfileProvider {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SecurityProfileProviderImpl.class);

    protected volatile Map<Domain, DomainSecurityProfileProvider> domainSecurityProfileProviderMap = new HashMap<>();

    @Autowired
    DomainSecurityProfileProviderFactory domainSecurityProfileProviderFactory;

    public DomainSecurityProfileProvider getDomainSecurityProfileProvider(Domain domain) {
        LOG.debug("Get domain SecurityProfileProviderMap for domain [{}]", domain);
        if (domainSecurityProfileProviderMap.get(domain) == null) {
            synchronized (domainSecurityProfileProviderMap) {
                if (domainSecurityProfileProviderMap.get(domain) == null) { //NOSONAR: double-check locking
                    LOG.debug("Creating domain SecurityProfileProvider for domain [{}]", domain);
                    DomainSecurityProfileProvider domainCertificateProvider = domainSecurityProfileProviderFactory.domainSecurityProfileProvider(domain);
                    domainSecurityProfileProviderMap.put(domain, domainCertificateProvider);
                }
            }
        }
        return domainSecurityProfileProviderMap.get(domain);
    }

    @Override
    public List<SecurityProfileConfiguration> getSecurityProfileConfigurations(Domain domain) {
        final DomainSecurityProfileProvider securityProfileProvider = getDomainSecurityProfileProvider(domain);
        return securityProfileProvider.getSecurityProfileConfigurations();
    }

    @Override
    public SecurityProfile determineSecurityProfile(Domain domain, SecurityProfileMetadata securityProfileMetadata) {
        //get all configured security profile configurations
        final DomainSecurityProfileProvider securityProfileProvider = getDomainSecurityProfileProvider(domain);
        final List<SecurityProfileConfiguration> securityProfileConfigurations = securityProfileProvider.getSecurityProfileConfigurations();
        if (CollectionUtils.isEmpty(securityProfileConfigurations)) {
            LOG.warn("No security profile configurations found for domain [{}]", domain);
            return null;
        }
        if (securityProfileConfigurations.size() == 1) {
            LOG.debug("Only one security profile configuration found for domain [{}]", domain);
            final SecurityProfileConfiguration securityProfileConfiguration = securityProfileConfigurations.get(0);
            if (securityProfileConfiguration.isLegacyConfiguration()) {
                LOG.debug("Legacy security profile configuration found for domain [{}]", domain);
                return securityProfileConfiguration.getSecurityProfile();
            }
        }

        final Map<String, String> securityProfileMetadataAsMap = securityProfileMetadataToMap(securityProfileMetadata);

        for (SecurityProfileConfiguration securityProfileConfiguration : securityProfileConfigurations) {
            final Map<String, String> matchExpression = securityProfileConfiguration.getMatchExpression();
            if (securityProfileMatchExpressionMatchesSecurityProfileMetadata(matchExpression, securityProfileMetadataAsMap)) {
                LOG.debug("Security profile [{}] matches metadata [{}]", securityProfileConfiguration.getSecurityProfile().getCode(), securityProfileMetadataAsMap);
                return securityProfileConfiguration.getSecurityProfile();
            }
        }

        LOG.warn("Could not determine security profile for metadata [{}]", securityProfileMetadataAsMap);
        return null;
    }

    protected boolean securityProfileMatchExpressionMatchesSecurityProfileMetadata(Map<String, String> securityProfileMatchExpression, Map<String, String> securityProfileMetadataAsMap) {
        for (Map.Entry<String, String> entry : securityProfileMatchExpression.entrySet()) {
            final String key = entry.getKey();
            final String value = entry.getValue();
            final String metadataValue = securityProfileMetadataAsMap.get(key);
            if (metadataValue == null || !StringUtils.equalsIgnoreCase(metadataValue, value)) {
                LOG.debug("Security profile match expression  [{}] does not match metadata [{}]", securityProfileMatchExpression, securityProfileMetadataAsMap);
                return false;
            }
        }
        return true;

    }

    protected Map<String, String> securityProfileMetadataToMap(SecurityProfileMetadata securityProfileMetadata) {
        Map<String, String> result = new HashMap<>();
        if(securityProfileMetadata == null) {
            LOG.debug("Security profile metadata is null");
            return result;
        }

        result.put("signatureAlgorithm", securityProfileMetadata.getSignatureAlgorithm());
        result.put("encryptionKeyTransportAlgorithm", securityProfileMetadata.getEncryptionKeyTransportAlgorithm());
        result.put("encryptionMGFAlgorithm", securityProfileMetadata.getEncryptionMGFAlgorithm());
        result.put("encryptionKeyAgreementMethod", securityProfileMetadata.getEncryptionKeyAgreementMethod());
        result.put("encryptionKeyDerivationFunction", securityProfileMetadata.getEncryptionKeyDerivationFunction());

        return result;
    }

    @Override
    public SecurityProfileConfiguration getSecurityProfileConfigurationWithRSAAsDefault(Domain domain, String securityProfileCode) throws PModeException {
        final DomainSecurityProfileProvider securityProfileProvider = getDomainSecurityProfileProvider(domain);
        return securityProfileProvider.getSecurityProfileConfigurationWithRSAAsDefault(securityProfileCode);
    }

    @Override
    public List<SecurityProfile> getSecurityProfilesPriorityList(Domain domain) {
        final DomainSecurityProfileProvider securityProfileProvider = getDomainSecurityProfileProvider(domain);
        return securityProfileProvider.getSecurityProfilesPriorityList();
    }

    @Override
    public SecurityProfile getSecurityProfileWithHighestPriority(Domain domain) {
        final DomainSecurityProfileProvider securityProfileProvider = getDomainSecurityProfileProvider(domain);
        final List<SecurityProfile> securityProfilesPriorityList = securityProfileProvider.getSecurityProfilesPriorityList();
        if (CollectionUtils.isEmpty(securityProfilesPriorityList)) {
            return null;
        }
        //the list is already sorted by priority
        return securityProfilesPriorityList.get(0);
    }

    @Override
    public List<SecurityProfile> getDefinedSecurityProfiles(Domain currentDomain) {
        final DomainSecurityProfileProvider securityProfileProvider = getDomainSecurityProfileProvider(currentDomain);
        return securityProfileProvider.getSecurityProfilesPriorityList();
    }

    public SecurityProfile getSecurityProfileByName(Domain currentDomain, String securityProfileValue) {
        final DomainSecurityProfileProvider securityProfileProvider = getDomainSecurityProfileProvider(currentDomain);
        return securityProfileProvider.getSecurityProfileByName(securityProfileValue);
    }
}
