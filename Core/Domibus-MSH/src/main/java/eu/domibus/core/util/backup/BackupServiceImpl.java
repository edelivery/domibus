package eu.domibus.core.util.backup;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.util.DateUtil;
import eu.domibus.configuration.spi.DomibusConfigurationManagerSpi;
import eu.domibus.configuration.spi.DomibusResource;
import eu.domibus.configuration.spi.SearchStrategy;
import eu.domibus.core.configuration.DomibusConfigurationManagerSpiProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Ion Perpegel
 * @since 4.1.1
 * <p>
 * Utility service used to back-up files before updating them.
 */
@Service
public class BackupServiceImpl implements BackupService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BackupServiceImpl.class);

    protected static final String BACKUP_EXT = ".backup-";
    protected static final DateTimeFormatter BACKUP_FILE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH_mm_ss.SSS");
    protected static final String BACKUP_FOLDER_NAME = "backups";

    @Autowired
    protected DateUtil dateUtil;

    @Override
    public void backupFile(String originalFile) {
        final String backupFile = getBackupFile(originalFile);
        backupFile(backupFile, null);
    }

    @Override
    public byte[] writeLinesAsBytes(List<String> lines) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try (final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out, StandardCharsets.UTF_8);
             BufferedWriter writer = new BufferedWriter(outputStreamWriter)) {
            for (String line : lines) {
                writer.append(line);
                writer.newLine();
            }
            return out.toByteArray();
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not write lines as bytes", e);
        }
    }

    @Override
    public void backupFile(String originalFile, String subFolder) {
        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();

        DomibusResource domibusResource = null;
        try {
            domibusResource = domibusConfigurationProvider.getResource(originalFile, originalFile);
        } catch (Exception e) {
            LOG.trace("Resource does not exist [{}]. Skip backing up resource", originalFile, e);
            LOG.info("Resource does not exists [{}]. Skip backing up resource", originalFile);
            return;
        }

        domibusConfigurationProvider.backupResource(domibusResource, subFolder);
    }

    @Override
    public void backupFileInLocation(DomibusResource configurationResource, String location) {
        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();
        domibusConfigurationProvider.backupResource(configurationResource, location);
    }

    @Override
    public void backupFileIfOlderThan(DomibusResource configurationResource, String subFolder, Integer periodInHours) {
        if (periodInHours == 0) {
            LOG.debug("Min backup period is 0 so backing up file [{}]", configurationResource.getName());
            backupFileInLocation(configurationResource, subFolder);
            return;
        }

        List<DomibusResource> backupFilesAlreadyPresent = getBackupFilesOf(configurationResource.getParentLocation() + "/" + subFolder, configurationResource.getName());
        if (CollectionUtils.isEmpty(backupFilesAlreadyPresent)) {
            LOG.debug("No backups found so backing up file [{}]", configurationResource.getName());
            backupFileInLocation(configurationResource, subFolder);
            return;
        }

        long elapsed = new Date().toInstant().toEpochMilli() - backupFilesAlreadyPresent.get(0).getLastModified().toEpochMilli();
        if (elapsed < Duration.ofHours(periodInHours).toMillis()) {
            LOG.debug("No minimum period of time elapsed since the last backup so NO backing up file [{}]", configurationResource.getName());
            return;
        }

        backupFileInLocation(configurationResource, subFolder);
    }

    @Override
    public void deleteBackupsIfMoreThan(DomibusResource originalFile, Integer maxFilesToKeep) {
        if (maxFilesToKeep == 0) {
            LOG.debug("Maximum backup history is 0 so exiting");
            return;
        }

        List<DomibusResource> backupFilesAlreadyPresent = getBackupFilesOf(originalFile.getParentLocation() + "/" + BACKUP_FOLDER_NAME, originalFile.getName());
        if (CollectionUtils.isEmpty(backupFilesAlreadyPresent)) {
            LOG.debug("No backups to delete");
            return;
        }

        if (backupFilesAlreadyPresent.size() <= maxFilesToKeep) {
            LOG.debug("Maximum number of allowed backups [{}] has not been reached for file [{}], so exiting.", maxFilesToKeep, originalFile.getName());
            return;
        }
        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();

        List<String> exceptions = new ArrayList<>();
        backupFilesAlreadyPresent.subList(maxFilesToKeep, backupFilesAlreadyPresent.size())
                .forEach(file -> {
                    try {
                        LOG.debug("Deleting backup file [{}].", file.getName());
                        domibusConfigurationProvider.deleteResource(file);
                    } catch (Exception e) {
                        exceptions.add(String.format("Could not delete backup file [%s] due to [%s].", file.getName(), e.getMessage()));
                    }
                });
        if (!CollectionUtils.isEmpty(exceptions)) {
            throw new DomibusCoreException("Could not delete backups:" + String.join("\n", exceptions));
        }
    }

    private List<DomibusResource> getBackupFilesOf(String backupFileLocation, String backupFileName) {
        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();
        final List<DomibusResource> backupFilesAlreadyPresent = domibusConfigurationProvider.findFiles(
                backupFileLocation,
                backupFileName,
                SearchStrategy.CURRENT_LOCATION_CONTAINING_PATTERN,
                new ArrayList<>(),
                "backup files");
        if (CollectionUtils.isEmpty(backupFilesAlreadyPresent)) {
            LOG.info("No backup files found in [{}]", backupFileLocation);
            return Collections.emptyList();
        }
        return backupFilesAlreadyPresent.stream()
//                .filter(file -> file.getName().startsWith(backupFile.getName()))
                .sorted(Comparator.comparing(DomibusResource::getLastModified).reversed())
                .collect(Collectors.toList());
    }

    protected File getBackupFile(File originalFile) {
        String backupFileName = originalFile.getName() + BACKUP_EXT + dateUtil.getCurrentTime(BACKUP_FILE_FORMATTER);
        return new File(originalFile.getParent(), backupFileName);
    }

    protected String getBackupFile(String originalFile) {
        String backupFileName = originalFile + BACKUP_EXT + dateUtil.getCurrentTime(BACKUP_FILE_FORMATTER);
        return backupFileName;
    }

    protected File createBackupFileInLocation(File originalFile, String backupLocation) throws IOException {
        File backupFile = new File(backupLocation);
        if (!Files.exists(Paths.get(backupLocation).normalize())) {
            LOG.debug("Creating backup directory [{}]", backupLocation);
            try {
                FileUtils.forceMkdir(backupFile);
            } catch (IOException e) {
                throw new IOException("Could not create backup directory", e);
            }
        }
        return getBackupFile(originalFile, backupFile);
    }

    protected File getBackupFile(File originalFile, File backupFile) {
        String backupFileName = originalFile.getName() + BACKUP_EXT + dateUtil.getCurrentTime(BACKUP_FILE_FORMATTER);
        return new File(backupFile, backupFileName);
    }
}
