package eu.domibus.core.clustering;

import eu.domibus.api.cluster.ClusterNodeIdentifierService;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.server.ServerInfoService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;

import java.util.List;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_CLUSTER_NODE_ID_LENGTH;

/**
 * @author Ion Perpegel
 * @since 5.2
 */
@Service
public class ClusterNodeIdentifierServiceImpl implements ClusterNodeIdentifierService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ClusterNodeIdentifierServiceImpl.class);

    private final ClusterNodeIdentifierDao clusterNodeIdentifierDao;

    private final ServerInfoService serverInfoService;

    private final DomibusPropertyProvider domibusPropertyProvider;

    // node id is unique for each node in the cluster
    private int nodeId;

    // the maximum value for the node id, calculated based on digit number red from properties
    private final int maxValue;

    public ClusterNodeIdentifierServiceImpl(ClusterNodeIdentifierDao clusterNodeIdentifierDao,
                                            ServerInfoService serverInfoService,
                                            DomibusPropertyProvider domibusPropertyProvider) {
        this.clusterNodeIdentifierDao = clusterNodeIdentifierDao;
        this.serverInfoService = serverInfoService;
        this.domibusPropertyProvider = domibusPropertyProvider;

        maxValue = (int) Math.pow(2, getNodeIdBits());
    }

    @Override
    // This method is called at initialization stage, in a synchronized manner, to fetch or create a node id for the current node
    // We call it early to make sure we do not get into stack-overflow issues when calling it from DAO methods
    public void fetchOrCreateNodeId() {
        nodeId = getOrCreate();
    }

    @Override
    public int getNodeId() {
        return nodeId;
    }

    @Override
    public int getNodeIdBits() {
        return domibusPropertyProvider.getIntegerProperty(DOMIBUS_CLUSTER_NODE_ID_LENGTH);
    }

    private Integer getOrCreate() {
        String nodeName = serverInfoService.getServerName();
        ClusterNodeIdentifierEntity entity = clusterNodeIdentifierDao.findByNodeName(nodeName);
        if (entity != null) {
            LOG.info("Node with name [{}] exists", nodeName);
            return entity.getNodeId();
        }
        return create(0);
    }

    private Integer create(int attemptNo) {
        String nodeName = serverInfoService.getServerName();
        LOG.info("Creating new id for node [{}], making the attempt [{}]", nodeName, attemptNo);

        ClusterNodeIdentifierEntity entity = new ClusterNodeIdentifierEntity();
        entity.setNodeName(nodeName);
        int nodeId = getNewId();
        entity.setNodeId(nodeId);
        try {
            clusterNodeIdentifierDao.create(entity);
            return nodeId;
        } catch (PersistenceException | DataIntegrityViolationException ex) {
            if (uniqueNodeIdConstraintViolation(ex)) {
                if (attemptNo > maxValue) {
                    throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not insert node id [" + nodeId + "] for node [" + nodeName + "] because it already exists. Giving up.", ex);
                }
                LOG.info("Could not insert node id [{}] for node [{}] because it already exists. Retrying [{}] time.", nodeId, nodeName, attemptNo + 1, ex);
                clusterNodeIdentifierDao.detach(entity);
                return create(attemptNo + 1);
            }
            throw ex;
        }
    }

    private boolean uniqueNodeIdConstraintViolation(RuntimeException ex) {
        // ignore the exception only if the unique constraint was violated; throw in other cases
        if (ex.getCause() instanceof ConstraintViolationException) {
            final String constraintName = ((ConstraintViolationException) ex.getCause()).getConstraintName();
            if (StringUtils.containsIgnoreCase(constraintName, ".UK_CLUSTER")) {
                return true;
            }
        }
        return false;
    }

    private int getNewId() {
        int nextId = clusterNodeIdentifierDao.getMaxId() + 1;
        if (nextId >= maxValue) {
            Integer unusedId = getUnusedId();
            if (unusedId != null) {
                return unusedId;
            }
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not generate new node id. The maximum number of nodes has been reached.");
        }
        return nextId;
    }

    private Integer getUnusedId() {
        List<Integer> all = clusterNodeIdentifierDao.getAllIds();
        for (int i = 0; i < maxValue; i++) {
            if (!all.contains(i)) {
                return i;
            }
        }
        return null;
    }
}
