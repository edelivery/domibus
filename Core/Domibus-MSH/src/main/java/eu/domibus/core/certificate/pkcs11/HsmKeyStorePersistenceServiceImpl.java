package eu.domibus.core.certificate.pkcs11;

import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.DomibusCertificateException;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.property.DomibusPropertyException;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.property.encryption.PasswordDecryptionService;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.core.certificate.KeyStorePersistenceServiceImpl;
import eu.domibus.core.crypto.TruststoreDao;
import eu.domibus.core.property.DomibusRawPropertyProvider;
import eu.domibus.core.util.backup.BackupService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;
import static eu.domibus.core.crypto.MultiDomainCryptoServiceImpl.DOMIBUS_KEYSTORE_NAME;

/**
 * @author G. Maier
 * @since 5.2
 */
public class HsmKeyStorePersistenceServiceImpl extends KeyStorePersistenceServiceImpl {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(HsmKeyStorePersistenceServiceImpl.class);
    private static final String WILL_NOT_PERSIST_KEYSTORE_PKCS_11_TO_THE_DISK = "Persisting keystore used from HSM (PKCS#11) is not supported";
    private Provider provider;
    private char[] userPin;
    private String pin;

    public HsmKeyStorePersistenceServiceImpl(CertificateHelper certificateHelper,
                                             TruststoreDao truststoreDao,
                                             PasswordDecryptionService passwordDecryptionService,
                                             DomainContextProvider domainContextProvider,
                                             DomibusPropertyProvider domibusPropertyProvider,
                                             DomibusRawPropertyProvider domibusRawPropertyProvider,
                                             BackupService backupService) {
        super(certificateHelper, truststoreDao, passwordDecryptionService, domainContextProvider, domibusPropertyProvider, domibusRawPropertyProvider, backupService);
    }

    public void init() {
        String providerName = domibusPropertyProvider.getProperty(DOMIBUS_EXTENSION_IAM_PKCS11_PROVIDER_NAME);
        provider = Security.getProvider(providerName);
        if (provider == null) {
            String pkcs11Enabled = domibusPropertyProvider.getProperty(DOMIBUS_EXTENSION_IAM_PKCS11_ENABLED);
            if (!Boolean.parseBoolean(pkcs11Enabled)) {
                LOG.error("The PKCS11 security provider is not enabled. See property [{}]", DOMIBUS_EXTENSION_IAM_PKCS11_ENABLED);
                throw new DomibusPropertyException(String.format("The PKCS11 security provider is not enabled. See property [%s]", DOMIBUS_EXTENSION_IAM_PKCS11_ENABLED));
            }

            String providerNames = Arrays.stream(Security.getProviders()).map(Provider::getName).collect(Collectors.joining(","));
            LOG.error("Security provider named [{}] defined by the property [{}] for PKCS11 was not found among the providers present [{}]",
                    providerName, DOMIBUS_EXTENSION_IAM_PKCS11_PROVIDER_NAME, providerNames);
            throw new DomibusPropertyException(String.format("Security provider defined by the property [%s] for PKCS11 was not found, see the logs for details", DOMIBUS_EXTENSION_IAM_PKCS11_PROVIDER_NAME));
        }
        pin = domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_KEYSTORE_PASSWORD);
        if (StringUtils.isBlank(pin)) {
            throw new DomibusPropertyException(String.format("The property [%s] cannot be empty", DOMIBUS_SECURITY_KEYSTORE_PASSWORD));
        }
        userPin = pin.toCharArray();
    }

    @Override
    public KeystorePersistenceInfo getKeyStorePersistenceInfo() {
        return new KeyStorePersistenceInfoImpl();
    }

    @Override
    public void saveStoreFromDBToDisk(KeystorePersistenceInfo persistenceInfo) {
        if (persistenceInfo.isReadOnly()) {
            LOG.warn(WILL_NOT_PERSIST_KEYSTORE_PKCS_11_TO_THE_DISK);
            return;
        }
        super.saveStoreFromDBToDisk(persistenceInfo);
    }

    @Override
    public void saveStore(KeyStoreContentInfo contentInfo, KeystorePersistenceInfo persistenceInfo) {
        if (persistenceInfo.isReadOnly()) {
            LOG.warn(WILL_NOT_PERSIST_KEYSTORE_PKCS_11_TO_THE_DISK);
            return;
        }
        super.saveStore(contentInfo, persistenceInfo);
    }

    @Override
    public void saveStore(KeyStore store, KeystorePersistenceInfo persistenceInfo) {
        if (persistenceInfo.isReadOnly()) {
            LOG.warn(WILL_NOT_PERSIST_KEYSTORE_PKCS_11_TO_THE_DISK);
            return;
        }
        super.saveStore(store, persistenceInfo);
    }

    @Override
    public void saveStore(byte[] content, KeystorePersistenceInfo persistenceInfo) {
        if (persistenceInfo.isReadOnly()) {
            LOG.warn(WILL_NOT_PERSIST_KEYSTORE_PKCS_11_TO_THE_DISK);
            return;
        }
        super.saveStore(content, persistenceInfo);
    }


    public KeyStoreContentInfo loadStore(KeystorePersistenceInfo persistenceInfo) {
        if (!persistenceInfo.isDownloadable()) {
            String pkcsKeystoreType = domibusPropertyProvider.getProperty(DOMIBUS_EXTENSION_IAM_PKCS11_KEYSTORE_TYPE);
            String keystoreInitString = domibusPropertyProvider.getProperty(DOMIBUS_EXTENSION_IAM_PKCS11_KEYSTORE_INIT_STRING);

            try (InputStream initStream = StringUtils.isEmpty(keystoreInitString) ? null : new ByteArrayInputStream(keystoreInitString.getBytes())) {
                KeyStore ks = KeyStore.getInstance(pkcsKeystoreType, provider);
                LOG.info("Initializing KeyStore type [{}] using initialization string [{}]", pkcsKeystoreType, keystoreInitString);
                ks.load(initStream, userPin);
                return certificateHelper.createPkcs11KeyStoreContentInfo(provider.getName(), provider.getName(), ks, pin, ks.getType(), false);
            } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
                LOG.error("Couldn't load PKCS11 keyStore", e);
                throw new DomibusCertificateException("Couldn't load PKCS11 keyStore", e);
            }
        }
        return super.loadStore(persistenceInfo);
    }

    private class KeyStorePersistenceInfoImpl implements KeystorePersistenceInfo {
        @Override
        public String getName() {
            return DOMIBUS_KEYSTORE_NAME;
        }

        @Override
        public boolean isOptional() {
            return false;
        }

        @Override
        public String getFileLocation() {
            return domibusPropertyProvider.getProperty(DOMIBUS_EXTENSION_IAM_PKCS11_PROVIDER_NAME);
        }

        @Override
        public String getType() {
            return domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_KEYSTORE_TYPE);
        }

        @Override
        public String getPassword() {
            return domibusRawPropertyProvider.getRawPropertyValue(DOMIBUS_SECURITY_KEYSTORE_PASSWORD);
        }

        @Override
        public void updatePassword(String password) {
            domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_KEYSTORE_PASSWORD, password);
        }

        @Override
        public void updateType(String type) {
            domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_KEYSTORE_TYPE, type);
        }

        @Override
        public void updateFileLocation(String fileLocation) {
            domibusPropertyProvider.setProperty(DOMIBUS_EXTENSION_IAM_PKCS11_PROVIDER_NAME, fileLocation);
        }

        @Override
        public boolean isDownloadable() {
            return false;
        }

        @Override
        public boolean isReadOnly() {
            return true;
        }
    }
}
