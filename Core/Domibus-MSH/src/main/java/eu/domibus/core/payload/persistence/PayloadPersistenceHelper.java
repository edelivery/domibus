package eu.domibus.core.payload.persistence;

import eu.domibus.api.model.PartInfo;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.payload.encryption.PayloadEncryptionService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.common.model.configuration.PayloadProfile;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

/**
 * @author Cosmin Baciu
 * @since 4.1
 */
@Service
public class PayloadPersistenceHelper {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PayloadPersistenceHelper.class);

    protected final DomainContextProvider domainContextProvider;

    protected final DomibusConfigurationService domibusConfigurationService;
    protected final PayloadEncryptionService payloadEncryptionService;

    public PayloadPersistenceHelper(DomainContextProvider domainContextProvider,
                                    DomibusConfigurationService domibusConfigurationService,
                                    PayloadEncryptionService payloadEncryptionService) {
        this.domainContextProvider = domainContextProvider;
        this.domibusConfigurationService = domibusConfigurationService;
        this.payloadEncryptionService = payloadEncryptionService;
    }

    /**
     * Validates the payload (partInfo) size
     * Disabled if maxSize not defined in {@link PayloadProfile}
     *
     * @param legConfiguration holding the {@link PayloadProfile}
     * @param totalLength sum of all the {@link eu.domibus.api.model.PartInfo}
     * @param isPayloadSavedAsync true is the payload was saved asynchronously, false by default
     * @throws InvalidPayloadSizeException Exception thrown if payload size is greater than the maxSize defined in PMode
     */
    public void validatePayloadSize(@NotNull LegConfiguration legConfiguration, long totalLength, boolean isPayloadSavedAsync) throws InvalidPayloadSizeException {
        final PayloadProfile profile = legConfiguration.getPayloadProfile();
        if (profile == null) {
            LOG.debug("payload profile is not defined for leg [{}]", legConfiguration.getName());
            return;
        }
        final String payloadProfileName = profile.getName();
        final Long payloadProfileMaxSize = legConfiguration.getPayloadProfile().getMaxSize();

        if (payloadProfileMaxSize == null || payloadProfileMaxSize < 0) {
            LOG.warn("No validation will be made for [{}] as maxSize has the value [{}]", payloadProfileName, payloadProfileMaxSize);
            return;
        }

        if (totalLength > payloadProfileMaxSize * 1000) {
            throw new InvalidPayloadSizeException("Payload size [" + totalLength + " B] is greater than the maximum value " +
                    "defined [" + payloadProfileMaxSize + " kB] for profile [" + payloadProfileName + "]", isPayloadSavedAsync);
        }
    }


    /**
     * Validates the payload (partInfo) size
     * Disabled if maxSize not defined in {@link eu.domibus.common.model.configuration.Payload}
     *
     * @param partInfo payload of the userMessage
     * @param isPayloadSavedAsync true is the payload was saved asynchronously, false by default
     * @throws InvalidPayloadSizeException Exception thrown if payload size is greater than the maxSize defined in PMode
     */
    public void validatePayloadSize(@NotNull PartInfo partInfo, boolean isPayloadSavedAsync) throws InvalidPayloadSizeException {
        Long maxSize = partInfo.getMaxSize();

        long length = partInfo.getLength();
        if (maxSize == null || maxSize < 0) {
            LOG.debug("No validation will be made for payload [{}] as maxSize has the value [{}]", partInfo.getHref(), maxSize);
            return;
        }

        if (length > maxSize * 1000) {
            throw new InvalidPayloadSizeException("Payload size [" + length + " b] is greater than the maximum value " +
                    "defined [" + maxSize + " kb] for payload [" + partInfo.getHref() + "]", isPayloadSavedAsync);
        }
    }
}
