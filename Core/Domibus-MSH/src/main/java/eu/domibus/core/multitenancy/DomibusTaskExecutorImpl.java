package eu.domibus.core.multitenancy;

import eu.domibus.api.multitenancy.*;
import eu.domibus.api.multitenancy.lock.DomibusSynchronizationException;
import eu.domibus.core.multitenancy.lock.SynchronizationService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.SchedulingTaskExecutor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static eu.domibus.common.TaskExecutorConstants.DOMIBUS_LONG_RUNNING_TASK_EXECUTOR_BEAN_NAME;
import static eu.domibus.common.TaskExecutorConstants.DOMIBUS_TASK_EXECUTOR_BEAN_NAME;

/**
 * @author Cosmin Baciu
 * @since 4.0
 */
@Service
public class DomibusTaskExecutorImpl implements DomibusTaskExecutor {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusTaskExecutorImpl.class);

    public static final long DEFAULT_WAIT_TIMEOUT_IN_SECONDS = 60L;

    @Autowired
    protected DomainContextProvider domainContextProvider;

    @Qualifier(DOMIBUS_TASK_EXECUTOR_BEAN_NAME)
    @Autowired
    protected SchedulingTaskExecutor schedulingTaskExecutor;

    @Qualifier(DOMIBUS_LONG_RUNNING_TASK_EXECUTOR_BEAN_NAME)
    @Autowired
    protected SchedulingTaskExecutor schedulingLongTaskExecutor;

    @Autowired
    SynchronizationService synchronizationService;

    @Override
    public <T extends Object> T submit(Callable<T> task) {
        return submitCallable(task,
                null,
                null,
                ExecutorWaitPolicy.WAIT,
                DEFAULT_WAIT_TIMEOUT_IN_SECONDS,
                TimeUnit.SECONDS,
                null);
    }

    @Override
    public <T extends Object> T submitWithSecurityContext(Callable<T> task) {
        final Authentication currentAuthentication = SecurityContextHolder.getContext().getAuthentication();
        final Callable<T> setAuthRunnable = new SetAuthRunnable<>(currentAuthentication, task);
        return submit(setAuthRunnable);
    }

    @Override
    public <T extends Object> T submit(Callable<T> task, Domain domain) {
        return submitCallable(task,
                domain,
                null,
                ExecutorWaitPolicy.WAIT,
                DEFAULT_WAIT_TIMEOUT_IN_SECONDS,
                TimeUnit.SECONDS,
                null);
    }

    @Override
    public <T extends Object> T submitCallable(Callable<T> task,
                                               Domain domain,
                                               final Authentication authentication,
                                               ExecutorWaitPolicy waitPolicy,
                                               Long timeout,
                                               TimeUnit timeUnit,
                                               ExecutorExceptionHandler errorHandler) {
        //use the normal task executor(not the long running one)
        return submitCallable(schedulingTaskExecutor, task, domain, authentication, waitPolicy, timeout, timeUnit, errorHandler);
    }

    protected <T extends Object> T submitCallable(SchedulingTaskExecutor schedulingTaskExecutor,
                                                  Callable<?> task,
                                                  Domain domain,
                                                  final Authentication currentAuthentication,
                                                  ExecutorWaitPolicy waitPolicy,
                                                  Long timeout,
                                                  TimeUnit timeUnit,
                                                  ExecutorExceptionHandler errorHandler) {
        final CompositeCallable compositeRunnable = getCompositeCallable(task, domain, currentAuthentication, errorHandler);

        final Future<T> utrFuture = schedulingTaskExecutor.submit(compositeRunnable);

        if (ExecutorWaitPolicy.WAIT == waitPolicy) {
            LOG.debug("Waiting for task to complete");
            try {
                final T result = utrFuture.get(timeout, timeUnit);
                LOG.debug("Task completed");
                return result;
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                handleRunnableError(e, errorHandler);
            } catch (Throwable e) {
                handleRunnableError(e, errorHandler);
            }
        }
        return null;
    }


    protected CompositeCallable getCompositeCallable(Callable task,
                                                     Domain domain,
                                                     Authentication currentAuthentication,
                                                     ExecutorExceptionHandler errorHandler) {
        CompositeCallable compositeRunnable = new CompositeCallable();

        //copy the MDC context
        SetMDCContextTaskRunnable setMDCContextTaskRunnable = new SetMDCContextTaskRunnable();
        compositeRunnable.addRunnable(setMDCContextTaskRunnable);
        LOG.debug("Added SetMDCContextTaskRunnable in the list of pre-run");

        //set or remove domain on thread
        if (domain != null) {
            DomainRunnable domainRunnable = new DomainRunnable(domainContextProvider, domain);
            compositeRunnable.addRunnable(domainRunnable);
            LOG.debug("Added DomainRunnable in the list of pre-run with domain [{}]", domain.getCode());
        } else {
            ClearDomainRunnable clearDomainRunnable = new ClearDomainRunnable(domainContextProvider);
            compositeRunnable.addRunnable(clearDomainRunnable);
            LOG.debug("Added ClearDomainRunnable in the list of pre-run");
        }

        //set authentication if available
        if (currentAuthentication != null) {
            SetAuthenticationRunnable setAuthenticationRunnable = new SetAuthenticationRunnable(currentAuthentication);
            compositeRunnable.addRunnable(setAuthenticationRunnable);
            LOG.debug("Added SetAuthenticationRunnable in the list of pre-run");
        }

        //finally we add the main task
        compositeRunnable.setMainTask(task);
        //and the error handler
        compositeRunnable.setErrorHandler(errorHandler);
        return compositeRunnable;
    }

    @Override
    public void submit(Runnable task) {
        LOG.trace("Submitting task");

        submit(task, ExecutorWaitPolicy.WAIT);
    }

    @Override
    public void submitWithSecurityContext(Runnable task) {
        Callable<Void> callable = () -> {
            task.run();
            return null;
        };
        submitWithSecurityContext(callable);
    }

    protected Future<?> submit(Runnable task, ExecutorWaitPolicy waitPolicy) {
        LOG.trace("Submitting task, waitForTask policy [{}]", waitPolicy);
        return submitRunnable(schedulingTaskExecutor, task, null, null, waitPolicy, DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS, null);
    }

    @Override
    public <T> T executeWithLock(final Callable<T> task,
                                 final String dbLockKey,
                                 final String javaLockKey,
                                 Domain domain,
                                 final Authentication authentication,
                                 ExecutorWaitPolicy waitPolicy,
                                 Long timeout,
                                 TimeUnit timeUnit,
                                 final ExecutorExceptionHandler errorHandler) {
        Callable<T> synchronizedCallable = synchronizationService.getSynchronizedCallable(task, dbLockKey, javaLockKey);

        return submitCallable(
                synchronizedCallable,
                domain,
                authentication,
                waitPolicy,
                timeout,
                timeUnit,
                errorHandler);
    }

    @Override
    public <R> R executeWithLock(Callable<R> task, String dbLockKey, String javaLockKey) {
        Callable<R> synchronizedCallable = synchronizationService.getSynchronizedCallable(task, dbLockKey, javaLockKey);
        try {
            return synchronizedCallable.call();
        } catch (Exception e) {
            throw new DomibusSynchronizationException(e);
        }
    }

    @Override
    public void submit(Runnable task, Domain domain) {
        submit(schedulingTaskExecutor, task, domain, ExecutorWaitPolicy.WAIT, DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);
    }

    @Override
    public void submit(Runnable task, Domain domain, ExecutorWaitPolicy waitPolicy, Long timeout, TimeUnit timeUnit) {
        submit(schedulingTaskExecutor, task, domain, waitPolicy, timeout, timeUnit);
    }

    @Override
    public void submitLongRunningTask(Runnable task,
                                      Domain domain,
                                      Authentication authentication,
                                      ExecutorExceptionHandler errorHandler) {
        submitRunnable(schedulingLongTaskExecutor,
                task,
                domain,
                authentication,
                ExecutorWaitPolicy.NO_WAIT,
                null,
                null,
                errorHandler);
    }

    protected Future<?> submit(SchedulingTaskExecutor taskExecutor,
                               Runnable task,
                               Domain domain,
                               ExecutorWaitPolicy waitPolicy,
                               Long timeout,
                               TimeUnit timeUnit) {
        return submitRunnable(taskExecutor,
                task,
                domain,
                null,
                waitPolicy,
                timeout,
                timeUnit,
                null);
    }

    protected Future<Void> submitRunnable(SchedulingTaskExecutor taskExecutor,
                                       Runnable task,
                                       Domain domain,
                                       final Authentication authentication,
                                       ExecutorWaitPolicy waitPolicy,
                                       Long timeout,
                                       TimeUnit timeUnit,
                                       ExecutorExceptionHandler errorHandler) {


        //transform the Runnable into a Callable to reuse the existing method
        Callable<Void> callable = () -> {
            task.run();
            return null;
        };

        return submitCallable(
                taskExecutor,
                callable,
                domain,
                authentication,
                waitPolicy,
                timeout,
                timeUnit,
                errorHandler);
    }

    protected void handleRunnableError(Throwable exception, ExecutorExceptionHandler errorHandler) {
        if (errorHandler != null) {
            LOG.debug("Running the error handler", exception);
            errorHandler.handleException(exception);
            return;
        }
        LOG.error("Error executing task", exception);
        throw new DomainTaskException("Could not execute task", exception);
    }
}
