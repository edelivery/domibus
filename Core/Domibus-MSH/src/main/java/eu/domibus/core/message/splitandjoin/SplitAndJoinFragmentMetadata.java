package eu.domibus.core.message.splitandjoin;

public class SplitAndJoinFragmentMetadata {

    protected String fragmentFileName;
    protected long bytesLength;

    public SplitAndJoinFragmentMetadata(String fragmentFileName, long bytesLength) {
        this.fragmentFileName = fragmentFileName;
        this.bytesLength = bytesLength;
    }

    public String getFragmentFileName() {
        return fragmentFileName;
    }

    public long getBytesLength() {
        return bytesLength;
    }
}
