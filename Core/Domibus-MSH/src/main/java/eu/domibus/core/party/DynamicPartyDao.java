package eu.domibus.core.party;

import eu.domibus.common.model.configuration.Process;
import eu.domibus.common.model.configuration.*;
import eu.domibus.common.model.parties.*;
import eu.domibus.core.dao.BasicDao;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class DynamicPartyDao extends BasicDao<DynamicParty> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DynamicPartyDao.class);

    public DynamicPartyDao() {
        super(DynamicParty.class);
    }

    @Transactional
    public void deleteByName(String name) {
        Query query = em.createNamedQuery("DynamicParty.deleteByName");
        query.setParameter("NAME", name);
        query.executeUpdate();
    }

    @Transactional
    public List<DynamicParty> findDynamicParties() {
        final TypedQuery<DynamicParty> query = em.createNamedQuery("DynamicParty.findAll", DynamicParty.class);

        List<DynamicParty> parties = query.getResultList();
        parties.forEach(party -> loadDynamicParty(party));
        return parties;
    }

    @Transactional
    public DynamicParty findDynamicParty(String partyName) {
        return getResultsOfQuery("DynamicParty.findByName", partyName);
    }

    private DynamicParty findAnyByName(String partyName) {
        return getResultsOfQuery("DynamicParty.findAnyByName", partyName);
    }

    private DynamicParty getResultsOfQuery(String queryName, String partyName) {
        final TypedQuery<DynamicParty> query = em.createNamedQuery(queryName, DynamicParty.class);
        query.setParameter("NAME", partyName);

        List<DynamicParty> parties = query.getResultList();
        if (parties.size() == 0) {
            LOG.debug("No dynamic party found with name [{}]", partyName);
            return null;
        }
        if (parties.size() > 1) {
            LOG.error("More than one dynamic party found with name [{}]: [{}]", partyName, parties);
        }
        return loadDynamicParty(parties.get(0));
    }

    private DynamicParty loadDynamicParty(DynamicParty dynamicParty) {
        dynamicParty.getInitiatorList().size();
        dynamicParty.getResponderList().size();
        dynamicParty.getIdentifiers().size();
        return dynamicParty;
    }

    @Override
    @Transactional
    public void flush() {
        super.flush();
    }

    @Transactional
    public void persistDynamicPartyIdType(BusinessProcesses businessProcesses, PartyIdType partyIdType) {
        DynamicPartyIdType dynamicPartyIdType = new DynamicPartyIdType();
        dynamicPartyIdType.setBusinessProcess(businessProcesses.getEntityId());
        dynamicPartyIdType.setName(partyIdType.getName());
        dynamicPartyIdType.setValue(partyIdType.getValue());
        em.persist(dynamicPartyIdType);

        // NOTE: make sure the partyIdType instance in the in-memory pmode has the entity id filled in
        partyIdType.setEntityId(dynamicPartyIdType.getEntityId());
    }

    @Transactional
    public void persistDynamicParty(Configuration configuration, Party party) {
        // check if the party already exists in the database: this can happen if the dynamic discovery was triggered due to the change of the endpoint
        DynamicParty dynamicParty = findAnyByName(party.getName());
        if (dynamicParty == null) {
            LOG.info("Creating dynamic party [{}]", party.getName());
            dynamicParty = new DynamicParty();
        } else {
            LOG.info("Party [{}] already exists in the database. Updating.", party.getName());
            dynamicParty.getIdentifiers().clear();
        }

        dynamicParty.setEndpoint(party.getEndpoint());
        dynamicParty.setName(party.getName());
        dynamicParty.setUserName(party.getUserName());
        dynamicParty.setPassword(party.getPassword());
        dynamicParty.setDynamic(party.isDynamic());
        dynamicParty.setBusinessProcess(configuration.getBusinessProcesses().getEntityId());

        addIdentifiers(party, dynamicParty);

        for (Process process : configuration.getBusinessProcesses().getProcesses()) {
            addInitiatorParties(party, dynamicParty, process);
            addResponderParties(party, dynamicParty, process);
        }

        try {
            update(dynamicParty);
            // make sure the dynamic party is persisted before signaling other nodes (as they'll need to retrieve it from the db);
            // NOTE: Hibernate doesn't "know" that the DynamicParty entity and the Party entity both share the same
            // physical representation, but the underlying datasource knows.
            flush();
        } catch (PersistenceException | DataIntegrityViolationException e) {
            // ignore the exception only if the unique constraint was violated; throw in other cases
            if (!isUniquePartyConstraintException(party, e)) {
                throw e;
            }
        }
    }

    public void addIdentifiers(Party party, DynamicParty dynamicParty) {
        for (Identifier identifier : party.getIdentifiers()) {
            DynamicIdentifier id = new DynamicIdentifier();
            id.setPartyId(identifier.getPartyId());
            id.setPartyIdType(identifier.getPartyIdType().getEntityId());
            id.setPartyIdTypeValue(identifier.getPartyIdType().getValue());
            dynamicParty.getIdentifiers().add(id);
        }
    }

    private boolean isUniquePartyConstraintException(Party party, RuntimeException e) {
        if (e.getCause() instanceof ConstraintViolationException) {
            final String constraintName = ((ConstraintViolationException) e.getCause()).getConstraintName();

            // possible unique constraint names:
            // - test_domain1.uk_pm_party_name_INDEX_A (in h2)
            // - TB_PM_PARTY.UK_PM_PARTY_NAME (in mysql)
            // (TB_PM_PARTY_IDENTIFIER doesn't have a unique constraint for now)
            if (StringUtils.containsIgnoreCase(constraintName, ".UK")) {
                LOG.warn("Could not persist party [{}] because it is already saved.", party.getName(), e);
                return true;
            }
        }
        return false;
    }

    public void addResponderParties(Party party, DynamicParty dynamicParty, Process process) {
        for (Party rParty : process.getResponderParties()) {
            if (rParty.getName().equalsIgnoreCase(party.getName())) {
                if (!dynamicParty.isResponderForProcess(process)) {
                    LOG.debug("Adding responder party [{}] for process [{}]", party.getName(), process.getName());
                    DynamicResponderParty responder = new DynamicResponderParty();
                    responder.setProcess(process.getEntityId());
                    responder.setParty(dynamicParty);
                    dynamicParty.getResponderList().add(responder);
                } else {
                    LOG.debug("Responder party [{}] already exists for process [{}]", party.getName(), process.getName());
                }
            }
        }
    }

    public void addInitiatorParties(Party party, DynamicParty dynamicParty, Process process) {
        for (Party iParty : process.getInitiatorParties()) {
            if (iParty.getName().equalsIgnoreCase(party.getName())) {
                if (!dynamicParty.isInitiatorForProcess(process)) {
                    LOG.debug("Adding initiator party [{}] for process [{}]", party.getName(), process.getName());
                    DynamicInitiatorParty initiator = new DynamicInitiatorParty();
                    initiator.setProcess(process.getEntityId());
                    initiator.setParty(dynamicParty);
                    dynamicParty.getInitiatorList().add(initiator);
                } else {
                    LOG.debug("Initiator party [{}] already exists for process [{}]", party.getName(), process.getName());
                }
            }
        }
    }

}
