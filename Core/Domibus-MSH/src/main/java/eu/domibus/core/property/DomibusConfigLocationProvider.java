package eu.domibus.core.property;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.core.configuration.DomibusConfigurationManagerSpiProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @author Cosmin Baciu
 * @since 4.2
 */
public class DomibusConfigLocationProvider {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusConfigLocationProvider.class);

    protected static final DateTimeFormatter TEMPORARY_DIRECTORY_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH_mm_ss.SSS");

    private static volatile String domibusConfigLocationValue;
    private static final Object domibusConfigLocationValueLock = new Object();

    public static String getDomibusConfigLocation() {
        //use synchronization to avoid creating multiple locations
        if (domibusConfigLocationValue == null) {
            synchronized (domibusConfigLocationValueLock) {
                if (domibusConfigLocationValue == null) {
                    LOG.info("Getting domibus config location");
                    String domibusLocation = doGetDomibusLocation();
                    if (StringUtils.isBlank(domibusLocation)) {
                        throw new DomibusCoreException("Please provide the Domibus config location");
                    }
                    domibusConfigLocationValue = Paths.get(domibusLocation).normalize().toString();

                    //set domibus.config.location as system variable to be resolved when using ${domibus.config.location}
                    System.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_CONFIG_LOCATION, domibusConfigLocationValue);
                    LOG.info("Setting [{}] as system property to [{}]", DomibusPropertyMetadataManagerSPI.DOMIBUS_CONFIG_LOCATION, domibusConfigLocationValue);
                }
            }

        }
        return domibusConfigLocationValue;
    }

    protected static String doGetDomibusLocation() {
        if (DomibusConfigurationManagerSpiProvider.isCustomConfigurationProviderUsed()) {
            //plugins jars and logback will be copied into this temporary location so that they can be loaded
            final File tempDomibusConfigDirectory = getTempDomibusConfigDirectory();
            LOG.debug("Custom configuration provider is used: using Domibus config location [{}]", tempDomibusConfigDirectory);
            return tempDomibusConfigDirectory.getAbsolutePath();
        }

        return getDefaultDomibusConfigLocation();
    }

    protected static String getDefaultDomibusConfigLocation() {
        return System.getProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_CONFIG_LOCATION);
    }

    protected static File getTempDomibusConfigDirectory() {
        final String javaTempDir = System.getProperty("java.io.tmpdir");

        final String currentTime = LocalDateTime.now(ZoneOffset.UTC).format(TEMPORARY_DIRECTORY_FORMATTER);
        String temporaryDirectory = "domibusConfig-" + currentTime;
        final File domibusTemporaryConfig = new File(javaTempDir, temporaryDirectory);

        if (!domibusTemporaryConfig.exists()) {
            try {
                LOG.info("Creating directory [{}]", domibusTemporaryConfig);
                FileUtils.forceMkdir(domibusTemporaryConfig);
            } catch (IOException e) {
                throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not create directory [" + domibusTemporaryConfig + "]", e);
            }
        }

        return domibusTemporaryConfig;
    }
}
