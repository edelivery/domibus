package eu.domibus.core.util;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.CertificateService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.crypto.AbstractTLSTrustManager;
import eu.domibus.core.crypto.TLSMshTrustStorePersistenceInfoImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.KeyStore;

/**
 * @author Ionut Breaz
 * @since 5.2
 * <p>
 * X509TrustManager with the TLS MSH Truststore configured in Domibus and optionally the cacerts </p>
 * Use this for all https requests that involve communication between access points
 */

@Service
public class TLSMshTrustManager extends AbstractTLSTrustManager {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSMshTrustManager.class);

    protected final DomibusPropertyProvider domibusPropertyProvider;

    protected final CertificateService certificateService;

    protected final TLSMshTrustStorePersistenceInfoImpl tlsMshTrustStorePersistenceInfoImpl;

    protected final DomibusConfigurationService domibusConfigurationService;

    protected final DomainContextProvider domainContextProvider;

    @Autowired
    public TLSMshTrustManager(DomibusPropertyProvider domibusPropertyProvider, CertificateService certificateService, TLSMshTrustStorePersistenceInfoImpl tlsMshTrustStorePersistenceInfoImpl, DomibusConfigurationService domibusConfigurationService, DomainContextProvider domainContextProvider) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.certificateService = certificateService;
        this.tlsMshTrustStorePersistenceInfoImpl = tlsMshTrustStorePersistenceInfoImpl;
        this.domibusConfigurationService = domibusConfigurationService;
        this.domainContextProvider = domainContextProvider;
        this.tlsTruststoreType = "Msh";
    }

    @Override
    protected boolean shouldUseCacerts() {
        return Boolean.TRUE.equals(domibusPropertyProvider.getBooleanProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_USE_CACERTS));
    }

    @Override
    protected KeyStore getTrustStore() {
        try {
            final String tlsTruststoreFileLocation = tlsMshTrustStorePersistenceInfoImpl.getFileLocation();
            if (StringUtils.isBlank(tlsTruststoreFileLocation)) {
                LOG.info("No TLS MSH Truststore defined for domain [{}]", getCurrentDomainCode());
                return null;
            }
            LOG.info("Getting the TLS MSH truststore from location [{}] for domain [{}].", tlsTruststoreFileLocation, getCurrentDomainCode());
            return certificateService.getStore(tlsMshTrustStorePersistenceInfoImpl);
        } catch (Exception ex) {
            LOG.info("Could not load TLS MSH Truststore found for domain [{}]", getCurrentDomainCode(), ex);
            return null;
        }
    }

    protected String getCurrentDomainCode() {
        Domain currentDomain = domainContextProvider.getCurrentDomain();
        return currentDomain != null ? currentDomain.getCode() : StringUtils.EMPTY;
    }
}
