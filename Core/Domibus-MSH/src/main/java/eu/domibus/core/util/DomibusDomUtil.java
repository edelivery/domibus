package eu.domibus.core.util;

import eu.domibus.api.exceptions.XmlProcessingException;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
@Service
public class DomibusDomUtil {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusDomUtil.class);

    public Node getFirstChild(Node parent, String childName) {
        final List<Node> nodes = getChildren(parent, childName);
        if (CollectionUtils.isNotEmpty(nodes)) {
            return nodes.get(0);
        }

        return null;
    }

    public String getFirstChildValue(Node parent, String childName) {
        final Node firstChild = getFirstChild(parent, childName);
        if (firstChild == null) {
            LOG.debug("Child [{}] could not be found in parent [{}]", childName, parent.getNodeName());
            return null;
        }
        return getTextContent(firstChild);
    }


    public String getTextContent(Node node) {
        if (node == null) {
            LOG.error("Unexpected null value for node.");
            return null;
        }
        return StringUtils.trim(node.getTextContent());
    }

    public List<Node> getChildren(Node parent, String childName) {
        List<Node> result = new ArrayList<>();

        final NodeList childNodes = parent.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); ++i) {
            final Node item = childNodes.item(i);
            if (childName.equals(item.getLocalName())) {
                result.add(item);
            }
        }

        return result;
    }

    public Element getChildElementByName(Node parentNode, String nodeName) throws XmlProcessingException {
        for (Node currentChild = parentNode.getFirstChild(); currentChild != null; currentChild = currentChild.getNextSibling()) {
            if (Node.ELEMENT_NODE == currentChild.getNodeType() && nodeName.equalsIgnoreCase(currentChild.getLocalName())) {
                return (Element) currentChild;
            }
        }
        return null;
    }
}
