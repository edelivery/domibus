package eu.domibus.core.ebms3.receiver.handler;

import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.ebms3.model.mf.Ebms3MessageFragmentType;
import eu.domibus.api.message.validation.UserMessageValidatorSpiService;
import eu.domibus.api.model.*;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.util.TsidUtil;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.ws.attachment.AttachmentCleanupService;
import eu.domibus.core.message.UserMessagePayloadService;
import eu.domibus.core.message.dictionary.MshRoleDao;
import eu.domibus.core.multitenancy.DomainContextProviderImpl;
import eu.domibus.core.security.AuthorizationServiceImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;

import static eu.domibus.api.util.DateUtil.DEFAULT_FORMATTER;
import static eu.domibus.logging.DomibusMessageCode.BUS_MSG_RECEIVED;

/**
 * Handles the incoming AS4 UserMessages
 *
 * @author Cosmin Baciu
 * @since 4.1
 */
@Service
public class IncomingUserMessageHandler extends AbstractIncomingMessageHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(IncomingUserMessageHandler.class);

    protected final AttachmentCleanupService attachmentCleanupService;

    protected final AuthorizationServiceImpl authorizationService;

    protected final UserMessageValidatorSpiService userMessageValidatorSpiService;

    protected final MshRoleDao mshRoleDao;

    protected final UserMessagePayloadService userMessagePayloadService;

    private final TsidUtil tsidUtil;

    public IncomingUserMessageHandler(AttachmentCleanupService attachmentCleanupService,
                                      AuthorizationServiceImpl authorizationService,
                                      UserMessageValidatorSpiService userMessageValidatorSpiService,
                                      MshRoleDao mshRoleDao,
                                      UserMessagePayloadService userMessagePayloadService,
                                      TsidUtil tsidUtil) {
        this.attachmentCleanupService = attachmentCleanupService;
        this.authorizationService = authorizationService;
        this.userMessageValidatorSpiService = userMessageValidatorSpiService;
        this.mshRoleDao = mshRoleDao;
        this.userMessagePayloadService = userMessagePayloadService;
        this.tsidUtil = tsidUtil;
    }

    @Override
    protected SOAPMessage processMessage(SecurityProfile securityProfile, String pmodeKey, ProcessingType processingType, SOAPMessage request, Ebms3Messaging ebms3Messaging) throws EbMS3Exception, TransformerException, IOException, JAXBException, SOAPException {
        LOG.debug("Processing UserMessage");

        UserMessage userMessage = ebms3Converter.convertFromEbms3(ebms3Messaging.getUserMessage());

        MSHRole mshRole = MSHRole.RECEIVING;
        final MSHRoleEntity mshRoleEntity = mshRoleDao.findOrCreate(mshRole);
        userMessage.setMshRole(mshRoleEntity);

        LOG.putMDC(DomibusLogger.MDC_MESSAGE_ID, userMessage.getMessageId());
        LOG.putMDC(DomibusLogger.MDC_MESSAGE_ROLE, mshRole.name());
        LOG.putMDC(DomibusLogger.MDC_FROM, userMessage.getPartyInfo().getFromParty());
        LOG.putMDC(DomibusLogger.MDC_TO, userMessage.getPartyInfo().getToParty());
        LOG.putMDC(DomibusLogger.MDC_CONVERSATION_ID, userMessage.getConversationId());

        Ebms3MessageFragmentType ebms3MessageFragmentType = messageUtil.getMessageFragment(request);
        List<PartInfo> partInfoList = userMessagePayloadService.handlePayloads(request, ebms3Messaging, ebms3MessageFragmentType);
        partInfoList.stream().forEach(partInfo -> partInfo.setUserMessage(userMessage));

        userMessageValidatorSpiService.validate(request, userMessage, partInfoList);

        if (ebms3MessageFragmentType != null) {
            userMessage.setMessageFragment(true);
        }

        authorizationService.authorizeUserMessage(request, userMessage, securityProfile);
        final SOAPMessage response = userMessageHandlerService.handleNewUserMessage(pmodeKey, processingType, request, userMessage, ebms3MessageFragmentType, partInfoList);
        ZonedDateTime creationDateTime = tsidUtil.getZonedDateTimeFromTsid(userMessage.getEntityId());

        LOG.businessInfo(BUS_MSG_RECEIVED, userMessage.getMessageId(), userMessage.getEntityId(), creationDateTime.format(DEFAULT_FORMATTER));
        attachmentCleanupService.cleanAttachments(request);
        return response;
    }
}
