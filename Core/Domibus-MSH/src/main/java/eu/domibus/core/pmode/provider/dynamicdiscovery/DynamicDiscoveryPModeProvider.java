package eu.domibus.core.pmode.provider.dynamicdiscovery;

import eu.domibus.api.cache.DomibusLocalCacheService;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.PartyId;
import eu.domibus.api.model.PartyRole;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.lock.DomibusSynchronizationException;
import eu.domibus.api.pki.CertificateService;
import eu.domibus.api.pki.MultiDomainCryptoService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.CertificatePurpose;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.security.SecurityProfileException;
import eu.domibus.common.ErrorCode;
import eu.domibus.common.model.configuration.Process;
import eu.domibus.common.model.configuration.*;
import eu.domibus.common.model.parties.DynamicParty;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.ws.policy.PolicyService;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.core.message.MessageExchangeConfiguration;
import eu.domibus.core.message.UserMessageServiceHelper;
import eu.domibus.core.message.dictionary.PartyIdDictionaryService;
import eu.domibus.core.message.dictionary.PartyRoleDictionaryService;
import eu.domibus.core.party.DynamicPartyDao;
import eu.domibus.core.pmode.provider.CachingPModeProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.plugin.ProcessingType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.InvalidNameException;
import javax.persistence.PersistenceException;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

import static eu.domibus.api.cache.DomibusLocalCacheService.DYNAMIC_DISCOVERY_ENDPOINT;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

/* This class is used for dynamic discovery of the parties participating in a message exchange.
 *
 * Dynamic discovery is activated when the pMode is configured with a dynamic
 * process (PMode.Initiator is not set and/or PMode.Responder is not set)
 *
 * The receiver of the message must be able to accept messages from previously unknown senders.
 * This requires the receiver to have one or more P-Modes configured for all registrations ii has in the SMP.
 * Therefore for each SMP Endpoint registration of the receiver with the type attribute set to 'bdxr-transport-ebms3-as4-v1p0'
 * there MUST exist a P-Mode that can handle a message with the following attributes:
 *      Service = ancestor::ServiceMetadata/ServiceInformation/Processlist/Process/ProcessIdentifier
 *      Service/@type = ancestor::ServiceMetadata/ServiceInformation/Processlist/Process/ProcessIdentifier/@scheme
 *      Action = ancestor::ServiceMetadata/ServiceInformation/DocumentIdentifier
 *
 * The sender must be able to send messages to unknown receivers. This requires that the sender performs a lookup to find
 * out the receivers details (partyId, type, endpoint address, public certificate - to encrypt the message).
 *
 * The sender may not register, it can send a message to a registered receiver even if he (the sender) is not registered.
 * Therefore, on the receiver there is no lookup for the sender. The message is accepted based on the root CA as long as the process matches.
 */

public class DynamicDiscoveryPModeProvider extends CachingPModeProvider {

    private static final String DYNAMIC_DISCOVERY_CLIENT_SPECIFICATION = DOMIBUS_DYNAMICDISCOVERY_CLIENT_SPECIFICATION;

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DynamicDiscoveryPModeProvider.class);

    @Autowired
    protected MultiDomainCryptoService multiDomainCertificateProvider;

    @Autowired
    protected DomainContextProvider domainProvider;

    @Autowired
    @Qualifier("dynamicDiscoveryServiceOASIS")
    private DynamicDiscoveryService dynamicDiscoveryServiceOASIS;

    @Autowired
    @Qualifier("dynamicDiscoveryServicePEPPOL")
    private DynamicDiscoveryService dynamicDiscoveryServicePEPPOL;

    protected DynamicDiscoveryService dynamicDiscoveryService = null;

    @Autowired
    protected CertificateService certificateService;

    @Autowired
    DomainContextProvider domainContextProvider;

    @Autowired
    protected PartyRoleDictionaryService partyRoleDictionaryService;

    @Autowired
    protected PartyIdDictionaryService partyIdDictionaryService;

    @Autowired
    protected DomibusLocalCacheService domibusLocalCacheService;

    @Autowired
    protected UserMessageServiceHelper userMessageServiceHelper;

    @Autowired
    protected PolicyService policyService;

    @Autowired
    protected DynamicPartyDao dynamicPartyDao;

    protected Collection<eu.domibus.common.model.configuration.Process> dynamicResponderProcesses;
    protected Collection<eu.domibus.common.model.configuration.Process> dynamicInitiatorProcesses;

    // default type in eDelivery profile
    protected static final String URN_TYPE_VALUE = "urn:oasis:names:tc:ebcore:partyid-type:unregistered";
    protected static final String DEFAULT_RESPONDER_ROLE = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder";
    protected static final String MSH_ENDPOINT = "msh_endpoint";

    public DynamicDiscoveryPModeProvider(Domain domain) {
        super(domain);
    }

    @Override
    public void init() {
        load();
    }

    @Override
    protected void load() {
        super.load();

        LOG.debug("Initialising the dynamic discovery configuration.");
        dynamicResponderProcesses = findDynamicResponderProcesses();
        dynamicInitiatorProcesses = findDynamicSenderProcesses();
        if (DynamicDiscoveryClientSpecification.PEPPOL.getName().equalsIgnoreCase(domibusPropertyProvider.getProperty(DYNAMIC_DISCOVERY_CLIENT_SPECIFICATION))) {
            dynamicDiscoveryService = dynamicDiscoveryServicePEPPOL;
        } else { // OASIS client is used by default
            dynamicDiscoveryService = dynamicDiscoveryServiceOASIS;
        }
    }

    public Collection<eu.domibus.common.model.configuration.Process> getDynamicProcesses(final MSHRole mshRole) {
        // TODO investigate why the configuration is empty when these lists are initialized in the first place
        if (CollectionUtils.isEmpty(dynamicResponderProcesses) && CollectionUtils.isEmpty(dynamicInitiatorProcesses)) {
            // this is needed when the processes were not initialized in the init()
            LOG.debug("Refreshing the configuration.");
            refresh();
        }

        return MSHRole.SENDING.equals(mshRole) ? dynamicResponderProcesses : dynamicInitiatorProcesses;
    }

    protected Collection<eu.domibus.common.model.configuration.Process> findDynamicResponderProcesses() {
        final Collection<eu.domibus.common.model.configuration.Process> result = new ArrayList<>();
        for (final eu.domibus.common.model.configuration.Process process : this.doGetConfiguration().getBusinessProcesses().getProcesses()) {
            if (process.isDynamicResponder() && (process.isDynamicInitiator() || process.getInitiatorParties().contains(doGetConfiguration().getParty()))) {
                if (!process.getInitiatorParties().contains(doGetConfiguration().getParty())) {
                    throw new ConfigurationException(process + " does not contain self party " + doGetConfiguration().getParty() + " as an initiator party.");
                }
                LOG.debug("Found dynamic receiver process: " + process.getName());
                result.add(process);
            }
        }
        return result;
    }

    protected Collection<eu.domibus.common.model.configuration.Process> findDynamicSenderProcesses() {
        final Collection<eu.domibus.common.model.configuration.Process> result = new ArrayList<>();
        for (final eu.domibus.common.model.configuration.Process process : this.doGetConfiguration().getBusinessProcesses().getProcesses()) {
            if (process.isDynamicInitiator() && (process.isDynamicResponder() || process.getResponderParties().contains(doGetConfiguration().getParty()))) {
                if (!process.getResponderParties().contains(doGetConfiguration().getParty())) {
                    throw new ConfigurationException(process + " does not contain self party " + doGetConfiguration().getParty() + " as a responder party.");
                }
                LOG.debug("Found dynamic sender process: " + process.getName());
                result.add(process);
            }
        }
        return result;
    }

    /**
     * Method validates if dynamic discovery is enabled for current domain.
     *
     * @return true if domibus.dynamicdiscovery.useDynamicDiscovery is enabled for the current domain.
     */
    protected boolean useDynamicDiscovery() {
        return domibusPropertyProvider.getBooleanProperty(DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY);
    }

    /* Method finds MessageExchangeConfiguration for given user message and role. If property domibus.smlzone
     * is not defined only static search is done else (if static search did not return result) also dynamic discovery is executed.
     */
    @Override
    public MessageExchangeConfiguration findUserMessageExchangeContext(final UserMessage userMessage, final SecurityProfile securityProfile, final MSHRole mshRole, final boolean isPull, ProcessingType processingType) throws EbMS3Exception {
        return findUserMessageExchangeContextWithSecurityProfileHandling(userMessage, securityProfile, mshRole, isPull, processingType);
    }

    /* Method finds MessageExchangeConfiguration for given user message and role. If property domibus.smlzone
     * is not defined only static search is done else (if static search did not return result) also dynamic discovery is executed.
     */
    @Override
    public MessageExchangeConfiguration findUserMessageExchangeContext(final UserMessage userMessage, final MSHRole mshRole, final boolean isPull, ProcessingType processingType) throws EbMS3Exception {
        return findUserMessageExchangeContextWithSecurityProfileHandling(userMessage, null, mshRole, isPull, processingType);
    }

    private MessageExchangeConfiguration findUserMessageExchangeContextWithSecurityProfileHandling(final UserMessage userMessage, final SecurityProfile securityProfile, final MSHRole mshRole, final boolean isPull, ProcessingType processingType) throws EbMS3Exception {
        try {
            final MessageExchangeConfiguration userMessageExchangeContext =
                    securityProfile != null ? super.findUserMessageExchangeContext(userMessage, securityProfile, mshRole, isPull, processingType, true)
                            : super.findUserMessageExchangeContext(userMessage, mshRole, isPull, processingType, true);

            if (useDynamicDiscovery() && mshRole == MSHRole.SENDING) { // on the sending side only
                verifyIfReceiverPublicCertificateIsInTheTruststore(userMessage, userMessageExchangeContext);
            }
            return userMessageExchangeContext;
        } catch (final EbMS3Exception e) {
            if (useDynamicDiscovery()) {
                LOG.info((mshRole == MSHRole.SENDING ? "PmodeKey/receiver public certificate" : "PmodeKey/sender")
                        + " not found, starting the dynamic discovery process: [{}]", e.getMessage());
                doDynamicDiscovery(userMessage, mshRole);
            } else {
                LOG.error("PmodeKey/receiver public certificate not found and dynamic discovery is not enabled! Check property [{}] for current domain [{}]: [{}]", DOMIBUS_SMLZONE, domainProvider.getCurrentDomain(), e.getMessage());
                throw e;
            }
        }
        LOG.debug("Recalling findUserMessageExchangeContext after the dynamic discovery");
        return securityProfile != null ?
                super.findUserMessageExchangeContext(userMessage, securityProfile, mshRole, isPull, processingType, false)
                : super.findUserMessageExchangeContext(userMessage, mshRole, isPull, processingType, false);
    }

    protected void verifyIfReceiverPublicCertificateIsInTheTruststore(final UserMessage userMessage, final MessageExchangeConfiguration userMessageExchangeContext) throws EbMS3Exception {
        Party receiverParty = this.getPartyByName(userMessageExchangeContext.getReceiverParty());

        if (receiverParty == null) { // the receiver party was not yet added to the PMode
            LOG.debug("Receiver party [{}] not found in the PMode", userMessageExchangeContext.getReceiverParty());
            return;
        }

        if (!receiverParty.isDynamic()) {
            LOG.debug("Receiver party [{}] is not dynamic, skipping the verification of the public certificate", receiverParty.getName());
            return;
        }

        // only for dynamic parties that were added/found in the pmode:
        // the party could be discovered a while ago and is still cached in the PMode memory and found above;
        // we verify if the receiver's certificate is still in the truststore(someone could have overridden the truststore)
        LOG.info("Checking public certificate for dynamic receiver party [{}]", receiverParty.getName());

        //get the party name eg red_gw
        final String partyToNameValue = userMessageExchangeContext.getReceiverParty();
        String pModeKey = userMessageExchangeContext.getPmodeKey();
        LegConfiguration legConfiguration = getLegConfiguration(pModeKey);

        SecurityProfileConfiguration securityProfileConfiguration = null;
        try {
            securityProfileConfiguration = securityProfileProvider.getSecurityProfileConfigurationWithRSAAsDefault(domainContextProvider.getCurrentDomain(), legConfiguration.getSecurity().getSecurityProfile());
        } catch (final SecurityProfileException e) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message("No security configuration found")
                    .mshRole(MSHRole.SENDING)
                    .cause(e)
                    .build();
        }

        if (securityProfileConfiguration == null) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message("No security configuration found")
                    .mshRole(MSHRole.SENDING)
                    .build();
        }

        //if no encryption is used, we don't need to check the receiver certificate in the truststore
        if (policyService.isNoSecurityPolicy(securityProfileConfiguration) || policyService.isNoEncryptionPolicy(securityProfileConfiguration)) {
            LOG.debug("Validation if public certificate of the receiver [{}] is present in the truststore: sign only/no security policy is used", partyToNameValue);
            return;
        }

        LOG.debug("Checking if public certificate for receiver party [{}] in the truststore", partyToNameValue);

        final X509Certificate receiverCertificateFromTruststore = getCertificateFromTruststore(partyToNameValue, userMessage.getMessageId());
        if (receiverCertificateFromTruststore == null) {
            LOG.info("Could not find public certificate for receiver party [{}] in the truststore. Triggering dynamic discovery", partyToNameValue);
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0003)
                    .message("Could not find public certificate for receiver party [" + partyToNameValue + "] in the truststore. Triggering dynamic discovery")
                    .refToMessageId(userMessage.getMessageId())
                    .build();
        }
    }

    protected void doDynamicDiscovery(final UserMessage userMessage, final MSHRole mshRole) throws EbMS3Exception {
        if (MSHRole.RECEIVING.equals(mshRole)) {
            synchronizedCall(() -> addSenderParty(userMessage));
        } else {//MSHRole.SENDING
            Collection<Process> candidates = getCandidateProcesses(userMessage, mshRole);

            final DynamicDiscoveryCheckResult dynamicDiscoveryCheckResult = checkIfDynamicDiscoveryShouldBePerformed(userMessage, candidates);

            final String finalRecipientCacheKey = dynamicDiscoveryCheckResult.getFinalRecipientCacheKey();
            if (dynamicDiscoveryCheckResult.isPerformDynamicDiscovery()) {
                // do the lookup
                lookupAndUpdateConfigurationForToPartyId(finalRecipientCacheKey, userMessage);
            } else {
                LOG.debug("Skip DDC lookup and add 'To Party' to UserMessage retrieved from the cache using key [{}]", finalRecipientCacheKey);

                final Party receiverPartyFromPmode = dynamicDiscoveryCheckResult.getPmodeReceiverParty();
                final PartyId receiverPartyTo = getPartyToIdForDynamicDiscovery(receiverPartyFromPmode.getName());

                userMessage.getPartyInfo().getTo().setToPartyId(receiverPartyTo);
                if (userMessage.getPartyInfo().getTo().getToRole() == null) {
                    String responderRoleValue = dynamicDiscoveryService.getResponderRole();
                    PartyRole partyRole = partyRoleDictionaryService.findOrCreateRole(responderRoleValue);
                    userMessage.getPartyInfo().getTo().setToRole(partyRole);
                }
            }
        }
    }

    private Collection<Process> getCandidateProcesses(UserMessage userMessage, MSHRole mshRole) throws EbMS3Exception {
        Collection<Process> candidates = findDynamicCandidateProcesses(userMessage, mshRole);

        if (candidates == null || candidates.isEmpty()) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message("No matching dynamic discovery processes found for message.")
                    .refToMessageId(userMessage.getMessageId())
                    .build();
        }

        LOG.info("Found [{}] dynamic discovery candidates: [{}]. MSHRole: [{}]", candidates.size(), getProcessNames(candidates), mshRole);
        return candidates;
    }

    protected void persistAndSignal(Party configurationParty) {
        final Configuration conf = doGetConfiguration(); // from memory
        // we persist the party so that it can be retrieved on any node
        dynamicPartyDao.persistDynamicParty(conf, configurationParty);

        // signal to the nodes in cluster
        signalService.signalPModePartyAdded(configurationParty.getName());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void removePartiesAndPersist(List<String> partyNamesToDelete) {
        if (CollectionUtils.isEmpty(partyNamesToDelete)) {
            LOG.debug("No parties to delete");
            return;
        }

        LOG.info("Preparing to delete the following parties [{}] from pMode", partyNamesToDelete);
        try {
            synchronizedCall(() -> doRemovePartiesAndPersist(partyNamesToDelete));
        } catch (EbMS3Exception ignored) {
            LOG.trace("Ignored exception ", ignored);
        } catch (Exception ex) {
            LOG.error("Error occurred while removing parties and persisting the pMode", ex);
            throw new DomibusSynchronizationException("Error occurred while removing parties and persisting the pMode", ex);
        }
    }

    /**
     * Remove the provided parties from the PMode party list businessProcesses->parties and from each process initiator and responder parties
     */
    @Override
    public void removeParties(List<String> partyNames) {
        LOG.info("Preparing to remove the following  parties [{}] from pMode", partyNames);
        for (String partyName : partyNames) {
            LOG.info("Removing from the PMode the party with alias [{}]", partyName);

            removeParty(partyName);
        }
    }

    private void doRemovePartiesAndPersist(List<String> partyNamesToDelete) {
        removeParties(partyNamesToDelete);

        for (String partyName : partyNamesToDelete) {
            dynamicPartyDao.deleteByName(partyName);
        }

        LOG.info("Signaling deleted parties [{}] from pMode", partyNamesToDelete);
        signalService.signalDeletePmodeParties(partyNamesToDelete);
    }

    private void addSenderParty(UserMessage userMessage) throws EbMS3Exception {
        Collection<Process> candidates = getCandidateProcesses(userMessage, MSHRole.RECEIVING);

        LOG.info("Found [{}] dynamic discovery process candidates: [{}]. MSHRole: [{}]", candidates.size(), getProcessNames(candidates), MSHRole.RECEIVING);

        PartyId fromPartyId = getFromPartyId(userMessage);
        Party party = createOrUpdateParty(fromPartyId.getValue(), fromPartyId.getType(), null);
        updateInitiatorPartiesInPmode(candidates, party);

        //we persist the party so that it can be retrieved on any node
        persistAndSignal(party);
    }

    private List<String> getProcessNames(Collection<Process> candidates) {
        return candidates.stream().map(Process::getName).collect(Collectors.toList());
    }

    protected DynamicDiscoveryCheckResult checkIfDynamicDiscoveryShouldBePerformed(final UserMessage userMessage, Collection<eu.domibus.common.model.configuration.Process> foundProcesses) throws EbMS3Exception {
        DynamicDiscoveryCheckResult result = new DynamicDiscoveryCheckResult();
        String finalRecipientCacheKey = dynamicDiscoveryService.getFinalRecipientCacheKeyForDynamicDiscovery(userMessage);
        result.setFinalRecipientCacheKey(finalRecipientCacheKey);

        EndpointInfo endpointInfo = (EndpointInfo) domibusLocalCacheService.getEntryFromCache(DYNAMIC_DISCOVERY_ENDPOINT, finalRecipientCacheKey);
        result.setEndpointInfo(endpointInfo);

        //final recipient was not previously discovered
        if (endpointInfo == null) {
            LOG.debug("Dynamic discovery will be performed: could not find EndpointInfo in the cache for final recipient key [{}]", finalRecipientCacheKey);
            result.setPerformDynamicDiscovery(true);
            return result;
        }
        final PartyEndpointInfo partyEndpointInfo = getPartyEndpointInfo(endpointInfo, userMessage.getMessageId());
        final String partyNameToFind = partyEndpointInfo.getCertificateCn();
        final Party pmodeReceiverParty = findPartyInTheProcessResponderParties(foundProcesses, partyNameToFind);
        result.setPmodeReceiverParty(pmodeReceiverParty);

        //receiver party was not found in the Pmode receiver parties; it could be that the Pmode was overridden after the final recipient was discovered
        if (pmodeReceiverParty == null) {
            LOG.debug("Dynamic discovery will be performed: could not find Party [{}] in Pmode in none of the processes [{}]", partyNameToFind, getProcessNames(foundProcesses));
            result.setPerformDynamicDiscovery(true);
            return result;
        }

        X509Certificate receiverCertificateFromTruststore = getCertificateFromTruststore(partyNameToFind, userMessage.getMessageId());
        result.setReceiverCertificate(receiverCertificateFromTruststore);

        //the public certificate of the receiver was not found in the truststore; it could be that the truststore was overridden after the final recipient was discovered
        if (receiverCertificateFromTruststore == null) {
            LOG.debug("Dynamic discovery will be performed: could not find public certificate with alias [{}] in the truststore", partyNameToFind);
            result.setPerformDynamicDiscovery(true);
            return result;
        }
        LOG.debug("Dynamic discovery will be skipped: found all details in cache/pmode/truststore");
        result.setPerformDynamicDiscovery(false);
        return result;
    }

    protected X509Certificate getCertificateFromTruststore(String alias, String messageId) throws EbMS3Exception {
        Domain currentDomain = domainProvider.getCurrentDomain();
        try {
            return multiDomainCertificateProvider.getCertificateFromTruststore(currentDomain, alias);
        } catch (final KeyStoreException e) {
            LOG.error("Error while checking if public certificate for party [" + alias + "] is in the truststore", e);
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0003)
                    .message("Error while checking if public certificate for party [" + alias + "] is in the truststore")
                    .refToMessageId(messageId)
                    .cause(e)
                    .build();
        }
    }

    /**
     * Method lookups and updates pmode configuration and truststore
     *
     * @param cacheKey    cached key matches the key for lookup data
     * @param userMessage lookupAndUpdateConfigurationForToPartyId   - user message which triggered the dynamic discovery search
     * @throws EbMS3Exception
     */
    public void lookupAndUpdateConfigurationForToPartyId(String cacheKey, UserMessage userMessage) throws EbMS3Exception {
        //we look up in SMP based on the combination of (domain, participantId, participantIdScheme, action, serviceValue, serviceType)
        //if the lookup was previously done, it is retrieved from cache
        //cache is domain specific
        EndpointInfo endpointInfo = lookupByFinalRecipient(cacheKey, userMessage);
        LOG.debug("Found endpoint [{}]. Configuring PMode and truststore", endpointInfo.getAddress());

        //extract the party information from the Endpoint eg X509 certificate, cn, endpoint URL
        final PartyEndpointInfo partyEndpointInfo = getPartyEndpointInfo(endpointInfo, userMessage.getMessageId());

        final X509Certificate signatureX509Certificate = partyEndpointInfo.getSignatureX509Certificate();
        final String certificateCn = partyEndpointInfo.getCertificateCn();

        //we create or get the partyTo based on the certificate common name
        final PartyId receiverParty = getPartyToIdForDynamicDiscovery(certificateCn);

        //we add the partyTo in the UserMessage
        addPartyToInUserMessage(userMessage, receiverParty);

        //update party in the Pmode with the latest discovered values; synchronized
        final String partyName = receiverParty.getValue();
        final String partyType = receiverParty.getType();
        synchronizedCall(() -> addReceiverParty(userMessage, partyEndpointInfo, receiverParty));

        //save the final recipient value and URL in the database
        final String finalRecipientValue = userMessageServiceHelper.getFinalRecipientValue(userMessage);
        final String receiverURL = endpointInfo.getAddress();
        Collection<Process> processCandidates = getCandidateProcesses(userMessage, MSHRole.SENDING);
        final List<String> partyProcessNames = getProcessNames(processCandidates);

        //we add the certificate in the Domibus truststore, domain specific
        //save certificate in the truststore using synchronisation
        addCertificatesReceivedFromSmp(userMessage, signatureX509Certificate, certificateCn, endpointInfo.getCertificates());

        if (CollectionUtils.isNotEmpty(pModeEventListeners)) {
            //we call the PMode event listeners
            pModeEventListeners.stream().forEach(pModeEventListener -> {
                try {
                    LOG.debug("Notifying listener [{}] for event afterDynamicDiscoveryLookup", pModeEventListener.getName());
                    pModeEventListener.afterDynamicDiscoveryLookup(finalRecipientValue, receiverURL, partyName, partyType, partyProcessNames, certificateCn, signatureX509Certificate);
                } catch (Exception e) {
                    LOG.error("Error in PMode event listener [{}]: afterDynamicDiscoveryLookup", pModeEventListener.getName(), e);
                }
            });
        }
    }

    private void addReceiverParty(UserMessage userMessage, PartyEndpointInfo partyEndpointInfo, PartyId receiverParty) throws EbMS3Exception {
        Collection<Process> processCandidates = getCandidateProcesses(userMessage, MSHRole.SENDING);

        final String partyName = receiverParty.getValue();
        final String partyType = receiverParty.getType();

        Party party = createOrUpdateParty(partyName, partyType, partyEndpointInfo.getEndpointUrl());

        //party is added in the responder parties only if it doesn't exist; synchronized
        updateToPartyInPmodeResponderParties(processCandidates, party);

        //we persist the newly added party so that it can be retrieved on any node
        persistAndSignal(party);
    }

    /**
     * @param userMessage          User message
     * @param signatureCertificate Signature certificate
     * @param certificateCn        Common name of the signature certificate
     * @param allCertificates      In case of security profiles, it will contain the signature and encryption certificate
     * @throws EbMS3Exception
     */
    private void addCertificatesReceivedFromSmp(UserMessage userMessage,
                                                X509Certificate signatureCertificate,
                                                String certificateCn,
                                                Map<String, X509Certificate> allCertificates) throws EbMS3Exception {
        //it is important to call the findUserMessageExchangeContext from the super class so that we avoid recursion(endless loop)
        final MessageExchangeConfiguration userMessageExchangeContext = super.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, null, false);
        String pModeKey = userMessageExchangeContext.getPmodeKey();
        LegConfiguration legConfiguration = getLegConfiguration(pModeKey);
        LOG.debug("Found leg [{}] for PMode key [{}]", legConfiguration.getName(), pModeKey);


        SecurityProfileConfiguration securityProfileConfiguration = null;
        try {
            securityProfileConfiguration = securityProfileProvider.getSecurityProfileConfigurationWithRSAAsDefault(domainContextProvider.getCurrentDomain(), legConfiguration.getSecurity().getSecurityProfile());
        } catch (final SecurityProfileException e) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message("No security configuration found")
                    .mshRole(MSHRole.SENDING)
                    .cause(e)
                    .build();
        }

        if (securityProfileConfiguration.isLegacyConfiguration()) {
            String alias = securityProfileService.getCertificateAliasForPurpose(certificateCn, securityProfileConfiguration, CertificatePurpose.SIGN);
            addCertificate(alias, signatureCertificate);
        } else {
            String signatureAlias = securityProfileService.getCertificateAliasForPurpose(certificateCn, securityProfileConfiguration, CertificatePurpose.SIGN);
            addCertificate(signatureAlias, signatureCertificate);

            if (securityProfileConfiguration.isEncryptionEnabled()) {
                String encryptionAlias = securityProfileService.getCertificateAliasForPurpose(certificateCn, securityProfileConfiguration, CertificatePurpose.ENCRYPT);
                final X509Certificate encryptionCertificate = getEncryptionCertificate(allCertificates);
                if (encryptionCertificate == null) {
                    throw EbMS3ExceptionBuilder.getInstance()
                            .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                            .message("Could not find the encryption certificate")
                            .mshRole(MSHRole.SENDING)
                            .build();
                }
                addCertificate(encryptionAlias, encryptionCertificate);
            }
        }
    }

    /**
     * Method adds certificate to the store. Currently, it is not possible to move this method to CertificateService since
     * MultiDomainCryptoService contains a bean of type CertificateService which would cause a circular dependency
     *
     * @param certificate - the certificate which is added to the store
     */
    private void addCertificate(String alias, final X509Certificate certificate) {
        //we log with info level since this is a critical operation; we exclude the certificate being logged to avoid log pollution
        LOG.info("Adding public certificate with alias [{}] to the truststore", alias);
        LOG.debug("Adding public certificate [{}] with alias [{}] to the truststore", certificate, alias);

        Domain currentDomain = domainProvider.getCurrentDomain();
        boolean added = multiDomainCertificateProvider.addCertificate(currentDomain, certificate, alias, true);
        if (added) {
            LOG.info("Added public certificate [{}] with alias [{}] to the truststore for domain [{}]", certificate, alias, currentDomain);
        }
    }

    protected PartyEndpointInfo getPartyEndpointInfo(EndpointInfo endpointInfo, String messageId) throws EbMS3Exception {
        final X509Certificate signatureCertificate = getSignatureCertificate(endpointInfo.getCertificates());
        if (signatureCertificate == null) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message("Invalid endpoint metadata received from the dynamic discovery process.")
                    .refToMessageId(messageId)
                    .build();
        }
        //according to the SMP specification, we need to get the common name from the signature certificate
        final String certificateCn = getCommonNameFromCertificate(messageId, signatureCertificate);
        return new PartyEndpointInfo(certificateCn, signatureCertificate, endpointInfo.getAddress());
    }

    protected X509Certificate getSignatureCertificate(Map<String, X509Certificate> certificates) {
        if (MapUtils.isEmpty(certificates)) {
            LOG.debug("No certificates found in the list of SMP certificates");
            return null;
        }
        if (certificates.size() == 1) {
            LOG.debug("Found only one certificate in the list of SMP certificates. Getting the certificate for signature");
            return certificates.values().iterator().next();
        }

        final String certificateTypeSignature = domibusPropertyProvider.getProperty(DOMIBUS_DYNAMICDISCOVERY_SMP_CERTIFICATE_TYPE_SIGNATURE);
        final X509Certificate signatureX509Certificate = certificates.get(certificateTypeSignature);
        if (signatureX509Certificate == null) {
            LOG.error("No certificate found in the list of SMP certificates for type [{}]", certificateTypeSignature);
        }
        return signatureX509Certificate;
    }

    protected X509Certificate getEncryptionCertificate(Map<String, X509Certificate> certificates) {
        if (MapUtils.isEmpty(certificates)) {
            LOG.debug("No certificates found in the list of SMP certificates");
            return null;
        }
        if (certificates.size() == 1) {
            LOG.debug("Found only one certificate in the list of SMP certificates. Getting the certificate for encryption");
            return certificates.values().iterator().next();
        }

        final String certificateTypeEncryption = domibusPropertyProvider.getProperty(DOMIBUS_DYNAMICDISCOVERY_SMP_CERTIFICATE_TYPE_ENCRYPTION);
        final X509Certificate encryptionX509Certificate = certificates.get(certificateTypeEncryption);
        if (encryptionX509Certificate != null) {
            LOG.debug("Found encryption certificate [{}] for type [{}]", encryptionX509Certificate, certificateTypeEncryption);
            return encryptionX509Certificate;
        } else {
            LOG.debug("No certificate found in the list of SMP certificates for type [{}]", certificateTypeEncryption);
        }

        final String certificateTypeExchange = domibusPropertyProvider.getProperty(DOMIBUS_DYNAMICDISCOVERY_SMP_CERTIFICATE_TYPE_EXCHANGE);
        final X509Certificate exchangeX509Certificate = certificates.get(certificateTypeExchange);
        if (exchangeX509Certificate != null) {
            LOG.debug("Found exchange certificate [{}] for type [{}]", exchangeX509Certificate, certificateTypeExchange);
            return exchangeX509Certificate;
        } else {
            LOG.debug("No certificate found in the list of SMP certificates for type [{}]", certificateTypeExchange);
        }

        return null;
    }


    protected PartyId getFromPartyId(UserMessage userMessage) throws EbMS3Exception {
        PartyId from = null;
        String messageId = getMessageId(userMessage);
        if (userMessage != null &&
                userMessage.getPartyInfo() != null &&
                userMessage.getPartyInfo().getFrom() != null) {
            from = userMessage.getPartyInfo().getFrom().getFromPartyId();
        }
        if (from == null) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0003)
                    .message("Invalid From party identifier")
                    .refToMessageId(messageId)
                    .build();
        }

        return from;
    }

    protected String getMessageId(UserMessage userMessage) {
        if (userMessage == null) {
            return null;
        }
        return userMessage.getMessageId();
    }

    /**
     * Update party in the in-memory Pmode with the latest discovered values
     */
    protected synchronized Party createOrUpdateParty(String name, String partyType, String endpoint) {
        LOG.info("Update the configuration party with [{}] [{}] [{}]", name, partyType, endpoint);
        // get the party type from Pmode; add it if party type doesn't exist
        PartyIdType partyIdType = getOrAddPartyIdTypeInPmode(partyType);

        // search if the party exists in the pMode
        Party party = doGetConfiguration().getBusinessProcesses().getParties()
                .stream().filter(p -> StringUtils.equalsIgnoreCase(p.getName(), name))
                .findFirst().orElse(null);

        // set the new endpoint if exists, otherwise copy the old one if exists
        String newEndpoint = endpoint;
        if (newEndpoint == null) {
            newEndpoint = MSH_ENDPOINT;
            if (party != null && party.getEndpoint() != null) {
                newEndpoint = party.getEndpoint();
                LOG.debug("Setting the existing party endpoint from the Pmode [{}]", newEndpoint);
            }
        }
        LOG.debug("New endpoint is [{}]", newEndpoint);

        if (party != null) {
            LOG.debug("Party [{}] exists in the in-memory Pmode", party.getName());

            final Identifier partyIdentifier = new Identifier();
            partyIdentifier.setPartyId(name);
            partyIdentifier.setPartyIdType(partyIdType);

            party.setIdentifiers(Arrays.asList(partyIdentifier));
            party.setEndpoint(newEndpoint);
            party.setDynamic(true);
            party.setModificationTime(new Date());

            return party;
        } else {
            //add the party in the pMode
            Party newParty = buildNewDynamicParty(name, partyIdType, newEndpoint);
            LOG.debug("Add new configuration party in the in-memory Pmode [{}]", newParty.getName());
            doGetConfiguration().getBusinessProcesses().addParty(newParty);
            return newParty;
        }
    }

    @Override
    protected List<DynamicParty> onBeforePModeUpdate() {
        List<DynamicParty> dynamicParties = dynamicPartyDao.findDynamicParties();
        if (CollectionUtils.isEmpty(dynamicParties)) {
            LOG.debug("No dynamic parties found in the database before updating pmode");
            return dynamicParties;
        }

        Configuration configuration = doGetConfiguration(); // existing in-memory configuration
        dynamicParties.forEach(dynamicParty -> enrichDynamicParty(dynamicParty, configuration));

        LOG.debug("Found [{}] dynamic parties in the database before updating pmode", dynamicParties.size());
        return dynamicParties;
    }

    @Override
    protected synchronized void onAfterPModeUpdate(final List<DynamicParty> dynamicParties) {
        //take all dynamic parties re-add them to the db (including party identifiers and links to processes)
        if (CollectionUtils.isEmpty(dynamicParties)) {
            LOG.debug("No dynamic parties retrieved from the database before updating pmode");
            return;
        }

        DynamicParty dynamicParty1 = dynamicPartyDao.findDynamicParty("party1");

        LOG.debug("[{}] dynamic parties to be re-added after updating the pmode", dynamicParties.size());

        // add the dynamic parties to the in-memory pmode:
        List<Party> addedParties = dynamicParties.stream()
                .map(dynamicParty -> addDynamicParty(dynamicParty))
                .collect(Collectors.toList());

        final Configuration conf = doGetConfiguration(); // from memory
        try {
            // add the dynamic parties to the db:
            addedParties.forEach(addedParty -> dynamicPartyDao.persistDynamicParty(conf, addedParty));
        } catch (PersistenceException | DataIntegrityViolationException e) {
            LOG.warn("Could not persist dynamic parties [{}]", dynamicParties, e);
        }
    }


    @Override
    public void addAlreadyPersistedParty(String partyName) {
        DynamicParty dynamicParty = dynamicPartyDao.findDynamicParty(partyName);
        if (dynamicParty == null) {
            LOG.warn("Dynamic party [{}] is no longer present in the database", partyName);
            return;
        }

        Configuration configuration = doGetConfiguration(); // existing in-memory configuration
        enrichDynamicParty(dynamicParty, configuration);

        addDynamicParty(dynamicParty);
    }

    /**
     * Add dynamic party to the in-memory pmode
     */
    private Party addDynamicParty(DynamicParty dynamicParty) {
        Configuration configuration = doGetConfiguration(); // existing in-memory configuration

        Party configurationParty = createOrUpdateParty(dynamicParty.getName(), dynamicParty.getIdentifiers().get(0).getPartyIdTypeValue(), dynamicParty.getEndpoint());

        // update the initiator and responder parties in the in-memory processes
        dynamicParty.getProcessNamesWherePartyIsInitiator().forEach(processName -> {
            Optional<Process> destProc = getProcessByName(configuration.getBusinessProcesses(), processName);
            destProc.ifPresent(process -> {
                if (partyIsInitiator(configurationParty, process)) {
                    LOG.debug("Party [{}] is already an initiator for process [{}]: no action", configurationParty, process);
                } else {
                    process.getInitiatorParties().add(configurationParty);
                }
            });
        });
        dynamicParty.getProcessNamesWherePartyIsResponder().forEach(processName -> {
            Optional<Process> destProc = getProcessByName(configuration.getBusinessProcesses(), processName);
            destProc.ifPresent(process -> {
                if (partyIsResponder(configurationParty, process)) {
                    LOG.debug("Party [{}] is already a responder for process [{}]: no action", configurationParty, process);
                } else {
                    process.getResponderParties().add(configurationParty);
                }
            });
        });

        return configurationParty;
    }

    private void enrichDynamicParty(DynamicParty dynamicParty, Configuration configuration) {
        // enrich dynamic parties with names of linked processes:
        List<String> initiatorProcessNames = configuration.getBusinessProcesses().getProcesses().stream()
                .filter(process -> process.getInitiatorParties().stream()
                        .anyMatch(p -> p.getName().equalsIgnoreCase(dynamicParty.getName())) || dynamicParty.isInitiatorForProcess(process))
                .map(Process::getName)
                .collect(Collectors.toList());
        dynamicParty.setProcessNamesWherePartyIsInitiator(initiatorProcessNames);

        List<String> responderProcessNames = configuration.getBusinessProcesses().getProcesses().stream()
                .filter(process -> process.getResponderParties().stream()
                        .anyMatch(p -> p.getName().equalsIgnoreCase(dynamicParty.getName())) || dynamicParty.isResponderForProcess(process))
                .map(Process::getName)
                .collect(Collectors.toList());
        dynamicParty.setProcessNamesWherePartyIsResponder(responderProcessNames);

        // enrich identifiers with the value of the partyIdType:
        dynamicParty.getIdentifiers().forEach(identifier -> {
            String partyIdTypeValue = configuration.getBusinessProcesses().getPartyIdTypes().stream()
                    .filter(partyIdType -> partyIdType.getEntityId() == identifier.getPartyIdType())
                    .findFirst().get().getValue();
            identifier.setPartyIdTypeValue(partyIdTypeValue);
        });
    }

    private Optional<Process> getProcessByName(BusinessProcesses processes, String processName) {
        return processes.getProcesses().stream()
                .filter(process -> StringUtils.equalsIgnoreCase(process.getName(), processName))
                .findAny();
    }

    private boolean partyIsInitiator(Party newParty, Process process) {
        return process.getInitiatorParties().stream().anyMatch(p -> p.getName().equals(newParty.getName()));
    }

    private boolean partyIsResponder(Party newParty, Process process) {
        return process.getInitiatorParties().stream().anyMatch(p -> p.getName().equals(newParty.getName()));
    }

    protected Party buildNewDynamicParty(String name, PartyIdType configurationType, String endpoint) {
        Party newConfigurationParty = new Party();
        final Identifier partyIdentifier = new Identifier();
        partyIdentifier.setPartyId(name);
        partyIdentifier.setPartyIdType(configurationType);

        newConfigurationParty.setName(name);
        newConfigurationParty.getIdentifiers().add(partyIdentifier);
        newConfigurationParty.setEndpoint(endpoint);

        newConfigurationParty.setDynamic(true);
        newConfigurationParty.setCreationTime(new Date());

        return newConfigurationParty;
    }

    protected PartyIdType getOrAddPartyIdTypeInPmode(String partyType) {
        BusinessProcesses businessProcesses = doGetConfiguration().getBusinessProcesses();
        Set<PartyIdType> partyIdTypes = businessProcesses.getPartyIdTypes();
        if (partyIdTypes == null) {
            LOG.info("Empty partyIdTypes set");
            partyIdTypes = new HashSet<>();
        }

        PartyIdType partyIdType = null;
        for (final PartyIdType t : partyIdTypes) {
            if (StringUtils.equalsIgnoreCase(t.getValue(), partyType)) {
                LOG.debug("PartyIdType exists in the pmode [{}]", partyType);
                partyIdType = t;
            }
        }
        // add to partyIdType list
        if (partyIdType == null) {
            LOG.debug("Add new PartyIdType [{}]", partyType);
            partyIdType = new PartyIdType();
            partyIdType.setName(partyType);
            partyIdType.setValue(partyType);
            partyIdTypes.add(partyIdType);
            businessProcesses.setPartyIdTypes(partyIdTypes);

            this.dynamicPartyDao.persistDynamicPartyIdType(businessProcesses, partyIdType);
        }
        return partyIdType;
    }

    //party is added in the responder parties only if it doesn't exist
    protected synchronized void updateToPartyInPmodeResponderParties(Collection<eu.domibus.common.model.configuration.Process> candidates, Party configurationParty) {
        LOG.debug("Update Pmode processes->responderParties with party [{}]", configurationParty.getName());

        for (final Process candidate : candidates) {
            final Party responderParty = findResponderPartyInProcess(candidate, configurationParty.getName());
            if (responderParty == null) {
                LOG.info("Adding party [{}] in the process responder parties of candidate process [{}]", configurationParty.getName(), candidate.getName());
                candidate.getResponderParties().add(configurationParty);
            }
        }
    }

    protected Party findPartyInTheProcessResponderParties(Collection<eu.domibus.common.model.configuration.Process> candidates, String partyName) {
        for (final Process candidate : candidates) {
            final Party responderParty = findResponderPartyInProcess(candidate, partyName);
            if (responderParty != null) {
                LOG.info("Found existing party [{}] in the process responder parties [{}]", partyName, candidate.getName());
                return responderParty;
            }
        }
        return null;
    }

    protected Party findResponderPartyInProcess(eu.domibus.common.model.configuration.Process process, String partyName) {
        for (final Party party : process.getResponderParties()) {
            if (StringUtils.equalsIgnoreCase(partyName, party.getName())) {
                LOG.debug("Party [{}] found as responder in process [{}]", partyName, process.getName());
                return party;
            }
        }

        return null;
    }

    protected synchronized void updateInitiatorPartiesInPmode(Collection<eu.domibus.common.model.configuration.Process> candidates, Party configurationParty) {
        LOG.debug("Update InitiatorParties in Pmode with party [{}] for [{}] candidate processes", configurationParty.getName(), candidates.size());
        for (final Process candidate : candidates) {
            boolean partyFound = false;
            for (final Party party : candidate.getInitiatorParties()) {
                if (StringUtils.equalsIgnoreCase(configurationParty.getName(), party.getName())) {
                    partyFound = true;
                    LOG.debug("Party [{}] already found as initiator in candidate process [{}]", party.getName(), candidate.getName());
                    break;
                }
            }
            if (!partyFound) {
                candidate.getInitiatorParties().add(configurationParty);
                LOG.info("Party [{}] added as initiator in candidate process [{}]", configurationParty.getName(), candidate.getName());
            }
        }
    }

    /**
     * Set partyTo in UserMessage
     */
    protected void addPartyToInUserMessage(UserMessage userMessage, PartyId receiverParty) {
        LOG.debug("Adding partyTo in the UserMessage [{}]", receiverParty);

        userMessage.getPartyInfo().getTo().setToPartyId(receiverParty);
        if (userMessage.getPartyInfo().getTo().getToRole() == null) {
            String responderRoleValue = dynamicDiscoveryService.getResponderRole();
            LOG.debug("Adding partyTo role in the UserMessage [{}]", responderRoleValue);
            PartyRole partyRole = partyRoleDictionaryService.findOrCreateRole(responderRoleValue);
            userMessage.getPartyInfo().getTo().setToRole(partyRole);
        }
    }

    /**
     * It creates or gets the receiver party based on the certificate common name
     */
    private PartyId getPartyToIdForDynamicDiscovery(String certificateCn) {
        String type = dynamicDiscoveryService.getPartyIdType();
        LOG.debug("DDC: using configured party type [{}]", type);

        // double check not to add empty value as a type
        // because it is invalid by the oasis messaging  xsd
        if (StringUtils.isEmpty(type)) {
            type = null;
        }

        final PartyId receiverParty = partyIdDictionaryService.findOrCreateParty(certificateCn, type);
        return receiverParty;
    }

    private String getCommonNameFromCertificate(String messageId, X509Certificate certificate) throws EbMS3Exception {
        try {
            //parse certificate for common name = toPartyId
            String cn = certificateService.extractCommonName(certificate);
            LOG.debug("Extracted the common name [{}]", cn);
            return cn;
        } catch (final InvalidNameException e) {
            LOG.error("Error while extracting CommonName from certificate", e);
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0003)
                    .message("Error while extracting CommonName from certificate")
                    .refToMessageId(messageId)
                    .cause(e)
                    .build();
        }
    }

    protected EndpointInfo lookupByFinalRecipient(String lookupCacheKey, UserMessage userMessage) throws EbMS3Exception {
        final String finalRecipientValue = userMessageServiceHelper.getFinalRecipientValue(userMessage);
        final String finalRecipientType = userMessageServiceHelper.getFinalRecipientType(userMessage);

        if (StringUtils.isBlank(finalRecipientValue)) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message("Dynamic discovery processes found for message but finalRecipient information is missing in messageProperties.")
                    .refToMessageId(userMessage.getMessageId())
                    .build();
        }
        LOG.info("Perform lookup by finalRecipient type [{}] and value [{}]", finalRecipientType, finalRecipientValue);

        //lookup sml/smp - result is cached
        final EndpointInfo endpoint = dynamicDiscoveryService.lookupInformation(lookupCacheKey, finalRecipientValue,
                finalRecipientType,
                userMessage.getActionValue(),
                userMessage.getService().getValue(),
                userMessage.getService().getType());

        // The SMP entries missing this info are not for the use of Domibus
        if (StringUtils.isEmpty(endpoint.getAddress()) || MapUtils.isEmpty(endpoint.getCertificates())) {
            throw EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message("Invalid endpoint metadata received from the dynamic discovery process.")
                    .refToMessageId(userMessage.getMessageId())
                    .build();
        }
        LOG.debug("Lookup successful: " + endpoint.getAddress());
        return endpoint;
    }

    /*
     * Check all dynamic processes to find candidates for dynamic discovery lookup.
     */
    protected Collection<eu.domibus.common.model.configuration.Process> findDynamicCandidateProcesses(UserMessage userMessage, final MSHRole mshRole) {
        LOG.debug("Finding dynamic candidate processes for msh role [{}]", mshRole);
        Collection<eu.domibus.common.model.configuration.Process> candidates = new HashSet<>();
        Collection<eu.domibus.common.model.configuration.Process> processes = getDynamicProcesses(mshRole);
        LOG.debug("[{}] dynamic processes for msh role [{}]", CollectionUtils.size(processes), mshRole);

        for (final Process process : processes) {
            if (matchDynamicProcess(process, mshRole)) {
                LOG.debug("Dynamic process matched: [{}] [{}]", process.getName(), mshRole);
                for (final LegConfiguration legConfiguration : process.getLegs()) {
                    if (StringUtils.equalsIgnoreCase(legConfiguration.getService().getValue(), userMessage.getService().getValue()) &&
                            StringUtils.equalsIgnoreCase(legConfiguration.getAction().getValue(), userMessage.getActionValue())) {
                        LOG.debug("Leg [{}] of process [{}] matched, adding process.", legConfiguration.getName(), process.getName());
                        candidates.add(process); // this is a set, so the process is only added once, event if multiple legs happen to match
                    }
                }
            }
        }

        return candidates;
    }

    /*
     * On the receiving, the initiator is unknown, on the sending side the responder is unknown.
     */
    protected boolean matchDynamicProcess(final Process process, MSHRole mshRole) {
        if (MSHRole.RECEIVING.equals(mshRole)) {
            return process.isDynamicInitiator() || process.getInitiatorParties().contains(this.doGetConfiguration().getParty());
        } else { // MSHRole.SENDING
            return process.isDynamicResponder() || process.getResponderParties().contains(this.doGetConfiguration().getParty());
        }
    }


}
