package eu.domibus.core.crypto;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.pki.CertificateAlgoType;
import eu.domibus.api.pki.SecurityProfileService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.CertificateException;
import eu.domibus.api.security.CertificatePurpose;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY;

/**
 * Validates keystore and truststore certificates
 *
 * @author Lucian FURCA
 * @since 5.1
 */
@Service
public class SecurityProfileValidatorService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SecurityProfileValidatorService.class);

    protected final DomibusPropertyProvider domibusPropertyProvider;
    protected final SecurityProfileService securityProfileService;
    protected final SecurityProfileHelper securityProfileHelper;

    public SecurityProfileValidatorService(DomibusPropertyProvider domibusPropertyProvider,
                                           SecurityProfileService securityProfileService,
                                           SecurityProfileHelper securityProfileHelper) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.securityProfileService = securityProfileService;
        this.securityProfileHelper = securityProfileHelper;
    }

    /**
     * Parses all the SecurityProfiles configurations defined in domibus.properties, checks that for each of them an alias
     * is present in the store(keystore/truststore) and validates that it's type is correct
     *
     * @param securityProfileAliasConfigurations the Security Profile configurations list for all the aliases defined in domibus.properties
     * @param store                              the domain's store(keystore/truststore)
     * @param storeType                          the type of store: keystore/truststore
     */
    public void validateConfiguredCertificates(List<SecurityProfileConfiguration> securityProfileAliasConfigurations, KeyStore store, StoreType storeType) {
        securityProfileAliasConfigurations.forEach(
                securityProfileAliasConfiguration -> checkIfConfiguredCertificateIsPresentInTheStoreAndIfIsItValid(securityProfileAliasConfiguration, store, storeType)
        );
    }

    private void checkIfConfiguredCertificateIsPresentInTheStoreAndIfIsItValid(SecurityProfileConfiguration securityProfileAliasConfiguration, KeyStore store, StoreType storeType) {
        if (BooleanUtils.isTrue(domibusPropertyProvider.getBooleanProperty(DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY))) {
            LOG.debug("Security Profile certificate aliases are not currently validated in dynamic discovery mode");
            return;
        }

        if (securityProfileAliasConfiguration.isSignatureEnabled()) {
            String signatureAlias = securityProfileAliasConfiguration.getSignaturePrivateKeyAlias();
            doCheckIfConfiguredCertificateIsPresentInTheStoreAndIfItIsValid(securityProfileAliasConfiguration, store, storeType, signatureAlias);
        }
        if (securityProfileAliasConfiguration.isEncryptionEnabled()) {
            String decryptionPrivateKeyAlias = securityProfileAliasConfiguration.getEncryptionPrivateKeyAlias();
            doCheckIfConfiguredCertificateIsPresentInTheStoreAndIfItIsValid(securityProfileAliasConfiguration, store, storeType, decryptionPrivateKeyAlias);

        }
    }

    private void doCheckIfConfiguredCertificateIsPresentInTheStoreAndIfItIsValid(SecurityProfileConfiguration securityProfileAliasConfiguration, KeyStore store, StoreType storeType, String signatureAlias) {
        SecurityProfile securityProfile = securityProfileAliasConfiguration.getSecurityProfile();
        X509Certificate certificate;
        try {
            certificate = (X509Certificate) store.getCertificate(signatureAlias);
        } catch (KeyStoreException e) {
            String exceptionMessage = String.format("Error getting alias [%s] from keystore with type [%s]: exception: %s", signatureAlias, storeType, e.getMessage());
            throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }
        if (certificate == null) {
            String exceptionMessage = String.format("Alias [%s] which is defined in the properties file does not exist in the [%s]", signatureAlias, storeType);
            throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }

        if (securityProfileAliasConfiguration.isLegacyConfiguration()) {
            validateLegacyAliasCertificateType(certificate.getPublicKey().getAlgorithm(), signatureAlias, storeType);
        } else {
            validateCertificateType(signatureAlias, certificate, securityProfile, storeType);
        }
    }

    /**
     * Extracts the CertificatePurpose from the given alias
     *
     * @param alias the alias from the store
     * @return the CertificatePurpose name extracted from the alias definition
     * @throws CertificateException if the certificate purpose can't be extracted from the alias
     */
    private CertificatePurpose getCertificatePurposeFromAlias(String alias) {
        if (securityProfileHelper.areSecurityProfilesDisabled()) {
            return null;
        }
        CertificatePurpose certificatePurpose = securityProfileService.extractCertificatePurpose(alias);
        if (certificatePurpose == null) {
            String exceptionMessage = String.format("Could not extract certificate purpose from alias [%s]. Available purposes are sign, encrypt, decrypt", alias);
            throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }
        return certificatePurpose;
    }

    private void validateCertificateType(String alias, X509Certificate certificate, SecurityProfile securityProfile, StoreType storeType) {
        final String publicKeyAlgorithm = certificate.getPublicKey().getAlgorithm().toUpperCase();
        String certificateAlgorithm = CertificateAlgoType.lookupAlgoIdByAlgoName(publicKeyAlgorithm).getCertificateAlgoType();
        if (certificateAlgorithm == null) {
            String exceptionMessage = String.format("Algorithm for certificate with alias [%s] in [%s] used in security profile: [%s] is null",
                    alias, storeType, securityProfile);
            throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }
        CertificatePurpose certificatePurpose = getCertificatePurposeFromAlias(alias);
        if (securityProfile == null || certificatePurpose == null) {
            validateLegacyAliasCertificateType(certificateAlgorithm, alias, storeType);
            return;
        }
        switch (certificatePurpose) {
            case ENCRYPT:
            case DECRYPT:
                validateDecryptionCertificateType(securityProfile, certificateAlgorithm, certificatePurpose, alias, storeType);
                break;
            case SIGN:
                validateSigningCertificateType(securityProfile, certificateAlgorithm, certificatePurpose, alias, storeType);
                break;
            default:
                String exceptionMessage = String.format("Invalid naming of alias [%s], it should end with sign or decrypt", alias);
                throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }
    }

    private void validateLegacyAliasCertificateType(String certificateAlgorithm, String alias, StoreType storeType) {
        if (!securityProfileHelper.areSecurityProfilesDisabled()) {
            String exceptionMessage = String.format("Legacy keystore alias [%s] is not defined in [%s]", alias, storeType);
            throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }
        if (!certificateAlgorithm.equalsIgnoreCase(CertificateAlgoType.RSA.getCertificateAlgoType())) {
            String exceptionMessage = String.format("Invalid certificate type with alias: [%s] defined in [%s]", alias, storeType);
            throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }
    }

    private void validateDecryptionCertificateType(SecurityProfile securityProfile, String certificateAlgorithm,
                                                   CertificatePurpose certificatePurpose, String alias, StoreType storeType) {
        List<String> acceptedCertificateAlgorithms = new ArrayList<>();

        String propertyNameCertificateDecryptType = securityProfileHelper.getSecurityProfilePropertyName(securityProfile.getCode(), "key.private.decrypt.type");
        final String decryptTypeCommaSeparated = domibusPropertyProvider.getProperty(propertyNameCertificateDecryptType);
        if (StringUtils.isEmpty(decryptTypeCommaSeparated)) {
            LOG.debug("No decrypt certificate types are defined for profile [{}], skipping validation", securityProfile.getCode());
            return;
        }
        acceptedCertificateAlgorithms.addAll(Arrays.asList(StringUtils.split(decryptTypeCommaSeparated, ",")));
        checkCertificateType(acceptedCertificateAlgorithms, certificateAlgorithm, certificatePurpose, alias, securityProfile, storeType);
    }

    private void validateSigningCertificateType(SecurityProfile securityProfile, String certificateAlgorithm,
                                                CertificatePurpose certificatePurpose, String alias, StoreType storeType) {
        List<String> acceptedCertificateAlgorithms = new ArrayList<>();

        String propertyNameCertificateSignType = securityProfileHelper.getSecurityProfilePropertyName(securityProfile.getCode(), "key.private.sign.type");
        final String signTypeCommaSeparated = domibusPropertyProvider.getProperty(propertyNameCertificateSignType);
        if (StringUtils.isEmpty(signTypeCommaSeparated)) {
            LOG.debug("No signature certificate types are defined for profile [{}], skipping validation", securityProfile.getCode());
            return;
        }
        acceptedCertificateAlgorithms.addAll(Arrays.asList(StringUtils.split(signTypeCommaSeparated, ",")));
        checkCertificateType(acceptedCertificateAlgorithms, certificateAlgorithm, certificatePurpose, alias, securityProfile, storeType);
    }

    private void checkCertificateType(List<String> acceptedCertificateAlgorithms, String certificateAlgorithm, CertificatePurpose certificatePurpose,
                                      String alias, SecurityProfile securityProfile, StoreType storeType) {
        if (certificateAlgorithm == null) {
            String exceptionMessage = String.format("Certificate algorithm in [%s] for alias: [%s] used in security profile: [%s] is null",
                    storeType, alias, securityProfile);
            throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }
        boolean certificateTypeIsAccepted = acceptedCertificateAlgorithms.stream().anyMatch(certificateAlgorithm::equalsIgnoreCase);
        if (!certificateTypeIsAccepted) {
            String exceptionMessage = String.format("Invalid [%s] certificate type in [%s] with alias: [%s] used in security profile: [%s]",
                    certificatePurpose, storeType, alias, securityProfile);
            throw new CertificateException(DomibusCoreErrorCode.DOM_005, exceptionMessage);
        }
    }
}
