package eu.domibus.core.message;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import eu.domibus.api.crypto.CryptoException;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.MessageStatus;
import eu.domibus.api.model.MessageStatusEntity;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.*;
import eu.domibus.api.pmode.PModeException;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.pull.PullRequestSenderService;
import eu.domibus.api.security.CertificatePurpose;
import eu.domibus.api.security.ChainCertificateInvalidException;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.usermessage.UserMessageService;
import eu.domibus.common.model.configuration.Process;
import eu.domibus.common.model.configuration.*;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.ws.policy.PolicyService;
import eu.domibus.core.generator.id.MessageIdGenerator;
import eu.domibus.core.message.pull.MpcService;
import eu.domibus.core.message.pull.PullContext;
import eu.domibus.core.message.pull.PullMessageService;
import eu.domibus.core.message.pull.PullProcessValidator;
import eu.domibus.core.metrics.Counter;
import eu.domibus.core.metrics.Timer;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.plugin.ProcessingType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_RECEIVER_CERTIFICATE_VALIDATION_ONSENDING;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SENDER_CERTIFICATE_VALIDATION_ONSENDING;

/**
 * @author Thomas Dussart
 * @since 3.3
 * {@inheritDoc}
 */

@Service
public class MessageExchangeServiceImpl implements MessageExchangeService, PullRequestSenderService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageExchangeServiceImpl.class);

    private final UserMessageService userMessageService;

    private final PullProcessValidator pullProcessValidator;

    private final PModeProvider pModeProvider;

    private final PolicyService policyService;

    protected final MultiDomainCryptoService multiDomainCertificateProvider;

    protected final CertificateService certificateService;

    protected final DomainContextProvider domainProvider;

    protected final DomibusPropertyProvider domibusPropertyProvider;

    private final PullMessageService pullMessageService;

    private final MpcService mpcService;

    protected final MessageStatusDao messageStatusDao;

    protected final MessageIdGenerator messageIdGenerator;

    protected final SecurityProfileService securityProfileService;

    protected SecurityProfileProvider securityProfileProvider;

    public MessageExchangeServiceImpl(PModeProvider pModeProvider,
                                      UserMessageService userMessageService,
                                      PullProcessValidator pullProcessValidator,
                                      PolicyService policyService,
                                      MultiDomainCryptoService multiDomainCertificateProvider,
                                      CertificateService certificateService,
                                      MessageIdGenerator messageIdGenerator,
                                      DomainContextProvider domainProvider,
                                      DomibusPropertyProvider domibusPropertyProvider,
                                      SecurityProfileService securityProfileService,
                                      PullMessageService pullMessageService,
                                      MpcService mpcService,
                                      MessageStatusDao messageStatusDao,
                                      SecurityProfileProvider securityProfileProvider) {
        this.pModeProvider = pModeProvider;
        this.userMessageService = userMessageService;
        this.pullProcessValidator = pullProcessValidator;
        this.policyService = policyService;
        this.multiDomainCertificateProvider = multiDomainCertificateProvider;
        this.certificateService = certificateService;
        this.messageIdGenerator = messageIdGenerator;
        this.domainProvider = domainProvider;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.securityProfileService = securityProfileService;
        this.pullMessageService = pullMessageService;
        this.mpcService = mpcService;
        this.messageStatusDao = messageStatusDao;
        this.securityProfileProvider = securityProfileProvider;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageStatusEntity getMessageStatus(final MessageExchangeConfiguration messageExchangeConfiguration, ProcessingType processingType) {
        MessageStatus messageStatus = MessageStatus.SEND_ENQUEUED;
        if (ProcessingType.PULL.equals(processingType)) {
            List<Process> processes = pModeProvider.findPullProcessesByMessageContext(messageExchangeConfiguration);
            pullProcessValidator.validatePullProcess(Lists.newArrayList(processes));
            messageStatus = MessageStatus.READY_TO_PULL;
        }
        return messageStatusDao.findOrCreate(messageStatus);
    }

    @Override
    public MessageStatusEntity getMessageStatus(final MessageExchangeConfiguration messageExchangeConfiguration) {
        MessageStatus messageStatus = MessageStatus.SEND_ENQUEUED;
        List<Process> processes = pModeProvider.findPullProcessesByMessageContext(messageExchangeConfiguration);
        if (!processes.isEmpty()) {
            pullProcessValidator.validatePullProcess(Lists.newArrayList(processes));
            messageStatus = MessageStatus.READY_TO_PULL;
        } else {
            LOG.debug("No pull process found for message configuration");
        }
        return messageStatusDao.findOrCreate(messageStatus);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageStatusEntity getMessageStatusForPush() {
        return messageStatusDao.findOrCreate(MessageStatus.SEND_ENQUEUED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public MessageStatusEntity retrieveMessageRestoreStatus(final String messageId, MSHRole role) {
        final UserMessage userMessage = userMessageService.getByMessageId(messageId, role);
        try {
            if (pullMessageService.isPull(userMessage)) {
                return messageStatusDao.findOrCreate(MessageStatus.READY_TO_PULL);
            }
            MessageExchangeConfiguration userMessageExchangeContext = pModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING);
            return getMessageStatus(userMessageExchangeContext);
        } catch (EbMS3Exception e) {
            throw new PModeException(DomibusCoreErrorCode.DOM_001, "Could not get the PMode key for message [" + messageId + "]", e);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    @Timer(clazz = MessageExchangeServiceImpl.class, value = "initiatePullRequest")
    @Counter(clazz = MessageExchangeServiceImpl.class, value = "initiatePullRequest")
    public void initiatePullRequest() {
        initiatePullRequest(null);
    }

    public void initiatePullRequest(String legName, String responder) {
        if (!pModeProvider.isConfigurationLoaded()) {
            throw new PModeException(DomibusCoreErrorCode.DOM_003, "No pmode configuration found");
        }
        Party initiator = pModeProvider.getGatewayParty();
        final LegConfiguration legConfigurationByLegName = pModeProvider.getLegConfigurationByLegName(legName);
        if (legConfigurationByLegName == null) {
            throw new PModeException(DomibusCoreErrorCode.DOM_003, "No leg configuration with name [" + legName + "] found ");
        }
        final Party partyByName = pModeProvider.getPartyByName(responder);
        if (partyByName == null) {
            throw new PModeException(DomibusCoreErrorCode.DOM_003, "Could not find responder party with name [" + responder + "] found ");
        }
        sendPullRequestForMpc(initiator, partyByName, legConfigurationByLegName);
    }

    private void sendPullRequestForMpc(Party initiator, Party responder, LegConfiguration legConfiguration) {
        MessageExchangeConfiguration messageExchangeConfiguration = new MessageExchangeConfiguration(null,
                responder.getName(),
                initiator.getName(),
                legConfiguration.getService().getName(),
                legConfiguration.getAction().getName(),
                legConfiguration.getName());
        LOG.debug("sendPullRequestForMpc messageExchangeConfiguration:[{}]", messageExchangeConfiguration);
        String mpcName = legConfiguration.getDefaultMpc().getName();

        String mpcQualifiedName = legConfiguration.getDefaultMpc().getQualifiedName();
        LOG.debug("Sending pull request for mpcFQN:[{}] to mpc:[{}]", mpcQualifiedName, mpcName);

        pullMessageService.sendPullMessage(legConfiguration, messageExchangeConfiguration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initiatePullRequest(final String mpc) {
        if (!pModeProvider.isConfigurationLoaded()) {
            LOG.debug("A configuration problem occurred while initiating the pull request. Probably no configuration is loaded.");
            return;
        }
        Party initiator = pModeProvider.getGatewayParty();
        List<Process> pullProcesses = pModeProvider.findPullProcessesByInitiator(initiator);
        LOG.trace("Initiating pull requests:");
        if (pullProcesses.isEmpty()) {
            LOG.trace("No pull process configured !");
            return;
        }

        //TODO fix getValidProcesses
        final List<Process> validPullProcesses = getValidProcesses(pullProcesses);

        pullMessageService.addMpcToFrequencyHelper(validPullProcesses);

        if (pullMessageService.pausePullingSystem()) {
            return;
        }

        initiateSendPullRequestForMpc(validPullProcesses, mpc, initiator);
    }

    private void initiateSendPullRequestForMpc(final List<Process> validPullProcesses, String mpc, Party initiator) {
        validPullProcesses.forEach(pullProcess -> {
            initiateSendPullRequestForProcessAndMpc(mpc, initiator, pullProcess);
        });
    }

    private void initiateSendPullRequestForProcessAndMpc(String mpc, Party initiator, Process pullProcess) {
        //TODO [EDELIVERY-12876] PULL refactoring mpc is not taken into account when getting the the legs

        Set<LegConfiguration> legConfigurationsForSendingPullRequest = getLegConfigurationsForSendingPullRequest(pullProcess.getLegs());
        if (LOG.isDebugEnabled()) {
            LOG.debug("Preparing pull requests for pull process [{}] - [{}] distinct mpcs found: [{}], chosen legs: [{}]",
                    pullProcess.getName(), legConfigurationsForSendingPullRequest.size(),
                    legConfigurationsForSendingPullRequest.stream().map(LegConfiguration::getDefaultMpc).map(Mpc::getName).collect(Collectors.joining(",")),
                    legConfigurationsForSendingPullRequest.stream().map(LegConfiguration::getName).collect(Collectors.joining(",")));
        }
        legConfigurationsForSendingPullRequest.forEach(legConfiguration -> {
            LOG.debug("Pull request initiated for mpc [{}] and party [{}] having Leg configuration [{}] with default mpc [{}] and security [{}]",
                    mpc, initiator, legConfiguration.getName(), legConfiguration.getDefaultMpc().getName(), legConfiguration.getSecurity().getName());
            sendPullRequestForMpc(mpc, initiator, pullProcess, legConfiguration);
        });
    }

    private Set<LegConfiguration> getLegConfigurationsForSendingPullRequest(Set<LegConfiguration> legs) {
        Set<LegConfiguration> result = new HashSet<>();

        final Map<String, List<LegConfiguration>> legConfigurationsToMapByMpcAndSecurityProfile = legConfigurationsToMapByMpcAndSecurityProfile(legs);
        legConfigurationsToMapByMpcAndSecurityProfile.entrySet().stream().forEach(entry -> {
            List<LegConfiguration> legConfigurations = entry.getValue();
            if (legConfigurations.size() > 1) {
                LOG.info("More than one leg configuration found for mpc [{}] with different security profiles [{}].", entry.getKey(), entry.getValue());
                final SecurityProfile securityProfileWithHighestPriority = securityProfileProvider.getSecurityProfileWithHighestPriority(domainProvider.getCurrentDomain());
                final LegConfiguration legConfigurationWithTheHighestSecurityProfilePriority = getLegConfigurationWithTheHighestSecurityProfilePriority(legConfigurations, securityProfileWithHighestPriority);
                LOG.debug("Adding leg configuration [{}] with the highest security profile [{}] to the list of leg configurations to send the pull request.", legConfigurationWithTheHighestSecurityProfilePriority, securityProfileWithHighestPriority);
                result.add(legConfigurationWithTheHighestSecurityProfilePriority);
            } else if (legConfigurations.size() == 1) {
                final LegConfiguration legConfiguration = legConfigurations.get(0);
                LOG.debug("Adding leg configuration [{}] to the list of leg configurations to send the pull request.", legConfiguration);
                result.add(legConfiguration);
            } else {
                LOG.warn("No leg configuration found for mpc [{}].", entry.getKey());
            }
        });

        return result;
    }

    private LegConfiguration getLegConfigurationWithTheHighestSecurityProfilePriority(List<LegConfiguration> legConfigurations, SecurityProfile securityProfileWithHighestPriority) {
        return legConfigurations.stream()
                .filter(legConfiguration -> StringUtils.equalsIgnoreCase(legConfiguration.getSecurity().getSecurityProfile(), securityProfileWithHighestPriority.getCode()))
                .findFirst().orElse(null);
    }

    /**
     * This method returns a map with the key being the mpcQualifiedName and the value being a list of leg configurations that have the same mpcQualifiedName and security profile.
     */
    private Map<String, List<LegConfiguration>> legConfigurationsToMapByMpcAndSecurityProfile(Set<LegConfiguration> legConfigurations) {
        Map<String, List<LegConfiguration>> result = new HashMap<>();

        for (LegConfiguration legConfiguration : legConfigurations) {
            final String mpcQualifiedName = legConfiguration.getDefaultMpc().getQualifiedName();
            List<LegConfiguration> legConfigurationsPerMpc = result.get(mpcQualifiedName);
            if (CollectionUtils.isEmpty(legConfigurationsPerMpc)) {
                legConfigurationsPerMpc = new ArrayList<>();
                result.put(mpcQualifiedName, legConfigurationsPerMpc);
            }

            boolean isUnique = legConfigurationsPerMpc.stream()
                    .noneMatch(existingLeg ->
                            StringUtils.equalsIgnoreCase(existingLeg.getDefaultMpc().getQualifiedName(), mpcQualifiedName) &&
                                    StringUtils.equalsIgnoreCase(existingLeg.getSecurity().getSecurityProfile(), legConfiguration.getSecurity().getSecurityProfile())
                    );
            if (isUnique) {
                legConfigurationsPerMpc.add(legConfiguration);
            }
        }
        return result;
    }

    private List<Process> getValidProcesses(List<Process> pullProcesses) {
        final List<Process> validPullProcesses = new ArrayList<>();
        for (Process pullProcess : pullProcesses) {
            try {
                pullProcessValidator.validatePullProcess(Lists.newArrayList(pullProcess));
                validPullProcesses.add(pullProcess);
            } catch (PModeException e) {
                LOG.warn("Invalid pull process configuration found during pull try", e);
            }
        }
        return validPullProcesses;
    }

    private void sendPullRequestForMpc(String mpc, Party initiator, Process pullProcess, LegConfiguration legConfiguration) {
        for (Party responder : pullProcess.getResponderParties()) {
            String mpcQualifiedName = legConfiguration.getDefaultMpc().getQualifiedName();
            if (mpc != null && !mpc.equals(mpcQualifiedName)) {
                continue;
            }
            String agreement = getAgreement(pullProcess);
            MessageExchangeConfiguration messageExchangeConfiguration = new MessageExchangeConfiguration(agreement,
                    responder.getName(),
                    initiator.getName(),
                    legConfiguration.getService().getName(),
                    legConfiguration.getAction().getName(),
                    legConfiguration.getName());
            LOG.debug("messageExchangeConfiguration:[{}]", messageExchangeConfiguration);
            String mpcName = legConfiguration.getDefaultMpc().getName();
            Integer pullRequestNumberForResponder = pullMessageService.getPullRequestNumberForMpc(mpcName);
            LOG.debug("Sending:[{}] pull request for mpcFQN:[{}] to mpc:[{}]", pullRequestNumberForResponder, mpcQualifiedName, mpcName);
            for (int i = 0; i < pullRequestNumberForResponder; i++) {
                pullMessageService.sendPullMessage(legConfiguration, messageExchangeConfiguration);
            }
        }
    }

    public String getAgreement(Process pullProcess) {
        if (pullProcess.getAgreement() != null) {
            return pullProcess.getAgreement().getName();
        }
        return "";
    }

    protected <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> mcpMap = new ConcurrentHashMap<>();
        return t -> mcpMap.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public String retrieveReadyToPullUserMessageId(final String mpc, final Party initiator) {
        Set<String> partyIds = getPartyIds(mpc, initiator);
        for (String partyId : partyIds) {
            String pullMessageId = pullMessageService.getPullMessageId(partyId, mpc);
            if (pullMessageId != null) {
                return pullMessageId;
            }
        }
        return null;
    }

    protected Set<String> getPartyIds(String mpc, Party initiator) {
        if (initiator != null && CollectionUtils.isNotEmpty(initiator.getIdentifiers())) {
            Set<String> collect = initiator.getIdentifiers().stream().map(Identifier::getPartyId).collect(Collectors.toSet());
            LOG.trace("Retrieving party id(s), initiator list with size:[{}] found", collect.size());
            return collect;
        }
        if (pullProcessValidator.allowDynamicInitiatorInPullProcess()) {
            LOG.debug("Pmode initiator list is empty, extracting partyId from mpc [{}]", mpc);
            return Sets.newHashSet(mpcService.extractInitiator(mpc));
        }
        return Sets.newHashSet();
    }

    /**
     * µ
     * {@inheritDoc}
     */
    @Override
    public PullContext extractProcessOnMpc(final String mpcQualifiedName) {
        try {
            // identify mpc - it is sent in the pull request (but maybe suffixed with the party identifier):
            String mpc = mpcQualifiedName;

            // identify responder party - it is the gateway party:
            final Party gatewayParty = pModeProvider.getGatewayParty();

            // identify the pull process - it is the process that corresponds to the mpc:
            List<Process> processes = pModeProvider.findPullProcessByMpc(mpc);
            if (CollectionUtils.isEmpty(processes) && mpcService.forcePullOnMpc(mpc)) {
                LOG.debug("No process corresponds to mpc:[{}]", mpc);
                mpc = mpcService.extractBaseMpc(mpc);
                processes = pModeProvider.findPullProcessByMpc(mpc);
            }
            if (LOG.isDebugEnabled()) {
                for (Process process : processes) {
                    LOG.debug("Process:[{}] corresponds to mpc:[{}]; mpcQualifiedName: [{}}", process.getName(), mpc, mpcQualifiedName);
                }
            }
            if (CollectionUtils.isEmpty(processes)) {
                LOG.warn("Could not find any pull process for mpc:[{}] mpcQualifiedName:[{}]", mpc, mpcQualifiedName);
            }
            pullProcessValidator.validatePullProcess(processes);

            // FIXME: this is a possible cause for EDELIVERY-12807 - Ion Perpegel, August 2023
            if (CollectionUtils.size(processes) > 1) {
                LOG.warn("[{}] pull processes found for mpc=[{}] : [{}]. The first one will be used.",
                        processes.size(), mpcQualifiedName, processes.stream().map(p -> p.getName()).collect(Collectors.joining(",")));
            }

            Process chosenProcess = processes.get(0); // we choose the first one. To be refactored in EDELIVERY-12876

            // identify initiator party - from mpc or from the process:
            Party initiatorParty = null;

            String initiatorPartyName = extractInitiator(mpcQualifiedName); // eg "CBPull@DE6373283"
            if (initiatorPartyName != null) {
                initiatorParty = pModeProvider.findAllParties().stream()
                        .filter(p -> StringUtils.equalsIgnoreCase(p.getName(), initiatorPartyName)).findFirst().orElse(null);
                if (initiatorParty == null) {
                    // try to find the party by partyId
                    initiatorParty = pModeProvider.findAllParties().stream()
                            .filter(p -> p.getIdentifiers().stream().anyMatch(i -> StringUtils.equalsIgnoreCase(i.getPartyId(), initiatorPartyName))).findFirst().orElse(null);
                    if (initiatorParty != null) {
                        LOG.warn("Could not find initiator party by name [{}], but found by partyId [{}]", initiatorPartyName);
                    }
                }
                if (initiatorParty != null) { // initiator party specified in the mpc exists in the pmode
                    // now check if it is (or can be) an initiator of the chosen process
                    if (!pModeProvider.hasInitiatorParty(chosenProcess, initiatorParty.getName())) {
                        LOG.warn("Initiator party [{}] specified in the mpc [{}] is not an initiator of the chosen process [{}].",
                                initiatorParty.getName(), mpcQualifiedName, chosenProcess.getName());
                        initiatorParty = null;
                    }
                }
            } else { // initiator party not specified in the mpc
                if (CollectionUtils.size(chosenProcess.getInitiatorParties()) == 1) {
                    initiatorParty = chosenProcess.getInitiatorParties().iterator().next();
                }
            }

            return new PullContext(chosenProcess, gatewayParty, initiatorParty, mpc);
        } catch (IllegalArgumentException e) {
            throw new PModeException(DomibusCoreErrorCode.DOM_003, "No pmode configuration found");
        } catch (Exception e) {
            if (e instanceof PModeException) throw e;
            else
                throw new PModeException(DomibusCoreErrorCode.DOM_003, "Could not extract valid pull process on mpc " + mpcQualifiedName);
        }
    }

    @Override
    public void verifyReceiverCertificate(SecurityProfileConfiguration securityProfileConfiguration, final LegConfiguration legConfiguration, String receiverName) {
        if (policyService.isNoSecurityPolicy(securityProfileConfiguration) || policyService.isNoEncryptionPolicy(securityProfileConfiguration)) {
            LOG.debug("Validation of the receiver certificate is skipped.");
            return;
        }

        String alias = securityProfileService.getCertificateAliasForPurpose(receiverName, securityProfileConfiguration, CertificatePurpose.ENCRYPT);

        if (domibusPropertyProvider.getBooleanProperty(DOMIBUS_RECEIVER_CERTIFICATE_VALIDATION_ONSENDING)) {
            String chainExceptionMessage = "Cannot send message: receiver certificate is not valid or it has been revoked [" + alias + "]";
            try {
                boolean certificateChainValid = multiDomainCertificateProvider.isCertificateChainValid(domainProvider.getCurrentDomain(), alias);
                if (!certificateChainValid) {
                    throw new ChainCertificateInvalidException(DomibusCoreErrorCode.DOM_001, chainExceptionMessage);
                }
                LOG.info("Receiver certificate exists and is valid [{}]", alias);
            } catch (DomibusCertificateException | CryptoException e) {
                throw new ChainCertificateInvalidException(DomibusCoreErrorCode.DOM_001, chainExceptionMessage, e);
            }
        }
    }

    @Override
    public boolean forcePullOnMpc(String mpc) {
        return mpcService.forcePullOnMpc(mpc);
    }

    @Override
    public boolean forcePullOnMpc(UserMessage userMessage) {
        return mpcService.forcePullOnMpc(userMessage);
    }

    @Override
    public String extractInitiator(String mpc) {
        return mpcService.extractInitiator(mpc);
    }

    @Override
    public String extractBaseMpc(String mpc) {
        return mpcService.extractBaseMpc(mpc);
    }

    @Override
    public void verifySenderCertificate(SecurityProfileConfiguration securityProfileConfiguration, final LegConfiguration legConfiguration, String senderName) {
        if (policyService.isNoSecurityPolicy(securityProfileConfiguration)) {
            LOG.debug("Validation of the sender certificate is skipped.");
            return;
        }

        String alias = securityProfileService.getCertificateAliasForPurpose(senderName, securityProfileConfiguration, CertificatePurpose.SIGN);

        if (domibusPropertyProvider.getBooleanProperty(DOMIBUS_SENDER_CERTIFICATE_VALIDATION_ONSENDING)) {
            String chainExceptionMessage = "Cannot send message: sender certificate is not valid or it has been revoked [" + alias + "]";
            try {
                X509Certificate certificate = multiDomainCertificateProvider.getCertificateFromKeystore(domainProvider.getCurrentDomain(), alias);
                if (certificate == null) {
                    throw new ChainCertificateInvalidException(DomibusCoreErrorCode.DOM_001, "Cannot send message: " +
                            "sender[" + senderName + "] certificate with alias [" + alias + "] not found in Keystore");
                }
                if (!certificateService.isCertificateValid(certificate)) {
                    throw new ChainCertificateInvalidException(DomibusCoreErrorCode.DOM_001, chainExceptionMessage);
                }
                LOG.info("Sender certificate exists and is valid [{}]", alias);
            } catch (DomibusCertificateException | KeyStoreException | CryptoException ex) {
                // Is this an error and we stop the sending or we just log a warning that we were not able to validate the cert?
                // my opinion is that since the option is enabled, we should validate no matter what => this is an error
                throw new ChainCertificateInvalidException(DomibusCoreErrorCode.DOM_001, chainExceptionMessage, ex);
            }
        }
    }
}

