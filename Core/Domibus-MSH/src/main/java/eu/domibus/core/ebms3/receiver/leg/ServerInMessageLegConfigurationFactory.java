package eu.domibus.core.ebms3.receiver.leg;

import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.core.message.pull.PullRequestLegConfigurationFactory;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.cxf.binding.soap.SoapMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Thomas Dussart
 * @since 3.3
 */

@Component
@Qualifier("serverInMessageLegConfigurationFactory")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ServerInMessageLegConfigurationFactory implements MessageLegConfigurationFactory {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ServerInMessageLegConfigurationFactory.class);

    private final UserMessageLegConfigurationFactory userMessageLegConfigurationFactory;

    private final PullRequestLegConfigurationFactory pullRequestLegConfigurationFactory;

    private final ServerInReceiptLegConfigurationFactory serverInReceiptLegConfigurationFactory;

    public ServerInMessageLegConfigurationFactory(UserMessageLegConfigurationFactory userMessageLegConfigurationFactory, PullRequestLegConfigurationFactory pullRequestLegConfigurationFactory, ServerInReceiptLegConfigurationFactory serverInReceiptLegConfigurationFactory) {
        this.userMessageLegConfigurationFactory = userMessageLegConfigurationFactory;
        this.pullRequestLegConfigurationFactory = pullRequestLegConfigurationFactory;
        this.serverInReceiptLegConfigurationFactory = serverInReceiptLegConfigurationFactory;
    }

    @PostConstruct
    void init() {
        userMessageLegConfigurationFactory.
                chain(pullRequestLegConfigurationFactory).
                chain(serverInReceiptLegConfigurationFactory);

    }

    @Override
    public LegConfigurationExtractor extractMessageConfiguration(SoapMessage soapMessage, Ebms3Messaging messaging) {
        LegConfigurationExtractor legConfigurationExtractor = userMessageLegConfigurationFactory.extractMessageConfiguration(soapMessage, messaging);
        if (legConfigurationExtractor == null) {
            LOG.error("Leg configuration not found for incoming message with id " + messaging.getId());
        }
        return legConfigurationExtractor;
    }
}
