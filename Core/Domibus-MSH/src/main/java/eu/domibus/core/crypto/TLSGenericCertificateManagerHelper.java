package eu.domibus.core.crypto;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.common.DomibusCacheConstants;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import static eu.domibus.core.crypto.TLSGenericCertificateManagerImpl.TLS_GENERIC_CACHE;

/**
 * @author Ionut Breaz
 * @since 5.2
 */

@Service
public class TLSGenericCertificateManagerHelper {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSGenericCertificateManagerHelper.class);
    protected final DomainContextProvider domainContextProvider;

    public TLSGenericCertificateManagerHelper(DomainContextProvider domainContextProvider) {
        this.domainContextProvider = domainContextProvider;
    }

    // This method need to be in a different class than the caller to work
    @CacheEvict(cacheManager = DomibusCacheConstants.CACHE_MANAGER, value = TLS_GENERIC_CACHE, key = "#root.target.getCurrentDomainCode()")
    public void resetCache() {
        LOG.trace("Evicting the TLS Generic cache.");
    }

    public String getCurrentDomainCode() {
        Domain currentDomain = domainContextProvider.getCurrentDomain();
        return currentDomain != null ? currentDomain.getCode() : StringUtils.EMPTY;
    }
}
