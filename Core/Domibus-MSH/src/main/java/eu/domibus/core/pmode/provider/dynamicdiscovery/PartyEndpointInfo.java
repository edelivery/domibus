package eu.domibus.core.pmode.provider.dynamicdiscovery;

import java.security.cert.X509Certificate;


/**
 * @author Cosmin Baciu
 * @since 5.1.1
 */
public class PartyEndpointInfo {

    protected String certificateCn;
    protected X509Certificate signatureX509Certificate;
    protected String endpointUrl;


    public PartyEndpointInfo(String certificateCn, X509Certificate signatureX509Certificate, String endpointUrl) {
        this.certificateCn = certificateCn;
        this.signatureX509Certificate = signatureX509Certificate;
        this.endpointUrl = endpointUrl;
    }

    public String getCertificateCn() {
        return certificateCn;
    }

    public void setCertificateCn(String certificateCn) {
        this.certificateCn = certificateCn;
    }

    public X509Certificate getSignatureX509Certificate() {
        return signatureX509Certificate;
    }

    public void setSignatureX509Certificate(X509Certificate signatureX509Certificate) {
        this.signatureX509Certificate = signatureX509Certificate;
    }

    public String getEndpointUrl() {
        return endpointUrl;
    }

    public void setEndpointUrl(String endpointUrl) {
        this.endpointUrl = endpointUrl;
    }
}
