package eu.domibus.core.property;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.configuration.spi.DomibusConfigurationManagerSpi;
import eu.domibus.configuration.spi.DomibusResource;
import eu.domibus.configuration.spi.DomibusResourceType;
import eu.domibus.configuration.spi.SearchStrategy;
import eu.domibus.core.spring.DomibusResourceUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.plugin.environment.DomibusEnvironmentConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyProvider.DOMIBUS_PROPERTY_FILE;
import static eu.domibus.ext.services.DomibusPropertyManagerExt.*;

/**
 * Class responsible for configuring Domibus property sources in a specific order
 *
 * @author Cosmin Baciu
 * @since 4.2
 */
@Configuration("domibusPropertyConfiguration")
public class DomibusPropertyConfiguration {

    public static final String MULTITENANT_DOMIBUS_PROPERTIES_SUFFIX = "-domibus.properties";
    public static final String SUPER_DOMIBUS_PROPERTIES = "super" + MULTITENANT_DOMIBUS_PROPERTIES_SUFFIX;
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusPropertyConfiguration.class);

    @Bean("domibusDefaultProperties")
    public PropertiesFactoryBean domibusDefaultProperties() throws IOException {
        PropertiesFactoryBean result = new PropertiesFactoryBean();
        result.setIgnoreResourceNotFound(true);
        result.setFileEncoding(StandardCharsets.UTF_8.name());

        List<Resource> resources = new ArrayList<>();
        resources.add(new ClassPathResource("config/domibus-default.properties"));
        resources.add(new ClassPathResource("config/" + DOMIBUS_PROPERTY_FILE));

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] pluginDefaultResourceList = resolver.getResources("classpath*:config/*-plugin-default.properties");
        LOG.debug("Adding the following plugin default properties files [{}]", pluginDefaultResourceList);
        resources.addAll(Arrays.asList(pluginDefaultResourceList));

        result.setLocations(resources.toArray(new Resource[0]));
        return result;
    }

    /**
     * This method is called twice when Domibus starts up: once to get the schema to determine if we are in single tenancy or multitenancy
     * and the second time for creating the PropertySources
     *
     * @param domibusConfigurationProvider the Domibus configuration provider
     * @throws IOException in case of IO exception
     * @return the properties factory bean
     */
    public PropertiesFactoryBean domibusProperties(DomibusConfigurationManagerSpi domibusConfigurationProvider) throws IOException {
        PropertiesFactoryBean result = new PropertiesFactoryBean();
        result.setIgnoreResourceNotFound(true);
        result.setFileEncoding(StandardCharsets.UTF_8.name());

        DomibusResourceUtil domibusResourceUtil = new DomibusResourceUtil();

        List<Resource> resources = new ArrayList<>();
        resources.add(new ClassPathResource("config/domibus-default.properties"));
        resources.add(new ClassPathResource("config/" + DOMIBUS_PROPERTY_FILE));

        final DomibusResource domibusPropertyFileResource = domibusConfigurationProvider.getResource(DOMIBUS_PROPERTY_FILE, DomibusResourceType.DOMIBUS_PROPERTIES_FILE);
        final Resource domibusPropertiesResource = domibusResourceUtil.domibusResourceToResource(domibusPropertyFileResource);

        final boolean isMultitenancy = isMultitenancy(domibusPropertiesResource);

        resources.add(domibusPropertiesResource);

        if (isMultitenancy) {
            LOG.info("Adding [{}] property file", SUPER_DOMIBUS_PROPERTIES);
            try {
                final DomibusResource superPropertiesFileResource = domibusConfigurationProvider.getResource(DOMAINS_HOME + "/" + SUPER_DOMIBUS_PROPERTIES, DomibusResourceType.DOMIBUS_SUPER_PROPERTIES_FILE);
                LOG.info("Adding the super properties file [{}]", superPropertiesFileResource.getName());
                resources.add(domibusResourceUtil.domibusResourceToResource(superPropertiesFileResource));
            } catch (Exception e) {
                throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Super properties file not found [" + SUPER_DOMIBUS_PROPERTIES + "]", e);
            }
        }

        if (isMultitenancy) {
            LOG.info("Adding domain property files with pattern [{}]", MULTITENANT_DOMIBUS_PROPERTIES_SUFFIX);

            List<DomibusResource> tenantConfigurationResources = domibusConfigurationProvider.findFiles(
                    DOMAINS_HOME,
                    MULTITENANT_DOMIBUS_PROPERTIES_SUFFIX,
                    SearchStrategy.RECURSIVE_ENDING_WITH_PATTERN,
                    List.of("backup"),
                    DomibusResourceType.DOMIBUS_TENANT_PROPERTIES_FILE);
            tenantConfigurationResources = tenantConfigurationResources.stream()
                    .filter(domibusResource -> !"super-domibus.properties".equalsIgnoreCase(domibusResource.getName()))
                    .collect(Collectors.toList());

            LOG.debug("Adding the following domain properties files [{}]", tenantConfigurationResources);

            for (DomibusResource tenantConfigurationResource : tenantConfigurationResources) {
                LOG.info("Adding tenant configuration file [{}]", tenantConfigurationResource.getLocation());
                final Resource tenantConfiguration = domibusResourceUtil.domibusResourceToResource(tenantConfigurationResource);
                resources.add(tenantConfiguration);
            }
        }


        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] pluginDefaultResourceList = resolver.getResources("classpath*:config/*-plugin-default.properties");
        LOG.debug("Adding the following plugin default properties files {}", Arrays.toString(pluginDefaultResourceList));
        resources.addAll(Arrays.asList(pluginDefaultResourceList));

        final ClassPathResource serverDefaultProperties = new ClassPathResource("config/" + DOMIBUS_PROPERTY_FILE);
        final InputStream inputStream = serverDefaultProperties.getInputStream();
        final Properties properties = new Properties();
        properties.load(inputStream);
        final String currentServer = properties.getProperty(DomibusEnvironmentConstants.DOMIBUS_ENVIRONMENT_SERVER_NAME);

        switch (currentServer) {
            case DomibusEnvironmentConstants.DOMIBUS_ENVIRONMENT_SERVER_TOMCAT:
                Resource[] pluginDefaultTomcatResourceList = resolver.getResources("classpath*:config/tomcat/*-plugin.properties");
                LOG.debug("Adding the following  plugin properties files for tomcat {}", Arrays.toString(pluginDefaultTomcatResourceList));
                resources.addAll(Arrays.asList(pluginDefaultTomcatResourceList));
                break;
            case DomibusEnvironmentConstants.DOMIBUS_ENVIRONMENT_SERVER_WILDFLY:
                Resource[] pluginDefaultWildflyResourceList = resolver.getResources("classpath*:config/wildfly/*-plugin.properties");
                LOG.debug("Adding the following plugin default properties files for wildfly {}", Arrays.toString(pluginDefaultWildflyResourceList));
                resources.addAll(Arrays.asList(pluginDefaultWildflyResourceList));
                break;
            case DomibusEnvironmentConstants.DOMIBUS_ENVIRONMENT_SERVER_WEBLOGIC:
                Resource[] pluginDefaultWeblogicResourceList = resolver.getResources("classpath*:config/weblogic/*-plugin.properties");
                LOG.debug("Adding the following plugin default properties files for weblogic {}", Arrays.toString(pluginDefaultWeblogicResourceList));
                resources.addAll(Arrays.asList(pluginDefaultWeblogicResourceList));
                break;
            default:
                LOG.warn("couldn't find valid server name for the plugin properties.");
        }


        final List<DomibusResource> pluginResourceList = domibusConfigurationProvider.findFiles(
                PLUGINS_CONFIG_HOME,
                "-plugin.properties",
                SearchStrategy.CURRENT_LOCATION_ENDING_WITH_PATTERN,
                new ArrayList<>(),
                DomibusResourceType.DOMIBUS_PLUGIN_PROPERTIES_FILE);
        LOG.debug("Adding the following plugin configuration files [{}]", pluginResourceList);

        for (DomibusResource pluginJarResource : pluginResourceList) {
            LOG.info("Adding plugin configuration file [{}]", pluginJarResource.getName());
            final Resource pluginConfiguration = domibusResourceUtil.domibusResourceToResource(pluginJarResource);
            resources.add(pluginConfiguration);
        }

        if (isMultitenancy) {
            final List<DomibusResource> domainPluginResourceList = domibusConfigurationProvider.findFiles(
                    PLUGINS_CONFIG_HOME + "/" + DOMAINS_HOME,
                    "-plugin.properties",
                    SearchStrategy.RECURSIVE_ENDING_WITH_PATTERN,
                    new ArrayList<>(),
                    DomibusResourceType.DOMIBUS_TENANT_PLUGIN_PROPERTIES_FILE);
            LOG.debug("Adding the following domain plugin configuration files [{}]", domainPluginResourceList);
            for (DomibusResource pluginJarResource : domainPluginResourceList) {
                LOG.info("Adding domain plugin configuration file [{}]", pluginJarResource.getLocation());
                final Resource pluginConfiguration = domibusResourceUtil.domibusResourceToResource(pluginJarResource);
                resources.add(pluginConfiguration);
            }
        }


        final List<DomibusResource> extensionResourceList = domibusConfigurationProvider.findFiles(
                EXTENSIONS_CONFIG_HOME,
                "-extension.properties",
                SearchStrategy.CURRENT_LOCATION_ENDING_WITH_PATTERN,
                new ArrayList<>(),
                DomibusResourceType.DOMIBUS_EXTENSION_PROPERTIES_FILE);
        LOG.debug("Adding the following extension configuration files [{}]", extensionResourceList);
        for (DomibusResource extensionJarResource : extensionResourceList) {
            LOG.info("Adding extension configuration file [{}]", extensionJarResource.getLocation());
            final Resource extensionConfiguration = domibusResourceUtil.domibusResourceToResource(extensionJarResource);
            resources.add(extensionConfiguration);
        }

        if (isMultitenancy) {
            final List<DomibusResource> domainExtensionResourceList = domibusConfigurationProvider.findFiles(
                    EXTENSIONS_CONFIG_HOME + "/" + DOMAINS_HOME,
                    "-extension.properties",
                    SearchStrategy.RECURSIVE_ENDING_WITH_PATTERN,
                    new ArrayList<>(),
                    DomibusResourceType.DOMIBUS_TENANT_EXTENSION_PROPERTIES_FILE);
            LOG.debug("Adding the following domain extension configuration files [{}]", domainExtensionResourceList);
            for (DomibusResource domainExtensionJarResource : domainExtensionResourceList) {
                LOG.info("Adding domain extension configuration file [{}]", domainExtensionJarResource.getLocation());
                final Resource domainExtensionConfiguration = domibusResourceUtil.domibusResourceToResource(domainExtensionJarResource);
                resources.add(domainExtensionConfiguration);
            }
        }

        result.setLocations(resources.toArray(new Resource[0]));
        return result;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer result = new PropertySourcesPlaceholderConfigurer();
        result.setFileEncoding(StandardCharsets.UTF_8.name());
        result.setIgnoreResourceNotFound(true);
        result.setIgnoreUnresolvablePlaceholders(true);
        return result;
    }

    protected Properties getDomibusProperties(Resource domibusPropertiesResource) {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setFileEncoding(StandardCharsets.UTF_8.name());
        propertiesFactoryBean.setSingleton(false);
        propertiesFactoryBean.setLocation(domibusPropertiesResource);
        Properties properties = null;
        try {
            properties = propertiesFactoryBean.getObject();
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Domibus properties file not found [" + DOMIBUS_PROPERTY_FILE + "]", e);
        }
        return properties;
    }

    protected boolean isMultitenancy(Resource domibusPropertiesResource) {
        final Properties domibusProperties = getDomibusProperties(domibusPropertiesResource);
        final String generalSchema = domibusProperties.getProperty(DomainService.GENERAL_SCHEMA_PROPERTY);
        //if general schema has a non null value, we are in multi tenancy
        return StringUtils.isNotBlank(generalSchema);
    }


}
