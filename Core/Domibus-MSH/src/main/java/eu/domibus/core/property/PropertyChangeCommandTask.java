package eu.domibus.core.property;

import eu.domibus.api.cluster.Command;
import eu.domibus.api.cluster.CommandProperty;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.property.DomibusPropertyMetadata;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.clustering.CommandTask;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Cosmin Baciu
 * @since 4.2
 */
@Service
public class PropertyChangeCommandTask implements CommandTask {

    private static final DomibusLogger LOGGER = DomibusLoggerFactory.getLogger(PropertyChangeCommandTask.class);

    private final DomibusPropertyProvider domibusPropertyProvider;
    protected final DomainContextProvider domainContextProvider;

    public PropertyChangeCommandTask(DomibusPropertyProvider domibusPropertyProvider, DomainContextProvider domainContextProvider) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domainContextProvider = domainContextProvider;
    }

    @Override
    public boolean canHandle(String command) {
        return StringUtils.equalsIgnoreCase(Command.DOMIBUS_PROPERTY_CHANGE, command);
    }

    @Override
    public void execute(Map<String, String> properties) {
        LOGGER.debug("Property value change command");

        final String propName = properties.get(CommandProperty.PROPERTY_NAME);
        String propVal = properties.get(CommandProperty.PROPERTY_VALUE);
        if (propVal == null) {
            // Domibus does not permit setting a property value to null
            // so, if it is null here, it means Weblogic transformed an empty value to null when propagating it via Oracle database;
            // we need to correct it back
            propVal = StringUtils.EMPTY;
        }
        final DomibusPropertyMetadata.Type propType = domibusPropertyProvider.getPropertyType(propName);

        Domain domain = domainContextProvider.getCurrentDomainSafely();
        try {
            LOGGER.trace("Updating the value of [{}] property on domain [{}], no broadcast", propName, domain);
            domibusPropertyProvider.setProperty(domain, propName, propVal, false);
            LOGGER.info("Property [{}] updated to [{}]", propName,
                    propType == DomibusPropertyMetadata.Type.PASSWORD ? "******" : propVal);
        } catch (Exception ex) {
            LOGGER.error("Error trying to set property [{}] with value [{}] on domain [{}]", propName, propVal, domain, ex);
        }
    }
}
