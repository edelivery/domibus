package eu.domibus.core.ebms3.sender.interceptor;

import eu.domibus.api.pki.SecurityProfileService;
import eu.domibus.api.pmode.PModeConstants;
import eu.domibus.api.security.CertificatePurpose;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.common.model.configuration.Party;
import eu.domibus.core.crypto.SecurityProfileHelper;
import eu.domibus.core.ebms3.ws.policy.PolicyService;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.util.SoapUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.ws.policy.PolicyOutInterceptor;
import org.apache.cxf.ws.policy.PolicyVerificationOutInterceptor;
import org.apache.cxf.ws.security.SecurityConstants;
import org.apache.wss4j.common.ConfigurationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.soap.SOAPMessage;
import java.util.Map;

/**
 * This interceptor is responsible for discovery and setup of WS-Security Policies for outgoing messages
 *
 * @author Christian Koch, Stefan Mueller
 * @since 3.0
 */
@Service("setPolicyOutInterceptor")
public class SetPolicyOutInterceptor extends AbstractSoapInterceptor {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SetPolicyOutInterceptor.class);

    @Autowired
    private PModeProvider pModeProvider;

    @Autowired
    private PolicyService policyService;

    @Autowired
    protected SecurityProfileHelper securityProfileHelper;

    @Autowired
    protected SecurityProfileService securityProfileService;

    @Autowired
    protected SoapUtil soapUtil;

    public SetPolicyOutInterceptor() {
        super(Phase.SETUP);
        this.addBefore(PolicyOutInterceptor.class.getName());
    }

    /**
     * Intercepts a message.
     * Interceptors should NOT invoke handleMessage or handleFault
     * on the next interceptor - the interceptor chain will
     * take care of this.
     *
     * @param message the message to handle
     */
    @Override
    public void handleMessage(final SoapMessage message) throws Fault {
        LOG.debug("SetPolicyOutInterceptor");
        final String pModeKey = (String) message.getContextualProperty(PModeConstants.PMODE_KEY_CONTEXT_PROPERTY);
        LOG.debug("Using pModeKey [{}]", pModeKey);
        message.getExchange().put(PModeConstants.PMODE_KEY_CONTEXT_PROPERTY, pModeKey);
        message.getInterceptorChain().add(new PrepareAttachmentInterceptor());

        final SecurityProfileConfiguration securityProfileConfiguration = (SecurityProfileConfiguration) message.getContextualProperty(PModeConstants.SECURITY_PROFILE_CONFIGURATION);
        message.getExchange().put(PModeConstants.SECURITY_PROFILE_CONFIGURATION, securityProfileConfiguration);

        setSecurityPropertiesOnMessage(message, securityProfileConfiguration, pModeKey);
    }

    protected void setSecurityPropertiesOnMessage(SoapMessage message, SecurityProfileConfiguration securityProfileConfiguration, String pModeKey) {
        final Map<String, Object> securityConfigurationAsMap = securityProfileHelper.getOutgoingSecurityConfigurationAsMap(securityProfileConfiguration);
        LOG.debug("Using the security profile configuration [{}]", securityConfigurationAsMap);
        for (Map.Entry<String, Object> entry : securityConfigurationAsMap.entrySet()) {
            message.put(entry.getKey(), entry.getValue());
        }

        LOG.businessInfo(DomibusMessageCode.BUS_SECURITY_ALGORITHM_OUTGOING_USE, securityProfileConfiguration);

        if (securityProfileConfiguration.isEncryptionEnabled()) {
            message.put(SecurityConstants.USE_ATTACHMENT_ENCRYPTION_CONTENT_ONLY_TRANSFORM, true);

            String receiverPartyName = extractReceiverPartyName(pModeKey);

            String encryptionAlias = securityProfileService.getCertificateAliasForPurpose(receiverPartyName, securityProfileConfiguration, CertificatePurpose.ENCRYPT);

            message.put(ConfigurationConstants.ENCRYPTION_USER, encryptionAlias);
            LOG.businessInfo(DomibusMessageCode.BUS_SECURITY_USER_OUTGOING_USE, encryptionAlias);
        }
    }

    protected String extractReceiverPartyName(String pModeKey) {
        String receiverPartyName = null;
        try {
            Party receiverParty = pModeProvider.getReceiverParty(pModeKey);
            if (receiverParty != null) {
                receiverPartyName = receiverParty.getName();
            }
        } catch (ConfigurationException exc) {
            LOG.info("Responder party was not found, will be extracted from pModeKey.");
        }
        if (receiverPartyName == null) {
            receiverPartyName = pModeProvider.getReceiverPartyNameFromPModeKey(pModeKey);
        }

        return receiverPartyName;
    }

    public static class LogAfterPolicyCheckInterceptor extends AbstractSoapInterceptor {
        public LogAfterPolicyCheckInterceptor() {
            super(Phase.POST_STREAM);
            this.addAfter(PolicyVerificationOutInterceptor.class.getName());
        }

        @Override
        public void handleMessage(final SoapMessage message) throws Fault {

            final SOAPMessage soapMessage = message.getContent(SOAPMessage.class);
            soapMessage.removeAllAttachments();
        }
    }
}
