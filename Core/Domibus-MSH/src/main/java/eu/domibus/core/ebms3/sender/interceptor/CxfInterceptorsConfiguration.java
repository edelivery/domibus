package eu.domibus.core.ebms3.sender.interceptor;

import org.apache.cxf.interceptor.AttachmentOutInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
@Configuration
public class CxfInterceptorsConfiguration {

    public static final String WSS4J_IN_INTERCEPTOR = "wss4JInInterceptor";
    public static final String WSS4J_OUT_INTERCEPTOR = "wss4JOutInterceptor";

    @Bean(WSS4J_OUT_INTERCEPTOR)
    public WSS4JOutInterceptor wss4JOutInterceptor() {
        return new WSS4JOutInterceptor();
    }

    @Bean(WSS4J_IN_INTERCEPTOR)
    public WSS4JInInterceptor wss4JInInterceptor() {
        return new WSS4JInInterceptor();
    }

    @Bean("attachmentOutInterceptor")
    public AttachmentOutInterceptor attachmentOutInterceptor() {
        return new AttachmentOutInterceptor();
    }
}
