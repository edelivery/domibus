package eu.domibus.core.certificate;

import eu.domibus.api.crypto.CryptoException;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.DomibusCertificateException;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.pki.KeystorePersistenceService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.property.encryption.PasswordDecryptionService;
import eu.domibus.configuration.spi.DomibusConfigurationManagerSpi;
import eu.domibus.configuration.spi.DomibusResource;
import eu.domibus.configuration.spi.DomibusResourceType;
import eu.domibus.core.configuration.DomibusConfigurationManagerSpiProvider;
import eu.domibus.core.crypto.TruststoreDao;
import eu.domibus.core.crypto.TruststoreEntity;
import eu.domibus.core.property.DomibusRawPropertyProvider;
import eu.domibus.core.spring.DomibusResourceUtil;
import eu.domibus.core.util.backup.BackupService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Arrays;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;
import static eu.domibus.core.crypto.MultiDomainCryptoServiceImpl.*;

/**
 * @author Ion Perpegel
 * @since 5.1
 */
public class KeyStorePersistenceServiceImpl implements KeystorePersistenceService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(KeyStorePersistenceServiceImpl.class);

    protected final CertificateHelper certificateHelper;

    protected final TruststoreDao truststoreDao;

    private final PasswordDecryptionService passwordDecryptionService;

    private final DomainContextProvider domainContextProvider;

    protected final DomibusPropertyProvider domibusPropertyProvider;

    protected final DomibusRawPropertyProvider domibusRawPropertyProvider;

    private final BackupService backupService;

    public KeyStorePersistenceServiceImpl(CertificateHelper certificateHelper,
                                          TruststoreDao truststoreDao,
                                          PasswordDecryptionService passwordDecryptionService,
                                          DomainContextProvider domainContextProvider,
                                          DomibusPropertyProvider domibusPropertyProvider,
                                          DomibusRawPropertyProvider domibusRawPropertyProvider,
                                          BackupService backupService) {
        this.certificateHelper = certificateHelper;
        this.truststoreDao = truststoreDao;
        this.passwordDecryptionService = passwordDecryptionService;
        this.domainContextProvider = domainContextProvider;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domibusRawPropertyProvider = domibusRawPropertyProvider;
        this.backupService = backupService;
    }

    @Override
    public KeystorePersistenceInfo getTrustStorePersistenceInfo() {
        return new TrustStorePersistenceInfoImpl();
    }

    @Override
    public KeystorePersistenceInfo getKeyStorePersistenceInfo() {
        return new KeyStorePersistenceInfoImpl();
    }

    @Override
    public KeyStoreContentInfo loadStore(KeystorePersistenceInfo persistenceInfo) {
        String storePath = persistenceInfo.getFileLocation();
        String storeType = persistenceInfo.getType();

        String storeName = persistenceInfo.getName();
        if (StringUtils.isBlank(storePath)) {
            if (persistenceInfo.isOptional()) {
                LOG.debug("The store location of [{}] is missing (and optional) so exiting.", storeName);
                return null;
            }
            throw new DomibusCertificateException(String.format("Store [%s] is missing and is not optional.", storeName));
        }
        certificateHelper.validateStoreType(storeType, storePath);

        byte[] contentOnDisk = getStoreContentFromFile(storePath, persistenceInfo.getName());
        String password = decrypt(storeName, persistenceInfo.getPassword());

        String fileName = FilenameUtils.getName(storePath);

        return certificateHelper.createStoreContentInfo(storeName, fileName, contentOnDisk, password, storeType, false);
    }

    @Override
    public void saveStore(KeyStoreContentInfo contentInfo, KeystorePersistenceInfo persistenceInfo) {
        String diskFileLocation = persistenceInfo.getFileLocation();
        String diskFileName = FilenameUtils.getName(diskFileLocation);
        if (!StringUtils.equals(contentInfo.getFileName(), diskFileName)) {
            Path newStoreFileLocation = Paths.get(FilenameUtils.getFullPath(diskFileLocation), contentInfo.getFileName());
            String newFileLocationString = newStoreFileLocation.toString().replace("\\", "/");

            doSaveStore(contentInfo.getContent(), newFileLocationString, persistenceInfo);
            persistenceInfo.updateFileLocation(newFileLocationString);
        } else {
            doSaveStore(contentInfo.getContent(), persistenceInfo);
        }

        if (!StringUtils.equals(contentInfo.getType(), persistenceInfo.getType())) {
            persistenceInfo.updateType(contentInfo.getType());
        }
        if (!StringUtils.equals(contentInfo.getPassword(), persistenceInfo.getPassword())) {
            persistenceInfo.updatePassword(contentInfo.getPassword());
        }
    }

    @Override
    public void saveStore(KeyStore store, KeystorePersistenceInfo persistenceInfo) {
        byte[] content = getContent(store, persistenceInfo);
        doSaveStore(content, persistenceInfo);
    }

    @Override
    public void saveStore(byte[] content, KeystorePersistenceInfo persistenceInfo) {
        doSaveStore(content, persistenceInfo);
    }

    @Override
    @Transactional
    public void saveStoreFromDBToDisk(KeystorePersistenceInfo persistenceInfo) {
        String storeName = persistenceInfo.getName();
        String filePath = persistenceInfo.getFileLocation();
        LOG.info("Saving the store [{}] from database to the configuration repository [{}]", storeName, filePath);

        try {
            if (filePath == null) {
                if (persistenceInfo.isOptional()) {
                    LOG.debug("The store location of [{}] is missing (and optional) so exiting.", storeName);
                    return;
                }
                throw new DomibusCertificateException(String.format("Truststore with type [%s] is missing and is not optional.", storeName));
            }

            certificateHelper.validateStoreType(persistenceInfo.getType(), filePath);

            TruststoreEntity persisted = truststoreDao.findByNameSafely(storeName);
            if (persisted == null) {
                LOG.info("Store [{}] is not found in the DB.", storeName);
                return;
            }

            final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();
            try (final DomibusResource truststoreResource = domibusConfigurationProvider.getResource(filePath, DomibusResourceType.DOMIBUS_TRUSTSTORE)) {
                if (persisted.getModificationTime().getTime() < truststoreResource.getLastModified().toEpochMilli()) {
                    LOG.info("The store [{}] on disk [{}] is newer [{}] than the persisted one [{}], so no overwriting.",
                            storeName, filePath, truststoreResource.getLastModified(), persisted.getModificationTime().getTime());
                    return;
                }
            }

            byte[] contentOnDisk = getStoreContentFromFile(filePath, persistenceInfo.getName());
            if (!Arrays.equals(persisted.getContent(), contentOnDisk)) {
                LOG.info("Saving the store [{}] from database to the configuration repository.", storeName);
                DomibusResource domibusResource = new DomibusResource(persistenceInfo.getName(), persistenceInfo.getName(), filePath, null, new ByteArrayInputStream(persisted.getContent()), null);

                domibusConfigurationProvider.storeResource(domibusResource);
            } else {
                LOG.info("The store [{}] on disk has the same content.", storeName);
            }
            truststoreDao.delete(persisted);
            LOG.info("The store [{}] was deleted from the DB.", storeName);
        } catch (Exception ex) {
            LOG.error(String.format("The store [%s], whose file location is [%s], could not be persisted! " +
                    "Please check that the store file is present and the location property is set accordingly.", storeName, filePath), ex);
        }
    }

    private void doSaveStore(byte[] content, KeystorePersistenceInfo persistenceInfo) {
        doSaveStore(content, persistenceInfo.getFileLocation(), persistenceInfo);
    }

    protected void doSaveStore(byte[] storeContent, String storeFileLocation, KeystorePersistenceInfo persistenceInfo) {
        if (storeFileLocation == null) {
            storeFileLocation = persistenceInfo.getFileLocation();
        }
        try {
            backupService.backupFile(storeFileLocation, "backups");
        } catch (DomibusCoreException e) {
            throw new CryptoException("Could not backup store:", e);
        }

        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();

        DomibusResource domibusResource = new DomibusResource(persistenceInfo.getName(), persistenceInfo.getName(), storeFileLocation, null, new ByteArrayInputStream(storeContent), null);
        domibusConfigurationProvider.storeResource(domibusResource);

    }

    private byte[] getContent(KeyStore store, KeystorePersistenceInfo persistenceInfo) {
        try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream()) {
            String password = persistenceInfo.getPassword();
            String decryptedPassword = decrypt(persistenceInfo.getName(), password);
            store.store(byteStream, decryptedPassword.toCharArray());
            return byteStream.toByteArray();
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            throw new CryptoException("Could not persist store:", e);
        }
    }

    protected byte[] getStoreContentFromFile(String location, String name) {
        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();
        try {
            final DomibusResource resource = domibusConfigurationProvider.getResource(location, name);
            DomibusResourceUtil domibusResourceUtil = new DomibusResourceUtil();
            return domibusResourceUtil.domibusResourceToByteArray(resource);
        } catch (Exception e) {
            throw new DomibusCertificateException("Could not read store from [" + location + "]");
        }
    }

    private String decrypt(String trustName, String password) {
        return passwordDecryptionService.decryptPropertyIfEncrypted(domainContextProvider.getCurrentDomainSafely(),
                trustName + ".password", password);
    }

    class TrustStorePersistenceInfoImpl implements KeystorePersistenceInfo {

        @Override
        public String getName() {
            return DOMIBUS_TRUSTSTORE_NAME;
        }

        @Override
        public boolean isOptional() {
            return false;
        }

        @Override
        public String getFileLocation() {
            return domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_TRUSTSTORE_LOCATION);
        }

        @Override
        public String getType() {
            return domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_TRUSTSTORE_TYPE);
        }

        @Override
        public String getPassword() {
            return domibusRawPropertyProvider.getRawPropertyValue(DOMIBUS_SECURITY_TRUSTSTORE_PASSWORD);
        }

        @Override
        public String toString() {
            return getName() + ":" + getFileLocation() + ":" + getType() + ":" + getPassword();
        }

        @Override
        public void updatePassword(String password) {
            domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_TRUSTSTORE_PASSWORD, password);
        }

        @Override
        public void updateType(String type) {
            domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_TRUSTSTORE_TYPE, type);
        }

        @Override
        public void updateFileLocation(String fileLocation) {
            domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_TRUSTSTORE_LOCATION, fileLocation);
        }

        @Override
        public boolean isDownloadable() {
            return true;
        }

        @Override
        public boolean isReadOnly() {
            return false;
        }
    }

    class KeyStorePersistenceInfoImpl implements KeystorePersistenceInfo {

        @Override
        public String getName() {
            return DOMIBUS_KEYSTORE_NAME;
        }

        @Override
        public boolean isOptional() {
            return false;
        }

        @Override
        public String getFileLocation() {
            return domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_KEYSTORE_LOCATION);
        }

        @Override
        public String getType() {
            return domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_KEYSTORE_TYPE);
        }

        @Override
        public String getPassword() {
            return domibusRawPropertyProvider.getRawPropertyValue(DOMIBUS_SECURITY_KEYSTORE_PASSWORD);
        }

        @Override
        public String getKeyEntryPassword() {
            return domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_KEY_PRIVATE_PASSWORD);
        }

        @Override
        public String toString() {
            return getName() + ":" + getFileLocation() + ":" + getType() + ":" + getPassword();
        }

        @Override
        public void updatePassword(String password) {
            domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_KEYSTORE_PASSWORD, password);
        }

        @Override
        public void updateType(String type) {
            domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_KEYSTORE_TYPE, type);
        }

        @Override
        public void updateFileLocation(String fileLocation) {
            domibusPropertyProvider.setProperty(DOMIBUS_SECURITY_KEYSTORE_LOCATION, fileLocation);
        }

        @Override
        public boolean isDownloadable() {
            return true;
        }

        @Override
        public boolean isReadOnly() {
            return false;
        }
    }

}
