package eu.domibus.core.pmode.provider.dynamicdiscovery;

import java.security.cert.X509Certificate;
import java.util.Map;

/**
 * @author Ioana Dragusanu (idragusa)
 * @since 3.2.5
 */
public class EndpointInfo {

    private String transportProfile;
    private String address;
    private Map<String, X509Certificate> certificates;

    public EndpointInfo(String transportProfile, String address, Map<String, X509Certificate> certificates) {
        this.transportProfile = transportProfile;
        this.address = address;
        this.certificates = certificates;
    }

    public String getAddress() {
        return address;

    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTransportProfile() {
        return transportProfile;
    }

    public void setTransportProfile(String transportProfile) {
        this.transportProfile = transportProfile;
    }

    public Map<String, X509Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(Map<String, X509Certificate> certificates) {
        this.certificates = certificates;
    }
}
