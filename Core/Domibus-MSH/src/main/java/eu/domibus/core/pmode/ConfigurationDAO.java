
package eu.domibus.core.pmode;


import eu.domibus.common.model.configuration.Process;
import eu.domibus.common.model.configuration.*;
import eu.domibus.core.dao.BasicDao;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Random;

/**
 * @author Christian Koch, Stefan Mueller
 */
@Repository
public class ConfigurationDAO extends BasicDao<Configuration> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ConfigurationDAO.class);

    public ConfigurationDAO() {
        super(Configuration.class);
    }

    @Transactional
    public boolean configurationExists() {
        final TypedQuery<Long> query = this.em.createNamedQuery("Configuration.count", Long.class);
        Long result = query.getSingleResult();
        if (result > 1) {
            LOG.warn("More than one configuration found: [{}]", result);
        }
        return result != 0;
    }

    public Configuration read() {
        final TypedQuery<Configuration> query = this.em.createNamedQuery("Configuration.getConfiguration", Configuration.class);
        return query.getSingleResult();
    }

    @Transactional
    public Configuration readEager() {
        final TypedQuery<Configuration> query = this.em.createNamedQuery("Configuration.getConfiguration", Configuration.class);

        final List<Configuration> configurationList = query.getResultList();
        if (CollectionUtils.isEmpty(configurationList)) {
            LOG.debug("PMode configuration not found");
            return null;
        }
        if (CollectionUtils.size(configurationList) > 1) {
            throw new ConfigurationException("Only one configuration is allowed");
        }

        final Configuration configuration = configurationList.get(0);

        for (final Mpc mpc : configuration.getMpcs()) {
            mpc.getName(); //This is just top avoid the compiler optimizing this away
        }
        final BusinessProcesses businessProcesses = configuration.getBusinessProcesses();

        for (final Process process : businessProcesses.getProcesses()) {
            process.getInitiatorParties().size();
            process.getInitiatorParties().stream().forEach(party -> party.getIdentifiers().size());

            process.getResponderParties().size();
            process.getResponderParties().stream().forEach(party -> party.getIdentifiers().size());

            process.getLegs().size();
            // change PersistentSet with HashSet
            process.detachParties();
        }

        businessProcesses.getRoles().size();

        businessProcesses.getProperties().size();
        businessProcesses.getSecurities().size();
        businessProcesses.getActions().size();
        if (businessProcesses.getPayloads() != null) {
            businessProcesses.getPayloads().size();
        }
        businessProcesses.getProcesses().size();
        businessProcesses.getParties().size();
        for (final Party p : businessProcesses.getParties()) {
            p.getIdentifiers().size();
        }

        businessProcesses.getAgreements().size();
        businessProcesses.getAs4ConfigReceptionAwareness().size();
        businessProcesses.getAs4Reliability().size();
        businessProcesses.getErrorHandlings().size();
        businessProcesses.getLegConfigurations().size();
        businessProcesses.getMepBindings().size();
        businessProcesses.getMeps().size();
        businessProcesses.getPartyIdTypes().size();

        if (businessProcesses.getPayloadProfiles() != null) {
            for (final PayloadProfile payloadProfile : businessProcesses.getPayloadProfiles()) {
                payloadProfile.getPayloads().size();
            }
        }
        for (final PropertySet propertySet : businessProcesses.getPropertySets()) {
            propertySet.getProperties().size();
        }
        businessProcesses.getRoles().size();
        businessProcesses.getServices().size();

        // IMPORTANT NOTE:
        // This clear() detaches the configuration object from the entity manager.
        // This configuration object is the in-memory pmode cached by the pmode provider;
        // it needs to be detached from the entity manager, so that changes made at runtime
        // on these objects (e.g. adding dynamically discovered parties)
        // are not automatically persisted to the database
        this.em.clear();

        return configuration;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    //TODO: EDELIVERY-14161 PMode update instead of wipe
    public void updateConfiguration(final Configuration configuration) {
        if (this.configurationExists()) {
            this.delete(this.read());
        }
        this.create(configuration);
    }

    @Override
    @Transactional
    public void delete(final Configuration configuration) {
        super.delete(configuration);

        // We need these changes (i.e. the deletion) to be flushed before the transaction is committed,
        // to make sure there is no conflict (unique constraint violation) when a new configuration
        // (i.e. a new Configuration tree) is created immediately after (but during the same transaction).
        // Omitting the flush would trigger a unique constraint violation on the parties table.
        flush();
    }

}
