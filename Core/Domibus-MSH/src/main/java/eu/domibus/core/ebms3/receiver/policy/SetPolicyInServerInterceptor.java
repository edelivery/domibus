package eu.domibus.core.ebms3.receiver.policy;

import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.ebms3.model.Ebms3MessagingWithSecurityProfileMetadata;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pmode.PModeConstants;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.security.SecurityProfileMetadata;
import eu.domibus.common.ErrorCode;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.crypto.SecurityProfileHelper;
import eu.domibus.core.crypto.SecurityProfileProviderImpl;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.mapper.Ebms3Converter;
import eu.domibus.core.ebms3.receiver.interceptor.CheckEBMSHeaderInterceptor;
import eu.domibus.core.ebms3.receiver.interceptor.HeaderLoggingInterceptor;
import eu.domibus.core.ebms3.receiver.interceptor.SOAPMessageBuilderInterceptor;
import eu.domibus.core.ebms3.receiver.leg.LegConfigurationExtractor;
import eu.domibus.core.ebms3.receiver.leg.ServerInMessageLegConfigurationFactory;
import eu.domibus.core.ebms3.sender.client.DispatchClientDefaultProvider;
import eu.domibus.core.ebms3.sender.interceptor.CxfInterceptorsConfiguration;
import eu.domibus.core.message.TestMessageValidator;
import eu.domibus.core.message.UserMessageErrorCreator;
import eu.domibus.core.util.SoapUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import eu.domibus.messaging.MessageConstants;
import org.apache.commons.io.IOUtils;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.neethi.builders.converters.ConverterException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static eu.domibus.api.exceptions.DomibusCoreErrorCode.DOM_006;
import static eu.domibus.messaging.MessageConstants.RAW_MESSAGE_XML;


/**
 * @author Thomas Dussart
 * @author Cosmin Baciu
 * @since 4.1
 */
@Service
public class SetPolicyInServerInterceptor extends SetPolicyInInterceptor {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SetPolicyInServerInterceptor.class);

    protected final ServerInMessageLegConfigurationFactory serverInMessageLegConfigurationFactory;

    protected final TestMessageValidator testMessageValidator;

    protected final Ebms3Converter ebms3Converter;

    private final HeaderLoggingInterceptor headerLoggingInterceptor;

    protected final UserMessageErrorCreator userMessageErrorCreator;

    protected final SecurityProfileProviderImpl securityProfileProvider;

    protected final DomibusPropertyProvider domibusPropertyProvider;

    protected final DomainContextProvider domainContextProvider;

    protected SecurityProfileHelper securityProfileHelper;

    protected WSS4JInInterceptor wss4JInInterceptor;

    protected SoapUtil soapUtil;

    public SetPolicyInServerInterceptor(ServerInMessageLegConfigurationFactory serverInMessageLegConfigurationFactory,
                                        TestMessageValidator testMessageValidator, Ebms3Converter ebms3Converter,
                                        UserMessageErrorCreator userMessageErrorCreator, SecurityProfileProviderImpl securityProfileProvider,
                                        DomibusPropertyProvider domibusPropertyProvider, DomainContextProvider domainContextProvider,
                                        SecurityProfileHelper securityProfileHelper,
                                        HeaderLoggingInterceptor headerLoggingInterceptor,
                                        @Qualifier(CxfInterceptorsConfiguration.WSS4J_IN_INTERCEPTOR)
                                        WSS4JInInterceptor wss4JInInterceptor,
                                        SoapUtil soapUtil) {
        this.serverInMessageLegConfigurationFactory = serverInMessageLegConfigurationFactory;
        this.testMessageValidator = testMessageValidator;
        this.ebms3Converter = ebms3Converter;
        this.headerLoggingInterceptor = headerLoggingInterceptor;
        this.userMessageErrorCreator = userMessageErrorCreator;
        this.securityProfileProvider = securityProfileProvider;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domainContextProvider = domainContextProvider;
        this.securityProfileHelper = securityProfileHelper;
        this.wss4JInInterceptor = wss4JInInterceptor;
        this.soapUtil = soapUtil;
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {
        SecurityProfile securityProfile = null;
        String messageId = null;
        LegConfiguration legConfiguration;
        Ebms3Messaging ebms3Messaging = null;

        try {
            Ebms3MessagingWithSecurityProfileMetadata ebms3MessagingWithSecurityProfileMetadata = soapService.getMessageWithSecurityProfileMetadata(message);

            ebms3Messaging = ebms3MessagingWithSecurityProfileMetadata.getEbms3Messaging();

            message.getExchange().put(MessageConstants.EMBS3_MESSAGING_OBJECT, ebms3Messaging);
            message.put(DispatchClientDefaultProvider.MESSAGING_WITH_SECURITY_PROFILE_METADATA, ebms3MessagingWithSecurityProfileMetadata);
            message.put(DispatchClientDefaultProvider.MESSAGING_KEY_CONTEXT_PROPERTY, ebms3Messaging);
            LegConfigurationExtractor legConfigurationExtractor = serverInMessageLegConfigurationFactory.extractMessageConfiguration(message, ebms3Messaging);
            if (legConfigurationExtractor == null) {
                throw new DomibusCoreException("Could not determine leg for the incoming message");
            }

            securityProfile = getSecurityProfile(ebms3MessagingWithSecurityProfileMetadata);

            if (securityProfile == null) {
                throw new DomibusCoreException("Could not determine security profile for the incoming message");
            }

            //we need to call this method so that it sets the PModeConstants.PMODE_KEY_CONTEXT_PROPERTY on the message and message exchange
            legConfiguration = legConfigurationExtractor.extractMessageConfiguration(securityProfile);

            LOG.businessInfo(DomibusMessageCode.BUS_SECURITY_POLICY_INCOMING_USE, securityProfile);

            message.getInterceptorChain().add(new CheckEBMSHeaderInterceptor());
            message.getInterceptorChain().add(new SOAPMessageBuilderInterceptor());
            message.getInterceptorChain().add(wss4JInInterceptor);

            final Domain currentDomain = domainContextProvider.getCurrentDomain();

            final SecurityProfileConfiguration securityProfileConfiguration = securityProfileProvider.getSecurityProfileConfigurationWithRSAAsDefault(currentDomain, securityProfile.getCode());
            if (securityProfileConfiguration == null) {
                throw new DomibusCoreException("Could not determine security profile configuration for security profile [" + securityProfile.getCode() + "] for the incoming message");
            }

            message.getExchange().put(PModeConstants.SECURITY_PROFILE_CONFIGURATION, securityProfileConfiguration);

            boolean attachmentsPresent = soapUtil.attachmentExists(message);

            final Map<String, Object> incomingSecurityConfigurationAsMap = securityProfileHelper.getIncomingSecurityConfigurationAsMap(securityProfileConfiguration, attachmentsPresent);
            LOG.debug("Using the security profile configuration for the incoming message [{}]", incomingSecurityConfigurationAsMap);
            for (Map.Entry<String, Object> entry : incomingSecurityConfigurationAsMap.entrySet()) {
                message.put(entry.getKey(), entry.getValue());
            }

            saveRawMessageMessageContext(message);

        } catch (EbMS3Exception ex) {
            headerLoggingInterceptor.handleMessage(PhaseInterceptorChain.getCurrentMessage());

            setBindingOperation(message);
            LOG.debug("", ex); // Those errors are expected (no PMode found, therefore DEBUG)
            logIncomingMessagingException(message, ex);
            throw new Fault(ex);
        } catch (DomibusCoreException | IOException | JAXBException e) {
            headerLoggingInterceptor.handleMessage(PhaseInterceptorChain.getCurrentMessage());

            setBindingOperation(message);
            LOG.businessError(DomibusMessageCode.BUS_SECURITY_POLICY_INCOMING_NOT_FOUND, e, securityProfile); // Those errors are not expected
            throw new Fault(EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message("no valid security policy found")
                    .refToMessageId(ebms3Messaging != null ? messageId : "unknown")
                    .cause(e)
                    .mshRole(MSHRole.RECEIVING)
                    .build());
        }
    }


    private SecurityProfile getSecurityProfile(Ebms3MessagingWithSecurityProfileMetadata ebms3MessagingWithSecurityProfileMetadata) {
        SecurityProfileMetadata securityProfileMetadata = ebms3MessagingWithSecurityProfileMetadata.getSecurityProfileMetadata();
        return securityProfileProvider.determineSecurityProfile(domainContextProvider.getCurrentDomain(), securityProfileMetadata);
    }

    protected void saveRawMessageMessageContext(SoapMessage message) throws IOException {
        final InputStream inputStream = message.getContent(InputStream.class);
        if (inputStream instanceof ByteArrayInputStream) {
            LOG.trace("Saving the raw message envelope content (to have the encrypted data section)");
            String rawXMLMessage = IOUtils.toString(inputStream, "UTF-8");
            ((ByteArrayInputStream) inputStream).reset();

            message.getExchange().put(RAW_MESSAGE_XML, rawXMLMessage);
        } else {
            throw new DomibusCoreException(DOM_006, "Could not get the message content since it is not a byteArray stream.");
        }
    }

    protected void logIncomingMessagingException(SoapMessage message, EbMS3Exception ex) {
        if (message == null) {
            LOG.debug("SoapMessage is null");
            return;
        }
        try {
            String xml = soapService.getMessagingAsRAWXml(message);
            LOG.error("EbMS3Exception [{}] caused by incoming message: [{}]", ex, xml);
        } catch (ConverterException | IOException | EbMS3Exception | TransformerException e) {
            LOG.error("Error while getting Soap Envelope", e);
        }
    }

}
