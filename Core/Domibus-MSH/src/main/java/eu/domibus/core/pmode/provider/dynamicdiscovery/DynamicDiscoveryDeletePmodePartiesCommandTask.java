package eu.domibus.core.pmode.provider.dynamicdiscovery;

import eu.domibus.api.cluster.Command;
import eu.domibus.api.cluster.CommandProperty;
import eu.domibus.core.clustering.CommandTask;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Cosmin Baciu
 * @since 5.1.1
 */
@Service
public class DynamicDiscoveryDeletePmodePartiesCommandTask implements CommandTask {

    private static final DomibusLogger LOGGER = DomibusLoggerFactory.getLogger(DynamicDiscoveryDeletePmodePartiesCommandTask.class);

    protected final PModeProvider pModeProvider;

    public DynamicDiscoveryDeletePmodePartiesCommandTask(PModeProvider pModeProvider) {
        this.pModeProvider = pModeProvider;
    }

    @Override
    public boolean canHandle(String command) {
        return StringUtils.equalsIgnoreCase(Command.DELETE_PMODE_PARTIES, command);
    }

    @Override
    public void execute(Map<String, String> properties) {
        LOGGER.debug("Deleting PMode parties command");

        final String pmodePartyNamesValue = properties.get(CommandProperty.PMODE_PARTY_NAMES);
        final List<String> pmodePartyNames = Arrays.asList(StringUtils.split(pmodePartyNamesValue, ","));
        if (CollectionUtils.isEmpty(pmodePartyNames)) {
            LOGGER.warn("PMode parties are empty. Nothing to delete");
            return;
        }
        pModeProvider.removeParties(pmodePartyNames);
    }
}
