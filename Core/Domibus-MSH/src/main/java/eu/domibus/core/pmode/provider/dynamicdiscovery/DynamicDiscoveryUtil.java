package eu.domibus.core.pmode.provider.dynamicdiscovery;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.pki.SecurityProfileProvider;
import eu.domibus.api.pki.SecurityProfileService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.core.crypto.SecurityProfileHelper;
import eu.domibus.core.multitenancy.DomainContextProviderImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.dynamicdiscovery.model.SMPEndpoint;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_TRANSPORTPROFILEAS_4;
import static org.apache.commons.lang3.StringUtils.trim;

/**
 * Utility class for Dynamic Discovery
 *
 * @author Lucian FURCA
 * @since 5.1
 */
@Service
public class DynamicDiscoveryUtil {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DynamicDiscoveryUtil.class);

    protected final DomibusPropertyProvider domibusPropertyProvider;

    protected final SecurityProfileProvider securityProfileProvider;
    private final DomainContextProviderImpl domainContextProviderImpl;
    private final SecurityProfileHelper securityProfileHelper;
    protected SecurityProfileService securityProfileService;

    public DynamicDiscoveryUtil(SecurityProfileService securityProfileService, DomibusPropertyProvider domibusPropertyProvider, SecurityProfileProvider securityProfileProvider, DomainContextProviderImpl domainContextProviderImpl, SecurityProfileHelper securityProfileHelper) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.securityProfileProvider = securityProfileProvider;
        this.domainContextProviderImpl = domainContextProviderImpl;
        this.securityProfileHelper = securityProfileHelper;
        this.securityProfileService = securityProfileService;
    }

    /**
     * Return trimmed Domibus property value
     *
     * @param propertyName Domibus property name
     * @return value for the given Domibus property name
     */
    public String getTrimmedDomibusProperty(String propertyName) {
        return trim(domibusPropertyProvider.getProperty(propertyName));
    }

    /**
     * Returns the available Transport Profile matching the highest ranking priority Security Profile.
     * The Security Profiles priority list is defined in the properties file.
     * If the priority list is not defined the transport profile value defined in the property file will be read.
     *
     * @param transportProfiles list of available transport profiles that are received from the SMP endpoint
     * @return the available Transport Profile matching the highest ranking priority Security Profile
     */
    public String getAvailableTransportProfileForHighestRankingSecurityProfile(List<String> transportProfiles) {
        final Domain currentDomain = domainContextProviderImpl.getCurrentDomain();

        if(securityProfileHelper.areSecurityProfilesDisabled()) {
            final String defaultTransportProfile = DOMIBUS_DYNAMICDISCOVERY_TRANSPORTPROFILEAS_4;
            LOG.debug("No security profiles priorities found for domain [{}]. Returning the default transport profile [{}]", currentDomain, defaultTransportProfile);
            return getTrimmedDomibusProperty(defaultTransportProfile);
        }
        List<SecurityProfile> securityProfilesPriorities = securityProfileProvider.getSecurityProfilesPriorityList(currentDomain);

        //find the Security Profile with the highest priority ranking that matches an available Transport Profile
        //the available security profiles are already sorted by priority
        return securityProfilesPriorities.stream()
                .filter(securityProfile -> {
                    final String securityProfileTransportProfilePropertyName = securityProfileHelper.getSecurityProfilePropertyName(securityProfile.getCode(), "transportProfile");
                    if (StringUtils.isBlank(securityProfileTransportProfilePropertyName)) {
                        LOG.debug("The transport profile property name for the security profile [{}] is not defined", securityProfile.getCode());
                        return false;
                    }
                    final String securityProfileTransportProfile = getTrimmedDomibusProperty(securityProfileTransportProfilePropertyName);
                    final boolean contains = transportProfiles.contains(securityProfileTransportProfile);
                    if (contains) {
                        LOG.debug("Found the transport profile [{}] for the highest ranking security profile [{}]", securityProfileTransportProfile, securityProfile.getCode());
                    }
                    return contains;
                })
                .map(securityProfile -> {
                            final String securityProfileTransportProfilePropertyName = securityProfileHelper.getSecurityProfilePropertyName(securityProfile.getCode(), "transportProfile");
                            return getTrimmedDomibusProperty(securityProfileTransportProfilePropertyName);
                        }
                )
                .findFirst()
                .orElse(null);
    }

    /**
     * Returns the list of Transport Profiles that are extracted from the Processes list
     *
     * @return a list of available Transport Profiles
     */
    public List<String> retrieveTransportProfilesFromProcesses(List<SMPEndpoint> processes) {
        List<String> transportProfiles = new ArrayList<>();
        processes.stream().forEach(
                process -> transportProfiles.add(process.getTransportProfile().getIdentifier())
        );

        return transportProfiles;
    }

}
