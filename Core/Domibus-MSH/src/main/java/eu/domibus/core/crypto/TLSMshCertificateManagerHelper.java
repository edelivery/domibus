package eu.domibus.core.crypto;

import eu.domibus.api.cache.DomibusLocalCacheService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

@Service
public class TLSMshCertificateManagerHelper {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSMshCertificateManagerHelper.class);

    protected final DomibusLocalCacheService domibusLocalCacheService;

    public TLSMshCertificateManagerHelper(DomibusLocalCacheService domibusLocalCacheService) {
        this.domibusLocalCacheService = domibusLocalCacheService;
    }

    @CacheEvict(value = TLSMshCertificateManagerImpl.TLS_CACHE, key = "#domainCode")
    public void resetCache(String domainCode) {
        LOG.trace("Evicting the TLS and dispatchClient caches.");
        this.domibusLocalCacheService.clearCache(DomibusLocalCacheService.DISPATCH_CLIENT);
    }
}
