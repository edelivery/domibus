package eu.domibus.core.crypto;

import eu.domibus.api.cluster.Command;
import eu.domibus.core.clustering.CommandTask;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Ionut Breaz
 * @since 5.2
 */
@Service
public class ReloadTLSGenericTruststoreCommandTask implements CommandTask {

    private static final DomibusLogger LOGGER = DomibusLoggerFactory.getLogger(ReloadTLSGenericTruststoreCommandTask.class);

    private final TLSGenericCertificateManagerHelper tlsGenericCertificateManagerHelper;

    public ReloadTLSGenericTruststoreCommandTask(TLSGenericCertificateManagerHelper tlsGenericCertificateManagerHelper) {
        this.tlsGenericCertificateManagerHelper = tlsGenericCertificateManagerHelper;
    }

    @Override
    public boolean canHandle(String command) {
        return StringUtils.equalsIgnoreCase(Command.RELOAD_TLS_GENERIC_TRUSTSTORE, command);
    }

    @Override
    public void execute(Map<String, String> properties) {
        LOGGER.debug("Resetting TLS Generic truststore cache command.");

        tlsGenericCertificateManagerHelper.resetCache();
    }
}
