package eu.domibus.core.property;

import eu.domibus.api.cache.DomibusLocalCacheService;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.DomibusPropertyChangeNotifier;
import eu.domibus.api.property.DomibusPropertyException;
import eu.domibus.api.property.DomibusPropertyMetadata;
import eu.domibus.api.util.DomibusIOUtil;
import eu.domibus.api.util.RegexUtil;
import eu.domibus.configuration.spi.DomibusConfigurationManagerSpi;
import eu.domibus.configuration.spi.DomibusResource;
import eu.domibus.core.configuration.DomibusConfigurationManagerSpiProvider;
import eu.domibus.core.converter.DomibusCoreMapper;
import eu.domibus.core.util.backup.BackupService;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomibusPropertyManagerExt;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_PROPERTY_BACKUP_HISTORY_MAX;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_PROPERTY_BACKUP_PERIOD_MIN;
import static eu.domibus.api.property.Module.MSH;
import static eu.domibus.api.property.Module.UNKNOWN;

/**
 * Responsible for changing the values of domibus properties
 *
 * @author Ion Perpegel
 * @since 4.2
 */
@Service
public class PropertyChangeManager {

    public static final String LINE_COMMENT_PREFIX = "#";
    public static final String PROPERTY_VALUE_DELIMITER = "=";

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PropertyChangeManager.class);

    private final GlobalPropertyMetadataManager globalPropertyMetadataManager;

    private final PropertyRetrieveManager propertyRetrieveManager;

    private final DomibusPropertyChangeNotifier propertyChangeNotifier;

    private final PropertyProviderHelper propertyProviderHelper;

    private final ConfigurableEnvironment environment;

    private final DomibusLocalCacheService domibusLocalCacheService;

    private final DomibusConfigurationService domibusConfigurationService;

    private final BackupService backupService;

    private final DomibusCoreMapper coreMapper;

    private final RegexUtil regexUtil;

    protected DomibusIOUtil domibusIoUtil;

    public PropertyChangeManager(GlobalPropertyMetadataManager globalPropertyMetadataManager,
                                 PropertyRetrieveManager propertyRetrieveManager,
                                 PropertyProviderHelper propertyProviderHelper,
                                 ConfigurableEnvironment environment,
                                 // needs to be lazy because we do have a conceptual cyclic dependency:
                                 // BeanX->PropertyProvider->PropertyChangeManager->PropertyChangeNotifier->PropertyChangeListenerX->BeanX
                                 @Lazy DomibusPropertyChangeNotifier propertyChangeNotifier, DomibusLocalCacheService domibusLocalCacheService,
                                 DomibusConfigurationService domibusConfigurationService, BackupService backupService, DomibusCoreMapper coreMapper, RegexUtil regexUtil,
                                 DomibusIOUtil domibusIoUtil) {
        this.propertyRetrieveManager = propertyRetrieveManager;
        this.globalPropertyMetadataManager = globalPropertyMetadataManager;
        this.propertyProviderHelper = propertyProviderHelper;
        this.environment = environment;
        this.propertyChangeNotifier = propertyChangeNotifier;
        this.domibusLocalCacheService = domibusLocalCacheService;
        this.domibusConfigurationService = domibusConfigurationService;
        this.backupService = backupService;
        this.coreMapper = coreMapper;
        this.regexUtil = regexUtil;
        this.domibusIoUtil = domibusIoUtil;
    }

    protected void setPropertyValue(Domain domain, String propertyName, String propertyValue, boolean broadcast) throws DomibusPropertyException {
        DomibusPropertyMetadata propMeta = globalPropertyMetadataManager.getPropertyMetadata(propertyName);

        //keep old value in case of an exception
        String oldValue = getInternalPropertyValue(domain, propertyName);

        //try to set the new value
        doSetPropertyValue(domain, propertyName, propertyValue);

        //let the custom property listeners do their job but on the unformatted value
        signalPropertyValueChanged(domain, propertyName, propertyValue, broadcast, propMeta, oldValue);
    }

    private String prepareValueForSaving(String propertyValue, DomibusPropertyMetadata propMeta) {
        if (propMeta.getTypeAsEnum() == DomibusPropertyMetadata.Type.REGEXP) {
            String serializableValue = propertyValue.replace("\\", "\\\\");
            LOG.debug("Property [{}] is of type REGEXP; escaping the value [{}] to [{}].", propMeta.getName(), propertyValue, serializableValue);
            return serializableValue;
        }
        return propertyValue;
    }

    protected String getInternalPropertyValue(Domain domain, String propertyName) {
        if (domain == null) {
            return propertyRetrieveManager.getInternalProperty(propertyName);
        }
        return propertyRetrieveManager.getInternalProperty(domain, propertyName);
    }

    protected void doSetPropertyValue(Domain domain, String propertyName, String propertyValue) {
        //keep old value in case of an exception
        String oldValue = getInternalPropertyValue(domain, propertyName);

        String propertyKey = getPropertyKey(domain, propertyName);

        //set the value
        setValueInDomibusPropertySource(propertyKey, propertyValue);

        try {
            saveInFile(domain, propertyName, propertyValue, propertyKey);
        } catch (Exception ex) {
            LOG.warn("Could not persist the property [{}] value [{}] in the file; reverting to [{}]", propertyName, propertyValue, oldValue, ex);
            setValueInDomibusPropertySource(propertyKey, oldValue);
            throw ex;
        }
    }

    private String getPropertyKey(Domain domain, String propertyName) {
        String propertyKey;
        //calculate property key
        if (propertyProviderHelper.isMultiTenantAware()) {
            // in multi-tenancy mode - some properties will be prefixed (depends on usage)
            propertyKey = computePropertyKeyInMultiTenancy(domain, propertyName);
        } else {
            // in single-tenancy mode - the property key is always the property name
            propertyKey = propertyName;
        }
        return propertyKey;
    }

    protected void signalPropertyValueChanged(Domain domain, String propertyName, String propertyValue,
                                              boolean broadcast, DomibusPropertyMetadata propMeta, String oldValue) {
        String domainCode = domain != null ? domain.getCode() : null;
        boolean shouldBroadcast = broadcast && propMeta.isClusterAware();
        LOG.debug("Property [{}] changed its value on domain [{}], broadcasting is [{}]", propMeta, domainCode, shouldBroadcast);

        try {
            propertyChangeNotifier.signalPropertyValueChanged(domainCode, propertyName, propertyValue, shouldBroadcast);
        } catch (DomibusPropertyException ex) {
            LOG.error("An error occurred when executing property change listeners for property [{}]. Reverting to the former value.", propertyName, ex);
            try {
                // revert to old value. A "null" value is not supported, use "empty" instead.
                doSetPropertyValue(domain, propertyName, oldValue == null ? StringUtils.EMPTY : oldValue);
                //clear the cache manually here since we are not calling the set method through dispatcher class
                domibusLocalCacheService.evict(DomibusLocalCacheService.DOMIBUS_PROPERTY_CACHE, propertyProviderHelper.getCacheKeyValue(domain, propMeta));
                // the original property set failed likely due to the change listener validation so, there is no side effect produced and no need to call the listener again
                // propertyChangeNotifier.signalPropertyValueChanged(domainCode, propertyName, oldValue, shouldBroadcast);
            } catch (DomibusPropertyException ex2) {
                LOG.error("An error occurred while trying to revert property [{}] to the previous value [{}].", propertyName, oldValue, ex2);
            }
            throw ex;
        }
    }

    protected String computePropertyKeyInMultiTenancy(Domain domain, String propertyName) {
        String propertyKey = null;
        DomibusPropertyMetadata prop = globalPropertyMetadataManager.getPropertyMetadata(propertyName);
        if (domain != null && prop.isDomain()) {
            propertyKey = computePropertyKeyForDomain(domain, propertyName, prop);
        } else {
            propertyKey = computePropertyKeyWithoutDomain(propertyName, prop);
        }
        return propertyKey;
    }

    private String computePropertyKeyWithoutDomain(String propertyName, DomibusPropertyMetadata prop) {
        String propertyKey = propertyName;
        if (prop.isSuper()) {
            propertyKey = propertyProviderHelper.getPropertyKeyForSuper(propertyName);
        } else {
            if (!prop.isGlobal()) {
                String error = String.format("Property [%s] is not applicable for global usage so it cannot be set.", propertyName);
                throw new DomibusPropertyException(error);
            }
        }
        return propertyKey;
    }

    private String computePropertyKeyForDomain(Domain domain, String propertyName, DomibusPropertyMetadata prop) {
        String propertyKey;
        if (prop.isDomain()) {
            propertyKey = propertyProviderHelper.getPropertyKeyForDomain(domain, propertyName);
        } else {
            String error = String.format("Property [%s] is not applicable for a specific domain so it cannot be set.", propertyName);
            throw new DomibusPropertyException(error);
        }
        return propertyKey;
    }

    protected void setValueInDomibusPropertySource(String propertyKey, String propertyValue) {
        MutablePropertySources propertySources = environment.getPropertySources();
        DomibusPropertiesPropertySource domibusPropertiesPropertySource = (DomibusPropertiesPropertySource) propertySources.get(DomibusPropertiesPropertySource.NAME);
        domibusPropertiesPropertySource.setProperty(propertyKey, propertyValue);

        DomibusPropertiesPropertySource updatedDomibusPropertiesSource = (DomibusPropertiesPropertySource) propertySources.get(DomibusPropertiesPropertySource.UPDATED_PROPERTIES_NAME);
        updatedDomibusPropertiesSource.setProperty(propertyKey, propertyValue);
    }

    protected void saveInFile(Domain domain, String propertyName, String propertyValue, String propertyKey) {
        DomibusPropertyMetadata propMeta = globalPropertyMetadataManager.getPropertyMetadata(propertyName);
        String formattedPropertyValue = prepareValueForSaving(propertyValue, propMeta);
        
        DomibusResource propertyFile = getConfigurationFile(domain, propertyName);
        replacePropertyInFile(propertyFile, propertyKey, formattedPropertyValue, domain);
    }

    private DomibusResource getConfigurationFile(Domain domain, String propertyName) {
        DomibusPropertyMetadata propMeta = globalPropertyMetadataManager.getPropertyMetadata(propertyName);
        if (StringUtils.equalsAny(propMeta.getModule(), MSH, UNKNOWN)) {
            LOG.debug("Getting property file for MSH property [{}] on domain [{}].", propertyName, domain);
            String domibusPropertyFileName = getDomibusPropertyFileName(domain, propMeta);
            return getResource(domibusPropertyFileName);
        } else {
            DomibusPropertyManagerExt manager = globalPropertyMetadataManager.getManagerForProperty(propertyName);
            LOG.debug("Getting property file of external module [{}] property [{}] on domain [{}].", manager.getClass(), propertyName, domain);
            return getExternalModulePropertyFile(domain, manager, propMeta);
        }
    }

    private String getDomibusPropertyFileName(Domain domain, DomibusPropertyMetadata propMeta) {
        if (!propertyProviderHelper.isMultiTenantAware()) {
            String configurationFileName = domibusConfigurationService.getConfigurationFileName();
            LOG.debug("Properties file name in single-tenancy mode for property [{}] is [{}].", propMeta.getName(), configurationFileName);
            return configurationFileName;
        }
        if (domain != null && propMeta.isDomain()) {
            return getDomainDomibusPropertyFile(domain, propMeta);
        }
        return getGlobalOrSuperDomibusPropertyFileName(propMeta);
    }

    private String getGlobalOrSuperDomibusPropertyFileName(DomibusPropertyMetadata propMeta) {
        if (propMeta.isSuper()) {
            String configurationFileNameForSuper = domibusConfigurationService.getSuperConfigurationFileName();
            LOG.debug("Properties file name in multi-tenancy mode for super property [{}] is [{}].", propMeta.getName(), configurationFileNameForSuper);
            return configurationFileNameForSuper;
        }
        if (propMeta.isGlobal()) {
            String configurationFileName = domibusConfigurationService.getConfigurationFileName();
            LOG.debug("Properties file name in multi-tenancy mode for global property [{}] is [{}].", propMeta.getName(), configurationFileName);
            return configurationFileName;
        }
        throw new DomibusPropertyException(String.format("Property [%s] is not applicable for global or super usage so it cannot be set.", propMeta.getName()));
    }

    private String getDomainDomibusPropertyFile(Domain domain, DomibusPropertyMetadata propMeta) {
        if (propMeta.isDomain()) {
            String configurationFileName = domibusConfigurationService.getConfigurationFileName(domain);
            LOG.debug("Properties file name in multi-tenancy mode for property [{}] on domain [{}] is [{}].", propMeta.getName(), domain, configurationFileName);
            return configurationFileName;
        }
        throw new DomibusPropertyException(String.format("Property [%s] is not applicable for domain usage so it cannot be set.", propMeta.getName()));
    }

    private DomibusResource getExternalModulePropertyFile(Domain domain, DomibusPropertyManagerExt manager, DomibusPropertyMetadata propMeta) {
        if (!propertyProviderHelper.isMultiTenantAware()) {
            String configurationFileName = manager.getConfigurationFileName();
            LOG.debug("Properties file name in single-tenancy mode for property [{}] is [{}].", propMeta.getName(), configurationFileName);
            return getOrCreateDomibusResource(domain, propMeta, configurationFileName);
        }

        if (domain != null && propMeta.isDomain()) {
            return getDomainExternalModulePropertyFile(domain, manager, propMeta);
        }
        return getGlobalExternalModulePropertyFile(manager, propMeta);
    }

    private DomibusResource getOrCreateDomibusResource(Domain domain, DomibusPropertyMetadata propMeta, String configurationFileName) {
        DomibusResource resource = null;
        try {
            resource = getResource(configurationFileName);
        } catch (DomibusCoreException e) {
            LOG.info("Domain properties file for module [{}] and domain [{}] could not be found at the location [{}]", propMeta.getName(), domain, configurationFileName);
            //we write the stacktrace in debug
            LOG.debug("Domain properties file for module [{}] and domain [{}] could not be found at the location [{}]", propMeta.getName(), domain, configurationFileName, e);
        }


        if (resource == null) {
            //we create an empty resource so that the property will be written into later on
            resource = new DomibusResource(configurationFileName, configurationFileName, configurationFileName, null, null, null);
        }
        return resource;
    }

    private DomibusResource getGlobalExternalModulePropertyFile(DomibusPropertyManagerExt manager, DomibusPropertyMetadata propMeta) {
        if (propMeta.isGlobal()) {
            String configurationFileName = manager.getConfigurationFileName();
            LOG.debug("Properties file name in multi-tenancy mode for global property [{}] is [{}].", propMeta.getName(), configurationFileName);

            try {
                return getResource(configurationFileName);
            } catch (DomibusCoreException e) {
                throw new DomibusPropertyException(String.format("Global properties file for module [%s] could not be found at the location [%s]",
                        propMeta.getName(), configurationFileName));
            }
        }
        throw new DomibusPropertyException(String.format("Property [%s] is not applicable for global usage so it cannot be set.", propMeta.getName()));
    }

    private DomibusResource getDomainExternalModulePropertyFile(Domain domain, DomibusPropertyManagerExt manager, DomibusPropertyMetadata propMeta) {
        if (propMeta.isDomain()) {
            DomainDTO extDomain = coreMapper.domainToDomainDTO(domain);
            String configurationFileName = manager.getConfigurationFileName(extDomain)
                    .orElseThrow(() -> new DomibusPropertyException(String.format("Could not find properties file name for external module [%s] on domain [%s].",
                            manager.getClass(), domain)));
            LOG.debug("Properties file name in multi-tenancy mode for property [{}] on domain [{}] is [{}].", propMeta.getName(), domain, configurationFileName);
            return getOrCreateDomibusResource(domain, propMeta, configurationFileName);
        }
        throw new DomibusPropertyException(String.format("Property [%s] is not applicable for domain usage so it cannot be set.", propMeta.getName()));
    }

    private DomibusResource getResource(String domibusPropertyFileName) {
        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();

        try {
            return domibusConfigurationProvider.getResource(domibusPropertyFileName, domibusPropertyFileName);
        } catch (DomibusCoreException e) {
            throw new DomibusPropertyException(String.format("Could not get content for file [%s]", domibusPropertyFileName), e);
        }
    }

    protected void replacePropertyInFile(DomibusResource configurationFile, String propertyName, String newPropertyValue, Domain domain) {
        final List<String> lines = replaceOrAddProperty(configurationFile, propertyName, newPropertyValue);

        manageBackups(configurationFile, domain);

        try (ByteArrayInputStream byteArrayInputStream = domibusIoUtil.linesToInputStream(lines)) {
            //we set the new content on the resource
            configurationFile.setInputStream(byteArrayInputStream);
            final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();
            domibusConfigurationProvider.storeResource(configurationFile);
        } catch (DomibusCoreException | IOException e) {
            throw new DomibusPropertyException(String.format("Could not write property [%s] to file [%s] ", propertyName, configurationFile), e);
        }

        /*try {
            Files.write(configurationFile.toPath(), lines);
        } catch (IOException e) {
            throw new DomibusPropertyException(String.format("Could not write property [%s] to file [%s] ", propertyName, configurationFile), e);
        }*/
    }

    protected List<String> replaceOrAddProperty(DomibusResource configurationFileResource, String
            propertyName, String newPropertyValue) {
        String propertyNameValueLine = propertyName + PROPERTY_VALUE_DELIMITER + newPropertyValue;
        try {
            List<String> lines = getDomibusResourceLines(configurationFileResource);
            // to make sure we do not replace a property that lists other props (like encryption.property)
            int i = findLineWithProperty(propertyName, lines);
            if (i >= 0) {
                LOG.debug("Replacing property [{}] with value [{}].", propertyName, newPropertyValue);
                lines.set(i, propertyNameValueLine);
            } else {
                // could not find, so just add at the end
                LOG.debug("Could not find property [{}] (probably it had a fall-back value set in the global file); adding value [{}] at the end of the file.",
                        propertyName, newPropertyValue);
                lines.add(propertyNameValueLine);
            }
            return lines;
        } catch (DomibusCoreException e) {
            throw new DomibusPropertyException(String.format("Could not replace property [%s] with value [%s]", propertyName, newPropertyValue), e);
        }
    }

    protected List<String> getDomibusResourceLines(DomibusResource configurationFileResource) {
        try (final InputStream inputStream = configurationFileResource.getInputStream()) {
            if (inputStream == null) {
                return new ArrayList<>();
            }
            return domibusIoUtil.readLines(inputStream);
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not read lines from resource [" + configurationFileResource + "]", e);
        }
    }

    public int findLineWithProperty(String propertyName, List<String> lines) {
        String valueToSearch = LINE_COMMENT_PREFIX + "?" + propertyName + "\\s*" + PROPERTY_VALUE_DELIMITER + ".*";
        // go backwards so that, in case there are more than one line with the same property, replace the last one because it has precedence
        for (int i = lines.size() - 1; i >= 0; i--) {
            String line = lines.get(i);
            if (regexUtil.matches(valueToSearch, StringUtils.trim(line))) {
                // found it, exit
                LOG.debug("Found property [{}] in file.", propertyName);
                return i;
            }
        }
        return -1;
    }

    private void manageBackups(DomibusResource configurationResource, Domain domain) {
        Integer period = getPropertyValueAsInteger(domain, DOMIBUS_PROPERTY_BACKUP_PERIOD_MIN, 24);
        try {
            backupService.backupFileIfOlderThan(configurationResource, "backups", period);
        } catch (DomibusCoreException e) {
            throw new DomibusPropertyException(String.format("Could not back up [%s]", configurationResource), e);
        }

        Integer maxFiles = getPropertyValueAsInteger(domain, DOMIBUS_PROPERTY_BACKUP_HISTORY_MAX, 10);
        try {
            backupService.deleteBackupsIfMoreThan(configurationResource, maxFiles);
        } catch (DomibusCoreException e) {
            LOG.info("Could not delete back up history for [{}]", configurationResource, e);
        }
    }

    protected Integer getPropertyValueAsInteger(Domain domain, String propertyName, int defaultValue) {
        Integer timeout;
        String propVal = null;
        try {
            propVal = getInternalPropertyValue(domain, propertyName);
            timeout = Integer.valueOf(propVal);
        } catch (final NumberFormatException e) {
            LOG.warn("Could not parse the property [{}] value [{}] to an integer; returning [{}].", propertyName, propVal, defaultValue, e);
            timeout = defaultValue;
        }
        return timeout;
    }
}
