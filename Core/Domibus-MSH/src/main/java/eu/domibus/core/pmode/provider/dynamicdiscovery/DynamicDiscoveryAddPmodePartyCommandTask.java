package eu.domibus.core.pmode.provider.dynamicdiscovery;

import eu.domibus.api.cluster.Command;
import eu.domibus.api.cluster.CommandProperty;
import eu.domibus.api.party.PartyService;
import eu.domibus.core.clustering.CommandTask;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Ion Perpegel
 * @since 5.2
 */
@Service
public class DynamicDiscoveryAddPmodePartyCommandTask implements CommandTask {

    private static final DomibusLogger LOGGER = DomibusLoggerFactory.getLogger(DynamicDiscoveryAddPmodePartyCommandTask.class);

    private final PartyService partyService;

    public DynamicDiscoveryAddPmodePartyCommandTask(PartyService partyService) {
        this.partyService = partyService;
    }

    @Override
    public boolean canHandle(String command) {
        return StringUtils.equalsIgnoreCase(Command.ADD_PMODE_PARTY, command);
    }

    @Override
    public void execute(Map<String, String> properties) {
        LOGGER.debug("Executing Add Party to PMode command");

        final String pmodePartyName = properties.get(CommandProperty.PMODE_PARTY_NAME);
        if (StringUtils.isEmpty(pmodePartyName)) {
            LOGGER.warn("Invalid command: PMode party is empty. Nothing to add");
            return;
        }

        //add to the pMode
        partyService.addDynamicParty(pmodePartyName);
    }
}
