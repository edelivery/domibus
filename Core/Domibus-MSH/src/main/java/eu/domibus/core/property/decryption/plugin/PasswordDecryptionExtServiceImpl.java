package eu.domibus.core.property.decryption.plugin;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.property.encryption.PasswordDecryptionService;
import eu.domibus.core.converter.DomibusCoreMapper;
import eu.domibus.core.property.encryption.PasswordEncryptionContextFactory;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.PasswordDecryptionExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author Soumya Chandran
 * @since 5.2
 */
@Service
public class PasswordDecryptionExtServiceImpl implements PasswordDecryptionExtService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PasswordDecryptionExtServiceImpl.class);

    protected final PasswordDecryptionService passwordDecryptionService;

    protected final DomibusCoreMapper coreMapper;

    public PasswordDecryptionExtServiceImpl(
            PasswordDecryptionService passwordDecryptionService,
            DomibusCoreMapper coreMapper) {
        this.passwordDecryptionService = passwordDecryptionService;
        this.coreMapper = coreMapper;
    }


    @Override
    public boolean isValueEncrypted(String propertyValue) {
        return passwordDecryptionService.isValueEncrypted(propertyValue);
    }

    @Override
    public String decryptProperty(DomainDTO domainDTO, String propertyName, String encryptedFormatValue) {
        LOG.debug("Decrypting property [{}] for domain [{}]", propertyName, domainDTO);
        final Domain domain = coreMapper.domainDTOToDomain(domainDTO);
        return passwordDecryptionService.decryptProperty(domain, propertyName, encryptedFormatValue);
    }

    @Override
    public String decryptPropertyIfEncrypted(DomainDTO domainDTO, String propertyName, String propertyValue) {
        LOG.debug("Decrypting an encrypted property [{}] for domain [{}]", propertyName, domainDTO);
        final Domain domain = coreMapper.domainDTOToDomain(domainDTO);
        return passwordDecryptionService.decryptPropertyIfEncrypted(domain, propertyName, propertyValue);
    }
}
