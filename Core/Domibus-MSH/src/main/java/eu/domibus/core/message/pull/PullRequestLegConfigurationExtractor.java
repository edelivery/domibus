package eu.domibus.core.message.pull;

import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.ebms3.model.Ebms3PullRequest;
import eu.domibus.api.model.MessageType;
import eu.domibus.api.pmode.PModeException;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.common.ErrorCode;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.receiver.leg.AbstractSignalLegConfigurationExtractor;
import eu.domibus.core.ebms3.receiver.leg.MessageLegConfigurationVisitor;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.message.MessageExchangeConfiguration;
import eu.domibus.core.message.MessageExchangeService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.cxf.binding.soap.SoapMessage;

import java.util.NoSuchElementException;
import java.util.Set;

/**
 * @author Thomas Dussart
 * @since 3.3
 */

public class PullRequestLegConfigurationExtractor extends AbstractSignalLegConfigurationExtractor {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PullRequestLegConfigurationExtractor.class);

    private MessageExchangeService messageExchangeService;

    public PullRequestLegConfigurationExtractor(SoapMessage message, Ebms3Messaging messaging) {
        super(message, messaging);
    }

    @Override
    protected String getMessageId() {
        return ebms3Messaging.getSignalMessage().getMessageInfo().getMessageId();
    }

    @Override
    public LegConfiguration process(SecurityProfile securityProfile) throws EbMS3Exception {
        message.put(MSHDispatcher.MESSAGE_TYPE_IN, MessageType.SIGNAL_MESSAGE);
        message.put(MSHDispatcher.MESSAGE_TYPE_OUT, MessageType.USER_MESSAGE);
        Ebms3PullRequest pullRequest = ebms3Messaging.getSignalMessage().getPullRequest();
        try {
            String mpc = pullRequest.getMpc();
            LOG.debug("Handling incoming pull request with mpc=[{}]", mpc);
            PullContext pullContext = messageExchangeService.extractProcessOnMpc(mpc);

            // NOTE: we know that there may be multiple matching legs, but we just choose one of them (the leg is not used below in a significant way)
            // TODO: maybe improve on this -> Ion Perpegel 03-01-25 [EDELIVERY-12876] PULL refactoring
            //the LegConfiguration should not be determined at this stage because we don't know what UserMessage will be sent back; the LegConfiguration should be determined after the UserMessage is retrieved from the database
            LegConfiguration legConfiguration = getLegConfiguration(securityProfile, pullContext);

            String initiatorPartyName = null;
            if (pullContext.getInitiator() != null) {
                LOG.debug("Get initiator from pull context");
                initiatorPartyName = pullContext.getInitiator().getName();
            } else if (messageExchangeService.forcePullOnMpc(mpc)) {
                LOG.debug("Extract initiator from mpc");
                initiatorPartyName = messageExchangeService.extractInitiator(mpc);
            }
            LOG.debug("Initiator is [{}]", initiatorPartyName);

            MessageExchangeConfiguration messageExchangeConfiguration = new MessageExchangeConfiguration(pullContext.getAgreement(),
                    pullContext.getResponder().getName(),
                    initiatorPartyName,
                    legConfiguration.getService().getName(),
                    legConfiguration.getAction().getName(),
                    legConfiguration.getName());
            LOG.debug("Extracted the exchange configuration, pModeKey is [{}]", messageExchangeConfiguration.getPmodeKey());
            setUpMessage(messageExchangeConfiguration.getPmodeKey());
            return legConfiguration;
        } catch (PModeException p) {
            EbMS3Exception ebMS3Exception =  EbMS3ExceptionBuilder.getInstance()
                    .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                    .message( "Error for pullrequest with mpc:" + pullRequest.getMpc() + " " + p.getMessage())
                    .cause(p)
                    .build();
            LOG.warn("Could not extract pull request leg configuration from pMode", ebMS3Exception);
            throw ebMS3Exception;
        }
    }

    private LegConfiguration getLegConfiguration(SecurityProfile securityProfile, PullContext pullContext) {
        Set<LegConfiguration> legConfigurations = pullContext.getProcess().getLegs();

        return legConfigurations.stream()
                //implement security profile after [EDELIVERY-12876] PULL refactoring is implemented
//                .filter(legConf -> isSecurityProfileProfileEqualInsideTheLegConfiguration(securityProfile, legConf))
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
    }

    private Boolean isSecurityProfileProfileEqualInsideTheLegConfiguration(SecurityProfile securityProfile, LegConfiguration legConfiguration) {
        return legConfiguration.getSecurity().getSecurityProfile() != null && legConfiguration.getSecurity().getSecurityProfile().equals(securityProfile);
    }

    @Override
    public void accept(MessageLegConfigurationVisitor visitor) {
        visitor.visit(this);
    }

    public void setMessageExchangeService(MessageExchangeService messageExchangeService) {
        this.messageExchangeService = messageExchangeService;
    }
}
