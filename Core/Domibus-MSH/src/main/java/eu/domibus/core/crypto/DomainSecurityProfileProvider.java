package eu.domibus.core.crypto;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.pki.SecurityProfileService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.security.SecurityProfileException;
import eu.domibus.api.util.DomibusStringUtil;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
public class DomainSecurityProfileProvider {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomainSecurityProfileProvider.class);

    private Domain domain;
    protected SecurityProfileService securityProfileService;
    protected SecurityProfileValidatorService securityProfileValidatorService;
    protected DomibusPropertyProvider domibusPropertyProvider;
    protected SecurityProfileHelper securityProfileHelper;
    protected DomibusStringUtil domibusStringUtil;

    protected List<SecurityProfileConfiguration> securityProfileAliasConfigurations = new ArrayList<>();


    public DomainSecurityProfileProvider(Domain domain,
                                         SecurityProfileService securityProfileService,
                                         SecurityProfileValidatorService securityProfileValidatorService,
                                         DomibusPropertyProvider domibusPropertyProvider,
                                         SecurityProfileHelper securityProfileHelper,
                                         DomibusStringUtil domibusStringUtil) {
        this.domain = domain;
        this.securityProfileService = securityProfileService;
        this.securityProfileValidatorService = securityProfileValidatorService;
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.securityProfileHelper = securityProfileHelper;
        this.domibusStringUtil = domibusStringUtil;
    }

    //we synchronize this method because we call the init method at runtime when the security profile properties are changed
    public synchronized void init() {
        LOG.info("Initializing security profile alias configurations for domain [{}]", domain);

        securityProfileAliasConfigurations.clear();

        //without Security Profiles
        boolean areSecurityProfilesDisabled = securityProfileHelper.areSecurityProfilesDisabled(domain);
        if (areSecurityProfilesDisabled) {
            LOG.info("Security profiles are disabled for domain [{}]: initializing the default configuration", domain);
            final SecurityProfileConfiguration defaultRSASecurityProfileAliasConfiguration = createDefaultRSASecurityProfileAliasConfiguration();
            addSecurityProfileAliasConfiguration(defaultRSASecurityProfileAliasConfiguration, "RSA");
        } else {
            LOG.info("Security profiles are enabled for domain [{}]: initializing the security profile alias configurations", domain);
            final List<SecurityProfileConfiguration> securityProfileAliasConfigurationsList = createSecurityProfileAliasConfigurations();
            for (SecurityProfileConfiguration securityProfileAliasConfiguration : securityProfileAliasConfigurationsList) {
                LOG.info("Adding security profile alias configuration for domain [{}]: [{}]", domain, securityProfileAliasConfiguration.getSecurityProfile().getName());
                addSecurityProfileAliasConfiguration(securityProfileAliasConfiguration, securityProfileAliasConfiguration.getSecurityProfile().getName());
            }
        }

        LOG.debug("Created security profile alias configurations for domain [{}]", domain);
    }

    public SecurityProfileConfiguration getSecurityProfileConfigurationWithRSAAsDefault(String securityProfileCode) {
        LOG.debug("Getting security profile configuration for domain [{}] and security profile code [{}]", domain, securityProfileCode);

        //for the sending side it can be that the security profile is not set on the leg in the PMode(for backward compatibility reasons)
        if (StringUtils.isBlank(securityProfileCode)) {
            //if no security profiles are used we get the default legacy RSA configuration
            if (securityProfileHelper.areSecurityProfilesDisabled(domain)) {
                LOG.debug("No security profiles are used: defaulting to RSA profile for domain [{}]", domain);
                return securityProfileAliasConfigurations.stream()
                        .filter(SecurityProfileConfiguration::isLegacyConfiguration)
                        .findFirst()
                        .orElse(null);
            } else {//security profiles are used but the security profile is not set in the PMode; we default to RSA profile for backward compatibility reasons
                LOG.debug("No security profile is set in the PMode: defaulting to RSA profile for domain [{}]", domain);
                return getSecurityProfileConfiguration(SecurityProfile.RSA);
            }
        }
        return securityProfileAliasConfigurations.stream()
                .filter(securityProfileConfiguration -> securityProfileConfiguration.getSecurityProfile().getCode().equalsIgnoreCase(securityProfileCode))
                .findFirst()
                .orElse(null);
    }

    /**
     * Returns a list of Security Profiles in the priority order specified by the domibus.security.profile.order property
     *
     * @return a list of ordered Security Profiles
     */
    public List<SecurityProfile> getSecurityProfilesPriorityList() {
        return securityProfileAliasConfigurations
                .stream()
                .map(SecurityProfileConfiguration::getSecurityProfile)
                .collect(Collectors.toList());
    }


    public SecurityProfile getSecurityProfileByName(String securityProfileName) {
        if (StringUtils.isBlank(securityProfileName)) {
            return null;
        }

        final List<SecurityProfileConfiguration> securityProfileConfigurations = getSecurityProfileConfigurations();
        return securityProfileConfigurations.stream()
                .map(SecurityProfileConfiguration::getSecurityProfile)
                .filter(securityProfile -> securityProfile.getCode().equalsIgnoreCase(securityProfileName))
                .findFirst()
                .orElse(null);
    }

    private List<SecurityProfileConfiguration> createSecurityProfileAliasConfigurations() {
        List<SecurityProfileConfiguration> result = new ArrayList<>();

        final List<SecurityProfile> definedSecurityProfiles = createDefinedSecurityProfiles();

        for (SecurityProfile definedSecurityProfile : definedSecurityProfiles) {
            final SecurityProfileConfiguration securityProfileAliasConfiguration = getSecurityProfileConfiguration(domain, definedSecurityProfile);
            result.add(securityProfileAliasConfiguration);
        }

        return result;
    }

    public SecurityProfileConfiguration getSecurityProfileConfiguration(String securityProfileCode) throws SecurityProfileException {
        if (StringUtils.isBlank(securityProfileCode)) {
            throw new SecurityProfileException("No security profile configuration found: provided code is null");
        }
        final List<SecurityProfileConfiguration> securityProfileAliasConfigurations = getSecurityProfileConfigurations();
        return securityProfileAliasConfigurations.stream()
                .filter(securityProfileConfiguration -> securityProfileConfiguration.getSecurityProfile().getCode().equalsIgnoreCase(securityProfileCode))
                .findFirst()
                .orElse(null);
    }

    /**
     * Returns the Security Profiles defined in the properties file
     *
     * @return the list of defined Security Profiles
     */
    private List<SecurityProfile> createDefinedSecurityProfiles() {
        List<String> domainSecurityProfiles = domibusPropertyProvider.getNestedProperties(domain, DOMIBUS_SECURITY_PROFILE);

        if (CollectionUtils.isEmpty(domainSecurityProfiles)) {
            throw new SecurityProfileException("No Security Profiles defined for domain [" + domain.getCode() + "]");
        }

        List<String> securityProfileNames = domainSecurityProfiles.stream()
                .filter(property -> StringUtils.containsNone(property, "."))
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(securityProfileNames)) {
            throw new SecurityProfileException("Security Profiles defined were incorrectly defined for domain [" + domain.getCode() + "]");
        }
        LOG.debug("The following security profiles were found [{}] for domain [{}]", securityProfileNames, domain.getCode());

        final List<SecurityProfile> securityProfiles = securityProfileNames.stream()
                .map(securityProfile -> {
                            String propertyNameSecurityProfileIsEnabled = securityProfileHelper.getSecurityProfilePropertyName(securityProfile, "enabled");
                            final String securityProfileIsEnabled = domibusPropertyProvider.getProperty(propertyNameSecurityProfileIsEnabled);
                            if (BooleanUtils.isTrue(Boolean.valueOf(securityProfileIsEnabled))) {
                                LOG.debug("The security profile [{}] is enabled", securityProfile);
                                String propertyNameSecurityProfileOrder = securityProfileHelper.getSecurityProfilePropertyName(securityProfile, "order");
                                final String securityProfileOrderValue = domibusPropertyProvider.getProperty(propertyNameSecurityProfileOrder);
                                if (StringUtils.isEmpty(securityProfileOrderValue)) {
                                    throw new SecurityProfileException("The security profile order [" + propertyNameSecurityProfileOrder + "] is not set for security profile [" + securityProfile + "]");
                                }
                                final int securityProfileOrder = Integer.parseInt(securityProfileOrderValue);
                                return new SecurityProfile(securityProfile, securityProfile, securityProfileOrder);
                            } else {
                                LOG.debug("The security profile [{}] is disabled", securityProfile);
                                return null;
                            }

                        }
                )
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(SecurityProfile::getOrder))
                .collect(Collectors.toList());

        LOG.debug("The following security profiles are enabled [{}] for domain [{}]", securityProfiles, domain.getCode());
        return securityProfiles;
    }

    private SecurityProfileConfiguration getSecurityProfileConfiguration(Domain domain, SecurityProfile definedSecurityProfile) {
        SecurityProfileConfiguration result = new SecurityProfileConfiguration();
        result.securityProfile(definedSecurityProfile);
        result.domain(domain);

        final String securityProfilePropertyName = getSecurityProfilePropertyName(definedSecurityProfile.getCode());

        final String matchExpressionValue = domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".expression");
        final Map<String, String> matchExpresionToMap = domibusStringUtil.keyValuesToMap(matchExpressionValue, ";");
        result.matchExpression(matchExpresionToMap);

        final boolean signatureEnabled = BooleanUtils.toBoolean(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".signature.enabled"));
        if (signatureEnabled) {
            LOG.debug("Signature is enabled for security profile [{}]", definedSecurityProfile.getCode());
            result
                    .signatureEnabled(true)
                    .pkiPath(BooleanUtils.toBoolean(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".signature.pkipath")))
                    .signaturePrivateKeyAlias(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".key.private.sign.alias"))
                    .signaturePrivateKeyPassword(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".key.private.sign.password"))
                    .encryptionPrivateKeyAlias(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".key.private.decrypt.alias"))
                    .encryptionPrivateKeyPassword(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".key.private.decrypt.password"))
                    .signatureOutKeyIdentifier(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.signature.keyIdentifier"))
                    .signatureOutSignedParts(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.signature.parts"))
                    .signatureOutSignatureAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.signature.signatureAlgorithm"))
                    .signatureOutDigestAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.signature.digestAlgorithm"))
                    .signatureInSignatureAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.signature.signatureAlgorithm"))
                    .signatureInDigestAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.signature.digestAlgorithm"));
        }

        final boolean encryptionEnabled = BooleanUtils.toBoolean(domibusPropertyProvider.getProperty(securityProfilePropertyName + ".encryption.enabled"));
        if (encryptionEnabled) {
            LOG.debug("Encryption is enabled for security profile [{}]", definedSecurityProfile.getCode());

            result
                    .encryptionEnabled(true)
                    .encryptionOutKeyIdentifier(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.keyIdentifier"))
                    .encryptionOutSymAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.symAlgorithm"))
                    .encryptionOutKeyTransportAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.keyTransportAlgorithm"))
                    .encryptionOutMGFAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.mgfAlgorithm"))
                    .encryptionOutDigestAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.digestAlgorithm"))
                    .encryptionOutEncryptionParts(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.parts"))
                    //for x25519
                    .encryptionOutKeyAgreementMethod(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.keyAgreementMethod"))
                    .encryptionOutKeyDerivationFunction(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.keyDerivationFunction"))
                    .encryptionInSymAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.symAlgorithm"))
                    .encryptionInKeyTransportAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.keyTransportAlgorithm"))
                    .encryptionInMGFAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.mgfAlgorithm"))
                    .encryptionInDigestAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.digestAlgorithm"))
                    //for x25519
                    .encryptionInKeyAgreementMethod(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.keyAgreementMethod"))
                    .encryptionInKeyDerivationFunction(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.keyDerivationFunction"))
            ;
        }
        return result;
    }

    private SecurityProfileConfiguration createDefaultRSASecurityProfileAliasConfiguration() {
        SecurityProfileConfiguration result = new SecurityProfileConfiguration();
        result.legacyConfiguration(true);
        result.domain(domain);

        //for the default profile we get the default values from the default RSA profile configuration
        final String securityProfilePropertyName = getSecurityProfilePropertyName("rsa");
        final String securityProfileDescription = domibusPropertyProvider.getProperty(domain, securityProfilePropertyName);
        final SecurityProfile securityProfile = new SecurityProfile("rsa", securityProfileDescription, 1);
        result.securityProfile(securityProfile);

        final Boolean signatureEnabled = domibusPropertyProvider.getBooleanProperty(domain, DOMIBUS_SECURITY_SIGNATURE_ENABLED);
        if (signatureEnabled) {
            LOG.debug("Signature is enabled for the default profile");
            result
                    .signatureEnabled(true)
                    .signaturePrivateKeyAlias(domibusPropertyProvider.getProperty(domain, DOMIBUS_SECURITY_KEY_PRIVATE_ALIAS))
                    .signaturePrivateKeyPassword(domibusPropertyProvider.getProperty(domain, DOMIBUS_SECURITY_KEY_PRIVATE_PASSWORD))
                    .encryptionPrivateKeyAlias(domibusPropertyProvider.getProperty(domain, DOMIBUS_SECURITY_KEY_PRIVATE_ALIAS))
                    .encryptionPrivateKeyPassword(domibusPropertyProvider.getProperty(domain, DOMIBUS_SECURITY_KEY_PRIVATE_PASSWORD))
                    .signatureOutKeyIdentifier(domibusPropertyProvider.getProperty(domain, DOMIBUS_SECURITY_SIGNATURE_KEY_IDENTIFIER))
                    .pkiPath(domibusPropertyProvider.getBooleanProperty(domain, DOMIBUS_SECURITY_SIGNATURE_PKI_PATH))
                    .signatureOutSignedParts(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.signature.parts"))
                    .signatureOutSignatureAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.signature.signatureAlgorithm"))
                    .signatureOutDigestAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.signature.digestAlgorithm"))
                    .signatureInSignatureAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.signature.signatureAlgorithm"))
                    .signatureInDigestAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.signature.digestAlgorithm"));
        }

        final Boolean encryptionEnabled = domibusPropertyProvider.getBooleanProperty(domain, DOMIBUS_SECURITY_ENCRYPTION_ENABLED);
        if (encryptionEnabled) {
            LOG.debug("Encryption is enabled for the default profile");

            result
                    .encryptionEnabled(true)
                    .encryptionOutKeyIdentifier(domibusPropertyProvider.getProperty(domain, DOMIBUS_SECURITY_ENCRYPTION_KEY_IDENTIFIER))
                    .encryptionOutSymAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.symAlgorithm"))
                    .encryptionOutKeyTransportAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.keyTransportAlgorithm"))
                    .encryptionOutMGFAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.mgfAlgorithm"))
                    .encryptionOutDigestAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.digestAlgorithm"))
                    .encryptionOutEncryptionParts(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".out.encryption.parts"))
                    .encryptionInSymAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.symAlgorithm"))
                    .encryptionInKeyTransportAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.keyTransportAlgorithm"))
                    .encryptionInMGFAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.mgfAlgorithm"))
                    .encryptionInDigestAlgorithm(domibusPropertyProvider.getProperty(domain, securityProfilePropertyName + ".in.encryption.digestAlgorithm"))
            ;
        }
        return result;
    }

    private String getSecurityProfilePropertyName(String securityProfileCode) {
        return DOMIBUS_SECURITY_PROFILE + "." + securityProfileCode;
    }

    private void addSecurityProfileAliasConfiguration(final SecurityProfileConfiguration configuration, String description) {
        final String signatureAliasValue = configuration.getSignaturePrivateKeyAlias();
        final String signaturePasswordValue = configuration.getSignaturePrivateKeyPassword();

        if (configuration.isSignatureEnabled()) {
            if (StringUtils.isBlank(signatureAliasValue)) {
                String message = String.format("The signature private key alias was not set for domain [%s]: ", domain);
                throw new ConfigurationException(message);
            }
            if (StringUtils.isBlank(signaturePasswordValue)) {
                String message = String.format("The signature private key password corresponding to the alias=[%s] was not set for domain [%s]: ", signatureAliasValue, domain);
                throw new ConfigurationException(message);
            }
            checkIfSignatureAliasIsDuplicated(signatureAliasValue, description);
        }

        final String encryptionAliasValue = configuration.getEncryptionPrivateKeyAlias();
        final String encryptionPasswordValue = configuration.getEncryptionPrivateKeyPassword();
        if (configuration.isEncryptionEnabled()) {
            if (StringUtils.isBlank(encryptionAliasValue)) {
                String message = String.format("The encryption private key alias was not set for domain [%s]: ", domain);
                throw new ConfigurationException(message);
            }
            if (StringUtils.isBlank(encryptionPasswordValue)) {
                String message = String.format("The encryption private key password corresponding to the alias=[%s] was not set for domain [%s]: ", encryptionAliasValue, domain);
                throw new ConfigurationException(message);
            }
            checkIfEncryptionAliasIsDuplicated(encryptionAliasValue, description);
        }
        securityProfileAliasConfigurations.add(configuration);
    }

    private void checkIfSignatureAliasIsDuplicated(String aliasValue, String aliasDescription) {
        Optional<SecurityProfileConfiguration> existing = securityProfileAliasConfigurations.stream()
                .filter(configuration -> configuration.getSignaturePrivateKeyAlias().equalsIgnoreCase(aliasValue))
                .findFirst();
        if (existing.isPresent()) {
            String message = getDuplicateErrorMessage(aliasValue, aliasDescription, existing.get());
            throw new ConfigurationException(message);
        }
    }

    private void checkIfEncryptionAliasIsDuplicated(String aliasValue, String aliasDescription) {
        Optional<SecurityProfileConfiguration> existing = securityProfileAliasConfigurations.stream()
                .filter(configuration -> configuration.getEncryptionPrivateKeyAlias().equalsIgnoreCase(aliasValue))
                .findFirst();
        if (existing.isPresent()) {
            String message = getDuplicateErrorMessage(aliasValue, aliasDescription, existing.get());
            throw new ConfigurationException(message);
        }
    }


    private String getDuplicateErrorMessage(String aliasValue, String aliasDescription, SecurityProfileConfiguration profileConfiguration) {
        if (securityProfileHelper.areSecurityProfilesDisabled(domain)) {
            return String.format("Both legacy single keystore alias [%s] and security profile alias [%s] for [%s] are defined for domain: [%s]",
                    aliasValue, profileConfiguration.getSignaturePrivateKeyAlias(), aliasDescription, domain);
        }
        return String.format("Keystore alias [%s] for [%s] already used on domain [%s]. All security profiles aliases (decrypt, sign) must be different from each other.",
                aliasValue, aliasDescription, domain);
    }

    public List<SecurityProfileConfiguration> getSecurityProfileConfigurations() {
        return securityProfileAliasConfigurations;
    }
}
