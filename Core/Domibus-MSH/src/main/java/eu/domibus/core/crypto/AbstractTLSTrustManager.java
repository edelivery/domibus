package eu.domibus.core.crypto;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashSet;

/**
 * @author Ionut Breaz
 * @since 5.2
 * <p>
 * X509TrustManager that combines a TLS Truststore with cacerts
 */

public abstract class AbstractTLSTrustManager implements X509TrustManager {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AbstractTLSTrustManager.class);

    protected String tlsTruststoreType;

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        try {
            HashSet<X509Certificate> result = new HashSet<>();

            X509TrustManager x509TrustManager = getX509TrustManager(false);
            if (x509TrustManager != null) {
                X509Certificate[] truststoreAcceptedIssuers = x509TrustManager.getAcceptedIssuers();
                result.addAll(Arrays.asList(truststoreAcceptedIssuers));
                LOG.info("Added [{}] truststore accepted issuer(s) for TLS {}", truststoreAcceptedIssuers.length, tlsTruststoreType);
            }

            if (shouldUseCacerts()) {
                X509TrustManager cacertsTm = getX509TrustManager(true);
                X509Certificate[] cacertsAcceptedIssuers = cacertsTm.getAcceptedIssuers();
                result.addAll(Arrays.asList(cacertsAcceptedIssuers));
                LOG.info("Added [{}] cacerts accepted issuer(s) for TLS {}", cacertsAcceptedIssuers.length, tlsTruststoreType);
            }

            if (LOG.isInfoEnabled()) {
                StringBuilder sb = new StringBuilder();
                for (X509Certificate certificate : result) {
                    String subjectDN = certificate.getSubjectX500Principal().toString();
                    if (sb.length() > 0) {
                        sb.append(", ");
                    }
                    sb.append(subjectDN);
                }
                LOG.info("Accepted issuers for TLS {}: {}", tlsTruststoreType, sb.length() == 0 ? "none" : sb.toString());
            }

            return result.toArray(new X509Certificate[0]);
        } catch (NoSuchAlgorithmException | KeyStoreException ex) {
            LOG.warn("Could not load accepted issuers for TLS {}.", tlsTruststoreType, ex);
            return null;
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain,
                                   String authType) throws CertificateException {
        X509TrustManager truststoreTrustManager = null;
        boolean anyTrustSourceUsed = false;
        try {
            truststoreTrustManager = getX509TrustManager(false);
        } catch (NoSuchAlgorithmException | KeyStoreException ex) {
            throw new CertificateException("Could not load TLS " + tlsTruststoreType + " certificates.", ex);
        }

        try {
            if (truststoreTrustManager != null) {
                LOG.debug("Performing validation of server trust against TLS {} certificates.", tlsTruststoreType);
                anyTrustSourceUsed = true;
                truststoreTrustManager.checkServerTrusted(chain, authType);
                LOG.info("Server trusted check against TLS {} certificates was successful", tlsTruststoreType);
                return;
            }
        } catch (CertificateException ex) {
            LOG.debug("Could not check the server trusted against TLS {} certificates", tlsTruststoreType, ex);
        }

        if (shouldUseCacerts()) {
            LOG.debug("Validation of server trust against cacerts certificates enabled for TLS {}.", tlsTruststoreType);
            X509TrustManager cacertsTrustManager;
            try {
                cacertsTrustManager = getX509TrustManager(true);
            } catch (NoSuchAlgorithmException | KeyStoreException ex) {
                throw new CertificateException("Could not load cacerts certificates.", ex);
            }
            anyTrustSourceUsed = true;
            cacertsTrustManager.checkServerTrusted(chain, authType);
            LOG.info("Server trusted check against cacerts certificates was successful for TLS {}", tlsTruststoreType);
            return;
        }

        if (!anyTrustSourceUsed) {
            LOG.error("No TLS server is trusted. No valid TLS {} truststore was configured and cacerts certificates for {} TLS are not enabled. ", tlsTruststoreType, tlsTruststoreType);
        }

        throw new CertificateException("Could not perform the validation of the server trust");
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain,
                                   String authType) throws CertificateException {
        X509TrustManager defaultTm;
        try {
            defaultTm = getX509TrustManager(true);
        } catch (NoSuchAlgorithmException | KeyStoreException ex) {
            LOG.warn("Could not load cacerts certificates.", ex);
            throw new CertificateException("Could not load cacerts certificates.", ex);
        }
        defaultTm.checkClientTrusted(chain, authType);
    }

    protected X509TrustManager getX509TrustManager(boolean isDefaultTrust) throws NoSuchAlgorithmException, KeyStoreException {
        KeyStore trustStore;
        if (isDefaultTrust) {
            LOG.debug("Getting cacerts certificates");
            trustStore = null;
        } else {
            LOG.debug("Getting TLS {} Truststore certificates", tlsTruststoreType);
            trustStore = getTrustStore();
            if (trustStore == null) {
                return null;
            }
        }

        X509TrustManager trustManager = null;
        try {
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            // Using null here initialises the TMF with the default trust store (cacerts).
            tmf.init(trustStore);

            for (TrustManager tm : tmf.getTrustManagers()) {
                if (tm instanceof X509TrustManager) {
                    trustManager = (X509TrustManager) tm;
                    break;
                }
            }
        } catch (NoSuchAlgorithmException | KeyStoreException ex) {
            LOG.warn("Could not load trustManager for SSL exchange. ", ex);
            throw ex;
        }

        return trustManager;
    }

    protected abstract boolean shouldUseCacerts();

    protected abstract KeyStore getTrustStore();
}
