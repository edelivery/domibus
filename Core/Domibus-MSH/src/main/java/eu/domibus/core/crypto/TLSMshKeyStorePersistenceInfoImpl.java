package eu.domibus.core.crypto;

import eu.domibus.api.crypto.TLSMshCertificateManager;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.property.DomibusRawPropertyProvider;
import org.springframework.stereotype.Service;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

@Service
public class TLSMshKeyStorePersistenceInfoImpl implements KeystorePersistenceInfo {

    protected final DomibusPropertyProvider domibusPropertyProvider;
    protected final DomibusRawPropertyProvider domibusRawPropertyProvider;
    protected final DomainContextProvider domainContextProvider;

    public TLSMshKeyStorePersistenceInfoImpl(DomibusPropertyProvider domibusPropertyProvider, DomibusRawPropertyProvider domibusRawPropertyProvider, DomainContextProvider domainContextProvider) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domibusRawPropertyProvider = domibusRawPropertyProvider;
        this.domainContextProvider = domainContextProvider;
    }

    @Override
    public String getName() {
        return TLSMshCertificateManager.TLS_MSH_KEYSTORE_NAME;
    }

    @Override
    public String getFileLocation() {
        return domibusPropertyProvider.getProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_KEYSTORE_LOCATION);
    }

    @Override
    public boolean isOptional() {
        return true;
    }

    @Override
    public String getType() {
        return domibusPropertyProvider.getProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_KEYSTORE_TYPE);
    }

    @Override
    public String getPassword() {
        return domibusRawPropertyProvider.getRawPropertyValue(getDomainCode(), DOMIBUS_SECURITY_TLS_KEYSTORE_PASSWORD);
    }

    @Override
    public String getKeyEntryPassword() {
        return domibusRawPropertyProvider.getRawPropertyValue(getDomainCode(), DOMIBUS_SECURITY_TLS_PRIVATE_PASSWORD);
    }

    @Override
    public void updatePassword(String password) {
        domibusPropertyProvider.setProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_KEYSTORE_PASSWORD, password);
    }

    @Override
    public void updateType(String type) {
        domibusPropertyProvider.setProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_KEYSTORE_TYPE, type);
    }

    @Override
    public void updateFileLocation(String fileLocation) {
        domibusPropertyProvider.setProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_KEYSTORE_LOCATION, fileLocation);
    }

    @Override
    public boolean isDownloadable() {
        return true;
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public String toString() {
        return getName() + ":" + getFileLocation() + ":" + getType() + ":" + getPassword();
    }

    protected Domain getDomainCode() {
        return domainContextProvider.getCurrentDomain();
    }
}
