package eu.domibus.core.crypto;

import eu.domibus.api.crypto.TLSGenericSSLContext;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_SECURE_SOCKET_PROTOCOL;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_SUPPORTED_PROTOCOLS;

/**
 * @author Ionut Breaz
 * @since 5.2
 */

@Service
public class TLSGenericSSLContextImpl implements TLSGenericSSLContext {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSGenericSSLContextImpl.class);
    private final TLSGenericTrustManager tlsGenericTrustManager;
    private final DomibusPropertyProvider domibusPropertyProvider;

    public TLSGenericSSLContextImpl(TLSGenericTrustManager tlsGenericTrustManager,
                                    DomibusPropertyProvider domibusPropertyProvider) {
        this.tlsGenericTrustManager = tlsGenericTrustManager;
        this.domibusPropertyProvider = domibusPropertyProvider;
    }

    @Override
    public SSLConnectionSocketFactory getNewSSLConnectionSocketFactory() throws NoSuchAlgorithmException, KeyManagementException {
        String sslProtocol = domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_TLS_GENERIC_SECURE_SOCKET_PROTOCOL);
        String[] supportedProtocols = getSupportedProtocols();
        return getNewSSLConnectionSocketFactory(sslProtocol, supportedProtocols);
    }

    @Override
    public SSLConnectionSocketFactory getNewSSLConnectionSocketFactory(String sslProtocol, String[] supportedProtocols) throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = getNewTLSGenericSSLContext(sslProtocol);
        return new SSLConnectionSocketFactory(
                sslContext,
                supportedProtocols,
                null,
                new DefaultHostnameVerifier());
    }

    @Override
    public SSLContext getNewTLSGenericSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        String sslProtocol = domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_TLS_GENERIC_SECURE_SOCKET_PROTOCOL);
        return getNewTLSGenericSSLContext(sslProtocol);
    }

    @Override
    public SSLContext getNewTLSGenericSSLContext(String sslProtocol) throws NoSuchAlgorithmException, KeyManagementException {
        try {
            SSLContext sslContext = SSLContext.getInstance(sslProtocol);
            sslContext.init(null, new TrustManager[]{tlsGenericTrustManager}, null);
            return sslContext;
        } catch (Exception ex) {
            LOG.error("Could not instantiate TLS Generic sslContext.", ex);
            throw ex;
        }
    }

    @Override
    public String[] getSupportedProtocols() {
        String supportedProtocols = domibusPropertyProvider.getProperty(DOMIBUS_SECURITY_TLS_GENERIC_SUPPORTED_PROTOCOLS);
        if (StringUtils.isBlank(supportedProtocols)) {
            return null;
        }
        return supportedProtocols.split("#");
    }
}
