package eu.domibus.core.util.backup;

import eu.domibus.configuration.spi.DomibusResource;

import java.io.IOException;
import java.util.List;

/**
 * @author Ion Perpegel
 * @since 4.1.1
 * <p>
 * Utility service used to back-up files before updating them.
 */
public interface BackupService {

    /**
     * Creates a copy of the originalFile, in the same folder.
     *
     * @param originalFile the file to be backed-up.
     * @throws IOException when the backup file cannot be written.
     * @implNote The backup file is named using the following convention: original_filename.backup-yyyy-MM-dd_HH_mm_ss.SSS
     */
    void backupFile(String originalFile);

    byte[] writeLinesAsBytes(List<String> lines);

    void backupFile(String originalFile, String subFolder);

    /**
     * Creates a copy of the originalFile, in the backup folder.
     *
     * @param originalFile the file to be backed-up.
     * @throws eu.domibus.api.exceptions.DomibusCoreException when the backup file cannot be written.
     * @implNote The backup file is named using the following convention: original_filename.backup-yyyy-MM-dd_HH_mm_ss.SSS
     */
    void backupFileInLocation(DomibusResource originalFile, String trustStoreBackupLocation);

    void backupFileIfOlderThan(DomibusResource configurationResource, String location, Integer timeout);

    void deleteBackupsIfMoreThan(DomibusResource originalFile, Integer maxFilesToKeep);
}
