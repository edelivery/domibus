package eu.domibus.core.spring;

import com.google.common.collect.Sets;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.plugin.PluginException;
import eu.domibus.configuration.spi.DomibusConfigurationManagerSpi;
import eu.domibus.configuration.spi.DomibusResource;
import eu.domibus.configuration.spi.DomibusResourceType;
import eu.domibus.configuration.spi.SearchStrategy;
import eu.domibus.core.configuration.DomibusConfigurationManagerSpiProvider;
import eu.domibus.core.logging.LogbackLoggingConfigurator;
import eu.domibus.core.plugin.classloader.PluginClassLoader;
import eu.domibus.core.property.DomibusConfigLocationProvider;
import eu.domibus.core.property.DomibusPropertiesPropertySource;
import eu.domibus.core.property.DomibusPropertyConfiguration;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;
import org.springframework.jndi.JndiPropertySource;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;
import static eu.domibus.ext.services.DomibusPropertyManagerExt.DOMAINS_HOME;
import static eu.domibus.ext.services.DomibusPropertyManagerExt.PLUGINS_CONFIG_HOME;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
public class DomibusApplicationInitializerHelper {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusApplicationInitializerHelper.class);

    public static final String PLUGINS_LOCATION = "/plugins/lib";
    public static final String EXTENSIONS_LOCATION = "/extensions/lib";
    public static final String INTERNAL_EHCACHE_XML = "internal/ehcache.xml";

    public AnnotationConfigWebApplicationContext onStartup() {
        //get the Domibus config directory
        final String domibusConfigLocation = DomibusConfigLocationProvider.getDomibusConfigLocation();
        LOG.info("Using Domibus config location: [{}]", domibusConfigLocation);


        final DomibusConfigurationManagerSpi domibusConfigurationProvider = DomibusConfigurationManagerSpiProvider.getConfigurationProvider();

        boolean singleTenancy;
        DomibusPropertiesPropertySource propertySource = null;
        try {
            propertySource = createDomibusPropertiesPropertySource(domibusConfigurationProvider);

            String generalSchemaValue = (String) propertySource.getProperty(DomainService.GENERAL_SCHEMA_PROPERTY);
            //if general schema has a non null value, we are in multi tenancy
            singleTenancy = StringUtils.isBlank(generalSchemaValue);
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Error creating property source using DomibusConfigurationProvider " + domibusConfigurationProvider, e);
        }

        if (BooleanUtils.toBoolean((String) propertySource.getProperty(DOMIBUS_EXTENSION_IAM_PKCS11_ENABLED))) {
            LOG.info("Enabling PKCS#11 keystores");
            registerHsmProviders(propertySource);
        }

        BouncyCastleInitializer bouncyCastleInitializer = new BouncyCastleInitializer(propertySource);
        bouncyCastleInitializer.registerBouncyCastle();
        bouncyCastleInitializer.checkStrengthJurisdictionPolicyLevel();

        final File domibusConfigDirectory = new File(domibusConfigLocation);

        if (DomibusConfigurationManagerSpiProvider.isCustomConfigurationProviderUsed()) {
            LOG.info("Using the custom DomibusConfigurationProvider with identifier [{}] and class [{}]", domibusConfigurationProvider.getName(), domibusConfigurationProvider.getClass());
            //copy the logging configuration files to the local temporary directory
            copyLoggingConfigurationToLocalDirectory(domibusConfigDirectory, domibusConfigurationProvider, singleTenancy);
            copyDomibusEhcacheToLocalDirectory(domibusConfigDirectory, domibusConfigurationProvider);
            copyPluginsEhcacheToLocalDirectory(domibusConfigDirectory, domibusConfigurationProvider);
            copyPluginsAndExtensionsToLocalDirectory(domibusConfigDirectory, domibusConfigurationProvider);
        }

        //configure logback
        configureLogging(domibusConfigDirectory.getAbsolutePath(), singleTenancy);

        PluginClassLoader pluginClassLoader = createPluginClassLoader(domibusConfigDirectory.getAbsolutePath());
        Thread.currentThread().setContextClassLoader(pluginClassLoader);

        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(DomibusRootConfiguration.class, DomibusSessionConfiguration.class);
        rootContext.setClassLoader(pluginClassLoader);

        try {
            configurePropertySources(rootContext.getEnvironment(), domibusConfigurationProvider);
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Error configuring property sources", e);
        }

        return rootContext;
    }

    private void registerHsmProviders(final DomibusPropertiesPropertySource propertySource) {
        HsmInitializer hsmInitializer = new HsmInitializer();
        //at this point of the initialization process the Domibus domains are not yet loaded and the DomibusPropertyProvider is not instantiated
        //getting the domain names and domain specific properties
        Arrays.stream(propertySource.getPropertyNames())
                .filter(p -> p.matches("^[^/.]+." + DOMIBUS_EXTENSION_IAM_PKCS11_PROVIDER_NAME + "$"))
                .map(p -> p.replace("." + DOMIBUS_EXTENSION_IAM_PKCS11_PROVIDER_NAME, ""))
                .forEachOrdered(domain -> {
                    LOG.debug("Initializing security provider for domain [{}], unless it was already initialized in which case it will be reused", domain);
                    String pkcs11ConfigPath = (String) propertySource.getProperty(domain + "." + DOMIBUS_EXTENSION_IAM_PKCS11_CONFIGURATION_FILE);
                    String providerName = (String) propertySource.getProperty(domain + "." + DOMIBUS_EXTENSION_IAM_PKCS11_PROVIDER_NAME);
                    hsmInitializer.register(providerName, pkcs11ConfigPath);
                });
    }

    public String getPluginsConfigLocation() {
        return DomibusConfigLocationProvider.getDomibusConfigLocation() + File.separator + PLUGINS_CONFIG_HOME;
    }

    protected PluginClassLoader createPluginClassLoader(String domibusConfigLocation) {
        String normalizedDomibusConfigLocation = Paths.get(domibusConfigLocation).normalize().toString();
        String pluginsLocation = normalizedDomibusConfigLocation + PLUGINS_LOCATION;
        String extensionsLocation = normalizedDomibusConfigLocation + EXTENSIONS_LOCATION;

        LOG.info("Using plugins location [{}]", pluginsLocation);

        Set<File> pluginsDirectories = Sets.newHashSet(new File(pluginsLocation));
        if (StringUtils.isNotEmpty(extensionsLocation)) {
            LOG.info("Using extension location [{}]", extensionsLocation);
            pluginsDirectories.add(new File(extensionsLocation));
        }

        PluginClassLoader pluginClassLoader = null;
        try {
            pluginClassLoader = new PluginClassLoader(pluginsDirectories, Thread.currentThread().getContextClassLoader());
        } catch (MalformedURLException e) {
            throw new PluginException(DomibusCoreErrorCode.DOM_001, "Malformed URL Exception", e);
        }
        return pluginClassLoader;
    }

    private void copyPluginsAndExtensionsToLocalDirectory(File domibusTempConfigDirectory,
                                                          DomibusConfigurationManagerSpi domibusConfigurationProvider) {
        String pluginsLocation = "plugins/lib";
        final File localPluginsLocationDirectory = new File(domibusTempConfigDirectory, pluginsLocation);

        String extensionsLocation = "extensions/lib";
        final File localExtensionLocationDirectory = new File(domibusTempConfigDirectory, extensionsLocation);

        DomibusResourceUtil domibusFileResourceUtil = new DomibusResourceUtil();

        final List<DomibusResource> pluginJars = domibusConfigurationProvider.findFiles(
                pluginsLocation,
                ".jar",
                SearchStrategy.CURRENT_LOCATION_ENDING_WITH_PATTERN,
                new ArrayList<>(),
                DomibusResourceType.DOMIBUS_PLUGIN_JAR_FILE);
        for (DomibusResource pluginJar : pluginJars) {
            final File localPluginJarFile = new File(localPluginsLocationDirectory, pluginJar.getName());
            LOG.info("Copying plugin jar to local file [{}]", localPluginJarFile);
            domibusFileResourceUtil.copyToFile(pluginJar, localPluginJarFile);
        }

        final List<DomibusResource> extensionJars = domibusConfigurationProvider.findFiles(
                extensionsLocation,
                ".jar",
                SearchStrategy.CURRENT_LOCATION_ENDING_WITH_PATTERN,
                new ArrayList<>(),
                DomibusResourceType.DOMIBUS_EXTENSION_JAR_FILE);
        for (DomibusResource extensionJar : extensionJars) {
            final File localExtensionJarFile = new File(localExtensionLocationDirectory, extensionJar.getName());
            LOG.info("Copying extension jar to local file [{}]", localExtensionJarFile);
            domibusFileResourceUtil.copyToFile(extensionJar, localExtensionJarFile);
        }
    }

    protected void copyDomibusEhcacheToLocalDirectory(File targetDomibusConfigTempDirectory,
                                                      DomibusConfigurationManagerSpi domibusConfigurationProvider) {
        LOG.info("Copy ehcache.xml configuration file using the DomibusConfigurationProvider");

        final String ehcacheExternalFile = INTERNAL_EHCACHE_XML;
        DomibusResource ehcacheResource = null;
        try {
            LOG.info("Getting [{}] file", ehcacheExternalFile);
            ehcacheResource = domibusConfigurationProvider.getResource(ehcacheExternalFile, DomibusResourceType.DOMIBUS_EH_CACHE_FILE);
        } catch (Exception e) {
            LOG.info("Couldn't find [{}] file", ehcacheExternalFile);
            LOG.debug("Couldn't find [{}] file", ehcacheExternalFile, e);
            return;
        }

        DomibusResourceUtil domibusResourceUtil = new DomibusResourceUtil();
        final File ehcacheTargetFile = new File(targetDomibusConfigTempDirectory + "/" + ehcacheExternalFile);
        domibusResourceUtil.copyToFile(ehcacheResource, ehcacheTargetFile);
    }

    protected void copyPluginsEhcacheToLocalDirectory(File domibusTempConfigDirectory,
                                                      DomibusConfigurationManagerSpi domibusConfigurationProvider) {
        String pluginsLocation = PLUGINS_CONFIG_HOME;
        LOG.info("Copy plugin ehcache configuration files using the DomibusConfigurationProvider");

        final File localPluginsConfigLocationDirectory = new File(domibusTempConfigDirectory, pluginsLocation);

        DomibusResourceUtil domibusResourceUtil = new DomibusResourceUtil();

        final List<DomibusResource> ehcacheFiles = domibusConfigurationProvider.findFiles(
                pluginsLocation,
                "-plugin-ehcache.xml",
                SearchStrategy.CURRENT_LOCATION_ENDING_WITH_PATTERN,
                new ArrayList<>(),
                DomibusResourceType.DOMIBUS_PLUGIN_EH_CACHE_FILE);
        for (DomibusResource pluginEhcacheFile : ehcacheFiles) {
            final File localPluginEhcacheFile = new File(localPluginsConfigLocationDirectory, pluginEhcacheFile.getName());
            LOG.info("Copying plugin ehcache configuration to local file [{}]", localPluginEhcacheFile);
            domibusResourceUtil.copyToFile(pluginEhcacheFile, localPluginEhcacheFile);
        }
    }


    /**
     * Copies the main logback.xml file and the logback.xml for each domain on a temporary directory
     * These temporary logback configuration files are used to initialize logback
     */
    protected void copyLoggingConfigurationToLocalDirectory(File targetDomibusConfigTempDirectory,
                                                            DomibusConfigurationManagerSpi domibusConfigurationProvider,
                                                            boolean singleTenancy) {
        LOG.info("Copy logback configuration file using the DomibusConfigurationProvider [{}]", domibusConfigurationProvider.getName());
        final DomibusResource logbackResource = getLogbackFile(domibusConfigurationProvider);
        final byte[] logbackContentBytes = replaceDomibusConfigInLogbackConfiguration(logbackResource, targetDomibusConfigTempDirectory);

        final File logbackTargetFile = new File(targetDomibusConfigTempDirectory + "/" + logbackResource.getName());
        try {
            FileUtils.writeByteArrayToFile(logbackTargetFile, logbackContentBytes);
        } catch (IOException e) {
            throw new DomibusCoreException(DomibusCoreErrorCode.DOM_001, "Could not write logback content to file [" + logbackTargetFile + "]", e);
        }

        //if we are in multi tenancy
        if (!singleTenancy) {
            final List<String> domains = domibusConfigurationProvider.getDomains();
            for (String domain : domains) {
                copyLoggingConfigurationToLocalDirectory(targetDomibusConfigTempDirectory, domain, domibusConfigurationProvider);
            }
        }
    }

    /**
     * Replace all occurrences of ${domibus.config.location} with the temporary directory where the Domibus configuration is copied on local machine
     */
    protected byte[] replaceDomibusConfigInLogbackConfiguration(final DomibusResource logbackResource, File targetDomibusConfigTempDirectory) {
        DomibusResourceUtil domibusResourceUtil = new DomibusResourceUtil();
        final byte[] logbackByteArray = domibusResourceUtil.domibusResourceToByteArray(logbackResource);
        final String logbackContent = new String(logbackByteArray, StandardCharsets.UTF_8);
        final String logbackContentReplaced = StringUtils.replace(logbackContent, "${" + DOMIBUS_CONFIG_LOCATION + "}", targetDomibusConfigTempDirectory.getAbsolutePath());
        LOG.trace("The content of logback.xml after [{}] occurrences were replaced is: " + DOMIBUS_CONFIG_LOCATION, logbackContentReplaced);
        return logbackContentReplaced.getBytes(StandardCharsets.UTF_8);
    }

    protected void copyLoggingConfigurationToLocalDirectory(File targetDomibusConfigTempDirectory,
                                                            String domain,
                                                            DomibusConfigurationManagerSpi domibusConfigurationProvider) {
        final String domainLogbackFile = DOMAINS_HOME + "/" + domain + "/" + domain + "-logback.xml";
        LOG.info("Copy [{}] from the DomibusConfigurationProvider", domainLogbackFile);

        DomibusResource domainLogbackFileResource = null;
        try {
            domainLogbackFileResource = domibusConfigurationProvider.getResource(domainLogbackFile, DomibusResourceType.DOMIBUS_TENANT_LOGBACK_FILE);
        } catch (Exception e) {
            LOG.info("Couldn't get [{}] logback file", domainLogbackFile);
            LOG.debug("Couldn't get [{}] logback file", domainLogbackFile, e);
            return;
        }

        DomibusResourceUtil domibusResourceUtil = new DomibusResourceUtil();
        final File domainTargetFile = new File(targetDomibusConfigTempDirectory + "/" + domainLogbackFile);
        domibusResourceUtil.copyToFile(domainLogbackFileResource, domainTargetFile);
    }

    public String getExternalEhCacheFileLocation() {
        final File domibusConfigDirectory = new File(DomibusConfigLocationProvider.getDomibusConfigLocation());
        String externalEhCacheFile = domibusConfigDirectory.getAbsolutePath() + File.separator + INTERNAL_EHCACHE_XML;
        return externalEhCacheFile;
    }

    protected DomibusResource getLogbackFile(DomibusConfigurationManagerSpi domibusConfigurationProvider) {
        final String logbackTestFile = "logback-test.xml";
        try {
            LOG.info("Getting [{}] file", logbackTestFile);
            final DomibusResource logbackTestResource = domibusConfigurationProvider.getResource(logbackTestFile, DomibusResourceType.DOMIBUS_LOGBACK_FILE);
            return logbackTestResource;
        } catch (Exception e) {
            LOG.info("Couldn't find [{}] file. Trying to get [{}]", logbackTestFile, LogbackLoggingConfigurator.DEFAULT_LOGBACK_FILE_NAME);
            LOG.debug("Couldn't find [{}] file. Trying to get [{}]", logbackTestFile, LogbackLoggingConfigurator.DEFAULT_LOGBACK_FILE_NAME, e);
        }

        final DomibusResource logbackResource = domibusConfigurationProvider.getResource(LogbackLoggingConfigurator.DEFAULT_LOGBACK_FILE_NAME, DomibusResourceType.DOMIBUS_LOGBACK_FILE);
        return logbackResource;
    }


    protected void configureLogging(String domibusConfigLocation,
                                    boolean singleTenancy) {
        try {
            //we need to initialize the logging before Spring is being initialized
            LogbackLoggingConfigurator logbackLoggingConfigurator = new LogbackLoggingConfigurator(domibusConfigLocation, singleTenancy);
            logbackLoggingConfigurator.configureLogging();
        } catch (RuntimeException e) {
            //logging configuration problems should not prevent the application to startup
            LOG.warn("Error occurred while configuring logging", e);
        }
    }

    protected void configurePropertySources(ConfigurableEnvironment configurableEnvironment,
                                            DomibusConfigurationManagerSpi domibusConfigurationProvider) throws IOException {
        MutablePropertySources propertySources = configurableEnvironment.getPropertySources();

        DomibusPropertiesPropertySource updatedDomibusProperties = createUpdatedDomibusPropertiesSource();
        propertySources.addFirst(updatedDomibusProperties);

//        MapPropertySource domibusConfigLocationSource = createDomibusConfigLocationSource(domibusConfigurationProvider);
//        propertySources.addAfter(updatedDomibusProperties.getName(), domibusConfigLocationSource);

        DomibusPropertiesPropertySource domibusPropertiesPropertySource = createDomibusPropertiesPropertySource(domibusConfigurationProvider);
        propertySources.addLast(domibusPropertiesPropertySource);

        Set<PropertySource> toRemove = propertySources.stream()
                .filter(ps -> ps instanceof JndiPropertySource)
                .collect(Collectors.toSet());
        toRemove.forEach(ps -> {
            LOG.debug("Removing Jndi property source: [{}]", ps.getName());
            propertySources.remove(ps.getName());
        });
    }

    public DomibusPropertiesPropertySource createDomibusPropertiesPropertySource(DomibusConfigurationManagerSpi domibusConfigurationProvider) throws IOException {
        PropertiesFactoryBean propertiesFactoryBean = new DomibusPropertyConfiguration().domibusProperties(domibusConfigurationProvider);
        propertiesFactoryBean.setSingleton(false);
        Properties properties = propertiesFactoryBean.getObject();
        return new DomibusPropertiesPropertySource(DomibusPropertiesPropertySource.NAME, properties);
    }

   /* protected MapPropertySource createDomibusConfigLocationSource(DomibusConfigurationProvider domibusConfigurationProvider) {
        Map domibusConfigLocationMap = new HashMap();
        domibusConfigLocationMap.put(DomibusPropertyMetadataManagerSPI.DOMIBUS_CONFIG_LOCATION, domibusConfigurationProvider);
        return new MapPropertySource("domibusConfigLocationSource", domibusConfigLocationMap);
    }*/

    public DomibusPropertiesPropertySource createUpdatedDomibusPropertiesSource() {
        Properties properties = new Properties();
        return new DomibusPropertiesPropertySource(DomibusPropertiesPropertySource.UPDATED_PROPERTIES_NAME, properties);
    }

}
