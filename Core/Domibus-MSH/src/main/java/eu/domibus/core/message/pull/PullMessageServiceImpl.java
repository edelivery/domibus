package eu.domibus.core.message.pull;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.jms.JMSManager;
import eu.domibus.api.jms.JMSMessageBuilder;
import eu.domibus.api.messaging.MessageNotFoundException;
import eu.domibus.api.model.*;
import eu.domibus.api.pmode.PModeException;
import eu.domibus.api.reliability.ReliabilityException;
import eu.domibus.api.usermessage.UserMessageService;
import eu.domibus.common.ErrorCode;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.common.model.configuration.Process;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.sender.ResponseHandler;
import eu.domibus.core.ebms3.sender.ResponseResult;
import eu.domibus.core.ebms3.sender.retry.UpdateRetryLoggingService;
import eu.domibus.core.error.ErrorLogService;
import eu.domibus.core.generator.id.MessageIdGenerator;
import eu.domibus.core.message.MessageExchangeConfiguration;
import eu.domibus.core.message.MessageStatusDao;
import eu.domibus.core.message.UserMessageLogDefaultService;
import eu.domibus.core.message.nonrepudiation.UserMessageRawEnvelopeDao;
import eu.domibus.core.message.reliability.ReliabilityChecker;
import eu.domibus.core.message.retention.MessageRetentionDefaultService;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.pulling.PullRequest;
import eu.domibus.core.pulling.PullRequestDao;
import eu.domibus.core.scheduler.ReprogrammableService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.Queue;
import javax.xml.soap.SOAPMessage;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.domibus.core.message.pull.PullContext.PMODE_KEY;
import static eu.domibus.core.message.pull.PullContext.PULL_REQUEST_ID;
import static eu.domibus.jms.spi.InternalJMSConstants.PULL_MESSAGE_QUEUE;

@Service
public class PullMessageServiceImpl implements PullMessageService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PullMessageServiceImpl.class);

    public static final String MPC = "mpc";

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    @Qualifier(PULL_MESSAGE_QUEUE)
    private Queue pullMessageQueue;

    @Autowired
    protected JMSManager jmsManager;

    @Autowired
    protected PullRequestDao pullRequestDao;

    @Autowired
    protected PullFrequencyHelper pullFrequencyHelper;

    @Autowired
    private UserMessageLogDefaultService userMessageLogService;

    @Autowired
    private BackendNotificationService backendNotificationService;

    @Autowired
    private PullMessageStateService pullMessageStateService;

    @Autowired
    private MessageIdGenerator messageIdGenerator;

    @Autowired
    private ResponseHandler responseHandler;

    @Autowired
    private UpdateRetryLoggingService updateRetryLoggingService;

    @Autowired
    private UserMessageRawEnvelopeDao rawEnvelopeLogDao;

    @Autowired
    private MessagingLockDao messagingLockDao;

    @Autowired
    private PModeProvider pModeProvider;

    @Autowired
    protected MpcService mpcService;

    @Autowired
    private UserMessageService userMessageService;

    @Autowired
    private MessageRetentionDefaultService messageRetentionService;

    @Autowired
    protected MessageStatusDao messageStatusDao;

    @Autowired
    private ReprogrammableService reprogrammableService;

    @Autowired
    private ErrorLogService errorLogService;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePullMessageAfterRequest(final UserMessage userMessage,
                                              final String messageId,
                                              final LegConfiguration legConfiguration,
                                              final ReliabilityChecker.CheckResult state) {

        final MessagingLock lock = getLock(messageId);
        if (lock == null || MessageState.PROCESS != lock.getMessageState()) {
            LOG.warn("Message [{}] could not acquire lock when updating status, it has been handled by another process.", messageId);
            return;
        }

        UserMessageLog userMessageLog = userMessageLogService.findByMessageId(messageId, MSHRole.SENDING);
        final int sendAttempts = userMessageLog.getSendAttempts() + 1;
        LOG.debug("[PULL_REQUEST]:Message[{}]:Increasing send attempts to[{}]", messageId, sendAttempts);
        userMessageLog.setSendAttempts(sendAttempts);
        switch (state) {
            case WAITING_FOR_CALLBACK:
                waitingForCallBack(userMessage, legConfiguration, userMessageLog);
                break;
            case PULL_FAILED:
                pullFailedOnRequest(userMessage, legConfiguration, userMessageLog);
                break;
            case ABORT:
                pullMessageStateService.sendFailed(userMessageLog, userMessage);
                reprogrammableService.removeRescheduleInfo(lock);
                lock.setMessageState(MessageState.DEL);
                messagingLockDao.save(lock);
                break;
            default:
                throw new IllegalStateException(String.format("Status:[%s] should never occur here", state.name()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PullRequestResult updatePullMessageAfterReceipt(
            ReliabilityChecker.CheckResult reliabilityCheckSuccessful,
            ResponseHandler.ResponseStatus isOk,
            ResponseResult responseResult,
            SOAPMessage responseSoapMessage,
            UserMessageLog userMessageLog,
            LegConfiguration legConfiguration,
            UserMessage userMessage) {
        final String messageId = userMessage.getMessageId();
        LOG.debug("[releaseLockAfterReceipt]:Message:[{}] release lock]", messageId);

        switch (reliabilityCheckSuccessful) {
            case OK:
                if (responseResult != null) {
                    LOG.warn("Saving signal message to the database");
                    responseHandler.saveResponse(responseSoapMessage, userMessage, responseResult.getResponseMessaging());
                } else {
                    LOG.warn("Could not save signal message in the database, invalid response result");
                }
                switch (isOk) {
                    case OK:
                        userMessageLogService.setMessageAsAcknowledged(userMessage, userMessageLog);
                        LOG.debug("[PULL_RECEIPT]:Message:[{}] acknowledged.", messageId);
                        break;
                    case WARNING:
                        userMessageLogService.setMessageAsAckWithWarnings(userMessage, userMessageLog);
                        LOG.debug("[PULL_RECEIPT]:Message:[{}] acknowledged with warning.", messageId);
                        break;
                    default:
                        assert false;
                }
                backendNotificationService.notifyOfSendSuccess(userMessage, userMessageLog, legConfiguration.isAsyncNotification());
                LOG.businessInfo(userMessage.isTestMessage() ? DomibusMessageCode.BUS_TEST_MESSAGE_SEND_SUCCESS : DomibusMessageCode.BUS_MESSAGE_SEND_SUCCESS,
                        userMessage.getPartyInfo().getFromParty(), userMessage.getPartyInfo().getToParty());
                messageRetentionService.deletePayloadOnSendSuccess(userMessage, userMessageLog);

                userMessageLogService.update(userMessageLog);

                return new PullRequestResult(messageId, userMessageLog);
            case PULL_FAILED:
                return pullFailedOnReceipt(userMessage, legConfiguration, userMessageLog);

        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public String getPullMessageId(final String initiator, final String mpc) {
        PullMessageId pullMessageId = null;
        try {
            pullMessageId = messagingLockDao.getNextPullMessageToProcess(initiator, mpc);
        } catch (Exception ex) {
            LOG.error("Error while locking message ", ex);
        }
        if (pullMessageId != null) {
            LOG.debug("[PULL_REQUEST]:Message:[{}] retrieved", pullMessageId.getMessageId());
            final String messageId = pullMessageId.getMessageId();
            switch (pullMessageId.getState()) {
                case EXPIRED:
                    pullMessageStateService.expirePullMessage(messageId);
                    LOG.debug("[PULL_REQUEST]:Message:[{}] is staled for reason:[{}].", pullMessageId.getMessageId(), pullMessageId.getStaledReason());
                    break;
                case FIRST_ATTEMPT:
                    LOG.debug("[PULL_REQUEST]:Message:[{}] first pull attempt.", pullMessageId.getMessageId());
                    return messageId;
                case RETRY:
                    LOG.debug("[PULL_REQUEST]:message:[{}] retry pull attempt.", pullMessageId.getMessageId());
                    final UserMessage userMessage = userMessageService.getByMessageId(messageId, MSHRole.SENDING);
                    rawEnvelopeLogDao.deleteUserMessageRawEnvelope(userMessage.getEntityId());
                    return messageId;
            }
        }
        LOG.trace("[PULL_REQUEST]:No message found.");
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void addPullMessageLock(final UserMessage userMessage, String partyIdentifier, final String pModeKey,
                                   final UserMessageLog messageLog) {
        MessagingLock messagingLock = prepareMessagingLock(userMessage, partyIdentifier, pModeKey, messageLog);
        messagingLockDao.save(messagingLock);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void addPullMessageLock(final UserMessage userMessage,
                                   final UserMessageLog messageLog) {
        final String pmodeKey; // FIXME: This does not work for signalmessages
        try {
            pmodeKey = this.pModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, true).getPmodeKey();
        } catch (EbMS3Exception e) {
            throw new PModeException(DomibusCoreErrorCode.DOM_001, "Could not get the PMode key for message [" + userMessage.getMessageId() + "]", e);
        }
        addPullMessageLock(userMessage, userMessage.getPartyInfo().getToParty(), pmodeKey, messageLog);
    }

    private MessagingLock prepareMessagingLock(UserMessage userMessage, String partyIdentifier, String pModeKey, UserMessageLog messageLog) {
        final String messageId = userMessage.getMessageId();
        final String mpc = userMessage.getMpcValue();
        LOG.trace("Saving message lock with partyID:[{}], mpc:[{}]", partyIdentifier, mpc);
        final LegConfiguration legConfiguration = this.pModeProvider.getLegConfiguration(pModeKey);
        final Date staledDate = updateRetryLoggingService.getMessageExpirationDate(messageLog, legConfiguration);

        return new MessagingLock(
                messageId,
                partyIdentifier,
                mpc,
                messageLog.getReceived(),
                staledDate,
                messageLog.getNextAttempt() == null ? new Date() : messageLog.getNextAttempt(),
                messageLog.getTimezoneOffset(),
                messageLog.getSendAttempts(),
                messageLog.getSendAttemptsMax());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void deletePullMessageLock(final String messageId) {
        messagingLockDao.delete(messageId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public MessagingLock getLock(final String messageId) {
        return messagingLockDao.getLock(messageId);
    }

    /**
     * This method is called when a message has been pulled successfully.
     *
     * @param legConfiguration processing information for the message
     * @param userMessageLog   the user message
     */
    protected void waitingForCallBack(UserMessage userMessage, LegConfiguration legConfiguration, UserMessageLog
            userMessageLog) {
        final MessagingLock lock = messagingLockDao.findMessagingLockForMessageId(userMessage.getMessageId());
        if (updateRetryLoggingService.isExpired(legConfiguration, userMessageLog)) {
            LOG.debug("[WAITING_FOR_CALLBACK]:Message:[{}] expired]", userMessage.getMessageId());
            pullMessageStateService.sendFailed(userMessageLog, userMessage);
            reprogrammableService.removeRescheduleInfo(lock);
            lock.setMessageState(MessageState.DEL);
            messagingLockDao.save(lock);
            return;
        }
        LOG.debug("[WAITING_FOR_CALLBACK]:Message:[{}] change status to:[{}]", userMessage.getMessageId(), MessageStatus.WAITING_FOR_RECEIPT);
        updateRetryLoggingService.updateMessageLogNextAttemptDate(legConfiguration, userMessageLog);
        if (LOG.isDebugEnabled()) {
            if (attemptNumberLeftIsLowerOrEqualThenMaxAttempts(userMessageLog, legConfiguration)) {
                LOG.debug("[WAITING_FOR_CALLBACK]:Message:[{}] has been pulled [{}] times", userMessage.getMessageId(), userMessageLog.getSendAttempts());
                LOG.debug("[WAITING_FOR_CALLBACK]:Message:[{}] In case of failure, will be available for pull at [{}]", userMessage.getMessageId(), userMessageLog.getNextAttempt());
            } else {
                LOG.debug("[WAITING_FOR_CALLBACK]:Message:[{}] has no more attempt, it has been pulled [{}] times and it will be the last try.", userMessage.getMessageId(), userMessageLog.getSendAttempts());
            }
        }
        lock.setMessageState(MessageState.WAITING);
        lock.setSendAttempts(userMessageLog.getSendAttempts());
        reprogrammableService.setRescheduleInfo(lock, userMessageLog.getNextAttempt());
        messagingLockDao.save(lock);
 
        backendNotificationService.notifyOfMessageStatusChange(userMessage, userMessageLog, MessageStatus.WAITING_FOR_RECEIPT, new Timestamp(System.currentTimeMillis()), legConfiguration.isAsyncNotification());
        userMessageLog.setMessageStatus(messageStatusDao.findOrCreate(MessageStatus.WAITING_FOR_RECEIPT));
        userMessageLogService.update(userMessageLog);
    }

    /**
     * Check if the message can be sent again: there is time and attempts left
     *
     * @param userMessageLog   the message
     * @param legConfiguration processing information for the message
     * @return true if the message can be sent again
     */
    protected boolean attemptNumberLeftIsLowerOrEqualThenMaxAttempts(final UserMessageLog userMessageLog,
                                                                     final LegConfiguration legConfiguration) {
        // retries start after the first send attempt
        if (legConfiguration.getReceptionAwareness() != null && userMessageLog.getSendAttempts() <= userMessageLog.getSendAttemptsMax()
                && !updateRetryLoggingService.isExpired(legConfiguration, userMessageLog)) {
            return true;
        }
        return false;
    }


    protected boolean attemptNumberLeftIsStrictlyLowerThanMaxAttempts(final UserMessageLog userMessageLog, final LegConfiguration legConfiguration) {
        // retries start after the first send attempt
        if (legConfiguration.getReceptionAwareness() != null && userMessageLog.getSendAttempts() < userMessageLog.getSendAttemptsMax()
                && !updateRetryLoggingService.isExpired(legConfiguration, userMessageLog)) {
            return true;
        }
        return false;
    }

    protected void pullFailedOnRequest(UserMessage userMessage, LegConfiguration legConfiguration, UserMessageLog
            userMessageLog) {
        LOG.debug("[PULL_REQUEST]:Message:[{}] failed on pull message retrieval", userMessage.getMessageId());
        final MessagingLock lock = messagingLockDao.findMessagingLockForMessageId(userMessage.getMessageId());
        if (attemptNumberLeftIsStrictlyLowerThanMaxAttempts(userMessageLog, legConfiguration)) {
            LOG.debug("[PULL_REQUEST]:Message:[{}] has been pulled [{}] times", userMessage.getMessageId(), userMessageLog.getSendAttempts() + 1);
            updateRetryLoggingService.updateMessageLogNextAttemptDate(legConfiguration, userMessageLog);
            updateRetryLoggingService.saveAndNotify(userMessage, MessageStatus.READY_TO_PULL, userMessageLog);
            LOG.debug("[pullFailedOnRequest]:Message:[{}] release lock", userMessage.getMessageId());
            LOG.debug("[PULL_REQUEST]:Message:[{}] will be available for pull at [{}]", userMessage.getMessageId(), userMessageLog.getNextAttempt());
            lock.setMessageState(MessageState.READY);
            lock.setSendAttempts(userMessageLog.getSendAttempts());
            reprogrammableService.setRescheduleInfo(lock, userMessageLog.getNextAttempt());
        } else {
            reprogrammableService.removeRescheduleInfo(lock);
            lock.setMessageState(MessageState.DEL);
            LOG.debug("[PULL_REQUEST]:Message:[{}] has no more attempt, it has been pulled [{}] times", userMessage.getMessageId(), userMessageLog.getSendAttempts() + 1);
            pullMessageStateService.sendFailed(userMessageLog, userMessage);
        }
        messagingLockDao.save(lock);
    }

    protected PullRequestResult pullFailedOnReceipt(UserMessage userMessage, LegConfiguration legConfiguration, UserMessageLog
            userMessageLog) {
        LOG.debug("[PULL_RECEIPT]:Message:[{}] failed on pull message acknowledgement", userMessage.getMessageId());
        if (attemptNumberLeftIsStrictlyLowerThanMaxAttempts(userMessageLog, legConfiguration)) {
            LOG.debug("[PULL_RECEIPT]:Message:[{}] has been pulled [{}] times", userMessage.getMessageId(), userMessageLog.getSendAttempts() + 1);
            pullMessageStateService.reset(userMessageLog, userMessage.getMessageId());
            LOG.debug("[pullFailedOnReceipt]:Message:[{}] add lock", userMessage.getMessageId());
            LOG.debug("[PULL_RECEIPT]:Message:[{}] will be available for pull at [{}]", userMessage.getMessageId(), userMessageLog.getNextAttempt());

        } else {
            LOG.debug("[PULL_RECEIPT]:Message:[{}] has no more attempt, it has been pulled [{}] times", userMessage.getMessageId(), userMessageLog.getSendAttempts() + 1);
            pullMessageStateService.sendFailed(userMessageLog, userMessage);
        }
        return new PullRequestResult(userMessage.getMessageId(), userMessageLog);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(final MessagingLock messagingLock) {
        messagingLockDao.delete(messagingLock);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteInNewTransaction(final String messageId) {
        final MessagingLock lock = getLock(messageId);
        if (lock != null) {
            messagingLockDao.delete(lock);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void resetMessageInWaitingForReceiptState(final String messageId) {
        MessagingLock lock = getLock(messageId);
        if (lock == null) {
            return;
        }
        LOG.debug("[resetWaitingForReceiptPullMessages]:Message:[{}] checking if can be retry, attempts[{}], max attempts[{}], expiration date[{}]", lock.getMessageId(), lock.getSendAttempts(), lock.getSendAttemptsMax(), lock.getStaled());
        final UserMessageLog userMessageLog = userMessageLogService.findByMessageId(lock.getMessageId(), MSHRole.SENDING);
        LOG.debug("resetMessageInWaitingForReceiptState Found message [{}]", userMessageLog);
        if (userMessageLog == null) {
            throw new MessageNotFoundException(messageId);
        }
        if (lock.getSendAttempts() < lock.getSendAttemptsMax() && lock.getStaled().getTime() > System.currentTimeMillis()) {
            LOG.debug("[resetWaitingForReceiptPullMessages]:Message:[{}] set ready for pulling", lock.getMessageId());
            pullMessageStateService.reset(userMessageLog, messageId);
            lock.setMessageState(MessageState.READY);
            messagingLockDao.save(lock);
        } else {
            LOG.debug("[resetWaitingForReceiptPullMessages]:Message:[{}] send failed.", lock.getMessageId());
            lock.setMessageState(MessageState.DEL);
            reprogrammableService.removeRescheduleInfo(lock);
            messagingLockDao.save(lock);
            pullMessageStateService.sendFailed(userMessageLog, messageId);
        }
        if (userMessageLog.getMessageStatus() == MessageStatus.WAITING_FOR_RECEIPT) {
            errorLogService.createErrorLog(messageId, ErrorCode.EBMS_0301, "Receipt is missing", MSHRole.SENDING, null);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void expireMessage(String messageId, MSHRole mshRole) {
        MessagingLock lock = messagingLockDao.getLock(messageId);
        if (lock != null && MessageState.ACK != lock.getMessageState()) {
            LOG.debug("[bulkExpirePullMessages]:Message:[{}] expired.", lock.getMessageId());
            UserMessageLog userMessageLog = userMessageLogService.findByMessageId(lock.getMessageId(), mshRole);
            if (userMessageLog == null) {
                throw new MessageNotFoundException(messageId);
            }
            if (userMessageLog.getMessageStatus() == MessageStatus.WAITING_FOR_RECEIPT) {
                errorLogService.createErrorLog(messageId, ErrorCode.EBMS_0301, "Receipt is missing", MSHRole.SENDING, null);
            }
            pullMessageStateService.sendFailed(userMessageLog, messageId);
            delete(lock);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void releaseLockAfterReceipt(final PullRequestResult requestResult) {
        LOG.debug("[releaseLockAfterReceipt]:Message:[{}] release lock]", requestResult.getMessageId());
        final MessagingLock lock = messagingLockDao.findMessagingLockForMessageId(requestResult.getMessageId());
        switch (requestResult.getMessageStatus()) {
            case READY_TO_PULL:
                lock.setMessageState(MessageState.READY);
                lock.setSendAttempts(requestResult.getSendAttempts());
                reprogrammableService.setRescheduleInfo(lock, requestResult.getNextAttempts());
                messagingLockDao.save(lock);
                break;
            case SEND_FAILURE:
                lock.setMessageState(MessageState.DEL);
                reprogrammableService.removeRescheduleInfo(lock);
                messagingLockDao.save(lock);
                break;
            case ACKNOWLEDGED:
                reprogrammableService.removeRescheduleInfo(lock);
                lock.setMessageState(MessageState.ACK);
                messagingLockDao.delete(lock);
                break;
        }
        LOG.debug("[releaseLockAfterReceipt]:Message:[{}] receive receiptResult  with status[{}] and next attempt:[{}].", requestResult.getMessageId(), requestResult.getMessageStatus(), requestResult.getNextAttempts());
        LOG.debug("[releaseLockAfterReceipt]:Message:[{}] release lock  with status[{}] and next attempt:[{}].", lock.getMessageId(), lock.getMessageState(), lock.getNextAttempt());

    }

    @Override
    public boolean isPull(UserMessage userMessage) {
        return mpcService.forcePullOnMpc(userMessage) || userMessageLogService.isPull(userMessage.getEntityId());
    }

    @Transactional
    @Override
    public void sendPullMessage(LegConfiguration legConfiguration, MessageExchangeConfiguration messageExchangeConfiguration) {
        eu.domibus.core.pulling.PullRequest pullRequest = new PullRequest();
        String uuid = messageIdGenerator.generatePullRequestId();
        pullRequest.setUuid(uuid);
        pullRequest.setMpc(legConfiguration.getDefaultMpc().getQualifiedName());
        LOG.trace("Sending pull request with UUID:[{}], MPC:[{}]", pullRequest.getUuid(), pullRequest.getMpc());
        pullRequestDao.savePullRequest(pullRequest);
        jmsManager.sendMapMessageToQueue(JMSMessageBuilder.create()
                .property(MPC, legConfiguration.getDefaultMpc().getQualifiedName())
                .property(PULL_REQUEST_ID, uuid)
                .property(PMODE_KEY, messageExchangeConfiguration.getReversePmodeKey())
                .property(PullContext.NOTIFY_BUSINNES_ON_ERROR, String.valueOf(legConfiguration.getErrorHandling().isBusinessErrorNotifyConsumer()))
                .build(), pullMessageQueue);
    }

    @Override
    public boolean pausePullingSystem() {
        Integer maxPullRequestNumber = pullFrequencyHelper.getTotalPullRequestNumberPerJobCycle();
        LOG.trace("Checking if the system should pause the pulling mechanism.");
        final long ongoingPullRequestNumber = pullRequestDao.countPendingPullRequest();

        final boolean shouldPause = ongoingPullRequestNumber > maxPullRequestNumber;

        if (shouldPause) {
            LOG.debug("[PULL]:Size of the pulling queue:[{}] is higher than the number of pull requests to send:[{}]. Pause adding to the queue so the system can consume the requests.", ongoingPullRequestNumber, maxPullRequestNumber);
        } else {
            LOG.trace("[PULL]:Size of the pulling queue:[{}], the number of pull requests to send:[{}].", ongoingPullRequestNumber, maxPullRequestNumber);
        }
        return shouldPause;
    }

    @Override
    public Integer getPullRequestNumberForMpc(String mpcName) {
        return pullFrequencyHelper.getPullRequestNumberForMpc(mpcName);
    }

    @Override
    public RawEnvelopeDto findPulledMessageRawXmlByMessageId(final String messageId, MSHRole role) {
        final RawEnvelopeDto rawXmlByMessageId = rawEnvelopeLogDao.findRawXmlByMessageIdAndRole(messageId, role);
        if (rawXmlByMessageId == null) {
            throw new ReliabilityException(DomibusCoreErrorCode.DOM_004, "There should always have a raw message for message " + messageId);
        }
        return rawXmlByMessageId;
    }

    @Override
    public void addMpcToFrequencyHelper(List<Process> processes) {
        final Set<String> mpcNames = processes.stream()
                .flatMap(pullProcess -> pullProcess.getLegs().stream().map(leg -> leg.getDefaultMpc().getName()))
                .collect(Collectors.toSet());
        pullFrequencyHelper.setMpcNames(mpcNames);
    }

    /**
     * This method is a bit weird as we delete and save a xml message for the same message id.
     * Saving the raw xml message in the case of the pull is occurring on the last outgoing interceptor in order
     * to have all the cxf message modification saved (reliability check.) Unfortunately this saving is not done in the
     * same transaction.
     *  @param rawXml    the soap envelope
     * @param messageId the user message
     * @param mshRole
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void saveRawXml(String rawXml, String messageId, MSHRole mshRole) {
        LOG.debug("Saving rawXML for message [{}]", messageId);

        UserMessageRaw newRawEnvelopeLog = new UserMessageRaw();
        newRawEnvelopeLog.setRawXML(rawXml);
        UserMessage userMessage = userMessageService.getByMessageId(messageId, mshRole);
        newRawEnvelopeLog.setUserMessage(userMessage);
        rawEnvelopeLogDao.create(newRawEnvelopeLog);
    }

}
