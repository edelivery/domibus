package eu.domibus.core.crypto;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.pki.MultiDomainCryptoService;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import java.security.KeyStore;
import java.util.List;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
@Service
public class DomainCryptoServiceListenerImpl implements DomainCryptoServiceListener {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomainCryptoServiceListenerImpl.class);

    protected final SecurityProfileProviderImpl securityProfileProvider;

    protected final SecurityProfileValidatorService securityProfileValidatorService;

    protected MultiDomainCryptoService multiDomainCryptoService;

    public DomainCryptoServiceListenerImpl(SecurityProfileProviderImpl securityProfileProvider, SecurityProfileValidatorService securityProfileValidatorService, MultiDomainCryptoService multiDomainCryptoService) {
        this.securityProfileProvider = securityProfileProvider;
        this.securityProfileValidatorService = securityProfileValidatorService;
        this.multiDomainCryptoService = multiDomainCryptoService;
    }

    @Override
    public void afterInit(Domain domain) {
        LOG.info("Initializing domain SecurityProfileProvider for domain [{}]", domain);
        final DomainSecurityProfileProvider domainSecurityProfileProvider = securityProfileProvider.getDomainSecurityProfileProvider(domain);
        domainSecurityProfileProvider.init();
    }

    @Override
    public void onValidateKeystore(Domain domain, KeyStore keystore) {
        LOG.info("On replacing keystore for domain [{}]", domain);
        final DomainSecurityProfileProvider domainSecurityProfileProvider = securityProfileProvider.getDomainSecurityProfileProvider(domain);
        final List<SecurityProfileConfiguration> securityProfileAliasConfigurations = domainSecurityProfileProvider.getSecurityProfileConfigurations();
        securityProfileValidatorService.validateConfiguredCertificates(securityProfileAliasConfigurations, keystore, StoreType.KEYSTORE);

    }
}
