package eu.domibus.core.crypto;

import eu.domibus.api.crypto.TLSMshCertificateManager;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.core.property.DomibusRawPropertyProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import static eu.domibus.api.multitenancy.DomainService.DOMAINS_HOME;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

@Service
public class TLSMshTrustStorePersistenceInfoImpl implements KeystorePersistenceInfo {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSMshTrustStorePersistenceInfoImpl.class);

    protected final DomibusPropertyProvider domibusPropertyProvider;
    protected final DomibusRawPropertyProvider domibusRawPropertyProvider;
    protected final DomainContextProvider domainContextProvider;
    protected final CertificateHelper certificateHelper;
    protected final DomibusConfigurationService domibusConfigurationService;

    public TLSMshTrustStorePersistenceInfoImpl(DomibusPropertyProvider domibusPropertyProvider,
                                               DomibusRawPropertyProvider domibusRawPropertyProvider,
                                               DomainContextProvider domainContextProvider,
                                               CertificateHelper certificateHelper,
                                               DomibusConfigurationService domibusConfigurationService) {
        this.domibusPropertyProvider = domibusPropertyProvider;
        this.domibusRawPropertyProvider = domibusRawPropertyProvider;
        this.domainContextProvider = domainContextProvider;
        this.certificateHelper = certificateHelper;
        this.domibusConfigurationService = domibusConfigurationService;
    }

    @Override
    public KeystorePersistenceInfo createDefault(String storeFileName, String password) {
        LOG.debug("Creating a default TLS truststore with file name [{}].", storeFileName);

        return new TLSMshTrustStorePersistenceInfoImpl(domibusPropertyProvider, domibusRawPropertyProvider, domainContextProvider, certificateHelper, domibusConfigurationService) {
            @Override
            public String getFileLocation() {
                if (domibusConfigurationService.isMultiTenantAware()) {
                    final Domain domainCode = getDomainCode();
                    String storeFilePath = DOMAINS_HOME + "/" + domainCode + "/keystores/" + storeFileName;
                    return storeFilePath;
                }
                return "keystores/" + storeFileName;
            }

            @Override
            public String getPassword() {
                return password;
            }

            @Override
            public String getType() {
                return certificateHelper.getStoreType(storeFileName);
            }
        };
    }

    @Override
    public String getName() {
        return TLSMshCertificateManager.TLS_MSH_TRUSTSTORE_NAME;
    }

    @Override
    public String getFileLocation() {
        return domibusPropertyProvider.getProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION);
    }

    @Override
    public boolean isOptional() {
        return true;
    }

    @Override
    public String getType() {
        return domibusPropertyProvider.getProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_TRUSTSTORE_TYPE);
    }

    @Override
    public String getPassword() {
        return domibusRawPropertyProvider.getRawPropertyValue(getDomainCode(), DOMIBUS_SECURITY_TLS_TRUSTSTORE_PASSWORD);
    }

    @Override
    public void updatePassword(String password) {
        domibusPropertyProvider.setProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_TRUSTSTORE_PASSWORD, password);
    }

    @Override
    public void updateType(String type) {
        domibusPropertyProvider.setProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_TRUSTSTORE_TYPE, type);
    }

    @Override
    public void updateFileLocation(String fileLocation) {
        domibusPropertyProvider.setProperty(getDomainCode(), DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, fileLocation);
    }

    @Override
    public boolean isDownloadable() {
        return true;
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public String toString() {
        return getName() + ":" + getFileLocation() + ":" + getType() + ":" + getPassword();
    }

    protected Domain getDomainCode() {
        return domainContextProvider.getCurrentDomain();
    }
}
