package eu.domibus.core.spring;

import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.core.metrics.DomibusAdminServlet;
import eu.domibus.core.metrics.HealthCheckServletContextListener;
import eu.domibus.core.metrics.MetricsServletContextListener;
import eu.domibus.core.plugin.classloader.PluginClassLoader;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.web.spring.DomibusWebConfiguration;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static eu.domibus.core.spring.DomibusSessionInitializer.SESSION_INITIALIZER_ORDER;

/**
 * @author Cosmin Baciu
 * @since 4.2
 * The priority should be lower (i.e. the order number higher) than that of DomibusSessionInitializer so that the Spring session filter is added to the chain first
 */
@Order(SESSION_INITIALIZER_ORDER + 1)
public class DomibusApplicationInitializer implements WebApplicationInitializer {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusApplicationInitializer.class);

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        DomibusApplicationInitializerHelper domibusApplicationInitializerHelper = new DomibusApplicationInitializerHelper();

        AnnotationConfigWebApplicationContext rootContext = null;
        try {
            rootContext = domibusApplicationInitializerHelper.onStartup();
        } catch (DomibusCoreException e) {
            throw new ServletException(e.getMessage(), e);
        }
        final PluginClassLoader pluginClassLoader = (PluginClassLoader) rootContext.getClassLoader();

        servletContext.addListener(new DomibusContextLoaderListener(rootContext, pluginClassLoader));
        servletContext.addListener(new RequestContextListener());

        AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
        dispatcherContext.register(DomibusWebConfiguration.class);
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", new DispatcherServlet(dispatcherContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
        dispatcherContext.setClassLoader(pluginClassLoader);

        Set<SessionTrackingMode> sessionTrackingModes = new HashSet<>();
        sessionTrackingModes.add(SessionTrackingMode.COOKIE);
        servletContext.setSessionTrackingModes(sessionTrackingModes);

        FilterRegistration.Dynamic springSecurityFilterChain = servletContext.addFilter("springSecurityFilterChain", DelegatingFilterProxy.class);
        springSecurityFilterChain.addMappingForUrlPatterns(null, false, "/*");

        ServletRegistration.Dynamic cxfServlet = servletContext.addServlet("CXF", CXFServlet.class);
        cxfServlet.setLoadOnStartup(1);
        cxfServlet.addMapping("/services/*");

        Map<String, String> initParams = new HashMap<>();
        initParams.put("hide-service-list-page", "true");
        cxfServlet.setInitParameters(initParams);

        configureMetrics(servletContext);
    }

    protected void configureMetrics(ServletContext servletContext) {
        servletContext.addListener(new MetricsServletContextListener());
        servletContext.addListener(new HealthCheckServletContextListener());

        ServletRegistration.Dynamic servlet = servletContext.addServlet("metrics", DomibusAdminServlet.class);
        servlet.addMapping("/metrics/*");
    }

}
