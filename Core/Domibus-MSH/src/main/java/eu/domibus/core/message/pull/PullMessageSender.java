package eu.domibus.core.message.pull;

import eu.domibus.api.ebms3.model.Ebms3Error;
import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.ebms3.model.Ebms3PullRequest;
import eu.domibus.api.ebms3.model.Ebms3SignalMessage;
import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.message.UserMessageException;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.PartInfo;
import eu.domibus.api.model.ProcessingType;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.security.SecurityProfileException;
import eu.domibus.common.ErrorCode;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.common.model.configuration.Party;
import eu.domibus.core.crypto.SecurityProfileProviderImpl;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.mapper.Ebms3Converter;
import eu.domibus.core.ebms3.sender.EbMS3MessageBuilder;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.message.*;
import eu.domibus.core.metrics.Counter;
import eu.domibus.core.metrics.Timer;
import eu.domibus.core.multitenancy.DomibusDomainException;
import eu.domibus.core.plugin.notification.BackendNotificationService;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.pulling.PullRequestDao;
import eu.domibus.core.status.DomibusStatusService;
import eu.domibus.core.util.MessageUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import eu.domibus.messaging.MessageConstants;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import javax.xml.ws.WebServiceException;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * @author Thomas Dussart
 * @since 3.3
 * <p>
 * Jms listener in charge of sending pullrequest.
 */
@Component
public class PullMessageSender {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PullMessageSender.class);

    @Autowired
    protected MessageUtil messageUtil;

    @Autowired
    private MSHDispatcher mshDispatcher;

    @Autowired
    private EbMS3MessageBuilder messageBuilder;

    @Autowired
    private UserMessageHandlerService userMessageHandlerService;

    @Autowired
    protected TestMessageValidator testMessageValidator;

    @Autowired
    private UserMessageErrorCreator userMessageErrorCreator;

    @Autowired
    private BackendNotificationService backendNotificationService;

    @Autowired
    private PModeProvider pModeProvider;

    @Autowired
    SecurityProfileProviderImpl securityProfileProvider;

    @Autowired
    private DomibusStatusService domibusStatusService;

    @Autowired
    private UserMessageDefaultService userMessageDefaultService;

    @Autowired
    private DomainContextProvider domainContextProvider;

    @Autowired
    protected Ebms3Converter ebms3Converter;

    @Autowired
    private PullFrequencyHelper pullFrequencyHelper;

    @Autowired
    private PullRequestDao pullRequestDao;

    @Autowired
    protected UserMessagePayloadService userMessagePayloadService;

    @SuppressWarnings("squid:S2583") //TODO: SONAR version updated!
    @Timer(clazz = PullMessageSender.class, value = "outgoing_pull_request")
    @Counter(clazz = PullMessageSender.class, value = "outgoing_pull_request")
    public void processPullRequest(final Message map) {
        if (domibusStatusService.isNotReady()) {
            return;
        }
        LOG.clearCustomKeys();

        String domainCode;
        try {
            domainCode = map.getStringProperty(MessageConstants.DOMAIN);
        } catch (JMSException e) {
            LOG.error("Could not get domain from pull request jms message:", e);
            return;
        }
        try {
            domainContextProvider.setCurrentDomainWithValidation(domainCode);
        } catch (DomibusDomainException ex) {
            LOG.error("Invalid domain: [{}]", domainCode, ex);
            return;
        }
        LOG.debug("Initiate pull request");
        boolean notifyBusinessOnError = false;
        String messageId = null;
        String mpcName = null;
        UserMessage userMessage = null;
        List<PartInfo> partInfos = null;
        String pullRequestId = null;
        LegConfiguration legConfiguration = null;
        try {
            final String mpcQualifiedName = map.getStringProperty(PullContext.MPC);
            final String pModeKey = map.getStringProperty(PullContext.PMODE_KEY);
            pullRequestId = map.getStringProperty(PullContext.PULL_REQUEST_ID);
            notifyBusinessOnError = Boolean.valueOf(map.getStringProperty(PullContext.NOTIFY_BUSINNES_ON_ERROR));
            Ebms3SignalMessage signalMessage = new Ebms3SignalMessage();
            Ebms3PullRequest pullRequest = new Ebms3PullRequest();
            pullRequest.setMpc(mpcQualifiedName);
            signalMessage.setPullRequest(pullRequest);
            LOG.debug("Sending pull request with mpc:[{}]", mpcQualifiedName);

            legConfiguration = pModeProvider.getLegConfiguration(pModeKey);
            mpcName = legConfiguration.getDefaultMpc().getName();
            Party receiverParty = pModeProvider.getReceiverParty(pModeKey);
            LOG.trace("Build soap message");

            SOAPMessage soapMessage = messageBuilder.buildSOAPMessage(signalMessage, null);
            LOG.trace("Send soap message");

            SecurityProfileConfiguration securityProfileConfiguration = null;
            try {
                securityProfileConfiguration = securityProfileProvider.getSecurityProfileConfigurationWithRSAAsDefault(domainContextProvider.getCurrentDomain(), legConfiguration.getSecurity().getSecurityProfile());
            } catch (final SecurityProfileException e) {
                throw EbMS3ExceptionBuilder.getInstance()
                        .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                        .message("No security configuration found")
                        .mshRole(MSHRole.RECEIVING)
                        .cause(e)
                        .build();
            }

            if (securityProfileConfiguration == null) {
                throw EbMS3ExceptionBuilder.getInstance()
                        .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0010)
                        .message("No security configuration found")
                        .mshRole(MSHRole.RECEIVING)
                        .build();
            }

            final SOAPMessage response = mshDispatcher.dispatch(soapMessage, receiverParty.getEndpoint(), securityProfileConfiguration, legConfiguration, pModeKey);
            pullFrequencyHelper.success(legConfiguration.getDefaultMpc().getName());
            Ebms3Messaging ebms3Messaging = messageUtil.getMessage(response);

            if (ebms3Messaging.getUserMessage() == null && ebms3Messaging.getSignalMessage() != null) {
                LOG.trace("No message for sent pull request [{}] with mpc:[{}]", signalMessage.getMessageInfo() == null ? null : signalMessage.getMessageInfo().getMessageId(), mpcQualifiedName);
                pullFrequencyHelper.increaseError(mpcName);
                logError(ebms3Messaging.getSignalMessage());
                return;
            }
            pullFrequencyHelper.success(legConfiguration.getDefaultMpc().getName());

            userMessage = ebms3Converter.convertFromEbms3(ebms3Messaging.getUserMessage());
            messageId = userMessage.getMessageId();
            LOG.trace("Message [{}] received in response to pull request [{}] with mpc:[{}]", messageId, signalMessage.getMessageInfo() == null ? null : signalMessage.getMessageInfo().getMessageId(), mpcQualifiedName);

            partInfos = userMessagePayloadService.handlePayloads(response, ebms3Messaging, null);
            handleResponse(response, userMessage, partInfos);

            String sendMessageId = messageId;
            try {
                LOG.debug("Schedule sending pull receipt for message [{}]", sendMessageId);
                userMessageDefaultService.scheduleSendingPullReceipt(sendMessageId, pModeKey);
            } catch (Exception ex) {
                LOG.warn("Message[{}] exception while sending receipt asynchronously.", messageId, ex);
            }
        } catch (TransformerException | SOAPException | IOException | JAXBException | JMSException e) {
            LOG.error(e.getMessage(), e);
            throw new UserMessageException(DomibusCoreErrorCode.DOM_001, "Error handling new UserMessage", e);
        } catch (final EbMS3Exception e) {
            try {
                if (notifyBusinessOnError && userMessage != null) {
                    boolean notifyAsyncLegConfiguration = legConfiguration != null && BooleanUtils.toBoolean(legConfiguration.isAsyncNotification());
                    backendNotificationService.notifyMessageReceivedFailure(userMessage, userMessageErrorCreator.createErrorResult(e), notifyAsyncLegConfiguration);
                }
            } catch (Exception ex) {
                LOG.businessError(DomibusMessageCode.BUS_BACKEND_NOTIFICATION_FAILED, ex, messageId);
            }
            checkConnectionProblem(e, mpcName);
        } finally {
            LOG.trace("Delete pull request with UUID:[{}]", pullRequestId);
            pullRequestDao.deletePullRequest(pullRequestId);
        }
    }

    protected void handleResponse(final SOAPMessage response, UserMessage userMessage, List<PartInfo> partInfos) throws TransformerException, SOAPException, IOException, JAXBException, EbMS3Exception {
        LOG.trace("handle message");

        // Find legConfiguration for the received UserMessage
        MessageExchangeConfiguration userMessageExchangeConfiguration = pModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.RECEIVING, true);
        String pModeKey = userMessageExchangeConfiguration.getPmodeKey();
        LOG.debug("pModeKey for received userMessage is [{}]", pModeKey);

        userMessageHandlerService.handleNewUserMessage(pModeKey, ProcessingType.PULL, response, userMessage, null, partInfos);

        Boolean testMessage = testMessageValidator.checkTestMessage(userMessage);
        LOG.businessInfo(testMessage ? DomibusMessageCode.BUS_TEST_MESSAGE_RECEIVED : DomibusMessageCode.BUS_MESSAGE_RECEIVED,
                userMessage.getPartyInfo().getFromParty(), userMessage.getPartyInfo().getToParty());

    }

    private void logError(Ebms3SignalMessage signalMessage) {
        Set<Ebms3Error> error = signalMessage.getError();
        for (Ebms3Error error1 : error) {
            LOG.info(error1.getErrorCode() + " " + error1.getShortDescription());
        }
    }

    private void checkConnectionProblem(EbMS3Exception e, String mpcName) {
        if (e.getEbMS3ErrorCode() == ErrorCode.EbMS3ErrorCode.EBMS_0005) {
            LOG.warn("ConnectionFailure ", e);
            pullFrequencyHelper.increaseError(mpcName);
        } else {
            throw new WebServiceException(e);
        }
    }
}
