package eu.domibus.core.ebms3.receiver.policy;

import eu.domibus.api.model.MSHRole;
import eu.domibus.api.pmode.PModeConstants;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.common.ErrorCode;
import eu.domibus.core.crypto.SecurityProfileHelper;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.ebms3.receiver.interceptor.CheckEBMSHeaderInterceptor;
import eu.domibus.core.ebms3.receiver.interceptor.SOAPMessageBuilderInterceptor;
import eu.domibus.core.ebms3.receiver.leg.ClientInMessageLegConfigurationFactory;
import eu.domibus.core.pmode.provider.PModeProvider;
import eu.domibus.core.util.SoapUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author Thomas Dussart
 * @author Cosmin Baciu
 * @since 4.1
 */
@Service("setPolicyInInterceptorClient")
public class SetPolicyInClientInterceptor extends SetPolicyInInterceptor {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SetPolicyInClientInterceptor.class);

    protected PModeProvider pModeProvider;
    protected ClientInMessageLegConfigurationFactory clientInMessageLegConfigurationFactory;
    protected WSS4JInInterceptor wss4JInInterceptor;
    protected SoapUtil soapUtil;
    protected SecurityProfileHelper securityProfileHelper;

    public SetPolicyInClientInterceptor(PModeProvider pModeProvider,
                                        ClientInMessageLegConfigurationFactory clientInMessageLegConfigurationFactory,
                                        WSS4JInInterceptor wss4JInInterceptor,
                                        SoapUtil soapUtil,
                                        SecurityProfileHelper securityProfileHelper) {
        this.pModeProvider = pModeProvider;
        this.clientInMessageLegConfigurationFactory = clientInMessageLegConfigurationFactory;
        this.wss4JInInterceptor = wss4JInInterceptor;
        this.soapUtil = soapUtil;
        this.securityProfileHelper = securityProfileHelper;
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {
        SecurityProfileConfiguration securityProfileConfiguration = (SecurityProfileConfiguration) message.getExchange().get(PModeConstants.SECURITY_PROFILE_CONFIGURATION);
        if (securityProfileConfiguration == null) {
            throwFault(message, ErrorCode.EbMS3ErrorCode.EBMS_0010, "No valid security configuration found");
        }

        boolean attachmentsPresent = soapUtil.attachmentExists(message);

        final Map<String, Object> incomingSecurityConfigurationAsMap = securityProfileHelper.getIncomingSecurityConfigurationAsMap(securityProfileConfiguration, attachmentsPresent);
        LOG.debug("Using the security profile configuration for the incoming message [{}]", incomingSecurityConfigurationAsMap);
        for (Map.Entry<String, Object> entry : incomingSecurityConfigurationAsMap.entrySet()) {
            message.put(entry.getKey(), entry.getValue());
        }

        String pModeKeyContextProperty = (String) message.getExchange().get(PModeConstants.PMODE_KEY_CONTEXT_PROPERTY);
        if (StringUtils.isBlank(pModeKeyContextProperty)) {
            throwFault(message, ErrorCode.EbMS3ErrorCode.EBMS_0010, "PMode key context property is empty");
        }
        message.put(PModeConstants.PMODE_KEY_CONTEXT_PROPERTY, pModeKeyContextProperty);

        message.getInterceptorChain().add(new CheckEBMSHeaderInterceptor());
        message.getInterceptorChain().add(new SOAPMessageBuilderInterceptor());
        message.getInterceptorChain().add(wss4JInInterceptor);
    }

    protected void throwFault(SoapMessage message, ErrorCode.EbMS3ErrorCode ebMS3ErrorCode, String errorMessage) {
        setBindingOperation(message);
        String messageId = LOG.getMDC(DomibusLogger.MDC_MESSAGE_ID);

        throw new Fault(EbMS3ExceptionBuilder.getInstance()
                .ebMS3ErrorCode(ebMS3ErrorCode)
                .message(errorMessage)
                .refToMessageId( StringUtils.isNotBlank(messageId) ? messageId : "unknown")
                .mshRole(MSHRole.SENDING)
                .build());
    }
}
