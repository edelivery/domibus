package eu.domibus.core.ebms3;

import eu.domibus.api.security.SecurityProfileException;
import eu.domibus.api.security.SecurityProfileMetadata;
import eu.domibus.api.util.xml.XMLUtil;
import eu.domibus.core.util.DomibusDomUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.wss4j.common.WSS4JConstants;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Utility class for extracting various elements from the SoapMessage
 *
 * @author Lucian Furca
 * @since 5.2
 */
@Service
public class SoapElementsExtractorUtilImpl {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SoapElementsExtractorUtilImpl.class);

    public static final String ALGORITHM_ATTRIBUTE = "Algorithm";
    public static final String ENCRYPTED_KEY = "EncryptedKey";
    public static final String SIGNATURE = "Signature";
    public static final String SIGNED_INFO = "SignedInfo";
    public static final String SIGNATURE_METHOD = "SignatureMethod";


    protected final XMLUtil xmlUtil;
    protected DomibusDomUtil domibusDomUtil;


    public SoapElementsExtractorUtilImpl(XMLUtil xmlUtil, DomibusDomUtil domibusDomUtil) {
        this.xmlUtil = xmlUtil;
        this.domibusDomUtil = domibusDomUtil;
    }

    /**
     * Retrieves the Security Profile Metadata from the security header node of the cxf SoapMessage. The SecurityProfile inside
     * the SecurityProfileMetadata returned object is null and will be set at a later point in time
     *
     * @param soapSecurityHeader the Node representing the security header
     * @return the determined Security Profile metadata
     */
    public SecurityProfileMetadata extractSecurityProfileMetadata(Node soapSecurityHeader) throws SecurityProfileException {
        SecurityProfileMetadata securityProfileMetadata = new SecurityProfileMetadata();

        Element signatureElement = domibusDomUtil.getChildElementByName(soapSecurityHeader, SIGNATURE);
        Element encryptedKeyElement = domibusDomUtil.getChildElementByName(soapSecurityHeader, ENCRYPTED_KEY);
        if (signatureElement != null) {
            enrichSecurityProfileMetadataFromSignatureElement(securityProfileMetadata, signatureElement);
        }
        if (encryptedKeyElement != null) {
            enrichSecurityProfileMetadataFromEncryptedKeyElement(securityProfileMetadata, encryptedKeyElement);
        }
        return securityProfileMetadata;
    }

    private void enrichSecurityProfileMetadataFromEncryptedKeyElement(SecurityProfileMetadata securityProfileMetadata, Element encryptedKeyElement) {
        LOG.debug("Enriching security profile metadata from encrypted key element");

        final Element encryptionMethod = domibusDomUtil.getChildElementByName(encryptedKeyElement, "EncryptionMethod");
        if (encryptionMethod != null) {
            final String encryptionKeyTransportAlgorithm = encryptionMethod.getAttribute("Algorithm");
            securityProfileMetadata.encryptionKeyTransportAlgorithm(encryptionKeyTransportAlgorithm);

            final Element digestMethodElement = domibusDomUtil.getChildElementByName(encryptionMethod, "DigestMethod");
            if (digestMethodElement != null) {
                String encryptionDigestAlgorithm = digestMethodElement.getAttribute("Algorithm");
                securityProfileMetadata.encryptionDigestAlgorithm(encryptionDigestAlgorithm);
            }

            final Element mgfElement = domibusDomUtil.getChildElementByName(encryptionMethod, "MGF");
            if (mgfElement != null) {
                String encryptionMgfAlgorithm = mgfElement.getAttribute("Algorithm");
                securityProfileMetadata.encryptionMGFAlgorithm(encryptionMgfAlgorithm);
            }
        }

        final Element keyInfoElement = domibusDomUtil.getChildElementByName(encryptedKeyElement, "KeyInfo");
        if (keyInfoElement != null) {
            final Element agreementMethod = domibusDomUtil.getChildElementByName(keyInfoElement, "AgreementMethod");
            if (agreementMethod != null) {
                final String agreementMethodAlgorithm = agreementMethod.getAttribute("Algorithm");
                securityProfileMetadata.encryptionKeyAgreementMethod(agreementMethodAlgorithm);

                final Element keyDerivationMethodElement = domibusDomUtil.getChildElementByName(agreementMethod, "KeyDerivationMethod");
                if (keyDerivationMethodElement != null) {
                    final String keyDerivationMethodAlgorithm = keyDerivationMethodElement.getAttribute("Algorithm");
                    securityProfileMetadata.encryptionKeyDerivationFunction(keyDerivationMethodAlgorithm);
                }
            }
        }
    }

    private void enrichSecurityProfileMetadataFromSignatureElement(SecurityProfileMetadata securityProfileMetadata, Element signatureElement) {
        LOG.debug("Enriching security profile metadata from signature element");

        final Element signedInfoElement = domibusDomUtil.getChildElementByName(signatureElement, "SignedInfo");
        if (signedInfoElement != null) {
            final Element signatureMethodElement = domibusDomUtil.getChildElementByName(signedInfoElement, "SignatureMethod");
            if (signatureMethodElement != null) {
                final String signatureAlgorithm = signatureMethodElement.getAttribute("Algorithm");
                securityProfileMetadata.signatureSignatureAlgorithm(signatureAlgorithm);
            }
        }
    }

    public String getSignatureAlgorithm(Node soapSecurityHeader) throws SecurityProfileException {
        Element signatureElement = domibusDomUtil.getChildElementByName(soapSecurityHeader, SIGNATURE);
        Element signedInfoElement = domibusDomUtil.getChildElementByName(signatureElement, SIGNED_INFO);
        Element signatureMethodElement = domibusDomUtil.getChildElementByName(signedInfoElement, SIGNATURE_METHOD);

        String signatureMethodAlgorithm = signatureMethodElement.getAttributes().getNamedItem(ALGORITHM_ATTRIBUTE).getNodeValue();
        switch (signatureMethodAlgorithm) {
            case WSS4JConstants.RSA_SHA256:
            case WSS4JConstants.ECDSA_SHA256:
                return signatureMethodAlgorithm;
            default:
                String errorMessage = "Invalid signature method algorithm " + signatureMethodAlgorithm +
                        ", it does not correspond to any security profile";
                LOG.error(errorMessage);
                throw new SecurityProfileException(errorMessage);
        }
    }


}
