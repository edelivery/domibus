package eu.domibus.core.message;

public interface UserMessageContextKeyProvider {

    String USER_MESSAGE = "userMessage";
    String ASYNC_NOTIFICATION_FOR_LEG = "asyncNotificationForLeg";
    String BACKEND_FILTER = "backendFilter";

    void setKeyOnTheCurrentMessage(String key, String value);

    String getKeyFromTheCurrentMessage(String key);

    void setObjectOnTheCurrentMessage(String key, Object value);

    Object getObjectFromTheCurrentMessage(String key);
}
