package eu.domibus.core.crypto;

import eu.domibus.api.cache.DomibusLocalCacheService;
import eu.domibus.api.cluster.SignalService;
import eu.domibus.api.crypto.CryptoException;
import eu.domibus.api.crypto.NoKeyStoreContentInformationException;
import eu.domibus.api.crypto.SameResourceCryptoException;
import eu.domibus.api.crypto.TLSGenericCertificateManager;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.CertificateService;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.pki.KeystorePersistenceService;
import eu.domibus.api.security.TrustStoreEntry;
import eu.domibus.common.DomibusCacheConstants;
import eu.domibus.core.certificate.TLSGenericTrustStorePersistenceInfoImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

@Service
public class TLSGenericCertificateManagerImpl implements TLSGenericCertificateManager {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSGenericCertificateManagerImpl.class);

    public static final String TLS_GENERIC_CACHE = "tlsGenericCache";
    protected final CertificateService certificateService;
    protected final KeystorePersistenceService keystorePersistenceService;
    protected final DomainContextProvider domainContextProvider;
    protected final TLSGenericTrustStorePersistenceInfoImpl tlsGenericTrustStorePersistenceInfoImpl;
    protected final SignalService signalService;
    protected final DomibusLocalCacheService domibusLocalCacheService;
    protected final TLSGenericCertificateManagerHelper tlsGenericCertificateManagerHelper;

    public TLSGenericCertificateManagerImpl(CertificateService certificateService,
                                            KeystorePersistenceService keystorePersistenceService,
                                            DomainContextProvider domainContextProvider,
                                            TLSGenericTrustStorePersistenceInfoImpl tlsGenericTrustStorePersistenceInfoImpl,
                                            SignalService signalService,
                                            DomibusLocalCacheService domibusLocalCacheService,
                                            TLSGenericCertificateManagerHelper tlsGenericCertificateManagerHelper) {
        this.certificateService = certificateService;
        this.keystorePersistenceService = keystorePersistenceService;
        this.domainContextProvider = domainContextProvider;
        this.tlsGenericTrustStorePersistenceInfoImpl = tlsGenericTrustStorePersistenceInfoImpl;
        this.signalService = signalService;
        this.domibusLocalCacheService = domibusLocalCacheService;
        this.tlsGenericCertificateManagerHelper = tlsGenericCertificateManagerHelper;
    }

    @Cacheable(cacheManager = DomibusCacheConstants.CACHE_MANAGER, value = TLS_GENERIC_CACHE, key = "#root.target.getCurrentDomainCode()")
    @Override
    public KeyStore getTLSGenericTruststore() {
        try {
            final String tlsTruststoreFileLocation = tlsGenericTrustStorePersistenceInfoImpl.getFileLocation();
            if (StringUtils.isBlank(tlsTruststoreFileLocation)) {
                LOG.info("No  TLS Generic Truststore defined for domain [{}]", getCurrentDomainCode());
                return null;
            }
            LOG.info("Getting the TLS Generic Truststore from location [{}] for domain [{}].", tlsTruststoreFileLocation, getCurrentDomainCode());
            return certificateService.getStore(tlsGenericTrustStorePersistenceInfoImpl);
        } catch (Exception ex) {
            LOG.info("Could not load TLS Generic Truststore found for domain [{}]", getCurrentDomainCode(), ex);
            return null;
        }
    }

    // this method is used as a cache key and needs to be public
    public String getCurrentDomainCode() {
        Domain currentDomain = domainContextProvider.getCurrentDomain();
        return currentDomain != null ? currentDomain.getCode() : StringUtils.EMPTY;
    }

    @Override
    public List<TrustStoreEntry> getTrustStoreEntries() {
        try {
            final KeystorePersistenceInfo persistenceInfo = getPersistenceInfo();
            return certificateService.getStoreEntries(persistenceInfo);
        } catch (NoKeyStoreContentInformationException ex) {
            LOG.debug("The TLS Generic truststore is not configured.", ex);
            return new ArrayList<>();
        } catch (CryptoException ex) {
            throw new CryptoException("Error getting the TLS Generic truststore entries", ex);
        }
    }

    @Override
    public KeystorePersistenceInfo getPersistenceInfo() {
        return tlsGenericTrustStorePersistenceInfoImpl;
    }

    @Override
    public KeyStoreContentInfo getTruststoreContent() {
        return certificateService.getStoreContent(getPersistenceInfo());
    }

    @Override
    public synchronized void replaceTrustStore(KeyStoreContentInfo contentInfo) {
        certificateService.createOrReplaceStore(contentInfo, tlsGenericTrustStorePersistenceInfoImpl);
        resetTLSTruststore();
    }

    @Override
    public void resetTLSTruststore() {
        tlsGenericCertificateManagerHelper.resetCache();
        Domain domain = domainContextProvider.getCurrentDomain();
        signalService.signalTLSGenericTrustStoreUpdate(domain);
    }

    @Override
    public boolean addCertificate(byte[] certificateData, String alias) {
        boolean added = certificateService.addCertificate(getPersistenceInfo(), certificateData, alias, true);
        if (added) {
            LOG.debug("Added certificate [{}] to the TLS Generic truststore; resetting it.", alias);
            resetTLSTruststore();
        }

        return added;
    }

    @Override
    public boolean removeCertificate(String alias) {
        boolean removed = certificateService.removeCertificate(getPersistenceInfo(), alias);
        if (removed) {
            LOG.debug("Removed certificate [{}] from the TLS Generic truststore; resetting it.", alias);
            resetTLSTruststore();
        }

        return removed;
    }
}
