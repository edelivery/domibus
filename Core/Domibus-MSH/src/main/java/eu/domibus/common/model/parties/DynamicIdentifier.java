package eu.domibus.common.model.parties;


import eu.domibus.api.model.AbstractBaseEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

@Entity
@Table(name = "TB_PM_PARTY_IDENTIFIER")
public class DynamicIdentifier extends AbstractBaseEntity {

    @Column(name = "PARTY_ID")
    protected String partyId;

    @Column(name = "FK_PARTY_ID_TYPE")
    private Long partyIdType;

    @Transient
    private String partyIdTypeValue;

    public String getPartyIdTypeValue() {
        return partyIdTypeValue;
    }

    public void setPartyIdTypeValue(String partyIdTypeValue) {
        this.partyIdTypeValue = partyIdTypeValue;
    }

    public String getPartyId() {
        return this.partyId;
    }

    public void setPartyId(final String value) {
        this.partyId = value;
    }

    public Long getPartyIdType() {
        return partyIdType;
    }

    public void setPartyIdType(Long partyIdType) {
        this.partyIdType = partyIdType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DynamicIdentifier that = (DynamicIdentifier) o;

        if (!StringUtils.equalsIgnoreCase(partyId, that.partyId)) return false;
        if (partyIdType == null && that.partyIdType != null) return false;
        if (partyIdType != null && !partyIdType.equals(that.partyIdType)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(partyId == null ? null : partyId.toLowerCase())
                .append(partyIdType)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("partyId", partyId)
                .append("partyIdType", partyIdType)
                .toString();
    }

}
