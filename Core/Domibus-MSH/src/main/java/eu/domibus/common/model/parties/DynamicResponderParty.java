package eu.domibus.common.model.parties;

import eu.domibus.api.model.AbstractBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TB_PM_JOIN_PROCESS_RESP_PARTY")
public class DynamicResponderParty extends AbstractBaseEntity {

    @Column(name = "PROCESS_FK")
    protected Long process;

    @ManyToOne
    @JoinColumn(name = "PARTY_FK")
    protected DynamicParty party;

    public DynamicParty getParty() {
        return party;
    }

    public void setParty(DynamicParty party) {
        this.party = party;
    }

    public Long getProcess() {
        return process;
    }

    public void setProcess(Long process) {
        this.process = process;
    }
}