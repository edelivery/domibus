package eu.domibus.common.model.parties;

import eu.domibus.api.model.AbstractBaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;


@Entity
@Table(name = "TB_PM_JOIN_PROCESS_INIT_PARTY")
public class DynamicInitiatorParty extends AbstractBaseEntity {

    @Column(name = "PROCESS_FK")
    protected Long process;

    @ManyToOne
    @JoinColumn(name = "PARTY_FK")
    protected DynamicParty party;

    public DynamicParty getParty() {
        return party;
    }

    public void setParty(DynamicParty party) {
        this.party = party;
    }

    public Long getProcess() {
        return process;
    }

    public void setProcess(Long process) {
        this.process = process;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DynamicInitiatorParty that = (DynamicInitiatorParty) o;

        if (process == null && that.process != null) return false;
        if (process != null && !process.equals(that.process)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(process == null ? null : process.hashCode())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("process", process)
                .toString();
    }

}