package eu.domibus.common.model.parties;

import eu.domibus.api.model.AbstractBaseEntity;
import eu.domibus.common.model.configuration.Process;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TB_PM_PARTY")
@NamedQueries({
        @NamedQuery(name = "DynamicParty.findAll", query = "select p from DynamicParty p where p.dynamic = true"),
        @NamedQuery(name = "DynamicParty.deleteByName", query = "delete from DynamicParty p where p.dynamic = true and p.name = :NAME"),
        @NamedQuery(name = "DynamicParty.findByName", query = "select p from DynamicParty p where p.dynamic = true and p.name = :NAME"),
        @NamedQuery(name = "DynamicParty.findAnyByName", query = "select p from DynamicParty p where p.name = :NAME")
})
public class DynamicParty extends AbstractBaseEntity {

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "FK_PARTY")
    protected List<DynamicIdentifier> identifiers; //NOSONAR

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "PARTY_FK")
    protected List<DynamicInitiatorParty> initiatorList; //NOSONAR

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "PARTY_FK")
    protected List<DynamicResponderParty> responderList; //NOSONAR

    @Column(name = "FK_BUSINESSPROCESS")
    protected Long businessProcess;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "USERNAME")
    protected String userName;

    @Column(name = "PASSWORD")
    protected String password;

    @Column(name = "ENDPOINT")
    protected String endpoint;

    @Column(name = "`DYNAMIC`")
    protected Boolean dynamic;

    @Transient
    protected List<String> processNamesWherePartyIsInitiator = new ArrayList<>();
    @Transient
    protected List<String> processNamesWherePartyIsResponder = new ArrayList<>();


    public List<String> getProcessNamesWherePartyIsInitiator() {
        return processNamesWherePartyIsInitiator;
    }

    public void setProcessNamesWherePartyIsInitiator(List<String> processNamesWherePartyIsInitiator) {
        this.processNamesWherePartyIsInitiator = processNamesWherePartyIsInitiator;
    }

    public List<String> getProcessNamesWherePartyIsResponder() {
        return processNamesWherePartyIsResponder;
    }

    public void setProcessNamesWherePartyIsResponder(List<String> processNamesWherePartyIsResponder) {
        this.processNamesWherePartyIsResponder = processNamesWherePartyIsResponder;
    }

    public List<DynamicIdentifier> getIdentifiers() {
        if (this.identifiers == null) {
            this.identifiers = new ArrayList<>();
        }
        return this.identifiers;
    }

    public void setIdentifiers(List<DynamicIdentifier> identifiers) {
        this.identifiers = identifiers;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String value) {
        this.name = value;
    }


    public String getUserName() {
        return this.userName;
    }

    public void setUserName(final String value) {
        this.userName = value;
    }


    public String getPassword() {
        return this.password;
    }

    public void setPassword(final String value) {
        this.password = value;
    }

    public String getEndpoint() {
        return this.endpoint;
    }

    public void setEndpoint(final String value) {
        this.endpoint = value;
    }

    public Boolean isDynamic() {
        return dynamic;
    }

    public void setDynamic(Boolean dynamic) {
        this.dynamic = dynamic;
    }

    public Long getBusinessProcess() {
        return businessProcess;
    }

    public void setBusinessProcess(Long businessProcess) {
        this.businessProcess = businessProcess;
    }

    public List<DynamicInitiatorParty> getInitiatorList() {
        if (initiatorList == null) {
            initiatorList = new ArrayList<>();
        }
        return initiatorList;
    }

    public void setInitiatorList(List<DynamicInitiatorParty> initiatorList) {
        this.initiatorList = initiatorList;
    }

    public List<DynamicResponderParty> getResponderList() {
        if (responderList == null) {
            responderList = new ArrayList<>();
        }
        return responderList;
    }

    public void setResponderList(List<DynamicResponderParty> responderList) {
        this.responderList = responderList;
    }

    @Override
    public boolean equals(Object otherParty) {
        if (this == otherParty) return true;

        if (otherParty == null || getClass() != otherParty.getClass()) return false;

        if (!super.equals(otherParty)) return false;

        DynamicParty party = (DynamicParty) otherParty;

        return StringUtils.equalsIgnoreCase(name, party.name);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        if (identifiers != null) {
            result = 31 * result + identifiers.hashCode();
        }
        if (name != null) {
            result = 31 * result + name.hashCode();
        }
        return result;
    }

    @Override
    public String toString() {
        return "DynamicParty(entityId=" + this.getEntityId() + ", name=" + this.getName() + ", endpoint=" + this.getEndpoint() + ", dynamic=" + this.isDynamic() + ")";
    }

    public boolean isInitiatorForProcess(Process process) {
        return getInitiatorList().stream().anyMatch(p -> p.getProcess().equals(process.getEntityId()));
    }

    public boolean isResponderForProcess(Process process) {
        return getResponderList().stream().anyMatch(p -> p.getProcess().equals(process.getEntityId()));
    }

}
