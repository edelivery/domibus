package eu.domibus.common.model.parties;

import eu.domibus.api.model.AbstractBaseEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

@Entity
@Table(name = "TB_PM_PARTY_ID_TYPE")
public class DynamicPartyIdType extends AbstractBaseEntity {

    @Column(name = "FK_BUSINESSPROCESS")
    protected Long businessProcess;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "VALUE")
    protected String value;

    public Long getBusinessProcess() {
        return businessProcess;
    }

    public void setBusinessProcess(Long businessProcess) {
        this.businessProcess = businessProcess;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String value) {
        this.name = value;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DynamicPartyIdType that = (DynamicPartyIdType) o;

        if (!StringUtils.equalsIgnoreCase(name, that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(name == null ? null : name.toLowerCase())
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("value", value)
                .toString();
    }

}
