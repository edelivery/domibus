package eu.domibus.core.spi.validation;

public enum UserMessageValidatorExceptionTypeEnum {

    EBMS3,
    SOAP_FAULT;
}
