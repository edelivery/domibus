package eu.domibus.core.spi.payload;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * This interface defines the contract for managing Domibus payloads using a custom repository eg S3
 * You can provide your own implementation with a specific identifier and configure the payload provider identifier in Domibus properties
 */
public interface DomibusPayloadManagerSpi {

    /**
     * The identifier of the DomibusPayloadManagerSpi using database storage
     */
    String DOMIBUS_DATABASE_SPI = "DomibusDatabase";

    /**
     * The identifier of the DomibusPayloadManagerSpi using file system storage
     */
    String DOMIBUS_FILE_SYSTEM_SPI = "DomibusFileSystem";

    /**
     * It will be called once by Domibus in order to initialize the persistence for the provided Domain
     */
    void initialize(String domain);

    /**
     * Returns the identifier of the DomibusPayloadManagerSpi. It must be unique amongst all the available DomibusPayloadManagerSPIs implementations.
     */
    String getIdentifier();

    /**
     * Returns true if the payload is stored in the Domibus database
     */
    boolean arePayloadsStoredInDomibusDatabase();

    /**
     * Retrieve the absolute location of a payload
     * eg for a file system persistence it returns the file absolute path
     * eg for a S3 persistence it returns the file absolute path including the S3 bucket
     * @param relativeLocation The payload relative location
     * @return The absolute location of a payload
     */
    String getAbsolutePayloadLocation(String domain, String relativeLocation);

    /**
     * Returns the input stream for the provided file
     * @param domain The Domibus domain eg default, domain1, etc
     * @param absoluteFileLocation The absolute file location
     * @return the input stream for the provided file
     */
    InputStream readPayload(String domain, String absoluteFileLocation);


    /**
     * Deletes the payload with the provided location
     * @param domain domain The Domibus domain eg default, domain1, etc
     * @param absoluteFileLocation The absolute file location
     */
    void deletePayload(String domain, String absoluteFileLocation);

    /**
     * Returns a stream for storing the payload into
     * @param domain The Domibus domain eg default, domain1, etc
     * @param relativeFileLocation The relative location of the file
     * @return a stream for storing the payload into
     */
    OutputStream getStreamForStoringPayload(String domain, String relativeFileLocation);

    /**
     * Deletes the payload folder with the provided location. The result can be OK, ERROR or PARTIAL.
     * In case of a PARTIAL deletion, the result will provide the details of the files failing to delete.
     *
     * @param domain domain The Domibus domain eg default, domain1, etc
     * @param absoluteFolderLocation The absolute folder location
     *
     * @return Result of the deletion.
     */
    DeleteFolderResult deleteFolder(String domain, String absoluteFolderLocation);

}
