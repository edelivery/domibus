package eu.domibus.core.spi.payload;

/**
 *
 */
public class DomibusPayloadSpiException extends RuntimeException {


    public DomibusPayloadSpiException() {
    }

    public DomibusPayloadSpiException(String message) {
        super(message);
    }

    public DomibusPayloadSpiException(String message, Throwable cause) {
        super(message, cause);
    }

    public DomibusPayloadSpiException(Throwable cause) {
        super(cause);
    }
}
