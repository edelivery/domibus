package eu.domibus.configuration.spi;

public enum SearchStrategy {

    /**
     * Search only the current location(first level) for files ending in pattern
     */
    CURRENT_LOCATION_ENDING_WITH_PATTERN,

    /**
     * Search in the current location and all descendants(all levels) for files ending in pattern
     */
    RECURSIVE_ENDING_WITH_PATTERN,

    /**
     * Search only the current location(first level) for files containing the pattern
     */
    CURRENT_LOCATION_CONTAINING_PATTERN,

    /**
     * Search in the current location and all descendants(all levels) for files containing the pattern
     */
    RECURSIVE_CONTAINING_PATTERN;
}
