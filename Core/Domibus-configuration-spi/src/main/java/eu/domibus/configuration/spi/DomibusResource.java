package eu.domibus.configuration.spi;

import java.io.InputStream;
import java.time.Instant;
import java.util.StringJoiner;

/**
 * This class contains the details of a resource eg input stream used by the Domibus configuration and payload mechanism
 *
 * @author Cosmin Baciu
 * @since 5.2
 */
public class DomibusResource implements AutoCloseable {

    protected String name;
    protected String description;
    protected String parentLocation;
    protected String location;
    protected Instant lastModified;
    protected InputStream inputStream;

    public DomibusResource(String name,
                           String description,
                           String location,
                           String parentLocation,
                           InputStream inputStream,
                           Instant lastModified) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.parentLocation  = parentLocation;
        this.inputStream = inputStream;
        this.lastModified = lastModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getLastModified() {
        return lastModified;
    }

    public void setLastModified(Instant lastModified) {
        this.lastModified = lastModified;
    }

    public String getParentLocation() {
        return parentLocation;
    }

    public void setParentLocation(String parentLocation) {
        this.parentLocation = parentLocation;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DomibusResource.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("parentLocation='" + parentLocation + "'")
                .add("location='" + location + "'")
                .add("lastModified=" + lastModified)
                .toString();
    }

    @Override
    public void close() throws Exception {
        if (inputStream != null) {
            inputStream.close();
        }
    }
}
