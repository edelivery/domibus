package eu.domibus.configuration.spi;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
public class DomibusConfigurationManagerSpiException extends RuntimeException {


    public DomibusConfigurationManagerSpiException() {
    }

    public DomibusConfigurationManagerSpiException(String message) {
        super(message);
    }

    public DomibusConfigurationManagerSpiException(String message, Throwable cause) {
        super(message, cause);
    }

    public DomibusConfigurationManagerSpiException(Throwable cause) {
        super(cause);
    }

    public DomibusConfigurationManagerSpiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
