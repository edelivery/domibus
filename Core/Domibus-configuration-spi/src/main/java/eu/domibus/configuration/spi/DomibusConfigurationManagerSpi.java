package eu.domibus.configuration.spi;


import java.util.List;

/**
 * This SPI may be implemented to retrieve the configuration from a custom location eg S3
 *
 * @author Cosmin Baciu
 * @since 5.2
 */
public interface DomibusConfigurationManagerSpi {

    //the location of the jar file containing the custom configuration provider
    String DOMIBUS_CONFIGURATION_PROVIDER_LOCATION = "domibus.configuration.provider.location";

    //the configuration provider class which will be loaded from the configuration provider jar
    String DOMIBUS_CONFIGURATION_PROVIDER_CLASS = "domibus.configuration.provider.class";

    /**
     * The name of the configuration provider
     */
    String getName();

    /**
     * It is called by Domibus to initialize the configuration provider
     */
    void initialize();

    /**
     * Retrieve a Domibus configuration resource or a jar file: eg domibus.properties, domains/super-domibus.properties, domains/tenant1-domibus.properties, plugins/config/ws-plugin.properties, etc
     * @param location The location of the resource relative to the domibus/config directory
     * @param description The description of the resource, it can contain the name or the type of the resource eg domibus.properties, pluginJar, extensionJar, etc
     * @return The retrieved resource
     */
    DomibusResource getResource(String location, String description);

    /**
     * Stores the Domibus resource into the provided location
     * @param domibusResource The Domibus resource to store
     */
    void storeResource(DomibusResource domibusResource);

    /**
     * Deletes the resource
     * @param domibusResource The resource to delete
     */
    void deleteResource(DomibusResource domibusResource);

    /**
     * Backup the provided Domibus resource into the provided location.
     * @param domibusResource The Domibus resource to backup
     * @param location The backup location is relative to the original resource location
     */
    void backupResource(DomibusResource domibusResource, String location);

    /**
     * Get the list of available domains in case of multitenant deployment. It is used by Domibus to get the domain configuration resources
     * @return The list of available domains
     */
    List<String> getDomains();

    /**
     * Find the Domibus configuration resources in the specified parent location based on a pattern eg files for the plugins, plugins/domains, extension, extension/domains
     *
     * @param parent The parent location where to look for resources
     * @param pattern The pattern to use for matching the resources
     * @param searchStrategy Which strategy to use for searching for files
     * @param description The description of the resource normally it is a value from DomibusResourceType class. It can contain the name or the type of the resource eg domibus.properties, pluginJar, extensionJar, etc
     * @return The found resources
     */
    List<DomibusResource> findFiles(String parent,
                                    String pattern,
                                    SearchStrategy searchStrategy,
                                    List<String> excludeIfContaining,
                                    String description);

}
