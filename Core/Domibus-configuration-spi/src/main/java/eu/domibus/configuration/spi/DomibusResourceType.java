package eu.domibus.configuration.spi;

/**
 * Contains the description of various resources used in Domibus
 *
 * @author Cosmin Baciu
 * @since 5.2
 */
public class DomibusResourceType {

    public static final String DOMIBUS_EH_CACHE_FILE = "ehcache.xml";
    public static final String DOMIBUS_PLUGIN_EH_CACHE_FILE = "plugin-ehcache.xml";
    public static final String DOMIBUS_EMBEDDED_ACTIVEMQ_FILE = "activemq.xml";
    public static final String DOMIBUS_PROPERTIES_FILE = "domibus.properties";
    public static final String DOMIBUS_TENANT_PROPERTIES_FILE = "tenant-domibus.properties";
    public static final String DOMIBUS_PLUGIN_PROPERTIES_FILE = "pluginProperties";
    public static final String DOMIBUS_PLUGIN_JAR_FILE = "pluginJar";
    public static final String DOMIBUS_TENANT_PLUGIN_PROPERTIES_FILE = "tenant-pluginProperties";
    public static final String DOMIBUS_EXTENSION_PROPERTIES_FILE = "extensionProperties";
    public static final String DOMIBUS_EXTENSION_JAR_FILE = "extensionJar";
    public static final String DOMIBUS_TENANT_EXTENSION_PROPERTIES_FILE = "tenant-extensionProperties";
    public static final String DOMIBUS_SUPER_PROPERTIES_FILE = "super-domibus.properties";
    public static final String DOMIBUS_SECURITY_POLICY = "securityPolicy";
    public static final String DOMIBUS_TENANT_LOGBACK_FILE = "tenant-logback.xml";
    public static final String DOMIBUS_LOGBACK_FILE = "logback.xml";
    public static final String DOMIBUS_TRUSTSTORE = "domibus.truststore";
    public static final String DOMIBUS_KEYSTORE = "domibus.keystore";
    public static final String DOMIBUS_TLS_MSH_TRUSTSTORE = "TLS.MSH.truststore";
}
