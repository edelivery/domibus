package eu.domibus.ext.services;

import java.time.ZonedDateTime;

/**
 * @author François Gautier
 * @since 5.2
 */
public interface TsidUtilExtService {

    ZonedDateTime getZonedDateTimeFromTsid(Long tsid);

}
