package eu.domibus.ext.services;

public enum ExecutorWaitPolicyExt {
    WAIT,
    NO_WAIT;
}
