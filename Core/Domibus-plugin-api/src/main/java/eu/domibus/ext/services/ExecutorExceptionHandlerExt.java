package eu.domibus.ext.services;

public interface ExecutorExceptionHandlerExt {

    void handleException(Throwable e);
}
