package eu.domibus.ext.exceptions;

public enum UserMessageExceptionTypeEnum {

    EBMS3,
    SOAP_FAULT;
}
