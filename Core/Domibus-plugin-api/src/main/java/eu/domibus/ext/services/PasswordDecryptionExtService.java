package eu.domibus.ext.services;

import eu.domibus.ext.domain.DomainDTO;

/**
 * An interface containing utility operations for decrypting passwords.
 *
 * @author Soumya Chandran
 * @since 5.2
 */
public interface PasswordDecryptionExtService {

    String decryptProperty(DomainDTO domain, String propertyName, String encryptedFormatValue);

    boolean isValueEncrypted(final String propertyValue);

    String decryptPropertyIfEncrypted(DomainDTO domain, String propertyName, String propertyValue);
}
