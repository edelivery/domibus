package eu.domibus.ext.services;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

/**
 * Interface allowing Domibus extension/plugin to use TLS Generic configuration of Domibus.
 *
 * @author Breaz Ionut
 * @since 5.2
 */
public interface TLSGenericReaderExtService {

    /**
     * Returns a new {@link SSLContext} that uses the TLS Generic truststore from Domibus for the current domain.
     */
    SSLContext getTLSGenericSSLContext() throws NoSuchAlgorithmException, KeyManagementException;

    KeyStore getTLSGenericTruststore();

    boolean useCacertsWithTLSGenericTruststore();
}
