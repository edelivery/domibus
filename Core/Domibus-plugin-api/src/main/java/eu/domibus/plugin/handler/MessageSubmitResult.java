package eu.domibus.plugin.handler;

import eu.domibus.common.MessageStatus;

import java.util.List;

/**
 * Contains the ids of the submitted UserMessage and payloads
 *
 *  @author Cosmin Baciu
 *  @since 5.2
 */
public class MessageSubmitResult {

    /**
     * The AS4 message id
     */
    protected String as4MessageId;

    /**
     * The database entity id of the UserMessage
     */
    protected Long messageEntityId;

    /**
     * The UserMessage status
     */
    protected MessageStatus messageStatus;

    /**
     * The ids of the submitted payloads
     */
    protected List<PayloadSubmitResult> payloadSubmitResults;

    public String getAs4MessageId() {
        return as4MessageId;
    }

    public void setAs4MessageId(String as4MessageId) {
        this.as4MessageId = as4MessageId;
    }

    public Long getMessageEntityId() {
        return messageEntityId;
    }

    public void setMessageEntityId(Long messageEntityId) {
        this.messageEntityId = messageEntityId;
    }

    public MessageStatus getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(MessageStatus messageStatus) {
        this.messageStatus = messageStatus;
    }

    public List<PayloadSubmitResult> getPayloadSubmitResults() {
        return payloadSubmitResults;
    }

    public void setPayloadSubmitResults(List<PayloadSubmitResult> payloadSubmitResults) {
        this.payloadSubmitResults = payloadSubmitResults;
    }
}
