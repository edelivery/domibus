package eu.domibus.plugin.handler;

/**
 * Contains the ids of the submitted payload
 *
 * @author Cosmin Baciu
 * @since 5.2
 */
public class PayloadSubmitResult {

    /**
     * The payload content id
     */
    protected String contentId;

    /**
     * The database entity id of the payload
     */
    protected Long payloadEntityId;

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public Long getPayloadEntityId() {
        return payloadEntityId;
    }

    public void setPayloadEntityId(Long payloadEntityId) {
        this.payloadEntityId = payloadEntityId;
    }
}
