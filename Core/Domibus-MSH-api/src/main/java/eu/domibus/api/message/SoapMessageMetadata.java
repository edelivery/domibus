package eu.domibus.api.message;

/**
 * Contains metadata about the incoming SoapMessage including the HTTP request
 *
 * @since 5.2
 * @author Cosmin Baciu
 */
public class SoapMessageMetadata {

    protected String contentType;
    protected String httpProtocolHeadersJson;
    protected String messagePropertiesValueJson;

    public SoapMessageMetadata(String contentType, String httpProtocolHeadersJson, String messagePropertiesValueJson) {
        this.contentType = contentType;
        this.httpProtocolHeadersJson = httpProtocolHeadersJson;
        this.messagePropertiesValueJson = messagePropertiesValueJson;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getHttpProtocolHeadersJson() {
        return httpProtocolHeadersJson;
    }

    public void setHttpProtocolHeadersJson(String httpProtocolHeadersJson) {
        this.httpProtocolHeadersJson = httpProtocolHeadersJson;
    }

    public String getMessagePropertiesValueJson() {
        return messagePropertiesValueJson;
    }

    public void setMessagePropertiesValueJson(String messagePropertiesValueJson) {
        this.messagePropertiesValueJson = messagePropertiesValueJson;
    }
}
