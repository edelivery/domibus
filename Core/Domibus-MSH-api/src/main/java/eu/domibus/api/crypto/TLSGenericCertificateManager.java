package eu.domibus.api.crypto;

import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.security.TrustStoreEntry;

import java.security.KeyStore;
import java.util.List;

/**
 * This should be used only for https requests that do not involve communication between access points
 *
 * @author Ionut Breaz
 * @since 5.2
 */
public interface TLSGenericCertificateManager {
    String TLS_GENERIC_TRUSTSTORE_NAME = "TLS.Generic.truststore";

    KeyStore getTLSGenericTruststore();

    List<TrustStoreEntry> getTrustStoreEntries();

    KeystorePersistenceInfo getPersistenceInfo();

    KeyStoreContentInfo getTruststoreContent();

    void replaceTrustStore(KeyStoreContentInfo storeInfo);

    void resetTLSTruststore();

    boolean addCertificate(byte[] certificateData, final String alias);

    boolean removeCertificate(String alias);
}
