package eu.domibus.api.crypto;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * @author Ionut Breaz
 * @since 5.2
 */
public interface TLSGenericSSLContext {
    SSLConnectionSocketFactory getNewSSLConnectionSocketFactory() throws NoSuchAlgorithmException, KeyManagementException;

    SSLConnectionSocketFactory getNewSSLConnectionSocketFactory(String sslProtocol, String[] supportedProtocols) throws NoSuchAlgorithmException, KeyManagementException;

    SSLContext getNewTLSGenericSSLContext() throws NoSuchAlgorithmException, KeyManagementException;

    SSLContext getNewTLSGenericSSLContext(String sslProtocol) throws NoSuchAlgorithmException, KeyManagementException;

    String[] getSupportedProtocols();
}
