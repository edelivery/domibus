package eu.domibus.api.security;

/**
 * Keeps all security message data needed to determine the SecurityProfile, together with the determined SecurityProfile
 *
 * @author Lucian FURCA
 * @author Cosmin Baciu
 * @since 5.2
 */
public class SecurityProfileMetadata {
    //signature
    protected String signatureAlgorithm;

    //encryption eg RSA
    protected String encryptionKeyTransportAlgorithm;
    protected String encryptionMGFAlgorithm;
    protected String encryptionDigestAlgorithm;

    //encryption eg X25519
    protected String encryptionKeyAgreementMethod;
    protected String encryptionKeyDerivationFunction;
    
    //create builder methods for all fields
    public SecurityProfileMetadata signatureSignatureAlgorithm(String signatureSignatureAlgorithm) {
        this.signatureAlgorithm = signatureSignatureAlgorithm;
        return this;
    }  

    public SecurityProfileMetadata encryptionKeyTransportAlgorithm(String encryptionKeyTransportAlgorithm) {
        this.encryptionKeyTransportAlgorithm = encryptionKeyTransportAlgorithm;
        return this;
    }
    
    public SecurityProfileMetadata encryptionMGFAlgorithm(String encryptionMGFAlgorithm) {
        this.encryptionMGFAlgorithm = encryptionMGFAlgorithm;
        return this;
    }
    
    public SecurityProfileMetadata encryptionDigestAlgorithm(String encryptionDigestAlgorithm) {
        this.encryptionDigestAlgorithm = encryptionDigestAlgorithm;
        return this;
    }

    public SecurityProfileMetadata encryptionKeyAgreementMethod(String encryptionKeyAgreementMethod) {
        this.encryptionKeyAgreementMethod = encryptionKeyAgreementMethod;
        return this;
    }
    
    public SecurityProfileMetadata encryptionKeyDerivationFunction(String encryptionKeyDerivationFunction) {
        this.encryptionKeyDerivationFunction = encryptionKeyDerivationFunction;
        return this;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public String getEncryptionKeyTransportAlgorithm() {
        return encryptionKeyTransportAlgorithm;
    }

    public void setEncryptionKeyTransportAlgorithm(String encryptionKeyTransportAlgorithm) {
        this.encryptionKeyTransportAlgorithm = encryptionKeyTransportAlgorithm;
    }

    public String getEncryptionMGFAlgorithm() {
        return encryptionMGFAlgorithm;
    }

    public void setEncryptionMGFAlgorithm(String encryptionMGFAlgorithm) {
        this.encryptionMGFAlgorithm = encryptionMGFAlgorithm;
    }

    public String getEncryptionDigestAlgorithm() {
        return encryptionDigestAlgorithm;
    }

    public void setEncryptionDigestAlgorithm(String encryptionDigestAlgorithm) {
        this.encryptionDigestAlgorithm = encryptionDigestAlgorithm;
    }

    public String getEncryptionKeyAgreementMethod() {
        return encryptionKeyAgreementMethod;
    }

    public void setEncryptionKeyAgreementMethod(String encryptionKeyAgreementMethod) {
        this.encryptionKeyAgreementMethod = encryptionKeyAgreementMethod;
    }

    public String getEncryptionKeyDerivationFunction() {
        return encryptionKeyDerivationFunction;
    }

    public void setEncryptionKeyDerivationFunction(String encryptionKeyDerivationFunction) {
        this.encryptionKeyDerivationFunction = encryptionKeyDerivationFunction;
    }
}
