package eu.domibus.api.payload;

import eu.domibus.api.model.DatabasePartition;
import eu.domibus.api.model.PartInfo;
import eu.domibus.api.model.PartInfoFileNameSpiDto;
import eu.domibus.api.model.UserMessage;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author François Gautier
 * @since 5.0
 */
public interface PartInfoService {

    void create(PartInfo partInfo, UserMessage userMessage);

    List<PartInfo> findPartInfo(UserMessage userMessage);

    List<PartInfo> findPartInfo(long entityId);

    Long findPartInfoTotalLength(long entityId);

    PartInfo findPartInfo(Long messageEntityId, String cid);

    void clearPayloadData(long entityId);

    void clearFileSystemPayloads(List<PartInfo> partInfos);

    void deletePayloadFiles(List<PartInfoFileNameSpiDto> filenames);

    List<PartInfoFileNameSpiDto> findFileSystemPayloadFilenames(List<Long> userMessageEntityIds);

    /**
     * The length of the payloads is ONLY accessible here with the fsPlugin
     * The other plugin will return {@code false}
     * see EDELIVERY-14202
     *
     * @param partInfos of the userMessage
     * @return {@code true} if the sum of the payload length > {@link eu.domibus.api.property.DomibusPropertyMetadataManagerSPI#DOMIBUS_DISPATCHER_SPLIT_AND_JOIN_PAYLOADS_SCHEDULE_THRESHOLD}
     */
    boolean scheduleSourceMessagePayloads(List<PartInfo> partInfos);

    void loadBinaryData(PartInfo partInfo);

    void deleteAllPayloadFromFileSystem(List<DatabasePartition> toDeletePartitionNames);

    String getPayloadFolder(long entityId);

    String getPayloadFolder(ZonedDateTime currentDate);
}
