package eu.domibus.api.security;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * @author Lucian FURCA
 * @since 5.1
 */
public enum CertificatePurpose {
    SIGN("sign"),
    ENCRYPT("encrypt"),
    DECRYPT("decrypt");

    public static CertificatePurpose lookupByName(String name) {
        return Arrays.stream(values())
                .filter(certPurpose -> StringUtils.equalsIgnoreCase(certPurpose.name(), name))
                .findFirst().
                orElse(null);
    }

    private final String certificatePurpose;

    CertificatePurpose(final String certificatePurpose) {
        this.certificatePurpose = certificatePurpose;
    }

    public String getCertificatePurpose() {
        return this.certificatePurpose;
    }
}
