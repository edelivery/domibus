package eu.domibus.api.pki;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

/**
 * Interface describing the persistence of a keyStore
 *
 * @author Ion Perpegel
 * @since 5.1
 */
public interface KeystorePersistenceInfo {

    /**
     * The name of the keyStore
     *
     * @return
     */
    String getName();

    /**
     * if the keyStore must exist or not
     *
     * @return
     */
    boolean isOptional();

    /**
     * The file location of the keyStore
     *
     * @return
     */
    String getFileLocation();

    /**
     * The type of the keyStore
     *
     * @return
     */
    String getType();

    /**
     * The password of the keyStore
     *
     * @return
     */
    String getPassword();

    void updatePassword(String password);

    void updateType(String type);

    void updateFileLocation(String fileLocation);

    /**
     * KeyStoreContentInfo can be associated to a truststore or to a keystore.
     * While truststores would normally support all access operations, keystores can have limited support (e.g. HSM keystores)
     * This flag indicates the keystore supports download operations.
     * @return true if the keystore is downloadable
     */
    boolean isDownloadable();

    /**
     * KeyStoreContentInfo can be associated to a truststore or to a keystore.
     * While truststores would normally support all access operations, keystores can have limited support (e.g. HSM keystores)
     * This flag indicates the keystore doesn't support write operations.
     * @return true if the keystore is read only
     */
    boolean isReadOnly();

    default String getKeyEntryPassword() {
        return null;
    }

    default boolean exists() {
        return StringUtils.isNotBlank(getFileLocation());
    }

    default KeystorePersistenceInfo createDefault(String storeFileName, String password) {
        // createDefault needs to be implemented only for the optional keystores that can be missing from start
        throw new NotImplementedException("createDefault is not implemented");
    }
}
