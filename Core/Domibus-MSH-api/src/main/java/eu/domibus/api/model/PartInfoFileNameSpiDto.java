package eu.domibus.api.model;

public class PartInfoFileNameSpiDto {

    protected String fileName;
    protected String payloadSpiIdentifier;

    public PartInfoFileNameSpiDto(String fileName, String payloadSpiIdentifier) {
        this.fileName = fileName;
        this.payloadSpiIdentifier = payloadSpiIdentifier;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPayloadSpiIdentifier() {
        return payloadSpiIdentifier;
    }
}
