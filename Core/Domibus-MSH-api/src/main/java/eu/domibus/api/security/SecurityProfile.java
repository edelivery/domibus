package eu.domibus.api.security;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author Lucian FURCA
 * @since 5.1
 */
public class SecurityProfile {

    public static final String RSA = "rsa";

    protected String code;
    protected String name;
    protected Integer order;

    public SecurityProfile(String code, String name, Integer order) {
        this.code = code;
        this.name = name;
        this.order = order;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("profileCode", code)
                .append("profileName", name)
                .append("order", order)
                .toString();
    }
}
