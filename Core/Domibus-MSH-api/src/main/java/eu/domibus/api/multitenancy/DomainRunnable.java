package eu.domibus.api.multitenancy;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

/**
 * Sets the domain on the thread
 *
 * @author Thomas Dussart
 * @since 4.0.1
 */
public class DomainRunnable implements Runnable {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomainRunnable.class);

    protected DomainContextProvider domainContextProvider;
    protected Domain domain;

    public DomainRunnable(final DomainContextProvider domainContextProvider, final Domain domain) {
        this.domainContextProvider = domainContextProvider;
        this.domain = domain;
    }

    @Override
    public void run() {
        LOG.debug("Setting domain to [{}]", domain);
        domainContextProvider.setCurrentDomain(domain);
    }
}
