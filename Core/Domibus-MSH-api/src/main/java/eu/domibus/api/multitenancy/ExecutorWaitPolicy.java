package eu.domibus.api.multitenancy;

public enum ExecutorWaitPolicy {
    WAIT,
    NO_WAIT;
}
