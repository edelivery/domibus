package eu.domibus.api.multitenancy;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Runs a series of tasks(preRunables) before running the main task
 *
 * @author Cosmin Baciu
 * @since 5.2
 */
public class CompositeCallable implements Callable {

    public static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(CompositeCallable.class);

    /**
     * A list of runnables to run before the main task
     */
    protected List<Runnable> preRunnables = new ArrayList<>();

    /**
     * The main task to run
     */
    protected Callable mainTask;
    protected ExecutorExceptionHandler errorHandler;

    /**
     * Trigger the execution of the composite runnable.
     * It executes first all runnables tasks and at last the main task
     *
     * @return
     * @throws Exception
     */
    @Override
    public Object call() throws Exception {
        for (Runnable runnable : preRunnables) {
            runnable.run();
        }
        try {
            return mainTask.call();
        } catch (Exception ex) {
            if (errorHandler != null) {
                LOG.debug("Running the error handler", ex);
                errorHandler.handleException(ex);
                return null;
            } else {
                LOG.error("Error executing task", ex);
                throw new DomainTaskException("Could not execute task", ex);
            }
        } finally {
            LOG.clearCustomKeys();
        }
    }

    public void addRunnable(Runnable runnable) {
        preRunnables.add(runnable);
    }

    public void setMainTask(Callable mainTask) {
        this.mainTask = mainTask;
    }

    public void setErrorHandler(ExecutorExceptionHandler errorHandler) {
        this.errorHandler = errorHandler;
    }
}
