package eu.domibus.api.multitenancy;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

/**
 * Clears the domain set on the thread
 *
 * @author Cosmin Baciu
 * @since 4.0.1
 */
public class ClearDomainRunnable implements Runnable {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ClearDomainRunnable.class);

    protected DomainContextProvider domainContextProvider;

    public ClearDomainRunnable(final DomainContextProvider domainContextProvider) {
        this.domainContextProvider = domainContextProvider;
    }

    @Override
    public void run() {
        LOG.debug("Clearing domain");
        domainContextProvider.clearCurrentDomain();
    }
}
