package eu.domibus.api.crypto;

import eu.domibus.api.multitenancy.DomainsAware;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.pki.KeystorePersistenceInfo;
import eu.domibus.api.security.TrustStoreEntry;
import org.apache.cxf.configuration.jsse.TLSClientParameters;

import java.util.List;

/**
 * Responsible for managing the TLS MSH truststore and TLS MSH keystore
 * This should be used only for one-way or two-way SSL between access points
 *
 * @author Ion Perpegel
 * @since 5.0
 */
public interface TLSMshCertificateManager extends DomainsAware {

    String TLS_MSH_TRUSTSTORE_NAME = "TLS.MSH.truststore";
    String TLS_MSH_KEYSTORE_NAME = "TLS.MSH.keystore";

    /**
     * Replaces the truststore pointed by the store info with the one provided as parameters
     *
     * @param storeInfo all neede info about the store
     * @throws CryptoException
     */
    void replaceTrustStore(KeyStoreContentInfo storeInfo);

    /**
     * Returns the certificate entries found in the tls truststore
     *
     * @return the list of entries
     */
    List<TrustStoreEntry> getTrustStoreEntries();

    /**
     * Returns the tls truststore content
     *
     * @return the content object
     */
    KeyStoreContentInfo getTruststoreContent();

    /**
     * Adds the specified certificate to the tls truststore content
     * @param certificateData the content in bytes
     * @param alias the name of the certificate
     * @return true if the certificate was actually added, false otherwise
     */
    boolean addCertificate(byte[] certificateData, final String alias);

    /**
     * Removes the specified certificate from the tls truststore content
     *
     * @param alias the name of the certificate
     * @return true if the certificate was removed, false otherwise
     */
    boolean removeCertificate(String alias);

    /**
     * Reads the truststore from a disk location and persists it in the DB if not already there
     */
    void saveStoresFromDBToDisk();

    KeystorePersistenceInfo getPersistenceInfo();

    String getStoreFileExtension();

    /**
     * Resets the TLS truststore cache on the local machine and other nodes in the cluster
     */
    void resetTLSTruststore();

    TLSClientParameters getTlsClientParameters(String currentDomainCode);
}
