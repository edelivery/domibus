package eu.domibus.api.ebms3.model;

import eu.domibus.api.security.SecurityProfileMetadata;

import java.util.Objects;

/**
 * @author Lucian FURCA
 * @since 5.2
 */
public class Ebms3MessagingWithSecurityProfileMetadata {
    private Ebms3Messaging ebms3Messaging;
    private SecurityProfileMetadata securityProfileMetadata;

    public Ebms3MessagingWithSecurityProfileMetadata(Ebms3Messaging ebms3Messaging, SecurityProfileMetadata securityProfileMetadata) {
        this.ebms3Messaging = ebms3Messaging;
        this.securityProfileMetadata = securityProfileMetadata;
    }

    public Ebms3Messaging getEbms3Messaging() {
        return ebms3Messaging;
    }

    public void setEbms3Messaging(Ebms3Messaging ebms3Messaging) {
        this.ebms3Messaging = ebms3Messaging;
    }

    public SecurityProfileMetadata getSecurityProfileMetadata() {
        return securityProfileMetadata;
    }

    public void setSecurityProfileMetadata(SecurityProfileMetadata securityProfileMetadata) {
        this.securityProfileMetadata = securityProfileMetadata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ebms3MessagingWithSecurityProfileMetadata that = (Ebms3MessagingWithSecurityProfileMetadata) o;
        return Objects.equals(ebms3Messaging, that.ebms3Messaging) && Objects.equals(securityProfileMetadata, that.securityProfileMetadata);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ebms3Messaging, securityProfileMetadata);
    }

    @Override
    public String toString() {
        return "Ebms3MessagingWithSecurityProfileMetadata{" +
                "ebms3Messaging=" + ebms3Messaging +
                ", securityProfileMetadata=" + securityProfileMetadata +
                '}';
    }
}
