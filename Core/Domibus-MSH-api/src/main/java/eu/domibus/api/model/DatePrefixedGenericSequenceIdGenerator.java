package eu.domibus.api.model;

import eu.domibus.api.cluster.ClusterNodeIdentifierService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.spring.SpringContextProvider;
import io.hypersistence.tsid.TSID;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Properties;

/**
 * Domibus primary key generator. Generates TSID(Time-Sorted Unique Identifiers (TSID)) primary keys keys.
 *
 * <p>
 * TSID has 2 components:
 * <ul
 * <li>
 * Time component (42 bits). This is the count of milliseconds since 2020-01-01 00:00:00 UTC.
 * </li>
 * <li>
 * Random component (22 bits). The Random component has 2 sub-parts: Node ID (0 to 20 bits) and Counter (2 to 22 bits)
 * </li>
 * </ul>
 * </p>
 * <p>
 * For more details see https://github.com/vladmihalcea/hypersistence-tsid.
 *
 * @author François Gautier
 * @author Cosmin Baciu
 * @since 5.0
 */
@Component
public class DatePrefixedGenericSequenceIdGenerator implements IdentifierGenerator, Configurable {

    protected TSID.Factory tsidFactory;

    protected Boolean isClusterDeployment;

    @Override
    public void configure(Type type, Properties params,
                          ServiceRegistry serviceRegistry) throws MappingException {
    }

    /**
     * Generates a TSID id. Eg: 477188111301737216
     * It reuses the same TSID factory instance for each generation to minimize the risk of collisions
     */
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        if (tsidFactory == null) {
            tsidFactory = getFactory();
        }

        final TSID tsid = tsidFactory.generate();
        final long generatedNumber = tsid.toLong();
        return generatedNumber;
    }

    protected TSID.Factory getFactory() {
        if (isClusterDeployment()) {
            ClusterNodeIdentifierService clusterNodeIdentifierService = SpringContextProvider.getApplicationContext().getBean(ClusterNodeIdentifierService.class);
            Integer node = clusterNodeIdentifierService.getNodeId();
            int nodeIdBits = clusterNodeIdentifierService.getNodeIdBits();
            return TSID.Factory.builder()
                    .withRandomFunction(TSID.Factory.THREAD_LOCAL_RANDOM_FUNCTION)
                    .withNode(node)
                    .withNodeBits(nodeIdBits)
                    .build();
        }
        return TSID.Factory.builder()
                .withRandomFunction(TSID.Factory.THREAD_LOCAL_RANDOM_FUNCTION)
                .build();
    }

    protected boolean isClusterDeployment() {
        if (isClusterDeployment == null) {
            DomibusConfigurationService domibusConfigurationService = SpringContextProvider.getApplicationContext().getBean(DomibusConfigurationService.class);
            isClusterDeployment = domibusConfigurationService.isClusterDeployment();
        }
        return isClusterDeployment;
    }
}
