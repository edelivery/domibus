package eu.domibus.api.pull;

public interface PullRequestSenderService {

    void initiatePullRequest(String legName, String responder);
}
