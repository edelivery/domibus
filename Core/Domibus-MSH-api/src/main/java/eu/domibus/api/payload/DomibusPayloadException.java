package eu.domibus.api.payload;

import eu.domibus.api.exceptions.DomibusCoreErrorCode;
import eu.domibus.api.exceptions.DomibusCoreException;

public class DomibusPayloadException extends DomibusCoreException {

    public DomibusPayloadException(String message) {
        super(DomibusCoreErrorCode.DOM_014, message);
    }

    public DomibusPayloadException(String message, Throwable cause) {
        super(DomibusCoreErrorCode.DOM_014, message, cause);
    }
}
