package eu.domibus.api.datasource;

import org.apache.commons.io.input.AutoCloseInputStream;

import javax.activation.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
public class AutoCloseDataSource implements DataSource {

    protected String name;
    protected String contentType;
    protected InputStream inputStream;

    public AutoCloseDataSource(String name, String contentType, InputStream inputStream) {
        this.name = name;
        this.contentType = contentType;
        this.inputStream = inputStream;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new AutoCloseInputStream(inputStream);
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        throw new UnsupportedOperationException("AutoCloseDataSource does not support getOutputStream method");
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public String getName() {
        return name;
    }
}
