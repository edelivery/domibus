package eu.domibus.api.pki;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.security.KeyStore;

/**
 * @author G. Maier
 * @since 5.2
 */
public class HsmKeyStoreContentInfo extends KeyStoreContentInfo {
    private KeyStore keyStore;

    public KeyStore getKeyStore() {
        return keyStore;
    }

    public void setKeyStore(KeyStore keyStore) {
        this.keyStore = keyStore;
    }

    @Override
    public byte[] getContent() {
        throw new UnsupportedOperationException("The content of PKCS11 key stores is not accessible");
    }

    @Override
    public void setContent(byte[] content) {
        throw new UnsupportedOperationException("The content of PKCS11 key stores is not accessible");
    }

    @Override
    public KeyStore loadKeyStore() {
        return getKeyStore();
    }

    @Override
    public boolean isReadOnly() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        HsmKeyStoreContentInfo store = (HsmKeyStoreContentInfo) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(keyStore, store.keyStore)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(keyStore)
                .toHashCode();
    }
}
