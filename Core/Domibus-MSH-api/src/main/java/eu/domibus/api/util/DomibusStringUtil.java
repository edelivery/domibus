package eu.domibus.api.util;

import java.util.Map;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SEND_MESSAGE_MESSAGE_ID_PATTERN;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_VALID_STRING_PATTERN;

/**
 * @author Soumya Chandran
 * @since 5.1
 */
public interface DomibusStringUtil {

    String VALID_STRING_REGEX = DOMIBUS_VALID_STRING_PATTERN;
    String MESSAGE_ID_PATTERN = DOMIBUS_SEND_MESSAGE_MESSAGE_ID_PATTERN;
    String unCamelCase(String str);

    boolean isStringLengthLongerThanDefaultMaxLength(String testString);

    boolean isTrimmedStringLengthLongerThanDefaultMaxLength(String testString);

    boolean isStringLengthLongerThan1024Chars(String testString);

    /**
     * replacing all special characters except [a-zA-Z0-9] and [@.-] with _ from any string
     *
     * @param fileName string to be sanitized by removing special characters
     * @return sanitized fileName
     */
    String sanitizeFileName(String fileName);

    boolean isValidString(String name);

    boolean isValidMessageId(String messageId);

    /**
     * Converts a string of key/value pairs separated by a separator into a map. Eg "key1=value1;key2=value2"
     * @param value The string containing the key/value pairs
     * @param keyValueSeparator The separator between the key and the value
     * @return A map containing the key/value pairs
     */
    Map<String, String> keyValuesToMap(String value, String keyValueSeparator);

}
