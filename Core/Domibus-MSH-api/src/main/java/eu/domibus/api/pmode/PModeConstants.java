package eu.domibus.api.pmode;

public final class PModeConstants {

    public static final String SECURITY_PROFILE_CONFIGURATION = "DOMIBUS_SECURITY_PROFILE_CONFIGURATION";
    public static final String PMODE_KEY_CONTEXT_PROPERTY = "PMODE_KEY_CONTEXT_PROPERTY";
    public static final String PMODEKEY_SEPARATOR = "_pMK_SEP_";
}
