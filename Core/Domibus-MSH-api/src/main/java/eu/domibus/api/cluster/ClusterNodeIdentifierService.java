package eu.domibus.api.cluster;

/**
 * @author Ion Perpegel
 * @since 5.2
 */
public interface ClusterNodeIdentifierService {

    /**
     * fetches the node id from the database or creates a new one if it does not exist
     */
    public void fetchOrCreateNodeId();


    /**
     * @return the node id
     */
    public int getNodeId();

    /**
     * @return the length of the node id
     */
    int getNodeIdBits();
}
