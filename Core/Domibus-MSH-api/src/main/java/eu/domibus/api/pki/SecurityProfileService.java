package eu.domibus.api.pki;

import eu.domibus.api.security.CertificatePurpose;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.security.SecurityProfileException;

import java.util.List;

/**
 * Provides services needed by the Security Profiles feature
 *
 * @author Lucian FURCA
 * @since 5.2
 */
public interface SecurityProfileService {

    /**
     * Creates the certificate alias in the form: partyName_securityProfile_certificatePurpose if security profiles are configured.
     * If no security profiles are configured then the alias consists only of the partyName, this being considered as a legacy alias
     *
     * @param partyName the party name
     * @param securityProfileConfiguration the configured security profile, or null if no security profile is configured
     * @param certificatePurpose can be SIGN, ENCRYPT, DECRYPT
     * @return the alias created as described above
     */
    String getCertificateAliasForPurpose(String partyName, SecurityProfileConfiguration securityProfileConfiguration, CertificatePurpose certificatePurpose);

    CertificatePurpose extractCertificatePurpose(String alias);

    SecurityProfile extractSecurityProfile(List<SecurityProfile> securityProfiles, String alias);

    /**
     * Returns the highest ranking Security Profile defined in the domibus.security.profile.order list property
     *
     * @throws SecurityProfileException - if none of the Security Profiles from the order list property is defined
     *
     * @return the highest ranking Security Profile defined in the domibus.security.profile.order list
     */
    SecurityProfile getTheHighestRankingDefinedSecurityProfile(List<SecurityProfile> definedSecurityProfilesList,
                                                               List<SecurityProfile> securityProfilesOrderList) throws SecurityProfileException;

}
