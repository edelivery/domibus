package eu.domibus.api.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

public interface DomibusIOUtil {

    List<String> readLines(InputStream inputStream);

    ByteArrayInputStream linesToInputStream(List<String> lines);
}
