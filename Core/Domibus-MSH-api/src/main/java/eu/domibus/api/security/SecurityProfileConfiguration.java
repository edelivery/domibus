package eu.domibus.api.security;

import eu.domibus.api.multitenancy.Domain;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;


/**
 * Keeps Security Profile data configuration corresponding to a specific alias
 *
 * @author Lucian FURCA
 * @author Cosmin Baciu
 * @since 5.1
 */
public class SecurityProfileConfiguration {

    protected Domain domain;
    protected boolean isLegacyConfiguration;

    protected Map<String, String> matchExpression = new HashMap<>();

    protected String signaturePrivateKeyAlias;

    protected String signaturePrivateKeyPassword;

    protected String encryptionPrivateKeyAlias;
    protected String encryptionPrivateKeyPassword;

    protected SecurityProfile securityProfile;

    //properties for the signature eg # ----------------------- RSA Signature -----------------------
    protected boolean signatureEnabled;
    protected boolean usePkiPath;
    protected String signatureOutKeyIdentifier;
    protected String signatureOutSignedParts;
    protected String signatureOutSignatureAlgorithm;
    protected String signatureOutDigestAlgorithm;
    protected String signatureInSignatureAlgorithm;
    protected String signatureInDigestAlgorithm;

    //properties for the encryption eg # ----------------------- RSA Encryption -----------------------
    protected boolean encryptionEnabled;
    protected String encryptionOutKeyIdentifier;
    protected String encryptionOutSymAlgorithm;
    protected String encryptionOutKeyTransportAlgorithm;
    protected String encryptionOutMGFAlgorithm;
    protected String encryptionOutDigestAlgorithm;
    protected String encryptionOutEncryptionParts;
    protected String encryptionInSymAlgorithm;
    protected String encryptionInKeyTransportAlgorithm;
    protected String encryptionInMGFAlgorithm;
    protected String encryptionInDigestAlgorithm;

    //properties for the encryption eg # ------------------ x25519 EdDSA Encryption ------------------
    protected String encryptionOutKeyAgreementMethod;
    protected String encryptionOutKeyDerivationFunction;
    protected String encryptionInKeyAgreementMethod;
    protected String encryptionInKeyDerivationFunction;

    //creates a method named copy that returns a copy of this class
    public SecurityProfileConfiguration copy() {
        return new SecurityProfileConfiguration()
                .domain(domain)
                .legacyConfiguration(isLegacyConfiguration)
                .matchExpression(matchExpression)
                .signaturePrivateKeyAlias(signaturePrivateKeyAlias)
                .signaturePrivateKeyPassword(signaturePrivateKeyPassword)
                .encryptionPrivateKeyAlias(encryptionPrivateKeyAlias)
                .encryptionPrivateKeyPassword(encryptionPrivateKeyPassword)
                .securityProfile(securityProfile)
                .signatureEnabled(signatureEnabled)
                .pkiPath(usePkiPath)
                .signatureOutKeyIdentifier(signatureOutKeyIdentifier)
                .signatureOutSignedParts(signatureOutSignedParts)
                .signatureOutSignatureAlgorithm(signatureOutSignatureAlgorithm)
                .signatureOutDigestAlgorithm(signatureOutDigestAlgorithm)
                .signatureInSignatureAlgorithm(signatureInSignatureAlgorithm)
                .signatureInDigestAlgorithm(signatureInDigestAlgorithm)
                .encryptionEnabled(encryptionEnabled)
                .encryptionOutKeyIdentifier(encryptionOutKeyIdentifier)
                .encryptionOutSymAlgorithm(encryptionOutSymAlgorithm)
                .encryptionOutKeyTransportAlgorithm(encryptionOutKeyTransportAlgorithm)
                .encryptionOutMGFAlgorithm(encryptionOutMGFAlgorithm)
                .encryptionOutDigestAlgorithm(encryptionOutDigestAlgorithm)
                .encryptionOutEncryptionParts(encryptionOutEncryptionParts)
                .encryptionInSymAlgorithm(encryptionInSymAlgorithm)
                .encryptionInKeyTransportAlgorithm(encryptionInKeyTransportAlgorithm)
                .encryptionInMGFAlgorithm(encryptionInMGFAlgorithm)
                .encryptionInDigestAlgorithm(encryptionInDigestAlgorithm)
                .encryptionOutKeyAgreementMethod(encryptionOutKeyAgreementMethod)
                .encryptionOutKeyDerivationFunction(encryptionOutKeyDerivationFunction)
                .encryptionInKeyAgreementMethod(encryptionInKeyAgreementMethod)
                .encryptionInKeyDerivationFunction(encryptionInKeyDerivationFunction);
    }

    //create builder methods for all fields
    public SecurityProfileConfiguration pkiPath(boolean usePkiPath) {
        this.usePkiPath = usePkiPath;
        return this;
    }

    public SecurityProfileConfiguration domain(Domain domain) {
        this.domain = domain;
        return this;
    }

    public SecurityProfileConfiguration signaturePrivateKeyAlias(String alias) {
        this.signaturePrivateKeyAlias = alias;
        return this;
    }

    public SecurityProfileConfiguration signaturePrivateKeyPassword(String password) {
        this.signaturePrivateKeyPassword = password;
        return this;
    }

    public SecurityProfileConfiguration encryptionPrivateKeyAlias(String alias) {
        this.encryptionPrivateKeyAlias = alias;
        return this;
    }

    public SecurityProfileConfiguration encryptionPrivateKeyPassword(String password) {
        this.encryptionPrivateKeyPassword = password;
        return this;
    }

    public SecurityProfileConfiguration securityProfile(SecurityProfile securityProfile) {
        this.securityProfile = securityProfile;
        return this;
    }

    public SecurityProfileConfiguration signatureEnabled(boolean signatureEnabled) {
        this.signatureEnabled = signatureEnabled;
        return this;
    }

    public SecurityProfileConfiguration signatureOutKeyIdentifier(String signatureOutKeyIdentifier) {
        this.signatureOutKeyIdentifier = signatureOutKeyIdentifier;
        return this;
    }

    public SecurityProfileConfiguration signatureOutSignedParts(String signatureOutSignedParts) {
        this.signatureOutSignedParts = signatureOutSignedParts;
        return this;
    }

    public SecurityProfileConfiguration signatureOutSignatureAlgorithm(String signatureOutSignatureAlgorithm) {
        this.signatureOutSignatureAlgorithm = signatureOutSignatureAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration signatureOutDigestAlgorithm(String signatureOutDigestAlgorithm) {
        this.signatureOutDigestAlgorithm = signatureOutDigestAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration signatureInSignatureAlgorithm(String signatureInSignatureAlgorithm) {
        this.signatureInSignatureAlgorithm = signatureInSignatureAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration matchExpression(Map<String, String> matchExpression) {
        this.matchExpression = matchExpression;
        return this;
    }


    public SecurityProfileConfiguration signatureInDigestAlgorithm(String signatureInDigestAlgorithm) {
        this.signatureInDigestAlgorithm = signatureInDigestAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionEnabled(boolean encryptionEnabled) {
        this.encryptionEnabled = encryptionEnabled;
        return this;
    }

    public SecurityProfileConfiguration encryptionOutKeyIdentifier(String encryptionOutKeyIdentifier) {
        this.encryptionOutKeyIdentifier = encryptionOutKeyIdentifier;
        return this;
    }

    public SecurityProfileConfiguration encryptionOutSymAlgorithm(String encryptionOutSymAlgorithm) {
        this.encryptionOutSymAlgorithm = encryptionOutSymAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionOutKeyTransportAlgorithm(String encryptionOutKeyTransportAlgorithm) {
        this.encryptionOutKeyTransportAlgorithm = encryptionOutKeyTransportAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionOutMGFAlgorithm(String encryptionOutMGFAlgorithm) {
        this.encryptionOutMGFAlgorithm = encryptionOutMGFAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionOutDigestAlgorithm(String encryptionOutDigestAlgorithm) {
        this.encryptionOutDigestAlgorithm = encryptionOutDigestAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionOutEncryptionParts(String encryptionOutEncryptionParts) {
        this.encryptionOutEncryptionParts = encryptionOutEncryptionParts;
        return this;
    }

    public SecurityProfileConfiguration encryptionInSymAlgorithm(String encryptionInSymAlgorithm) {
        this.encryptionInSymAlgorithm = encryptionInSymAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionInKeyTransportAlgorithm(String encryptionInKeyTransportAlgorithm) {
        this.encryptionInKeyTransportAlgorithm = encryptionInKeyTransportAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionInMGFAlgorithm(String encryptionInMGFAlgorithm) {
        this.encryptionInMGFAlgorithm = encryptionInMGFAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionInDigestAlgorithm(String encryptionInDigestAlgorithm) {
        this.encryptionInDigestAlgorithm = encryptionInDigestAlgorithm;
        return this;
    }

    public SecurityProfileConfiguration encryptionOutKeyAgreementMethod(String encryptionOutKeyAgreementMethod) {
        this.encryptionOutKeyAgreementMethod = encryptionOutKeyAgreementMethod;
        return this;
    }

    public SecurityProfileConfiguration encryptionOutKeyDerivationFunction(String encryptionOutKeyDerivationFunction) {
        this.encryptionOutKeyDerivationFunction = encryptionOutKeyDerivationFunction;
        return this;
    }

    public SecurityProfileConfiguration encryptionInKeyAgreementMethod(String encryptionInKeyAgreementMethod) {
        this.encryptionInKeyAgreementMethod = encryptionInKeyAgreementMethod;
        return this;
    }

    public SecurityProfileConfiguration encryptionInKeyDerivationFunction(String encryptionInKeyDerivationFunction) {
        this.encryptionInKeyDerivationFunction = encryptionInKeyDerivationFunction;
        return this;
    }

    public SecurityProfileConfiguration legacyConfiguration(boolean isLegacyConfiguration) {
        this.isLegacyConfiguration = isLegacyConfiguration;
        return this;
    }

    public boolean isLegacyConfiguration() {
        return isLegacyConfiguration;
    }

    public String getSignaturePrivateKeyAlias() {
        return signaturePrivateKeyAlias;
    }

    public String getSignaturePrivateKeyPass() {
        return signaturePrivateKeyPassword;
    }

    public SecurityProfile getSecurityProfile() {
        return securityProfile;
    }

    public boolean isSignatureEnabled() {
        return signatureEnabled;
    }

    public String getSignatureOutKeyIdentifier() {
        return signatureOutKeyIdentifier;
    }

    public String getSignatureOutSignedParts() {
        return signatureOutSignedParts;
    }

    public String getSignatureOutSignatureAlgorithm() {
        return signatureOutSignatureAlgorithm;
    }

    public String getSignatureOutDigestAlgorithm() {
        return signatureOutDigestAlgorithm;
    }

    public String getSignatureInSignatureAlgorithm() {
        return signatureInSignatureAlgorithm;
    }

    public String getSignatureInDigestAlgorithm() {
        return signatureInDigestAlgorithm;
    }

    public boolean isEncryptionEnabled() {
        return encryptionEnabled;
    }

    public String getEncryptionOutKeyIdentifier() {
        return encryptionOutKeyIdentifier;
    }

    public String getEncryptionOutSymAlgorithm() {
        return encryptionOutSymAlgorithm;
    }

    public String getEncryptionOutKeyTransportAlgorithm() {
        return encryptionOutKeyTransportAlgorithm;
    }

    public String getEncryptionOutMGFAlgorithm() {
        return encryptionOutMGFAlgorithm;
    }

    public String getEncryptionOutDigestAlgorithm() {
        return encryptionOutDigestAlgorithm;
    }

    public String getEncryptionOutEncryptionParts() {
        return encryptionOutEncryptionParts;
    }

    public String getEncryptionInSymAlgorithm() {
        return encryptionInSymAlgorithm;
    }

    public String getEncryptionInKeyTransportAlgorithm() {
        return encryptionInKeyTransportAlgorithm;
    }

    public String getEncryptionInMGFAlgorithm() {
        return encryptionInMGFAlgorithm;
    }

    public String getEncryptionInDigestAlgorithm() {
        return encryptionInDigestAlgorithm;
    }

    public String getEncryptionOutKeyAgreementMethod() {
        return encryptionOutKeyAgreementMethod;
    }

    public String getEncryptionOutKeyDerivationFunction() {
        return encryptionOutKeyDerivationFunction;
    }

    public String getEncryptionInKeyAgreementMethod() {
        return encryptionInKeyAgreementMethod;
    }

    public String getEncryptionInKeyDerivationFunction() {
        return encryptionInKeyDerivationFunction;
    }

    public String getSignaturePrivateKeyPassword() {
        return signaturePrivateKeyPassword;
    }

    public String getEncryptionPrivateKeyAlias() {
        return encryptionPrivateKeyAlias;
    }

    public String getEncryptionPrivateKeyPassword() {
        return encryptionPrivateKeyPassword;
    }

    public Domain getDomain() {
        return domain;
    }

    public Map<String, String> getMatchExpression() {
        return matchExpression;
    }

    public boolean isUsePkiPath() {
        return usePkiPath;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("domain", domain)
                .append("isLegacyConfiguration", isLegacyConfiguration)
                .append("matchExpression", matchExpression)
                .append("signaturePrivateKeyAlias", signaturePrivateKeyAlias)
                .append("encryptionPrivateKeyAlias", encryptionPrivateKeyAlias)
                .append("securityProfile", securityProfile)
                .append("signatureEnabled", signatureEnabled)
                .append("usePkiPath", usePkiPath)
                .append("signatureOutKeyIdentifier", signatureOutKeyIdentifier)
                .append("signatureOutSignedParts", signatureOutSignedParts)
                .append("signatureOutSignatureAlgorithm", signatureOutSignatureAlgorithm)
                .append("signatureOutDigestAlgorithm", signatureOutDigestAlgorithm)
                .append("signatureInSignatureAlgorithm", signatureInSignatureAlgorithm)
                .append("signatureInDigestAlgorithm", signatureInDigestAlgorithm)
                .append("encryptionEnabled", encryptionEnabled)
                .append("encryptionOutKeyIdentifier", encryptionOutKeyIdentifier)
                .append("encryptionOutSymAlgorithm", encryptionOutSymAlgorithm)
                .append("encryptionOutKeyTransportAlgorithm", encryptionOutKeyTransportAlgorithm)
                .append("encryptionOutMGFAlgorithm", encryptionOutMGFAlgorithm)
                .append("encryptionOutDigestAlgorithm", encryptionOutDigestAlgorithm)
                .append("encryptionOutEncryptionParts", encryptionOutEncryptionParts)
                .append("encryptionInSymAlgorithm", encryptionInSymAlgorithm)
                .append("encryptionInKeyTransportAlgorithm", encryptionInKeyTransportAlgorithm)
                .append("encryptionInMGFAlgorithm", encryptionInMGFAlgorithm)
                .append("encryptionInDigestAlgorithm", encryptionInDigestAlgorithm)
                .append("encryptionOutKeyAgreementMethod", encryptionOutKeyAgreementMethod)
                .append("encryptionOutKeyDerivationFunction", encryptionOutKeyDerivationFunction)
                .append("encryptionInKeyAgreementMethod", encryptionInKeyAgreementMethod)
                .append("encryptionInKeyDerivationFunction", encryptionInKeyDerivationFunction)
                .toString();
    }
}
