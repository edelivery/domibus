package eu.domibus.api.pki;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.api.security.SecurityProfileMetadata;

import java.util.List;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
public interface SecurityProfileProvider {

    List<SecurityProfileConfiguration> getSecurityProfileConfigurations(Domain domain);;

    SecurityProfile determineSecurityProfile(Domain domain, SecurityProfileMetadata securityProfileMetadata);

    SecurityProfileConfiguration getSecurityProfileConfigurationWithRSAAsDefault(Domain domain, String securityProfileCode);

    List<SecurityProfile> getSecurityProfilesPriorityList(Domain domain);

    SecurityProfile getSecurityProfileWithHighestPriority(Domain domain);

    List<SecurityProfile> getDefinedSecurityProfiles(Domain currentDomain);
}
