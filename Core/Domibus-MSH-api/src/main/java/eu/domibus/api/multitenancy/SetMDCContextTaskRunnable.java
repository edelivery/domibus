package eu.domibus.api.multitenancy;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import java.util.Map;

/**
 * Copies the LOG context map from the main thread and sets it on the child thread
 *
 * @author Cosmin Baciu
 * @since 4.1
 */
public class SetMDCContextTaskRunnable implements Runnable {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SetMDCContextTaskRunnable.class);

    protected Map<String, String> copyOfContextMap;

    public SetMDCContextTaskRunnable() {
        this.copyOfContextMap = LOG.getCopyOfContextMap();
    }

    @Override
    public void run() {
        if (copyOfContextMap != null) {
            LOG.trace("Setting MDC context map");
            LOG.setContextMap(copyOfContextMap);
            LOG.trace("Finished setting MDC context map");
        }
    }
}
