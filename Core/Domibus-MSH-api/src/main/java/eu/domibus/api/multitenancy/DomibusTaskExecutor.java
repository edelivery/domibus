package eu.domibus.api.multitenancy;

import eu.domibus.api.multitenancy.lock.DomibusSynchronizationException;
import org.springframework.security.core.Authentication;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Task executor used to schedule tasks against a specific domain or against the general schema.
 *
 * @author Cosmin Baciu
 * @since 4.0
 */
public interface DomibusTaskExecutor {

    /**
     * See documentation for {@link DomibusTaskExecutor#submitCallable(Callable, Domain, Authentication, ExecutorWaitPolicy, Long, TimeUnit, ExecutorExceptionHandler)}
     */
    <T extends Object> T submit(Callable<T> task);

    /**
     * See documentation for {@link DomibusTaskExecutor#submitCallable(Callable, Domain, Authentication, ExecutorWaitPolicy, Long, TimeUnit, ExecutorExceptionHandler)}
     */
    <T extends Object> T submit(Callable<T> task, Domain domain);

    /**
     * Launches a callable task preserving the security context
     */
    <T extends Object> T submitWithSecurityContext(Callable<T> task);

    /**
     * Executes the callable on a different thread. Sets the domain and the authentication on the new thread before executing the callable task
     *
     * @param task           The callable task to be executed
     * @param domain         The domain to be set on the thread before the callable task is executed. If null, the domain will be cleared from the thread.
     * @param authentication The authentication to be set on the thread before the callable task is executed. If null, the authentication will be cleared from the thread.
     * @param waitPolicy     The wait policy to be used for executing the task
     * @param timeout        In case wait policy is set to WAIT, how much time we wait for the task to execute before raising a timeout exception
     * @param timeUnit       The timeout unit
     * @param errorHandler   The error handler is executed in case an error is raised while executing the task
     * @return
     */
    <T extends Object> T submitCallable(Callable<T> task,
                                        Domain domain,
                                        Authentication authentication,
                                        ExecutorWaitPolicy waitPolicy,
                                        Long timeout,
                                        TimeUnit timeUnit,
                                        ExecutorExceptionHandler errorHandler);

    /**
     * See documentation for {@link DomibusTaskExecutor#submitCallable(Callable, Domain, Authentication, ExecutorWaitPolicy, Long, TimeUnit, ExecutorExceptionHandler)}
     */
    void submit(Runnable task);

    /**
     * Launches a runnable task preserving the security context
     */
    void submitWithSecurityContext(Runnable task);

    /**
     * See documentation for {@link DomibusTaskExecutor#submitCallable(Callable, Domain, Authentication, ExecutorWaitPolicy, Long, TimeUnit, ExecutorExceptionHandler)}
     */
    void submit(Runnable task, Domain domain);

    /**
     * See documentation for {@link DomibusTaskExecutor#submitCallable(Callable, Domain, Authentication, ExecutorWaitPolicy, Long, TimeUnit, ExecutorExceptionHandler)}
     */
    void submit(Runnable task, Domain domain, ExecutorWaitPolicy waitPolicy, Long timeout, TimeUnit timeUnit);

    /**
     * Executes the callable task in a synchronized way on a different thread.
     *
     * @param task        The callable task to be executed
     * @param dbLockKey   the lock key from the TB_LOCK table; uses a DB row level lock in case of cluster deployment
     * @param javaLockKey java object instance to sync on; used in case of single deployment
     *                    See also documentation for {@link DomibusTaskExecutor#submitCallable(Callable, Domain, Authentication, ExecutorWaitPolicy, Long, TimeUnit, ExecutorExceptionHandler)}
     */
    <R> R executeWithLock(final Callable<R> task,
                          final String dbLockKey,
                          final String javaLockKey,
                          Domain domain,
                          final Authentication authentication,
                          ExecutorWaitPolicy waitPolicy,
                          Long timeout,
                          TimeUnit timeUnit,
                          final ExecutorExceptionHandler errorHandler);

    /**
     * Executes the callable task in a synchronized way on the same thread as the thread which calls it.
     *
     * @param task        The callable task to be executed
     * @param dbLockKey   the lock key from the TB_LOCK table; uses a DB row level lock in case of cluster deployment
     * @param javaLockKey java object instance to sync on; used in case of single deployment
     * @throws DomibusSynchronizationException in case of errors executing the task
     */
    <R> R executeWithLock(final Callable<R> task,
                          final String dbLockKey,
                          final String javaLockKey) throws DomibusSynchronizationException;

    /**
     * Submits a long running task to be executed for a specific domain
     *
     * @param task         The task to be executed
     * @param errorHandler The error handler to be executed in case errors are thrown while running the task
     * @param domain       The domain for which the task is executed
     */
    void submitLongRunningTask(Runnable task,
                               Domain domain,
                               Authentication authentication,
                               ExecutorExceptionHandler errorHandler);


}
