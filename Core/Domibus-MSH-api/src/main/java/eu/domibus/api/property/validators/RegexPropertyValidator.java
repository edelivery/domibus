package eu.domibus.api.property.validators;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Validates the property contains a valid regular expression.
 *
 * @author Soumya Chandran
 * @since 5.2
 */
public class RegexPropertyValidator implements DomibusPropertyValidator {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(RegexPropertyValidator.class);

    @Override
    public boolean isValid(String propValue) {
        try {
            Pattern.compile(propValue);
        } catch (PatternSyntaxException exception) {
            LOG.warn("Invalid regular expression syntax:", exception);
            return false;
        }
        return true;
    }
}
