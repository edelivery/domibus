package eu.domibus.api.property.validators;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
/**
 * Validates the value of the property against the regular expression pattern.
 *
 * @author Soumya Chandran
 * @since 5.2
 */
public class PropertyValuePatternValidator implements DomibusPropertyValidator {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PropertyValuePatternValidator.class);

    String pattern;

    public PropertyValuePatternValidator(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean isValid(String propValue) {
        if (StringUtils.isBlank(this.pattern)) {
            LOG.debug("Pattern for the property type is null; exiting validation.");
            return true;
        }
        if (StringUtils.isBlank(propValue)) {
            LOG.debug("Property value is null; exiting validation.");
            return true;
        }
        return propValue.matches(this.pattern);
    }
}
