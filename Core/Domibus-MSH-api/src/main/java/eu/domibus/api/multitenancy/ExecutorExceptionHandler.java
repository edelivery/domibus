package eu.domibus.api.multitenancy;

public interface ExecutorExceptionHandler {

    void handleException(Throwable e);
}
