package eu.domibus.api.payload.encryption;

import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainsAware;
import eu.domibus.api.property.DomibusConfigurationService;

import javax.crypto.Cipher;

/**
 * @author Cosmin Baciu
 * @since 4.1.1
 */
public interface PayloadEncryptionService extends DomainsAware {

    /**
     * Creates the payload encryption key for all available domains if does not yet exists
     */
    void createPayloadEncryptionKeyForAllDomainsIfNotExists();

    /**
     * Creates the encryption key for the given domain if it does not yet exist
     */
    void createPayloadEncryptionKeyIfNotExists(Domain domain);

    Cipher getEncryptCipherForPayload();

    Cipher getDecryptCipherForPayload();

    /**
     * Determines if payload encryption is active for the given user message.
     *
     * @param userMessage The user message to check for payload encryption.
     * @return true if payload encryption is active for the message, false otherwise.
     * @see DomibusConfigurationService#isPayloadEncryptionActive(Domain)
     * @implNote This method takes into account the SplitAndJoin limitation (EDELIVERY-4749).
     *           Payload encryption is not active for split and join messages.
     */
    boolean isPayloadEncryptionActive(UserMessage userMessage);
}
