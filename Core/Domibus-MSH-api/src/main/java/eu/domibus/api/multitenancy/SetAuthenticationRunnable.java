package eu.domibus.api.multitenancy;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Sets the current authentication context on the thread
 *
 * @author Cosmin Baciu
 * @since 5.2
 */
public class SetAuthenticationRunnable implements Runnable {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SetAuthenticationRunnable.class);

    protected Authentication currentAuthentication;

    public SetAuthenticationRunnable(Authentication currentAuthentication) {
        this.currentAuthentication = currentAuthentication;
    }

    @Override
    public void run() {
        LOG.debug("Setting the authentication on the thread");
        SecurityContextHolder.getContext().setAuthentication(currentAuthentication);
    }
}
