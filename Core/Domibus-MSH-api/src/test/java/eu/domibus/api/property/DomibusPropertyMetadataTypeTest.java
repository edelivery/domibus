package eu.domibus.api.property;

import eu.domibus.api.property.validators.DomibusPropertyValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author Soumya Chandran
 * @since 5.2
 */
public class DomibusPropertyMetadataTypeTest {

    @ParameterizedTest
    @ValueSource(strings = {"localhost",
            "urn:oasis:names:tc:ebcore:partyid-type:unregistered",
            "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder",
            "/linux/path/",
            "c:/windows/path with spaces/~!@#$%^&()_+-=[]{};',.`/this_is_a_valid_windows_path",
            "file:///c:/windows/path/",
            "https://some-other-url:1234/url.aspx?param1=val&param2=val2+aaa"
    })
    public void testURIValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.URI.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"this is invalid \r\b",
            "this is invalid <"
    })
    public void testInvalid_URIValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.URI.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"50", "1-10"})
    public void testConcurrencyValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.CONCURRENCY.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"-1", "?"})
    public void testInValid_ConcurrencyValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.CONCURRENCY.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"eu.domibus.core.property.encryption.plugin.PasswordEncryptionExtServiceImplTest",
            "com.sun.xml.messaging.saaj.soap.ver1_2.SOAPMessageFactory1_2Impl"})
    public void testClassValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.CLASS.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"?"})
    public void testInvalid_ClassValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.CLASS.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"domibus.backend.jms.replyQueue",
            "jms/domibus.backend.jms.replyQueue"})
    public void testJndiValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.JNDI.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"aaa>aaa"})
    public void testInvalid_JndiValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.JNDI.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }


    @ParameterizedTest
    @ValueSource(strings = {"abc_ABC@domibus-host.com",
            "abc_ABC@domibus-host.com ; second.address@example.org "})
    public void testEmailValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.EMAIL.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"invalid@@email.com"})
    public void testInvalid_EmailValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.EMAIL.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"domibus-blue, domibus-123"})
    public void testCommaSeparatedListValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.COMMA_SEPARATED_LIST.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"aaa;"})
    public void testInvalid_CommaSeparatedListValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.COMMA_SEPARATED_LIST.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"bdxr-transport-ebms3-as4-v1p0"})
    public void testHyphenedNameValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.HYPHENED_NAME.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"abc'abc"})
    public void testInvalid_HyphenedNameValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.HYPHENED_NAME.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"12.55"})
    public void testPositiveDecimalValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.POSITIVE_DECIMAL.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"-12.55", "12.555"})
    public void testInvalid_PositiveDecimalValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.POSITIVE_DECIMAL.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "12555"})
    public void testPositiveIntegerValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.POSITIVE_INTEGER.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"12.555", "-214"})
    public void testInvalid_PositiveIntegerValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.POSITIVE_INTEGER.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }


    @ParameterizedTest
    @ValueSource(strings = {"^[A-Za-z][A-Za-z0-9-_@.]*[A-Za-z0-9]$",
            "^[\\x20-\\x7E]*$",
            "]", "/"})
    public void testRegexPropertyValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.REGEXP.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"\\", "][", "["})
    public void testInvalid_RegexPropertyValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.REGEXP.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"[++", "[", "1*w", "9abc", "@.com"})
    public void testFreeTextValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.FREE_TEXT.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Hello\tWorld", "©2024"})
    public void testInvalid_FreeTextValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.FREE_TEXT.getValidator();
        Assertions.assertFalse(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"^[\\\\x20-\\\\x7E]*$",
            "^[A-Za-z][A-Za-z0-9-_@.]*[A-Za-z0-9]$"})
    public void testPasswordValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.PASSWORD.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {" 0 0/1 * * * ?",
            "0 0 0/1 * * ?",
            "0 0 0/1 ? * * *"})
    public void testCronValidator(String value) {
        DomibusPropertyValidator validator = DomibusPropertyMetadata.Type.CRON.getValidator();
        Assertions.assertTrue(validator.isValid(value));
    }

}
