package eu.domibus.api.model;

import io.hypersistence.tsid.TSID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.*;

import static org.mockito.Mockito.when;

public class DatePrefixedGenericSequenceIdGeneratorTest {

    private DatePrefixedGenericSequenceIdGenerator idGenerator;

    @BeforeEach
    public void setUp() {
        idGenerator = new DatePrefixedGenericSequenceIdGenerator();
        idGenerator.isClusterDeployment = false;
    }

    @Test
    public void testGenerateUniqueIdsInSingleThread() throws InterruptedException, ExecutionException {
        int numberOfThreads = 1;
        int idsPerThread = 10000;

        Set<Long> uniqueIds = generateIds(numberOfThreads, idsPerThread);

        int expectedCount = numberOfThreads * idsPerThread;
        Assertions.assertEquals(expectedCount, uniqueIds.size());
    }

    @Test
    public void testGenerateUniqueIdsAcrossMultipleThreads() throws InterruptedException, ExecutionException {
        int numberOfThreads = 10;
        int idsPerThread = 1000;

        Set<Long> uniqueIds = generateIds(numberOfThreads, idsPerThread);

        int expectedCount = numberOfThreads * idsPerThread;
        Assertions.assertEquals(expectedCount, uniqueIds.size());
    }

    @Test
    public void testGenerateUniqueIdsAcrossLotsOfThreads() throws InterruptedException, ExecutionException {
        int numberOfThreads = 200;
        int idsPerThread = 50;
        Set<Long> uniqueIds = generateIds(numberOfThreads, idsPerThread);

        int expectedCount = numberOfThreads * idsPerThread;
        Assertions.assertEquals(expectedCount, uniqueIds.size());
    }

    private Set<Long> generateIds(int numberOfThreads, int idsPerThread) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        Set<Long> uniqueIds = Collections.synchronizedSet(new HashSet<>());

        List<Callable<Void>> tasks = new ArrayList<>();
        for (int i = 0; i < numberOfThreads; i++) {
            tasks.add(() -> {
                for (int j = 0; j < idsPerThread; j++) {
                    Long id = (Long) idGenerator.generate(null, null);
                    uniqueIds.add(id);
                }
                return null;
            });
        }
        List<Future<Void>> futures = executorService.invokeAll(tasks);
        for (Future<Void> future : futures) {
            future.get();
        }
        executorService.shutdown();

        return uniqueIds;
    }

    @Test
    public void testGenerateUniqueIdsInCluster() throws InterruptedException, ExecutionException {
        final int nodeBits = 4; // max 16 nodes
        int numberOfNodes = 10;

        Map<Integer, TSID.Factory> factoryMap = new ConcurrentHashMap<>();

        for (int node = 0; node < numberOfNodes; node++) {
            TSID.Factory tsidFactory = TSID.Factory.builder()
                    .withRandomFunction(TSID.Factory.THREAD_LOCAL_RANDOM_FUNCTION)
                    .withNode(node)
                    .withNodeBits(nodeBits)
                    .build();
            factoryMap.put(node, tsidFactory);
        }

        int numberOfThreadsPerNode = 100;
        int idsPerThread = 1000;

        Set<Long> uniqueIds = generateIdsInCluster(factoryMap, numberOfNodes, numberOfThreadsPerNode, idsPerThread);

        int numberOfGeneratedIds = numberOfNodes * numberOfThreadsPerNode * idsPerThread;
        int numberOfUniqueIds = uniqueIds.size();
        Assertions.assertEquals(numberOfGeneratedIds, numberOfUniqueIds);
    }

    @Test
    public void testGenerateUniqueIdsInCluster_negativeTest() throws InterruptedException, ExecutionException {
        final int nodeBits = 4; // max 16 nodes
        int numberOfNodes = 10;

        Map<Integer, TSID.Factory> factoryMap = new ConcurrentHashMap<>();

        for (int node = 0; node < numberOfNodes; node++) {
            TSID.Factory tsidFactory = TSID.Factory.builder()
                    .withRandomFunction(TSID.Factory.THREAD_LOCAL_RANDOM_FUNCTION)
                    .withNode(node % 3) // only 3 node identifiers: 0, 1, and 2
                    .withNodeBits(nodeBits)
                    .build();
            factoryMap.put(node, tsidFactory);
        }

        int numberOfThreadsPerNode = 100;
        int idsPerThread = 1000;

        Set<Long> uniqueIds = generateIdsInCluster(factoryMap, numberOfNodes, numberOfThreadsPerNode, idsPerThread);

        int numberOfGeneratedIds = numberOfNodes * numberOfThreadsPerNode * idsPerThread;
        int numberOfUniqueIds = uniqueIds.size();
        Assertions.assertTrue(numberOfUniqueIds < numberOfGeneratedIds, "We're expecting collissions in case of misconfiguration (some nodes use the same node identifier)");
    }

    private Set<Long> generateIdsInCluster(Map<Integer, TSID.Factory> factoryMap,
                                           int numberOfNodes,
                                           int numberOfThreadsPerNode,
                                           int idsPerThread) throws InterruptedException, ExecutionException {
        Set<Long> uniqueIds = Collections.synchronizedSet(new HashSet<>());

        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreadsPerNode * numberOfNodes);

        List<Callable<Void>> tasks = new ArrayList<>();
        for (int i = 0; i < numberOfThreadsPerNode; i++) {
            for (int node = 0; node < numberOfNodes; node++) {
                TSID.Factory tsidFactory = factoryMap.get(node);
                tasks.add(() -> {
                    for (int j = 0; j < idsPerThread; j++) {
                        Long id = tsidFactory.generate().toLong();
                        uniqueIds.add(id);
                    }
                    return null;
                });
            }
        }
        List<Future<Void>> futures = executorService.invokeAll(tasks);
        for (Future<Void> future : futures) {
            future.get();
        }
        executorService.shutdown();

        return uniqueIds;
    }
}