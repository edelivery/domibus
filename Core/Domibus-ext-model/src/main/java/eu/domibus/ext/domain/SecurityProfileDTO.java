package eu.domibus.ext.domain;

/**
 * @since 5.2
 * @author Cosmin Baciu
 */
public class SecurityProfileDTO {

    protected String code;
    protected String name;
    protected Integer order;

    public SecurityProfileDTO() {
    }

    public SecurityProfileDTO(String code, String name, Integer order) {
        this.code = code;
        this.name = name;
        this.order = order;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
