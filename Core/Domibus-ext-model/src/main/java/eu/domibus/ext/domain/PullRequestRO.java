package eu.domibus.ext.domain;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
@Schema(description = "Details of the pull request")
public class PullRequestRO implements Serializable {

    protected String responder;
    protected String legName;

    public String getResponder() {
        return responder;
    }

    public void setResponder(String responder) {
        this.responder = responder;
    }

    public String getLegName() {
        return legName;
    }

    public void setLegName(String legName) {
        this.legName = legName;
    }
}
