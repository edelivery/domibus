package eu.domibus.jms.weblogic;

public class JMXAuthDTO {
    String host;
    String protocol;
    int port;

    public JMXAuthDTO(String host, String protocol, int port) {
        this.host = host;
        this.protocol = protocol;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public String getProtocol() {
        return protocol;
    }

    public int getPort() {
        return port;
    }

}
