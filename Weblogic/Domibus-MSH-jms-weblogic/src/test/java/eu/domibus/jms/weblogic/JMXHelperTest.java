package eu.domibus.jms.weblogic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;

import static org.junit.jupiter.api.Assertions.*;

class JMXHelperTest {


    private JMXHelper jmxHelper;

    @BeforeEach
    void setUp() {
        jmxHelper = new JMXHelper();
    }

    @Test
    void getJmxAuthDTO() throws ReflectionException, AttributeNotFoundException, InstanceNotFoundException, MBeanException {
        String adminUrl = "t3s://domibus-test-cluster-cls:1143";
        JMXAuthDTO jmxAuthDTO = jmxHelper.getJmxAuthDTO(adminUrl);

        assertEquals(1143, jmxAuthDTO.getPort());
        assertEquals("domibus-test-cluster-cls", jmxAuthDTO.getHost());
        assertEquals("t3s", jmxAuthDTO.getProtocol());
    }
}