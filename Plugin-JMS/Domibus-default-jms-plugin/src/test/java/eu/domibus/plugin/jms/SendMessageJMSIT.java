
package eu.domibus.plugin.jms;

import eu.domibus.common.ErrorCode;
import eu.domibus.core.ebms3.EbMS3ExceptionBuilder;
import eu.domibus.core.message.MessagingService;
import eu.domibus.core.message.UserMessageLogDefaultService;
import eu.domibus.core.message.reliability.ReliabilityChecker;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.Submission;
import eu.domibus.test.PModeUtil;
import eu.domibus.test.common.JMSMessageUtil;
import eu.domibus.test.common.SubmissionUtil;
import org.apache.activemq.command.ActiveMQMapMessage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.MapMessage;
import javax.jms.Message;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static eu.domibus.plugin.jms.JMSMessageConstants.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * This class implements the test cases Receive Deliver Message-01
 *
 * @author Cosmin Baciu
 * @author martifp
 */
public class SendMessageJMSIT extends AbstractJMSPluginTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SendMessageJMSIT.class);

    @Autowired
    MessagingService messagingService;

    @Autowired
    JmsPluginTestUtil jmsPluginTestUtil;

    @Autowired
    UserMessageLogDefaultService userMessageLogService;

    @Autowired
    protected SubmissionUtil submissionUtil;

    @Autowired
    JMSMessageTransformer jmsMessageTransformer;

    @Autowired
    JMSMessageUtil jmsMessageUtil;

    @Autowired
    PModeUtil pModeUtil;

    @BeforeEach
    public void before() throws IOException, XmlProcessingException {
        super.before();
        pModeUtil.uploadPmode();

        //we consume the messages from the queue(max 3 to avoid waiting too much) in case there are leftovers from other queues;
        jmsMessageUtil.consumeMessagesSafely(JMS_BACKEND_REPLY_QUEUE_NAME, 3);
        jmsMessageUtil.consumeMessagesSafely(JMS_BACKEND_ERROR_NOTIFY_PRODUCER_QUEUE_NAME, 3);
    }

    @AfterEach
    public void clean() {
        deleteAllMessages();
    }

    /**
     * It tests the message reception by Domibus through the JMS channel.
     * It also checks that the messages are actually pushed on the right queues (dispatch and reply).
     * The message ID is cleaned to simulate the submission of the a new message.
     *
     * @throws Exception
     */
    @Test
    public void testSendMessageSuccess() throws Exception {
        //we save the JMS manager to restore it later
        Object savedJmsManagerField = itTestsService.prepareMocksForSendingMessage("validAS4Response.xml", "123", ReliabilityChecker.CheckResult.OK);

        final MapMessage mapMessage = createJMSMessageForReceive();

        LOG.debug("Submitting MapMessage: " + mapMessage);
        String messageId = UUID.randomUUID().toString();
        mapMessage.setStringProperty(MESSAGE_ID, messageId); // Cleaning the message ID since it is supposed to submit a new message.
        mapMessage.setStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY, JMSMessageConstants.MESSAGE_TYPE_SUBMIT);

        jmsPlugin.receiveMessage(mapMessage);

        //put the real manager back
        itTestsService.resetBackJmsManager(savedJmsManagerField);

        //we expect 2 notifications
        final List<Message> messages = jmsMessageUtil.consumeMessages(JMS_BACKEND_REPLY_QUEUE_NAME, 2);
        assertNotNull(messages);

        //get the submit response notification and assert
        final Message submitResponseNotification = jmsPluginTestUtil.getMessageWithType(messages, MESSAGE_TYPE_SUBMIT_RESPONSE);
        assertEquals(messageId, submitResponseNotification.getStringProperty(JMSMessageConstants.MESSAGE_ID));
        assertNotNull(submitResponseNotification.getLongProperty(JMSMessageConstants.MESSAGE_ENTITY_ID));
        assertEquals("default", submitResponseNotification.getStringProperty("DOMAIN"));
        assertEquals(MESSAGE_TYPE_SUBMIT_RESPONSE, submitResponseNotification.getStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY));
        assertEquals("domibus.backend.jms.replyQueue", submitResponseNotification.getStringProperty("originalQueue"));
        assertNull(submitResponseNotification.getStringProperty("ErrorMessage"));

        //get the message sent notification and assert
        final Message sentMessage = jmsPluginTestUtil.getMessageWithType(messages, MESSAGE_TYPE_SEND_SUCCESS);
        assertEquals(messageId, sentMessage.getStringProperty(JMSMessageConstants.MESSAGE_ID));
        assertNotNull(sentMessage.getLongProperty(JMSMessageConstants.MESSAGE_ENTITY_ID));
        assertEquals("default", sentMessage.getStringProperty("DOMAIN"));
        assertEquals(MESSAGE_TYPE_SEND_SUCCESS, sentMessage.getStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY));
        assertEquals("domibus.backend.jms.replyQueue", sentMessage.getStringProperty("originalQueue"));
        assertNull(sentMessage.getStringProperty("ErrorMessage"));
    }

    @Test
    public void testSendMessageFailure() throws Exception {
        //we modify the retry policy so that the message fails immediately and not goes in WAITING_FOR_RETRY
        itTestsService.updatePmodeWithZeroRetries();

        String messageId = UUID.randomUUID().toString();

        //we save the JMS manager to restore it later
        Object savedJmsManagerField = itTestsService.prepareMocksForFailingSendingMessage(EbMS3ExceptionBuilder.getInstance()
                .ebMS3ErrorCode(ErrorCode.EbMS3ErrorCode.EBMS_0052)
                .message("Simulating error")
                .refToMessageId(messageId)
                .build());



        final MapMessage mapMessage = createJMSMessageForReceive();

        LOG.debug("Submitting MapMessage: " + mapMessage);

        mapMessage.setStringProperty(MESSAGE_ID, messageId); // Cleaning the message ID since it is supposed to submit a new message.
        mapMessage.setStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY, JMSMessageConstants.MESSAGE_TYPE_SUBMIT);

        jmsPlugin.receiveMessage(mapMessage);

        //put the real manager back
        itTestsService.setJmsManager(savedJmsManagerField);

        //we expect 1 notification
        final List<Message> messages = jmsMessageUtil.consumeMessages(JMS_BACKEND_REPLY_QUEUE_NAME, 1);
        LOG.info("Retrieved message [{}]", messages);
        assertNotNull(messages);
        assertEquals(1, messages.size());

        //get the submit response notification and assert
        final Message submitResponseNotification = jmsPluginTestUtil.getMessageWithType(messages, MESSAGE_TYPE_SUBMIT_RESPONSE);
        assertEquals(messageId, submitResponseNotification.getStringProperty(JMSMessageConstants.MESSAGE_ID));
        assertEquals("default", submitResponseNotification.getStringProperty("DOMAIN"));
        assertEquals(MESSAGE_TYPE_SUBMIT_RESPONSE, submitResponseNotification.getStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY));
        assertEquals(JMS_BACKEND_REPLY_QUEUE_NAME, submitResponseNotification.getStringProperty("originalQueue"));
        assertNull(submitResponseNotification.getStringProperty("ErrorMessage"));

        //we expect 1 notification
        final List<Message> errorMessages = jmsMessageUtil.consumeMessages(JMS_BACKEND_ERROR_NOTIFY_PRODUCER_QUEUE_NAME, 1);
        assertNotNull(errorMessages);

        //get the message send failure notification and assert
        final Message sendFailureMessage = jmsPluginTestUtil.getMessageWithType(errorMessages, MESSAGE_TYPE_SEND_FAILURE);
        assertEquals(messageId, sendFailureMessage.getStringProperty(JMSMessageConstants.MESSAGE_ID));
        assertEquals("default", sendFailureMessage.getStringProperty("DOMAIN"));
        assertEquals(MESSAGE_TYPE_SEND_FAILURE, sendFailureMessage.getStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY));
        assertEquals(JMS_BACKEND_ERROR_NOTIFY_PRODUCER_QUEUE_NAME, sendFailureMessage.getStringProperty("originalQueue"));
    }

    protected MapMessage createJMSMessageForReceive() throws Exception {
        Submission submission = submissionUtil.createSubmission();
        final MapMessage mapMessage = new ActiveMQMapMessage();
        return jmsMessageTransformer.transformFromSubmission(submission, mapMessage);
    }

    /*
     *
     * Similar test to the previous one but this does not change the Message ID so that an exception is raised and handled with an JMS error message.
     * It tests that the message is actually into the REPLY queue.
     *
     * @throws Exception
     */
    @Test
    public void testSendDuplicateMessage() throws Exception {
        final MapMessage mapMessage = createJMSMessageForReceive();

        final String messageId = mapMessage.getStringProperty(MESSAGE_ID);

        LOG.debug("Submitting MapMessage: " + mapMessage);
        mapMessage.setStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY, JMSMessageConstants.MESSAGE_TYPE_SUBMIT);

        jmsPlugin.receiveMessage(mapMessage);
        jmsMessageUtil.consumeMessage(JMS_BACKEND_REPLY_QUEUE_NAME);

        LOG.debug("Submitting MapMessage: " + mapMessage);
        jmsPlugin.receiveMessage(mapMessage);
        // Verifies that the message is really in the queue
        Message message = jmsMessageUtil.consumeMessage(JMS_BACKEND_REPLY_QUEUE_NAME);
        Assertions.assertEquals(messageId, message.getStringProperty(JMSMessageConstants.MESSAGE_ID));
        assertNotNull(message.getStringProperty("ErrorMessage"));
    }


}
