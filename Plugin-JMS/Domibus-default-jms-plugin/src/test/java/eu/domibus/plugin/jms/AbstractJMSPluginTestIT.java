package eu.domibus.plugin.jms;

import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.BackendConnector;
import eu.domibus.test.AbstractIT;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Cosmin Baciu
 * @since 4.0
 */
public abstract class AbstractJMSPluginTestIT extends AbstractIT {

    protected static final String JMS_NOT_QUEUE_NAME = "domibus.notification.jms";

    protected static final String JMS_BACKEND_IN_QUEUE_NAME = "domibus.backend.jms.inQueue";

    protected static final String JMS_BACKEND_OUT_QUEUE_NAME = "domibus.backend.jms.outQueue";

    protected static final String JMS_BACKEND_REPLY_QUEUE_NAME = "domibus.backend.jms.replyQueue";
    protected static final String JMS_BACKEND_ERROR_NOTIFY_PRODUCER_QUEUE_NAME = "domibus.backend.jms.errorNotifyProducer";

    @Autowired
    protected JMSPluginImpl jmsPlugin;


    @Override
    protected List<BackendConnector<?, ?>> getBackendConnectors() {
        return Arrays.asList(jmsPlugin);
    }

    @BeforeEach
    public void before() throws IOException, XmlProcessingException {
        Mockito.when(backendConnectorProvider.getBackendConnector(Mockito.any(String.class))).thenReturn((BackendConnector) jmsPlugin);
        //activate sync notifications
        itTestsService.clearConnectorFromAsyncNotification();
    }
}
