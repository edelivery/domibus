package eu.domibus.plugin.jms;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.List;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
@Service
public class JmsPluginTestUtil {

    public Message getMessageWithType(List<Message> messages, String type) throws JMSException {
        for (Message message : messages) {
            final String messageType = message.getStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY);
            if (StringUtils.equalsIgnoreCase(type, messageType)) {
                return message;
            }
        }
        return null;
    }
}
