
package eu.domibus.plugin.jms;


import eu.domibus.api.property.DomibusPropertyException;
import eu.domibus.common.DeliverMessageEvent;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.jms.property.JmsPluginPropertyManager;
import eu.domibus.test.PModeUtil;
import eu.domibus.test.common.JMSMessageUtil;
import eu.domibus.test.common.SoapSampleUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.Message;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.util.HashMap;

import static eu.domibus.plugin.jms.JMSMessageConstants.JMSPLUGIN_DOMAIN_ENABLED;
import static org.junit.jupiter.api.Assertions.*;

/**
 * This JUNIT implements the Test cases Download Message-03 and Download Message-04.
 * It uses the JMS backend connector.
 *
 * @author Cosmin Baciu
 * @author martifp
 */
public class DownloadMessageJMSIT extends AbstractJMSPluginTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DownloadMessageJMSIT.class);

    @Autowired
    PModeUtil pModeUtil;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    JMSMessageUtil jmsMessageUtil;

    @Autowired
    JmsPluginPropertyManager jmsPluginPropertyManager;

    @Autowired
    MSHWebservice mshWebserviceTest;

    @BeforeEach
    public void before() throws IOException, XmlProcessingException {
        super.before();
        pModeUtil.uploadPmode();
    }

    @AfterEach
    public void clean() {
        deleteAllMessages();
    }

    /**
     * Negative test: the message is not found in the JMS queue and a specific exception is returned.
     */
    @Test
    void testDownloadMessageInvalidId() throws RuntimeException {

        // Prepare the request to the backend
        String messageId = "invalid@e-delivery.eu";

        DeliverMessageEvent deliverMessageEvent = new DeliverMessageEvent(123, messageId, new HashMap<>());
        Assertions.assertThrows(RuntimeException.class, () -> jmsPlugin.deliverMessage(deliverMessageEvent));

    }

    /**
     * Tests that a message is found in the JMS queue and pushed to the business queue.
     */
    @Test
    public void testDownloadMessageOk() throws Exception {
        String filename = "SOAPMessage2.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        Message message = jmsMessageUtil.consumeMessage(JMS_BACKEND_OUT_QUEUE_NAME);
        assertNotNull(message);
        assertEquals("tc1", message.getStringProperty(JMSMessageConstants.SERVICE_TYPE));
        assertEquals("domibus-blue", message.getStringProperty(JMSMessageConstants.FROM_PARTY_ID));
        assertEquals("urn:oasis:names:tc:ebcore:partyid-type:unregistered", message.getStringProperty(JMSMessageConstants.FROM_PARTY_TYPE));
        assertEquals("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator", message.getStringProperty(JMSMessageConstants.FROM_ROLE));

        assertEquals("domibus-red", message.getStringProperty(JMSMessageConstants.TO_PARTY_ID));
        assertEquals("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder", message.getStringProperty(JMSMessageConstants.TO_ROLE));
        assertEquals("urn:oasis:names:tc:ebcore:partyid-type:unregistered", message.getStringProperty(JMSMessageConstants.TO_PARTY_TYPE));

        assertEquals("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1", message.getStringProperty(JMSMessageConstants.PROPERTY_ORIGINAL_SENDER));
        assertEquals("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4", message.getStringProperty(JMSMessageConstants.PROPERTY_FINAL_RECIPIENT));
        assertEquals(messageId, message.getStringProperty(JMSMessageConstants.MESSAGE_ID));
        assertNotNull(message.getLongProperty(JMSMessageConstants.MESSAGE_ENTITY_ID));
        assertEquals("incomingMessage", message.getStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY));
        assertEquals("bdx:noprocess", message.getStringProperty(JMSMessageConstants.SERVICE));
        assertEquals("TC1Leg1", message.getStringProperty(JMSMessageConstants.ACTION));
        assertEquals("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC", message.getStringProperty(JMSMessageConstants.MPC));
        assertEquals(1, message.getIntProperty("totalNumberOfPayloads"));
        assertEquals("text/xml", message.getStringProperty("payload_1_mimeType"));
        assertEquals("cid:message", message.getStringProperty("payload_1_mimeContentId"));
        assertEquals("default", message.getStringProperty("DOMAIN"));
        assertNull(message.getStringProperty(JMSMessageConstants.AGREEMENT_REF_TYPE));
        assertNull(message.getStringProperty(JMSMessageConstants.REF_TO_MESSAGE_ID));

    }

    @Test
    public void testDisableDomain() {
        Assertions.assertThrows(DomibusPropertyException.class, () -> jmsPluginPropertyManager.setKnownPropertyValue(JMSPLUGIN_DOMAIN_ENABLED, "false"));
    }
}
