package eu.domibus.plugin.jms;

import com.codahale.metrics.MetricRegistry;
import eu.domibus.common.*;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.domain.JmsMessageDTO;
import eu.domibus.ext.domain.metrics.Counter;
import eu.domibus.ext.domain.metrics.Timer;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomibusPropertyManagerExt;
import eu.domibus.ext.services.JMSExtService;
import eu.domibus.ext.services.TsidUtilExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.DomibusMessageCode;
import eu.domibus.logging.MDCKey;
import eu.domibus.messaging.DuplicateMessageException;
import eu.domibus.messaging.MessageConstants;
import eu.domibus.messaging.MessageNotFoundException;
import eu.domibus.messaging.MessagingProcessingException;
import eu.domibus.plugin.AbstractBackendConnector;
import eu.domibus.plugin.Submission;
import eu.domibus.plugin.handler.MessageSubmitResult;
import eu.domibus.plugin.jms.property.JmsPluginPropertyManager;
import eu.domibus.plugin.transformer.MessageRetrievalTransformer;
import eu.domibus.plugin.transformer.MessageSubmissionTransformer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jms.core.JmsOperations;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.destination.JndiDestinationResolver;

import javax.jms.*;
import java.text.MessageFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static eu.domibus.logging.DomibusMessageCode.DUPLICATE_MESSAGEID;
import static eu.domibus.plugin.jms.JMSMessageConstants.*;

/**
 * @author Christian Koch, Stefan Mueller
 * @author Cosmin Baciu
 */
public class JMSPluginImpl extends AbstractBackendConnector<MapMessage, MapMessage> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(JMSPluginImpl.class);
    private static final DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    public static final String PLUGIN_NAME = "Jms";

    protected final JMSExtService jmsExtService;
    protected final DomainContextExtService domainContextExtService;
    protected final JMSPluginQueueService jmsPluginQueueService;
    protected final JmsOperations mshToBackendTemplate;
    protected final JMSMessageTransformer jmsMessageTransformer;
    protected final MetricRegistry metricRegistry;
    protected final JndiDestinationResolver jndiDestinationResolver;
    protected final JmsPluginPropertyManager jmsPluginPropertyManager;
    protected final TsidUtilExtService tsidUtilExtService;

    public JMSPluginImpl(MetricRegistry metricRegistry,
                         JMSExtService jmsExtService,
                         DomainContextExtService domainContextExtService,
                         JMSPluginQueueService jmsPluginQueueService,
                         JmsOperations mshToBackendTemplate,
                         JMSMessageTransformer jmsMessageTransformer,
                         JndiDestinationResolver jndiDestinationResolver,
                         JmsPluginPropertyManager jmsPluginPropertyManager,
                         TsidUtilExtService tsidUtilExtService) {
        super(PLUGIN_NAME);
        this.jmsExtService = jmsExtService;
        this.domainContextExtService = domainContextExtService;
        this.jmsPluginQueueService = jmsPluginQueueService;
        this.mshToBackendTemplate = mshToBackendTemplate;
        this.jmsMessageTransformer = jmsMessageTransformer;
        this.metricRegistry = metricRegistry;
        this.jndiDestinationResolver = jndiDestinationResolver;
        this.jmsPluginPropertyManager = jmsPluginPropertyManager;
        this.tsidUtilExtService = tsidUtilExtService;
    }

    @Override
    public boolean shouldCoreManageResources() {
        return true;
    }

    @Override
    public MessageSubmissionTransformer<MapMessage> getMessageSubmissionTransformer() {
        return this.jmsMessageTransformer;
    }

    @Override
    public MessageRetrievalTransformer<MapMessage> getMessageRetrievalTransformer() {
        return this.jmsMessageTransformer;
    }

    /**
     * This method is called when a message was received at the incoming queue
     *
     * @param map The incoming JMS Message
     */
    @MDCKey(value = {DomibusLogger.MDC_MESSAGE_ID, DomibusLogger.MDC_MESSAGE_ROLE, DomibusLogger.MDC_MESSAGE_ENTITY_ID, DomibusLogger.MDC_CONVERSATION_ID}, cleanOnStart = true)
    @Timer(clazz = JMSPluginImpl.class, value = "receiveMessage")
    @Counter(clazz = JMSPluginImpl.class, value = "receiveMessage")
    public void receiveMessage(final MapMessage map) {
        try {
            checkEnabled();

            String messageID = map.getStringProperty(MESSAGE_ID);
            if (StringUtils.isNotBlank(messageID)) {
                //trim the empty space
                messageID = messageExtService.cleanMessageIdentifier(messageID);
                LOG.putMDC(DomibusLogger.MDC_MESSAGE_ID, messageID);
            }
            final String conversationId = map.getStringProperty(CONVERSATION_ID);
            final String jmsCorrelationID = map.getJMSCorrelationID();
            LOG.putMDC(JMS_CORRELATION_ID, conversationId);
            final String messageType = map.getStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY);
            LOG.putMDC(CONVERSATION_ID, conversationId);
            LOG.businessInfo(DomibusMessageCode.BUS_MSG_RECEIVED_FROM_JMS_IN_QUEUE, messageID, conversationId, jmsCorrelationID);

            QueueContext queueContext = jmsMessageTransformer.getQueueContext(messageID, null, map);
            LOG.debug("Extracted queue context [{}]", queueContext);

            if (!MESSAGE_TYPE_SUBMIT.equals(messageType)) {
                String wrongMessageTypeMessage = getWrongMessageTypeErrorMessage(messageID, jmsCorrelationID, messageType);
                LOG.error(wrongMessageTypeMessage);
                sendReplyMessage(queueContext, wrongMessageTypeMessage, jmsCorrelationID);
                return;
            }

            String errorMessage = null;
            MessageSubmitResult messageSubmitResult = null;
            try {
                //in case the messageID is not sent by the user it will be generated
                messageSubmitResult = submitMessage(map);
                messageID = messageSubmitResult.getAs4MessageId();
                queueContext.setMessageEntityId(messageSubmitResult.getMessageEntityId());
            } catch (final MessagingProcessingException e) {
                if (e instanceof DuplicateMessageException){
                    LOG.businessError(DUPLICATE_MESSAGEID, messageID);
                }
                LOG.error("Exception occurred receiving message [{}}], jmsCorrelationID [{}}]", messageID, jmsCorrelationID, e);
                errorMessage = e.getMessage() + ": Error Code: " + (e.getEbms3ErrorCode() != null ? e.getEbms3ErrorCode().getErrorCodeName() : " not set");
            }

            queueContext.setMessageId(messageID);
            sendReplyMessage(queueContext, errorMessage, jmsCorrelationID);

            LOG.info("Submitted message with messageId [{}], jmsCorrelationID [{}]", messageID, jmsCorrelationID);
        } catch (Exception e) {
            throw new DefaultJmsPluginException("Exception occurred while receiving message [" + map + "]", e);
        }
    }

    protected String getWrongMessageTypeErrorMessage(String messageID, String jmsCorrelationID, String messageType) {
        return MessageFormat.format("Illegal messageType [{0}] on message with JMSCorrelationId [{1}] and messageId [{2}]. Only [{3}] messages are accepted on this queue",
                messageType, jmsCorrelationID, messageID, MESSAGE_TYPE_SUBMIT);
    }

    protected void sendReplyMessage(QueueContext queueContext, final String errorMessage, final String correlationId) {
        String messageId = queueContext.getMessageId();
        Long messageEntityId = queueContext.getMessageEntityId();;
        LOG.info("Sending reply message with message id [{}], message entity id [{}], error message [{}] and correlation id [{}]", messageId, messageEntityId, errorMessage, correlationId);

        final JmsMessageDTO jmsMessageDTO = new ReplyMessageCreator(messageId, messageEntityId, errorMessage, correlationId).createMessage();
        sendJmsMessage(jmsMessageDTO, queueContext, JMSPLUGIN_QUEUE_REPLY, JMSPLUGIN_QUEUE_REPLY_ROUTING);
    }

    @Override
    @Timer(clazz = JMSPluginImpl.class, value = "deliverMessage")
    @Counter(clazz = JMSPluginImpl.class, value = "deliverMessage")
    @MDCKey({DomibusLogger.MDC_CONVERSATION_ID})
    public void deliverMessage(final DeliverMessageEvent event) {
        checkEnabled();

        final String messageId = event.getMessageId();
        final Long messageEntityId = event.getMessageEntityId();
        final String conversationId = event.getProps().get(MessageConstants.CONVERSATION_ID);
        LOG.putMDC(DomibusLogger.MDC_CONVERSATION_ID, conversationId);
        final String from = event.getProps().get(MessageConstants.FROM_PARTY_ID);
        LOG.putMDC(DomibusLogger.MDC_FROM, from);
        final String to = event.getProps().get(MessageConstants.TO_PARTY_ID);
        LOG.putMDC(DomibusLogger.MDC_TO, to);
        ZonedDateTime creationDateTime = tsidUtilExtService.getZonedDateTimeFromTsid(messageEntityId);

        LOG.businessInfo(DomibusMessageCode.BUS_MSG_DELIVERED_TO_JMS_OUT_QUEUE, messageId, messageEntityId,creationDateTime.format(DEFAULT_FORMATTER), conversationId);
        LOG.debug("Delivering message with id [{}], message entity id [{}] for final recipient [{}]", messageId, messageEntityId, event.getProps().get(MessageConstants.FINAL_RECIPIENT));

        QueueContext queueContext = createQueueContext(event);
        final String queueValue = jmsPluginQueueService.getJMSQueue(queueContext, JMSPLUGIN_QUEUE_OUT, JMSPLUGIN_QUEUE_OUT_ROUTING);
        LOG.info("Sending message to queue [{}]", queueValue);
        mshToBackendTemplate.send(queueValue, new DownloadMessageCreator(messageId, messageEntityId, queueValue));
    }

    @Override
    public void messageReceiveFailed(MessageReceiveFailureEvent messageReceiveFailureEvent) {
        checkEnabled();

        LOG.debug("Handling messageReceiveFailed");
        final JmsMessageDTO jmsMessageDTO = new ErrorMessageCreator(
                messageReceiveFailureEvent.getMessageEntityId(),
                messageReceiveFailureEvent.getMessageId(),
                messageReceiveFailureEvent.getErrorResult(),
                messageReceiveFailureEvent.getEndpoint(),
                NotificationType.MESSAGE_RECEIVED_FAILURE).createMessage();

        QueueContext queueContext = createQueueContext(messageReceiveFailureEvent);
        sendJmsMessage(jmsMessageDTO, queueContext, JMSPLUGIN_QUEUE_CONSUMER_NOTIFICATION_ERROR, JMSPLUGIN_QUEUE_CONSUMER_NOTIFICATION_ERROR_ROUTING);
    }

    @Override
    public void messageSendFailed(final MessageSendFailedEvent event) {
        checkEnabled();

        final ErrorResult errorResult = getErrorResult(event.getMessageId(), MSHRole.SENDING);
        final JmsMessageDTO jmsMessageDTO = new ErrorMessageCreator(
                event.getMessageEntityId(),
                event.getMessageId(),
                errorResult,
                null,
                NotificationType.MESSAGE_SEND_FAILURE).createMessage();

        QueueContext queueContext = createQueueContext(event);
        sendJmsMessage(jmsMessageDTO, queueContext, JMSPLUGIN_QUEUE_PRODUCER_NOTIFICATION_ERROR, JMSPLUGIN_QUEUE_PRODUCER_NOTIFICATION_ERROR_ROUTING);
    }

    protected ErrorResult getErrorResult(String messageId, MSHRole mshRole) {
        try {
            List<ErrorResult> errors = super.getErrorsForMessage(messageId, mshRole);
            if (CollectionUtils.isEmpty(errors)) {
                return null;
            }
            return errors.get(errors.size() - 1);
        } catch (final MessageNotFoundException e) {
            LOG.error("Exception occurred while getting errors for message [{}], mshRole [{}]", messageId, mshRole, e);
            throw new DefaultJmsPluginException("Exception occurred while getting errors for message [" + messageId + "]", e);
        }
    }

    private QueueContext createQueueContext(MessageEvent event) {
        final String service = event.getProps().get(MessageConstants.SERVICE);
        final String action = event.getProps().get(MessageConstants.ACTION);
        final String jmsCorrelationId = LOG.getMDC(JMSMessageConstants.JMS_CORRELATION_ID);
        final String messageId = event.getMessageId();
        final Long messageEntityId = event.getMessageEntityId();
        QueueContext queueContext = new QueueContext(messageId, messageEntityId, service, action, jmsCorrelationId);
        return queueContext;
    }

    @Timer(clazz = JMSPluginImpl.class, value = "messageSendSuccess")
    @Counter(clazz = JMSPluginImpl.class, value = "messageSendSuccess")
    @Override
    public void messageSendSuccess(MessageSendSuccessEvent event) {
        checkEnabled();

        LOG.debug("Handling messageSendSuccess");
        final JmsMessageDTO jmsMessageDTO = new SignalMessageCreator(event.getMessageEntityId(), event.getMessageId(), NotificationType.MESSAGE_SEND_SUCCESS).createMessage();

        QueueContext queueContext = createQueueContext(event);
        jmsMessageDTO.setJmsCorrelationId(queueContext.getJmsCorrelationId());
        sendJmsMessage(jmsMessageDTO, queueContext, JMSPLUGIN_QUEUE_REPLY, JMSPLUGIN_QUEUE_REPLY_ROUTING);
    }

    @Override
    public void messageDeletedBatchEvent(final MessageDeletedBatchEvent event) {
        LOG.info("Message delete batch event");
    }

    @Override
    public void messageDeletedEvent(final MessageDeletedEvent event) {
        LOG.info("Message delete event [{}]", event.getMessageId());
    }

    protected void sendJmsMessage(JmsMessageDTO message, QueueContext queueContext, String defaultQueueProperty, String routingQueuePrefixProperty) {
        final String queueValue = jmsPluginQueueService.getJMSQueue(queueContext, defaultQueueProperty, routingQueuePrefixProperty);

        LOG.info("Sending message with message id [{}] and message entity id [{}] to queue [{}]", queueContext.getMessageId(), queueContext.getMessageEntityId(), queueValue);
        jmsExtService.sendMapMessageToQueue(message, queueValue, mshToBackendTemplate);
    }

    @Override
    public MapMessage downloadMessage(final Long messageEntityId, MapMessage target) throws MessageNotFoundException {
        checkEnabled();

        LOG.debug("Downloading message with entity id [{}]", messageEntityId);
        try {
            Submission submission = messageRetriever.downloadMessage(messageEntityId);
            MapMessage result = getMessageRetrievalTransformer().transformFromSubmission(submission, target);

            LOG.businessInfo(DomibusMessageCode.BUS_MESSAGE_RETRIEVED);
            return result;
        } catch (Exception ex) {
            LOG.businessError(DomibusMessageCode.BUS_MESSAGE_RETRIEVE_FAILED, ex);
            throw ex;
        }
    }

    private class DownloadMessageCreator implements MessageCreator {
        private String destination;

        private String messageId;
        private long messageEntityId;

        public DownloadMessageCreator(String messageId, final long messageEntityId, String destination) {
            this.messageId = messageId;
            this.messageEntityId = messageEntityId;
            this.destination = destination;
        }

        @Override
        @MDCKey(value = {DomibusLogger.MDC_MESSAGE_ID, DomibusLogger.MDC_MESSAGE_ROLE, DomibusLogger.MDC_MESSAGE_ENTITY_ID}, cleanOnStart = true)
        public MapMessage createMessage(final Session session) throws JMSException {
            final MapMessage mapMessage = session.createMapMessage();
            try {
                downloadMessage(messageEntityId, mapMessage);
            } catch (final MessageNotFoundException e) {
                throw new DefaultJmsPluginException("Unable to create push message", e);
            }
            mapMessage.setStringProperty(JMSMessageConstants.JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY, JMSMessageConstants.MESSAGE_TYPE_INCOMING);
            final DomainDTO currentDomain = domainContextExtService.getCurrentDomain();
            mapMessage.setStringProperty(MessageConstants.DOMAIN, currentDomain.getCode());

            String queueName = destination;
            if (jndiDestinationResolver != null) {
                Destination jmsDestination = jndiDestinationResolver.resolveDestinationName(session, destination, false);
                LOG.debug("Jms destination [{}] resolved to: [{}]", destination, jmsDestination);
                if (jmsDestination instanceof Queue) {
                    queueName = ((Queue) jmsDestination).getQueueName();
                }
            }
            mapMessage.setStringProperty(MESSAGE_ID, messageId);
            mapMessage.setLongProperty(MESSAGE_ENTITY_ID, messageEntityId);
            mapMessage.setStringProperty(JMSMessageConstants.PROPERTY_ORIGINAL_QUEUE, queueName);

            return mapMessage;
        }
    }

    @Override
    public boolean isEnabled(final String domainCode) {
        return doIsEnabled(domainCode);
    }

    @Override
    public void setEnabled(final String domainCode, final boolean enabled) {
        doSetEnabled(domainCode, enabled);
    }

    @Override
    public String getDomainEnabledPropertyName() {
        return JMSPLUGIN_DOMAIN_ENABLED;
    }

    @Override
    public String getDomainsDisabledPropertyName() {
        return JMSPLUGIN_DOMAINS_DISABLED;
    }

    @Override
    public DomibusPropertyManagerExt getPropertyManager() {
        return jmsPluginPropertyManager;
    }
}
