package eu.domibus.tomcat.jpa;

import com.zaxxer.hikari.HikariDataSource;
import eu.domibus.api.datasource.DataSourceConstants;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.core.jpa.BaseDatasourceConfiguration;
import eu.domibus.core.property.PrimitivePropertyTypesManager;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;
import static org.apache.commons.lang3.time.DateUtils.MILLIS_PER_SECOND;

/**
 * @author Cosmin Baciu
 * @since 4.0
 */
@Configuration
public class TomcatDatasourceConfiguration extends BaseDatasourceConfiguration {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TomcatDatasourceConfiguration.class);

    private HikariDataSource defaultDataSource;

    public TomcatDatasourceConfiguration(DomibusPropertyProvider domibusPropertyProvider,
                                         PrimitivePropertyTypesManager primitivePropertyTypesManager) {
        super(domibusPropertyProvider, primitivePropertyTypesManager);
    }

    @Override
    protected DataSource getReadWriteDataSource() {
        return getMainDataSource();
    }

    @Override
    protected DataSource getReadOnlyDataSource() {
        return getReplicaDataSource();
    }

    @Bean(name = DataSourceConstants.DOMIBUS_JDBC_QUARTZ_DATA_SOURCE, destroyMethod = "close")
    public DataSource quartzDatasource() {
        return getQuartzDataSource();
    }

    private HikariDataSource getMainDataSource() {
        return getHikariDataSource(DOMIBUS_DATASOURCE_DRIVER_CLASS_NAME, DOMIBUS_DATASOURCE_URL, DOMIBUS_DATASOURCE_USER,
                DOMIBUS_DATASOURCE_PASSWORD, DOMIBUS_DATASOURCE_MAX_LIFETIME, DOMIBUS_DATASOURCE_MAX_POOL_SIZE, DOMIBUS_DATASOURCE_CONNECTION_TIMEOUT,
                DOMIBUS_DATASOURCE_IDLE_TIMEOUT, DOMIBUS_DATASOURCE_MINIMUM_IDLE, DOMIBUS_DATASOURCE_POOL_NAME);
    }

    private HikariDataSource getQuartzDataSource() {
        return getHikariDataSource(DOMIBUS_QUARTZ_DATASOURCE_DRIVER_CLASS_NAME, DOMIBUS_QUARTZ_DATASOURCE_URL,
                DOMIBUS_QUARTZ_DATASOURCE_USER, DOMIBUS_QUARTZ_DATASOURCE_PASSWORD, DOMIBUS_QUARTZ_DATASOURCE_MAX_LIFETIME,
                DOMIBUS_QUARTZ_DATASOURCE_MAX_POOL_SIZE, DOMIBUS_QUARTZ_DATASOURCE_CONNECTION_TIMEOUT, DOMIBUS_QUARTZ_DATASOURCE_IDLE_TIMEOUT,
                DOMIBUS_QUARTZ_DATASOURCE_MINIMUM_IDLE, DOMIBUS_QUARTZ_DATASOURCE_POOL_NAME);
    }

    private HikariDataSource getHikariDataSource(String datasourceDriverClassName, String datasourceUrl, String datasourceUser, String datasourcePassword,
                                                 String datasourceMaxLifetime, String datasourceMaxPoolSize, String datasourceConnectionTimeout,
                                                 String datasourceIdleTimeout, String datasourceMinimumIdle, String datasourcePoolName) {
        HikariDataSource dataSource = new HikariDataSource();
        final String driverClassName = domibusPropertyProvider.getProperty(datasourceDriverClassName);
        dataSource.setDriverClassName(driverClassName);

        final String dataSourceURL = domibusPropertyProvider.getProperty(datasourceUrl);
        dataSource.setJdbcUrl(dataSourceURL);

        final String user = domibusPropertyProvider.getProperty(datasourceUser);
        dataSource.setUsername(user);

        final String password = domibusPropertyProvider.getProperty(datasourcePassword); //NOSONAR

        dataSource.setPassword(password);

        final Integer maxLifetimeInSecs = domibusPropertyProvider.getIntegerProperty(datasourceMaxLifetime);
        dataSource.setMaxLifetime(maxLifetimeInSecs * MILLIS_PER_SECOND);

        final Integer maxPoolSize = domibusPropertyProvider.getIntegerProperty(datasourceMaxPoolSize);
        dataSource.setMaximumPoolSize(maxPoolSize);

        final Integer connectionTimeout = domibusPropertyProvider.getIntegerProperty(datasourceConnectionTimeout);
        dataSource.setConnectionTimeout(connectionTimeout * MILLIS_PER_SECOND);

        final Integer idleTimeout = domibusPropertyProvider.getIntegerProperty(datasourceIdleTimeout);
        dataSource.setIdleTimeout(idleTimeout * MILLIS_PER_SECOND);

        final Integer minimumIdle = domibusPropertyProvider.getIntegerProperty(datasourceMinimumIdle);
        dataSource.setMinimumIdle(minimumIdle);

        final String poolName = domibusPropertyProvider.getProperty(datasourcePoolName);
        if (!StringUtils.isBlank(poolName)) {
            dataSource.setPoolName(poolName);
        }

        return dataSource;
    }

    private HikariDataSource getReplicaDataSource() {
        HikariDataSource dataSource = new HikariDataSource();

        final String dataSourceURL = domibusPropertyProvider.getProperty(DOMIBUS_DATASOURCE_REPLICA_URL);
        dataSource.setJdbcUrl(dataSourceURL);

        String driverClassName = getStringPropertyValueWithfallback(DOMIBUS_DATASOURCE_REPLICA_DRIVER_CLASS_NAME, DOMIBUS_DATASOURCE_DRIVER_CLASS_NAME);
        dataSource.setDriverClassName(driverClassName);

        String user = getStringPropertyValueWithfallback(DOMIBUS_DATASOURCE_REPLICA_USER, DOMIBUS_DATASOURCE_USER);
        dataSource.setUsername(user);

        String password = getStringPropertyValueWithfallback(DOMIBUS_DATASOURCE_REPLICA_PASSWORD, DOMIBUS_DATASOURCE_PASSWORD); //NOSONAR
        dataSource.setPassword(password);

        Integer maxLifetimeInSecs = getIntegerPropertyValueWithFallback(DOMIBUS_DATASOURCE_REPLICA_MAX_LIFETIME, DOMIBUS_DATASOURCE_MAX_LIFETIME);
        dataSource.setMaxLifetime(maxLifetimeInSecs * MILLIS_PER_SECOND);

        Integer maxPoolSize = getIntegerPropertyValueWithFallback(DOMIBUS_DATASOURCE_REPLICA_MAX_POOL_SIZE, DOMIBUS_DATASOURCE_MAX_POOL_SIZE);
        dataSource.setMaximumPoolSize(maxPoolSize);

        Integer connectionTimeout = getIntegerPropertyValueWithFallback(DOMIBUS_DATASOURCE_REPLICA_CONNECTION_TIMEOUT, DOMIBUS_DATASOURCE_CONNECTION_TIMEOUT);
        dataSource.setConnectionTimeout(connectionTimeout * MILLIS_PER_SECOND);

        Integer idleTimeout = getIntegerPropertyValueWithFallback(DOMIBUS_DATASOURCE_REPLICA_IDLE_TIMEOUT, DOMIBUS_DATASOURCE_IDLE_TIMEOUT);
        dataSource.setIdleTimeout(idleTimeout * MILLIS_PER_SECOND);

        Integer minimumIdle = getIntegerPropertyValueWithFallback(DOMIBUS_DATASOURCE_REPLICA_MINIMUM_IDLE, DOMIBUS_DATASOURCE_MINIMUM_IDLE);
        dataSource.setMinimumIdle(minimumIdle);

        String poolName = getStringPropertyValueWithfallback(DOMIBUS_DATASOURCE_REPLICA_POOL_NAME, DOMIBUS_DATASOURCE_POOL_NAME);
        if (!StringUtils.isBlank(poolName)) {
            dataSource.setPoolName(poolName);
        }

        return dataSource;
    }

    private Integer getIntegerPropertyValueWithFallback(String replicaPropertyName, String mainPropertyName) {
        String propertyValueAsString = getStringPropertyValueWithfallback(replicaPropertyName, mainPropertyName);
        return primitivePropertyTypesManager.getIntegerValue(mainPropertyName, propertyValueAsString);
    }

    private String getStringPropertyValueWithfallback(String replicaPropertyName, String mainPropertyName) {
        String propertyValue = domibusPropertyProvider.getProperty(replicaPropertyName);
        if (StringUtils.isNotBlank(propertyValue)) {
            LOG.debug("Returning replica datasource property value [{}] for property [{}]",propertyValue, replicaPropertyName);
            return propertyValue;
        }
        // fallback to default datasource property
        propertyValue = domibusPropertyProvider.getProperty(mainPropertyName);
        LOG.info("Falling back to default datasource property value [{}] for property [{}]", propertyValue, mainPropertyName);
        return propertyValue;
    }

}
