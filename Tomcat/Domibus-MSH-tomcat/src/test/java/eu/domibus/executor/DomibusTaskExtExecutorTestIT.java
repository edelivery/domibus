package eu.domibus.executor;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.common.AuthRole;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.exceptions.DomainTaskExtException;
import eu.domibus.ext.services.DomibusTaskExtExecutor;
import eu.domibus.ext.services.ExecutorExceptionHandlerExt;
import eu.domibus.ext.services.ExecutorWaitPolicyExt;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class DomibusTaskExtExecutorTestIT extends AbstractTomcatTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusTaskExtExecutorTestIT.class);

    @Autowired
    DomibusTaskExtExecutor domibusTaskExtExecutor;

    protected Long errorHandlerValue;

    @Test
    public void submitCallable() {
        final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                "myuser",
                "mypass",
                Collections.singleton(new SimpleGrantedAuthority(AuthRole.ROLE_ADMIN.name())));

        Callable callable = () -> {
            LOG.debug("Executing callable");
            final Authentication currentAuthentication = SecurityContextHolder.getContext().getAuthentication();
            assertEquals(currentAuthentication, authentication);
            return Boolean.TRUE;
        };

        final Object result = domibusTaskExtExecutor.submitCallable(callable,
                new DomainDTO("default", "default"),
                authentication,
                ExecutorWaitPolicyExt.WAIT,
                10L,
                TimeUnit.SECONDS,
                null);
        assertNotNull(result);
        assertTrue(result instanceof Boolean);
        assertTrue(((Boolean) result));
    }

    @Test
    public void submitCallableWithExceededTimeout() {
        Callable callable = () -> {
            LOG.debug("Executing callable");
            Thread.sleep(200L);
            return Boolean.TRUE;
        };

        Assertions.assertThrows(DomainTaskExtException.class, () -> {
            final Object result = domibusTaskExtExecutor.submitCallable(callable,
                    new DomainDTO("default", "default"),
                    null,
                    ExecutorWaitPolicyExt.WAIT,
                    100L,
                    TimeUnit.MILLISECONDS,
                    null);
        });
    }

    @Test
    public void submitCallableWithExceptionThrown() {
        Callable callable = () -> {
            LOG.debug("Executing callable");
            Thread.sleep(200L);
            return Boolean.TRUE;
        };
        Long newErrorHandlerValue = 10L;
        ExecutorExceptionHandlerExt executorExceptionHandlerExt = exception -> {
            errorHandlerValue = newErrorHandlerValue;
        };

        final Object result = domibusTaskExtExecutor.submitCallable(callable,
                new DomainDTO("default", "default"),
                null,
                ExecutorWaitPolicyExt.WAIT,
                100L,
                TimeUnit.MILLISECONDS,
                executorExceptionHandlerExt);

        assertEquals(newErrorHandlerValue, errorHandlerValue);
    }
}
