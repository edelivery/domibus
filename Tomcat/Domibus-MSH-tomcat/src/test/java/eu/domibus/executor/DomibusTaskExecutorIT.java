package eu.domibus.executor;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.property.DomibusPropertyException;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ion Perpegel
 * @since 5.2
 */
@Transactional
public class DomibusTaskExecutorIT extends AbstractTomcatTestIT {

    private final static DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusTaskExecutorIT.class);

    @Test
    public void testLongRunningTaskException() throws InterruptedException {
        final Map<String, Boolean> outcome = new HashMap<>();
        outcome.put("outcome", true);

        final Domain currentDomain = domainContextProvider.getCurrentDomain();
        domainTaskExecutor.submitLongRunningTask(
                () -> {
                    throw new DomibusPropertyException("Business error");
                },
                currentDomain,
                null,
                (exception) -> {
                    outcome.put("outcome", false);
                });
        // need this sleep so that the exception handler can finish executing on the other thread and set the outcome
        Thread.sleep(500);
        Assert.assertFalse(outcome.get("outcome"));
    }

}
