package eu.domibus;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.web.security.AuthenticationService;
import eu.domibus.web.security.DomibusUserDetailsImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;

import static eu.domibus.test.AbstractIT.TEST_ADMIN_PASSWORD;
import static eu.domibus.test.AbstractIT.TEST_ADMIN_USERNAME;


public class MockAuthenticationService implements AuthenticationService {
    @Override
    public DomibusUserDetailsImpl authenticate(String username, String password, String domain) {
        SecurityContextHolder.getContext()
                .setAuthentication(new UsernamePasswordAuthenticationToken(
                        TEST_ADMIN_USERNAME,
                        TEST_ADMIN_PASSWORD,
                        Collections.singleton(new SimpleGrantedAuthority(eu.domibus.api.security.AuthRole.ROLE_ADMIN.name()))));

        DomibusUserDetailsImpl userDetails = getLoggedUser();
        return userDetails;
    }

    @Override
    public void changeDomain(String domainCode) {
    }

    @Override
    public void changePassword(String currentPassword, String newPassword) {
    }

    @Override
    public DomibusUserDetailsImpl getLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
            if (authentication.getPrincipal() instanceof String) {
                String userName = (String) authentication.getPrincipal();
                DomibusUserDetailsImpl domibusUserDetails = new DomibusUserDetailsImpl(userName, StringUtils.EMPTY, authentication.getAuthorities());
                return domibusUserDetails;
            }
            if (authentication.getPrincipal() instanceof DomibusUserDetailsImpl) {
                return (DomibusUserDetailsImpl) authentication.getPrincipal();
            }
        }
        return null;
    }

    @Override
    public void onDomainAdded(Domain domain) {

    }

    @Override
    public void onDomainRemoved(Domain domain) {

    }
}
