package eu.domibus.message;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.MessageStatus;
import eu.domibus.api.model.NotificationStatus;
import eu.domibus.common.JPAConstants;
import eu.domibus.core.message.MessageLogInfo;
import eu.domibus.core.message.MessageLogInfoFilter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

/**
 * @author Soumya Chandran
 * @since 5.2
 */
public class MessageLogInfoFilterIT extends AbstractTomcatTestIT {
    @Autowired
    MessageLogInfoFilter userMessageLogInfoFilter;

    @PersistenceContext(unitName = JPAConstants.PERSISTENCE_UNIT_NAME)
    protected EntityManager em;

    @Test
    public void getHQLKey() {
        String fieldName = userMessageLogInfoFilter.getHQLKey("messageStatus");
        Assertions.assertEquals("log.messageStatus", fieldName);
    }


    @Test
    public void filterQuery() {
        StringBuilder filterQuery = userMessageLogInfoFilter.filterQuery("select * from table where z = 1", "messageStatus", false, returnFilters());

        String filterQueryString = filterQuery.toString();
        Assertions.assertTrue(filterQueryString.contains("log.notificationStatus = :notificationStatus"));
        Assertions.assertTrue(filterQueryString.contains("message.messageId = :messageId"));
    }

    @Test
    public void testApplyParameters() {
        String result = userMessageLogInfoFilter.getFilterMessageLogQuery("column", true, returnFilters(), Collections.emptyList());
        TypedQuery<MessageLogInfo> typedQuery = em.createQuery(result, MessageLogInfo.class);
        TypedQuery<MessageLogInfo> messageLogInfoTypedQuery = userMessageLogInfoFilter.applyParameters(typedQuery, returnFilters());
        Assertions.assertNotNull(messageLogInfoTypedQuery);
    }

    public static HashMap<String, Object> returnFilters() {
        HashMap<String, Object> filters = new HashMap<>();

        filters.put("conversationId", "CONVERSATIONID");
        filters.put("messageId", "MESSAGEID");
        filters.put("mshRole", MSHRole.SENDING);
        filters.put("messageStatus", MessageStatus.ACKNOWLEDGED);
        filters.put("notificationStatus", NotificationStatus.NOTIFIED);
        filters.put("deleted", new Date());
        filters.put("received", new Date());
        filters.put("sendAttempts", 1);
        filters.put("sendAttemptsMax", 5);
        filters.put("nextAttempt", new Date());
        filters.put("fromPartyId", "FROMPARTYID");
        filters.put("toPartyId", "TOPARTYID");
        filters.put("refToMessageId", "REFTOMESSAGEID");
        filters.put("originalSender", "ORIGINALSENDER");
        filters.put("finalRecipient", "FINALRECIPIENT");
        filters.put("receivedFrom", new Date());
        filters.put("receivedTo", new Date());

        return filters;
    }
}
