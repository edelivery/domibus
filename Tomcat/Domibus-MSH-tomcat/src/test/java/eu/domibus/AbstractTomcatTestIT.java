package eu.domibus;

import eu.domibus.plugin.BackendConnector;
import eu.domibus.test.AbstractIT;
import eu.domibus.test.common.BackendConnectorMock;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

public class AbstractTomcatTestIT extends AbstractIT {

    @Autowired
    protected BackendConnectorMock backendConnectorMock;
    
    @Override
    protected List<BackendConnector<?, ?>> getBackendConnectors() {
        return Arrays.asList(backendConnectorMock);
    }
}
