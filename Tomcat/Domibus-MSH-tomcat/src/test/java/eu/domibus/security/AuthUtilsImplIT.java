package eu.domibus.security;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.security.AuthRole;
import eu.domibus.api.security.AuthUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;

/**
 * This class tests the PreAuthorize annotation.
 * In order to trigger the @PreAuthorize, we use Spring to create the bean with
 * '@EnableGlobalMethodSecurity(prePostEnabled = true)'
 * <p>
 * '@WithMockUser' is then use to set up the security context with a mock user easily
 *
 * @author François Gautier
 * @since 4.2
 */

public class AuthUtilsImplIT extends AbstractTomcatTestIT {

    @Autowired
    private AuthUtils authUtils;

    @Test
    void hasAdminRole_noUser() {
        authUtils.clearSecurityContext();

        Assertions.assertThrows(AuthenticationCredentialsNotFoundException.class, () -> authUtils.checkHasAdminRoleOrUserRoleWithOriginalUser());
    }

    @Test
    void hasAdminRole_user() {
        SecurityContextHolder.getContext()
                .setAuthentication(new UsernamePasswordAuthenticationToken(
                        TEST_ADMIN_USERNAME,
                        TEST_ADMIN_PASSWORD,
                        Collections.singleton(new SimpleGrantedAuthority("ECAS"))));

        Assertions.assertThrows(AccessDeniedException.class, () -> authUtils.checkHasAdminRoleOrUserRoleWithOriginalUser());
    }

    @Test
    public void hasAdminRole_apAdmin() {
        authUtils.setAuthenticationToSecurityContext(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD, AuthRole.ROLE_AP_ADMIN);

        authUtils.checkHasAdminRoleOrUserRoleWithOriginalUser();
    }

    @Test
    public void hasAdminRole_admin() {
        authUtils.setAuthenticationToSecurityContext(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD, AuthRole.ROLE_ADMIN);

        authUtils.checkHasAdminRoleOrUserRoleWithOriginalUser();
    }

    @Test
    void hasUserRole_noUser() {
        authUtils.clearSecurityContext();

        Assertions.assertThrows(AuthenticationCredentialsNotFoundException.class, () -> authUtils.checkHasAdminRoleOrUserRoleWithOriginalUser());
    }

}
