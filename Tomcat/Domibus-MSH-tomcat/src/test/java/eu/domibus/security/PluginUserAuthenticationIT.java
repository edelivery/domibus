package eu.domibus.security;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.DomibusPropertyMetadata;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.AuthRole;
import eu.domibus.api.security.AuthenticationException;
import eu.domibus.api.security.BasicAuthentication;
import eu.domibus.core.message.UserMessageSecurityDefaultService;
import eu.domibus.core.property.GlobalPropertyMetadataManager;
import eu.domibus.core.security.AuthUtilsImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.test.common.UserMessageSampleUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_AUTH_UNSECURE_LOGIN_ALLOWED;

/**
 * @author Soumya Chandran
 * @since 5.2
 */
@Transactional
public class PluginUserAuthenticationIT extends AbstractTomcatTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PluginUserAuthenticationIT.class);


    @Autowired
    UserMessageSecurityDefaultService userMessageSecurityDefaultService;

    @Autowired
    private AuthUtilsImpl authUtilsImpl;

    @Autowired
    protected DomibusPropertyProvider domibusPropertyProvider;


    @Autowired
    protected DomibusConfigurationService domibusConfigurationService;
    String originalValue;
    Domain currentDomain;

    @BeforeEach
    public void initInstance() {
        LOG.putMDC(DomibusLogger.MDC_DOMAIN, "default");
        currentDomain = domainContextProvider.getCurrentDomain();
        originalValue = domibusPropertyProvider.getProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_AUTH_UNSECURE_LOGIN_ALLOWED);
        setUnsecureLoginAllowedProperty("false");
    }

    protected void setUnsecureLoginAllowedProperty(String value) {
        domainContextProvider.clearCurrentDomain();
        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_AUTH_UNSECURE_LOGIN_ALLOWED, value);
        domainContextProvider.setCurrentDomain(currentDomain);
    }

    @AfterEach
    public void tearDown() {
        setUnsecureLoginAllowedProperty(originalValue);
    }

    @Test
    @Transactional
    public void checkMessageAuthorizationWithUnsecureLoginAllowed_roleUser_originalSenderMismatch() {

        final UserMessage userMessage = UserMessageSampleUtil.createUserMessage();
        String propertyName = "originalSender";
        setBasicAuthentication_RoleUser_invalidOriginalUser();

        try {
            userMessageSecurityDefaultService.checkMessageAuthorizationWithUnsecureLoginAllowed(userMessage, propertyName);
            Assertions.fail();
        } catch (AuthenticationException ex) {
            LOG.info(ex.getMessage(), ex);
            Assertions.assertTrue(ex.getMessage().contains("You are not allowed to handle this message"));
        }
    }

    @Test
    @Transactional
    public void checkMessageAuthorizationWithUnsecureLoginAllowed_roleUser_validOriginalSender() {
        final UserMessage userMessage = UserMessageSampleUtil.createUserMessage();
        String propertyName = "originalSender";
        setBasicAuthentication_RoleUser_validOriginalSender();
        userMessageSecurityDefaultService.checkMessageAuthorizationWithUnsecureLoginAllowed(userMessage, propertyName);

    }

    private static void setBasicAuthentication_RoleUser_invalidOriginalUser() {
        BasicAuthentication authentication = createPluginUserWithBasicAuthentication();
        authentication.setOriginalUser("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C3");
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private static BasicAuthentication createPluginUserWithBasicAuthentication() {
        BasicAuthentication authentication = new BasicAuthentication("pluginUser1", "pwd");
        authentication.setAuthenticated(true);
        authentication.setAuthorityList(Collections.singleton(new SimpleGrantedAuthority(AuthRole.ROLE_USER.name())));
        return authentication;
    }

    private static void setBasicAuthentication_RoleUser_validOriginalSender() {
        BasicAuthentication authentication = createPluginUserWithBasicAuthentication();
        authentication.setOriginalUser("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1");
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private static void setBasicAuthentication_RoleUser_validFinalRecipient() {
        BasicAuthentication authentication = createPluginUserWithBasicAuthentication();
        authentication.setOriginalUser("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4");
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    @Transactional
    public void checkMessageAuthorizationWithUnsecureLoginAllowed_roleUser_finalRecipientMismatch() {
        final UserMessage userMessage = UserMessageSampleUtil.createUserMessage();
        String propertyName = "finalRecipient";
        setBasicAuthentication_RoleUser_invalidOriginalUser();
        Assertions.assertThrows(AuthenticationException.class,
                () -> userMessageSecurityDefaultService.checkMessageAuthorizationWithUnsecureLoginAllowed(userMessage, propertyName));
    }

    @Test
    @Transactional
    public void checkMessageAuthorizationWithUnsecureLoginAllowed_roleUser_validFinalRecipient() {
        final UserMessage userMessage = UserMessageSampleUtil.createUserMessage();
        String propertyName = "finalRecipient";
        setBasicAuthentication_RoleUser_validFinalRecipient();
        userMessageSecurityDefaultService.checkMessageAuthorizationWithUnsecureLoginAllowed(userMessage, propertyName);
    }

    @Test
    @Transactional
    public void checkMessageAuthorizationWithUnsecureLoginAllowed_adminUser() {
        final UserMessage userMessage = UserMessageSampleUtil.createUserMessage();
        String propertyName = "finalRecipient";
        authUtilsImpl.setAuthenticationToSecurityContext("pluginUser1", "PluginUser@123", AuthRole.ROLE_ADMIN);

        userMessageSecurityDefaultService.checkMessageAuthorizationWithUnsecureLoginAllowed(userMessage, propertyName);
    }
}
