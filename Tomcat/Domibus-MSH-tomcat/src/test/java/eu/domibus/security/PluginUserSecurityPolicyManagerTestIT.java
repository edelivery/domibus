package eu.domibus.security;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.security.AuthRole;
import eu.domibus.api.user.UserManagementException;
import eu.domibus.api.user.plugin.AuthenticationEntity;
import eu.domibus.common.JPAConstants;
import eu.domibus.core.multitenancy.dao.UserDomainDao;
import eu.domibus.core.user.plugin.AuthenticationDAO;
import eu.domibus.core.user.plugin.security.PluginUserSecurityPolicyManager;
import eu.domibus.core.user.ui.UserRoleDao;
import eu.domibus.test.ITTestsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Ion Perpegel
 * @since 4.1
 */
@Transactional
public class PluginUserSecurityPolicyManagerTestIT extends AbstractTomcatTestIT {

    @Autowired
    PluginUserSecurityPolicyManager userSecurityPolicyManager;

    @Autowired
    protected AuthenticationDAO userDao;

    @Autowired
    protected UserRoleDao userRoleDao;

    @Autowired
    UserDomainDao userDomainDao;

    @Autowired
    protected DomainContextProvider domainContextProvider;

    @Autowired
    protected DomibusConfigurationService domibusConfigurationService;

    @Autowired
    private DomibusTaskExecutor domainTaskExecutor;

    @Autowired
    ITTestsService itTestsService;

    @PersistenceContext(unitName = JPAConstants.PERSISTENCE_UNIT_NAME)
    protected EntityManager entityManager;

    @Test
    @Transactional
    public void testPasswordReusePolicy_shouldPass() {
        AuthenticationEntity user = initTestUser("testUser1");
        userSecurityPolicyManager.changePassword(user, "Password-1111111");
        userSecurityPolicyManager.changePassword(user, "Password-2222222");
        userSecurityPolicyManager.changePassword(user, "Password-3333333");
        userSecurityPolicyManager.changePassword(user, "Password-4444444");
        userSecurityPolicyManager.changePassword(user, "Password-5555555");
        userSecurityPolicyManager.changePassword(user, "Password-6666666");
        userSecurityPolicyManager.changePassword(user, "Password-1111111");
    }

    @Test
    @Transactional
    public void testPasswordReusePolicy_shouldFail() {
        AuthenticationEntity user = initTestUser("testUser2");
        Assertions.assertThrows(DomibusCoreException.class, () -> {

            userSecurityPolicyManager.changePassword(user, "Password-1111111");
            userSecurityPolicyManager.changePassword(user, "Password-2222222");
            userSecurityPolicyManager.changePassword(user, "Password-3333333");
            userSecurityPolicyManager.changePassword(user, "Password-4444444");
            userSecurityPolicyManager.changePassword(user, "Password-5555555");
            userSecurityPolicyManager.changePassword(user, "Password-1111111");
        });

    }

    @Test
    @Transactional
    public void testPasswordComplexity_blankPasswordShouldFail() {
        AuthenticationEntity user = initTestUser("testUser3");
        Assertions.assertThrows(DomibusCoreException.class, () -> userSecurityPolicyManager.changePassword(user, ""));
    }

    @Test
    @Transactional
    public void testPasswordComplexity_shortPasswordShouldFail() {
        AuthenticationEntity user = initTestUser("testUser4");
        Assertions.assertThrows(DomibusCoreException. class,() -> userSecurityPolicyManager.changePassword(user, "Aa-1"));
    }

    @Test
    @Transactional
    public void test_validateUniqueUser() {
        AuthenticationEntity user = initTestUser("testUser_Unique");
        Assertions.assertThrows(UserManagementException. class,() -> userSecurityPolicyManager.validateUniqueUser(user));
    }

    private AuthenticationEntity initTestUser(String userName) {
        return itTestsService.createPluginUser(AuthRole.ROLE_USER, userName, "Password-0");
    }
}
