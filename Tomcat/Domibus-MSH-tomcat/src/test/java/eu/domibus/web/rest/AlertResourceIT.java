package eu.domibus.web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import eu.domibus.api.alerts.AlertLevel;
import eu.domibus.core.alerts.model.common.AlertStatus;
import eu.domibus.core.alerts.model.common.AlertType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Ionut Breaz
 * @since 5.1
 */

class AlertResourceIT  extends BaseResourceIT {
    @Autowired
    AlertResource alertResource;

    @BeforeEach
    public void before() throws Exception {
        super.setUp();
    }

    @Test
    void getAlertTypes_OK() throws Exception {
        checkListResult("/rest/internal/admin/alerts/types", AlertType.values().length);
    }

    @Test
    void getAlertLevels_OK() throws Exception {
        checkListResult("/rest/internal/admin/alerts/levels", AlertLevel.values().length);
    }

    @Test
    void getAlertStatus_OK() throws Exception {
        checkListResult("/rest/internal/admin/alerts/status", AlertStatus.values().length);
    }

    @Test
    void getAlertParameters_OK() throws Exception {
        MvcResult result = mockMvc.perform(get("/rest/internal/admin/alerts/params")
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                        .with(csrf())
                        .param("alertType", "PASSWORD_EXPIRED")
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        List<String> list = objectMapper.readValue(content, new TypeReference<List<String>>(){});
        Assertions.assertEquals(3, list.size());
    }

    private void checkListResult(String path, int expectedLength) throws Exception {
        MvcResult result = mockMvc.perform(get(path)
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                        .with(csrf())
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        List<String> list = objectMapper.readValue(content, new TypeReference<List<String>>(){});
        Assertions.assertEquals(expectedLength, list.size());
    }
}
