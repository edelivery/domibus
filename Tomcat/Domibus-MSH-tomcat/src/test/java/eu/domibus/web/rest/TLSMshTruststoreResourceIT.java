package eu.domibus.web.rest;

import eu.domibus.api.crypto.CryptoException;
import eu.domibus.api.exceptions.RequestValidationException;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.pki.DomibusCertificateException;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.util.MultiPartFileUtil;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.web.rest.ro.TrustStoreRO;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

import static eu.domibus.api.crypto.TLSMshCertificateManager.TLS_MSH_TRUSTSTORE_NAME;
import static eu.domibus.api.multitenancy.DomainService.DEFAULT_DOMAIN;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Soumya
 * @author Ion Perpegel
 * @since 5.0
 */
public class TLSMshTruststoreResourceIT extends BaseResourceIT {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSMshTruststoreResourceIT.class);

    public static final String KEYSTORES = "keystores";

    public static final String TEST_ENDPOINT_RESOURCE = "/rest/internal/admin/tlstruststore";

    @Autowired
    private TLSMshTruststoreResource tlsMshTruststoreResource;

    @Autowired
    protected MultiPartFileUtil multiPartFileUtil;

    @Autowired
    DomibusConfigurationService domibusConfigurationService;

    @Autowired
    CertificateHelper certificateHelper;

    @BeforeEach
    public void before() {
        super.setUp();
        resetInitialTruststore();
    }

    @Test
    public void testTruststoreEntries_ok() {
        List<TrustStoreRO> trustStoreROS = tlsMshTruststoreResource.getTLSTruststoreEntries().getTrustStoreList();

        for (TrustStoreRO trustStoreRO : trustStoreROS) {
            Assertions.assertNotNull(trustStoreRO.getName(), "Certificate name should be populated in TrustStoreRO:");
            Assertions.assertNotNull(trustStoreRO.getSubject(), "Certificate subject should be populated in TrustStoreRO:");
            Assertions.assertNotNull(trustStoreRO.getIssuer(), "Certificate issuer should be populated in TrustStoreRO:");
            Assertions.assertNotNull(trustStoreRO.getValidFrom(), "Certificate validity from should be populated in TrustStoreRO:");
            Assertions.assertNotNull(trustStoreRO.getValidUntil(), "Certificate validity until should be populated in TrustStoreRO:");
            Assertions.assertNotNull(trustStoreRO.getFingerprints(), "Certificate fingerprints should be populated in TrustStoreRO:");
            Assertions.assertNotNull(trustStoreRO.getCertificateExpiryAlertDays(), "Certificate imminent expiry alert days should be populated in TrustStoreRO:");
            Assertions.assertEquals(60, trustStoreRO.getCertificateExpiryAlertDays(), "Certificate imminent expiry alert days should be populated in TrustStoreRO:");
        }
    }

    @Test
    public void replaceTrust_EmptyPass() {
        byte[] content = {1, 0, 1};
        String filename = "file";
        MockMultipartFile truststoreFile = new MockMultipartFile("file", filename, "octetstream", content);
        try {
            tlsMshTruststoreResource.uploadTLSTruststoreFile(truststoreFile, "", false);
            Assertions.fail();
        } catch (RequestValidationException ex) {
            Assertions.assertEquals("[DOM_001]:Failed to upload the truststoreFile file since its password was empty.", ex.getMessage());
        }
    }

    @Test
    public void replaceTrust_NotValid() {
        byte[] content = {1, 0, 1};
        String filename = "file.jks";
        MockMultipartFile truststoreFile = new MockMultipartFile("file", filename, "octetstream", content);
        try {
            tlsMshTruststoreResource.uploadTLSTruststoreFile(truststoreFile, "test123", false);
            Assertions.fail();
        } catch (CryptoException ex) {
            Assertions.assertTrue(ex.getMessage().contains("[DOM_001]:Error while replacing the store [TLS.MSH.truststore] with content of the file named [file.jks]."));
        }
    }

    @Test
    public void replaceExisting() throws IOException {
        List<TrustStoreRO> entries = tlsMshTruststoreResource.getTLSTruststoreEntries().getTrustStoreList();
        Assertions.assertEquals(2, entries.size());

        try (InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("keystores/gateway_truststore2.jks")) {
            MultipartFile multiPartFile = new MockMultipartFile("tls_truststore.jks", "tls_truststore.jks",
                    "octetstream", IOUtils.toByteArray(resourceAsStream));
            tlsMshTruststoreResource.uploadTLSTruststoreFile(multiPartFile, "test123", false);

            List<TrustStoreRO> newEntries = tlsMshTruststoreResource.getTLSTruststoreEntries().getTrustStoreList();

            Assertions.assertEquals(9, newEntries.size());
        }
    }

    @Test
    public void uploadStoreWhenNoTrustStoreIsConfigured() throws IOException {
        final String initialTruststoreLocation = domibusPropertyProvider.getProperty(DomainService.DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION);

        //change the truststore location to a non-existing one
        String nonExistingTruststore = "keystores/non_existent_gateway_truststore_" + UUID.randomUUID() + ".jks";
        domibusPropertyProvider.setProperty(DomainService.DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, nonExistingTruststore);

        try (InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("keystores/gateway_truststore2.jks")) {
            MultipartFile multiPartFile = new MockMultipartFile("gateway_truststore2.jks", "gateway_truststore2.jks",
                    "octetstream", IOUtils.toByteArray(resourceAsStream));
            tlsMshTruststoreResource.uploadTLSTruststoreFile(multiPartFile, "test123", false);

            List<TrustStoreRO> newEntries = tlsMshTruststoreResource.getTLSTruststoreEntries().getTrustStoreList();

            Assertions.assertEquals(9, newEntries.size());
        } finally {
            if (StringUtils.isNotBlank(initialTruststoreLocation)) {
                domibusPropertyProvider.setProperty(DomainService.DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, initialTruststoreLocation);
            }

        }
    }

    @Test
    public void setAnew() throws IOException {

        try (InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("keystores/gateway_truststore_original.jks")) {
            MultipartFile multiPartFile = new MockMultipartFile("gateway_truststore.jks", "gateway_truststore.jks",
                    "octetstream", IOUtils.toByteArray(resourceAsStream));
            try {
                tlsMshTruststoreResource.uploadTLSTruststoreFile(multiPartFile, "test123", false);
                Assertions.fail();
            } catch (CryptoException ex) {
                Assertions.assertTrue(ex.getMessage().contains("[DOM_001]:Current store [TLS.MSH.truststore] was not replaced with the content of the file [gateway_truststore.jks] because they are identical."));
            }
        }
    }

    @Test()
    public void downloadTrust() {
        ResponseEntity<ByteArrayResource> res = tlsMshTruststoreResource.downloadTLSTrustStore();
        Assertions.assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @Test
    @Transactional
    @WithMockUser(username = TEST_ADMIN_USERNAME, roles = {"ADMIN"})
    public void uploadTrustStore() throws Exception {
//        uploadTrustStore("keystores/default.jks", "default.jks");

        MvcResult result;
        result = uploadTrustStore("keystores/gateway_truststore2.jks", "gateway_truststore2.jks");
        String content = result.getResponse().getContentAsString();
        Assertions.assertEquals("\"TLS MSH truststore file has been successfully replaced.\"", content);
    }

    private static String getAliases(KeyStore keystore) throws KeyStoreException {
        StringBuilder stringBuilder = new StringBuilder();
        Enumeration<String> aliases = keystore.aliases();
        while (aliases.hasMoreElements()) {
            stringBuilder.append(aliases.nextElement()).append(",");
        }
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }

    private MvcResult uploadTrustStore(String name, String originalFilename) throws Exception {
        MvcResult result;
        KeyStore keystore;
        try (InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(name)) {
            keystore = KeyStore.getInstance("jks");
            keystore.load(resourceAsStream, "test123".toCharArray());
        }
        LOG.info("upload truststore with aliases [{}]", getAliases(keystore));

        try (InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(name)) {
            MockMultipartFile multiPartFile = getMultiPartFile(originalFilename, resourceAsStream);

            result = mockMvc.perform(multipart(TEST_ENDPOINT_RESOURCE)
                            .file(multiPartFile)
                            .param("password", "test123")
                            .param("allowChangingDiskStoreProps", "false")
                            .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                            .with(csrf()))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();
        }
        return result;
    }

    @Test
    @Transactional
    @WithMockUser(username = TEST_ADMIN_USERNAME, roles = {"ADMIN"})
    public void downloadTrustStore() throws Exception {
        uploadTrustStore("keystores/default.jks", "default.jks");

        // when
        MvcResult result = mockMvc.perform(get(TEST_ENDPOINT_RESOURCE)
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                        .with(csrf()))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        // then
        String content = result.getResponse().getContentAsString();
        Assertions.assertNotNull(content);
    }

    @Test
    void addTLSCertificateWithoutSecurityProfiles() {
        Assertions.assertThrows(DomibusCertificateException.class, () -> addTLSCertificate());
    }

    protected void addTLSCertificate() {
        byte[] content = {1, 0, 1};
        String filename = "file";
        MockMultipartFile truststoreFile = new MockMultipartFile("file", filename, "octetstream", content);
        tlsMshTruststoreResource.addTLSCertificate(truststoreFile, "tlscert");
    }


    @Test
    void removeTLSCertificateWithoutSecurityProfiles() {
        Assertions.assertThrows(DomibusCertificateException.class, () -> removeTLSCertificate());
    }

    public void removeTLSCertificate() {
        tlsMshTruststoreResource.removeTLSCertificate("tlscert");
    }

    private void resetInitialTruststore() {
        try {
            String storePassword = "test123";
            String relativePath = KEYSTORES + "/gateway_truststore_original.jks";

            Path path = Paths.get(domibusConfigurationService.getConfigLocation(), relativePath);
            byte[] content = Files.readAllBytes(path);

            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, KEYSTORES + "/mykeystore.jks");
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_TYPE, "jks");
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_PASSWORD, "test123");

            KeyStoreContentInfo storeInfo = certificateHelper.createStoreContentInfo(TLS_MSH_TRUSTSTORE_NAME, KEYSTORES + "/mykeystore.jks", content, storePassword);
            tlsMshTruststoreResource.doUploadStore(storeInfo);
        } catch (Exception e) {
            LOG.info("Error restoring initial keystore", e);
        }
    }
}
