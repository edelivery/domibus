package eu.domibus.web.rest;

import eu.domibus.common.CsvUtil;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.plugin.BackendConnectorProvider;
import eu.domibus.plugin.BackendConnector;
import eu.domibus.test.ITTestsService;
import eu.domibus.test.SoapMessageSecurityUtil;
import eu.domibus.test.common.SoapSampleUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import javax.xml.soap.SOAPMessage;
import java.util.Arrays;
import java.util.List;

import static eu.domibus.core.message.MessageLogInfoFilter.MESSAGE_ACTION;
import static eu.domibus.core.message.MessageLogInfoFilter.MESSAGE_SERVICE_TYPE;
import static eu.domibus.web.rest.MessageLogResource.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Ionut Breaz
 * @since 5.1
 */

@Transactional
class MessageLogResourceIT extends BaseResourceIT {

    @Autowired
    private MessageLogResource messageLogResource;

    @Autowired
    BackendConnectorProvider backendConnectorProvider;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    CsvUtil csvUtil;

    @Autowired
    MSHWebservice mshWebservice;

    @Autowired
    SoapMessageSecurityUtil soapMessageSecurityUtil;

    @Autowired
    ITTestsService itTestsService;

    private final String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

    @BeforeEach
    protected void before() throws Exception {
        super.setUp();
        uploadPMode();
        BackendConnector backendConnector = Mockito.mock(BackendConnector.class);
        Mockito.when(backendConnectorProvider.getBackendConnector(Mockito.any(String.class))).thenReturn(backendConnector);

        itTestsService.receiveMessage(messageId);
    }

    @Test
    void getMessageLog_OK() throws Exception {
        final List<String> list = Arrays.asList(PROPERTY_MESSAGE_STATUS, PROPERTY_MSH_ROLE, PROPERTY_NOTIFICATION_STATUS, MESSAGE_ACTION, MESSAGE_SERVICE_TYPE, PROPERTY_FROM_PARTY_ID, PROPERTY_TO_PARTY_ID);
        mockMvc.perform(get("/rest/internal/user/messagelog")
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                        .param("fields", list.toArray(new String[0]))
                        .with(csrf())
                        .param("messageId", messageId)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.count").value(1))
                .andExpect(jsonPath("$.messageLogEntries[0].messageId").value(messageId))
                .andExpect(jsonPath("$.messageLogEntries[0].fromPartyId").value("domibus-blue"))
                .andExpect(jsonPath("$.messageLogEntries[0].toPartyId").value("domibus-red"))
                .andExpect(jsonPath("$.messageLogEntries[0].messageStatus").value("RECEIVED"))
                .andExpect(jsonPath("$.messageLogEntries[0].notificationStatus").value("REQUIRED"))
                .andExpect(jsonPath("$.messageLogEntries[0].mshRole").value("RECEIVING"))
                .andExpect(jsonPath("$.messageLogEntries[0].messageType").value("USER_MESSAGE"));
    }

    @Test
    void getCsv_OK() throws Exception {
        final List<String> list = Arrays.asList(PROPERTY_MESSAGE_ID, PROPERTY_MESSAGE_STATUS, PROPERTY_MSH_ROLE, PROPERTY_NOTIFICATION_STATUS,
                MESSAGE_ACTION, MESSAGE_SERVICE_TYPE, PROPERTY_FROM_PARTY_ID, PROPERTY_TO_PARTY_ID);
        MvcResult result = mockMvc.perform(get("/rest/internal/user/messagelog/csv")
                        .contentType("text/html; charset=UTF-8")
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                        .with(csrf())
                        .param("fields", list.toArray(new String[0]))
                        .param("messageId", messageId)
                        .param("orderBy", "received")
                        .param("asc", "false")
                        .param("page", "0")
                        .param("pageSize", "10")

                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(header().exists("Content-Disposition"))
                .andReturn();

        String csv = result.getResponse().getContentAsString();
        Assertions.assertNotNull(csv);

        List<List<String>> csvRecords = csvUtil.getCsvRecords(csv);
        Assertions.assertEquals(2, csvRecords.size());
        List<String> header = csvRecords.get(0);
        List<String> row = csvRecords.get(1);
        Assertions.assertEquals("Message Id", header.get(0));
        Assertions.assertEquals(messageId, row.get(0));
        Assertions.assertEquals("From Party Id", header.get(1));
        Assertions.assertEquals("domibus-blue", row.get(1));
        Assertions.assertEquals("To Party Id", header.get(2));
        Assertions.assertEquals("domibus-red", row.get(2));
        Assertions.assertEquals("Message Status", header.get(3));
        Assertions.assertEquals("RECEIVED", row.get(3));
        Assertions.assertEquals("AP Role", header.get(5));
        Assertions.assertEquals("RECEIVING", row.get(5));
    }

    @Test
    void getLastTestSent_Not_Found() throws Exception {
        MvcResult result = mockMvc.perform(get("/rest/internal/admin/messagelog/test/outgoing/latest")
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                        .with(csrf())
                        .param("partyId", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1")
                        .param("senderPartyId", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4")
                )
                .andReturn();
        String response = result.getResponse().getContentAsString();
        Assertions.assertTrue(response.contains("[DOM_001]:No User message found for party"));
    }

    @Test
    void getLastTestReceived_Not_Found() throws Exception {
        MvcResult result = mockMvc.perform(get("/rest/internal/admin/messagelog/test/incoming/latest")
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                        .with(csrf())
                        .param("partyId", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1")
                        .param("senderPartyId", "urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4")
                )
                .andReturn();
        String response = result.getResponse().getContentAsString();
        Assertions.assertTrue(response.contains("No Signal Message found"));
    }
}
