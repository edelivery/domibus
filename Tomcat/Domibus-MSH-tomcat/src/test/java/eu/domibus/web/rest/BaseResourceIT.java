package eu.domibus.web.rest;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.web.rest.ro.LoginRO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.SharedHttpSessionConfigurer.sharedHttpSession;

/**
 * @author Ion Perpegel
 * @since 5.2
 * Base class for IT test classes that test resources
 */
@Transactional
public class BaseResourceIT extends AbstractTomcatTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BaseResourceIT.class);

    protected MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Autowired
    protected AuthenticationResource authenticationResource;

    @Override
    protected void setAuth() {
        //intentionally
    }

    protected void setUp() {
        setUp(DomibusHttpSessionStrategy.SINGLE_SESSION);
    }

    protected void setUp(DomibusHttpSessionStrategy domibusHttpSessionStrategy) {
        final DefaultMockMvcBuilder defaultMockMvcBuilder = MockMvcBuilders.webAppContextSetup(context);
        if (DomibusHttpSessionStrategy.SHARED_SESSION == domibusHttpSessionStrategy) {
            LOG.info("Using HTTP shared session");
            defaultMockMvcBuilder.apply(sharedHttpSession());
        }
        this.mockMvc = defaultMockMvcBuilder
                .apply(springSecurity())
                .build();
    }

    protected MvcResult loginWithAdmin() throws Exception {
        return login(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD);
    }

    protected MvcResult login(String userName, String userPass) throws Exception {
        LoginRO payload = new LoginRO();
        payload.setUsername(userName);
        payload.setPassword(userPass);

        MvcResult result = mockMvc.perform(post("/rest/public/security/authentication")
                        .content(asJsonString(payload))
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(csrf())
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        return result;
    }
}
