package eu.domibus.web.rest;

import eu.domibus.api.security.AuthUtils;
import eu.domibus.core.converter.DomibusCoreMapper;
import eu.domibus.core.logging.LoggingEntry;
import eu.domibus.core.logging.LoggingService;
import eu.domibus.web.rest.ro.LoggingFilterRequestRO;
import eu.domibus.web.rest.ro.LoggingLevelRO;
import org.apache.commons.lang3.BooleanUtils;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author François Gautier
 * @since 4.2
 */
public class LoggingResourceIT extends BaseResourceIT {

    @Autowired
    private DomibusCoreMapper coreMapper;

    @Autowired
    private LoggingService loggingService;

    @Autowired
    private LoggingResource loggingResource;

    @Autowired
    protected AuthUtils authUtils;

    private LoggingService loggingServiceMock;


    @BeforeEach
    public void setUp() {
        super.setUp();
        loggingServiceMock = Mockito.mock(LoggingService.class);

        loggingResource.setLoggingService(loggingServiceMock);
    }

    @Test
    public void setLogLevel_ok() throws Exception {

        LoggingLevelRO loggingLevelRO = new LoggingLevelRO();
        loggingLevelRO.setLevel("DEBUG");
        loggingLevelRO.setName("eu.domibus");

        Mockito.when(loggingServiceMock.exists(loggingLevelRO.getName())).thenReturn(true);

        mockMvc.perform(post("/rest/internal/admin/logging/loglevel")
                        .with(httpBasic(TEST_SUPER_USERNAME, TEST_SUPER_PASSWORD))
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(loggingLevelRO)))
                .andExpect(status().is2xxSuccessful())
                .andReturn();


    }

//    @Test
//    @WithMockUser(username = "admin", roles = {"AP_ADMIN"})
//    public void setLogLevel_nok_custom_name() throws Exception {
//        LoggingLevelRO loggingLevelRO = new LoggingLevelRO();
//        loggingLevelRO.setLevel("DEBUG");
//        loggingLevelRO.setName("custom.package");
//
//        Mockito.when(loggingService.exists(loggingLevelRO.getName())).thenReturn(false);
//
//        mockMvc.perform(post("/rest/logging/loglevel")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(asJsonString(loggingLevelRO)))
//                .andExpect(status().is4xxClientError())
//                .andReturn();
//    }

    @Test
    public void setLogLevel_nok_ALL() throws Exception {

        LoggingLevelRO loggingLevelRO = new LoggingLevelRO();
        loggingLevelRO.setLevel("CUSTOM_LEVEL");
        loggingLevelRO.setName("eu.domibus");

        mockMvc.perform(post("/rest/internal/admin/logging/loglevel")
                        .with(httpBasic(TEST_SUPER_USERNAME, TEST_SUPER_PASSWORD))
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(loggingLevelRO)))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    void getLogLevel_accessDenied() throws Exception {
        mockMvc.perform(get("/rest/internal/admin/logging/loglevel"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getLogLevel_ok() throws Exception {
        domainContextProvider.clearCurrentDomain();

        final List<LoggingEntry> loggingEntryList = new ArrayList<>();
        LoggingEntry loggingLevelRO1 = new LoggingEntry();
        loggingLevelRO1.setLevel("INFO");
        loggingLevelRO1.setName("eu.domibus");
        loggingEntryList.add(loggingLevelRO1);
        LoggingEntry loggingLevelRO2 = new LoggingEntry();
        loggingLevelRO2.setLevel("DEBUG");
        loggingLevelRO2.setName("eu.domibus.common");
        loggingEntryList.add(loggingLevelRO2);
        LoggingEntry loggingLevelRO3 = new LoggingEntry();
        loggingLevelRO3.setLevel("TRACE");
        loggingLevelRO3.setName("eu.domibus.common.model");
        loggingEntryList.add(loggingLevelRO3);

        LoggingFilterRequestRO loggingFilterRequestRO = new LoggingFilterRequestRO();
        loggingFilterRequestRO.setAsc(Boolean.TRUE);
        loggingFilterRequestRO.setLoggerName("eu.domibus2");
        loggingFilterRequestRO.setOrderBy("");
        loggingFilterRequestRO.setPageSize(20);
        loggingFilterRequestRO.setPage(0);
        loggingFilterRequestRO.setShowClasses(true);

        Mockito.when(loggingServiceMock.getLoggingLevel(loggingFilterRequestRO.getLoggerName(), loggingFilterRequestRO.isShowClasses())).thenReturn(loggingEntryList);

        // the order of the items are not checked
        mockMvc.perform(get("/rest/internal/admin/logging/loglevel").with(httpBasic(TEST_SUPER_USERNAME, TEST_SUPER_PASSWORD))
                        .param("page", loggingFilterRequestRO.getPage() + "")
                        .param("loggerName", loggingFilterRequestRO.getLoggerName())
                        .param("pageSize", loggingFilterRequestRO.getPageSize() + "")
                        .param("orderBy", loggingFilterRequestRO.getOrderBy())
                        .param("asc", BooleanUtils.toStringTrueFalse(loggingFilterRequestRO.getAsc()))
                        .param("showClasses", BooleanUtils.toStringTrueFalse(loggingFilterRequestRO.isShowClasses()))
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.filter.loggerName").value(loggingFilterRequestRO.getLoggerName()))
                .andExpect(jsonPath("$.filter.showClasses").value(loggingFilterRequestRO.isShowClasses()))
                .andExpect(jsonPath("$.pageSize").value(loggingFilterRequestRO.getPageSize()))
                .andExpect(jsonPath("$.page").value(loggingFilterRequestRO.getPage()))
                .andExpect(jsonPath("$.loggingEntries.[*].name").value(hasItems(
                        "eu.domibus",
                        "eu.domibus.common",
                        "eu.domibus.common.model"
                )))
                .andExpect(jsonPath("$.loggingEntries.[*].level").value(hasItems(
                        "INFO",
                        "DEBUG",
                        "TRACE"
                )))
        ;
    }
}
