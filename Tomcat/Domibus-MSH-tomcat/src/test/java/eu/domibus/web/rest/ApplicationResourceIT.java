package eu.domibus.web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import eu.domibus.web.rest.ro.DomibusInfoRO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
class ApplicationResourceIT extends BaseResourceIT {

    @BeforeEach
    public void setUp() {
        super.setUp();
        createUser();
    }

    @Test
    void getAppInfo() throws Exception {
        MvcResult result = mockMvc.perform(get("/rest/internal/user/application/info")
                        .with(httpBasic(TEST_ROLE_USER_USERNAME, TEST_ROLE_USER_PASSWORD))
                        .with(csrf())
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        DomibusInfoRO info = objectMapper.readValue(content, new TypeReference<DomibusInfoRO>() {});
    }

}
