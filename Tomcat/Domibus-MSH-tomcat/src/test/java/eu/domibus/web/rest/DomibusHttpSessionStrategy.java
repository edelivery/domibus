package eu.domibus.web.rest;

public enum DomibusHttpSessionStrategy {

    //no session is reused between requests
    SINGLE_SESSION,

    //when using shared session the session is reused between requests
    //eg you can login once and then call another resource while being authenticated
    SHARED_SESSION;
}
