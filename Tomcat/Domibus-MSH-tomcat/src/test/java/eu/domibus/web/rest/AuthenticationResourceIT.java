package eu.domibus.web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import eu.domibus.web.rest.ro.UserRO;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
class AuthenticationResourceIT extends BaseResourceIT {

    @BeforeEach
    public void setUp() {
        super.setUp(DomibusHttpSessionStrategy.SHARED_SESSION);
    }

    @Test
    void logInAndOut() throws Exception {
        MvcResult result = loginWithAdmin();

        String content = result.getResponse().getContentAsString();
        UserRO user = objectMapper.readValue(content, UserRO.class);
        Assert.assertEquals(TEST_ADMIN_USERNAME, user.getUsername());

        logout();
    }

    @Test
    void getCurrentUser() throws Exception {
        MvcResult result1 = mockMvc.perform(get("/rest/internal/user/security/user")
                        .with(csrf())
                )
                .andReturn();

        String content = result1.getResponse().getContentAsString();
        Assert.assertEquals(StringUtils.EMPTY, content);

        MvcResult result2 = loginWithAdmin();
        String content2 = result2.getResponse().getContentAsString();
        UserRO user1 = objectMapper.readValue(content2, new TypeReference<UserRO>() {});

        MvcResult result3 = mockMvc.perform(get("/rest/internal/user/security/user")
                        .with(csrf())
                )
                .andReturn();
        String content3 = result3.getResponse().getContentAsString();
        UserRO user2 = objectMapper.readValue(content3, new TypeReference<UserRO>() {});

        Assert.assertEquals(user1.getUsername(), user2.getUsername());
    }

    protected void logout() throws Exception {
        mockMvc.perform(delete("/rest/internal/user/security/authentication")
                        .with(csrf())
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }

}
