package eu.domibus.web.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import eu.domibus.core.plugin.BackendConnectorProvider;
import eu.domibus.core.user.multitenancy.SuperUserManagementServiceImpl;
import eu.domibus.web.rest.ro.DomainRO;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
class DomainResourceIT extends BaseResourceIT {

    @Autowired
    DomainResource domainResource;

    @Autowired
    SuperUserManagementServiceImpl superUserManagementService;

    @Autowired
    protected BackendConnectorProvider backendConnectorProvider;

    @Override
    protected void setAuth() {
        //intentionally
    }

    @BeforeEach
    public void setUp() {
        super.setUp();

        domainContextProvider.clearCurrentDomain();
    }

    @Test
    void getDomains() throws Exception {
        MvcResult result = mockMvc.perform(get("/rest/internal/super/domains")
                        .with(httpBasic(TEST_SUPER_USERNAME, TEST_SUPER_PASSWORD))
                        .with(csrf())
                        .param("active", "true")
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        List<DomainRO> entries = objectMapper.readValue(content, new TypeReference<List<DomainRO>>() {
        });
        Assert.assertEquals(2, entries.size());
        Assert.assertEquals("default", entries.get(0).getCode());
        Assert.assertEquals("red", entries.get(1).getCode());
    }


}
