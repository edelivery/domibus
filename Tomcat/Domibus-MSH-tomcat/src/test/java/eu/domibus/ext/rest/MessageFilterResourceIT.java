package eu.domibus.ext.rest;

import eu.domibus.core.plugin.BackendConnectorProvider;
import eu.domibus.test.common.BackendConnectorMock;
import eu.domibus.web.rest.BaseResourceIT;
import eu.domibus.web.rest.MessageFilterResource;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author François Gautier
 * @since 5.1
 */
public class MessageFilterResourceIT extends BaseResourceIT {

    public static final String TEST_ENDPOINT_RESOURCE = "/rest/internal/admin/messagefilters";

    @Autowired
    private BackendConnectorProvider backendConnectorProvider;

    @Autowired
    private MessageFilterResource messageFilterResource;

    @BeforeEach
    public void setUp() {
        super.setUp();
    }

    @Test
    public void getMessageFilter() throws Exception {
        Mockito.when(backendConnectorProvider.getBackendConnector(Mockito.any(String.class)))
                .thenReturn(backendConnectorMock);

        mockMvc.perform(get(TEST_ENDPOINT_RESOURCE)
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect((jsonPath("$.messageFilterEntries", Matchers.hasSize(1))));
    }

}
