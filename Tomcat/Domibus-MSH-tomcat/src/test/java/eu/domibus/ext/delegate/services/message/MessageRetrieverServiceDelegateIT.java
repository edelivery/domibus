package eu.domibus.ext.delegate.services.message;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.model.UserMessageLog;
import eu.domibus.common.ErrorResult;
import eu.domibus.common.MSHRole;
import eu.domibus.common.MessageDaoTestUtil;
import eu.domibus.common.MessageStatus;
import eu.domibus.core.plugin.BackendConnectorProvider;
import eu.domibus.messaging.DuplicateMessageException;
import eu.domibus.messaging.MessageNotFoundException;
import eu.domibus.plugin.BackendConnector;
import eu.domibus.plugin.Submission;
import eu.domibus.test.ITTestsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
public class MessageRetrieverServiceDelegateIT extends AbstractTomcatTestIT {
    private static final String MESS_ID = UUID.randomUUID().toString();
    private static final String MESS_1 = "msg_ack_100";

    @Autowired
    private MessageRetrieverServiceDelegate messageRetrieverServiceDelegate;

    @Autowired
    ITTestsService itTestsService;

    @Autowired
    MessageDaoTestUtil messageDaoTestUtil;

    @Autowired
    BackendConnectorProvider backendConnectorProvider;

    @Test
    public void testDownloadMessageAuthUserNok() {
        try {
            messageRetrieverServiceDelegate.checkMessageAuthorization(MESS_ID);
            Assertions.fail("It should throw AuthenticationException");
        } catch (eu.domibus.api.messaging.MessageNotFoundException adEx) {
            assertTrue(adEx.getMessage().contains("[DOM_009]:Message [" + MESS_ID + "] does not exist"));
        }
    }

    @Test
    public void testGetStatus() throws MessageNotFoundException, DuplicateMessageException {

        // When
        final MessageStatus status = messageRetrieverServiceDelegate.getStatus("not_found");
        Assertions.assertEquals(MessageStatus.NOT_FOUND, status);

    }

    @Test
    public void testGetStatus_found() throws MessageNotFoundException, DuplicateMessageException {
        createAcknowledgedMessage();


        // When
        final MessageStatus status = messageRetrieverServiceDelegate.getStatus(MESS_1);

        Assertions.assertEquals(MessageStatus.ACKNOWLEDGED, status);
    }

    private UserMessageLog createAcknowledgedMessage() {
        UserMessageLog userMessageLog1 = messageDaoTestUtil.createUserMessageAndUserMessageLog(
                MESS_1,
                new Date(),
                eu.domibus.api.model.MSHRole.SENDING,
                eu.domibus.api.model.MessageStatus.ACKNOWLEDGED,
                false,
                true,
                "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC",
                null,
                false);
        return userMessageLog1;
    }

    private UserMessageLog createReceivedMessage() {
        UserMessageLog userMessageLog1 = messageDaoTestUtil.createUserMessageAndUserMessageLog(
                MESS_1,
                new Date(),
                eu.domibus.api.model.MSHRole.RECEIVING,
                eu.domibus.api.model.MessageStatus.RECEIVED,
                false,
                true,
                "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC",
                null,
                false);
        return userMessageLog1;
    }

    @Test
    public void testGetStatus_mshrole_found() throws MessageNotFoundException {
        createAcknowledgedMessage();

        // When
        final MessageStatus status = messageRetrieverServiceDelegate.getStatus(MESS_1, MSHRole.SENDING);

        Assertions.assertEquals(MessageStatus.ACKNOWLEDGED, status);
    }

    @Test
    public void testGetStatus_found2() throws MessageNotFoundException {
        final UserMessageLog userMessageLog = createAcknowledgedMessage();

        // When
        final MessageStatus status = messageRetrieverServiceDelegate.getStatus(userMessageLog.getEntityId());

        Assertions.assertEquals(MessageStatus.ACKNOWLEDGED, status);
    }

    @Test
    public void download() throws MessageNotFoundException {
        final UserMessageLog receivedMessage = createReceivedMessage();

        // When
        final Submission msg = messageRetrieverServiceDelegate.downloadMessage(receivedMessage.getEntityId(), false);

        Assertions.assertNotNull(msg);
    }

    @Test
    public void download2() throws MessageNotFoundException {
        final UserMessageLog receivedMessage = createReceivedMessage();

        // When
        final Submission msg = messageRetrieverServiceDelegate.downloadMessage(MESS_1, false);

        Assertions.assertNotNull(msg);
    }

    @Test
    public void download3() throws MessageNotFoundException {
        final UserMessageLog acknowledgedMessage = createAcknowledgedMessage();

        // When
        Assertions.assertThrows(MessageNotFoundException.class, () -> messageRetrieverServiceDelegate.downloadMessage(acknowledgedMessage.getEntityId()));
        //expecting error: Could not find message with entity id [230412100000000001] in status RECEIVED. Current status is [ACKNOWLEDGED]
    }

    @Test
    public void download4() throws MessageNotFoundException {
        final UserMessageLog acknowledgedMessage = createAcknowledgedMessage();

        // When
        Assertions.assertThrows(MessageNotFoundException.class, () -> messageRetrieverServiceDelegate.downloadMessage(MESS_1));
        //Could not find message with entity id [230412100000000001] in status RECEIVED. Current status is [ACKNOWLEDGED]
    }

    @Test
    public void markMessageAsDownloaded() throws Exception {
        final String messageId = "cos-23";
        uploadPMode();
        BackendConnector backendConnector = Mockito.mock(BackendConnector.class);
        Mockito.when(backendConnectorProvider.getBackendConnector(Mockito.any(String.class))).thenReturn(backendConnector);


        itTestsService.receiveMessage(messageId);

        // When
        messageRetrieverServiceDelegate.markMessageAsDownloaded(messageId);
    }

    @Test
    public void browseMessage() throws Exception {
        final UserMessageLog receivedMessage = createReceivedMessage();

        // When
        final Submission msg = messageRetrieverServiceDelegate.browseMessage(MESS_1, MSHRole.RECEIVING);

        Assertions.assertNotNull(msg);
    }

    @Test
    public void browseMessage2() throws MessageNotFoundException {
        final UserMessageLog receivedMessage = createReceivedMessage();

        // When
        final Submission msg = messageRetrieverServiceDelegate.browseMessage(receivedMessage.getEntityId());

        Assertions.assertNotNull(msg);
    }

    @Test
    public void browseMessage3() throws MessageNotFoundException {
        final UserMessageLog receivedMessage = createReceivedMessage();

        // When
        final Submission msg = messageRetrieverServiceDelegate.browseMessage(MESS_1);

        Assertions.assertNotNull(msg);
    }

    @Test
    public void getErrorsForMessage() throws MessageNotFoundException, DuplicateMessageException {
        createAcknowledgedMessage();

        // When
        List<? extends ErrorResult> errorsForMessage = messageRetrieverServiceDelegate.getErrorsForMessage(MESS_1);

        Assertions.assertEquals(0, errorsForMessage.size());
    }
    @Test
    public void getErrorsForMessage2() throws MessageNotFoundException {
        createReceivedMessage();

        // When
        List<? extends ErrorResult> errorsForMessage = messageRetrieverServiceDelegate.getErrorsForMessage(MESS_1, MSHRole.RECEIVING);

        Assertions.assertEquals(0, errorsForMessage.size());
    }


}
