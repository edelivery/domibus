package eu.domibus.ext.rest;

import eu.domibus.api.model.MpcEntity;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.core.message.dictionary.MpcDao;
import eu.domibus.ext.services.CacheExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.web.rest.BaseResourceIT;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

/**
 * @author François Gautier
 * @since 5.0
 */
@Transactional
public class CacheExtResourceIT extends BaseResourceIT {

    private final static DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusEArchiveExtResourceIT.class);
    public static final String NOT_FOUND = "not_found";

    @Autowired
    private CacheExtResource cacheExtResource;

    @Autowired
    private CacheExtService cacheExtService;
    @Autowired
    private CacheManager cacheManager;
    @Autowired
    private DomainService domainService;
    @Autowired
    private MpcDao mpcDao;
    @Autowired
    protected LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean;

    private org.hibernate.Cache secondLevelCache;
    private MpcEntity dummyMpc;

    @BeforeEach
    public void setUp() {
        super.setUp();
        secondLevelCache = localContainerEntityManagerFactoryBean
                .getNativeEntityManagerFactory()
                .unwrap(SessionFactory.class)
                .getCache();

        //set one value in cache
        domainService.getDomain(NOT_FOUND);
        //check value is present in cache
        checkStillInCache();

        //set one value in 2L cache
        dummyMpc = mpcDao.findOrCreateMpc("DUMMY_MPC");
    }

    @AfterEach
    public void tearDown() throws Exception {
        cacheExtService.evict2LCaches();
        cacheExtService.evictCaches();
    }

    private boolean specificMpc2LIsCached(long mpc) {
        return secondLevelCache.contains(MpcEntity.class, mpc);
    }

    private Cache.ValueWrapper getSpecificDomainCached(String domainName) {
        Cache domainByCode = cacheManager.getCache("domainByCode");
        if (domainByCode == null) {
            return null;
        }
        return domainByCode.get(domainName);
    }

    @Test
    public void deleteCache_noUser() throws Exception {
        mockMvc.perform(delete("/ext/cache"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());

        checkStillInCache();
    }

    private void checkStillInCache() {
        Cache.ValueWrapper wrapper = getSpecificDomainCached(NOT_FOUND);
        Assertions.assertNotNull(wrapper);
    }

    private void checkNothingInCache() {
        Cache.ValueWrapper domainCached = getSpecificDomainCached(NOT_FOUND);
        Assertions.assertNull(domainCached);
    }

    @Test
    public void deleteCache_notAdmin() throws Exception {
        mockMvc.perform(delete("/ext/cache"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());

        checkStillInCache();
    }

    @Test
    public void deleteCache_admin() throws Exception {
        mockMvc.perform(delete("/ext/cache")
                .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
        );

        checkNothingInCache();
    }

    @Test
    public void delete2LCache_noUser() throws Exception {
        mockMvc.perform(delete("/ext/2LCache"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
        checkStillIn2LCache();
    }

    private void checkStillIn2LCache() {
        boolean isCached = specificMpc2LIsCached(dummyMpc.getEntityId());
        Assertions.assertTrue(isCached);
    }

    private void checkNothingIn2LCache() {
        Assertions.assertFalse(specificMpc2LIsCached(dummyMpc.getEntityId()));
    }

    @Test
    public void delete2LCache_notAdmin() throws Exception {
        createUser();

        mockMvc.perform(delete("/ext/2LCache")
                .with(httpBasic(TEST_ROLE_USER_USERNAME, TEST_ROLE_USER_PASSWORD))
        );

        checkStillIn2LCache();
    }

    @Test
    public void delete2LCache_admin() throws Exception {
        mockMvc.perform(delete("/ext/2LCache")
                .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
        );

        checkNothingIn2LCache();
    }

}
