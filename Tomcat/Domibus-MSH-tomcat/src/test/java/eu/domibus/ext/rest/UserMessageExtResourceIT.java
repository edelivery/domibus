package eu.domibus.ext.rest;


import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.model.MessageStatus;
import eu.domibus.api.model.UserMessageLog;
import eu.domibus.common.MSHRole;
import eu.domibus.common.MessageDaoTestUtil;
import eu.domibus.ext.domain.UserMessageDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class UserMessageExtResourceIT extends AbstractTomcatTestIT {

    public static final String TEST_ENDPOINT_BASE = "/ext/messages/usermessages";
    public static final String TEST_ENDPOINT_MESSAGE_ID = TEST_ENDPOINT_BASE + "/{messageId}";
    public static final String TEST_ENDPOINT_MESSAGE_ID_ENVELOPE = TEST_ENDPOINT_MESSAGE_ID + "/envelope";
    public static final String TEST_ENDPOINT_MESSAGE_ID_SIGNALENVELOP = TEST_ENDPOINT_MESSAGE_ID + "/signalEnvelope";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webAppContext;

    @Autowired
    MessageDaoTestUtil messageDaoTestUtil;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext)
                .build();
    }

    @Test
    @Transactional
    public void testDownloadMsg_messageFound() throws Exception {
        createReceivedMessage();


        MvcResult result = mockMvc.perform(get(TEST_ENDPOINT_MESSAGE_ID, "msg_ack_100")
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        UserMessageDTO response = objectMapper.readValue(content, UserMessageDTO.class);

        Assertions.assertNotNull(response);
    }

    private UserMessageLog createReceivedMessage() {
        UserMessageLog userMessageLog1 = messageDaoTestUtil.createUserMessageAndUserMessageLog(
                "msg_ack_100",
                new Date(),
                eu.domibus.api.model.MSHRole.RECEIVING,
                MessageStatus.RECEIVED,
                false,
                true,
                "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC",
                null,
                false);
        return userMessageLog1;
    }

    private UserMessageLog createAcknowledgedMessage() {
        UserMessageLog userMessageLog1 = messageDaoTestUtil.createUserMessageAndUserMessageLog(
                "msg_ack_100",
                new Date(),
                eu.domibus.api.model.MSHRole.SENDING,
                MessageStatus.ACKNOWLEDGED,
                false,
                true,
                "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC",
                null,
                false);
        return userMessageLog1;
    }


    @Test
    @Transactional
    public void testDownloadMsg_messageNotFound_role() throws Exception {

         mockMvc.perform(get(TEST_ENDPOINT_MESSAGE_ID, "msg_ack_100")
                        .param("mshRole", MSHRole.SENDING.name())
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                )
                .andExpect(status().is4xxClientError())
                .andReturn();
    }



    @Test
    @Transactional
    public void testDownloadEnvelop_messageFound() throws Exception {
        createReceivedMessage();

        MvcResult result = mockMvc.perform(get(TEST_ENDPOINT_MESSAGE_ID_ENVELOPE, "msg_ack_100")
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        String content = result.getResponse().getContentAsString();

        Assertions.assertNotNull(content);
    }

    @Test
    @Transactional
    public void testDownloadEnvelop_messageNotFound_role() throws Exception {

         mockMvc.perform(get(TEST_ENDPOINT_MESSAGE_ID_ENVELOPE, "msg_ack_100")
                        .param("mshRole", MSHRole.SENDING.name())
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                )
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    @Transactional
    public void testDownloadSignalEnvelop_messageFound() throws Exception {
        final String messageId = "msg_ack_100";
        messageDaoTestUtil.createSignalMessageLog(messageId, new Date());


        MvcResult result = mockMvc.perform(get(TEST_ENDPOINT_MESSAGE_ID_SIGNALENVELOP, messageId)
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        String content = result.getResponse().getContentAsString();

        Assertions.assertNotNull(content);
    }

    @Test
    @Transactional
    public void testDownloadSignalEnvelop_messageNotFound_role() throws Exception {

         mockMvc.perform(get(TEST_ENDPOINT_MESSAGE_ID_SIGNALENVELOP, "msg_ack_100")
                        .param("mshRole", MSHRole.SENDING.name())
                        .with(httpBasic(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD))
                )
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

}
