package eu.domibus.ext.rest;

import eu.domibus.api.crypto.SameResourceCryptoException;
import eu.domibus.ext.rest.util.LegacyRestUtil;
import eu.domibus.ext.rest.util.RestUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.web.rest.DomibusHttpSessionStrategy;
import eu.domibus.web.rest.TruststoreResourceIT;
import org.hibernate.loader.plan.build.internal.LoadGraphLoadPlanBuildingStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author François Gautier
 * @since 5.0
 *
 * Pay attention with using gateway_truststore.jks because it will be overridden by 'uploadTruststore'
 */
public class TrustStoreExtResourceIT extends TrustStoreExtResourceBaseIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TrustStoreExtResourceIT.class);

    public static final String TEST_ENDPOINT_RESOURCE = "/ext/truststore";
    public static final String TEST_ENDPOINT_DOWNLOAD = TEST_ENDPOINT_RESOURCE + "/download";
    public static final String TEST_ENDPOINT_ADD = TEST_ENDPOINT_RESOURCE + "/entries";
    public static final String TEST_ENDPOINT_ADD_WITH_SECURITY_PROFILES = TEST_ENDPOINT_RESOURCE + "/certificates";
    public static final String TEST_ENDPOINT_DELETE = TEST_ENDPOINT_RESOURCE + "/entries/{alias}";
    public static final String TEST_ENDPOINT_DELETE_WITH_SECURITY_PROFILES = TEST_ENDPOINT_RESOURCE + "/certificates/{partyName:.+}";

    @Autowired
    private TruststoreExtResource truststoreExtResource;

    @BeforeEach
    public void before() throws Exception {
        super.setUp(DomibusHttpSessionStrategy.SHARED_SESSION);
        restUtil = new RestUtil(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD, mockMvc);
        legacyRestUtil = new LegacyRestUtil(TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD, mockMvc);
    }

    @Test
    @Transactional
    public void uploadTrustStore() throws Exception {
        //given
        uploadTrustStore("keystores/default.jks", "default.jks", TEST_ENDPOINT_RESOURCE);

        //when
        MvcResult result = uploadTrustStore("keystores/gateway_truststore2.jks", "gateway_truststore2.jks", TEST_ENDPOINT_RESOURCE);

        //then
        String content = result.getResponse().getContentAsString();
        Assertions.assertEquals("\"Truststore file has been successfully replaced.\"", content);
    }

    @Test
    @Transactional
    public void downloadTrustStore() throws Exception {
        //given
        uploadTrustStore("keystores/default.jks", "default.jks", TEST_ENDPOINT_RESOURCE);

        //when
        MvcResult result = downloadTrustStore(TEST_ENDPOINT_DOWNLOAD);

        // then
        String content = result.getResponse().getContentAsString();
        Assertions.assertNotNull(content);
    }

    @Test
    @Transactional
    public void add() throws Exception {
        //given
        uploadTrustStore("keystores/gateway_truststore2.jks", "gateway_truststore2.jks", TEST_ENDPOINT_RESOURCE);

        //when
        MvcResult result = legacyRestUtil.addCertificateToStore(TEST_ENDPOINT_ADD);

        //then
        String content = result.getResponse().getContentAsString();
        Assertions.assertEquals("\"Certificate [red_gw] has been successfully added to the truststore.\"", content);
    }

    @Test
    @Transactional
    public void addWithSecurityProfiles() throws Exception {
        //given
        uploadTrustStore("keystores/gateway_truststore2.jks", "gateway_truststore2.jks", TEST_ENDPOINT_RESOURCE);

        //when
        MvcResult result = restUtil.addCertificateToStore(TEST_ENDPOINT_ADD_WITH_SECURITY_PROFILES);

        //then
        String content = result.getResponse().getContentAsString();
        Assertions.assertEquals("\"Certificate [red_gw] has been successfully added to the truststore.\"", content);
    }

    @Test
    @Transactional
    public void delete() throws Exception {
        //given
        uploadTrustStore("keystores/default.jks", "default.jks", TEST_ENDPOINT_RESOURCE);

        //when
        MvcResult result = legacyRestUtil.deleteCertificateFromStore(TEST_ENDPOINT_DELETE);

        //then
        String content = result.getResponse().getContentAsString();
        Assertions.assertEquals("\"Certificate [blue_gw] has been successfully removed from the truststore.\"", content);
    }

    @Test
    @Transactional
    public void deleteWithSecurityProfiles() throws Exception {
        //given
        uploadTrustStore("keystores/default.jks", "default.jks", TEST_ENDPOINT_RESOURCE);

        try {
            restUtil.addCertificateToStore(TEST_ENDPOINT_ADD_WITH_SECURITY_PROFILES);
        } catch (SameResourceCryptoException e) {
            LOG.debug("Certificate already exists in truststore", e);
        }


        //when
        MvcResult result = restUtil.deleteCertificateFromStore(TEST_ENDPOINT_DELETE_WITH_SECURITY_PROFILES);

        //then
        String content = result.getResponse().getContentAsString();
        Assertions.assertEquals("\"Certificate [red_gw] has been successfully removed from the truststore.\"", content);
    }
}
