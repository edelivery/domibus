package eu.domibus.backendConnector;

import eu.domibus.ext.services.DomibusPropertyManagerExt;
import eu.domibus.plugin.Submission;
import eu.domibus.plugin.transformer.MessageSubmissionTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.domibus.backendConnector.TestFSPluginPropertyManager.TEST_FSPLUGIN_DOMAIN_ENABLED;

/**
 * @author Ion perpegel
 * @since 5.2
 */
public class TestFSPluginMock extends BackendConnectorBaseMock {

    public static final String TEST_FS_PLUGIN = "fsPlugin";

    TestFSPluginPropertyManager testPluginPropertyManager;

    public TestFSPluginMock(TestFSPluginPropertyManager testPluginPropertyManager) {
        super(TEST_FS_PLUGIN);
        this.testPluginPropertyManager = testPluginPropertyManager;
    }

    @Override
    public boolean isEnabled(final String domainCode) {
        return doIsEnabled(domainCode);
    }

    @Override
    public String getDomainEnabledPropertyName() {
        return TEST_FSPLUGIN_DOMAIN_ENABLED;
    }

    @Override
    public DomibusPropertyManagerExt getPropertyManager() {
        return testPluginPropertyManager;
    }

    @Override
    public void setEnabled(final String domainCode, final boolean enabled) {
        doSetEnabled(domainCode, enabled);
    }

}
