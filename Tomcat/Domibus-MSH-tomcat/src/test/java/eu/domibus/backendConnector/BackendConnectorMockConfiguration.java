package eu.domibus.backendConnector;


import eu.domibus.test.common.BackendConnectorMock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BackendConnectorMockConfiguration {

    @Bean
    public BackendConnectorMock backendConnectorMock() {
        return new BackendConnectorMock(BackendConnectorMock.BACKEND_CONNECTOR_NAME);
    }
}
