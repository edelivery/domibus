package eu.domibus.property;

/**
 * @author Ionut Breaz
 * @since 5.1.5
 */

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.multitenancy.DomainTaskException;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import eu.domibus.core.property.DomibusPropertyValidatorService;
import eu.domibus.api.property.DomibusPropertyProvider;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

public class DomibusPropertyValidatorServiceIT extends AbstractTomcatTestIT {

    @Autowired
    DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    DomibusPropertyValidatorService domibusPropertyValidatorService;

    @Autowired
    DomibusTaskExecutor domibusTaskExecutor;


    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusPropertyValidatorServiceIT.class);

    @Test
    public void testDomibusPropertyExceptionIsRaised() {
        DomainTaskException exception = Assertions.assertThrows(DomainTaskException.class,
                () -> callPasswordPropertiesValidation(true));

        String underlyingExceptionMessage = ExceptionUtils.getRootCauseMessage(exception.getCause());
        Assertions.assertTrue(StringUtils.contains(underlyingExceptionMessage, "all password properties must match"),
                "Actual exception message was: [" + underlyingExceptionMessage + "]");
    }

    @Test
    public void testDomibusPropertyExceptionIsNotRaised() {
         callPasswordPropertiesValidation(false);
    }

    private void callPasswordPropertiesValidation(boolean enforcePasswordPolicy) {
        domibusTaskExecutor.submit(() -> {
            String enforcePreviousPropValue = domibusPropertyProvider.getProperty(DOMIBUS_PROPERTIES_PASSWORD_POLICY_ENFORCE);

            try {
                domibusPropertyProvider.setProperty(DOMIBUS_PROPERTIES_PASSWORD_POLICY_ENFORCE, String.valueOf(enforcePasswordPolicy));
                domibusPropertyValidatorService.validatePropertiesPasswordPolicy();
            }
            finally {
                domibusPropertyProvider.setProperty(DOMIBUS_PROPERTIES_PASSWORD_POLICY_ENFORCE, enforcePreviousPropValue);
            }
        });
    }
}
