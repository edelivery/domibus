package eu.domibus.core.cache.distributed;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.cache.distributed.DistributedCacheService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class DistributedCacheServiceNonClusterTestIT extends AbstractTomcatTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DistributedCacheServiceNonClusterTestIT.class);
    public static final String EXISTING_LOCAL_CACHE = "userDomain";

    @Autowired
    DistributedCacheService distributedCacheService;

    @Test
    public void testCreateCache() {
        //check that in a non cluster deployment, the create cache operations are not failing
        distributedCacheService.createCache("mycache1");
        distributedCacheService.createCache("mycache2", 1, 2, 3);
        distributedCacheService.createCache("mycache3", 1, 1, 1, 1, 1, 1);
    }

    @Test
    public void evictEntryFromCache() {
        final String myKey = "mykey";
        final String myValue = "myvalue";
        distributedCacheService.addEntryInCache(EXISTING_LOCAL_CACHE, myKey, myValue);

        assertEquals(myValue, distributedCacheService.getEntryFromCache(EXISTING_LOCAL_CACHE, myKey));
        distributedCacheService.evictEntryFromCache(EXISTING_LOCAL_CACHE, myKey);
        assertNull(distributedCacheService.getEntryFromCache(EXISTING_LOCAL_CACHE, myKey));
    }

    @Test
    public void getCacheNames() {
        final List<String> distributedCacheNames = distributedCacheService.getDistributedCacheNames();
        assertNotNull(distributedCacheNames);
    }

    @Test
    public void getEntriesFromCache() {
        distributedCacheService.createCache(EXISTING_LOCAL_CACHE);
        final Map<String, Object> existingEntries = distributedCacheService.getEntriesFromCache(EXISTING_LOCAL_CACHE);
        for (String cacheKey : existingEntries.keySet()) {
            distributedCacheService.evictEntryFromCache(EXISTING_LOCAL_CACHE, cacheKey);
        }

        distributedCacheService.addEntryInCache(EXISTING_LOCAL_CACHE, "key11", "value11");
        final Map<String, Object> entriesFromCache = distributedCacheService.getEntriesFromCache(EXISTING_LOCAL_CACHE);
        assertNotNull(entriesFromCache);
        assertEquals(1, entriesFromCache.size());
        assertEquals("value11", entriesFromCache.get("key11"));
    }
}
