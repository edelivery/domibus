package eu.domibus.core.pmode.provider.dynamicdiscovery;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.util.HttpUtil;
import eu.domibus.ext.services.CacheExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.dynamicdiscovery.DynamicDiscovery;
import eu.europa.ec.dynamicdiscovery.exception.ConnectionException;
import eu.europa.ec.dynamicdiscovery.model.identifiers.SMPDocumentIdentifier;
import eu.europa.ec.dynamicdiscovery.model.identifiers.SMPParticipantIdentifier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Objects;

/**
 * @author Ionut Breaz
 * @since 5.2
 */
@WireMockTest(httpsEnabled = true)
class DynamicDiscoveryServiceDefaultWireMockServerIT extends AbstractTomcatTestIT {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DynamicDiscoveryServiceDefaultWireMockServerIT.class);

    @Autowired
    private CacheExtService cacheExtService;

    @Autowired
    HttpUtil httpUtil;

    @Autowired
    DynamicDiscoveryServiceRealInstance dynamicDiscoveryServiceRealInstance;


    @Test
    void testFetchFromAnUntrustedSMPServer(WireMockRuntimeInfo wmRuntimeInfo) {
        cacheExtService.evictCaches();

        String mockedSmpUrl = "https://localhost:" + wmRuntimeInfo.getHttpsPort();

        String previousSmpUrl = domibusPropertyProvider.getProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_SMP_URL);
        try {
            domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_SMP_URL, mockedSmpUrl);

            final DynamicDiscovery dynamicDiscoveryClient = dynamicDiscoveryServiceRealInstance.createDynamicDiscoveryClient();
            SMPParticipantIdentifier resourceId = Mockito.mock(SMPParticipantIdentifier.class);
            SMPDocumentIdentifier subresourceId = Mockito.mock(SMPDocumentIdentifier.class);
            ConnectionException connectionException = Assertions.assertThrows(ConnectionException.class,
                    () -> dynamicDiscoveryClient.getService().discoverEndpoint(resourceId, subresourceId, "any", "any", "any"));
            LOG.info("Exception: {}", connectionException.toString());
            Assertions.assertTrue(connectionException.getMessage().contains("SunCertPathBuilderException: unable to find valid certification path to requested target"));
        } finally {
            domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_SMP_URL, Objects.requireNonNullElse(previousSmpUrl, "") );
        }
    }
}
