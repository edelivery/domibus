package eu.domibus.core.util;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.property.DomibusPropertyProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.cert.X509Certificate;

import static eu.domibus.api.multitenancy.DomainService.DEFAULT_DOMAIN;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;

/**
 * @author Ionut Breaz
 * @since 5.2
 */
class TLSMshTrustManagerIT extends AbstractTomcatTestIT {

    @Autowired
    TLSMshTrustManager tlsMshTrustManager;

    @Autowired
    DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    DomibusConfigurationService domibusConfigurationService;

    @Test
    void test() {
        // since other tests are changing these properties, make sure they are set to the expected values
        final String initialTruststoreLocation = domibusPropertyProvider.getProperty(DomainService.DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION);
        final String initialTruststoreType = domibusPropertyProvider.getProperty(DomainService.DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_TYPE);
        final String initialTruststorePassword = domibusPropertyProvider.getProperty(DomainService.DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_PASSWORD);

        try {
            final String configLocation = domibusConfigurationService.getConfigLocation();
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, configLocation + "/domains/default/keystores/tls_msh_truststore.jks");
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_TYPE, "jks");
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_PASSWORD, "test123");


            X509Certificate[] allAcceptedIssuers = tlsMshTrustManager.getAcceptedIssuers();
            Assertions.assertTrue(allAcceptedIssuers.length > 1);


            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_USE_CACERTS, "false");
            X509Certificate[] tlsTruststoreAcceptedIssuers = tlsMshTrustManager.getAcceptedIssuers();
            Assertions.assertEquals(1, tlsTruststoreAcceptedIssuers.length);
        } finally {
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_USE_CACERTS, "true");
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, initialTruststoreLocation);
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_TYPE, initialTruststoreType);
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_PASSWORD, initialTruststorePassword);
        }
    }
}
