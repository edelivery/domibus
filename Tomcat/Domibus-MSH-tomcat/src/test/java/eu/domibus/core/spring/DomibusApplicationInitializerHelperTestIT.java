package eu.domibus.core.spring;

import eu.domibus.AbstractTomcatTestIT;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class DomibusApplicationInitializerHelperTestIT extends AbstractTomcatTestIT {

    @Test
    public void onStartup() throws IOException {
        //we set the custom directory where the Domibus plugins lib and logback files will be copied from the abstract repository
        String javaTemp = DOMIBUS_CONFIG_LOCATION + "/temp";
        FileUtils.forceMkdir(new File(javaTemp));

        System.setProperty("java.io.tmpdir", javaTemp);

        DomibusApplicationInitializerHelper helper = new DomibusApplicationInitializerHelper();
        helper.onStartup();
    }
}
