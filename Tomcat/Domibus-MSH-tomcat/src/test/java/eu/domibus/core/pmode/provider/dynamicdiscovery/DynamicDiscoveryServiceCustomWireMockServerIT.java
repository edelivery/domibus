package eu.domibus.core.pmode.provider.dynamicdiscovery;

import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.util.HttpUtil;
import eu.domibus.ext.services.CacheExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.dynamicdiscovery.DynamicDiscovery;
import eu.europa.ec.dynamicdiscovery.model.identifiers.SMPDocumentIdentifier;
import eu.europa.ec.dynamicdiscovery.model.identifiers.SMPParticipantIdentifier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Objects;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

/**
 * @author Ionut Breaz
 * @since 5.2
 */
@WireMockTest(httpsEnabled = true)
class DynamicDiscoveryServiceCustomWireMockServerIT extends AbstractTomcatTestIT {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DynamicDiscoveryServiceCustomWireMockServerIT.class);
    private static final String STORE_PASSWORD = "test123";

    @Autowired
    private CacheExtService cacheExtService;

    @Autowired
    HttpUtil httpUtil;

    @Autowired
    DynamicDiscoveryServiceRealInstance dynamicDiscoveryServiceRealInstance;

    @RegisterExtension
    static WireMockExtension wireMockExtension = WireMockExtension.newInstance()
            .options(wireMockConfig()
                    .dynamicPort()
                    .dynamicHttpsPort()
                    // set up WireMock with a trusted TLS certificate
                    .keystorePath(DynamicDiscoveryServiceCustomWireMockServerIT.class.getClassLoader().getResource("keystores/wiremock_keystore.jks").getPath())
                    .keystorePassword(STORE_PASSWORD)
                    .keyManagerPassword(STORE_PASSWORD))
            .build();

    @Test
    void testFetchFromATrustedSMPServer() {
        cacheExtService.evictCaches();

        WireMockRuntimeInfo wireMockRuntimeInfo = wireMockExtension.getRuntimeInfo();
        String mockedSmpUrl = "https://localhost:" + wireMockRuntimeInfo.getHttpsPort();

        String previousSmpUrl = domibusPropertyProvider.getProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_SMP_URL);
        try {
            domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_SMP_URL, mockedSmpUrl);

            final DynamicDiscovery dynamicDiscoveryClient = dynamicDiscoveryServiceRealInstance.createDynamicDiscoveryClient();
            SMPParticipantIdentifier resourceId = Mockito.mock(SMPParticipantIdentifier.class);
            SMPDocumentIdentifier subresourceId = Mockito.mock(SMPDocumentIdentifier.class);

            Exception exception = Assertions.assertThrows(Exception.class,
                    () -> dynamicDiscoveryClient.getService().discoverEndpoint(resourceId, subresourceId, "any", "any", "any"));
            LOG.info("Exception: {}", exception.toString());
            // if 404 error was received then the SSL handshake was successful. This is what is important for this test
            Assertions.assertTrue(exception.getMessage().contains("not found - response 404"));
        } finally {
            domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_SMP_URL, Objects.requireNonNullElse(previousSmpUrl, "") );
        }
    }
}
