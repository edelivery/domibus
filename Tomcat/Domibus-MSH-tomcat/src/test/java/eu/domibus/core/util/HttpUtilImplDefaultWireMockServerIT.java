package eu.domibus.core.util;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.util.HttpUtil;
import eu.domibus.ext.services.CacheExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.net.ssl.SSLHandshakeException;

/**
 * @author Ionut Breaz
 * @since 5.2
 */
@WireMockTest(httpsEnabled = true)
class HttpUtilImplDefaultWireMockServerIT extends AbstractTomcatTestIT {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(HttpUtilImplDefaultWireMockServerIT.class);

    @Autowired
    private CacheExtService cacheExtService;

    @Autowired
    HttpUtil httpUtil;

    @Test
    void testDownloadFromAnUntrustedHttpsServer(WireMockRuntimeInfo wmRuntimeInfo) {
        cacheExtService.evictCaches();

        // call the WireMock server using TLSGenericTruststore
        String mockedUrl = "https://localhost:" + wmRuntimeInfo.getHttpsPort();
        SSLHandshakeException sslException = Assertions.assertThrows(SSLHandshakeException.class, () -> httpUtil.downloadURL(mockedUrl));
        LOG.info("SSL Exception: {}", sslException.toString());
        Assertions.assertTrue(sslException.getMessage().contains("PKIX path building failed"));
    }
}
