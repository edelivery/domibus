package eu.domibus.core.pmode.provider.dynamicdiscovery;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.cluster.Command;
import eu.domibus.api.cluster.SignalService;
import eu.domibus.api.dynamicdyscovery.DynamicDiscoveryLookupEntity;
import eu.domibus.api.exceptions.DomibusCoreException;
import eu.domibus.api.jms.JmsMessage;
import eu.domibus.api.model.*;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.party.PartyService;
import eu.domibus.api.pki.MultiDomainCryptoService;
import eu.domibus.api.pmode.PModeService;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.AuthenticationException;
import eu.domibus.api.security.TrustStoreEntry;
import eu.domibus.common.model.configuration.Configuration;
import eu.domibus.common.model.configuration.Party;
import eu.domibus.common.model.configuration.Process;
import eu.domibus.common.model.parties.DynamicParty;
import eu.domibus.core.certificate.CertificateTestUtils;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.sender.MessageSenderErrorHandler;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.jms.JMSManagerImpl;
import eu.domibus.core.message.MessageExchangeConfiguration;
import eu.domibus.core.message.dictionary.NotificationStatusDao;
import eu.domibus.core.party.DynamicPartyDao;
import eu.domibus.core.pmode.multitenancy.MultiDomainPModeProvider;
import eu.domibus.core.pmode.provider.PModeProviderFactoryImpl;
import eu.domibus.core.util.DateUtilImpl;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.ProcessingType;
import eu.domibus.test.common.PKIUtil;
import eu.europa.ec.dynamicdiscovery.DynamicDiscovery;
import eu.europa.ec.dynamicdiscovery.core.locator.IMetadataLocator;
import eu.europa.ec.dynamicdiscovery.core.locator.impl.DefaultBDXRLocator;
import eu.europa.ec.dynamicdiscovery.core.locator.impl.StaticMapMetadataLocator;
import eu.europa.ec.dynamicdiscovery.exception.TechnicalException;
import eu.europa.ec.dynamicdiscovery.model.identifiers.SMPProcessIdentifier;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.util.ReflectionTestUtils;

import javax.jms.Topic;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URI;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;
import static eu.domibus.core.pmode.provider.dynamicdiscovery.DynamicDiscoveryServicePEPPOLConfigurationMockup.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Cosmin Baciu
 * @since 5.1.1
 */
public class DynamicDiscoveryServiceTestIT extends AbstractTomcatTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DynamicDiscoveryServiceTestIT.class);
    public static final String DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION = "dataset/pmode/PModeDynamicDiscovery.xml";
    public static final String DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_ONLY = "dataset/pmode/PModeDynamicDiscoverySignOnly.xml";

    public static final String CERTIFICATE_POLICY_ANY = "2.5.29.32.0";
    public static final String CERTIFICATE_POLICY_QCP_NATURAL = "0.4.0.194112.1.0";
    public static final String CERTIFICATE_POLICY_QCP_LEGAL = "0.4.0.194112.1.1";
    public static final String CERTIFICATE_POLICY_QCP_NATURAL_QSCD = "0.4.0.194112.1.2";
    public static final String CERTIFICATE_POLICY_QCP_LEGAL_QSCD = "0.4.0.194112.1.3";
    public static final String KEYSTORES_GATEWAY_TRUSTSTORE_DYN_DISC_JKS = "keystores/gateway_truststore_dyn_disc.jks";
    public static final String KEYSTORES_GATEWAY_TRUSTSTORE_DYN_DISC_JKS_PASSWORD = "test123";

    @Autowired
    MultiDomainPModeProvider multiDomainPModeProvider;

    @Autowired
    DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    MultiDomainCryptoService multiDomainCertificateProvider;

    @Autowired
    DynamicDiscoveryLookupDao dynamicDiscoveryLookupDao;

    @Autowired
    DynamicDiscoveryLookupService dynamicDiscoveryLookupService;

    @Autowired
    SignalService signalService;

    @Autowired
    DynamicDiscoveryDeleteFinalRecipientsFromCacheCommandTask dynamicDiscoveryDeleteFinalRecipientsFromCacheCommandTask;

    @Autowired
    DynamicDiscoveryDeletePmodePartiesCommandTask dynamicDiscoveryDeletePmodePartiesCommandTask;

    @Autowired
    DynamicDiscoveryAssertionUtil dynamicDiscoveryAssertionUtil;

    @Autowired
    CertificateTestUtils certificateTestUtils;

    //injected mock from DynamicDiscoveryServicePEPPOLConfigurationMockup
    @Autowired
    DynamicDiscoveryServicePEPPOL dynamicDiscoveryService;

    //real instance(no mock)
    @Autowired
    DynamicDiscoveryServiceRealInstance dynamicDiscoveryServiceRealInstance;

    @Autowired
    protected PModeProviderFactoryImpl pModeProviderFactory;

    @Autowired
    protected MSHDispatcher mshDispatcher;

    @Autowired
    @Qualifier("messageSenderErrorHandler")
    protected MessageSenderErrorHandler messageSenderErrorHandler;

    @Autowired
    NotificationStatusDao notificationStatusDao;

    @Autowired
    DynamicPartyDao dynamicPartyDao;

    @Autowired
    PModeService pModeService;

    @Autowired
    PartyService partyService;

//    @Mock
//    private ConsoleAppender<ILoggingEvent> mockAppender;
//
//    @Captor
//    private ArgumentCaptor<ILoggingEvent> logCaptor;

    private void initializePmodeAndProperties(String pmodeFilePath) throws XmlProcessingException, IOException {
        uploadPMode(SERVICE_PORT, pmodeFilePath, null);

        domibusPropertyProvider.setProperty(DOMAIN, DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY, "true");
        setClusteredProperty(true);

        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_CLIENT_SPECIFICATION, DynamicDiscoveryClientSpecification.PEPPOL.getName());
        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_TRANSPORTPROFILEAS_4, "peppol-transport-as4-v2_0");
    }

    @AfterEach
    public void clean() {
        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_CLIENT_SPECIFICATION, DynamicDiscoveryClientSpecification.OASIS.getName());
        domibusPropertyProvider.setProperty(DOMAIN, DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY, "false");
        setClusteredProperty(false);
        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_TRANSPORTPROFILEAS_4, "bdxr-transport-ebms3-as4-v1p0");
    }

    //start tests

    @Test
    public void lookupFinalRecipientRegisteredOnTheSameAccessPointAsTheSender() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        certificateTestUtils.resetTruststore(KEYSTORES_GATEWAY_TRUSTSTORE_DYN_DISC_JKS, KEYSTORES_GATEWAY_TRUSTSTORE_DYN_DISC_JKS_PASSWORD);

        //clean up
        cleanBeforeLookup();

        //get the party identifying our Access Point
        final Party gatewayParty = multiDomainPModeProvider.getGatewayParty();
        final String gatewayPartyName = gatewayParty.getName();
        String expectedGatewayParty = "blue_gw";
        assertEquals(expectedGatewayParty, gatewayPartyName);

        final String finalRecipient1PartyName = DynamicDiscoveryServicePEPPOLConfigurationMockup.participantConfigurations.get(FINAL_RECIPIENT10).getPartyName();
        LOG.info("---first lookup for final recipient [{}] and party [{}]", FINAL_RECIPIENT10, finalRecipient1PartyName);
        //we expect the party blue_gw certificate is not added in the truststore because it already exists
        //we expect the blue_gw is added in the Pmode responder parties
        doLookupForFinalRecipient(FINAL_RECIPIENT10, finalRecipient1PartyName, 2, 1, 1, 1);

        final List<DynamicDiscoveryLookupEntity> dynamicDiscoveryLookupEntries = dynamicDiscoveryLookupDao.findAll();
        assertEquals(1, dynamicDiscoveryLookupEntries.size());
        final DynamicDiscoveryLookupEntity dynamicDiscoveryLookupEntity = dynamicDiscoveryLookupEntries.get(0);
        assertEquals(expectedGatewayParty, dynamicDiscoveryLookupEntity.getPartyName());

        //check that the Access Point party is in the Pmode(was added by dynamic discovery lookup)
        assertPartyIsConfiguredInPmode(expectedGatewayParty);

        //we create a date in the future so that the call below deletes all entries
        Date dateLimit = DateUtils.addHours(new Date(), 1);
        dynamicDiscoveryLookupService.deleteDDCLookupEntriesNotDiscoveredInTheLastPeriod(dateLimit);

        //we expect that the cleanup did not delete the Access Point party from the Pmode
        assertPartyIsConfiguredInPmode(expectedGatewayParty);

    }

    @Test
    public void lookupFinalRecipientRegisteredOnSmp20() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_TRANSPORTPROFILEAS_4, "bdxr-transport-ebms3-as4-v2p0");

        certificateTestUtils.resetTruststore(KEYSTORES_GATEWAY_TRUSTSTORE_DYN_DISC_JKS, KEYSTORES_GATEWAY_TRUSTSTORE_DYN_DISC_JKS_PASSWORD);

        //clean up
        cleanBeforeLookup();

        //get the party identifying our Access Point
        final Party gatewayParty = multiDomainPModeProvider.getGatewayParty();
        final String gatewayPartyName = gatewayParty.getName();
        String expectedGatewayParty = "blue_gw";
        assertEquals(expectedGatewayParty, gatewayPartyName);

        final String finalRecipient1PartyName = DynamicDiscoveryServicePEPPOLConfigurationMockup.participantConfigurations.get(FINAL_RECIPIENT20).getPartyName();
        LOG.info("---first lookup for final recipient [{}] and party [{}]", FINAL_RECIPIENT20, finalRecipient1PartyName);
        //we expect the party blue_gw certificate is not added in the truststore because it already exists
        //we expect the blue_gw is added in the Pmode responder parties
        doLookupForFinalRecipient(FINAL_RECIPIENT20, finalRecipient1PartyName, 3, 2, 1, 1);
    }

    protected void assertPartyIsConfiguredInPmode(String expectedGatewayParty) {
        //assert that the party is in the Pmode parties list
        final Party partyByName = multiDomainPModeProvider.getPartyByName(expectedGatewayParty);
        assertNotNull(partyByName);
        assertEquals(expectedGatewayParty, partyByName.getName());

        //assert that the party is present in the responder parties
        final List<Process> allProcesses = multiDomainPModeProvider.findAllProcesses();
        for (Process allProcess : allProcesses) {
            assertTrue(allProcess.getResponderParties().stream().anyMatch(party -> party.getName().equals(expectedGatewayParty)));
        }
    }

    @Test
    public void lookupAndUpdateConfigurationForPartyToId() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        certificateTestUtils.resetTruststore(KEYSTORES_GATEWAY_TRUSTSTORE_DYN_DISC_JKS, KEYSTORES_GATEWAY_TRUSTSTORE_DYN_DISC_JKS_PASSWORD);

        //clean up
        cleanBeforeLookup();

        final String finalRecipient1PartyName = DynamicDiscoveryServicePEPPOLConfigurationMockup.participantConfigurations.get(FINAL_RECIPIENT1).getPartyName();
        LOG.info("---first lookup for final recipient [{}] and party [{}]", FINAL_RECIPIENT1, finalRecipient1PartyName);
        //we expect the party and the certificate are added
        doLookupForFinalRecipient(FINAL_RECIPIENT1, finalRecipient1PartyName, 3, 2, 1, 1);

        //we expect that one entry is added in the database for the first lookup
        final DynamicDiscoveryLookupEntity finalRecipient1PartyEntityFirstLookup = dynamicDiscoveryLookupDao.findByFinalRecipient(FINAL_RECIPIENT1);
        assertNotNull(finalRecipient1PartyEntityFirstLookup);
        assertEquals(PARTY_NAME1, finalRecipient1PartyEntityFirstLookup.getCn());
        final Date dynamicDiscoveryTimeFirstLookup = finalRecipient1PartyEntityFirstLookup.getDynamicDiscoveryTime();
        assertNotNull(dynamicDiscoveryTimeFirstLookup);
        assertNotNull(finalRecipient1PartyEntityFirstLookup.getFingerprint());
        assertNotNull(finalRecipient1PartyEntityFirstLookup.getSubject());
        assertNotNull(finalRecipient1PartyEntityFirstLookup.getSerial());

        try {
            //sleep 100 milliseconds so that the second DDC lookup time is after the first lookup
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException("Error sleeping thread", e);
        }

        LOG.info("---second lookup for final recipient [{}] and party [{}]", FINAL_RECIPIENT1, finalRecipient1PartyName);
        //we expect that no new certificates are added in the truststore and no new parties are added in the Pmode
        doLookupForFinalRecipient(FINAL_RECIPIENT1, finalRecipient1PartyName, 3, 2, 1, 1);

        //we expect that the DDC lookup time was updated
        final DynamicDiscoveryLookupEntity finalRecipient1PartyEntitySecondLookup = dynamicDiscoveryLookupDao.findByFinalRecipient(FINAL_RECIPIENT1);
        assertNotNull(finalRecipient1PartyEntitySecondLookup);
        assertEquals(PARTY_NAME1, finalRecipient1PartyEntitySecondLookup.getCn());
        final Date dynamicDiscoveryTimeSecondLookup = finalRecipient1PartyEntitySecondLookup.getDynamicDiscoveryTime();
        assertNotNull(dynamicDiscoveryTimeSecondLookup);

        LOG.debug("first lookup [{}], second lookup [{}]", dynamicDiscoveryTimeFirstLookup, dynamicDiscoveryTimeSecondLookup);
        //we expect that the DDC lookup time was updated
        assertTrue(dynamicDiscoveryTimeSecondLookup.compareTo(dynamicDiscoveryTimeFirstLookup) > 0);

        final String finalRecipient2PartyName = DynamicDiscoveryServicePEPPOLConfigurationMockup.participantConfigurations.get(FINAL_RECIPIENT2).getPartyName();
        LOG.info("---first lookup for final recipient [{}] and party [{}]", FINAL_RECIPIENT2, finalRecipient2PartyName);
        //FINAL_RECIPIENT1 is associated with PARTY_NAME1 which was already added in the previous lookup, we expect that no new certificates are added in the truststore and no new parties are added in the PMode
        //we expect the URL for FINAL_RECIPIENT2 is saved in the database
        doLookupForFinalRecipient(FINAL_RECIPIENT2, finalRecipient2PartyName, 3, 2, 1, 2);

        final String finalRecipient3PartyName = DynamicDiscoveryServicePEPPOLConfigurationMockup.participantConfigurations.get(FINAL_RECIPIENT3).getPartyName();
        LOG.info("---first lookup for final recipient [{}] and party [{}]", FINAL_RECIPIENT3, finalRecipient3PartyName);
        //we expect the party and the certificate are added
        //we expect the URL for FINAL_RECIPIENT2 is saved in the database
        doLookupForFinalRecipient(FINAL_RECIPIENT3, finalRecipient3PartyName, 4, 3, 2, 3);
    }

    @Test
    public void lookupAndUpdateConfigurationForToPartyIdWithMultipleThreads() throws XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        //before starting the test we verify the initial setup
        //we have the blue_gw and red_gw already present in the truststore
        dynamicDiscoveryAssertionUtil.verifyNumberOfEntriesInTheTruststore(2);
        //the blue_gw party is present already in the Pmode
        dynamicDiscoveryAssertionUtil.verifyIfPartyIsPresentInTheListOfPartiesFromPmode(1, "blue_gw");

        int numberOfThreads = 10;

        //we create the final recipient name, party name and the party Access Point X509 certificate
        PKIUtil pkiUtil = new PKIUtil();
        for (int index = 0; index < numberOfThreads; index++) {

            final String finalRecipient = String.format(FINAL_RECIPIENT_MULTIPLE_THREADS_FORMAT, index);
            final String partyName = String.format(PARTY_NAME_MULTIPLE_THREADS_FORMAT, index);
            //we create the certificate for the party Access Point associated to the final recipient
            final Long certificateSerialNumber = Long.valueOf(String.format(PARTY_NAME_MULTIPLE_THREADS_CERTIFICATE_SERIAL_NUMBER_FORMAT, index));
            final X509Certificate partyCertificate = pkiUtil.createCertificateWithSubject(BigInteger.valueOf(certificateSerialNumber), "CN=" + partyName + ",OU=Domibus,O=eDelivery,C=EU");

            //we add the configuration so that the final recipient can be lookup up and Endpoint is returned(simulating the lookup in SMP)
            DynamicDiscoveryServicePEPPOLConfigurationMockup.addParticipantConfiguration(
                    finalRecipient,
                    partyName,
                    "peppol-transport-as4-v2_0",
                    new SMPProcessIdentifier("bdx:noprocess", "tc1"),
                    partyCertificate
            );
        }

        LOG.info("Starting [{}] threads", numberOfThreads);
        List<Thread> threads = new ArrayList<>();
        //we start all threads
        for (int index = 0; index < numberOfThreads; index++) {
            final String finalRecipient = String.format(FINAL_RECIPIENT_MULTIPLE_THREADS_FORMAT, index);
            final String partyName = String.format(PARTY_NAME_MULTIPLE_THREADS_FORMAT, index);

            LOG.info("---lookup for final recipient [{}] and party [{}]", finalRecipient, partyName);

            final Thread thread = new Thread(() -> {
                try {
                    final long millisecondsToSleep = RandomUtils.nextLong(100, 500);
                    //simulate the business logic executed
                    LOG.info("Sleeping for [{}] milliseconds", millisecondsToSleep);
                    try {
                        Thread.sleep(millisecondsToSleep);
                    } catch (InterruptedException e) {
                        LOG.error("Error sleeping");
                        throw new RuntimeException(e);
                    }
                    //perform lookup
                    doLookupForFinalRecipient(finalRecipient);
                } catch (EbMS3Exception e) {
                    LOG.error("Error while looking up for final recipient [{}]", finalRecipient);
                    throw new RuntimeException(e);
                }
            });
            thread.start();
            threads.add(thread);
        }

        LOG.info("Waiting for threads to finish");
        //we wait for all threads to finish
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                LOG.error("Error joining thread [{}]", thread);
                throw new RuntimeException(e);
            }
        }

        //we expect 12 certificates in the truststore: we have 2 initial certificates in the truststore(blue_gw and red_gw) + we do 10 unique lookups
        dynamicDiscoveryAssertionUtil.verifyNumberOfEntriesInTheTruststore(2 + numberOfThreads);

        for (int index = 0; index < numberOfThreads; index++) {
            final String finalRecipient = String.format(FINAL_RECIPIENT_MULTIPLE_THREADS_FORMAT, index);
            final String partyName = String.format(PARTY_NAME_MULTIPLE_THREADS_FORMAT, index);

            //we expect 21 parties in the Pmode list(equal to the number of unique lookups + 1(the initial blue_gw party)) and 20 in the responder parties for the unique lookups
            dynamicDiscoveryAssertionUtil.verifyIfPartyIsPresentInTheListOfPartiesFromPmode(numberOfThreads + 1, partyName);
            dynamicDiscoveryAssertionUtil.verifyIfPartyIsPresentInTheResponderPartiesForAllProcesses(numberOfThreads, partyName);

            //we verify if the final recipient was added in the database
            dynamicDiscoveryAssertionUtil.verifyThatDynamicDiscoveryLookupWasAddedInTheDatabase(numberOfThreads, finalRecipient, partyName);
        }
    }

    @Test
    public void dynamicDiscoveryInSMPTriggeredOnce_secondTimeConfigurationIsLoadedFromCache() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

//        setUpLoggetForTesting();

        //clean up
        cleanBeforeLookup();

        final String finalRecipient1 = FINAL_RECIPIENT1;
        final String finalRecipient1PartyName = DynamicDiscoveryServicePEPPOLConfigurationMockup.participantConfigurations.get(finalRecipient1).getPartyName();
        final UserMessage userMessage = buildUserMessage(finalRecipient1);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //id adds in the PMode the discovered party and it adds in the truststore the certificate of the discovered party
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        final DynamicDiscoveryLookupEntity dynamicDiscoveryLookupEntity = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient1);
        assertNotNull(dynamicDiscoveryLookupEntity);
        final Date firstDynamicDiscoveryTime = dynamicDiscoveryLookupEntity.getDynamicDiscoveryTime();

        try {
            //sleep 100 milliseconds so that the second DDC lookup time should be after the first lookup
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException("Error sleeping thread", e);
        }

        //it should take the responder party details from the cache; to verify if dynamic discovery lookup in SMP is triggered again, we check the dynamic discovery time of the final recipient entity; if the dynamic discovery time is not changed,
        //it means that the SMP lookup was not performed; as a reminder, each time an SMP lookup is done for a final recipient, we update the dynamic discovery time of the final recipient entity)
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        //verify that the dynamic discovery was not triggered again
        dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient1).getDynamicDiscoveryTime();

        final Date dynamicDiscoveryTimeLatest = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient1).getDynamicDiscoveryTime();
        assertEquals(firstDynamicDiscoveryTime, dynamicDiscoveryTimeLatest);

//        Mockito.verify(mockAppender, Mockito.atLeastOnce()).doAppend(logCaptor.capture());
//        List<ILoggingEvent> loggingEvents = logCaptor.getAllValues();
//        assertTrue(loggingEvents.stream().anyMatch(event -> event.getMessage().contains("Checking public certificate for dynamic receiver party")));
    }

    @Test
    public void dynamicDiscoveryInSMPTriggeredOnce_clearPmode_secondTimeDynamicDiscoveryIsTriggeredAgain() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        final String finalRecipient1 = FINAL_RECIPIENT1;
        final String finalRecipient1PartyName = DynamicDiscoveryServicePEPPOLConfigurationMockup.participantConfigurations.get(finalRecipient1).getPartyName();
        final UserMessage userMessage = buildUserMessage(finalRecipient1);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //id adds in the PMode the discovered party and it adds in the truststore the certificate of the discovered party
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        final DynamicDiscoveryLookupEntity dynamicDiscoveryLookupEntity = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient1);
        assertNotNull(dynamicDiscoveryLookupEntity);
        final Date firstDynamicDiscoveryTime = dynamicDiscoveryLookupEntity.getDynamicDiscoveryTime();

        //refresh the PMode so that the receiver party is removed from the PMode cache memory
        refreshPmode();

        try {
            //sleep 100 milliseconds so that the second DDC lookup time is after the first lookup
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException("Error sleeping thread", e);
        }

        //it should not trigger a dynamic discovery because the receiver party is not missing from the Pmode this time as it was persistet in the db the first time it was found;
        // to verify if dynamic discovery lookup in SMP is not triggered again, we check the dynamic discovery time of the final recipient entity; if the dynamic discovery time is changed,
        //it means that the SMP lookup was performed again; as a reminder, each time an SMP lookup is done for a final recipient, we update the dynamic discovery time of the final recipient entity)
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        //verify that the dynamic discovery was triggered again
        final Date dynamicDiscoveryTimeLatest = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient1).getDynamicDiscoveryTime();
        assertEquals(firstDynamicDiscoveryTime, dynamicDiscoveryTimeLatest);
    }

    @Test
    public void dynamicDiscoveryInSMPTriggeredOnce_clearTruststore_secondTimeDynamicDiscoveryIsTriggeredAgain() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        final String finalRecipient1 = FINAL_RECIPIENT1;
        final String finalRecipient1PartyName = DynamicDiscoveryServicePEPPOLConfigurationMockup.participantConfigurations.get(finalRecipient1).getPartyName();
        final UserMessage userMessage = buildUserMessage(finalRecipient1);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //id adds in the PMode the discovered party and it adds in the truststore the certificate of the discovered party
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        final DynamicDiscoveryLookupEntity dynamicDiscoveryLookupEntity = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient1);
        assertNotNull(dynamicDiscoveryLookupEntity);
        final Date firstDynamicDiscoveryTime = dynamicDiscoveryLookupEntity.getDynamicDiscoveryTime();

        //clean the truststore so that the receiver party public certificate is removed from the truststore
        cleanTruststore();

        try {
            //sleep 100 milliseconds so that the second DDC lookup time is after the first lookup
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException("Error sleeping thread", e);
        }

        //it should trigger a dynamic discovery because the receiver party public certificate is missing from the truststore; to verify if dynamic discovery lookup in SMP is triggered again, we check the dynamic discovery time of the final recipient entity; if the dynamic discovery time is changed,
        //it means that the SMP lookup was performed again; as a reminder, each time an SMP lookup is done for a final recipient, we update the dynamic discovery time of the final recipient entity)
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        //verify that the dynamic discovery was triggered again
        final Date dynamicDiscoveryTimeLatest = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient1).getDynamicDiscoveryTime();
        assertNotEquals(firstDynamicDiscoveryTime, dynamicDiscoveryTimeLatest);
    }

    @Test
    public void c1SubmitsMessageOnServer1_messageSendingFromC2ToC3IsDoneOnServer2() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        final UserMessage userMessage = buildUserMessage(FINAL_RECIPIENT1);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //id adds in the PMode the discovered party and it adds in the truststore the certificate of the discovered party
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        //we simulate the following scenario
        //- message is submitted on server 1 and the dynamic discovery is triggered at submission time: Pmode and truststore are updated normally
        //- message is sent from server 2 via JMS listener; in this case the receiver party is not in the Pmode memory
        //we clean the Pmode memory
        refreshPmode();

        //it should trigger a lookup in SMP and the PMode context should be retrieved successfully
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));
    }

    @Test
    public void c1SubmitsMessageWithPartyConfiguredInPmodeAndSignOnlyPolicy() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_ONLY);

        try {
            domibusPropertyProvider.setProperty(DOMAIN, DOMIBUS_SECURITY_ENCRYPTION_ENABLED, "false");

            //clean up
            cleanBeforeLookup();

            final UserMessage userMessage = buildUserMessage(FINAL_RECIPIENT1);
            //we change the action to match the process tc2Process
            userMessage.getAction().setValue("TC1Leg2");

            //we set the party identifier for the party already configured in the Pmode
            final To toParty = userMessage.getPartyInfo().getTo();
            final PartyId fromPartyId = new PartyId();
            fromPartyId.setValue(PARTY_NAME5);
            fromPartyId.setType("urn:fdc:peppol.eu:2017:identifiers:ap");
            final PartyRole partyRole = new PartyRole();
            partyRole.setValue("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder");
            toParty.setToRole(partyRole);
            toParty.setToPartyId(fromPartyId);

            //no dynamic discovery should be triggered and no check of the public certificate is done; exception is not raised
            final MessageExchangeConfiguration userMessageExchangeContext = multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH);
            assertNotNull(userMessageExchangeContext);

        } finally {
            domibusPropertyProvider.setProperty(DOMAIN, DOMIBUS_SECURITY_ENCRYPTION_ENABLED, "true");
        }
    }

    @Test
    public void c1SubmitsMessageToPartyWithExpiredCertificate() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        final UserMessage userMessage = buildUserMessage(FINAL_RECIPIENT4);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //it throws an exception because the discovered certificate is expired
        Assertions.assertThrows(AuthenticationException.class, () -> {
            multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH);
        });

    }

    @Test
    public void deleteDDCCertificatesNotDiscoveredInTheLastPeriod() throws EbMS3Exception, KeyStoreException, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);
        //uncomment to access the H2 console in the browser; JDBC URL is jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=false
        //Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082").start();

        //clean up
        cleanBeforeLookup();

        //we check the number of parties in the Pmode before running the test
        dynamicDiscoveryAssertionUtil.verifyListOfPartiesFromPmodeSize(1);

        //we check the number of parties in the truststore before running the test
        dynamicDiscoveryAssertionUtil.verifyNumberOfEntriesInTheTruststore(2);

        final String finalRecipient1 = FINAL_RECIPIENT1;//party1
        final String finalRecipient2 = FINAL_RECIPIENT2;//party1
        final String finalRecipient3 = FINAL_RECIPIENT3;//party2
        //we skip finalRecipient4 because it's configured with party3 which has an expired certificate
        final String finalRecipient5 = FINAL_RECIPIENT5;//party4
        String partyName1 = PARTY_NAME1;
        String partyName2 = PARTY_NAME2;
        String partyName4 = PARTY_NAME4;

        //---finalRecipient1, party1, certificate1

        //we expect the party and the certificate are added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient1, partyName1, 3, 2, 1, 1);

        //we set the DDC time for the finalRecipient1 to 25h ago
        Date ddcTimeFinalRecipient1 = DateUtils.addHours(new Date(), 25 * -1);
        setDynamicDiscoveryTime(finalRecipient1, ddcTimeFinalRecipient1);

        //---finalRecipient2, party1, certificate1
        //we expect the party and the certificate are not added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient2, partyName1, 3, 2, 1, 2);
        //we set the DDC time for the finalRecipient2 to 2h ago
        Date ddcTimeFinalRecipient2 = DateUtils.addHours(new Date(), 2 * -1);
        setDynamicDiscoveryTime(finalRecipient2, ddcTimeFinalRecipient2);

        //finalRecipient3, party2, certificate2
        //we expect the party and the certificate are added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient3, partyName2, 4, 3, 2, 3);
        //we set the DDC time for the finalRecipient3 to 4h ago
        Date ddcTimeFinalRecipient3 = DateUtils.addHours(new Date(), 4 * -1);
        setDynamicDiscoveryTime(finalRecipient3, ddcTimeFinalRecipient3);

        //finalRecipient5, party4, certificate4
        //we expect the party and the certificate are added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient5, partyName4, 5, 4, 3, 4);
        //we keep the final recipient5 discovery time to the current time

        //we expect 5 entries in the truststore and 4 entries the Pmode
        dynamicDiscoveryAssertionUtil.verifyListOfPartiesFromPmodeSize(4);
        dynamicDiscoveryAssertionUtil.verifyNumberOfEntriesInTheTruststore(5);

        //we set the retention to 3h and start clean up
        dynamicDiscoveryLookupService.deleteDDCLookupEntriesNotDiscoveredInTheLastPeriod(3);

        //start assertions

        //we expect 4 entries in the truststore and 3 entries the Pmode: party 2 was deleted from the Pmode and certificate 2 deleted from the truststore
        dynamicDiscoveryAssertionUtil.verifyListOfPartiesFromPmodeSize(3);
        dynamicDiscoveryAssertionUtil.verifyNumberOfEntriesInTheTruststore(4);

        //we expect that the entry for finalRecipient1 was deleted from the database; DDC time is old
        DynamicDiscoveryLookupEntity lookupEntryFinalRecipient1AfterJobRan = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient1);
        Assertions.assertNull(lookupEntryFinalRecipient1AfterJobRan);
        //we expect that the certificate1 is still present in the truststore; it's still used by finalRecipient2
        assertNotNull(multiDomainCertificateProvider.getCertificateFromTruststore(DOMAIN, partyName1));
        //we expect that the party1 is still present in the PMode responder parties; it's still used by finalRecipient2
        assertTrue(isPartyPresentInTheResponderParties(partyName1));
        //we expect that the party1 is still present in the PMode parties list
        assertNotNull(pModeProvider.getPartyByName(partyName1));

        DynamicDiscoveryLookupEntity lookupEntryFinalRecipient2AfterJobRan = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient2);
        assertNotNull(lookupEntryFinalRecipient2AfterJobRan);


        //we expect that the entry for finalRecipient3 was deleted from the database; DDC time is old
        DynamicDiscoveryLookupEntity lookupEntryFinalRecipient3AfterJobRan = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient3);
        Assertions.assertNull(lookupEntryFinalRecipient3AfterJobRan);
        //we expect that the certificate2 was deleted from the truststore
        Assertions.assertNull(multiDomainCertificateProvider.getCertificateFromTruststore(DOMAIN, partyName2));
        //we expect that the party2 was deleted from the PMode responder parties
        Assertions.assertFalse(isPartyPresentInTheResponderParties(partyName2));
        //we expect that the party2 was deleted from the PMode parties list
        Assertions.assertNull(pModeProvider.getPartyByName(partyName2));

        //we expect that the entry for finalRecipient5 is still present int the database; DDC time is new
        DynamicDiscoveryLookupEntity lookupEntryFinalRecipient5AfterJobRan = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient5);
        assertNotNull(lookupEntryFinalRecipient5AfterJobRan);
        //we expect that the certificate4 is still present in the truststore
        assertNotNull(multiDomainCertificateProvider.getCertificateFromTruststore(DOMAIN, partyName4));
        //we expect that the partyName4 is still present in the PMode responder parties
        assertTrue(isPartyPresentInTheResponderParties(partyName4));
    }

    @Test
    public void testSignalForPartyAndFinalRecipientDeletion() throws EbMS3Exception, XmlProcessingException, IOException {
        Object saveField = ReflectionTestUtils.getField(signalService, "jmsManager");

        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        final String finalRecipient1 = FINAL_RECIPIENT1;//party1
        String partyName1 = PARTY_NAME1;

        dynamicDiscoveryAssertionUtil.verifyListOfPartiesFromPmodeSize(1);

        //---finalRecipient1, party1, certificate1
        //we expect the party and the certificate are added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient1, partyName1, 3, 2, 1, 1);
        //we set the DDC time for the finalRecipient1 to 25h ago
        Date ddcTimeFinalRecipient1 = DateUtils.addHours(new Date(), 25 * -1);
        setDynamicDiscoveryTime(finalRecipient1, ddcTimeFinalRecipient1);

        setClusteredProperty(true);
        try {
            //we verify that the URL for final recipient is present in the cache
            assertTrue(dynamicDiscoveryLookupService.getFinalRecipientAccessPointUrls().containsKey(finalRecipient1));
            //we simulate the signal to the other servers

            ReflectionTestUtils.setField(signalService, "jmsManager", new JMSManagerImpl() {
                @Override
                public void sendMessageToTopic(JmsMessage message, Topic destination, boolean excludeOrigin) {
                    LOG.info("Jms override [{}] [{}]", message, destination);

                    final String commandName = message.getProperties().get(Command.COMMAND);
                    if (dynamicDiscoveryDeletePmodePartiesCommandTask.canHandle(commandName)) {
                        dynamicDiscoveryDeletePmodePartiesCommandTask.execute(message.getProperties());
                    }
                    if (dynamicDiscoveryDeleteFinalRecipientsFromCacheCommandTask.canHandle(commandName)) {
                        dynamicDiscoveryDeleteFinalRecipientsFromCacheCommandTask.execute(message.getProperties());
                    }
                }
            });
            signalService.signalDeleteFinalRecipientCache(Arrays.asList(finalRecipient1));
            //we verify that the URL for final recipient was removed from the cache
            assertFalse(dynamicDiscoveryLookupService.getFinalRecipientAccessPointUrls().containsKey(finalRecipient1));

            //we verify that the party is present in the PMode party list
            assertNotNull(pModeProvider.getPartyByName(partyName1));
            signalService.signalDeletePmodeParties(Arrays.asList(partyName1));
            assertNull(pModeProvider.getPartyByName(partyName1));

            //check that there are no processes which contain the party name in the responder parties
            assertFalse(isPartyPresentInTheResponderParties(partyName1));
        } finally {
            setClusteredProperty(false);
            ReflectionTestUtils.setField(signalService, "jmsManager", saveField);
        }
    }

    protected boolean isPartyPresentInTheResponderParties(String partyName) {

        final Collection<Process> dynamicSenderProcesses = ((DynamicDiscoveryPModeProvider) multiDomainPModeProvider.getCurrentPModeProvider()).getDynamicProcesses(MSHRole.SENDING);
        return dynamicSenderProcesses.stream().filter(process -> {
            final Set<Party> responderParties = process.getResponderParties();
            return responderParties.stream().filter(party -> party.getName().equals(partyName)).count() > 0;
        }).count() > 0;
    }

    protected void setDynamicDiscoveryTime(String finalRecipient, Date date) {
        final DynamicDiscoveryLookupEntity finalRecipient1Entity = dynamicDiscoveryLookupDao.findByFinalRecipient(finalRecipient);
        finalRecipient1Entity.setDynamicDiscoveryTime(date);
        dynamicDiscoveryLookupDao.createOrUpdate(finalRecipient1Entity);
    }


    private void cleanBeforeLookup() throws IOException {
        cleanTruststore();
        deleteAllFinalRecipients();
        refreshPmode();
    }

    private void refreshPmode() {
        multiDomainPModeProvider.refresh();
    }

    private void cleanTruststore() throws IOException {
        certificateTestUtils.resetTruststore("keystores/gateway_truststore_dyn_disc.jks", "test123");
    }

    private void doLookupForFinalRecipient(String finalRecipient) throws EbMS3Exception {
        domainContextProvider.setCurrentDomain(DOMAIN);
        final UserMessage userMessage = buildUserMessage(finalRecipient);
        final DynamicDiscoveryPModeProvider dynamicDiscoveryPModeProvider = (DynamicDiscoveryPModeProvider) multiDomainPModeProvider.getCurrentPModeProvider();
        final eu.domibus.common.model.configuration.Configuration initialPModeConfiguration = dynamicDiscoveryPModeProvider.getConfiguration();
        final Collection<Process> pmodeProcesses = initialPModeConfiguration.getBusinessProcesses().getProcesses();

        //do the lookup based on final recipient
        dynamicDiscoveryPModeProvider.lookupAndUpdateConfigurationForToPartyId(getFinalRecipientCacheKey(finalRecipient), userMessage);
    }

    private void doLookupForFinalRecipient(String finalRecipient,
                                           String partyNameAccessPointForFinalRecipient,
                                           int expectedTruststoreEntriesAfterLookup,
                                           int expectedPmodePartiesAfterLookup,
                                           int expectedResponderPartiesAfterLookup,
                                           int expectedDDCLookupEntriesInDatabase) throws EbMS3Exception {
        final UserMessage userMessage = buildUserMessage(finalRecipient);
        final DynamicDiscoveryPModeProvider dynamicDiscoveryPModeProvider = (DynamicDiscoveryPModeProvider) multiDomainPModeProvider.getCurrentPModeProvider();
        final eu.domibus.common.model.configuration.Configuration initialPModeConfiguration = dynamicDiscoveryPModeProvider.getConfiguration();
        final Collection<Process> pmodeProcesses = initialPModeConfiguration.getBusinessProcesses().getProcesses();

        //do the lookup based on final recipient
        dynamicDiscoveryPModeProvider.lookupAndUpdateConfigurationForToPartyId(getFinalRecipientCacheKey(finalRecipient), userMessage);

        //verify that the party to was added in the UserMessage
        final String toParty = userMessage.getPartyInfo().getToParty();
        assertEquals(partyNameAccessPointForFinalRecipient, toParty);

        //verify that the party certificate was added in the truststore
        dynamicDiscoveryAssertionUtil.verifyNumberOfEntriesInTheTruststore(expectedTruststoreEntriesAfterLookup);

        //verify that the party was added in the list of available parties in the Pmode
        dynamicDiscoveryAssertionUtil.verifyIfPartyIsPresentInTheListOfPartiesFromPmode(expectedPmodePartiesAfterLookup, partyNameAccessPointForFinalRecipient);

        //verify that the party was added in the responder parties for all process
        dynamicDiscoveryAssertionUtil.verifyIfPartyIsPresentInTheResponderPartiesForAllProcesses(expectedResponderPartiesAfterLookup, partyNameAccessPointForFinalRecipient);

        //verify that the final recipient was added in the database
        dynamicDiscoveryAssertionUtil.verifyThatDynamicDiscoveryLookupWasAddedInTheDatabase(expectedDDCLookupEntriesInDatabase, finalRecipient, partyNameAccessPointForFinalRecipient);
    }


    private void deleteAllFinalRecipients() {
        //we delete the final recipient entries from the database and we clear the memory cache
        final List<DynamicDiscoveryLookupEntity> allLookupEntities = dynamicDiscoveryLookupDao.findAll();
        dynamicDiscoveryLookupDao.deleteAll(allLookupEntities);
        dynamicDiscoveryLookupService.clearFinalRecipientAccessPointUrlsCache();
    }

    private List<TrustStoreEntry> keepOnlyInitialCertificates(List<String> initialAliasesInTheTruststore) {
        List<TrustStoreEntry> trustStoreEntries = multiDomainCertificateProvider.getTrustStoreEntries(DOMAIN);
        //we delete any other certificates apart from blue and red to have a clean state
        final Collection<TrustStoreEntry> certificatesToBeDeleted = trustStoreEntries.stream().filter(trustStoreEntry -> !initialAliasesInTheTruststore.contains(trustStoreEntry.getName())).collect(Collectors.toList());
        certificatesToBeDeleted.stream().forEach(trustStoreEntry -> multiDomainCertificateProvider.removeCertificate(DOMAIN, trustStoreEntry.getName()));

        //check that we have only the initial certificates
        trustStoreEntries = multiDomainCertificateProvider.getTrustStoreEntries(DOMAIN);
        assertEquals(2, trustStoreEntries.size());

        return trustStoreEntries;
    }


    protected String getFinalRecipientCacheKey(String finalRecipient) {
        return finalRecipient + "cacheKey";
    }

    private UserMessage buildUserMessage(String finalRecipient) {
        UserMessage userMessage = new UserMessage();
        final MpcEntity mpc = new MpcEntity();
        mpc.setValue("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC");
        userMessage.setMpc(mpc);

        Set<MessageProperty> messageProperties = new HashSet<>();
        final MessageProperty finalRecipientProperty = new MessageProperty();
        finalRecipientProperty.setName("finalRecipient");
        finalRecipientProperty.setValue(finalRecipient);
        messageProperties.add(finalRecipientProperty);
        userMessage.setMessageProperties(messageProperties);

        ServiceEntity serviceEntity = new ServiceEntity();
        serviceEntity.setValue("bdx:noprocess");
        serviceEntity.setType("tc1");
        userMessage.setService(serviceEntity);

        final ActionEntity actionEntity = new ActionEntity();
        actionEntity.setValue("TC1Leg1");
        userMessage.setAction(actionEntity);

        final PartyInfo partyInfo = new PartyInfo();
        From from = new From();
        final PartyId fromPartyId = new PartyId();
        fromPartyId.setValue("blue_gw");
        fromPartyId.setType("urn:fdc:peppol.eu:2017:identifiers:ap");
        final PartyRole partyRole = new PartyRole();
        partyRole.setValue("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator");
        from.setFromRole(partyRole);
        from.setFromPartyId(fromPartyId);
        partyInfo.setFrom(from);


        partyInfo.setTo(new To());

        userMessage.setPartyInfo(partyInfo);
        return userMessage;
    }

    @Test
    public void testGetAllowedSMPCertificatePolicyOIDsPropertyNotDefined() throws XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);
        final Domain currentDomain = domainContextProvider.getCurrentDomain();

        final String initialValue = domibusPropertyProvider.getProperty(currentDomain, DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION);

        domibusPropertyProvider.setProperty(currentDomain,
                DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION,
                "");

        assertTrue(CollectionUtils.isEmpty(dynamicDiscoveryService.getAllowedSMPCertificatePolicyOIDs()));

        domibusPropertyProvider.setProperty(currentDomain,
                DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION,
                initialValue);
    }

    @Test
    public void testGetAllowedSMPCertificatePolicyOIDsPropertyWithOne() {
        final Domain currentDomain = domainContextProvider.getCurrentDomain();

        final String initialValue = domibusPropertyProvider.getProperty(currentDomain, DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION);

        domibusPropertyProvider.setProperty(currentDomain,
                DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION,
                CERTIFICATE_POLICY_QCP_NATURAL);

        final List<String> allowedSMPCertificatePolicyOIDs = dynamicDiscoveryService.getAllowedSMPCertificatePolicyOIDs();
        assertEquals(1, allowedSMPCertificatePolicyOIDs.size());
        assertTrue(allowedSMPCertificatePolicyOIDs.contains(CERTIFICATE_POLICY_QCP_NATURAL));

        domibusPropertyProvider.setProperty(currentDomain,
                DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION,
                initialValue);
    }


    @Test
    public void testGetAllowedSMPCertificatePolicyOIDsPropertyWithMultipleAndSpaces() {
        final Domain currentDomain = domainContextProvider.getCurrentDomain();

        final String initialValue = domibusPropertyProvider.getProperty(currentDomain, DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION);

        String newValue = CERTIFICATE_POLICY_QCP_NATURAL
                + "," + CERTIFICATE_POLICY_QCP_LEGAL
                + ", " + CERTIFICATE_POLICY_QCP_NATURAL_QSCD
                + " ,     " + CERTIFICATE_POLICY_QCP_LEGAL_QSCD;
        domibusPropertyProvider.setProperty(currentDomain,
                DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION,
                newValue);

        final List<String> allowedSMPCertificatePolicyOIDs = dynamicDiscoveryService.getAllowedSMPCertificatePolicyOIDs();
        assertEquals(4, allowedSMPCertificatePolicyOIDs.size());
        assertEquals(CERTIFICATE_POLICY_QCP_NATURAL, allowedSMPCertificatePolicyOIDs.get(0));
        assertEquals(CERTIFICATE_POLICY_QCP_LEGAL, allowedSMPCertificatePolicyOIDs.get(1));
        assertEquals(CERTIFICATE_POLICY_QCP_NATURAL_QSCD, allowedSMPCertificatePolicyOIDs.get(2));
        assertEquals(CERTIFICATE_POLICY_QCP_LEGAL_QSCD, allowedSMPCertificatePolicyOIDs.get(3));

        domibusPropertyProvider.setProperty(currentDomain,
                DOMIBUS_DYNAMICDISCOVERY_CLIENT_CERTIFICATE_POLICY_OID_VALIDATION,
                initialValue);
    }

    @Test
    public void testIsValidEndpointForNullPeriod() {
        boolean result = dynamicDiscoveryService.isValidEndpoint(null, null);

        assertTrue(result, "If period is not given the endpoint must be considered as valid!");
    }

    @Test
    public void testIsValidEndpointForValidPeriod() {
        Date currentDate = Calendar.getInstance().getTime();

        final DateUtilImpl dateUtil = new DateUtilImpl();

        final OffsetDateTime startDate = dateUtil.convertDateToOffsetDateTime(DateUtils.addDays(currentDate, -1));
        final OffsetDateTime endDate = dateUtil.convertDateToOffsetDateTime(DateUtils.addDays(currentDate, +1));
        boolean result = dynamicDiscoveryService.isValidEndpoint(startDate, endDate);

        assertTrue(result);
    }

    @Test
    public void testIsValidEndpointForNotYetValid() {
        final DateUtilImpl dateUtil = new DateUtilImpl();
        Date currentDate = Calendar.getInstance().getTime();
        OffsetDateTime fromDate = dateUtil.convertDateToOffsetDateTime(DateUtils.addDays(currentDate, 1));
        OffsetDateTime toDate = dateUtil.convertDateToOffsetDateTime(DateUtils.addDays(currentDate, 2));

        boolean result = dynamicDiscoveryService.isValidEndpoint(fromDate, toDate);
        assertFalse(result);
    }

    @Test
    public void testIsValidEndpointForExpired() {
        final DateUtilImpl dateUtil = new DateUtilImpl();
        Date currentDate = Calendar.getInstance().getTime();
        OffsetDateTime fromDate = dateUtil.convertDateToOffsetDateTime(DateUtils.addDays(currentDate, -2));
        OffsetDateTime toDate = dateUtil.convertDateToOffsetDateTime(DateUtils.addDays(currentDate, -1));

        boolean result = dynamicDiscoveryService.isValidEndpoint(fromDate, toDate);
        assertFalse(result);
    }

    @Test
    public void testSmlZoneEmpty() {
        final Domain currentDomain = domainContextProvider.getCurrentDomain();

        final String initialValue = domibusPropertyProvider.getProperty(currentDomain, DOMIBUS_SMLZONE);

        domibusPropertyProvider.setProperty(currentDomain, DOMIBUS_SMLZONE, "");

        Assertions.assertThrows(DomibusCoreException.class, () -> {
            dynamicDiscoveryService.lookupInformation("domain",
                    "participantId_expired",
                    "participantIdScheme",
                    "scheme::value",
                    "urn:epsosPatientService::List",
                    "ehealth-procid-qns");
        });

        domibusPropertyProvider.setProperty(currentDomain, DOMIBUS_SMLZONE, initialValue);
    }

    @Test
    public void createDynamicDiscoveryClientWithOneSmlZone() {
        final DynamicDiscovery dynamicDiscoveryClient = dynamicDiscoveryServiceRealInstance.createDynamicDiscoveryClient();
        final IMetadataLocator metadataLocator = dynamicDiscoveryClient.getService().getMetadataLocator();
        assertInstanceOf(DefaultBDXRLocator.class, metadataLocator);
        final DefaultBDXRLocator defaultBDXRLocator = (DefaultBDXRLocator) metadataLocator;
        final List<String> topDnsDomains = defaultBDXRLocator.getTopDnsDomains();
        assertTrue(CollectionUtils.isNotEmpty(topDnsDomains));
        assertEquals(1, topDnsDomains.size());
        assertEquals("acc.edelivery.tech.ec.europa.eu", topDnsDomains.get(0));
    }

    @Test
    public void createDynamicDiscoveryClientWithMultipleSmlZones() {
        final String initialSmlZoneValue = domibusPropertyProvider.getProperty(DOMIBUS_SMLZONE);
        String smlZone1 = "zone1.edelivery.tech.ec.europa.eu";
        String smlZone2 = "zone2.edelivery.tech.ec.europa.eu";
        String smlZonePropertyValueWithMultipleSmlZones = smlZone1 + "," + smlZone2;
        domibusPropertyProvider.setProperty(DOMIBUS_SMLZONE, smlZonePropertyValueWithMultipleSmlZones);

        try {
            final DynamicDiscovery dynamicDiscoveryClient = dynamicDiscoveryServiceRealInstance.createDynamicDiscoveryClient();
            final IMetadataLocator metadataLocator = dynamicDiscoveryClient.getService().getMetadataLocator();
            assertInstanceOf(DefaultBDXRLocator.class, metadataLocator);
            final DefaultBDXRLocator defaultBDXRLocator = (DefaultBDXRLocator) metadataLocator;
            final List<String> topDnsDomains = defaultBDXRLocator.getTopDnsDomains();
            assertTrue(CollectionUtils.isNotEmpty(topDnsDomains));
            assertEquals(2, topDnsDomains.size());
            assertTrue(topDnsDomains.contains(smlZone1));
            assertTrue(topDnsDomains.contains(smlZone2));
        } finally {
            domibusPropertyProvider.setProperty(DOMIBUS_SMLZONE, initialSmlZoneValue);
        }
    }

    @Test
    public void createDynamicDiscoveryClientWithStaticSmpUrl() throws TechnicalException {
        String staticSmpUrl = "http://localhost:8080/staticSmp";
        domibusPropertyProvider.setProperty(DOMIBUS_DYNAMICDISCOVERY_SMP_URL, staticSmpUrl);

        try {
            final DynamicDiscovery dynamicDiscoveryClient = dynamicDiscoveryServiceRealInstance.createDynamicDiscoveryClient();
            final IMetadataLocator metadataLocator = dynamicDiscoveryClient.getService().getMetadataLocator();
            assertInstanceOf(StaticMapMetadataLocator.class, metadataLocator);
            final StaticMapMetadataLocator staticMapMetadataLocator = (StaticMapMetadataLocator) metadataLocator;
            final URI lookedUpSmpUrl = staticMapMetadataLocator.lookup("partId", "partScheme");
            assertEquals(staticSmpUrl, lookedUpSmpUrl.toString());
        } finally {
            domibusPropertyProvider.setProperty(DOMIBUS_DYNAMICDISCOVERY_SMP_URL, "");
        }
    }

//    @Test
//    public void testMethodVerifyIfReceiverPublicCertificateIsInTheTruststoreNotCalledBecauseReceiverPartyIsStatic() throws MessagingProcessingException, EbMS3Exception, IOException {
//        setUpTest();
//
//        domibusPropertyProvider.setProperty(DOMIBUS_RECEIVER_CERTIFICATE_VALIDATION_ONSENDING, "false");
//        domibusPropertyProvider.setProperty(DOMIBUS_SENDER_CERTIFICATE_VALIDATION_ONSENDING, "false");
//        domibusPropertyProvider.setProperty(DOMAIN, DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY, "true");
//
//        Mockito.when(mshDispatcher.dispatch(Mockito.any(SOAPMessage.class), Mockito.any(String.class), Mockito.any(Policy.class), Mockito.any(LegConfiguration.class), Mockito.any(String.class)))
//                .thenReturn(Mockito.mock(SOAPMessage.class));
//
//        String messageId = itTestsService.sendMessageWithStatus(MessageStatus.WAITING_FOR_RETRY);
//        UserMessageLog userMessageLog = userMessageLogDao.findByMessageId(messageId, MSHRole.SENDING);
//        userMessageLog.setScheduled(false);
//        userMessageLog.setSendAttempts(5);
//        NotificationStatusEntity entity = notificationStatusDao.findOrCreate(NotificationStatus.REQUIRED);
//        userMessageLog.setNotificationStatus(entity);
//        userMessageLogDao.update(userMessageLog);
//
//        LOG.putMDC(DomibusLogger.MDC_MESSAGE_ID, messageId);
//        messageSenderErrorHandler.handleError(new Exception());
//
//        Mockito.verify(mockAppender, Mockito.atLeastOnce()).doAppend(logCaptor.capture());
//        List<ILoggingEvent> loggingEvents = logCaptor.getAllValues();
//        assertTrue(loggingEvents.stream().anyMatch(event -> event.getMessage().contains("is not dynamic, skipping the verification of the public certificate")));
//
//        deleteAllMessages(messageId);
//    }

//    private void setUpTest() throws IOException, XmlProcessingException {
//        uploadPMode();
//
//        Mockito.when(backendConnectorProvider.getBackendConnector(ArgumentMatchers.anyString()))
//                .thenReturn(backendConnectorMock);
//
//        setUpLoggerForTesting();
//    }

//    private void setUpLoggerForTesting() {
//        // setup logging for testing:
//        MockitoAnnotations.openMocks(this);
//        ch.qos.logback.classic.Logger rootLogger = (ch.qos.logback.classic.Logger)
//                org.slf4j.LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
//        rootLogger.setLevel(ch.qos.logback.classic.Level.DEBUG);
//        rootLogger.addAppender(mockAppender);
//    }

    @Test
    public void testDynamicPartyPersisted() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        final UserMessage userMessage = buildUserMessage(FINAL_RECIPIENT1);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //id adds in the PMode the discovered party and it adds in the truststore the certificate of the discovered party
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        //we clean the Pmode memory
        //if it hadn't been persisted, the party would have been lost
        refreshPmode();

        //we prove it is not lost
        final DynamicDiscoveryPModeProvider dynamicDiscoveryPModeProvider = (DynamicDiscoveryPModeProvider) multiDomainPModeProvider.getCurrentPModeProvider();
        Boolean exists = dynamicDiscoveryPModeProvider.getConfiguration().getBusinessProcesses().getParties().stream()
                .anyMatch(party -> party.getName().equals("party1"));
        assertTrue(exists);

        //double-check it is in db
        DynamicParty dynamicParty = dynamicPartyDao.findDynamicParty("party1");
        assertNotNull(dynamicParty);
    }

    @Test
    public void testDynamicPartyNotLostWhenUploadingPmode() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        //check dynamic party is added in db
        DynamicParty dynamicParty1 = dynamicPartyDao.findDynamicParty("party1");
        assertNull(dynamicParty1);

        final UserMessage userMessage = buildUserMessage(FINAL_RECIPIENT1);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //id adds in the PMode the discovered party and it adds in the truststore the certificate of the discovered party
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        //check dynamic party is added in db
        dynamicParty1 = dynamicPartyDao.findDynamicParty("party1");
        assertNotNull(dynamicParty1);

        //we upload pmode again
        final InputStream inputStream = new ClassPathResource("dataset/pmode/PModeTemplate.xml").getInputStream();
        pModeService.updatePModeFile(IOUtils.toByteArray(inputStream), "PModeTemplate.xml");

        //verify that the dynamic party is still in the db
        dynamicParty1 = dynamicPartyDao.findDynamicParty("party1");
        assertNotNull(dynamicParty1);

        refreshPmode();
        //we prove it is not lost
        final DynamicDiscoveryPModeProvider dynamicDiscoveryPModeProvider = (DynamicDiscoveryPModeProvider) multiDomainPModeProvider.getCurrentPModeProvider();
        Boolean exists = dynamicDiscoveryPModeProvider.getConfiguration().getBusinessProcesses().getParties().stream()
                .anyMatch(party -> party.getName().equals("party1"));
        assertTrue(exists);

    }

    @Test
    public void testRemoveDynamicParty() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

//        final InputStream inputStream = new ClassPathResource(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION).getInputStream();
//        pModeService.updatePModeFile(IOUtils.toByteArray(inputStream), "PModeTemplate.xml");

        //clean up
        cleanBeforeLookup();

        //check dynamic party is added in db
        DynamicParty dynamicParty1 = dynamicPartyDao.findDynamicParty("party1");
        assertNull(dynamicParty1);

        final UserMessage userMessage = buildUserMessage(FINAL_RECIPIENT1);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //id adds in the PMode the discovered party and it adds in the truststore the certificate of the discovered party
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        List<DynamicParty> all = dynamicPartyDao.findAll();
        Assert.assertEquals(2, all.size());

        //check dynamic party is added in db
        dynamicParty1 = dynamicPartyDao.findDynamicParty("party1");
        assertNotNull(dynamicParty1);

        //remove the dynamic party and update the parties
        List<eu.domibus.api.party.Party> parties = partyService.getParties(null, null, null, null, null, 0, 1000);
        parties.removeIf(party -> party.getName().equals("party1"));
        partyService.updateParties(parties, new HashMap<>());

        //verify that the dynamic party is removed from the db
        dynamicParty1 = dynamicPartyDao.findDynamicParty("party1");
        assertNull(dynamicParty1);

        refreshPmode();
        //double check
        DynamicDiscoveryPModeProvider dynamicDiscoveryPModeProvider = (DynamicDiscoveryPModeProvider) multiDomainPModeProvider.getCurrentPModeProvider();
        Boolean exists = dynamicDiscoveryPModeProvider.getConfiguration().getBusinessProcesses().getParties().stream()
                .anyMatch(party -> party.getName().equals("party1"));
        assertFalse(exists);
    }

    @Test
    public void deleteDynamicPartiesNotDiscoveredInTheLastPeriod() throws EbMS3Exception, KeyStoreException, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);

        //clean up
        cleanBeforeLookup();

        //we check the number of parties in the Pmode before running the test
        dynamicDiscoveryAssertionUtil.verifyListOfPartiesFromPmodeSize(1);

        final String finalRecipient1 = FINAL_RECIPIENT1;//party1
        final String finalRecipient2 = FINAL_RECIPIENT2;//party1
        final String finalRecipient3 = FINAL_RECIPIENT3;//party2
        //we skip finalRecipient4 because it's configured with party3 which has an expired certificate
        final String finalRecipient5 = FINAL_RECIPIENT5;//party4
        String partyName1 = PARTY_NAME1;
        String partyName2 = PARTY_NAME2;
        String partyName4 = PARTY_NAME4;

        //---finalRecipient1, party1, certificate1

        //we expect the party and the certificate are added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient1, partyName1, 3, 2, 1, 1);

        //we set the DDC time for the finalRecipient1 to 25h ago
        Date ddcTimeFinalRecipient1 = DateUtils.addHours(new Date(), 25 * -1);
        setDynamicDiscoveryTime(finalRecipient1, ddcTimeFinalRecipient1);

        //---finalRecipient2, party1, certificate1
        //we expect the party and the certificate are not added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient2, partyName1, 3, 2, 1, 2);
        //we set the DDC time for the finalRecipient2 to 2h ago
        Date ddcTimeFinalRecipient2 = DateUtils.addHours(new Date(), 2 * -1);
        setDynamicDiscoveryTime(finalRecipient2, ddcTimeFinalRecipient2);

        //finalRecipient3, party2, certificate2
        //we expect the party and the certificate are added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient3, partyName2, 4, 3, 2, 3);
        //we set the DDC time for the finalRecipient3 to 4h ago
        Date ddcTimeFinalRecipient3 = DateUtils.addHours(new Date(), 4 * -1);
        setDynamicDiscoveryTime(finalRecipient3, ddcTimeFinalRecipient3);

        //finalRecipient5, party4, certificate4
        //we expect the party and the certificate are added in the PMode and truststore
        doLookupForFinalRecipient(finalRecipient5, partyName4, 5, 4, 3, 4);
        //we keep the final recipient5 discovery time to the current time

        //we expect 5 entries in the truststore and 4 entries the Pmode
        dynamicDiscoveryAssertionUtil.verifyListOfPartiesFromPmodeSize(4);
        dynamicDiscoveryAssertionUtil.verifyNumberOfEntriesInTheTruststore(5);

        //we verify that de party2 is persisted
        DynamicParty dynamicParty2 = dynamicPartyDao.findDynamicParty(partyName2);
        assertNotNull(dynamicParty2);

        //we set the retention to 3h and start clean up
        dynamicDiscoveryLookupService.deleteDDCLookupEntriesNotDiscoveredInTheLastPeriod(3);

        //we verify that de party2 was deleted from the db
        dynamicParty2 = dynamicPartyDao.findDynamicParty(partyName2);
        assertNull(dynamicParty2);

        //we expect 4 entries in the truststore and 3 entries the Pmode: party 2 was deleted from the Pmode and certificate 2 deleted from the truststore
        dynamicDiscoveryAssertionUtil.verifyListOfPartiesFromPmodeSize(3);

        List<DynamicParty> all = dynamicPartyDao.findAll();
        Assert.assertEquals(3, all.size());
        Assert.assertTrue(all.stream().anyMatch(party -> party.getName().equals(partyName1)));
        Assert.assertTrue(all.stream().anyMatch(party -> party.getName().equals(partyName4)));
    }

    @Test
    public void propagateAddingPartyOnAnotherNode() throws EbMS3Exception, KeyStoreException, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);
        cleanBeforeLookup();

        String partyName = "anotherParty";

        final DynamicDiscoveryPModeProvider dynamicDiscoveryPModeProvider = (DynamicDiscoveryPModeProvider) multiDomainPModeProvider.getCurrentPModeProvider();
        final eu.domibus.common.model.configuration.Configuration configuration = dynamicDiscoveryPModeProvider.getConfiguration();
        Party party = configuration.getParty();

        //add a dynamic party directly into db, like it was added on another node
        createDynamicPartyInDb(partyName, configuration, party);

        assertTrue(dynamicPartyDao.findAll().stream().anyMatch(p -> p.getName().equals(partyName)));

        //simulate the propagation of party addition to another node
        partyService.addDynamicParty(partyName);

        //check it is successfully added in the PMode
        assertTrue(configuration.getBusinessProcesses().getParties().stream().anyMatch(p -> p.getName().equals(partyName)));
    }

    private void createDynamicPartyInDb(String partyName, Configuration configuration, Party party) {
        DynamicParty dynamicParty = new DynamicParty();
        dynamicParty.setEndpoint("http://localhost:8080/anotherParty");
        dynamicParty.setName(partyName);
        dynamicParty.setUserName("anotherPartyUserName");
        dynamicParty.setPassword("anotherPartyUserPassword");
        dynamicParty.setDynamic(true);
        dynamicParty.setBusinessProcess(configuration.getBusinessProcesses().getEntityId());

        dynamicPartyDao.addIdentifiers(party, dynamicParty);

        for (Process process : configuration.getBusinessProcesses().getProcesses()) {
            dynamicPartyDao.addInitiatorParties(party, dynamicParty, process);
            dynamicPartyDao.addResponderParties(party, dynamicParty, process);
        }

        dynamicPartyDao.update(dynamicParty);
        dynamicPartyDao.flush();
    }

    @Test
    public void testPropagateRemoveDynamicParty() throws EbMS3Exception, XmlProcessingException, IOException {
        initializePmodeAndProperties(DYNAMIC_DISCOVERY_PMODE_WITH_SIGN_AND_ENCRYPTION);
        cleanBeforeLookup();

        final UserMessage userMessage = buildUserMessage(FINAL_RECIPIENT1);
        //it triggers dynamic discovery lookup in SMP  because toParty is empty
        //id adds in the PMode the discovered party and it adds in the truststore the certificate of the discovered party
        assertNotNull(multiDomainPModeProvider.findUserMessageExchangeContext(userMessage, MSHRole.SENDING, false, ProcessingType.PUSH));

        String partyName = "party1";
        List<DynamicParty> all = dynamicPartyDao.findAll();
        Assert.assertEquals(2, all.size());
        //check dynamic party is added in db
        DynamicParty dynamicParty1 = dynamicPartyDao.findDynamicParty(partyName);
        assertNotNull(dynamicParty1);

        //remove the dynamic party from db
        dynamicPartyDao.deleteByName(partyName);

        //verify that the dynamic party is removed from the db
        dynamicParty1 = dynamicPartyDao.findDynamicParty(partyName);
        assertNull(dynamicParty1);

        //simulate the propagation of party deletion to another node
        pModeProvider.removeParties(Arrays.asList(partyName));

        final DynamicDiscoveryPModeProvider dynamicDiscoveryPModeProvider = (DynamicDiscoveryPModeProvider) multiDomainPModeProvider.getCurrentPModeProvider();
        final eu.domibus.common.model.configuration.Configuration configuration = dynamicDiscoveryPModeProvider.getConfiguration();
        //check it is successfully added in the PMode
        assertTrue(configuration.getBusinessProcesses().getParties().stream().noneMatch(p -> p.getName().equals(partyName)));
    }
}
