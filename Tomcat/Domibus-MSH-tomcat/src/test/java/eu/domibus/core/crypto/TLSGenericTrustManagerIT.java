package eu.domibus.core.crypto;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.security.cert.X509Certificate;

/**
 * @author Ionut Breaz
 * @since 5.2
 */
class TLSGenericTrustManagerIT extends AbstractTomcatTestIT {

    @Autowired
    TLSGenericTrustManager tlsGenericTrustManager;

    @Autowired
    DomibusPropertyProvider domibusPropertyProvider;

    @Test
    void test() {
        X509Certificate[] allAcceptedIssuers = tlsGenericTrustManager.getAcceptedIssuers();
        Assertions.assertTrue(allAcceptedIssuers.length > 1);

        try {
            domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_USE_CACERTS, "false");
            X509Certificate[] tlsTruststoreAcceptedIssuers = tlsGenericTrustManager.getAcceptedIssuers();
            Assertions.assertEquals(1, tlsTruststoreAcceptedIssuers.length);
        } finally {
            domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_USE_CACERTS, "true");
        }
    }
}
