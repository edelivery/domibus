package eu.domibus.core.pmode.provider.dynamicdiscovery;

import org.springframework.stereotype.Service;

/**
 * This instance is used in the IT tests where a real(instead of a mock) dynamic discovery is needed
 *
 * @author Cosmin Baciu
 * @since 5.2
 */
@Service
public class DynamicDiscoveryServiceRealInstance extends DynamicDiscoveryServiceOASIS {

}
