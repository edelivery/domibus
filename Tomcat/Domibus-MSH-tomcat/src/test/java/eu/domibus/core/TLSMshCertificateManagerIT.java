package eu.domibus.core;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.multitenancy.DomibusTaskExecutor;
import eu.domibus.api.pki.KeyStoreContentInfo;
import eu.domibus.api.property.DomibusConfigurationService;
import eu.domibus.api.security.TrustStoreEntry;
import eu.domibus.core.certificate.CertificateHelper;
import eu.domibus.core.crypto.TLSMshCertificateManagerImpl;
import eu.domibus.core.crypto.TruststoreDao;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static eu.domibus.api.multitenancy.DomainService.DEFAULT_DOMAIN;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.*;
import static eu.domibus.core.crypto.TLSMshCertificateManagerImpl.TLS_MSH_TRUSTSTORE_NAME;

/**
 * @author Ion Perpegel
 * @since 5.0
 */
@Transactional
public class TLSMshCertificateManagerIT extends AbstractTomcatTestIT {
    public static final String KEYSTORES = "keystores";

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TLSMshCertificateManagerIT.class);

    @Autowired
    private TLSMshCertificateManagerImpl tlsMshCertificateManager;

    @Autowired
    TruststoreDao truststoreDao;

    @Autowired
    DomibusConfigurationService domibusConfigurationService;

    @Autowired
    CertificateHelper certificateHelper;

    @BeforeEach
    public void clean() {
        resetCacheInitialTruststore();
    }

    @Autowired
    DomibusTaskExecutor domainTaskExecutor;

    @Test
    public void persistTrustStoresIfApplicable() {
        final String initialTruststoreLocation = domibusPropertyProvider.getProperty(DomainService.DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION);

        try {
            List<TrustStoreEntry> storeEntries = tlsMshCertificateManager.getTrustStoreEntries();
            Assertions.assertEquals(2, storeEntries.size());

            domainTaskExecutor.submit(() -> createStore(TLS_MSH_TRUSTSTORE_NAME, "keystores/gateway_truststore2.jks"), DEFAULT_DOMAIN);

            boolean exists = truststoreDao.existsWithName(TLS_MSH_TRUSTSTORE_NAME);
            Assertions.assertTrue(exists);

            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, KEYSTORES + "/gateway_truststore_original.jks");
            tlsMshCertificateManager.saveStoresFromDBToDisk();

            boolean isPersisted = truststoreDao.existsWithName(TLS_MSH_TRUSTSTORE_NAME);
            Assertions.assertFalse(isPersisted);

            storeEntries = tlsMshCertificateManager.getTrustStoreEntries();
            Assertions.assertEquals(9, storeEntries.size());
            Assertions.assertTrue(storeEntries.stream().anyMatch(entry -> entry.getName().equals("cefsupportgw")));
        } finally {
            if (StringUtils.isNotBlank(initialTruststoreLocation)) {
                domibusPropertyProvider.setProperty(DomainService.DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, initialTruststoreLocation);
            }
        }
    }

    @Test
    public void getTrustStoreEntries() {
        List<TrustStoreEntry> trustStoreEntries = tlsMshCertificateManager.getTrustStoreEntries();
        Assertions.assertEquals(2, trustStoreEntries.size());
    }

    @Test
    public void addCertificate() throws IOException {
        List<TrustStoreEntry> trustStoreEntries = tlsMshCertificateManager.getTrustStoreEntries();
        Assertions.assertEquals(2, trustStoreEntries.size());

        Path path = Paths.get(domibusConfigurationService.getConfigLocation(), KEYSTORES, "green_gw.cer");
        byte[] content = Files.readAllBytes(path);
        String green_gw = "green_gw";
        tlsMshCertificateManager.addCertificate(content, green_gw);

        trustStoreEntries = tlsMshCertificateManager.getTrustStoreEntries();
        Assertions.assertEquals(3, trustStoreEntries.size());
        Assertions.assertTrue(trustStoreEntries.stream().anyMatch(entry -> entry.getName().equals(green_gw)));
    }

    @Test
    public void removeCertificate() {
        List<TrustStoreEntry> trustStoreEntries = tlsMshCertificateManager.getTrustStoreEntries();
        Assertions.assertEquals(2, trustStoreEntries.size());

        String blue_gw = "blue_gw";
        tlsMshCertificateManager.removeCertificate(blue_gw);

        trustStoreEntries = tlsMshCertificateManager.getTrustStoreEntries();
        Assertions.assertEquals(1, trustStoreEntries.size());
        Assertions.assertFalse(trustStoreEntries.stream().anyMatch(entry -> entry.getName().equals(blue_gw)));
    }

    @Test
    @Transactional
    public void replaceTrustStore() throws IOException {
        List<TrustStoreEntry> trustStoreEntries = tlsMshCertificateManager.getTrustStoreEntries();
        Assertions.assertEquals(2, trustStoreEntries.size());

        String relativePath = KEYSTORES + "/cefsupportgwtruststore.jks";
        Path path = Paths.get(domibusConfigurationService.getConfigLocation(), relativePath);

        byte[] content = Files.readAllBytes(path);
        String fileName = relativePath;
        KeyStoreContentInfo storeInfo = certificateHelper.createStoreContentInfo(TLS_MSH_TRUSTSTORE_NAME, fileName, content, "test123");

        tlsMshCertificateManager.replaceTrustStore(storeInfo);

        trustStoreEntries = tlsMshCertificateManager.getTrustStoreEntries();
        Assertions.assertEquals(9, trustStoreEntries.size());
    }

    private void resetCacheInitialTruststore() {
        try {
            String storePassword = "test123";
            String relativePath = KEYSTORES + "/gateway_truststore_original.jks";

            Path path = Paths.get(domibusConfigurationService.getConfigLocation(), relativePath);
            byte[] content = Files.readAllBytes(path);

            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_LOCATION, KEYSTORES + "/mykeystore.jks");
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_TYPE, "jks");
            domibusPropertyProvider.setProperty(DEFAULT_DOMAIN, DOMIBUS_SECURITY_TLS_TRUSTSTORE_PASSWORD, "test123");

            KeyStoreContentInfo storeInfo = certificateHelper.createStoreContentInfo(TLS_MSH_TRUSTSTORE_NAME, KEYSTORES + "/mykeystore.jks", content, storePassword);
            tlsMshCertificateManager.replaceTrustStore(storeInfo);
        } catch (Exception e) {
            LOG.info("Error restoring initial keystore", e);
        }
    }
}
