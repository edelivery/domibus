package eu.domibus.core.message.test;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.ebms3.Ebms3Constants;
import eu.domibus.core.message.dictionary.PartyIdDao;
import eu.domibus.core.message.testservice.TestService;
import eu.domibus.core.message.testservice.TestServiceException;
import eu.domibus.messaging.MessagingProcessingException;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.Submission;
import eu.domibus.web.rest.ro.TestServiceMessageInfoRO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TestServiceIT extends AbstractTomcatTestIT {

    @Autowired
    private TestService testService;

    @Autowired
    PartyIdDao partyIdDao;

    @Test
    public void submitTest() throws MessagingProcessingException, IOException {

        uploadPMode();

        String pModePartyType = "urn:oasis:names:tc:ebcore:partyid-type:unregistered";
        String anotherPartyType = "urn:oasis:names:tc:ebcore:partyid-type:eudamed";
        String senderParty = "domibus-blue";
        String receiverParty = "domibus-red";

        partyIdDao.findOrCreateParty(senderParty, pModePartyType);
        partyIdDao.findOrCreateParty(receiverParty, pModePartyType);
        partyIdDao.findOrCreateParty(senderParty, anotherPartyType);
        partyIdDao.findOrCreateParty(receiverParty, anotherPartyType);

        testService.submitTest(senderParty, receiverParty);

        TestServiceMessageInfoRO res = testService.getLastTestSentWithErrors(senderParty, receiverParty);
        assertNotNull(res);
        assertNull(res.getErrorInfo());

        Map<String, String> toReplace = new HashMap<>();
        toReplace.put("partyIdType name=\"partyTypeUrn\" value=\"urn:oasis:names:tc:ebcore:partyid-type:unregistered\"",
                "partyIdType name=\"partyTypeUrn\" value=\"urn:oasis:names:tc:ebcore:partyid-type:eudamed\"");
        uploadPMode(SERVICE_PORT, toReplace);

        assertThrows(TestServiceException.class, () -> testService.getLastTestSentWithErrors(senderParty, receiverParty));
    }

    @Test
    public void createSubmission() throws MessagingProcessingException, IOException {
        uploadPMode(null, "dataset/pmode/PMode_testService.xml", null);

        Submission submission = testService.createSubmission("sti-taxud");

        Assert.notNull(submission, "Submission is null");
        assertEquals("sti-taxud", submission.getFromParties().iterator().next().getPartyId());
        assertEquals("Customs", submission.getFromRole());
        assertEquals("Trader", submission.getToRole());
        assertEquals("EU-ICS2-TI-V1.0", submission.getAgreementRef());
        assertEquals("ics2-interface-version", submission.getAgreementRefType());
        assertEquals(Ebms3Constants.TEST_SERVICE, submission.getService());

        uploadPMode();
    }
}
