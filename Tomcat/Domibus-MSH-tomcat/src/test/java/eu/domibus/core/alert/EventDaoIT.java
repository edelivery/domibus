package eu.domibus.core.alert;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.core.alerts.dao.EventDao;
import eu.domibus.core.alerts.model.common.EventType;
import eu.domibus.core.alerts.model.persist.Event;
import eu.domibus.core.alerts.model.persist.StringEventProperty;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author François GAUTIER
 * @since 5.2
 */
@Transactional
class EventDaoIT extends AbstractTomcatTestIT {

    public static final String FROM_PARTY = "FROM_PARTY";

    @Autowired
    private EventDao eventDao;

    public void createEvent(String fromParty) {
        Event event = new Event();

        event.addProperty(FROM_PARTY, getStringEventProperty(fromParty));
        event.setType(EventType.MSG_STATUS_CHANGED);
        event.setReportingTime(new Date());

        eventDao.create(event);
    }

    private static StringEventProperty getStringEventProperty(String fromParty) {
        final StringEventProperty from = new StringEventProperty();
        from.setStringValue(fromParty);
        return from;
    }

    @Test
    void findWithTypeAndPropertyValue() {
        createEvent("blue_gw");

        Event event = eventDao.findWithTypeAndPropertyValue(EventType.MSG_STATUS_CHANGED, FROM_PARTY, "blue_gw");
        assertNotNull(event);
        assertNotNull(event.toString());
        assertEquals(1, event.getProperties().size());
    }

}
