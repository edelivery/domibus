package eu.domibus.core.crypto;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.crypto.TLSGenericCertificateManager;
import eu.domibus.api.multitenancy.DomainContextProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.KeyStore;
import java.util.Collections;

/**
 * @author Ionut Breaz
 * @since 5.2
 */

class TLSGenericCertificateManagerImplIT extends AbstractTomcatTestIT {
    @Autowired
    TLSGenericCertificateManager tlsGenericCertificateManager;

    @Autowired
    DomainContextProvider domainProvider;

    @Test
    void testLoadTruststore() throws Exception {
        KeyStore truststore = tlsGenericCertificateManager.getTLSGenericTruststore();
        Assertions.assertEquals(1, Collections.list(truststore.aliases()).size());
    }
}
