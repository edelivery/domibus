package eu.domibus.core.crypto;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainService;
import eu.domibus.api.security.SecurityProfile;
import eu.domibus.api.security.SecurityProfileMetadata;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class SecurityProfileServiceImplTestIT extends AbstractTomcatTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SecurityProfileServiceImplTestIT.class);

    @Autowired
    SecurityProfileProviderImpl securityProfileProvider;

    @Test
    public void getSecurityProfileBasedOnWSSecurityMetadataWithRsa() {
        SecurityProfileMetadata securityProfileMetadata = new SecurityProfileMetadata();
        securityProfileMetadata.setSignatureAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
        securityProfileMetadata.setEncryptionKeyTransportAlgorithm("http://www.w3.org/2009/xmlenc11#rsa-oaep");
        securityProfileMetadata.setEncryptionMGFAlgorithm("http://www.w3.org/2009/xmlenc11#mgf1sha256");
        securityProfileMetadata.setEncryptionDigestAlgorithm("http://www.w3.org/2001/04/xmlenc#sha256");
        final SecurityProfile securityProfileBasedOnWSSecurityMetadata = securityProfileProvider.determineSecurityProfile(DomainService.DEFAULT_DOMAIN, securityProfileMetadata);
        Assertions.assertEquals("rsa", securityProfileBasedOnWSSecurityMetadata.getCode());
    }
}
