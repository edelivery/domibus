package eu.domibus.core.crypto;


import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.crypto.TLSMshCertificateManager;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.core.exception.ConfigurationException;
import eu.domibus.ext.services.CacheExtService;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TLSMshCertificateManagerImplIT extends AbstractTomcatTestIT {
    @Autowired
    TLSMshCertificateManager tlsMshCertificateManager;

    @Autowired
    DomainContextProvider domainContextProvider;

    @Autowired
    private CacheExtService cacheExtService;


    @Test
    void getTlsClientParameters() {
        cacheExtService.evictCaches();

        Domain currentDomain = domainContextProvider.getCurrentDomain();
        TLSClientParameters tlsClientParameters = tlsMshCertificateManager.getTlsClientParameters(currentDomain.getCode());
        Assertions.assertEquals(1, tlsClientParameters.getKeyManagers().length);
        Assertions.assertEquals(1, tlsClientParameters.getTrustManagers().length);
        Assertions.assertFalse(tlsClientParameters.isDisableCNCheck());
        Assertions.assertEquals("blue_tls", tlsClientParameters.getCertAlias());
        Assertions.assertEquals("TLS", tlsClientParameters.getSecureSocketProtocol());
    }

    @Test
    void getTlsClientParametersWrongKeystorePassword() {
        cacheExtService.evictCaches();

        Domain currentDomain = domainContextProvider.getCurrentDomain();
        String prevKeystorePassword = domibusPropertyProvider.getProperty(currentDomain, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_KEYSTORE_PASSWORD);
        try {
            domibusPropertyProvider.setProperty(currentDomain, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_KEYSTORE_PASSWORD, "wrongKeystorePass");
            ConfigurationException configurationException = Assertions.assertThrows(ConfigurationException.class,
                    () -> tlsMshCertificateManager.getTlsClientParameters(currentDomain.getCode()));
            Assertions.assertEquals("Error getting the KeyManager", configurationException.getMessage());
        } finally {
            domibusPropertyProvider.setProperty(currentDomain, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_KEYSTORE_PASSWORD, prevKeystorePassword);
        }
    }

    @Test
    void getTlsClientParametersWrongKeyPassword() {
        cacheExtService.evictCaches();

        Domain currentDomain = domainContextProvider.getCurrentDomain();
        String prevKeyPassword = domibusPropertyProvider.getProperty(currentDomain, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_PRIVATE_PASSWORD);
        try {
            domibusPropertyProvider.setProperty(currentDomain, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_PRIVATE_PASSWORD, "wrongKeyPass");
            ConfigurationException configurationException = Assertions.assertThrows(ConfigurationException.class,
                    () -> tlsMshCertificateManager.getTlsClientParameters(currentDomain.getCode()));
            Assertions.assertEquals("Error getting the KeyManager", configurationException.getMessage());
        } finally {
            domibusPropertyProvider.setProperty(currentDomain, DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_PRIVATE_PASSWORD, prevKeyPassword);
        }
    }
}
