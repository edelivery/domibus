package eu.domibus.core.ebms3.receiver.policy;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.pmode.PModeConstants;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.core.ebms3.receiver.leg.MessageLegConfigurationFactory;
import eu.domibus.core.ebms3.sender.client.DispatchClientDefaultProvider;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.test.common.SoapSampleUtil;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.apache.cxf.ws.policy.PolicyConstants;
import org.apache.cxf.ws.security.SecurityConstants;
import org.apache.neethi.Policy;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;


/**
 * @author draguio
 * @since 3.3
 */
@Transactional
public class SetPolicyInInterceptorIT extends AbstractTomcatTestIT {

    @Autowired
    SoapSampleUtil soapSampleUtil;
    @Autowired
    SetPolicyInServerInterceptor setPolicyInInterceptorServer;

    @Autowired
    MessageLegConfigurationFactory serverInMessageLegConfigurationFactory;

    @BeforeEach
    public void before() throws IOException, XmlProcessingException {
        uploadPMode(SERVICE_PORT);
    }

    @Test
    public void testHandleMessage() throws  IOException {
        String filename = "SOAPMessage2.xml";

        SoapMessage sm = soapSampleUtil.createSoapMessage(filename, UUID.randomUUID() + "@domibus.eu");

        setPolicyInInterceptorServer.handleMessage(sm);

        final SecurityProfileConfiguration securityProfileConfiguration = (SecurityProfileConfiguration) sm.getExchange().get(PModeConstants.SECURITY_PROFILE_CONFIGURATION);
        Assertions.assertEquals("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", securityProfileConfiguration.getSignatureInSignatureAlgorithm());
    }

    @Test
    void testHandleMessageNull() throws IOException {

        String filename = "SOAPMessageNoMessaging.xml";
        SoapMessage sm = soapSampleUtil.createSoapMessage(filename, UUID.randomUUID() + "@domibus.eu");

        // handle message without adding any content
        Assertions.assertThrows(org.apache.cxf.interceptor.Fault. class,() -> setPolicyInInterceptorServer.handleMessage(sm));
    }
}
