package eu.domibus.core.message;

import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.MessageStatus;
import eu.domibus.api.model.*;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.common.*;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.ebms3.sender.MessageSenderErrorHandler;
import eu.domibus.core.ebms3.sender.ResponseHandler;
import eu.domibus.core.ebms3.sender.ResponseResult;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.message.dictionary.NotificationStatusDao;
import eu.domibus.core.message.reliability.ReliabilityChecker;
import eu.domibus.core.plugin.BackendConnectorHelper;
import eu.domibus.core.plugin.handler.MessageSubmitterImpl;
import eu.domibus.core.util.MessageUtil;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessagingProcessingException;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.notification.PluginAsyncNotificationConfiguration;
import eu.domibus.test.common.BackendConnectorMock;
import eu.domibus.test.common.SoapSampleUtil;
import eu.domibus.test.common.SubmissionUtil;
import org.apache.neethi.Policy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import javax.jms.Queue;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.WebServiceException;
import java.io.IOException;
import java.util.*;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_RECEIVER_CERTIFICATE_VALIDATION_ONSENDING;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SENDER_CERTIFICATE_VALIDATION_ONSENDING;
import static eu.domibus.jms.spi.InternalJMSConstants.UNKNOWN_RECEIVER_QUEUE;
import static eu.domibus.messaging.MessageConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Transactional
public class BackendNotificationServiceIT extends DeleteMessageAbstractIT {

    @Autowired
    protected MessageExchangeService messageExchangeService;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    MSHWebservice mshWebserviceTest;

    @Autowired
    MessageUtil messageUtil;

    @Autowired
    protected BackendConnectorHelper backendConnectorHelper;

    @Autowired
    PluginAsyncNotificationConfiguration pluginAsyncNotificationConfiguration;

    @Autowired
    Queue notifyBackendWebServiceQueue;

    @Autowired
    @Qualifier(UNKNOWN_RECEIVER_QUEUE)
    protected Queue unknownReceiverQueue;

    @Autowired
    protected SubmissionUtil submissionUtil;

    @Autowired
    MessageSubmitterImpl messageSubmitter;

    @Autowired
    MessagesLogServiceImpl messagesLogService;

    @Autowired
    private UserMessageDao userMessageDao;

    @Autowired
    protected MSHDispatcher mshDispatcher;

    @Autowired
    protected ResponseHandler responseHandler;

    @Autowired
    protected ReliabilityChecker reliabilityChecker;

    @Autowired
    protected MessageStatusDao messageStatusDao;

    @Autowired
    protected DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    NotificationStatusDao notificationStatusDao;

    @Autowired
    @Qualifier("messageSenderErrorHandler")
    protected MessageSenderErrorHandler messageSenderErrorHandler;

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BackendNotificationServiceIT.class);

    String messageId, filename;

    @Transactional
    @BeforeEach
    public void before() throws XmlProcessingException, IOException {
        messageId = BackendConnectorMock.MESSAGE_ID;
        filename = "SOAPMessage2.xml";

        uploadPMode();

        Mockito.when(backendConnectorProvider.getBackendConnector(ArgumentMatchers.anyString()))
                .thenReturn(backendConnectorMock);
    }

    @Transactional
    @AfterEach
    public void after() {
        backendConnectorMock.clear();
        Mockito.reset(pluginAsyncNotificationConfiguration);
        List<MessageLogInfo> list = userMessageLogDao.findAllInfoPaged(0, 100, "ID_PK", true, new HashMap<>(), Collections.emptyList());
        if (list.size() > 0) {
            list.forEach(el -> {
                UserMessageLog res = userMessageLogDao.findByMessageId(el.getMessageId(), el.getMshRole());
                userMessageLogDao.deleteMessageLogs(Arrays.asList(res.getEntityId()));
            });
        }
    }

    @Test
    @Transactional
    public void testNotifyMessageReceivedSync() throws SOAPException, IOException, ParserConfigurationException, SAXException, EbMS3Exception {
        final SOAPMessage soapResponse = itTestsService.receiveMessage(filename, messageId);

        assertEquals(backendConnectorMock.getDeliverMessageEvent().getMessageId(), messageId);

        final Ebms3Messaging ebms3Messaging = messageUtil.getMessagingWithDom(soapResponse);
        assertNotNull(ebms3Messaging);

        assertEquals(backendConnectorMock.getDeliverMessageEvent().getMessageId(), messageId);
    }

    @Test
    @Transactional
    public void testValidateAndNotifyAsync() throws SOAPException, IOException, ParserConfigurationException, SAXException, EbMS3Exception {

        Mockito.when(pluginAsyncNotificationConfiguration.getBackendConnector()).thenReturn(backendConnectorMock);
        Mockito.when(pluginAsyncNotificationConfiguration.getBackendNotificationQueue()).thenReturn(notifyBackendWebServiceQueue);

        final SOAPMessage soapResponse = itTestsService.receiveMessage(filename, messageId);

        domibusConditionUtil.waitUntilMessageHasStatus(messageId, MSHRole.SENDING, MessageStatus.NOT_FOUND);

        final Ebms3Messaging ebms3Messaging = messageUtil.getMessagingWithDom(soapResponse);
        assertNotNull(ebms3Messaging);
    }

    @Test
    @Transactional
    void testValidateAndNotifyReceivedFailure() throws SOAPException, IOException, ParserConfigurationException, SAXException {
        filename = "InvalidBodyloadCidSOAPMessage.xml";
        Assertions.assertThrows(WebServiceException.class, () -> itTestsService.receiveMessage(filename, messageId));
    }

    @Test
    public void testNotifyMessageDeleted() throws MessagingProcessingException {
        String messageId = itTestsService.sendMessageWithStatus(MessageStatus.ACKNOWLEDGED);

        deleteAllMessages(messageId);

        MessageDeletedBatchEvent messageDeletedBatchEvent = backendConnectorMock.getMessageDeletedBatchEvent();
        assertEquals(1, messageDeletedBatchEvent.getMessageDeletedEvents().size());
        MessageDeletedEvent singleMessageDeletedEvent = messageDeletedBatchEvent.getMessageDeletedEvents().get(0);
        assertEquals(singleMessageDeletedEvent.getMessageId(), messageId);
        assertNotNull(singleMessageDeletedEvent.getMessageEntityId());

        assertNotNull(singleMessageDeletedEvent.getProps().get(ORIGINAL_SENDER));
        assertNotNull(singleMessageDeletedEvent.getProps().get(FINAL_RECIPIENT));

        Assertions.assertNull(userMessageDao.findByMessageId(messageId));
        Assertions.assertNull(userMessageLogDao.findByMessageId(messageId, MSHRole.SENDING));


    }

    @Test
    public void testNotifyOfSendSuccess() throws MessagingProcessingException, EbMS3Exception {
        domibusPropertyProvider.setProperty(DOMIBUS_RECEIVER_CERTIFICATE_VALIDATION_ONSENDING, "false");
        domibusPropertyProvider.setProperty(DOMIBUS_SENDER_CERTIFICATE_VALIDATION_ONSENDING, "false");

        Mockito.when(mshDispatcher.dispatch(Mockito.any(SOAPMessage.class), Mockito.any(String.class), Mockito.any(SecurityProfileConfiguration.class), Mockito.any(LegConfiguration.class), Mockito.any(String.class)))
                .thenReturn(Mockito.mock(SOAPMessage.class));

        ResponseResult responseResult = Mockito.mock(ResponseResult.class);
        Mockito.when(responseResult.getResponseStatus()).thenReturn(ResponseHandler.ResponseStatus.OK);

        Mockito.when(reliabilityChecker.check(Mockito.any(SOAPMessage.class), Mockito.any(SOAPMessage.class), Mockito.any(ResponseResult.class), Mockito.any(LegConfiguration.class)))
                .thenReturn(ReliabilityChecker.CheckResult.OK);

        String messageId = itTestsService.sendMessageWithStatus(MessageStatus.SEND_ENQUEUED);

        PayloadSubmittedEvent payloadSubmittedEvent = backendConnectorMock.getPayloadSubmittedEvent();
        PayloadProcessedEvent payloadProcessedEvent = backendConnectorMock.getPayloadProcessedEvent();
        assertEquals(payloadSubmittedEvent.getMessageId(), messageId);
        assertNotNull(payloadSubmittedEvent.getMessageEntityId());
        assertEquals(payloadProcessedEvent.getMessageId(), messageId);
        assertNotNull(payloadProcessedEvent.getMessageEntityId());

        assertPropertiesPresentInEvent(payloadSubmittedEvent);
        assertPropertiesPresentInEvent(payloadProcessedEvent);

        UserMessage byMessageId = userMessageDao.findByMessageId(messageId);
        Assertions.assertNotNull(byMessageId);

        deleteAllMessages(messageId);
    }

    @Test
    public void testNotifyOfSendFailure() throws MessagingProcessingException, EbMS3Exception {
        domibusPropertyProvider.setProperty(DOMIBUS_RECEIVER_CERTIFICATE_VALIDATION_ONSENDING, "false");
        domibusPropertyProvider.setProperty(DOMIBUS_SENDER_CERTIFICATE_VALIDATION_ONSENDING, "false");

        Mockito.when(mshDispatcher.dispatch(Mockito.any(SOAPMessage.class), Mockito.any(String.class), Mockito.any(SecurityProfileConfiguration.class), Mockito.any(LegConfiguration.class), Mockito.any(String.class)))
                .thenReturn(Mockito.mock(SOAPMessage.class));

        String messageId = itTestsService.sendMessageWithStatus(MessageStatus.WAITING_FOR_RETRY);
        UserMessageLog userMessageLog = userMessageLogDao.findByMessageId(messageId, MSHRole.SENDING);
        userMessageLog.setScheduled(false);
        userMessageLog.setSendAttempts(5);
        NotificationStatusEntity entity = notificationStatusDao.findOrCreate(NotificationStatus.REQUIRED);
        userMessageLog.setNotificationStatus(entity);
        userMessageLogDao.update(userMessageLog);

        LOG.putMDC(DomibusLogger.MDC_MESSAGE_ID, messageId);
        messageSenderErrorHandler.handleError(new Exception());

        MessageSendFailedEvent messageSendFailedEvent = backendConnectorMock.getMessageSendFailedEvent();
        assertEquals(messageSendFailedEvent.getMessageId(), messageId);
        assertNotNull(messageSendFailedEvent.getMessageEntityId());

        assertPropertiesPresentInEvent(messageSendFailedEvent);

        deleteAllMessages(messageId);

        LOG.removeMDC(DomibusLogger.MDC_MESSAGE_ID);
    }


    @Test
    @Transactional
    public void testEventProps_NotifyMessageReceived() throws SOAPException, IOException, ParserConfigurationException, SAXException {
        itTestsService.receiveMessage(filename, messageId);

        DeliverMessageEvent messageReceivedEvent = backendConnectorMock.getDeliverMessageEvent();
        assertEquals(messageReceivedEvent.getMessageId(), messageId);
        assertNotNull(messageReceivedEvent.getMessageEntityId());
        assertEquals(messageReceivedEvent.getProps().get(MSH_ROLE), eu.domibus.common.MSHRole.RECEIVING.name());

        assertPropertiesPresentInEvent(messageReceivedEvent);
    }

    @Test
    @Transactional
    public void testEventProps_NotifyReceivedFailure() throws SOAPException, IOException, ParserConfigurationException, SAXException {
        filename = "InvalidBodyloadCidSOAPMessage.xml";

        try {
            final SOAPMessage soapResponse = itTestsService.receiveMessage(filename, messageId);
        } catch (WebServiceException wse) {
            //OK
        }
    }


    private void assertPropertiesPresentInEvent(MessageEvent messageEvent) {
        Map<String, String> properties = messageEvent.getProps();

        assertNotNull(properties.get(CONVERSATION_ID));
        assertNotNull(properties.get(FROM_PARTY_ID));
        assertNotNull(properties.get(TO_PARTY_ID));
        assertNotNull(properties.get(ORIGINAL_SENDER));
        assertNotNull(properties.get(FINAL_RECIPIENT));
        assertNotNull(properties.get(SERVICE));
        assertNotNull(properties.get(SERVICE_TYPE));

        assertNotNull(properties.get(ACTION));
    }

}
