package eu.domibus.core.ebms3.receiver;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.ebms3.model.Ebms3Messaging;
import eu.domibus.api.model.PartInfo;
import eu.domibus.common.Ebms3ErrorExt;
import eu.domibus.core.cxf.CxfCurrentMessageService;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.mapper.usermessage.Ebms3UserMessageMapper;
import eu.domibus.ext.exceptions.DomibusErrorCode;
import eu.domibus.ext.exceptions.UserMessageExceptionTypeEnum;
import eu.domibus.ext.exceptions.UserMessageExtException;
import eu.domibus.messaging.MessageConstants;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.test.common.BackendConnectorMock;
import eu.domibus.test.common.UserMessageSampleUtil;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.jaxws.handler.soap.SOAPMessageContextImpl;
import org.apache.cxf.message.ExchangeImpl;
import org.apache.cxf.message.MessageImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.MissingResourceException;
import java.util.UUID;

import static eu.domibus.common.model.configuration.Payload_.cid;
import static eu.domibus.core.spi.payload.DomibusPayloadManagerSpi.DOMIBUS_DATABASE_SPI;
import static org.junit.jupiter.api.Assertions.*;


public class FaultInHandlerIT extends AbstractTomcatTestIT {

    public static final String MESSAGE_ID = "123";

    @Autowired
    private FaultInHandler faultInHandler;

    @Autowired
    private BackendConnectorMock backendConnectorMock;

    @Autowired
    private Ebms3UserMessageMapper ebms3UserMessageMapper;

    CxfCurrentMessageService cxfCurrentMessageService = Mockito.mock(CxfCurrentMessageService.class);
    private CxfCurrentMessageService savedCxfCurrentMessageService;

    @Test
    void handles() {
        assertTrue(faultInHandler.handleMessage(null));
    }

    @BeforeEach
    void setUp() {
        savedCxfCurrentMessageService = (CxfCurrentMessageService) ReflectionTestUtils.getField(faultInHandler, "cxfCurrentMessageService");
        ReflectionTestUtils.setField(faultInHandler, "cxfCurrentMessageService", cxfCurrentMessageService);
        backendConnectorMock.clear();
    }

    @AfterEach
    void tearDown() {
        ReflectionTestUtils.setField(faultInHandler, "cxfCurrentMessageService", savedCxfCurrentMessageService);
        backendConnectorMock.clear();
    }

    @Test
    void testHandleFaultNullContext() {
        Assertions.assertThrows(MissingResourceException.class, () -> faultInHandler.handleFault(null));
    }

    @Test
    void handleFault() throws XmlProcessingException, IOException {

        uploadPMode();
        Mockito.when(backendConnectorProvider.getBackendConnector(ArgumentMatchers.anyString()))
                .thenReturn(backendConnectorMock);

        MessageImpl message = getMessage();

        Mockito.when(cxfCurrentMessageService.getCurrentMessage()).thenReturn(message);

        faultInHandler.handleFault(new SOAPMessageContextImpl(new SoapMessage(message)));

        assertNotNull(backendConnectorMock.getMessageReceiveFailureEvent());
    }

    private MessageImpl getMessage() {
        MessageImpl message = new MessageImpl();
        message.put("ebms.messageid", UUID.randomUUID().toString());
        message.put(Exception.class.getName(), new UserMessageExtException(DomibusErrorCode.DOM_001, "TEST"));
        message.setExchange(getExchange());
        return message;
    }

    private ExchangeImpl getExchange() {
        ExchangeImpl e = new ExchangeImpl();
        e.put(MessageConstants.EMBS3_MESSAGING_OBJECT, getEbms3Messaging());
        return e;
    }

    private Ebms3Messaging getEbms3Messaging() {
        Ebms3Messaging value = new Ebms3Messaging();
        value.setUserMessage(ebms3UserMessageMapper.userMessageEntityToEbms3(UserMessageSampleUtil.createUserMessage(), List.of(getPartInfo())));
        return value;
    }

    private static PartInfo getPartInfo() {
        PartInfo partInfo = new PartInfo();
        partInfo.setHref("cid:" + cid);
        partInfo.setBinaryData("Hello world".getBytes(StandardCharsets.UTF_8));
        partInfo.setMime("application/text");
        partInfo.setPayloadManagerSPI(DOMIBUS_DATABASE_SPI);
        partInfo.loadBinary();
        return partInfo;

    }

    @Test
    void getEBMS3ExceptionWithUserMessageExtException() {
        UserMessageExtException userMessageExtException = new UserMessageExtException(DomibusErrorCode.DOM_001, "my UserMessageExtException detail");

        Ebms3ErrorExt ebms3Error = new Ebms3ErrorExt();
        final String errorCode = "COS-1";
        final String errorDetail = "COS-1 detail";
        final String myCategory = "myCategory";
        final String mySeverity = "mySeverity";
        final String myOrigin = "myOrigin";
        final String myShortDescription = "myShortDescription";

        ebms3Error.setErrorCode(errorCode);
        ebms3Error.setErrorDetail(errorDetail);
        ebms3Error.setCategory(myCategory);
        ebms3Error.setSeverity(mySeverity);
        ebms3Error.setOrigin(myOrigin);
        ebms3Error.setShortDescription(myShortDescription);

        userMessageExtException.setEbmsError(ebms3Error);
        Exception myException = new Exception(userMessageExtException);
        String messageId = null;
        final EbMS3Exception ebms3Exception = faultInHandler.getEBMS3Exception(myException, messageId);
        assertEquals(errorCode, ebms3Exception.getErrorCode());
        assertEquals(errorDetail, ebms3Exception.getErrorDetail());
        assertEquals(myCategory, ebms3Exception.getCategory());
        assertEquals(mySeverity, ebms3Exception.getSeverity());
        assertEquals(myOrigin, ebms3Exception.getOrigin());
        assertEquals(myShortDescription, ebms3Exception.getShortDescription());
    }

    @Test
    @SuppressWarnings("ThrowableNotThrown")
    void getEBMS3ExceptionWithUserMessageExtExceptionThrowingSoapFaultException() {
        UserMessageExtException userMessageExtException = new UserMessageExtException(DomibusErrorCode.DOM_001, "my UserMessageExtException detail");
        userMessageExtException.setExceptionTypeEnum(UserMessageExceptionTypeEnum.SOAP_FAULT);
        Exception myException = new Exception(userMessageExtException);
        String messageId = null;

        Assertions.assertThrows(UserMessageExtException.class, () -> faultInHandler.getEBMS3Exception(myException, messageId));

    }
}
