package eu.domibus.core;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.core.certificate.CertificateTestUtils;
import eu.domibus.core.crypto.spi.DefaultAuthorizationServiceSpiImpl;
import eu.domibus.core.crypto.spi.model.AuthorizationException;
import eu.domibus.test.common.PKIUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.security.cert.X509Certificate;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_SENDER_TRUST_VALIDATION_EXPRESSION;


/**
 * @author Soumya Chandran
 * @since 5.2
 */public class DefaultAuthorizationServiceSpiImplIT extends AbstractTomcatTestIT {
    String TRUST_EXP = ".*TEST.EU";
    String certificateSubject = "C=EU, O=eDelivery, OU=Domibus, CN=domibus-blue";
    String PROPERTY_OLD_VALUE;
    String PARTY_NAME = "domibus-blue";
    String PARTY_NAME1 = "domibus-red";

    @Autowired
    DefaultAuthorizationServiceSpiImpl defaultAuthorizationServiceSpi;

    @Autowired
    CertificateTestUtils certificateTestUtils;


    @BeforeEach
    public void setUp() throws Exception {
        PROPERTY_OLD_VALUE = domibusPropertyProvider.getProperty(DOMIBUS_SENDER_TRUST_VALIDATION_EXPRESSION);
    }

    @AfterEach
    public void clean() {
        domibusPropertyProvider.setProperty(domainContextProvider.getCurrentDomain(), DOMIBUS_SENDER_TRUST_VALIDATION_EXPRESSION, PROPERTY_OLD_VALUE);
    }


    @Test
    public void authorizeAgainstCertificateSubjectExpressionNonMatchingRegExp() {

        PKIUtil pkiUtil = new PKIUtil();

        domibusPropertyProvider.setProperty(domainContextProvider.getCurrentDomain(), DOMIBUS_SENDER_TRUST_VALIDATION_EXPRESSION, TRUST_EXP);


        final X509Certificate signingCertificate = pkiUtil.createCertificateWithSubject(BigInteger.valueOf(111), certificateSubject);


        Assertions.assertThrows(AuthorizationException.class, () -> defaultAuthorizationServiceSpi.authorizeAgainstCertificateSubjectExpression(signingCertificate));


    }


    @Test
    public void authorizeAgainstCertificateSubjectExpressionEmptyList() {

        PKIUtil pkiUtil = new PKIUtil();

        domibusPropertyProvider.setProperty(domainContextProvider.getCurrentDomain(), DOMIBUS_SENDER_TRUST_VALIDATION_EXPRESSION, "");

        final X509Certificate signingCertificate = pkiUtil.createCertificateWithSubject(BigInteger.valueOf(111), certificateSubject);

        defaultAuthorizationServiceSpi.authorizeAgainstCertificateSubjectExpression(signingCertificate);

    }

    @Test
    public void authorizeAgainstCertificateSubjectExpressionMatchingRegExp() {

        PKIUtil pkiUtil = new PKIUtil();

        domibusPropertyProvider.setProperty(domainContextProvider.getCurrentDomain(), DOMIBUS_SENDER_TRUST_VALIDATION_EXPRESSION, "CN=" + PARTY_NAME + " , CN=" + PARTY_NAME1);

        final X509Certificate signingCertificate = pkiUtil.createCertificateWithSubject(BigInteger.valueOf(111), "CN=" + PARTY_NAME);

        defaultAuthorizationServiceSpi.authorizeAgainstCertificateSubjectExpression(signingCertificate);

    }


}
