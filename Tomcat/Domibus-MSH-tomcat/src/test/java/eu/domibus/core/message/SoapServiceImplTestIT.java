package eu.domibus.core.message;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.ebms3.model.Ebms3MessagingWithSecurityProfileMetadata;
import eu.domibus.api.security.SecurityProfileMetadata;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.test.common.SoapSampleUtil;
import org.apache.cxf.binding.soap.SoapMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class SoapServiceImplTestIT extends AbstractTomcatTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SoapServiceImplTestIT.class);

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    SoapServiceImpl soapService;

    @Test
    public void getSecurityProfileMetadataForRsa() throws IOException, EbMS3Exception {
        String filename = "rsa.xml";
        SoapMessage soapMessage = soapSampleUtil.createSoapMessage(filename, UUID.randomUUID() + "@domibus.eu");
        final Ebms3MessagingWithSecurityProfileMetadata messageWithSecurityProfileMetadata = soapService.getMessageWithSecurityProfileMetadata(soapMessage);
        assertNotNull(messageWithSecurityProfileMetadata);

        final SecurityProfileMetadata securityProfileMetadata = messageWithSecurityProfileMetadata.getSecurityProfileMetadata();
        assertNotNull(securityProfileMetadata);

        assertEquals("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", securityProfileMetadata.getSignatureAlgorithm());
        assertEquals("http://www.w3.org/2009/xmlenc11#rsa-oaep", securityProfileMetadata.getEncryptionKeyTransportAlgorithm());
        assertEquals("http://www.w3.org/2009/xmlenc11#mgf1sha256", securityProfileMetadata.getEncryptionMGFAlgorithm());
        assertEquals("http://www.w3.org/2001/04/xmlenc#sha256", securityProfileMetadata.getEncryptionDigestAlgorithm());
    }

    @Test
    public void getSecurityProfileMetadataForX25519() throws IOException, EbMS3Exception {
        String filename = "x25519.xml";
        SoapMessage soapMessage = soapSampleUtil.createSoapMessage(filename, UUID.randomUUID() + "@domibus.eu");
        final Ebms3MessagingWithSecurityProfileMetadata messageWithSecurityProfileMetadata = soapService.getMessageWithSecurityProfileMetadata(soapMessage);
        assertNotNull(messageWithSecurityProfileMetadata);

        final SecurityProfileMetadata securityProfileMetadata = messageWithSecurityProfileMetadata.getSecurityProfileMetadata();
        assertNotNull(securityProfileMetadata);

        assertEquals("http://www.w3.org/2021/04/xmldsig-more#eddsa-ed25519", securityProfileMetadata.getSignatureAlgorithm());
        assertEquals("http://www.w3.org/2001/04/xmlenc#kw-aes128", securityProfileMetadata.getEncryptionKeyTransportAlgorithm());
        assertNull(securityProfileMetadata.getEncryptionMGFAlgorithm());
        assertNull(securityProfileMetadata.getEncryptionDigestAlgorithm());
        assertEquals("http://www.w3.org/2021/04/xmldsig-more#x25519", securityProfileMetadata.getEncryptionKeyAgreementMethod());
        assertEquals("http://www.w3.org/2021/04/xmldsig-more#hkdf", securityProfileMetadata.getEncryptionKeyDerivationFunction());
    }
}
