package eu.domibus.core.plugin.handler;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.model.*;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.api.pki.MultiDomainCryptoService;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.property.DomibusPropertyProvider;
import eu.domibus.api.security.SecurityProfileConfiguration;
import eu.domibus.common.MessageEvent;
import eu.domibus.common.MessageSendSuccessEvent;
import eu.domibus.common.NotificationType;
import eu.domibus.common.PayloadSubmittedEvent;
import eu.domibus.common.model.configuration.LegConfiguration;
import eu.domibus.core.ebms3.EbMS3Exception;
import eu.domibus.core.ebms3.sender.MessageSenderListener;
import eu.domibus.core.ebms3.sender.ResponseResult;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.message.*;
import eu.domibus.core.message.reliability.ReliabilityChecker;
import eu.domibus.core.plugin.BackendConnectorHelper;
import eu.domibus.core.plugin.BackendConnectorProvider;
import eu.domibus.core.pmode.provider.dynamicdiscovery.DynamicDiscoveryAssertionUtil;
import eu.domibus.core.pmode.provider.dynamicdiscovery.DynamicDiscoveryClientSpecification;
import eu.domibus.messaging.MessageConstants;
import eu.domibus.messaging.MessagingProcessingException;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.BackendConnector;
import eu.domibus.plugin.Submission;
import eu.domibus.plugin.handler.MessageSubmitResult;
import eu.domibus.plugin.handler.MessageSubmitter;
import eu.domibus.test.common.BackendConnectorMock;
import eu.domibus.test.common.SoapSampleUtil;
import eu.domibus.test.common.SubmissionUtil;
import org.apache.neethi.Policy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY;
import static eu.domibus.api.property.DomibusPropertyMetadataManagerSPI.DOMIBUS_PARTYINFO_ROLES_VALIDATION_ENABLED;
import static eu.domibus.core.crypto.MultiDomainCryptoServiceImpl.DOMIBUS_KEYSTORE_NAME;
import static eu.domibus.core.crypto.MultiDomainCryptoServiceImpl.DOMIBUS_TRUSTSTORE_NAME;
import static eu.domibus.core.pmode.provider.dynamicdiscovery.DynamicDiscoveryServicePEPPOLConfigurationMockup.DOMAIN;
import static eu.domibus.test.common.SubmissionUtil.DOMIBUS_BLUE;
import static eu.domibus.test.common.SubmissionUtil.DOMIBUS_RED;

@SuppressWarnings("deprecation")
@Transactional
class MessageSubmitterTestIT extends AbstractTomcatTestIT {

    public static final String DOMIBUS_MESSAGE_ID = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

    @Autowired
    DynamicDiscoveryAssertionUtil dynamicDiscoveryAssertionUtil;

    @Autowired
    protected SubmissionUtil submissionUtil;

    @Autowired
    BackendConnectorProvider backendConnectorProvider;

    @Autowired
    MessageSubmitter messageSubmitter;

    @Autowired
    MessagesLogServiceImpl messagesLogService;

    @Autowired
    UserMessageLogDao userMessageLogDao;

    @Autowired
    UserMessageDao userMessageDao;

    @Autowired
    UserMessageDefaultService userMessageDefaultService;

    @Autowired
    MessageSenderListener messageSenderListener;

    @Autowired
    MSHDispatcher mshDispatcher;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    ReliabilityChecker reliabilityChecker;

    @Autowired
    BackendConnectorHelper backendConnectorHelper;

    @Autowired
    DomibusPropertyProvider domibusPropertyProvider;

    @Autowired
    MultiDomainCryptoService multiDomainCryptoService;

    @Autowired
    DomainContextProvider domainContextProvider;

    @Autowired
    protected MessageStatusDao messageStatusDao;

    BackendConnector backendConnector = Mockito.mock(BackendConnector.class);

    private Object savedJmsManagerField;

    @BeforeEach
    void before() throws XmlProcessingException, IOException {
        Mockito.when(backendConnectorProvider.getBackendConnector(Mockito.any(String.class))).thenReturn(backendConnector);
        Mockito.when(backendConnector.getRequiredNotifications()).thenReturn(NotificationType.DEFAULT_PUSH_NOTIFICATIONS);
        uploadPMode();
    }

    @AfterEach
    void clean() {
        itTestsService.deleteAllMessages(DOMIBUS_MESSAGE_ID);
    }

    @Test
    void testBrowseMessageForSentMessageWithEntityId() throws Exception {
        Submission submissionToSend = submissionUtil.createSubmission();
        uploadPMode();
        savedJmsManagerField = itTestsService.prepareMocksForSendingMessage("validAS4Response.xml", submissionToSend.getMessageId(), ReliabilityChecker.CheckResult.OK);

        final MessageSubmitResult messageSubmitResult = messageSubmitter.submitMessage(submissionToSend, BackendConnectorMock.BACKEND_CONNECTOR_NAME);
        UserMessageLog byEntityId = userMessageLogDao.findByEntityId(messageSubmitResult.getMessageEntityId());
        byEntityId.setMessageStatus(messageStatusDao.findOrCreate(MessageStatus.SEND_ENQUEUED));
        messageSenderListener.onMessage(itTestsService.getActiveMQTextMessage("default", messageSubmitResult.getMessageEntityId() + "", messageSubmitResult.getAs4MessageId()));

        itTestsService.resetBackJmsManager(savedJmsManagerField);
    }

    @Test
    void testReliability() throws EbMS3Exception {
        Mockito.when(reliabilityChecker.check(Mockito.any(SOAPMessage.class), Mockito.any(SOAPMessage.class), Mockito.any(ResponseResult.class), Mockito.any(LegConfiguration.class))).thenReturn(ReliabilityChecker.CheckResult.OK);

        final SOAPMessage requestSoapMessage = Mockito.mock(SOAPMessage.class);
        final SOAPMessage responseSoapMessage = Mockito.mock(SOAPMessage.class);
        final ResponseResult responseResult = Mockito.mock(ResponseResult.class);
        final LegConfiguration legConfiguration = Mockito.mock(LegConfiguration.class);
        ReliabilityChecker.CheckResult reliabilityCheckResult = reliabilityChecker.check(requestSoapMessage, responseSoapMessage, responseResult, legConfiguration);
        Assertions.assertNotNull(reliabilityCheckResult);
    }

    @Test
    void messageSendSuccessWithStaticDiscovery() throws Exception {
        //we save the JMS manager to restore it later
        savedJmsManagerField = itTestsService.prepareMocksForSendingMessage("validAS4Response.xml", "123", ReliabilityChecker.CheckResult.OK);

        Submission submission = submissionUtil.createSubmission();
        uploadPMode();
        final String messageId = messageSubmitter.submit(submission, BackendConnectorMock.BACKEND_CONNECTOR_NAME);

        assertUserMessageAndUserMessageLogAfterSubmission(messageId, submission, DOMIBUS_BLUE, DOMIBUS_RED);

        itTestsService.resetBackJmsManager(savedJmsManagerField);
    }

    private void assertUserMessageAndUserMessageLogAfterSubmission(String messageId, Submission submission, String expectedPartyFrom, String expectedPartyTo) {
        //check that PayloadSubmittedEvent was called
        ArgumentCaptor<PayloadSubmittedEvent> payloadSubmittedEventCaptor = ArgumentCaptor.forClass(PayloadSubmittedEvent.class);
        Mockito.verify(backendConnector, Mockito.times(1)).payloadSubmittedEvent(payloadSubmittedEventCaptor.capture());
        final PayloadSubmittedEvent submittedEvent = payloadSubmittedEventCaptor.getValue();
        assertSubmittedEvent(submittedEvent, messageId, expectedPartyFrom, expectedPartyTo);

        //check that MessageSendSuccessEvent was called
        ArgumentCaptor<MessageSendSuccessEvent> messageSendSuccessEventCaptor = ArgumentCaptor.forClass(MessageSendSuccessEvent.class);
        Mockito.verify(backendConnector, Mockito.times(1)).messageSendSuccess(messageSendSuccessEventCaptor.capture());
        final MessageSendSuccessEvent sendSuccessEvent = messageSendSuccessEventCaptor.getValue();
        assertSubmittedEvent(sendSuccessEvent, messageId, expectedPartyFrom, expectedPartyTo);
        Assertions.assertNotNull(sendSuccessEvent.getMessageEntityId());

        //check the UserMessageLog
        final UserMessageLog userMessageLog = userMessageLogDao.findByMessageId(messageId, MSHRole.SENDING);
        Assertions.assertNotNull(userMessageLog);
        Assertions.assertEquals(MessageStatus.ACKNOWLEDGED, userMessageLog.getMessageStatus());
        Assertions.assertEquals(MSHRole.SENDING, userMessageLog.getMshRole().getRole());
        Assertions.assertNotNull(userMessageLog.getAcknowledged());

        //check the UserMessage
        final UserMessage userMessage = userMessageDao.findByEntityId(userMessageLog.getEntityId());
        Assertions.assertNotNull(userMessage);
        Assertions.assertEquals(submission.getRefToMessageId(), userMessage.getRefToMessageId());
        Assertions.assertEquals(submission.getAction(), userMessage.getActionValue());
        Assertions.assertEquals(submission.getService(), userMessage.getServiceValue());

        //check that we can retrieve the message by simulating the UI
        final HashMap<String, Object> filters = new HashMap<>();
        filters.put("receivedTo", new Date());
        messagesLogService.countAndFindPaged(MessageType.USER_MESSAGE, 0, 10, "received", false, filters, Collections.emptyList());
    }

    protected void assertSubmittedEvent(MessageEvent messageEvent, String expectedMessageId, String expectedPartyFrom, String expectedPartyTo) {
        Assertions.assertEquals(expectedMessageId, messageEvent.getMessageId());
        Assertions.assertEquals(expectedPartyFrom, messageEvent.getProps().get(MessageConstants.FROM_PARTY_ID));
        Assertions.assertEquals(expectedPartyTo, messageEvent.getProps().get(MessageConstants.TO_PARTY_ID));
    }

    @Test
    void messageSendSuccessWithDynamicDiscovery() throws MessagingProcessingException, IOException, EbMS3Exception, SOAPException, ParserConfigurationException, SAXException, KeyStoreException {
        //we save the JMS manager to restore it later

        itTestsService.modifyJmsManagerToSubmitUserMessage();
        final Domain currentDomain = domainContextProvider.getCurrentDomain();

        //we create the SoapResponse following the dispatch to C3
        final SOAPMessage soapMessage = soapSampleUtil.createSOAPMessage("validAS4Response.xml", "123");
        Mockito.when(mshDispatcher.dispatch(Mockito.any(SOAPMessage.class), Mockito.anyString(), Mockito.any(SecurityProfileConfiguration.class), Mockito.any(LegConfiguration.class), Mockito.anyString())).thenReturn(soapMessage);

        //reliability is OK
        Mockito.when(reliabilityChecker.check(Mockito.any(SOAPMessage.class), Mockito.any(SOAPMessage.class), Mockito.any(ResponseResult.class), Mockito.any(LegConfiguration.class))).thenReturn(ReliabilityChecker.CheckResult.OK);

        //upload the Pmode for dynamic discovery
        uploadPMode(null, "dataset/pmode/PModeDynamicDiscovery.xml", null);

        //activate dynamic discovery
        domibusPropertyProvider.setProperty(DOMAIN, DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY, "true");
        domibusPropertyProvider.setProperty(DOMAIN, DOMIBUS_PARTYINFO_ROLES_VALIDATION_ENABLED, "false");
        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_CLIENT_SPECIFICATION, DynamicDiscoveryClientSpecification.PEPPOL.getName());
        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_TRANSPORTPROFILEAS_4, "peppol-transport-as4-v2_0");

        //create keystore and truststore
        createStore(DOMIBUS_TRUSTSTORE_NAME, "keystores/gateway_truststore.jks");
        createStore(DOMIBUS_KEYSTORE_NAME, "keystores/gateway_keystore.jks");

        //prepare the submission
        Submission submission = submissionUtil.createSubmission();
        final Submission.TypedProperty finalRecipientProperty = submission.getMessageProperties().stream().filter(typedProperty -> typedProperty.getKey().equals("finalRecipient")).findFirst().orElseThrow(() -> new RuntimeException("Could not find final recipient"));

        final String expectedDiscoveredPartyName = "party1";

        //we set the correct party type according to the Pmode
        submission.getFromParties().clear();
        submission.getFromParties().add(new Submission.Party("blue_gw", "urn:fdc:peppol.eu:2017:identifiers:ap"));
        final String finalRecipient = "0208:1111";
        finalRecipientProperty.setValue(finalRecipient);
        final String messageId = messageSubmitter.submit(submission, BackendConnectorMock.BACKEND_CONNECTOR_NAME);

        //we check that the discovered certificate was added to the truststore by the dynamic discovery mechanism
        final X509Certificate certificateFromTruststore = multiDomainCryptoService.getCertificateFromTruststore(currentDomain, expectedDiscoveredPartyName);
        Assertions.assertNotNull(certificateFromTruststore);

        //check that the party was added in the Pmode by the dynamic discovery mechanism
        dynamicDiscoveryAssertionUtil.verifyIfPartyIsPresentInTheListOfPartiesFromPmode(2, expectedDiscoveredPartyName);
        dynamicDiscoveryAssertionUtil.verifyIfPartyIsPresentInTheListOfPartiesFromPmode(2, expectedDiscoveredPartyName);

        //check an entry was added in the lookup table by the dynamic discovery mechanism
        dynamicDiscoveryAssertionUtil.verifyThatDynamicDiscoveryLookupWasAddedInTheDatabase(1, finalRecipient, expectedDiscoveredPartyName);

        assertUserMessageAndUserMessageLogAfterSubmission(messageId, submission, "blue_gw", expectedDiscoveredPartyName);

        domibusPropertyProvider.setProperty(DOMAIN, DOMIBUS_DYNAMICDISCOVERY_USE_DYNAMIC_DISCOVERY, "false");
        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_CLIENT_SPECIFICATION, DynamicDiscoveryClientSpecification.OASIS.getName());
        domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_DYNAMICDISCOVERY_TRANSPORTPROFILEAS_4, "bdxr-transport-ebms3-as4-v1p0");

        multiDomainCryptoService.removeCertificate(currentDomain, expectedDiscoveredPartyName);
    }

    @Test
    void submitWithNoFromPartyId() throws MessagingProcessingException, IOException {
        Submission submission = submissionUtil.createSubmission();
        submission.getFromParties().clear();

        uploadPMode();

        MessagingProcessingException messagingProcessingException = Assertions.assertThrows(MessagingProcessingException.class,
                () -> messageSubmitter.submit(submission, BackendConnectorMock.BACKEND_CONNECTOR_NAME));
        Assertions.assertTrue(messagingProcessingException.getMessage().contains("Mandatory field From PartyId is not provided"));
    }

    @Test
    void submitWithNoToPartyId() throws MessagingProcessingException, IOException {
        Submission submission = submissionUtil.createSubmission();
        submission.getToParties().clear();

        uploadPMode();

        MessagingProcessingException messagingProcessingException = Assertions.assertThrows(MessagingProcessingException.class,
                () -> messageSubmitter.submit(submission, BackendConnectorMock.BACKEND_CONNECTOR_NAME));
        Assertions.assertTrue(messagingProcessingException.getMessage().contains("ValueInconsistent detail: Mandatory field To PartyId is not provided"));
    }


}
