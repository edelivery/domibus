package eu.domibus.core.util;

import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.property.DomibusPropertyMetadataManagerSPI;
import eu.domibus.api.util.HttpUtil;
import eu.domibus.ext.services.CacheExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;

import javax.net.ssl.SSLHandshakeException;
import java.nio.charset.StandardCharsets;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

/**
 * @author Ionut Breaz
 * @since 5.2
 */
class HttpUtilImplCustomWireMockServerIT extends AbstractTomcatTestIT {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(HttpUtilImplCustomWireMockServerIT.class);

    public static final String MOCKED_CLR_CONTENT = "Mocked CLR content";
    public static final String STORE_PASSWORD = "test123";

    @Autowired
    private CacheExtService cacheExtService;

    @Autowired
    HttpUtil httpUtil;

    @RegisterExtension
    static WireMockExtension wireMockExtension = WireMockExtension.newInstance()
            .options(wireMockConfig()
                    .dynamicPort()
                    .dynamicHttpsPort()
                    // set up WireMock with a trusted TLS certificate
                    .keystorePath(HttpUtilImplCustomWireMockServerIT.class.getClassLoader().getResource("keystores/wiremock_keystore.jks").getPath())
                    .keystorePassword(STORE_PASSWORD)
                    .keyManagerPassword(STORE_PASSWORD))
            .build();

    @Test
    void testDownloadFromATrustedHttpsServer() throws Exception {
        cacheExtService.evictCaches();
        WireMockRuntimeInfo wireMockRuntimeInfo = wireMockExtension.getRuntimeInfo();
        wireMockExtension.stubFor(get("/").willReturn(ok(MOCKED_CLR_CONTENT)));

        // call the WireMock server using TLSGenericTruststore
        String mockedUrl = "https://localhost:" + wireMockRuntimeInfo.getHttpsPort();
        String response = new String(httpUtil.downloadURL(mockedUrl).readAllBytes(), StandardCharsets.UTF_8);
        Assertions.assertEquals(MOCKED_CLR_CONTENT, response);
    }


    @Test
    void testDownloadFromATrustedHttpsServerButWithNoTLSKeystore() {
        cacheExtService.evictCaches();
        WireMockRuntimeInfo wireMockRuntimeInfo = wireMockExtension.getRuntimeInfo();
        wireMockExtension.stubFor(get("/").willReturn(ok()));

        String tlsGenericTruststoreLocation = domibusPropertyProvider.getProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_LOCATION);
        try {
            domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_LOCATION, "");
            String mockedUrl = "https://localhost:" + wireMockRuntimeInfo.getHttpsPort();
            SSLHandshakeException sslException = Assertions.assertThrows(SSLHandshakeException.class, () -> httpUtil.downloadURL(mockedUrl));
            LOG.info("SSL exception: {}", sslException.toString());
            Assertions.assertTrue(sslException.getMessage().contains("PKIX path building failed"));
        } finally {
            domibusPropertyProvider.setProperty(DomibusPropertyMetadataManagerSPI.DOMIBUS_SECURITY_TLS_GENERIC_TRUSTSTORE_LOCATION, tlsGenericTruststoreLocation);
        }
    }

}
