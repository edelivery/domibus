package eu.domibus.core.plugin.handler;

import eu.domibus.AbstractTomcatTestIT;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.usermessage.UserMessageService;
import eu.domibus.common.ErrorResult;
import eu.domibus.common.MSHRole;
import eu.domibus.common.MessageStatus;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.plugin.BackendConnectorProvider;
import eu.domibus.messaging.MessageNotFoundException;
import eu.domibus.messaging.MessagingProcessingException;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.BackendConnector;
import eu.domibus.plugin.Submission;
import eu.domibus.plugin.handler.MessageRetriever;
import eu.domibus.plugin.handler.MessageSubmitResult;
import eu.domibus.plugin.handler.MessageSubmitter;
import eu.domibus.test.ITTestsService;
import eu.domibus.test.common.BackendConnectorMock;
import eu.domibus.test.common.SoapSampleUtil;
import eu.domibus.test.common.SubmissionUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Cosmin Baciu
 * @since 5.2
 */
@Transactional
public class MessageRetrieverTestIT extends AbstractTomcatTestIT {

    public static final String DOMIBUS_MESSAGE_ID = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

    @Autowired
    BackendConnectorProvider backendConnectorProvider;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    MSHWebservice mshWebserviceTest;

    @Autowired
    MessageRetriever messageRetriever;

    @Autowired
    UserMessageService userMessageService;

    @Autowired
    ITTestsService itTestsService;


    @Autowired
    protected SubmissionUtil submissionUtil;

    @Autowired
    MessageSubmitter messageSubmitter;

    @BeforeEach
    public void before() throws IOException, XmlProcessingException {
        BackendConnector backendConnector = Mockito.mock(BackendConnector.class);
        Mockito.when(backendConnectorProvider.getBackendConnector(Mockito.any(String.class))).thenReturn(backendConnector);
        uploadPMode();
    }

    @AfterEach
    public void clean() {
        itTestsService.deleteAllMessages(DOMIBUS_MESSAGE_ID);
    }

    @Test
    void testDownloadMessageWithEntityIdWithMarkDownloadedTrue() throws Exception {
//        Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082").start();

        final UserMessage userMessageByMessageId = receiveUserMessageViaMshAndRetrieveCreatedUserMessage();

        final long userMessageByMessageIdEntityId = userMessageByMessageId.getEntityId();
        assertEquals(MessageStatus.RECEIVED, messageRetriever.getStatus(userMessageByMessageIdEntityId));
        final Submission submission = messageRetriever.downloadMessage(userMessageByMessageIdEntityId);
        assertNotNull(submission);
        assertEquals(userMessageByMessageIdEntityId, submission.getMessageEntityId());
        assertEquals(DOMIBUS_MESSAGE_ID, submission.getMessageId());
        final Set<Submission.Payload> payloads = submission.getPayloads();
        for (Submission.Payload payload : payloads) {
            assertNotNull(payload.getPayloadEntityId());
        }
        assertEquals(MessageStatus.DOWNLOADED, messageRetriever.getStatus(userMessageByMessageIdEntityId));
    }

    @Test
    void testGetMessageStatusByMessageIdAndRole() throws Exception {
        final UserMessage userMessageByMessageId = receiveUserMessageViaMshAndRetrieveCreatedUserMessage();
        assertEquals(MessageStatus.RECEIVED, messageRetriever.getStatus(userMessageByMessageId.getMessageId(), MSHRole.RECEIVING));
    }

    @Test
    void testDownloadMessageWithEntityIdWithMarkDownloadedFalse() throws Exception {
        final UserMessage userMessageByMessageId = receiveUserMessageViaMshAndRetrieveCreatedUserMessage();

        final long userMessageByMessageIdEntityId = userMessageByMessageId.getEntityId();
        assertEquals(MessageStatus.RECEIVED, messageRetriever.getStatus(userMessageByMessageIdEntityId));
        messageRetriever.downloadMessage(userMessageByMessageIdEntityId, false);
        //message is still in RECEIVED
        assertEquals(MessageStatus.RECEIVED, messageRetriever.getStatus(userMessageByMessageIdEntityId));
    }

    @Test
    void testMarkMessageAsDownloadedWithEntityId() throws Exception {
        final UserMessage userMessageByMessageId = receiveUserMessageViaMshAndRetrieveCreatedUserMessage();

        final long userMessageByMessageIdEntityId = userMessageByMessageId.getEntityId();
        assertEquals(MessageStatus.RECEIVED, messageRetriever.getStatus(userMessageByMessageIdEntityId));
        messageRetriever.markMessageAsDownloaded(userMessageByMessageIdEntityId);
        assertEquals(MessageStatus.DOWNLOADED, messageRetriever.getStatus(userMessageByMessageIdEntityId));

        //calling again markMessageAsDownloaded does not trigger an error; markMessageAsDownloaded is idempotent
        messageRetriever.markMessageAsDownloaded(userMessageByMessageIdEntityId);
    }

    @Test
    void testMarkMessageAsDownloadedWithMessageInStatusDifferentThanReceived() throws Exception {
        final UserMessage userMessageByMessageId = receiveUserMessageViaMshAndRetrieveCreatedUserMessage();
        itTestsService.changeMessageStatus(userMessageByMessageId.getEntityId(), eu.domibus.api.model.MessageStatus.ACKNOWLEDGED);

        final long userMessageByMessageIdEntityId = userMessageByMessageId.getEntityId();
        assertEquals(MessageStatus.ACKNOWLEDGED, messageRetriever.getStatus(userMessageByMessageIdEntityId));
        Assertions.assertThrows(MessageNotFoundException.class,
                () -> messageRetriever.markMessageAsDownloaded(userMessageByMessageIdEntityId));
    }

    @Test
    void testBrowseMessageForReceivedMessageWithEntityId() throws Exception {
        final UserMessage userMessageByMessageId = receiveUserMessageViaMshAndRetrieveCreatedUserMessage();

        final long userMessageByMessageIdEntityId = userMessageByMessageId.getEntityId();
        assertEquals(MessageStatus.RECEIVED, messageRetriever.getStatus(userMessageByMessageIdEntityId));
        final Submission submission = messageRetriever.browseMessage(userMessageByMessageIdEntityId);
        assertNotNull(submission);
        assertEquals(userMessageByMessageIdEntityId, submission.getMessageEntityId());
        //message is still in RECEIVED status
        assertEquals(MessageStatus.RECEIVED, messageRetriever.getStatus(userMessageByMessageIdEntityId));
    }

    @Test
    void testBrowseMessageForSentMessageWithEntityId() throws Exception {
        Submission submissionToSend = submissionUtil.createSubmission();
        uploadPMode();
        final MessageSubmitResult messageSubmitResult = messageSubmitter.submitMessage(submissionToSend, BackendConnectorMock.BACKEND_CONNECTOR_NAME);

        final Submission submission = messageRetriever.browseMessage(messageSubmitResult.getMessageEntityId());
        assertNotNull(submission);
    }

    @Test
    void testGetErrors() throws Exception {
        uploadPMode();

        Submission submission = submissionUtil.createSubmission();
        submission.getToParties().clear();
        final String messageId = submission.getMessageId();


        assertThrows(MessagingProcessingException.class, () -> messageSubmitter.submitMessage(submission, BackendConnectorMock.BACKEND_CONNECTOR_NAME));
        final List<? extends ErrorResult> errorsForMessage = messageRetriever.getErrorsForMessage(messageId, MSHRole.SENDING);
        assertTrue(CollectionUtils.isNotEmpty(errorsForMessage));
        assertEquals(1, errorsForMessage.size());
        final ErrorResult errorResult = errorsForMessage.iterator().next();
        assertEquals(MSHRole.SENDING, errorResult.getMshRole());
        assertEquals(messageId, errorResult.getMessageInErrorId());
        assertEquals("EBMS:0003", errorResult.getErrorCodeAsString());
        assertNotNull(errorResult.getErrorDetail());
    }

    private UserMessage receiveUserMessageViaMshAndRetrieveCreatedUserMessage() throws Exception {
        itTestsService.receiveMessage(DOMIBUS_MESSAGE_ID);

        final UserMessage userMessageByMessageId = userMessageService.getByMessageId(DOMIBUS_MESSAGE_ID);
        assertNotNull(userMessageByMessageId);
        return userMessageByMessageId;
    }
}
