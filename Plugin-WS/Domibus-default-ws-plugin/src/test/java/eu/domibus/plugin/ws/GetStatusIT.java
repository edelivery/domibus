package eu.domibus.plugin.ws;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.usermessage.UserMessageService;
import eu.domibus.common.JPAConstants;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.message.MessagingService;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.ws.generated.StatusFault;
import eu.domibus.plugin.ws.generated.body.MessageStatus;
import eu.domibus.plugin.ws.generated.body.MshRole;
import eu.domibus.plugin.ws.generated.body.StatusRequest;
import eu.domibus.plugin.ws.generated.body.StatusRequestWithAccessPointRole;
import eu.domibus.test.DomibusConditionUtil;
import eu.domibus.test.PModeUtil;
import eu.domibus.test.common.SoapSampleUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;

@Transactional
public class GetStatusIT extends AbstractWSPluginTestIT {

    @Autowired
    MessagingService messagingService;

    @Autowired
    MSHWebservice mshWebserviceTest;


    @Autowired
    UserMessageService userMessageService;

    @Autowired
    DomibusConditionUtil domibusConditionUtil;

    @Autowired
    PModeUtil pModeUtil;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @PersistenceContext(unitName = JPAConstants.PERSISTENCE_UNIT_NAME)
    protected EntityManager em;

    @BeforeEach
    public void before(WireMockRuntimeInfo wmRuntimeInfo) throws IOException, XmlProcessingException {
        pModeUtil.uploadPmode(wmRuntimeInfo.getHttpPort());
    }

    @Test
    public void testGetStatusReceived() throws StatusFault, IOException, SOAPException, SAXException, ParserConfigurationException {
        String filename = "SOAPMessage2.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        StatusRequestWithAccessPointRole messageStatusRequest = createMessageStatusRequest(messageId, MshRole.RECEIVING);
        MessageStatus response = webServicePluginInterface.getStatusWithAccessPointRole(messageStatusRequest);
        Assertions.assertEquals(MessageStatus.RECEIVED, response);
    }

    @Test
    public void testGetStatusInvalidId() throws StatusFault {
        String invalidMessageId = "invalid";
        StatusRequestWithAccessPointRole messageStatusRequest = createMessageStatusRequest(invalidMessageId, MshRole.RECEIVING);
        MessageStatus response = webServicePluginInterface.getStatusWithAccessPointRole(messageStatusRequest);
        Assertions.assertEquals(MessageStatus.NOT_FOUND, response);
    }

    @Test
    public void testGetStatusEmptyMessageId() {
        String emptyMessageId = "";
        StatusRequestWithAccessPointRole messageStatusRequest = createMessageStatusRequest(emptyMessageId, MshRole.RECEIVING);
        try {
            webServicePluginInterface.getStatusWithAccessPointRole(messageStatusRequest);
            Assertions.fail();
        } catch (StatusFault statusFault) {
            String message = "Message ID is empty";
            Assertions.assertEquals(message, statusFault.getMessage());
        }
    }

    private StatusRequestWithAccessPointRole createMessageStatusRequest(final String messageId, MshRole role) {
        StatusRequestWithAccessPointRole statusRequest = new StatusRequestWithAccessPointRole();
        statusRequest.setMessageID(messageId);
        statusRequest.setAccessPointRole(role);
        return statusRequest;
    }

    @Test
    public void getStatusWithMessageIdAndMessageEntityId() throws SOAPException, IOException, ParserConfigurationException, SAXException, StatusFault {
        String filename = "SOAPMessage2.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        final UserMessage userMessage = userMessageService.getMessageEntity(messageId, MSHRole.RECEIVING);

        StatusRequest statusRequestWithMessageId = new StatusRequest();
        statusRequestWithMessageId.setMessageID(messageId);
        final MessageStatus statusWithMessageId = webServicePluginInterface.getStatus(statusRequestWithMessageId);

        Assertions.assertEquals(MessageStatus.RECEIVED, statusWithMessageId);

        StatusRequest statusRequestWithMessageEntityId = new StatusRequest();
        statusRequestWithMessageEntityId.setMessageEntityID(userMessage.getEntityId());
        final MessageStatus statusWithMessageEntityId = webServicePluginInterface.getStatus(statusRequestWithMessageEntityId);
        Assertions.assertEquals(MessageStatus.RECEIVED, statusWithMessageEntityId);
    }
}
