package eu.domibus.plugin.ws.webservice;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import eu.domibus.api.jms.JMSManager;
import eu.domibus.api.model.MSHRole;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.message.MessagingService;
import eu.domibus.core.message.UserMessageLogDefaultService;
import eu.domibus.core.message.retention.MessageRetentionDefaultService;
import eu.domibus.core.pmode.ConfigurationDAO;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.handler.MessageRetriever;
import eu.domibus.plugin.ws.AbstractWSPluginTestIT;
import eu.domibus.plugin.ws.connector.WSPluginImpl;
import eu.domibus.plugin.ws.generated.RetrieveMessageFault;
import eu.domibus.plugin.ws.generated.body.ObjectFactory;
import eu.domibus.plugin.ws.generated.body.RetrieveMessageRequest;
import eu.domibus.plugin.ws.generated.body.RetrieveMessageResponse;
import eu.domibus.plugin.ws.generated.header.common.model.org.oasis_open.docs.ebxml_msg.ebms.v3_0.ns.core._200704.Messaging;
import eu.domibus.plugin.ws.generated.header.common.model.org.oasis_open.docs.ebxml_msg.ebms.v3_0.ns.core._200704.UserMessage;
import eu.domibus.test.common.SoapSampleUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Holder;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RetrieveMessageIT extends AbstractWSPluginTestIT {

    @Autowired
    JMSManager jmsManager;

    @Autowired
    MessagingService messagingService;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    protected MessageRetriever messageRetriever;

    @Autowired
    UserMessageLogDefaultService userMessageLogService;

    @Autowired
    protected ConfigurationDAO configurationDAO;

    @Autowired
    MSHWebservice mshWebserviceTest;

    @Autowired
    WSPluginImpl wsPlugin;

    @Autowired
    MessageRetentionDefaultService messageRetentionDefaultService;

    @BeforeEach
    public void updatePMode(WireMockRuntimeInfo wmRuntimeInfo) throws IOException, XmlProcessingException {
        uploadPmode(wmRuntimeInfo.getHttpPort());
    }

    @Test
    void testMessageIdEmpty() {
        Assertions.assertThrows(RetrieveMessageFault.class, () -> retrieveMessageFail("", "Please provide message id or message entity id"));
    }

    @Test
    void testMessageNotFound() {
        Assertions.assertThrows(RetrieveMessageFault.class, () -> retrieveMessageFail("notFound", "Message not found, id [notFound]"));
    }

    @Test
    public void testRetrieveMessageBasedOnMessageId() throws Exception {
        String filename = "SOAPMessage2.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        RetrieveMessageRequest retrieveMessageRequest = createRetrieveMessageRequest(messageId);
        Holder<RetrieveMessageResponse> retrieveMessageResponse = new Holder<>();
        Holder<eu.domibus.plugin.ws.generated.header.common.model.org.oasis_open.docs.ebxml_msg.ebms.v3_0.ns.core._200704.Messaging> ebMSHeaderInfo = new Holder<>();

        webServicePluginInterface.retrieveMessage(retrieveMessageRequest, retrieveMessageResponse, ebMSHeaderInfo);

        final Messaging messaging = ebMSHeaderInfo.value;
        final UserMessage userMessage = messaging.getUserMessage();
        assertEquals(messageId, userMessage.getMessageInfo().getMessageId());
        assertEquals(2, userMessage.getMessageProperties().getProperty().size());
    }

    @Test
    public void testRetrieveMessageBasedOnMessageEntityId() throws Exception {
        String filename = "SOAPMessage2.xml";
        String messageId = "43bb6883-77d2-4a41-bac4-52a485d50084@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        final eu.domibus.api.model.UserMessage userMessageFromDb = userMessageDefaultService.getByMessageId(messageId, MSHRole.RECEIVING);

        RetrieveMessageRequest retrieveMessageRequest = new RetrieveMessageRequest();
        retrieveMessageRequest.setMessageEntityID(new ObjectFactory().createRetrieveMessageRequestMessageEntityID(userMessageFromDb.getEntityId()));

        Holder<RetrieveMessageResponse> retrieveMessageResponse = new Holder<>();
        Holder<eu.domibus.plugin.ws.generated.header.common.model.org.oasis_open.docs.ebxml_msg.ebms.v3_0.ns.core._200704.Messaging> ebMSHeaderInfo = new Holder<>();

        webServicePluginInterface.retrieveMessage(retrieveMessageRequest, retrieveMessageResponse, ebMSHeaderInfo);

        final Messaging messaging = ebMSHeaderInfo.value;
        final UserMessage userMessage = messaging.getUserMessage();
        assertEquals(messageId, userMessage.getMessageInfo().getMessageId());
        assertEquals(2, userMessage.getMessageProperties().getProperty().size());
    }

    private void retrieveMessageFail(String messageId, String errorMessage) throws RetrieveMessageFault {
        RetrieveMessageRequest retrieveMessageRequest = createRetrieveMessageRequest(messageId);

        Holder<RetrieveMessageResponse> retrieveMessageResponse = new Holder<>();
        Holder<eu.domibus.plugin.ws.generated.header.common.model.org.oasis_open.docs.ebxml_msg.ebms.v3_0.ns.core._200704.Messaging> ebMSHeaderInfo = new Holder<>();

        try {
            webServicePluginInterface.retrieveMessage(retrieveMessageRequest, retrieveMessageResponse, ebMSHeaderInfo);
        } catch (RetrieveMessageFault re) {
            assertEquals(errorMessage, re.getMessage());
            throw re;
        }
        Assertions.fail("DownloadMessageFault was expected but was not raised");
    }


    private RetrieveMessageRequest createRetrieveMessageRequest(String messageId) {
        RetrieveMessageRequest retrieveMessageRequest = new RetrieveMessageRequest();
        retrieveMessageRequest.setMessageID(new ObjectFactory().createRetrieveMessageRequestMessageID(messageId));
        return retrieveMessageRequest;
    }
}
