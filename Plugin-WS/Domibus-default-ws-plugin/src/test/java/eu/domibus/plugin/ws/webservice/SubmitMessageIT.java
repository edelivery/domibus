package eu.domibus.plugin.ws.webservice;


import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.usermessage.UserMessageService;
import eu.domibus.common.ErrorCode;
import eu.domibus.core.ebms3.sender.client.MSHDispatcher;
import eu.domibus.core.error.ErrorLogService;
import eu.domibus.core.message.reliability.ReliabilityChecker;
import eu.domibus.core.message.retention.MessageRetentionDefaultService;
import eu.domibus.core.pmode.multitenancy.MultiDomainPModeProvider;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.ws.AbstractWSPluginTestIT;
import eu.domibus.plugin.ws.generated.body.ErrorResultImplArray;
import eu.domibus.plugin.ws.generated.body.GetErrorsRequest;
import eu.domibus.plugin.ws.generated.body.SubmitRequest;
import eu.domibus.plugin.ws.generated.body.SubmitResponse;
import eu.domibus.plugin.ws.generated.header.common.model.org.oasis_open.docs.ebxml_msg.ebms.v3_0.ns.core._200704.Messaging;
import eu.domibus.test.common.SoapSampleUtil;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by draguio on 17/02/2016.
 */
public class SubmitMessageIT extends AbstractWSPluginTestIT {

    private final static DomibusLogger LOG = DomibusLoggerFactory.getLogger(SubmitMessageIT.class);

    @Autowired
    MessageRetentionDefaultService messageRetentionDefaultService;


    @Autowired
    UserMessageService userMessageService;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    ErrorLogService errorLogService;

    @Autowired
    MSHDispatcher mshDispatcher;

    @Autowired
    MultiDomainPModeProvider pModeProvider;

    @Autowired
    ReliabilityChecker reliabilityChecker;

    @BeforeEach
    public void before(WireMockRuntimeInfo wmRuntimeInfo) throws IOException, XmlProcessingException {
        uploadPmode(wmRuntimeInfo.getHttpPort());
    }

    /**
     * Test for the backend sendMessage service with payload profile enabled
     */
    @Test
    public void testSubmitMessageValid() throws Exception {
        doSubmitMessageValid();
    }

    private SubmitResponse doSubmitMessageValid() throws Exception {
        String payloadHref = "cid:message";
        SubmitRequest submitRequest = createSubmitRequestWs(payloadHref);
        Messaging ebMSHeaderInfo = createMessageHeaderWs(payloadHref);

        //we save the JMS manager to restore it later
        Object savedJmsManagerField = itTestsService.prepareMocksForSendingMessage("validAS4Response.xml", "123", ReliabilityChecker.CheckResult.OK);

        super.prepareSendMessage("validAS4Response.xml", Pair.of("MESSAGE_ID", UUID.randomUUID() + "@domibus.eu"), Pair.of("REF_MESSAGE_ID", UUID.randomUUID() + "@domibus.eu"));
        SubmitResponse response = webServicePluginInterface.submitMessage(submitRequest, ebMSHeaderInfo);

        //put the real manager back
        itTestsService.setJmsManager(savedJmsManagerField);

        final List<String> messageID = response.getMessageID();
        assertNotNull(response);
        assertNotNull(messageID);
        assertEquals(1, messageID.size());
        final String messageId = messageID.iterator().next();

        final List<Long> messageEntityID = response.getMessageEntityID();
        assertNotNull(messageEntityID);
        assertEquals(1, messageEntityID.size());

        domibusConditionUtil.waitUntilMessageIsAcknowledged(messageId);

        deleteAllMessages(messageID.get(0));

        return response;
    }

    @Test
    public void testSubmitMessageAndFailToSendMessage() throws Exception {
        String payloadHref = "cid:message";
        SubmitRequest submitRequest = createSubmitRequestWs(payloadHref);
        Messaging ebMSHeaderInfo = createMessageHeaderWs(payloadHref);

        //we modify the retry policy so that the message fails immediately and not goes in WAITING_FOR_RETRY
        itTestsService.updatePmodeWithZeroRetries();

        //we save the JMS manager to restore it later
        Object savedJmsManagerField = itTestsService.prepareMocksForFailingSendingMessage(new RuntimeException("Simulating dispatch error"));

        super.prepareSendMessage("validAS4Response.xml", Pair.of("MESSAGE_ID", UUID.randomUUID() + "@domibus.eu"), Pair.of("REF_MESSAGE_ID", UUID.randomUUID() + "@domibus.eu"));
        SubmitResponse response = webServicePluginInterface.submitMessage(submitRequest, ebMSHeaderInfo);

        //put the real manager back
        itTestsService.resetBackJmsManager(savedJmsManagerField);

        final List<String> messageID = response.getMessageID();
        assertNotNull(response);
        assertNotNull(messageID);
        assertEquals(1, messageID.size());
        final String messageId = messageID.iterator().next();

        final List<Long> messageEntityID = response.getMessageEntityID();
        assertNotNull(messageEntityID);
        assertEquals(1, messageEntityID.size());

        domibusConditionUtil.waitUntilMessageIsSentFailed(messageId);

        final UserMessage userMessage = userMessageService.getMessageEntity(messageId, MSHRole.SENDING);
        errorLogService.createErrorLog(messageId, ErrorCode.EBMS_0004, "simulating dispatch error", MSHRole.SENDING, userMessage);

        //get errors based on the message id
        GetErrorsRequest errorRequest = new GetErrorsRequest();
        errorRequest.setMessageID(messageId);
        final ErrorResultImplArray messageErrors = webServicePluginInterface.getMessageErrors(errorRequest);
        assertTrue(messageErrors.getItem().size() > 0);

        //get errors based on the message entity id
        GetErrorsRequest errorRequestWithMessageEntityId = new GetErrorsRequest();
        errorRequestWithMessageEntityId.setMessageEntityID(userMessage.getEntityId());
        final ErrorResultImplArray messageErrorsBasedOnMessageEntityId = webServicePluginInterface.getMessageErrors(errorRequestWithMessageEntityId);
        assertTrue(messageErrorsBasedOnMessageEntityId.getItem().size() > 0);

        deleteAllMessages(messageID.get(0));
    }
}
