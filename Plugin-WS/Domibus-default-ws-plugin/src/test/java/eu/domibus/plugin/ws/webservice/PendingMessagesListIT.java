package eu.domibus.plugin.ws.webservice;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import eu.domibus.api.jms.JMSManager;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.UserMessage;
import eu.domibus.api.usermessage.UserMessageService;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.ws.AbstractWSPluginTestIT;
import eu.domibus.plugin.ws.generated.ListPendingMessagesFault;
import eu.domibus.plugin.ws.generated.body.ListPendingMessagesRequest;
import eu.domibus.plugin.ws.generated.body.ListPendingMessagesResponse;
import eu.domibus.plugin.ws.message.WSMessageLogDao;
import eu.domibus.test.common.SoapSampleUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This JUNIT implements the Test cases List Pending Messages-01 and List Pending Messages-02.
 *
 * @author Cosmin Baciu
 * @author martifp
 */
public class PendingMessagesListIT extends AbstractWSPluginTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PendingMessagesListIT.class);

    @Autowired
    private WSMessageLogDao wsMessageLogDao;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    MSHWebservice mshWebserviceTest;

    @Autowired
    UserMessageService userMessageService;

    @Autowired
    JMSManager jmsManager;

    @BeforeEach
    public void before(WireMockRuntimeInfo wmRuntimeInfo) throws IOException, XmlProcessingException {
        pModeUtil.uploadPmode(wmRuntimeInfo.getHttpPort());
    }

    @AfterEach
    public void clean() {
        deleteAllMessages();
        wsMessageLogDao.deleteAll(wsMessageLogDao.findAll());
    }

    @Test
    public void testListPendingMessages() throws ListPendingMessagesFault, SOAPException, IOException, ParserConfigurationException, SAXException {
        wsMessageLogDao.deleteAll(wsMessageLogDao.findAll());

        String filename = "SOAPMessage2.xml";
        String messageId1 = "123@domibus.eu";
        String messageId2 = "567@domibus.eu";
        itTestsService.receiveMessage(filename, messageId1);
        itTestsService.receiveMessage(filename, messageId2);

        //get all messages
        ListPendingMessagesRequest requestAllMessages = new ListPendingMessagesRequest();
        ListPendingMessagesResponse response = webServicePluginInterface.listPendingMessages(requestAllMessages);
        // Verifies the response
        assertNotNull(response);
        assertTrue(response.getMessageID().contains(messageId1));
        assertEquals(2, response.getMessageID().size());

        //based on message id
        ListPendingMessagesRequest requestMessageBasedOnMessageId = new ListPendingMessagesRequest();
        requestMessageBasedOnMessageId.setMessageId(messageId1);
        ListPendingMessagesResponse responseBaseOnMessageId = webServicePluginInterface.listPendingMessages(requestMessageBasedOnMessageId);
        assertNotNull(responseBaseOnMessageId);
        assertEquals(1, responseBaseOnMessageId.getMessageID().size());
        assertTrue(responseBaseOnMessageId.getMessageID().contains(messageId1));

        //based on message entity id
        final UserMessage userMessage = userMessageService.getMessageEntity(messageId1, MSHRole.RECEIVING);
        ListPendingMessagesRequest requestMessageBasedOnMessageEntityId = new ListPendingMessagesRequest();
        requestMessageBasedOnMessageEntityId.setMessageEntityID(userMessage.getEntityId());
        ListPendingMessagesResponse responseBaseOnMessageEntityId = webServicePluginInterface.listPendingMessages(requestMessageBasedOnMessageEntityId);
        assertNotNull(responseBaseOnMessageEntityId);
        assertEquals(1, responseBaseOnMessageEntityId.getMessageID().size());
        assertTrue(responseBaseOnMessageEntityId.getMessageID().contains(messageId1));
    }

    @Test
    public void testDownloadedMessagesAreNotInPendingMessages() throws SOAPException, IOException, ParserConfigurationException, SAXException, ListPendingMessagesFault {
        wsMessageLogDao.deleteAll(wsMessageLogDao.findAll());

        String filename = "SOAPMessage2.xml";
        String messageId1 = "123@domibus.eu";
        String messageId2 = "567@domibus.eu";

        itTestsService.receiveMessage(filename, messageId1);
        itTestsService.receiveMessage(filename, messageId2);

        //get all messages
        ListPendingMessagesRequest requestAllMessages = new ListPendingMessagesRequest();
        ListPendingMessagesResponse response = webServicePluginInterface.listPendingMessages(requestAllMessages);
        // Verifies the response
        assertNotNull(response);
        assertTrue(response.getMessageID().contains(messageId1));
        assertTrue(response.getMessageID().contains(messageId2));
        assertEquals(2, response.getMessageID().size());

        ((WebServiceImpl)webServicePluginInterface).downloadUserMessage(messageId1);
        response = webServicePluginInterface.listPendingMessages(requestAllMessages);
        // Verifies the response
        assertNotNull(response);
        assertFalse(response.getMessageID().contains(messageId1));
        assertTrue(response.getMessageID().contains(messageId2));
        assertEquals(1, response.getMessageID().size());
    }

}
