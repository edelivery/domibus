package eu.domibus.plugin.ws.webservice;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import eu.domibus.api.model.MSHRole;
import eu.domibus.api.model.UserMessage;
import eu.domibus.core.ebms3.receiver.MSHWebservice;
import eu.domibus.core.message.retention.MessageRetentionDefaultService;
import eu.domibus.messaging.XmlProcessingException;
import eu.domibus.plugin.ws.AbstractWSPluginTestIT;
import eu.domibus.plugin.ws.backend.dispatch.WSPluginDispatchClientProvider;
import eu.domibus.plugin.ws.generated.MarkMessageAsDownloadedFault;
import eu.domibus.plugin.ws.generated.body.*;
import eu.domibus.plugin.ws.generated.header.common.model.org.oasis_open.docs.ebxml_msg.ebms.v3_0.ns.core._200704.Messaging;
import eu.domibus.test.common.SoapSampleUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Holder;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * This class implements the test cases Receive Message-01 and Receive Message-02.
 *
 * @author draguio
 * @author martifp
 */
@Transactional
public class ReceiveMessageIT extends AbstractWSPluginTestIT {

    @Autowired
    MSHWebservice mshWebserviceTest;

    @Autowired
    SoapSampleUtil soapSampleUtil;

    @Autowired
    MessageRetentionDefaultService messageRetentionDefaultService;

    @Autowired
    WSPluginDispatchClientProvider wsPluginDispatchClientProvider;


    @BeforeEach
    public void before(WireMockRuntimeInfo wmRuntimeInfo) throws IOException, XmlProcessingException {
        uploadPmode(wmRuntimeInfo.getHttpPort());
    }

    /**
     * This test invokes the MSHWebService and verifies that the message is stored
     * in the database with the status RECEIVED
     *
     * @throws SOAPException, IOException, SQLException, ParserConfigurationException, SAXException
     *                        <p>
     *                        ref: Receive Message-01
     */
    @Test
    public void testReceiveMessage() throws SOAPException, IOException, ParserConfigurationException, SAXException, InterruptedException {
        String filename = "SOAPMessage2.xml";
        String messageId = UUID.randomUUID() + "@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);
    }

    @Test
    public void testDeleteBatch() throws SOAPException, IOException, ParserConfigurationException, SAXException, InterruptedException, NoSuchAlgorithmException, KeyManagementException {
        String filename = "SOAPMessage2.xml";
        String messageId = UUID.randomUUID() + "@domibus.eu";

        Dispatch dispatch = Mockito.mock(Dispatch.class);
        SOAPMessage reply = Mockito.mock(SOAPMessage.class);
        Mockito.when(dispatch.invoke(Mockito.any(SOAPMessage.class)))
                .thenReturn(reply);
        Mockito.when(wsPluginDispatchClientProvider.getClient(Mockito.any(String.class)))
                .thenReturn(dispatch);

        itTestsService.receiveMessage(filename, messageId);

        deleteAllMessages(messageId);

        Thread.sleep(1000);
    }

    @Test
    public void testReceiveTestMessage() throws Exception {
        String filename = "SOAPTestMessage.xml";
        String messageId = "ping123@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);
    }

    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void testMarkMessageAsDownloadedUsingMessageId() throws Exception {
        String filename = "SOAPMessage2.xml";
        String messageId = "ping123@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        MarkMessageAsDownloadedRequest markMessageAsDownloadedRequest = new MarkMessageAsDownloadedRequest();
        markMessageAsDownloadedRequest.setMessageID(new ObjectFactory().createMarkMessageAsDownloadedRequestMessageID(messageId));
        Holder<MarkMessageAsDownloadedResponse> markMessageAsDownloadedResponse = new Holder(new MarkMessageAsDownloadedResponse());
        Holder<Messaging> headerInfo = new Holder(new Messaging());

        webServicePluginInterface.markMessageAsDownloaded(markMessageAsDownloadedRequest, markMessageAsDownloadedResponse, headerInfo);

        assertEquals(messageId, markMessageAsDownloadedResponse.value.getMessageID());
    }

    @Test
    public void testMarkMessageAsDownloadedUsingTooLongMessageId() {
        String messageId = StringUtils.repeat("X", 256);

        MarkMessageAsDownloadedRequest markMessageAsDownloadedRequest = new MarkMessageAsDownloadedRequest();
        markMessageAsDownloadedRequest.setMessageID(new ObjectFactory().createMarkMessageAsDownloadedRequestMessageID(messageId));
        Holder<MarkMessageAsDownloadedResponse> markMessageAsDownloadedResponse = new Holder(new MarkMessageAsDownloadedResponse());
        Holder<Messaging> headerInfo = new Holder(new Messaging());

        try {
            webServicePluginInterface.markMessageAsDownloaded(markMessageAsDownloadedRequest, markMessageAsDownloadedResponse, headerInfo);
            Assertions.fail();
        } catch (MarkMessageAsDownloadedFault markMessageAsDownloadedFault) {
            String message = "Invalid Message Id. ";
            Assertions.assertEquals(message, markMessageAsDownloadedFault.getMessage());
        }
    }

    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void testMarkMessageAsDownloadedUsingMessageEntityId() throws Exception {
        String filename = "SOAPMessage2.xml";
        String messageId = "ping123@domibus.eu";

        itTestsService.receiveMessage(filename, messageId);

        final UserMessage userMessage = userMessageDefaultService.getByMessageId(messageId, MSHRole.RECEIVING);

        MarkMessageAsDownloadedRequest markMessageAsDownloadedRequest = new MarkMessageAsDownloadedRequest();
        final long userMessageEntityId = userMessage.getEntityId();
        markMessageAsDownloadedRequest.setMessageEntityID(new ObjectFactory().createMarkMessageAsDownloadedRequestMessageEntityID(userMessageEntityId));
        Holder<MarkMessageAsDownloadedResponse> markMessageAsDownloadedResponse = new Holder(new MarkMessageAsDownloadedResponse());
        Holder<Messaging> headerInfo = new Holder(new Messaging());

        webServicePluginInterface.markMessageAsDownloaded(markMessageAsDownloadedRequest, markMessageAsDownloadedResponse, headerInfo);

        assertEquals(userMessageEntityId, markMessageAsDownloadedResponse.value.getMessageEntityID());
    }
}
