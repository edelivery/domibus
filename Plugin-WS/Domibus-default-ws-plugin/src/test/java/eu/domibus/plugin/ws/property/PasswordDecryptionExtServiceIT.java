package eu.domibus.plugin.ws.property;

import eu.domibus.api.multitenancy.DomainContextProvider;
import eu.domibus.core.converter.DomibusCoreMapper;
import eu.domibus.core.property.decryption.plugin.PasswordDecryptionExtServiceImpl;
import eu.domibus.core.property.encryption.plugin.PasswordEncryptionExtServiceImpl;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.domain.PasswordEncryptionResultDTO;
import eu.domibus.ext.services.PasswordDecryptionExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.plugin.ws.AbstractWSPluginTestIT;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

import static eu.domibus.api.property.DomibusConfigurationService.PASSWORD_ENCRYPTION_ACTIVE_PROPERTY;
import static eu.domibus.plugin.ws.property.WSPluginPropertyManager.DISPATCHER_PUSH_AUTH_PASSWORD;
import static org.apache.commons.lang3.StringUtils.isNoneBlank;

/**
 * @author Soumya Chandran
 * @since 5.2
 */
public class PasswordDecryptionExtServiceIT extends AbstractWSPluginTestIT {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PasswordDecryptionExtServiceIT.class);

    @Autowired
    private WSPluginPropertyManager wsPluginPropertyManager;

    @Autowired
    private PasswordDecryptionExtService passwordDecryptionExtService;

    @Autowired
    private PasswordDecryptionExtServiceImpl pwdDecryptionServiceImpl;

    @Autowired
    private DomainContextProvider domainContextProvider;

    @Autowired
    private DomibusCoreMapper coreMapper;

    @Autowired
    private PasswordEncryptionExtServiceImpl passwordEncryptionService;

    @Test
    public void decryptProperty() {
        String password = wsPluginPropertyManager.getKnownPropertyValue(DISPATCHER_PUSH_AUTH_PASSWORD);
        final DomainDTO domain = coreMapper.domainToDomainDTO(domainContextProvider.getCurrentDomainSafely());
        LOG.debug("Decrypting wsplugin.push.auth property password [{}] for domain [{}]", password, domain);
        String decryptedPassword = null;
        if (isNoneBlank(DISPATCHER_PUSH_AUTH_PASSWORD, password)) {
            decryptedPassword = passwordDecryptionExtService.decryptProperty(domain, DISPATCHER_PUSH_AUTH_PASSWORD, password);
        }
        Assertions.assertEquals(decryptedPassword, password);
    }

    @Test
    public void isValueEncryptedNonEncrypted() {
        boolean isValueEncrypted = passwordDecryptionExtService.isValueEncrypted("nonEncrypted");
        Assertions.assertFalse(isValueEncrypted);
    }

    @Test
    public void isValueEncryptedBlank() {
        boolean isValueEncrypted = passwordDecryptionExtService.isValueEncrypted("");
        Assertions.assertFalse(isValueEncrypted);
    }

    @Test
    public void decryptPropertyIfEncrypted() {
        String encryptionOldValue = domibusPropertyProvider.getProperty(PASSWORD_ENCRYPTION_ACTIVE_PROPERTY);
        String oldPassword = wsPluginPropertyManager.getKnownPropertyValue(DISPATCHER_PUSH_AUTH_PASSWORD);
        final DomainDTO domain = coreMapper.domainToDomainDTO(domainContextProvider.getCurrentDomainSafely());
        domibusPropertyProvider.setProperty(PASSWORD_ENCRYPTION_ACTIVE_PROPERTY, "true");
        domibusPropertyProvider.setProperty(DISPATCHER_PUSH_AUTH_PASSWORD, "passwordtest");

        final PasswordEncryptionResultDTO passwordEncryptionResult = passwordEncryptionService.encryptProperty(domain, "wsplugin.push.auth.password", "passwordtest");
        Assertions.assertTrue(passwordEncryptionResult.getFormattedBase64EncryptedValue().contains("ENC"));

        String decryptedPassword = passwordDecryptionExtService.decryptPropertyIfEncrypted(domain, "wsplugin.push.auth.password", passwordEncryptionResult.getFormattedBase64EncryptedValue());
        Assertions.assertEquals(decryptedPassword, "passwordtest");

        domibusPropertyProvider.setProperty(PASSWORD_ENCRYPTION_ACTIVE_PROPERTY, encryptionOldValue);
        domibusPropertyProvider.setProperty(DISPATCHER_PUSH_AUTH_PASSWORD, Objects.requireNonNullElse(oldPassword, ""));
    }

}

