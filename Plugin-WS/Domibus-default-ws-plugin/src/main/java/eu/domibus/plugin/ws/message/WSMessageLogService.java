package eu.domibus.plugin.ws.message;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Catalin Enache
 * @since 5.0
 */
public interface WSMessageLogService {

    void create(WSMessageLogEntity wsMessageLogEntity);

    WSMessageLogEntity findByMessageId(final String messageId);

    WSMessageLogEntity findByMessageEntityId(Long messageEntityId);

    void delete(WSMessageLogEntity wsMessageLogEntity);

    int deleteByMessageId(final String messageId);

    void deleteByMessageIds(List<String> messageIds);

    List<WSMessageLogEntity> findAllWithFilter(final String messageId, Long messageEntityId, final String fromPartyId,
                                               final String conversationId, final String referenceMessageId, final String originalSender,
                                               final String finalRecipient, LocalDateTime receivedFrom, LocalDateTime receivedTo, int maxPendingMessagesRetrieveCount);
}

